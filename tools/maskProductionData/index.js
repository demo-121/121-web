const fs = require('fs');
const path = require('path');
const process = require('process');
const moveFrom = path.join(__dirname, '/originalFiles');
const moveTo = path.join(__dirname, '/maskedFiles/files');
const _ = require('lodash');
const agentCodeReMapping = {
    fieldIds: ['agentcode', 'agentid'],
    replaceValue: '010010'
};

const writeFile = (file, pathToSave) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(pathToSave, JSON.stringify(file), 'utf8', (saveErr) => {
            if (saveErr) {
                reject(`save error ${saveErr}`);
            } else {
                resolve();
            }
        });
    }).catch(err => {
        console.log(err);
    });
};

const maskData = (file, outputName) => {
    _.each(_.keys(file), key => {
        if (file[key] instanceof Object) {
            maskData(file[key], outputName);
        } else if (outputName.substr(0,2) === 'CP' &&  _.toLower(key).indexOf('email') > -1) {
            file[key] = '123@test.com';
        } else if (outputName.substr(0,2) === 'CP' &&  key.indexOf('mobileCountryCode') > -1) {
            file[key] = '+65';
        } else if (outputName.substr(0,2) === 'CP' &&  key.indexOf('mobileNo') > -1) {
            file[key] = '123';
        } else if (_.toLower(key).indexOf('email') > -1 || _.toLower(key).indexOf('mobile') > -1) {
            // console.log(`maskData :: ${file[key]} ${key}`);
            file[key] = '';
        } else if (agentCodeReMapping.fieldIds.indexOf(_.toLower(key)) > -1) {
            file[key] = agentCodeReMapping.replaceValue;
        }
    });
};

const convertOutputName = fileName => {
    const relativeInitial = ['CP', 'FN', 'NB', 'QU', 'SA', 'SP'];
    let splitedFileName = fileName.split('.');
    let convertedOutputName = fileName;
    let defaultName = true;
    _.each(splitedFileName, splitedName => {
        if (relativeInitial.indexOf(splitedName.substr(0,2)) > -1) {
            convertedOutputName = splitedName;
            defaultName = false;
        }
    });
    return defaultName ? convertedOutputName : convertedOutputName + '.json';
};

const main = () => {
// Loop through all the files in the temp directory
fs.readdir(moveFrom, function (err, files) {
  if (err) {
    console.error('Could not list the directory.', err);
    process.exit(1);
  }
  let importFile = fs.createWriteStream('./maskedFiles/updateDocs.txt');
  files.forEach(function (file, index) {
    let fromPath = path.join(moveFrom, file);
    fs.stat(fromPath, async function (error, stat) {
        if (stat.isFile && path.extname(fromPath) === '.json' || path.extname(fromPath) === '.JSON') {
            let outputName = convertOutputName(file);
            let toPath = path.join(moveTo, outputName);
            importFile.write('D\t' + outputName.split(path.extname(outputName))[0] + '\t' + outputName + '\n');
            let inputFile = require(fromPath);
            maskData(inputFile, outputName);
            inputFile = _.omit(inputFile, ['_id', '_rev']);
            await writeFile(inputFile, toPath);
        }
    });
  });
});
};

main();
