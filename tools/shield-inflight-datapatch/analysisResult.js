const _ = require('lodash');
const fs = require('fs');


const main = function () {
    const result = require('./files/DataPatchJob_2018_8_14');
    const trx = _.get(result, 'log.1534240800000.transaction');
    let impactedList = fs.createWriteStream('analysisResult.csv');
    impactedList.write('Policy_Number,Status_Before_Datapatch,Status_After_Datapatch\n');
    _.each(trx, t => {
        let policyNumber = _.get(t, 'docId');
        let befores = _.get(t, 'beforeDataPatch.status');
        let afters = _.get(t, 'afterDataPatch.status');
        if (afters && !_.isEmpty(afters) && policyNumber.indexOf('SP') === -1) {
            impactedList.write(`${policyNumber},${convertStatus(befores)},${convertStatus(afters)}\n`);  
        }
    });
};

const convertStatus = function(status) {
    switch (status) {
        case 'A': 
            return 'Approved';
        case 'R':
            return 'Reject';
        case 'E':
            return 'Expired'
        case 'Invalidated Paid ':
            return 'Invalidated Paid';
        case 'Invalidated':
            return 'Invalidated';
        case 'API_RESUBMISSION':
            return 'API_RESUBMISSION';
        case 'SUBMITTED':
            return 'SUBMITTED';
        default:
            return '';
    }
};

main();
