const _ = require('lodash');
const fs = require('fs');
const PDFHandler = require('../../bz/PDFHandler');
const path = require('path');
const async = require('async');

const syncGeneratePdf = (policyData, supervisorTemplate, lang, callback) => {
    try {
        let policyNumber = policyData.id;
        let fileName = `./result_pdf/eapproval_supervisor_pdf_${policyNumber}.pdf`;
        if (policyData) {
            return new Promise(resolve => {
                PDFHandler.getSupervisorTemplatePdf(policyData.value, supervisorTemplate, lang, function (pdf) {
                    fs.writeFile(fileName, pdf, 'base64', err => {
                        console.log(`writeFile :: ${err}`);
                        callback(null, {success: true});
                    });
                });
            }).catch(err => {
                console.log(`syncGeneratePdf ${policyNumber} :: ${err}`);
                callback(null, {success: true});
            });
        } else {
            console.log(`syncGeneratePdf ${policyNumber} :: No Policy Data`);
            return Promise.resolve().then(() =>{
                callback(null, {success: true});
            });
        }
    } catch (err) {
        console.log(`syncGeneratePdf try catch :: ${err}`);
    }
};

const syncPdfGeneration = (wholeSetOfPolicyData, index, supervisorTemplate, lang) => {
    async.waterfall([
        (callback) => {
            syncGeneratePdf(wholeSetOfPolicyData[index], supervisorTemplate, lang, callback);
        },
    ], (err, approvalCase) => {
        if (err) {
            console.log(`syncPdfGeneration :: ${err}`);
        }
        if (index + 1 < wholeSetOfPolicyData.length) {
            syncPdfGeneration(wholeSetOfPolicyData, index + 1, supervisorTemplate, lang);
        } else {
            console.log('Completed');
        }
    });
};

const main = function () {
    // Extract the Couchbase View
    const caseToProcessFromCouchbaseView = require('./files/cbViewResult');
    // Template going to use
    const supervisorTemplate = require('./files/supervisorTemplate');
    const lang = 'en';
    var JavaRunner = require('../../bz/JavaRunner');
    global.rootPath = path.join(__dirname, '../..');
    global.JavaRunner = new JavaRunner();
    try {
        syncPdfGeneration(caseToProcessFromCouchbaseView && caseToProcessFromCouchbaseView.rows, 0, supervisorTemplate, lang);
    } catch (exp) {
        console.error(exp);
    }
};

main();
