const _ = require('lodash');
const fs = require('fs');
const PDFHandler = require('../../bz/PDFHandler');
const commonApp = require('../../bz/handler/application/common');
const path = require('path');
const async = require('async');

const syncGeneratePdf = (app, templates, lang, callback) => {
  try {
    let policyNumber = app.policyNumber;
    let fileName = `./result_pdf/appPdf_${policyNumber}.pdf`;

    const isShowDollarSign = _.get(app, 'applicationForm.values.appFormTemplate.properties.dollarSignForAllCcy', true);
    const ccy = isShowDollarSign ? null : _.get(app, 'quotation.ccy');
    const paymentTmpl = require('./files/paymentTmpl');
    let paymentValues = _.cloneDeep(app.payment);
    let trigger = {};
    commonApp.replaceAppFormValuesByTemplate(paymentTmpl, app.payment, paymentValues, trigger, [], lang, ccy);
    let reportData = {
      root: {
        payment: paymentValues,
        originalData: app.payment,
        policyOptions: app.quotation.policyOptions
      }
    };

    if (app) {
      return new Promise(resolve => {
        PDFHandler.getPremiumPaymentPdf(reportData, templates, lang, function (pdf) {
          fs.writeFile(fileName, pdf, 'base64', err => {
            console.log(`writeFile :: ${err}`);
            callback(null, {
              success: true
            });
          });
        });
      }).catch(err => {
        console.log(`syncGeneratePdf ${policyNumber} :: ${err}`);
        callback(null, {
          success: true
        });
      });
    } else {
      console.log(`syncGeneratePdf ${policyNumber} :: No Policy Data`);
      return Promise.resolve().then(() => {
        callback(null, {
          success: true
        });
      });
    }
  } catch (err) {
    console.log(`syncGeneratePdf try catch :: ${err}`);
  }
};

const syncPdfGeneration = (wholeSetOfPolicyData, index, supervisorTemplate, lang) => {
  async.waterfall([
    (callback) => {
      syncGeneratePdf(wholeSetOfPolicyData[index], supervisorTemplate, lang, callback);
    },
  ], (err, approvalCase) => {
    if (err) {
      console.log(`syncPdfGeneration :: ${err}`);
    }
    if (index + 1 < wholeSetOfPolicyData.length) {
      syncPdfGeneration(wholeSetOfPolicyData, index + 1, supervisorTemplate, lang);
    } else {
      console.log('Completed');
    }
  });
};

const calculatePremiumValues = (app, proposerProfile) => {
  let values = {
    proposalNo: '',
    policyCcy: '',
    initBasicPrem: 0,
    backdatingPrem: 0,
    topupPrem: 0,
    initRspPrem: 0,
    initTotalPrem: 0,
    initPayMethod: '',
    initPayMethodButton: false,
    ttRemittingBank: '',
    ttDOR: '',
    ttRemarks: '',
    subseqPayMethod: '',
    paymentMode: '',
    rspSelect: false,
    covCode: '',
    paymentMethod: '',
    cpfisoaBlock: {
      idCardNo: ''
    },
    cpfissaBlock: {
      idCardNo: ''
    },
    srsBlock: {
      idCardNo: ''
    }
  };
  const quot = app.quotation;
  var initBasicPrem = 0;
  var backdatingPrem = 0;
  var topupPrem = 0;
  var initRspPrem = 0;
  var initTotalPrem = 0;
  var backdatedDay = 0;

  var signedDate = null;
  var backdate = null;
  if (proposerProfile && !proposerProfile.error) {
    values.initPayMethodButton = app.hasOwnProperty('isInitialPaymentCompleted') ? app.isInitialPaymentCompleted : false;
    values.proposalNo = app.policyNumber || app._id;
    values.policyCcy = quot.ccy;
    values.baseProductCode = quot.baseProductCode;
    values.paymentMode = quot.paymentMode;
    values.rspSelect = _.get(quot, 'policyOptions.rspSelect') || _.get(quot, 'policyOptions.rspInd') ? 'Y' : 'N';
    values.covCode = quot.baseProductCode;
    values.paymentMethod = (quot.policyOptions && quot.policyOptions.paymentMethod) ? quot.policyOptions.paymentMethod : '';
    values.cpfisoaBlock.idCardNo = proposerProfile.idCardNo;
    values.cpfissaBlock.idCardNo = proposerProfile.idCardNo;
    values.srsBlock.idCardNo = proposerProfile.idCardNo;

    // Calculate All Premium
    let initBasicPrem = quot.premium * (quot.paymentMode === 'M' ? 2 : 1);

    //- Backdating Premium
    if (quot.isBackDate == 'Y' || app.isBackDate) {
      signedDate = new Date(app.applicationSignedDate.substring(0, 10) + 'T00:00:00.000Z');
      backdate = new Date(quot.riskCommenDate);
      backdatedDay = Math.ceil((signedDate.getTime() - backdate.getTime()) / (1000 * 3600 * 24));
      var backdatedMonth = 0;
      if (backdatedDay == 0) {
        backdatedMonth = 0;
      } else if (backdatedDay > 0 && backdatedDay <= 30) {
        backdatedMonth = 1;
      } else if (backdatedDay > 30 && backdatedDay <= 61) {
        backdatedMonth = 2;
      } else if (backdatedDay > 61 && backdatedDay <= 91) {
        backdatedMonth = 3;
      } else if (backdatedDay > 91 && backdatedDay <= 122) {
        backdatedMonth = 4;
      } else if (backdatedDay > 122 && backdatedDay <= 152) {
        backdatedMonth = 5;
      } else if (backdatedDay > 152) {
        backdatedMonth = 6;
      }

      backdatingPrem = 0;
      if (quot.paymentMode == 'M') {
        backdatingPrem = quot.premium * backdatedMonth;
      } else if (quot.paymentMode == 'Q') {
        if (backdatedMonth > 1 && backdatedMonth <= 4) {
          backdatingPrem = quot.premium;
        } else if (backdatedMonth > 4 && backdatedMonth <= 6) {
          backdatingPrem = quot.premium * 2;
        }
      } else if (quot.paymentMode == 'S') {
        backdatingPrem = quot.premium * (backdatedMonth > 4 && backdatedMonth <= 6 ? 1 : 0);
      }
    }

    // Calculate RSP Premium
    initRspPrem = 0;
    let rspAmount = _.get(quot, 'policyOptions.rspAmount') || _.get(quot, 'policyOptions.rspAmt', 0);
    if (rspAmount > 0) {
      // rspPayFreq is quotation field from SAV, IND, SHIELD. rspFrequency is quotation field from FLEXI
      const rspPayFreq = _.get(quot, 'policyOptions.rspPayFreq');

      // Init RSP Premium should be two times of RSPAmount for monthly mode, and equals to RSPAmount for Annaul/Semi-Annual/Quarter modes
      if (rspPayFreq === 'monthly' || rspPayFreq === 'M') {
        initRspPrem = rspAmount * 2;
      } else {
        initRspPrem = rspAmount;
      }

    }

    //TODO: Update the folloiwng premium logic
    topupPrem = _.get(quot, 'policyOptions.topUpAmt', 0);

    initTotalPrem = initBasicPrem + backdatingPrem + topupPrem + initRspPrem;

    values.initBasicPrem = initBasicPrem;
    if (backdatingPrem > 0) {
      values.backdatingPrem = backdatingPrem;
    }
    if (topupPrem > 0) {
      values.topupPrem = topupPrem;
    }
    if (initRspPrem > 0) {
      values.initRspPrem = initRspPrem;
    }
    values.initTotalPrem = initTotalPrem;

    return values;
  } else {
    return values;
  }
}

const main = function () {
  // Extract the Couchbase View
  let app = require('./files/NB001208-00007');
  const proposerProfile = require('./files/CP001208-00008');
  // Template going to use
  const paymentMain = require('./files/appform_report_payment_main');
  const common = require('./files/appform_report_common');
  const premium = require('./files/appform_report_premium_payment');
  
  const payment = calculatePremiumValues(app, proposerProfile);
  _.each(_.keys(payment), (key) => {
      app.payment[key] = payment[key];
  });
  app.payment.initPayMethod = 'crCard';
  app.payment.initPayMethodButton = 'initPayMethod';
  app.payment.subseqPayMethod = 'N';

//   console.log(app.payment);

  const templates = [];
  templates.push(paymentMain);
  templates.push(common);
  templates.push(premium);

  const lang = 'en';
  var JavaRunner = require('../../bz/JavaRunner');
  global.rootPath = path.join(__dirname, '../..');
  global.JavaRunner = new JavaRunner();
  try {
    syncGeneratePdf(app, templates, lang, (result) => {
      console.log(`completed :: ${result}`);
    });
  } catch (exp) {
    console.error(exp);
  }
};

main();
