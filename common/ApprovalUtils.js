const _ = require('lodash');

module.exports.getHeaderTemplate = (isIphone) => {
  if (isIphone) {
    return {
      'id': 'sortSearchCase',
      'type': 'Picker',
      'options': [
          {
              'value': 'newToold',
              'title': {
                  'en': 'Sort by Newest'
              }
          },
          {
            'value': 'oldTonew',
            'title': {
                'en': 'Sort by Oldest'
            }
        }
      ]
    };
  } else {
    return {
      'id': 'sortSearchCase',
      'type': 'Picker',
      'options': [
          {
              'value': 'oldTonew',
              'title': {
                  'en': 'Sort by Date (Oldest to Newest)'
              }
          },
          {
              'value': 'newToold',
              'title': {
                  'en': 'Sort by Date (Newest to Oldest)'
              }
          }
      ]
    };
  }
};
