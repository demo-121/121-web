<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>
  <match target="pattern">
    <test qual="any" name="family">
      <string>Source Sans</string>
    </test>
    <edit name="family" mode="assign">
      <string>Source Sans Pro</string>
    </edit>
  </match>
  <!-- Source Sans Pro's language specific fallback -->
  <match>
    <test name="lang">
      <string>zh-cn</string>
    </test>
    <test name="family">
      <string>Source Sans Pro</string>
    </test>
    <edit name="family" mode="append">
      <string>Source Han Sans CN</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>zh-sg</string>
    </test>
    <test name="family">
      <string>Source Sans Pro</string>
    </test>
    <edit name="family" mode="append">
      <string>Source Han Sans CN</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>zh-tw</string>
    </test>
    <test name="family">
      <string>Source Sans Pro</string>
    </test>
    <edit name="family" mode="append">
      <string>Source Han Sans TWHK</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>zh-hk</string>
    </test>
    <test name="family">
      <string>Source Sans Pro</string>
    </test>
    <edit name="family" mode="append">
      <string>Source Han Sans TWHK</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>zh</string>
    </test>
    <test name="family">
      <string>Source Sans Pro</string>
    </test>
    <edit name="family" mode="append">
      <string>Source Han Sans CN</string>
      <string>Source Han Sans TWHK</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>jp</string>
    </test>
    <test name="family">
      <string>Source Sans Pro</string>
    </test>
    <edit name="family" mode="append">
      <string>Source Han Sans JP</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>kr</string>
    </test>
    <test name="family">
      <string>Source Sans Pro</string>
    </test>
    <edit name="family" mode="append">
      <string>Source Han Sans KR</string>
    </edit>
  </match>
  <!-- sans-serif prepend -->
  <match>
    <test name="lang">
      <string>zh-cn</string>
    </test>
    <test name="family">
      <string>sans-serif</string>
    </test>
    <edit name="family" mode="prepend">
      <string>Source Han Sans CN</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>zh-sg</string>
    </test>
    <test name="family">
      <string>sans-serif</string>
    </test>
    <edit name="family" mode="prepend">
      <string>Source Han Sans CN</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>zh-tw</string>
    </test>
    <test name="family">
      <string>sans-serif</string>
    </test>
    <edit name="family" mode="prepend">
      <string>Source Han Sans TWHK</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>zh-hk</string>
    </test>
    <test name="family">
      <string>sans-serif</string>
    </test>
    <edit name="family" mode="prepend">
      <string>Source Han Sans TWHK</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>zh</string>
    </test>
    <test name="family">
      <string>sans-serif</string>
    </test>
    <edit name="family" mode="prepend">
      <string>Source Han Sans CN</string>
      <string>Source Han Sans TWHK</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>jp</string>
    </test>
    <test name="family">
      <string>sans-serif</string>
    </test>
    <edit name="family" mode="prepend">
      <string>Source Han Sans JP</string>
    </edit>
  </match>
  <match>
    <test name="lang">
      <string>kr</string>
    </test>
    <test name="family">
      <string>sans-serif</string>
    </test>
    <edit name="family" mode="prepend">
      <string>Source Han Sans KR</string>
    </edit>
  </match>
  <!-- Source Han Sans fallback -->
  <!-- Noto Sans CJK is identical to Source Han Sans -->
  <alias>
    <family>Source Han Sans CN</family>
    <accept>
      <family>Noto Sans S Chinese</family>
      <family>Source Han Sans</family>
    </accept>
    <default>
      <family>sans-serif</family>
    </default>
  </alias>
  <alias>
    <family>Source Han Sans TWHK</family>
    <accept>
      <family>Noto Sans T Chinese</family>
      <family>Source Han Sans</family>
    </accept>
    <default>
      <family>sans-serif</family>
    </default>
  </alias>
  <alias>
    <family>Source Han Sans JP</family>
    <accept>
      <family>Noto Sans Japanese</family>
      <family>Source Han Sans</family>
    </accept>
    <default>
      <family>sans-serif</family>
    </default>
  </alias>
  <alias>
    <family>Source Han Sans KR</family>
    <accept>
      <family>Noto Sans Korean</family>
      <family>Source Han Sans</family>
    </accept>
    <default>
      <family>sans-serif</family>
    </default>
  </alias>
  <alias>
    <family>Source Han Sans</family>
    <accept>
      <family>Noto Sans CJK</family>
      <family>Source Sans Pro</family>
    </accept>
    <default>
      <family>sans-serif</family>
    </default>
  </alias>
  <!-- Generic name aliasing -->
  <alias>
    <family>Source Sans Pro</family>
    <accept>
      <family>Source Han Sans</family>
      <family>Source Han Sans CN</family>
      <family>Source Han Sans TWHK</family>
      <family>Source Han Sans JP</family>
      <family>Source Han Sans KR</family>
    </accept>
    <default>
      <family>sans-serif</family>
    </default>
  </alias>
  <alias>
    <family>sans-serif</family>
    <prefer>
      <family>Source Sans Pro</family>
      <family>Source Han Sans</family>
      <family>Source Han Sans CN</family>
      <family>Source Han Sans TWHK</family>
      <family>Source Han Sans JP</family>
      <family>Source Han Sans KR</family>
    </prefer>
  </alias>
</fontconfig>