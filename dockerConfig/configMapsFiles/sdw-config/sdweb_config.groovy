println "begin custom configuration"

sdweb.external_server_url="https://web.ease-sit.axa.com.sg/sdweb"

sdweb.plugins.loadlist = ['de.softpro.sdweb.plugins.impl.ServletDms']
sdwebplugins.de.softpro.sdweb.plugins.impl.ServletDms.target_urls=["http://ease-web:3000/setAttachmentSigned"]
sdwebplugins.de.softpro.sdweb.plugins.impl.ServletDms.use_all_targets=true
sdwebplugins.de.softpro.sdweb.plugins.impl.ServletDms.createXMLFile=false

sdweb.authenticate.pluginid="de.softpro.sdweb.plugins.impl.BasicAuthenticator"
sdwebplugins.de.softpro.sdweb.plugins.impl.BasicAuthenticator.enabled=true

sdweb.certificate.store.pkcs12.file="/var/softpro/sdweb_home/cert/cert_store.p12"
sdweb.certificate.store.pkcs12.password="secret"

sdweb.usage.enable.aboutpage=false
sdweb.usage.enable.loadpage=false

sdweb.signature.display.signtime=true
sdweb.defaults.signature.date.format="dd/MM/yyyy HH:mm:ss Z"

sdweb.capture.html5_mobile=force
sdweb.capture.html5_desktop=force

println "end custom configuration"
