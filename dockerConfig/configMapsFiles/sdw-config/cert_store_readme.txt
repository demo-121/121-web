The certstore.p12 file contains a selfsigned sample PKCS#12 certificate.
This file should be replaced by your own PKCS#12 Certificate.
Right now only one key pair per PKCS#12 certificate store is supported.