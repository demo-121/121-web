function(quotation, planInfo, planDetail) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var roundUp = function(value, digit) {
    var scale = Math.pow(10, digit);
    if (digit === 0) {
      return Math.ceil(value);
    } else {
      return Math.ceil(number * scale) / scale;
    }
  };
  var numberTrunc = function(value = 0, digit) {
    value = value.toString();
    if (value.indexOf('.') === -1) {
      return math.bignumber(Number.parseInt(value));
    } else {
      value = value.substr(0, value.indexOf('.') + digit + 1);
      return math.bignumber(Number.parseFloat(value));
    }
  };
  var round = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var premium = null;
  if (quotation.baseProductCode === 'ESP') {
    premium = math.number(numberTrunc(math.divide(math.multiply(math.bignumber(runFunc(planDetail.formulas.getESPPremRate, quotation, planInfo, planDetail, quotation.pAge)), math.add(math.bignumber(_.get(quotation, 'plans[0].yearPrem', 0)), math.bignumber(_.get(quotation, 'plans[1].yearPrem', 0)))), 100), 2));
    var modalFactor = 1;
    for (var p in planDetail.payModes) {
      if (planDetail.payModes[p].mode === quotation.paymentMode) {
        modalFactor = planDetail.payModes[p].factor;
      }
    }
    premium = (modalFactor === 1) ? premium : roundDown(math.multiply(premium, modalFactor), 2);
  } else if (planInfo.premTerm && planInfo.policyTerm && quotation.plans[0] && quotation.plans[0].premium) {
    var covCodeArr = ['TPX', 'TPPX', 'DIS', 'CRX', 'DCB', 'CIP', 'AP', 'SV'];
    var AB_WU = 0;
    for (var v in quotation.plans) {
      if (covCodeArr.indexOf(quotation.plans[v].covCode) > -1) {
        AB_WU += quotation.plans[v].yearPrem;
      }
    }
    var naP = runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.pAge);
    premium = round(roundDown(math.divide(roundDown(math.multiply(naP, roundDown(AB_WU, 1)), 2), 100), 2), 2);
    var modalFactor = 1;
    for (var p in planDetail.payModes) {
      if (planDetail.payModes[p].mode === quotation.paymentMode) {
        modalFactor = planDetail.payModes[p].factor;
      }
    }
    premium = (modalFactor === 1) ? premium : roundDown(math.multiply(premium, modalFactor), 2);
  } else {
    premium = 0;
  }
  return premium;
}