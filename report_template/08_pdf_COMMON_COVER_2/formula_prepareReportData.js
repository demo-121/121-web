function(quotation, planInfo, planDetails, extraPara) {
  var get_policy_currency = () => {
    var ccy = quotation.ccy;
    var polCcy = '';
    var ccySyb = '';
    if (ccy == 'SGD') {
      polCcy = 'Singapore Dollars';
      ccySyb = 'S$';
    } else if (ccy == 'USD') {
      polCcy = 'US Dollars';
      ccySyb = 'US$';
    } else if (ccy == 'ASD') {
      polCcy = 'Australian Dollars';
      ccySyb = 'A$';
    } else if (ccy == 'EUR') {
      polCcy = 'Euro';
      ccySyb = '€';
    } else if (ccy == 'GBP') {
      polCcy = 'British Pound';
      ccySyb = '£';
    }
    return polCcy;
  };
  var reportData = {};
  Object.assign(reportData, extraPara.company);
  reportData.sysDateDesc = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  reportData.riskCommenDateDesc = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  var payModeDescs = {
    A: 'Annual',
    S: 'Semi-Annual',
    Q: 'Quarterly',
    M: 'Monthly',
    L: 'Single Premium'
  };
  reportData.payModeDesc = payModeDescs[quotation.paymentMode];
  reportData.ccyDesc = get_policy_currency();
  reportData.pGenderDesc = quotation.pGender == 'M' ? 'Male' : 'Female';
  reportData.pSmokeDesc = quotation.pSmoke == 'Y' ? 'Smoker' : 'Non-Smoker';
  reportData.pDobDesc = new Date(quotation.pDob).format(extraPara.dateFormat);
  reportData.pResidenceDesc = _.get(_.find(extraPara.optionsMap.residency.options, opt => opt.value === quotation.pResidence), 'title.en');
  reportData.pResidenceCityDesc = _.get(_.find(extraPara.optionsMap.city.options, opt => opt.value === quotation.pResidenceCity), 'title.en');
  reportData.iGenderDesc = quotation.iGender == 'M' ? 'Male' : 'Female';
  reportData.iSmokeDesc = quotation.iSmoke == 'Y' ? 'Smoker' : 'Non-Smoker';
  reportData.iDobDesc = new Date(quotation.iDob).format(extraPara.dateFormat);
  reportData.iResidenceDesc = _.get(_.find(extraPara.optionsMap.residency.options, opt => opt.value === quotation.iResidence), 'title.en');
  reportData.iResidenceCityDesc = _.get(_.find(extraPara.optionsMap.city.options, opt => opt.value === quotation.iResidenceCity), 'title.en');
  var plans = [];
  _.each(quotation.plans, (plan) => {
    plans.push({
      covName: plan.covName.en,
      polTermDesc: plan.polTermDesc,
      premTermDesc: plan.premTermDesc,
      sumInsured: getCurrency(plan.sumInsured, '', 0)
    });
  });
  reportData.plans = plans;
  reportData.planCodes = _.join(_.map(quotation.plans, p => p.planCode), ', ');
  return reportData;
}