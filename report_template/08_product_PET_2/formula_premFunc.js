function(quotation, planInfo, planDetail) {
  var trunc = function(value, position) {
    return runFunc(planDetail.formulas.trunc, value, position);
  };
  var premium = math.bignumber(runFunc(planDetail.formulas.getPremium, quotation, planInfo, planDetail));
  var payModes = planDetail.payModes;
  for (var i = 0; i < payModes.length; i++) {
    let {
      mode,
      factor,
      operator
    } = payModes[i];
    factor = math.bignumber(factor);
    if (quotation.paymentMode == mode) {
      if (operator == 'M') {
        premium = math.multiply(premium, factor);
      } else if (operator == 'D') {
        premium = math.divide(premium, factor);
      }
    }
  }
  return math.number(trunc(premium, 2));
}