function(quotation, planInfo, planDetails, extraPara) { /**prepareReportData MAIN BI*/
  var trunc = function(value, position) {
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.number(math.divide(math.floor(math.multiply(value, scale)), scale));
  };
  var numberTrunc = function(value, digit) {
    value = value.toString();
    if (value.indexOf('.') === -1) {
      return Number.parseInt(value);
    } else {
      value = value.substr(0, value.indexOf('.') + digit + 1);
      return Number.parseFloat(value);
    }
  };
  var paddingZeroRight = function(value) {
    value = value.toString();
    if (value.indexOf('.') > -1) {
      var decimal = value.split('.');
      if (decimal[1].length === 1) {
        decimal[1] = decimal[1] + '0';
        return decimal[0] + '.' + decimal[1];
      } else {
        return value;
      }
    } else {
      return value + '.00';
    }
  };
  var illustrations = extraPara.illustrations;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb
  } = extraPara.company;
  var retVal = {
    'compName': compName,
    'compRegNo': compRegNo,
    'compAddr': compAddr,
    'compAddr2': compAddr2,
    'compTel': compTel,
    'compFax': compFax,
    'compWeb': compWeb
  };
  var basicPlan = quotation.plans[0];
  var basicIllustrations = illustrations[basicPlan.covCode];
  var illustrateData_DB = [];
  var illustrateData_SV_b4_mature = [];
  var illustrateData_SV_at_mature = [];
  var illustrateData_deduct = [];
  var illustrateData_totalDistribCost = [];
  var polTerm = basicPlan.policyTerm ? basicPlan.policyTerm : 15;
  var projections = planDetails[planInfo.covCode].projection;
  var low = 999;
  var high = -1;
  for (var i = 0; i < 2; i++) {
    var id = projections[i].title.en;
    low = Math.min(Number(id), low);
    high = Math.max(Number(id), high);
  }
  for (var i = 0; i < polTerm; i++) {
    var polYr = i + 1;
    var row = {};
    var basicIllustration = basicIllustrations[i];
    var polYr_age = polYr + '/' + basicIllustration.age;
    var totalPremPaid = getCurrency(trunc(Number(basicIllustration.totalPremPaid), 0), ' ', 0);
    if (polYr <= 10 || (polYr > 10 && polYr % 5 === 0) || polYr == polTerm) {
      row = {
        polYr_age: polYr_age,
        totalPremPaid: totalPremPaid,
        GteedDB: getCurrency(trunc(Number(basicIllustration.GteedDB), 0), ' ', 0),
        nonGteedDB_low: getCurrency(trunc(Number(basicIllustration.nonGteedDB[low]), 0), ' ', 0),
        totalDB_low: getCurrency(trunc(Number(basicIllustration.totalDB[low]), 0), ' ', 0),
        nonGteedDB_high: getCurrency(trunc(Number(basicIllustration.nonGteedDB[high]), 0), ' ', 0),
        totalDB_high: getCurrency(trunc(Number(basicIllustration.totalDB[high]), 0), ' ', 0),
      };
      illustrateData_DB.push(row);
      row = {
        polYr_age: polYr_age,
        totalPremPaid: totalPremPaid,
        effectOfDeduct_high: getCurrency(trunc(Number(basicIllustration.effectOfDeduct[high]), 0), ' ', 0),
        VOP_high: getCurrency(trunc(Number(basicIllustration.VOP[high]), 0), ' ', 0),
        totalSV_high: getCurrency(trunc(Number(basicIllustration.totalSV[high]), 0), ' ', 0),
        effectOfDeduct_low: getCurrency(trunc(Number(basicIllustration.effectOfDeduct[low]), 0), ' ', 0),
        VOP_low: getCurrency(trunc(Number(basicIllustration.VOP[low]), 0), ' ', 0),
        totalSV_low: getCurrency(trunc(Number(basicIllustration.totalSV[low]), 0), ' ', 0),
      };
      illustrateData_deduct.push(row);
      row = {
        polYr_age: polYr_age,
        totalPremPaid: totalPremPaid,
        totalDistribCost: getCurrency(trunc(Number(basicIllustration.totalDistribCost), 0), ' ', 0),
      };
      illustrateData_totalDistribCost.push(row);
      row = {
        polYr_age: polYr_age,
        totalPremPaid: totalPremPaid,
        annaulGteedCashPayout: getCurrency(Number(basicIllustration.GteedAnnualCashback), ' ', 0),
        GteedSV: getCurrency(trunc(Number(basicIllustration.GteedSV), 0), ' ', 0),
        nonGteedSV_low: getCurrency(trunc(Number(basicIllustration.nonGteedSV[low]), 0), ' ', 0),
        totalSV_low: getCurrency(trunc(Number(basicIllustration.totalSV[low]), 0), ' ', 0),
        nonGteedSV_high: getCurrency(trunc(Number(basicIllustration.nonGteedSV[high]), 0), ' ', 0),
        totalSV_high: getCurrency(trunc(Number(basicIllustration.totalSV[high]), 0), ' ', 0),
      };
      if (i < polTerm - 1) {
        illustrateData_SV_b4_mature.push(row);
      } else {
        illustrateData_SV_at_mature.push(row);
      }
    }
  }
  retVal.illustrateData_DB = illustrateData_DB;
  retVal.illustrateData_SV_b4_mature = illustrateData_SV_b4_mature;
  retVal.illustrateData_SV_at_mature = illustrateData_SV_at_mature;
  retVal.illustrateData_deduct = illustrateData_deduct;
  retVal.illustrateData_totalDistribCost = illustrateData_totalDistribCost;
  var reductInYield_high = basicIllustrations[0].reductYield[high];
  var reductInYield_low = basicIllustrations[0].reductYield[low];
  if (reductInYield_high) {
    reductInYield_high = math.multiply(math.bignumber(reductInYield_high), math.bignumber(100));
    retVal.reductInYield_high = math.number(math.round(reductInYield_high, 2));
  }
  if (reductInYield_low) {
    reductInYield_low = math.multiply(math.bignumber(reductInYield_low), math.bignumber(100));
    retVal.reductInYield_low = math.number(math.round(reductInYield_low, 2));
  }
  retVal.rate_high = (basicIllustrations[0].projRate[high] * 100);
  retVal.rate_low = (basicIllustrations[0].projRate[low] * 100);
  retVal.deduct_high = getCurrency(trunc(Number(basicIllustrations[basicIllustrations.length - 1].effectOfDeduct[high]), 0), ' ', 0), retVal.deduct_low = getCurrency(trunc(Number(basicIllustrations[basicIllustrations.length - 1].effectOfDeduct[low]), 0), ' ', 0), retVal.headerName = quotation.sameAs == "Y" ? quotation.pFullName : quotation.iFullName;
  retVal.headerGender = (quotation.sameAs == "Y" ? quotation.pGender : quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.headerSmoke = (quotation.sameAs == "Y" ? quotation.pSmoke : quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.headerAge = quotation.sameAs == "Y" ? quotation.pAge : quotation.iAge;
  var ccy = quotation.ccy;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = 'US$';
  } else if (ccy == 'ASD') {
    polCcy = 'Australian Dollars';
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    polCcy = 'British Pound';
    ccySyb = '£';
  }
  retVal.polCcy = polCcy;
  retVal.ccySyb = ccySyb;
  var paymentModeTitle = '';
  if (quotation.paymentMode == 'A') {
    paymentModeTitle = 'Annual';
  } else if (quotation.paymentMode == 'S') {
    paymentModeTitle = 'Semi-Annual';
  } else if (quotation.paymentMode == 'Q') {
    paymentModeTitle = 'Quarterly';
  } else if (quotation.paymentMode == 'M') {
    paymentModeTitle = 'Monthly';
  }
  retVal.paymentModeTitle = paymentModeTitle;
  retVal.basicAnnualPrem = getCurrency(planInfo.yearPrem, ' ', 2);
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat); /** FAIR BI */
  retVal.HighIRR = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE.HighIRR;
  retVal.LowIRR = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE.LowIRR;
  retVal.YTM_H = paddingZeroRight(numberTrunc(math.multiply(math.bignumber(extraPara.illustrations[quotation.plans[0].covCode][0].reductYield[4.75]), 100), 2));
  retVal.YTM_L = paddingZeroRight(numberTrunc(math.multiply(math.bignumber(extraPara.illustrations[quotation.plans[0].covCode][0].reductYield[3.25]), 100), 2)); /** FAIR BI */
  return retVal;
}