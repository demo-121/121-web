package html2jsonStr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class convert {
	
	public static String removeSpecialCharacter(String inStr) {
		return inStr.replace("\"", "\\\"")
				.replaceAll("\t", "")
				.replace("\n", "")
				.replace("\r", "");
	}
	
	public static String extractTemplate(String inStr) {
		
		int bodyStart = inStr.indexOf("<body>");
		int bodyEnd = inStr.indexOf("</body>");
		
		if (bodyStart > 0 && bodyEnd > 0) {
			String bodyStr = inStr.substring(bodyStart, bodyEnd + 7);
			return removeSpecialCharacter(bodyStr.replace("<body>", "<xsl:template match=\"/\">").replace("</body>", "</xsl:template>").trim());
		} else {
			String searchStartStr = "http://www.w3.org/1999/XSL/Transform\">";
			int stylesheetStart = inStr.indexOf(searchStartStr);
			int stylesheetEnd = inStr.indexOf("</xsl:stylesheet>");
			String bodyStr = inStr.substring(stylesheetStart + searchStartStr.length(), stylesheetEnd);
			return removeSpecialCharacter(bodyStr.trim());
		}
	}

	public static String convertToJson2(String jsonName, String htmlStrs[], String cssStr, String[] addressStr) {
		String result = "{\n";
		
		result += "\t\"style\": \"";
		if (cssStr.length() > 0) {
			result += removeSpecialCharacter(cssStr);
		}
		result += "\",\n";
		
		result += "\t\"pdfCode\": \"" + jsonName + "\",\n";
		
		result += "\t\"header\": {\n";
		result += "\t\t\"en\": \"\",\n";
		result += "\t\t\"zh-Hant\": \"\"\n";
		result += "\t},\n";
		
		
		result += "\t\"footer\": {\n";
		result += "\t\t\"en\": \"";
		if (jsonName.contains("main") && !jsonName.contains("payment")) {
			result += "<div class=\\\"address\\\">";
			
			for (int i = 0; i < addressStr.length; i ++) {
				result += "<p class=\\\"address\\\"><span class=\\\"address\\\">" + addressStr[i] + "</span></p>";
			}
//			result += "<p class=\\\"address\\\"><span class=\\\"address\\\">AXA Insurance Pte Ltd (Company Reg. No. 199903512M)</span></p>";
//			result += "<p class=\\\"address\\\"><span class=\\\"address\\\">8 Shenton Way #24-01 AXA Tower Singapore 068811</span></p>";
//			result += "<p class=\\\"address\\\"><span class=\\\"address\\\">AXA Customer Centre #B1-01 Tel: 1800 880 4888 Fax: 6880 5501</span></p>";
//			result += "<p class=\\\"address\\\"><span class=\\\"address\\\">www.axa.com.sg</span></p>";
			result += "</div>";
		} 
		result += "<div class=\\\"pageNum\\\">";
		result += "<p class=\\\"pageNum\\\">";
		result += "<span class=\\\"pageNum\\\">{{page}}</span>";
		result += "<span class=\\\"pageDesc\\\"> of </span>";
		result += "<span class=\\\"pageNum\\\">{{pages}}</span>";
		result += "</p>";
		result += "</div>";
				
		result += "\",";
		
	
		result += "\"zh-Hant\": \"\"\n";
		result += "\t},\n";
		
		result += "\t\"template\": {\n";
		result += "\t\t\"en\": [\n";
		result += "\t\t\t";
		for (int i = 0; i < htmlStrs.length; i ++) {
			result += "\"";
			result += htmlStrs[i];
			result += "\"";
			if (i != htmlStrs.length - 1) {
				result += ",";	
			}
			result += "\n";
		}
		
		result += "\t\t],\n";
		result += "\t\t\"zh-Hant\":[]\n";
		result += "\t}\n";
		result += "}";
		
		return result;
	}

	
	public static void main(String[] args) {
		
		String strHtml = "";
		String strCss = "";
		
		// TODO Auto-generated method stub
		String basePath = "";
		String outputPath = "";
		if (args.length > 0) {
    		basePath = args[0];
    	} else {
    		basePath = System.getProperty("user.dir") + "/../../";
    	}
		
		if (args.length > 1) {
			outputPath = args[1];
		} else {
			outputPath = basePath + "/json";
		}
		
		File currfolder = new File(basePath);
		File[] listOfFiles = currfolder.listFiles();
		
		HashMap<String, String[]> rptMap = new HashMap<String, String[]>();
		HashMap<String, String> cssMap = new HashMap<String, String>();
		
		System.out.println("basePath: " + currfolder.getAbsolutePath());
		
		for (int i = 0; i < listOfFiles.length; i ++) {
			File currFile = listOfFiles[i];
			if (currFile.isFile()) {
				String fileName = currFile.getName();
				
				if (fileName.endsWith(".xsl")) {
					rptMap.put(fileName.replace(".xsl", ""), new String [] {fileName});
					System.out.println("init setting for file: " + fileName);
				} else if (currFile.getName().endsWith("main.html")) {
					rptMap.put(fileName.replace(".html", ""), new String [] {fileName});
					cssMap.put(fileName.replace(".html", ""), "style.css");
					System.out.println("init setting for file: " + fileName);
				}
			}
		}
		
		FileInputStream fis;
		InputStreamReader reader;
		try {
			int length = -1; 
			char[] cs = new char[1024]; 
			long time1 = System.currentTimeMillis();
			long time2 = 0;
			
			Set<Map.Entry<String, String[]>> set = rptMap.entrySet();
			
			String strAddress = "";
			//read address
			fis = new FileInputStream(basePath+"/address.txt");
			reader = new InputStreamReader(fis,"UTF-8");
			while ((length = reader.read(cs)) != -1) {
				strAddress += new String(cs,0,length);
			}	
			reader.close();
			fis.close();
			
			String osLineSep = System.getProperty("line.separator");
			String usedLineSep = strAddress.contains(osLineSep) ? osLineSep : "\n";
			String[] strAddresses = strAddress.split(usedLineSep);
			
			for(Map.Entry<String, String[]> entry : set) {
				String key = entry.getKey();
				String[] filesList = entry.getValue();
				
				String cssFileName = (String)cssMap.get(key);
				
				strCss = "";
				//read Css
				if (cssFileName != null && !cssFileName.equals("")) {
					fis = new FileInputStream(basePath+"/"+(String)cssMap.get(key));
					reader = new InputStreamReader(fis,"UTF-8");
					while ((length = reader.read(cs)) != -1) {
						strCss += new String(cs,0,length);
					}	
					reader.close();
					fis.close();
				}
				
				ArrayList<String> templateStr = new ArrayList<String>();
				
				for (int i = 0; i < filesList.length; i ++) {
		
					strHtml = "";
					//Read html file
					fis = new FileInputStream(basePath+"/"+filesList[i]);
					reader = new InputStreamReader(fis,"UTF-8");
					while ((length = reader.read(cs)) != -1) {
						strHtml += new String(cs,0,length);
					}	
					reader.close();
					fis.close();
					
					time2 = System.currentTimeMillis();
					System.out.println("time="+(time2-time1)+"ms");
					
					
					templateStr.add(extractTemplate(strHtml));
				}
				
				//convert file
				String convertStr = convertToJson2(key, templateStr.toArray(new String[0]), strCss, strAddresses);
				
				
				//Write file
				File file = new File(outputPath + "/" + key +".json");
				
				FileOutputStream fop = new FileOutputStream(file);
				if (!file.exists()) {
					file.createNewFile();
				}
				byte[] contentInBytes = convertStr.getBytes();

				fop.write(contentInBytes);
				fop.flush();
				fop.close();		
			}

			System.out.println("Done");
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
