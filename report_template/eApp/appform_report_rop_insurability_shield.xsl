<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template name="appform_shield_insurance_info">
  <xsl:param  name="node"/>
  <xsl:param  name="orgNode"/>
  <xsl:param  name="isProposer"/>

  <xsl:variable name="clientName">
    <xsl:value-of select="$node/personalInfo/fullName"/>
  </xsl:variable>

  <xsl:variable name="sectionHeaderCol02">
    <xsl:choose>
      <xsl:when test="$isProposer = 'true'">
        <xsl:value-of select="'Proposer'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'Life Assured'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!--INSURANCE INFO -->
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">INSURANCE INFO</span>
    </p>

    <table class="dataGroup">
      <tr>
        <xsl:call-template  name  ="comSecHeaderCols02WideCol01">
          <xsl:with-param   name  ="sectionHeader"
                            select="'Replacement of Policy'"/>
          <xsl:with-param   name  ="sectionSubHeader">
            <xsl:choose>
              <xsl:when test="$isProposer = 'true'"></xsl:when>
              <xsl:otherwise><xsl:value-of select="concat(' (', $clientName, ')')"/></xsl:otherwise>
            </xsl:choose>
          </xsl:with-param>
          <xsl:with-param   name  ="col02Text"
                            select="$sectionHeaderCol02"/>
        </xsl:call-template>
      </tr>

      <xsl:variable   name="numReplacementOfPolicy">
        <xsl:value-of select ="count($node/policies/ROP01_DATA)"/>
      </xsl:variable>

      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'1. Is this proposal to replace or intended
                            to replace an existing Integrated Shield Plan?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/policies/ROP_01"/>
        </xsl:call-template>
      </tr>

      <xsl:if test="$node/policies/ROP_01 = 'Yes'">
        <tr>
          <td  colspan = "2" style = "padding: 5px 8px;">
            <table class = "simple centeredData">
              <colgroup>
                <col style="width:50%"/>
                <col style="width:50%"/>
              </colgroup>

              <tr>
                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Name of Company</span>
                  </p>
                </td>
                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Plan Name</span>
                  </p>
                </td>
              </tr>

              <xsl:for-each select="$node/policies/ROP01_DATA">
                <tr>
                  <td>
                    <p class="userData">
                      <span lang="EN-HK" class="tdAns">
                        <xsl:value-of select="./nameOfCompany"/>
                      </span>
                    </p>
                  </td>
                  <td>
                    <p class="userData">
                      <span lang="EN-HK" class="tdAns">
                        <xsl:choose>
                          <xsl:when test="string-length(./planName) > 0"><xsl:value-of select="./planName"/></xsl:when>
                          <xsl:otherwise>-</xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                </tr>
              </xsl:for-each>
            </table>

          </td>
        </tr>
      </xsl:if>

      <tr>
        <td colspan = "2" style="padding-left: 7.2pt;">
          <p class="normal">
            <br/>
            <span lang="EN-HK" class="warningPoliciesBold">If yes, your Financial
            Consultant is required to explain the following to you. Please tick and
            confirm the below declaration:</span>
          </p>
        </td>
      </tr>

      <tr>
        <td colspan = "2" style="padding-left: 7.2pt;">
          <p class="normal">
            <br/>

            <xsl:variable name="chkValue_1">
              <xsl:choose>
                <xsl:when test="$node/policies/ROP_DECLARATION_01 = 'Yes'">
                  <xsl:value-of select="'1'"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="'0'"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:call-template  name  ="comCheckbox">
              <xsl:with-param   name  ="isChecked"
                                select="$chkValue_1"/>
            </xsl:call-template>

            <span lang="EN-HK" class="warningPolicies">I confirm that my
            Financial Consultant has explained to my satisfaction the
            implications associated with this switch/replacement and, based on
            his/her recommendation, I agree to proceed with the switch/replacement
            of my existing integrated Shield Plan. I am aware that each Life
            Assured can only have one integrated Shield Plan. Once this policy
            commences, the existing integrated Shield Plan covering the
            Life Assured will be automatically terminated.
            </span>
          </p>
        </td>
      </tr>

      <tr>
        <td colspan = "2" style="padding-left: 7.2pt;">
          <p class="normal">
            <br/>

            <xsl:variable name="chkValue_2">
              <xsl:choose>
                <xsl:when test="$node/policies/ROP_DECLARATION_02 = 'Yes'">
                  <xsl:value-of select="'1'"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="'0'"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:call-template  name  ="comCheckbox">
              <xsl:with-param   name  ="isChecked"
                                select="$chkValue_2"/>
            </xsl:call-template>

            <span lang="EN-HK" class="warningPolicies">My Financial Consultant
            has explained to me the implications associated with this
            switch/replacement. I am aware the implications that may arise from
            a switch/replacement could outweigh any potential benefit such as:
            </span>

            <br/>

            <span lang="EN-HK" class="warningPolicies">- The new policy may
            offer a lower level of benefit at a higher cost or same cost, or
            offer the same level of benefit at higher cost and, the new policy
            may be less suitable for me.
            </span>

            <br/>
            <span lang="EN-HK" class="warningPolicies">- If I am switching to
            this plan and I have existing medical conditions that are currently
            covered by my existing plan, I am aware that I may lose coverage
            for those conditions.
            </span>

            <br/>
            <span lang="EN-HK" class="warningPolicies">- If I am replacing my
            existing plan by upgrading to this plan and I have existing medical
            conditions that are currently covered by my existing plan, I am
            aware that I may not be given the enhanced benefits for those conditions.
            </span>
          </p>
        </td>
      </tr>
    </table>
  </div>

  <!--Insurance History -->
  <div class="section">
    <table class="dataGroup first last">
      <tr>
        <xsl:call-template  name  ="comSecHeaderCols02WideCol01">
          <xsl:with-param   name  ="sectionHeader"
                            select="'Insurance History'"/>
          <xsl:with-param   name  ="sectionSubHeader">
            <xsl:choose>
              <xsl:when test="$isProposer = 'true'"></xsl:when>
              <xsl:otherwise><xsl:value-of select="concat(' (', $clientName, ')')"/></xsl:otherwise>
            </xsl:choose>
          </xsl:with-param>
          <xsl:with-param   name  ="col02Text"
                            select="$sectionHeaderCol02"/>
        </xsl:call-template>
      </tr>

      <xsl:variable   name="numInsuranceHistory_1">
        <xsl:value-of select ="count($node/insurability/INS03_DATA)"/>
      </xsl:variable>

      <tr>
        <xsl:variable name="strYesNoInsuranceHistory_01">
          <xsl:choose>
            <xsl:when test="$numInsuranceHistory_1 > 0">
              <xsl:value-of select="'Yes'"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="'No'"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'1. Have you had an application, reinstatement
                            or renewal of a life or critical illness or disability,
                            or accident or hospital insurance policy been
                            postponed, declined, accepted at special rates or is
                            still being considered with AXA Insurance Pte Ltd or
                            any other insurer?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$strYesNoInsuranceHistory_01"/>
        </xsl:call-template>
      </tr>

      <xsl:choose>
        <xsl:when test="$numInsuranceHistory_1 > 0">
          <tr>
            <td  colspan = "2" style = "padding: 5px 8px;">

              <table class = "simple centeredData">
                <colgroup>
                  <col style="width:17%"/>
                  <col style="width:17%"/>
                  <col style="width:17%"/>
                  <col style="width:17%"/>
                  <col style="width:32%"/>
                </colgroup>

                <tr>
                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Name of the Insurance Company</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Type of Coverage</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Date incurred (mm/yyyy)</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Medical Condition</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Decision and Detailed Reason(s) of special terms</span>
                    </p>
                  </td>
                </tr>

                <xsl:for-each select="$node/insurability/INS03_DATA">
                  <tr>
                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS03a"/>
                        </span>
                      </p>
                    </td>

                    <td>
                      <xsl:if test="./INS03b1 = 'Y'">
                        <div style = "text-align:left">
                          <p class="normal">
                            <span>
                              <xsl:call-template  name  ="comCheckbox">
                                <xsl:with-param   name  ="isChecked"
                                                  select="'1'"/>
                              </xsl:call-template>
                            </span>
                            <span lang="EN-HK" class="tdAns">Life</span>
                          </p>
                        </div>
                      </xsl:if>

                      <xsl:if test="./INS03b2 = 'Y'">
                        <div style = "text-align:left">
                          <p class="normal">
                            <span>
                              <xsl:call-template  name  ="comCheckbox">
                                <xsl:with-param   name  ="isChecked"
                                                  select="'1'"/>
                              </xsl:call-template>
                            </span>
                            <span lang="EN-HK" class="tdAns">Total Permanent Disability</span>
                          </p>
                        </div>
                      </xsl:if>

                      <xsl:if test="./INS03b3 = 'Y'">
                        <div style = "text-align:left">
                          <p class="normal">
                            <span>
                              <xsl:call-template  name  ="comCheckbox">
                                <xsl:with-param   name  ="isChecked"
                                                  select="'1'"/>
                              </xsl:call-template>
                            </span>
                            <span lang="EN-HK" class="tdAns">Critical Illness</span>
                          </p>
                        </div>
                      </xsl:if>

                      <xsl:if test="./INS03b4 = 'Y'">
                        <div style = "text-align:left">
                          <p class="normal">
                            <span>
                              <xsl:call-template  name  ="comCheckbox">
                                <xsl:with-param   name  ="isChecked"
                                                  select="'1'"/>
                              </xsl:call-template>
                            </span>
                            <span lang="EN-HK" class="tdAns">Accident</span>
                          </p>
                        </div>
                      </xsl:if>

                      <xsl:if test="./INS03b5 = 'Y'">
                        <div style = "text-align:left">
                          <p class="normal">
                            <span>
                              <xsl:call-template  name  ="comCheckbox">
                                <xsl:with-param   name  ="isChecked"
                                                  select="'1'"/>
                              </xsl:call-template>
                            </span>
                            <span lang="EN-HK" class="tdAns">Hospitalisation and Surgical  Benefit</span>
                          </p>
                        </div>
                      </xsl:if>
                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS03c"/>
                        </span>
                      </p>
                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS03d"/>
                        </span>
                      </p>
                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS03e"/>
                        </span>
                      </p>
                    </td>

                  </tr>
                </xsl:for-each>
              </table>
            </td>

          </tr>
        </xsl:when>
      </xsl:choose>

      <xsl:variable   name="numInsuranceHistory_2">
        <xsl:value-of select ="count($node/insurability/INS04_DATA)"/>
      </xsl:variable>

      <tr class = "even">
        <xsl:variable name="strYesNoInsuranceHistory_02">
          <xsl:choose>
            <xsl:when test="$numInsuranceHistory_2 > 0">
              <xsl:value-of select="'Yes'"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="'No'"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'2. Have you made or going to make any claims,
                            including hospitalization claims on any policy with
                            AXA Insurance Pte Ltd or any other insurer?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$strYesNoInsuranceHistory_02"/>
        </xsl:call-template>
      </tr>

      <xsl:choose>
        <xsl:when test="$numInsuranceHistory_2 > 0">
          <tr>
            <td  colspan = "2" style = "padding: 5px 8px;">

              <table class = "simple centeredData">
                <colgroup>
                  <col style="width:17%"/>
                  <col style="width:17%"/>
                  <col style="width:17%"/>
                  <col style="width:17%"/>
                  <col style="width:32%"/>
                </colgroup>

                <tr>
                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Name of the Insurance Company</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Type of the Claim</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Date incurred (mm/yyyy)</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Medical Condition</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Claim Details</span>
                    </p>
                  </td>
                </tr>

                <xsl:for-each select="$node/insurability/INS04_DATA">
                  <tr>
                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS04a"/>
                        </span>
                      </p>
                    </td>

                    <td>
                      <xsl:if test="./INS04b1 = 'Y'">
                        <div style = "text-align:left">
                          <p class="normal">
                            <span>
                              <xsl:call-template  name  ="comCheckbox">
                                <xsl:with-param   name  ="isChecked"
                                                  select="'1'"/>
                              </xsl:call-template>
                            </span>
                            <span lang="EN-HK" class="tdAns">Total Permanent Disability</span>
                          </p>
                        </div>
                      </xsl:if>

                      <xsl:if test="./INS04b2 = 'Y'">
                        <div style = "text-align:left">
                          <p class="normal">
                            <span>
                              <xsl:call-template  name  ="comCheckbox">
                                <xsl:with-param   name  ="isChecked"
                                                  select="'1'"/>
                              </xsl:call-template>
                            </span>
                            <span lang="EN-HK" class="tdAns">Critical Illness</span>
                          </p>
                        </div>
                      </xsl:if>

                      <xsl:if test="./INS04b3 = 'Y'">
                        <div style = "text-align:left">
                          <p class="normal">
                            <span>
                              <xsl:call-template  name  ="comCheckbox">
                                <xsl:with-param   name  ="isChecked"
                                                  select="'1'"/>
                              </xsl:call-template>
                            </span>
                            <span lang="EN-HK" class="tdAns">Accident</span>
                          </p>
                        </div>
                      </xsl:if>

                      <xsl:if test="./INS04b4 = 'Y'">
                        <div style = "text-align:left">
                          <p class="normal">
                            <span>
                              <xsl:call-template  name  ="comCheckbox">
                                <xsl:with-param   name  ="isChecked"
                                                  select="'1'"/>
                              </xsl:call-template>
                            </span>
                            <span lang="EN-HK" class="tdAns">Hospitalisation and Surgical Benefit</span>
                          </p>
                        </div>
                      </xsl:if>

                      <xsl:if test="./other = 'Y'">
                        <div style = "text-align:left">
                          <p class="normal">
                            <span>
                              <xsl:call-template  name  ="comCheckbox">
                                <xsl:with-param   name  ="isChecked"
                                                  select="'1'"/>
                              </xsl:call-template>
                            </span>
                            <span lang="EN-HK" class="tdAns"><xsl:value-of select="./resultOther"/></span>
                          </p>
                        </div>
                      </xsl:if>

                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS04c"/>
                        </span>
                      </p>
                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS04d"/>
                        </span>
                      </p>
                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS04e"/>
                        </span>
                      </p>
                    </td>

                  </tr>
                </xsl:for-each>


              </table>
            </td>

          </tr>
        </xsl:when>
      </xsl:choose>

      <xsl:variable   name="numInsuranceHistory_3">
        <xsl:value-of select ="count($node/insurability/INS05_DATA)"/>
      </xsl:variable>

      <tr>
        <xsl:variable name="strYesNoInsuranceHistory_03">
          <xsl:choose>
            <xsl:when test="$numInsuranceHistory_3 > 0">
              <xsl:value-of select="'Yes'"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="'No'"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'3. Have you ever been informed by Ministry
                            of Health that an extra premium of 30% has been
                            imposed on your Medishield life insurance?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$strYesNoInsuranceHistory_03"/>
        </xsl:call-template>
      </tr>

      <xsl:choose>
        <xsl:when test="$numInsuranceHistory_3 > 0">
          <tr>
            <td  colspan = "2" style = "padding: 5px 8px;">

              <table class = "simple centeredData">
                <colgroup>
                  <col style="width:34%"/>
                  <col style="width:17%"/>
                  <col style="width:33%"/>
                  <col style="width:16%"/>
                </colgroup>

                <tr>
                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Chronic/ Pre-existing Medical Condition</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Year Diagnosed</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Current/ ongoing Treatment</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Any existing Medical Reports?</span>
                    </p>
                  </td>
                </tr>

                <xsl:for-each select="$node/insurability/INS05_DATA">
                  <tr>
                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS05a"/>
                        </span>
                      </p>
                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS05b"/>
                        </span>
                      </p>
                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS05c"/>
                        </span>
                      </p>
                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="./INS05d"/>
                        </span>
                      </p>
                    </td>

                  </tr>
                </xsl:for-each>


              </table>
            </td>

          </tr>
        </xsl:when>
      </xsl:choose>


    </table>
    <p><br/></p>
  </div>



</xsl:template>


<xsl:template name="medical_questionaire_questions_1">
  <xsl:param  name="dataTable"/>

  <xsl:variable name="qn">
    <xsl:choose>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_01_DATA'">1a</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_02_DATA'">1b</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_03_DATA'">1c</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_04_DATA'">1d</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_05_DATA'">1e</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_06_DATA'">1f</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_07_DATA'">1g</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_08_DATA'">1h</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_09_DATA'">1i</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_10_DATA'">1j</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_11_DATA'">1k</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_12_DATA'">1l</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_13_DATA'">1m</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_14_DATA'">1n</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_15_DATA'">1o</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_16_DATA'">1p</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_17_DATA'">1q</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_18_DATA'">2</xsl:when>
      <xsl:otherwise>-</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:value-of select="$qn" />
</xsl:template>



<xsl:template name="medical_questionaire_questions_4_female">
  <xsl:param  name="dataTable"/>

  <xsl:variable name="qn">
    <xsl:choose>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_20_DATA'">4b</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_21_DATA'">4c</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_22_DATA'">4d</xsl:when>
      <xsl:when test="$dataTable = 'HEALTH_SHIELD_23_DATA'">4e</xsl:when>
      <xsl:otherwise>-</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:value-of select="$qn" />
</xsl:template>



<xsl:template name="appform_shield_medical_questionaire">
  <xsl:param  name="node"/>
  <xsl:param  name="orgNode"/>
  <xsl:param  name="isProposer"/>

  <xsl:variable name = "lifeAssuredName">
    <xsl:value-of select="$node/personalInfo/fullName"/>
  </xsl:variable>

  <xsl:variable name="sectionHeaderCol02">
    <xsl:choose>
      <xsl:when test="$isProposer = 'true'">
        <xsl:value-of select="'Proposer'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'Life Assured'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!--Lifestyle and Habits -->
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">MEDICAL QUESTIONNAIRE</span>
    </p>

    <table class="dataGroup first last">
      <tr>
        <xsl:call-template  name  ="comSecHeaderCols02WideCol01">
          <xsl:with-param   name  ="sectionHeader"
                            select="'Lifestyle and Habits'"/>
          <xsl:with-param   name  ="sectionSubHeader">
            <xsl:choose>
              <xsl:when test="$isProposer = 'true'"></xsl:when>
              <xsl:otherwise><xsl:value-of select="concat(' (', $lifeAssuredName, ')')"/></xsl:otherwise>
            </xsl:choose>
          </xsl:with-param>
          <xsl:with-param   name  ="col02Text"
                            select="$sectionHeaderCol02"/>
        </xsl:call-template>
      </tr>

      <!--Qn1-->
      <tr>

        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'1. Have you smoked or used any tobacco,
                            nicotine or smokeless tobacco products (e.g. cigarettes, cigar, e-cigarettes, pipes, nicotine patch, etc.) within
                            the past 12 months?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/LIFESTYLE01"/>
        </xsl:call-template>
      </tr>

      <xsl:if test= "$node/insurability/LIFESTYLE01 = 'Yes'">
        <tr>
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'i) Type of product (e.g. cigarettes, cigar, e-cigarettes, pipes, nicotine patch, etc.):'"/>
            <xsl:with-param   name  ="col02Text" >
              <xsl:choose>
                <xsl:when test="$node/insurability/LIFESTYLE01a = 'Other'">
                  <xsl:value-of select="$node/insurability/LIFESTYLE01a_OTH"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$node/insurability/LIFESTYLE01a"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:with-param>
            <xsl:with-param   name  ="col01Align"
                              select="'2'"/>
          </xsl:call-template>
        </tr>

        <tr>
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'ii) Number of products smoked per day (i.e. how many sticks/ pipes/ patches per day?):'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/LIFESTYLE01b"/>
            <xsl:with-param   name  ="col01Align"
                              select="'2'"/>
          </xsl:call-template>
        </tr>

        <tr>
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'iii) Number of years smoked:'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/LIFESTYLE01c"/>
            <xsl:with-param   name  ="col01Align"
                              select="'2'"/>
          </xsl:call-template>
        </tr>
      </xsl:if>

      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'2. Do you consume alcohol? If yes, how much alcohol do you drink per week on average?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/LIFESTYLE02"/>

        </xsl:call-template>
      </tr>

      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'Note: ONE standard unit of alcoholic drink
                            equates to Beer 330ml/can, Wine 125ml/glass, or
                            Spirits 30ml/cup.'"/>
          <xsl:with-param   name  ="col02Text"
                            select=""
                            />
          <xsl:with-param   name  ="isHideCol02Text"
                            select="'true'"/>
        </xsl:call-template>
      </tr>

      <xsl:if test= "$node/insurability/LIFESTYLE02 = 'Yes'">

        <xsl:if test="$node/insurability/LIFESTYLE02a_1 = 'Yes'">
          <tr>
            <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
              <xsl:with-param   name  ="col01Text"
                                select="'Beer 330ml/can'"/>
              <xsl:with-param   name  ="col02Text"
                                select="concat($node/insurability/LIFESTYLE02a_2, ' can(s)')"/>
              <xsl:with-param   name  ="col01PaddingLeft"
                                select="'490px'"/>
              <xsl:with-param   name  ="col01WithCheckbox"
                                select="'1'"/>
            </xsl:call-template>
          </tr>
        </xsl:if>

        <xsl:if test="$node/insurability/LIFESTYLE02b_1 = 'Yes'">
          <tr>
            <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
              <xsl:with-param   name  ="col01Text"
                                select="'Wine 125 ml/glass'"/>
              <xsl:with-param   name  ="col02Text"
                                select="concat($node/insurability/LIFESTYLE02b_2, ' glass(es)')"/>
              <xsl:with-param   name  ="col01PaddingLeft"
                                select="'490px'"/>
              <xsl:with-param   name  ="col01WithCheckbox"
                                select="'1'"/>
            </xsl:call-template>
          </tr>
        </xsl:if>

        <xsl:if test="$node/insurability/LIFESTYLE02c_1 = 'Yes'">
          <tr>
            <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
              <xsl:with-param   name  ="col01Text"
                                select="'Spirits 30 ml/cup'"/>
              <xsl:with-param   name  ="col02Text"
                                select="concat($node/insurability/LIFESTYLE02c_2, ' cup(s)')"/>
              <xsl:with-param   name  ="col01PaddingLeft"
                                select="'490px'"/>
              <xsl:with-param   name  ="col01WithCheckbox"
                                select="'1'"/>
            </xsl:call-template>
          </tr>
        </xsl:if>
      </xsl:if>
    </table>
  </div>

  <!--Height and Weight  -->
  <div class="section">
    <table class="dataGroup first last">
      <tr>
        <xsl:call-template  name  ="comSecHeaderCols02WideCol01">
          <xsl:with-param   name  ="sectionHeader"
                            select="'Height and Weight'"/>
          <xsl:with-param   name  ="sectionSubHeader">
            <xsl:choose>
              <xsl:when test="$isProposer = 'true'"></xsl:when>
              <xsl:otherwise><xsl:value-of select="concat(' (', $lifeAssuredName, ')')"/></xsl:otherwise>
            </xsl:choose>
          </xsl:with-param>
          <xsl:with-param   name  ="col02Text"
                            select="$sectionHeaderCol02"/>
        </xsl:call-template>
      </tr>

      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'Height (m)'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HW01"/>
        </xsl:call-template>
      </tr>

      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'Weight (kg)'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HW02"/>
        </xsl:call-template>
      </tr>

      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'Any weight change in the last 12 months? (kg)'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HW03"/>
        </xsl:call-template>
      </tr>

      <xsl:if test="/root/reportData/showQuestions/insurability/question = 'HW03a'">
        <tr class = "even">
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'Weight change'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HW03a"/>
            <xsl:with-param   name  ="col01Align"
                              select="'2'"/>
          </xsl:call-template>
        </tr>
      </xsl:if>

      <xsl:if test="/root/reportData/showQuestions/insurability/question = 'HW03c'">
        <tr>
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'How many kg?'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HW03b"/>
            <xsl:with-param   name  ="col01Align"
                              select="'2'"/>
          </xsl:call-template>
        </tr>
      </xsl:if>

      <xsl:if test="/root/reportData/showQuestions/insurability/question = 'HW03c'">
        <tr class = "even">
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'Reason of weight change'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HW03c"/>
            <xsl:with-param   name  ="col01Align"
                              select="'2'"/>
          </xsl:call-template>
        </tr>
      </xsl:if>
    </table>
  </div>

  <!--Medical and Health Information (Life Assured’s name)-->
  <div class="section">
    <table class="dataGroup first">
      <tr>
        <xsl:call-template  name  ="comSecHeaderCols02WideCol01">
          <xsl:with-param   name  ="sectionHeader"
                            select="'Medical and Health Information'"/>
          <xsl:with-param   name  ="sectionSubHeader">
            <xsl:choose>
              <xsl:when test="$isProposer = 'true'"></xsl:when>
              <xsl:otherwise><xsl:value-of select="concat(' (', $lifeAssuredName, ')')"/></xsl:otherwise>
            </xsl:choose>
          </xsl:with-param>
          <xsl:with-param   name  ="col02Text"
                            select="$sectionHeaderCol02"/>
        </xsl:call-template>
      </tr>

      <!--####Q1#### -->
      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'1. Have you ever had, or been told you had, or received treatment for:'"/>
          <xsl:with-param   name  ="col02Text"
                            select=""/>
          <xsl:with-param   name  ="isHideCol02Text"
                            select="'true'"/>
        </xsl:call-template>
      </tr>

      <!--Q1a -->
      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'a) Raised blood pressure, raised cholesterol,
                            chest pain or discomfort, heart attack, heart murmur,
                            prolapsed mitral valve or other heart valve disorders,
                            breathlessness, irregular or fast heart rate,
                            hole in the heart, disease or any other disorder
                            of the heart or blood vessels?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_01"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1b -->
      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'b) Epilepsy, fits, stroke, paralysis,
                            weakness of limbs, persistent headache, unconsciousness,
                            nervous breakdown, depression or any other nervous or
                            mental disorder?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_02"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1c -->
      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'c) Respiratory disorders (e.g. asthma,
                            bronchitis, pneumonia, pneumothorax or tuberculosis)?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_03"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1d -->
      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'d) Digestive disorder which include those
                            of the esophagus, colon and rectum (e.g. gastritis,
                            stomach or duodenal ulcer, blood in stool, hemorrhoids,
                            irritable bowel syndrome, fistula in ano)?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_04"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1e -->
      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'e) Spleen or liver or other hepatobiliary
                            system disorders which include liver problem,
                            hepatitis (including hepatitis B carrier), gallstone
                            or other gallbladder problems, inflammation of pancreas?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_05"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1f -->
      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'f) Eye, ear, nose or throat disorders (e.g.
                            cataracts, sinus problem or rhinitis)?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_06"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1g -->
      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'g) Urinary disorder (e.g. protein, blood or
                            sugar in urine, kidney stones, prolapsed urinary
                            bladder, prostate problem or urinary incontinence)?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_07"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1h -->
      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'h) Diabetes mellitus (Type 1, Type 2 or
                            gestational), gout, thyroid disorders or other
                            endocrine disorders?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_08"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1i -->
      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'i) Bone, spine, joint or muscle disorder
                            (e.g. scoliosis, slipped disc or arthritis)?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_09"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1j -->
      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'j) Cancer, or any abnormal growth or tumour
                            (e.g. cyst, lump, polyp or nodule) whether cancerous
                            or benign?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_10"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1k -->
      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'k) Blood disorder (e.g. anaemia, haemophilia,
                            thalassaemia or systemic lupus erythematosus)?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_11"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1l -->
      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'l) Physical impairments or problems, or
                            congenital or hereditary disorders (e.g. speech
                            impairment, autism or attention deficit hyperactivity
                            disorder)?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_12"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1m -->
      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'m) HIV infection or sexually transmitted
                            diseases?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_13"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1n -->
      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'n) Any skin disorders (e.g. eczema,
                            dermatitis etc)?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_14"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1o -->
      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'o) Any illness, disorder, abnormalities or
                            recurrence symptoms, which are not mentioned above?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_15"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1p -->
      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'p) In the past 12 months, have you
                            experienced any symptoms for more than 2 weeks (e.g.
                            feeling giddy, breathless, abnormal growth/enlargement,
                            persistent fever, diarrhea, bodily discomfort or pain)
                            or recurring symptoms?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_16"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--Q1q -->
      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'q) Any injuries/diseases/ illness /symptoms
                            that are recurrent or have continued for more than
                            one month, which are not mentioned above?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_17"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <!--#####Q2#### -->
      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'2. Have you ever had been told that you
                            snore loudly in your sleep,and/ or stop breathing
                            momentarily, or have choking episode, or experience
                            sleepiness during your waking hours, treated for or
                            been told to get treatment for  any sleep disorders
                            such as sleep apnea, snoring, or have undergone any
                            sleep study test?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_18"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <div class="section">
    <table class="dataGroup mid">
      <xsl:variable   name="numAnsQuestion2">
        <xsl:value-of select ="count($node/insurability/HEALTH_GROUP_DATA)"/>
      </xsl:variable>

      <!--Q2 Table -->
      <xsl:if test="$numAnsQuestion2 > 0">
        <tr>
          <td  colspan = "2" style = "padding: 5px 8px;">

            <table class = "simple centeredData">
              <colgroup>
                <col style="width:7%"/>
                <col style="width:18%"/>
                <col style="width:8%"/>
                <col style="width:7%"/>
                <col style="width:24%"/>
                <col style="width:12%"/>
                <col style="width:24%"/>
              </colgroup>

              <tr>
                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Qn</span>
                  </p>
                </td>

                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Medical Condition/ Diagnosis</span>
                  </p>
                </td>

                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Year of diagnosis</span>
                  </p>
                </td>

                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Medical Test Report</span>
                  </p>
                </td>

                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Medication</span>
                  </p>
                </td>

                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Next Follow-up visit date</span>
                  </p>
                </td>

                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Current Complication</span>
                  </p>
                </td>
              </tr>


              <xsl:for-each select="$node/insurability/HEALTH_GROUP_DATA">
                <tr>
                  <xsl:variable name="dataTable" select="./DATATABLE"/>

                  <td>
                    <p class="userData">
                      <span lang="EN-HK" class="tdAns">
                        <xsl:call-template name  ="medical_questionaire_questions_1">
                        <xsl:with-param   name  ="dataTable"
                                          select="$dataTable"/>
                      </xsl:call-template>
                      </span>
                    </p>
                  </td>

                  <td>
                    <p class="userData">
                      <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_GROUPa"/></span>
                    </p>
                  </td>

                  <td>
                    <p class="userData" style = "text-align:left;">
                      <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_GROUPb"/></span>
                    </p>
                  </td>

                  <td>
                    <p class="userData" style = "text-align:left;">
                      <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_GROUPc"/></span>
                    </p>
                  </td>

                  <td>
                    <p class="userData">
                      <span lang="EN-HK" class="tdAns">
                        <xsl:choose>
                          <xsl:when test="string-length(./HEALTH_GROUPd) = 0">-</xsl:when>
                          <xsl:otherwise><xsl:value-of select="./HEALTH_GROUPd"/></xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>

                  <td>
                    <p class="userData" style = "text-align:left;">
                      <span lang="EN-HK" class="tdAns">
                        <xsl:choose>
                          <xsl:when test="string-length(./HEALTH_GROUPe) = 0">-</xsl:when>
                          <xsl:otherwise><xsl:value-of select="./HEALTH_GROUPe"/></xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>

                  <td>
                    <p class="userData" style = "text-align:left;">
                      <span lang="EN-HK" class="tdAns">
                        <xsl:choose>
                          <xsl:when test="string-length(./HEALTH_GROUPf) = 0">-</xsl:when>
                          <xsl:otherwise><xsl:value-of select="./HEALTH_GROUPf"/></xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                </tr>
              </xsl:for-each>

            </table>
          </td>

        </tr>
      </xsl:if>
    </table>
  </div>

  <xsl:variable name="isFemale">
    <xsl:choose>
      <xsl:when test="/root/reportData/showQuestions/insurability/question = 'HEALTH13'">
        <xsl:value-of select="'Yes'"/>
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="'No'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="isJuvenile">
    <xsl:choose>
      <xsl:when test="/root/reportData/showQuestions/insurability/question = 'HEALTH15'">
        <xsl:value-of select="'Yes'"/>
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="'No'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <div class="section">
    <table>
      <xsl:attribute name="class">
        <xsl:choose>
          <xsl:when test="$isFemale = 'Yes' or $isJuvenile = 'Yes' or $node/insurability/insAllNoInd[. = 'Yes']"><xsl:value-of select="'dataGroup mid'"/></xsl:when>
          <xsl:otherwise><xsl:value-of select="'dataGroup last'"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>

      <!--#####Q3#### -->
      <tr class = "even">
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text"
                            select="'3. Have you undergone any sex reassignment surgery?'"/>
          <xsl:with-param   name  ="col02Text"
                            select="$node/insurability/HEALTH_SHIELD_19"/>
        </xsl:call-template>
      </tr>

      <xsl:variable   name="numAnsQuestion3">
        <xsl:value-of select ="count($node/insurability/HEALTH_SHIELD_19_DATA)"/>
      </xsl:variable>

      <!--Q3 Table -->
      <xsl:if test="$numAnsQuestion3 > 0">
        <tr>
          <td  colspan = "2" style = "padding: 5px 8px;">

            <table class = "simple centeredData">
              <colgroup>
                <col style="width:22%"/>
                <col style="width:8%"/>
                <col style="width:9%"/>
                <col style="width:25%"/>
                <col style="width:12%"/>
                <col style="width:24%"/>
              </colgroup>

              <tr>
                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Operation(s) undergone</span>
                  </p>
                </td>

                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Year of Surgery/ Operation</span>
                  </p>
                </td>

                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Discharge Summary Report</span>
                  </p>
                </td>

                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Current Medication/ Hormonal Therapy</span>
                  </p>
                </td>

                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Next Follow-up visit date</span>
                  </p>
                </td>

                <td>
                  <p class="normal">
                    <span lang="EN-HK" class="th">Current Complication/ Follow-up for other conditions due to SRS</span>
                  </p>
                </td>
              </tr>

              <xsl:for-each select="$node/insurability/HEALTH_SHIELD_19_DATA">
                <tr>
                  <td>
                    <p class="userData">
                      <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_SHIELD_19a"/></span>
                    </p>
                  </td>

                  <td>
                    <p class="userData" style = "text-align:left;">
                      <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_SHIELD_19b"/></span>
                    </p>
                  </td>

                  <td>
                    <p class="userData" style = "text-align:left;">
                      <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_SHIELD_19c"/></span>
                    </p>
                  </td>

                  <td>
                    <p class="userData">
                      <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_SHIELD_19d"/></span>
                    </p>
                  </td>

                  <td>
                    <p class="userData" style = "text-align:left;">
                      <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_SHIELD_19e"/></span>
                    </p>
                  </td>

                  <td>
                    <p class="userData" style = "text-align:left;">
                      <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_SHIELD_19f"/></span>
                    </p>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
          </td>

        </tr>
      </xsl:if>

    </table>
  </div>

  <div class="section">
    <table class="dataGroup last">

      <!--#####Q4 Female ####-->

      <xsl:if test="$isFemale = 'Yes'">
        <tr>
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'4. For Female Applicants only (For age 10 and above)'"/>
            <xsl:with-param   name  ="col01IsBold"
                              select="'true'"/>
            <xsl:with-param   name  ="isHideCol02Text"
                              select="'true'"/>
          </xsl:call-template>
        </tr>

        <!--Q4a Female -->
        <tr class = "even">
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'a) Are you currently pregnant?'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HEALTH13"/>
          </xsl:call-template>
        </tr>

        <xsl:if test="$node/insurability/HEALTH13 = 'Yes'">
          <!--Q4a Female gestation-->
          <tr>
            <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
              <xsl:with-param   name  ="col01Text"
                                select="'i) Number of gestational weeks: '"/>
              <xsl:with-param   name  ="col02Text"
                                select="$node/insurability/HEALTH13a"/>
              <xsl:with-param   name  ="col01Align"
                                select="'2'"/>
            </xsl:call-template>
          </tr>

          <!--Q4a Female pregnancy complications-->
          <tr>
            <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
              <xsl:with-param   name  ="col01Text"
                                select="'Have you been diagnosed with any pregnancy complications or pregnancy-related conditions? '"/>
              <xsl:with-param   name  ="col02Text"
                                select="$node/insurability/HEALTH13b"/>
            </xsl:call-template>
          </tr>

          <xsl:variable   name="numAnsQuestion4aFemale">
            <xsl:value-of select ="count($node/insurability/HEALTH13b_DATA)"/>
          </xsl:variable>

          <!--Q4a Female pregnancy complications Table-->
          <xsl:if test="$numAnsQuestion4aFemale > 0">
            <tr>
              <td  colspan = "2" style = "padding: 5px 8px;">

                <table class = "simple centeredData">
                  <colgroup>
                    <col style="width:22%"/>
                    <col style="width:8%"/>
                    <col style="width:9%"/>
                    <col style="width:25%"/>
                    <col style="width:12%"/>
                    <col style="width:24%"/>
                  </colgroup>

                  <tr>
                    <td>
                      <p class="normal">
                        <span lang="EN-HK" class="th">Medical Condition/ Diagnosis</span>
                      </p>
                    </td>

                    <td>
                      <p class="normal">
                        <span lang="EN-HK" class="th">Year of diagnosis</span>
                      </p>
                    </td>

                    <td>
                      <p class="normal">
                        <span lang="EN-HK" class="th">Medical Test Report</span>
                      </p>
                    </td>

                    <td>
                      <p class="normal">
                        <span lang="EN-HK" class="th">Medication</span>
                      </p>
                    </td>

                    <td>
                      <p class="normal">
                        <span lang="EN-HK" class="th">Next Follow-up visit date</span>
                      </p>
                    </td>

                    <td>
                      <p class="normal">
                        <span lang="EN-HK" class="th">Current Complication</span>
                      </p>
                    </td>
                  </tr>

                  <xsl:for-each select="$node/insurability/HEALTH13b_DATA">
                    <tr>
                      <td>
                        <p class="userData">
                          <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH13b1"/></span>
                        </p>
                      </td>

                      <td>
                        <p class="userData" style = "text-align:left;">
                          <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH13b2"/></span>
                        </p>
                      </td>

                      <td>
                        <p class="userData" style = "text-align:left;">
                          <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH13b3"/></span>
                        </p>
                      </td>

                      <td>
                        <p class="userData">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:choose>
                              <xsl:when test="string-length(./HEALTH13b4) = 0">-</xsl:when>
                              <xsl:otherwise><xsl:value-of select="./HEALTH13b4"/></xsl:otherwise>
                            </xsl:choose>
                          </span>
                        </p>
                      </td>

                      <td>
                        <p class="userData" style = "text-align:left;">
                          <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH13b5"/></span>
                        </p>
                      </td>

                      <td>
                        <p class="userData" style = "text-align:left;">
                          <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH13b6"/></span>
                        </p>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>

            </tr>
          </xsl:if>
        </xsl:if>

        <!--Q4b Female-->
        <tr>
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'b) Is there any incidence(s) of
                              pregnancy complications or pregnancy-related
                              conditions (e.g. gestational diabetes,
                              miscarriage or ectopic pregnancy) in your
                              prior pregnancy(ies)?'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HEALTH_SHIELD_20"/>
          </xsl:call-template>
        </tr>

        <!--Q4c Female-->
        <tr class = "even">
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'c) Have you had or received any
                              treatment for or plan to be treated for any
                              disease or disorder of the breast including
                              breast lump, cysts, fibroadenoma, fibrocystic
                              disease, nipple changes or discharge, mammary
                              dysplasia, Paget’s disease of the nipple or
                              breast, carcinoma in situ, cancer or growth?'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HEALTH_SHIELD_21"/>
          </xsl:call-template>
        </tr>

        <!--Q4d Female-->
        <tr>
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'d) Have you had or received any
                              treatment for or plan to be treated for any
                              disease or disorder of the cervix uteri,
                              uterus or ovaries including ovarian cysts,
                              fibroids or endometriosis, abnormal uterine
                              or vaginal bleeding, abnormal enlargement of
                              the abdomen, carcinoma in situ or cancer?'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HEALTH_SHIELD_22"/>
          </xsl:call-template>
        </tr>

        <!--Q4e Female-->
        <tr class = "even">
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'e) Have you had an abnormal mammogram,
                              PAP smear, pelvis ultrasound, breast ultrasound,
                              cone biopsy, colposcopy, or other gynecological
                              test; or have you ever been advised for
                              further follow-up on (or to repeat) any one
                              of these tests within a 6 month or 12 month period?'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HEALTH_SHIELD_23"/>
          </xsl:call-template>
        </tr>

        <xsl:variable   name="numAnsQuestion4eFemale">
          <xsl:value-of select ="count($node/insurability/HEALTH_GROUP_DATA2)"/>
        </xsl:variable>

        <!--Q4e Female Table-->
        <xsl:if test="$numAnsQuestion4eFemale > 0">
          <tr>
            <td  colspan = "2" style = "padding: 5px 8px;">

              <table class = "simple centeredData">
                <colgroup>
                  <col style="width:7%"/>
                  <col style="width:18%"/>
                  <col style="width:8%"/>
                  <col style="width:7%"/>
                  <col style="width:24%"/>
                  <col style="width:12%"/>
                  <col style="width:24%"/>
                </colgroup>

                <tr>
                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Qn</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Medical Condition/ Diagnosis</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Year of diagnosis</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Medical Test Report</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Medication</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Next Follow-up visit date</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Current Complication</span>
                    </p>
                  </td>
                </tr>


                <xsl:for-each select="$node/insurability/HEALTH_GROUP_DATA2">
                  <tr>
                    <xsl:variable name="dataTable" select="./DATATABLE"/>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:call-template name  ="medical_questionaire_questions_4_female">
                          <xsl:with-param   name  ="dataTable"
                                            select="$dataTable"/>
                        </xsl:call-template>
                        </span>
                      </p>
                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_GROUPa"/></span>
                      </p>
                    </td>

                    <td>
                      <p class="userData" style = "text-align:left;">
                        <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_GROUPb"/></span>
                      </p>
                    </td>

                    <td>
                      <p class="userData" style = "text-align:left;">
                        <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_GROUPc"/></span>
                      </p>
                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:choose>
                            <xsl:when test="string-length(./HEALTH_GROUPd) = 0">-</xsl:when>
                            <xsl:otherwise><xsl:value-of select="./HEALTH_GROUPd"/></xsl:otherwise>
                          </xsl:choose>
                        </span>
                      </p>
                    </td>

                    <td>
                      <p class="userData" style = "text-align:left;">
                        <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_GROUPe"/></span>
                      </p>
                    </td>

                    <td>
                      <p class="userData" style = "text-align:left;">
                        <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_GROUPf"/></span>
                      </p>
                    </td>
                  </tr>
                </xsl:for-each>

              </table>
            </td>

          </tr>
        </xsl:if>




      </xsl:if>




      <!--#####Q4 Juvenile ####-->
      <xsl:if test="$isJuvenile = 'Yes'">
        <tr class = "even">
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'4. For Juvenile Applicants only (For age up to 5)'"/>
            <xsl:with-param   name  ="col01IsBold"
                              select="'true'"/>
            <xsl:with-param   name  ="isHideCol02Text"
                              select="'true'"/>
          </xsl:call-template>
        </tr>

        <tr class = "even">
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'Note: Child Health Booklet is compulsary
                              for all child(ren) from 0 - 6 months old.'"/>
            <xsl:with-param   name  ="col01IsRed"
                              select="'true'"/>
            <xsl:with-param   name  ="isHideCol02Text"
                              select="'true'"/>
          </xsl:call-template>
        </tr>

        <!--Q4a Juvenile -->
        <tr>
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'a) Was the Life Assured’s birth weight
                              below 2.5kg?'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HEALTH15"/>
          </xsl:call-template>
        </tr>

        <xsl:if test="$node/insurability/HEALTH15 = 'Yes'">
          <tr>
            <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
              <xsl:with-param   name  ="col01Text"
                                select="'Birth Weight (kgs):'"/>
              <xsl:with-param   name  ="col02Text"
                                select="$node/insurability/HEALTH15a"/>
              <xsl:with-param   name  ="col01Align"
                                select="2"/>
            </xsl:call-template>
          </tr>
        </xsl:if>

        <!--Q4b Juvenile -->
        <tr class = "even">
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'b) Was the Life Assured born before 37
                              weeks of pregnancy?'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HEALTH14"/>
          </xsl:call-template>
        </tr>

        <xsl:if test="$node/insurability/HEALTH14 = 'Yes'">
          <tr>
            <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
              <xsl:with-param   name  ="col01Text"
                                select="'Gestational Week:'"/>
              <xsl:with-param   name  ="col02Text"
                                select="$node/insurability/HEALTH14a"/>
              <xsl:with-param   name  ="col01Align"
                                select="'2'"/>
            </xsl:call-template>
          </tr>
        </xsl:if>

        <!--Q4c Juvenile -->
        <tr>
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'c) Was the duration of hospital stay
                              after birth more than 3 days?'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HEALTH16"/>
          </xsl:call-template>
        </tr>

        <xsl:if test="$node/insurability/HEALTH16 = 'Yes'">
          <tr>
            <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
              <xsl:with-param   name  ="col01Text"
                                select="'Duration of Hospital Stay (days):'"/>
              <xsl:with-param   name  ="col02Text"
                                select="$node/insurability/HEALTH16a"/>
              <xsl:with-param   name  ="col01Align"
                                select="2"/>
            </xsl:call-template>
          </tr>
        </xsl:if>

        <!--Q4d Juvenile -->
        <tr class = "even">
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text"
                              select="'d) Did the child ever suffer from/does
                              the child currently suffer from/or being followed
                              up or being investigated for any residual
                              birth/delivery complications, congenital
                              disorder/birth defect, physical impairment,
                              mental retardation, G6PD deficiency,
                              cerebral palsy, Down’s Syndrome, prolonged
                              jaundice, respiratory distress syndrome or
                              any other disorder?'"/>
            <xsl:with-param   name  ="col02Text"
                              select="$node/insurability/HEALTH_SHIELD_24"/>
          </xsl:call-template>
        </tr>


        <xsl:variable   name="numAnsQuestion4dJuvenilce">
          <xsl:value-of select ="count($node/insurability/HEALTH_SHIELD_24_DATA)"/>
        </xsl:variable>

        <!--Q4d Juvenile Table-->
        <xsl:if test="$numAnsQuestion4dJuvenilce > 0">
          <tr>
            <td  colspan = "2" style = "padding: 5px 8px;">

              <table class = "simple centeredData">
                <colgroup>
                  <col style="width:22%"/>
                  <col style="width:8%"/>
                  <col style="width:9%"/>
                  <col style="width:25%"/>
                  <col style="width:12%"/>
                  <col style="width:24%"/>
                </colgroup>

                <tr>
                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Medical Condition/ Diagnosis</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Year of diagnosis</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Medical Test Report</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Medication</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Next Follow-up visit date</span>
                    </p>
                  </td>

                  <td>
                    <p class="normal">
                      <span lang="EN-HK" class="th">Current Complication</span>
                    </p>
                  </td>
                </tr>


                <xsl:for-each select="$node/insurability/HEALTH_SHIELD_24_DATA">
                  <tr>
                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_SHIELD_24a"/></span>
                      </p>
                    </td>

                    <td>
                      <p class="userData" style = "text-align:left;">
                        <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_SHIELD_24b"/></span>
                      </p>
                    </td>

                    <td>
                      <p class="userData" style = "text-align:left;">
                        <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_SHIELD_24c"/></span>
                      </p>
                    </td>

                    <td>
                      <p class="userData">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:choose>
                            <xsl:when test="string-length(./HEALTH_SHIELD_24d) = 0">-</xsl:when>
                            <xsl:otherwise><xsl:value-of select="./HEALTH_SHIELD_24d"/></xsl:otherwise>
                          </xsl:choose>
                        </span>
                      </p>
                    </td>

                    <td>
                      <p class="userData" style = "text-align:left;">
                        <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_SHIELD_24e"/></span>
                      </p>
                    </td>

                    <td>
                      <p class="userData" style = "text-align:left;">
                        <span lang="EN-HK" class="tdAns"><xsl:value-of select="./HEALTH_SHIELD_24f"/></span>
                      </p>
                    </td>
                  </tr>
                </xsl:for-each>
              </table>
            </td>

          </tr>
        </xsl:if>

        <xsl:if test="$node/insurability/HEALTH14 = 'Yes'
          or $node/insurability/HEALTH15 = 'Yes'
          or $node/insurability/HEALTH16 = 'Yes'
          or $node/insurability/HEALTH_SHIELD_24 = 'Yes'">
          <tr>
            <td style="padding-left: 7.2pt;" colspan = "2">
              <p class="normal">
                <br/>
                <br/>

                <span lang="EN-HK" class="normal warningPolicies">
                  Note: If any of the Question 4(a) to 4(d) is “Yes”, please submit
                  Child Health Booklet
                </span>
                <br/>

              </p>
            </td>
          </tr>
        </xsl:if>
      </xsl:if>

      <xsl:if test="$node/insurability/insAllNoInd[. = 'Yes']">
        <tr>
          <td colspan = "2" style="padding-left: 7.2pt;">
            <p class="normal">
              <br/>

              <xsl:variable   name="chkConfirmed">
                <xsl:choose>
                  <xsl:when test="$node/insurability/insAllNoInd = 'Yes'">
                    <xsl:value-of select="'1'"/>
                  </xsl:when>

                  <xsl:otherwise>
                    <xsl:value-of select="'0'"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>


              <xsl:call-template  name  ="comCheckbox">
                <xsl:with-param   name  ="isChecked"
                                  select="$chkConfirmed"/>
              </xsl:call-template>


              <span lang="EN-HK" class="question">
                I confirm the answer to all of the above medical and health
                questions for life assured is "no"
                <br/>
              </span>
            </p>
          </td>
        </tr>
        <tr>
          <td colspan = "2" style="padding-left: 7.2pt;">
            <p class="normal">
              <br/>

              <span lang="EN-HK" class="normal warningPoliciesBold">
                Note: The above declaration doesn’t apply to smoking question,
                which is auto-populated from Client Profile
              </span>
              <br/>

            </p>
          </td>
        </tr>
      </xsl:if>
    </table>
    <p><br/></p>
  </div>


</xsl:template>



<xsl:template name="appform_report_insurability">
  <xsl:param  name="isProposer"/>

  <xsl:choose>
    <xsl:when test = "$isProposer = 'true'">
      <xsl:call-template    name  ="appform_shield_insurance_info">
        <xsl:with-param     name  ="node" select="/root/reportData/proposer"/>
        <xsl:with-param     name  ="orgNode" select="/root/originalData/proposer"/>
        <xsl:with-param     name  ="isProposer" select="$isProposer"/>
      </xsl:call-template>

      <xsl:if test="/root/reportData/proposer/extra/isPhSameAsLa = 'Y'">
        <xsl:call-template  name  ="appform_shield_medical_questionaire">
          <xsl:with-param   name  ="node" select="/root/reportData/proposer"/>
          <xsl:with-param   name  ="orgNode" select="/root/originalData/proposer"/>
          <xsl:with-param   name  ="isProposer" select="$isProposer"/>
        </xsl:call-template>
      </xsl:if>
    </xsl:when>

    <xsl:otherwise>
      <xsl:call-template    name  ="appform_shield_insurance_info">
        <xsl:with-param     name  ="node" select="/root/reportData/insured"/>
        <xsl:with-param     name  ="orgNode" select="/root/originalData/insured"/>
        <xsl:with-param     name  ="isProposer" select="$isProposer"/>
      </xsl:call-template>

      <xsl:call-template    name  ="appform_shield_medical_questionaire">
        <xsl:with-param     name  ="node" select="/root/reportData/insured"/>
        <xsl:with-param     name  ="orgNode" select="/root/originalData/insured"/>
        <xsl:with-param     name  ="isProposer" select="$isProposer"/>
      </xsl:call-template>

    </xsl:otherwise>
  </xsl:choose>

</xsl:template>


</xsl:stylesheet>
