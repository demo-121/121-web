<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="appform_pdf_planDetails">
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">PLAN DETAILS</span>
    </p>

    <table class="dataGroup">
      <thead>
        <xsl:for-each select="/root/planDetails/planList[covCode = /root/baseProductCode]">
          <tr>
            <xsl:variable name="basicPlanName">
              <xsl:choose>
                <xsl:when test="/root/lang = 'en'">
                  <xsl:value-of select="./covName/en"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="./covName/zh-Hant"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="$basicPlanName"/>
              <xsl:with-param name="colspan" select="8"/>
            </xsl:call-template>
          </tr>
        </xsl:for-each>
      </thead>
      <tbody>
        <xsl:for-each select="/root/planDetails/planList[covCode = /root/baseProductCode]">
          <tr class="dataGroup">
            <xsl:variable name="basicPlanName">
              <xsl:choose>
                <xsl:when test="/root/lang = 'en'">
                  <xsl:value-of select="./covName/en"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="./covName/zh-Hant"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
              <xsl:with-param name="header1" select="'Plan Name'"/>
              <xsl:with-param name="value1" select="$basicPlanName"/>
              <xsl:with-param name="header2" select="'Sum Assured / Benefits'"/>
              <xsl:with-param name="value2" select="../sumInsured"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
              <xsl:with-param name="header1" select="'Payment Method'"/>
              <xsl:with-param name="value1" select="/root/policyOptionsDesc/paymentMethod/en"/>
              <xsl:with-param name="header2" select="'Single Premium Amount'"/>
              <xsl:with-param name="value2" select="./premium"/>
            </xsl:call-template>
          </tr>
        </xsl:for-each>
        <xsl:for-each select="/root/planDetails/planList[not(covCode = /root/baseProductCode)]">
          <tr class="dataGroup">
            <xsl:attribute name="class">
              <xsl:choose>
                <xsl:when test="position() mod 2 = 0">
                  <xsl:value-of select="'even'"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="'odd'"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <td width="113" class="tdFirst padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">
                  <xsl:choose>
                    <xsl:when test="position() = 1">Rider</xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td width="189" colspan="2" class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="/root/lang = 'en'">
                      <xsl:value-of select="./covName/en"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./covName/zh-Hant"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td width="123" colspan="2" class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="./saViewInd = 'Yes'">-</xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./sumInsured"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td width="113" class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./polTermDesc"/>
                </span>
              </p>
            </td>
            <td width="180" colspan="2" class="tdLast padding">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./premTermDesc"/>
                </span>
              </p>
            </td>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>

    <!-- OTHER PLAN DETAILS START -->
    <table class="dataGroup">
      <thead>
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'Other Plan Details'"/>
            <xsl:with-param name="colspan" select="8"/>
          </xsl:call-template>
        </tr>
      </thead>
      <tbody>
        <tr class="dataGroup">
          <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
            <xsl:with-param name="header1" select="'Policy Currency'"/>
            <xsl:with-param name="value1" select="/root/planDetails/ccy"/>
            <xsl:with-param name="header2" select="'Single Premium Sales Charge'"/>
            <xsl:with-param name="value2" select="/root/policyOptionsDesc/singlePremSalesCharge/en"/>
          </xsl:call-template>
        </tr>

        <xsl:if test="not(root/policyOptions/rspAmount = 'null') and not(/root/policyOptions/rspPayFreq = 'null')">
          <tr class="dataGroup">
            <td class="tdOneCol" colspan="8">
              <p class="title subHeader">
                <span lang="EN-HK" class="questionItalic">Recurring Single Premium (RSP) Details</span>
              </p>
            </td>
          </tr>

          <tr class="dataGroup">
            <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
              <xsl:with-param name="header1" select="'RSP Amount'"/>
              <xsl:with-param name="value1" select="/root/policyOptions/rspAmount"/>
              <xsl:with-param name="header2" select="'RSP Payment Frequency'"/>
              <xsl:with-param name="value2" select="/root/policyOptionsDesc/rspPayFreq/en"/>
            </xsl:call-template>
          </tr>

          <tr class="dataGroup">
            <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
              <xsl:with-param name="header1" select="'RSP Sales Charge'"/>
              <xsl:with-param name="value1" select="/root/policyOptions/rspSalesCharges"/>
            </xsl:call-template>
          </tr>
        </xsl:if>
      </tbody>
    </table>
    <!-- OTHER PLAN DETAILS END -->

    <!-- FUND START -->
    <table class="dataGroupLast">
      <thead>
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'Fund Details'"/>
            <xsl:with-param name="colspan" select="8"/>
          </xsl:call-template>
        </tr>
      </thead>
      <tbody>
        <tr class="odd">
          <xsl:call-template name="planDetails_planInfoTwoHeadersTmpl">
            <xsl:with-param name="header1" select="'Fund Name'"/>
            <xsl:with-param name="header2" select="'Allocation'"/>
          </xsl:call-template>
        </tr>
        <xsl:for-each select="/root/fund/funds">
          <tr class="dataGroup">
            <xsl:call-template name="planDetails_planInfoTwoValuesTmpl">
              <xsl:with-param name="value1">
                <xsl:choose>
                  <xsl:when test="/root/lang = 'en'">
                    <xsl:value-of select="./fundName/en"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="./fundName/zh-Hant"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:with-param>
              <xsl:with-param name="value2">
                <xsl:choose>
                  <xsl:when test="contains(./alloc, '%')">
                    <xsl:value-of select="./alloc"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="concat(./alloc, '%')"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:with-param>
            </xsl:call-template>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>
    <!-- FUND END -->

    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
  </div>
</xsl:template>

</xsl:stylesheet>
