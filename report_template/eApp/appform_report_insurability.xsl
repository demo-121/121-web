<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="appform_pdf_insurability">
  <xsl:if test="count(/root/showQuestions/insurability/question) > 0">
    <div class="section">
      <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 2) = 'HW']">
        <p class="sectionGroup">
          <span class="sectionGroup">INSURABILITY INFORMATION</span>
        </p>
        <table class="dataGroupLastNoAutoColor">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Height and Weight'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW01'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Height (m)'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW01'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW02'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Weight (kg)'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW02'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW03'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Any weight change in the last 12 months? (kg)'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW03'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW03a'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Weight change'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW03a'"/>
                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW03b'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'How many kg?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW03b'"/>
                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW03c'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Reason of weight change'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW03c'"/>
                <xsl:with-param name="repalceOtherAnswerId" select="'HW03c1'"/>
                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
        </table>
      </xsl:if>
    </div>
    <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 3) = 'INS']">
      <div class="section">
        <xsl:if test="count(/root/showQuestions/insurability/question[substring(., 1, 2) = 'HW']) = 0">
          <p class="sectionGroup">
            <span class="sectionGroup">INSURABILITY INFORMATION</span>
          </p>
        </xsl:if>
        <table class="dataGroup first">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Insurance History'"/>
            </xsl:call-template>
          </tr>
          <tr class="odd">
            <xsl:call-template name="questionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'1.
                Have you ever made an application or application for
                reinstatement of a life, disability, accident, medical or
                critical illness insurance which has been accepted with an
                extra premium or on special terms, postponed, declined,
                withdrawn or is still being considered?'"/>
              <xsl:with-param name="sectionGroupName" select="'insurability'"/>
              <xsl:with-param name="answerId" select="'INS01'"/>
            </xsl:call-template>
          </tr>

          <xsl:if test="/root/showQuestions/insurability/question = 'INS01_DATA'">
            <tr>
              <td class="tdOneCol">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <table class="data">
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <col width="72"/>
                  </xsl:if>
                  <col width="148"/>
                  <col width="110"/>
                  <col width="110"/>
                  <col width="95"/>
                  <col width="156"/>
                  <tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">For</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Name of Insurance Company</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Type of Coverage</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Date incurred (MM/YYYY)</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Condition of Special Terms</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Decision &amp; Detailed Reason(s) of special terms</span>
                      </p>
                    </td>
                  </tr>
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <xsl:for-each select="/root/insured">
                      <xsl:for-each select="insurability/*[local-name() = 'INS01_DATA']">
                        <tr class="odd">
                          <xsl:if test="position() = 1">
                            <td>
                              <xsl:attribute name="class">
                                <xsl:choose>
                                  <xsl:when test="count(/root/proposer/insurability/*[local-name() = 'INS01_DATA']) > 0">
                                    <xsl:value-of select="'tdFor'"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="'tdFor bottom'"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:attribute>
                              <xsl:attribute name="rowspan">
                                <xsl:value-of select="last()"/>
                              </xsl:attribute>
                              <p>
                                <span lang="EN-HK" class="person">Life Assured</span>
                              </p>
                            </td>
                          </xsl:if>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS01a"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <xsl:if test="INS01b1 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Life
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS01b2 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Total Permanent Disability
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS01b3 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Critical Illness
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS01b4 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Accident
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS01b5 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                                </span>
                              </p>
                            </xsl:if>
                          </td>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS01c"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS01d"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS01e"/>
                              </span>
                            </p>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:for-each>
                  </xsl:if>
                  <xsl:for-each select="/root/proposer/insurability/*[local-name() = 'INS01_DATA']">
                    <tr class="even">
                      <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                        <td class="tdFor bottom">
                          <xsl:attribute name="rowspan">
                            <xsl:value-of select="last()"/>
                          </xsl:attribute>
                          <p class="tdAns">
                            <span lang="EN-HK" class="person">Proposer</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS01a"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <xsl:if test="INS01b1 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Life
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS01b2 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Total Permanent Disability
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS01b3 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Critical Illness
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS01b4 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Accident
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS01b5 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                            </span>
                          </p>
                        </xsl:if>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS01c"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS01d"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS01e"/>
                          </span>
                        </p>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
            <tr>
              <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>

                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr>
          </xsl:if>
        </table>
      </div>
      <div class="section">
        <table class="dataGroup last">
          <tr class="even">
            <xsl:call-template name="questionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'2.
                  Are you presently receiving a disability benefit or incapable
                  for work or have you ever made or intend to make any claim
                  against any insurer for disability, accident, medical care,
                  hospitalisation, critical illness and/or other benefits?'"/>
              <xsl:with-param name="sectionGroupName" select="'insurability'"/>
              <xsl:with-param name="answerId" select="'INS02'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/showQuestions/insurability/question = 'INS02_DATA'">
            <tr>
              <td class="tdOneCol">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>

                <table class="data">
                  <xsl:choose>
                    <xsl:when test="$lifeAssuredIsProposer = 'true'">
                      <col width="140"/>
                      <col width="118"/>
                      <col width="80"/>
                      <col width="112"/>
                      <col width="240"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <col width="84"/>
                      <col width="140"/>
                      <col width="118"/>
                      <col width="80"/>
                      <col width="112"/>
                      <col width="156"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">For</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Name of Insurance Company</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Type of the Claim</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Date incurred (MM/YYYY)</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Medical Condition</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Claim Details</span>
                      </p>
                    </td>
                  </tr>

                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <xsl:for-each select="/root/insured">
                      <xsl:for-each select="insurability/*[local-name() = 'INS02_DATA']">
                        <tr class="odd">
                          <xsl:if test="position() = 1">
                            <td>
                              <xsl:attribute name="class">
                                <xsl:choose>
                                  <xsl:when test="count(/root/proposer/insurability/*[local-name() = 'INS02_DATA']) > 0">
                                    <xsl:value-of select="'tdFor'"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="'tdFor bottom'"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:attribute>
                              <xsl:attribute name="rowspan">
                                <xsl:value-of select="last()"/>
                              </xsl:attribute>
                              <p>
                                <span lang="EN-HK" class="person">Life Assured</span>
                              </p>
                            </td>
                          </xsl:if>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS02a"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <xsl:if test="INS02b1 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Total Permanent Disability
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS02b2 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Critical Illness
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS02b3 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Accident
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS02b4 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="other = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/><xsl:value-of select="resultOther"/>
                                </span>
                              </p>
                            </xsl:if>
                          </td>
                          <td class="tdAns" width="100">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS02c"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns" width="100">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS02d"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns" width="149">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS02e"/>
                              </span>
                            </p>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:for-each>
                  </xsl:if>

                  <xsl:for-each select="/root/proposer/insurability/*[local-name() = 'INS02_DATA']">
                    <tr class="even">
                      <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                        <td class="tdFor bottom">
                          <xsl:attribute name="rowspan">
                            <xsl:value-of select="last()"/>
                          </xsl:attribute>
                          <p class="tdAns">
                            <span lang="EN-HK" class="person">Proposer</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS02a"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <xsl:if test="INS02b1 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Total Permanent Disability
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS02b2 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Critical Illness
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS02b3 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Accident
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS02b4 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="other = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/><xsl:value-of select="resultOther"/>
                            </span>
                          </p>
                        </xsl:if>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS02c"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS02d"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS02e"/>
                          </span>
                        </p>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
            <tr>
              <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr>
          </xsl:if>
        </table>
      </div>
    </xsl:if>

    <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 9) = 'LIFESTYLE']">

      <div class="section">
        <table class="dataGroup first">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Lifestyle and Habits'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE01'">
            <tr>
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'1.
                  Have you smoked or used any tobacco, nicotine or smokeless
                  tobacco products (e.g. cigarettes, cigar, e-cigarettes, pipes, nicotine patch, etc.) within the past 12 months?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE01'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE01a'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'i) Type of product (e.g. cigarettes, cigar, e-cigarettes, pipes, nicotine patch, etc.):'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE01a'"/>
                  <xsl:with-param name="repalceOtherAnswerId" select="'LIFESTYLE01a_OTH'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE01b'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'ii) Number of products smoked per day (i.e. how many sticks/ pipes/ patches per day?):'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE01b'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE01c'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'iii) Number of years smoked:'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE01c'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <!-- <tr>
              <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>

                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr> -->
          </xsl:if>
        </table>
      </div>
      <div class="section">
        <table class="dataGroup mid">
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE02'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'2.
                    Do you consume alcohol? If yes, how much alcohol do you drink
                    per week on average?'"/>
                <xsl:with-param name="extraQuestion" select="'Note:
                    ONE standard unit of alcoholic drink equates to Beer 330ml/can,
                    Wine 125ml/glass, or Spirits 30ml/cup.'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE02'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE02a_2'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'Beer (330 ml/can)'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE02a_2'"/>
                  <xsl:with-param name="extraAnswer" select="' can(s)'"/>
                  <xsl:with-param name="isAddExtraAnswerId" select="'LIFESTYLE02a_1'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                  <xsl:with-param name="isSubQuestionWithTick" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE02b_2'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'Wine (125 ml/glass)'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE02b_2'"/>
                  <xsl:with-param name="extraAnswer" select="' glass(es)'"/>
                  <xsl:with-param name="isAddExtraAnswerId" select="'LIFESTYLE02b_1'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                  <xsl:with-param name="isSubQuestionWithTick" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE02c_2'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'Spirits (30 ml/cup)'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE02c_2'"/>
                  <xsl:with-param name="extraAnswer" select="' cup(s)'"/>
                  <xsl:with-param name="isAddExtraAnswerId" select="'LIFESTYLE02c_1'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                  <xsl:with-param name="isSubQuestionWithTick" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <!-- <xsl:if test="/root/showQuestions/insurability/question[. = 'LIFESTYLE02a_2' or . = 'LIFESTYLE02b_2' or . = 'LIFESTYLE02c_2']">
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if> -->
          </xsl:if>
        </table>
      </div>
      <div class="section">
        <table class="dataGroup mid">
          <xsl:attribute name="class">
            <xsl:choose>
              <xsl:when test="/root/showQuestions/insurability/question = 'LIFESTYLE04'">
                <xsl:value-of select="'dataGroup mid'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'dataGroup last'"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE03'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'3.
                    Have you ever used any habit forming drugs or narcotics or been treated for drug habits?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE03'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE03_DATA'">
              <tr>
                <td style="border: none; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>

                  <table class="data">
                    <xsl:choose>
                      <xsl:when test="$lifeAssuredIsProposer = 'true'">
                        <col width="140"/>
                        <col width="142"/>
                        <col width="142"/>
                        <col width="266"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <col width="84"/>
                        <col width="140"/>
                        <col width="142"/>
                        <col width="142"/>
                        <col width="182"/>
                      </xsl:otherwise>
                    </xsl:choose>
                    <tr>
                      <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                        <td class="tdAns">
                          <p class="thAnsCenter">
                            <span lang="EN-HK" class="th">For</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Substances used</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Date Commenced (MM/YYYY)</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Date Ceased (MM/YYYY)</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Details</span>
                        </p>
                      </td>
                    </tr>

                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <xsl:for-each select="/root/insured">
                        <xsl:for-each select="insurability/LIFESTYLE03_DATA">
                          <tr class="odd">
                            <xsl:if test="position() = 1">
                              <td>
                                <xsl:attribute name="class">
                                  <xsl:choose>
                                    <xsl:when test="count(/root/proposer/insurability/LIFESTYLE03_DATA) > 0">
                                      <xsl:value-of select="'tdFor'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="'tdFor bottom'"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:attribute>
                                <xsl:attribute name="rowspan">
                                  <xsl:value-of select="last()"/>
                                </xsl:attribute>
                                <p>
                                  <span lang="EN-HK" class="person">Life Assured</span>
                                </p>
                              </td>
                            </xsl:if>
                            <td class="tdAns">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:value-of select="LIFESTYLE03a"/>
                                </span>
                              </p>
                            </td>
                            <td class="tdAns">
                              <p class="tdAns">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:value-of select="LIFESTYLE03b"/>
                                </span>
                              </p>
                            </td>
                            <td class="tdAns">
                              <p class="tdAns">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:value-of select="LIFESTYLE03c"/>
                                </span>
                              </p>
                            </td>
                            <td class="tdAns">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:value-of select="LIFESTYLE03d"/>
                                </span>
                              </p>
                            </td>
                          </tr>
                        </xsl:for-each>
                      </xsl:for-each>
                    </xsl:if>

                    <xsl:for-each select="/root/proposer/insurability/LIFESTYLE03_DATA">
                      <tr class="even">
                        <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                          <td class="tdFor bottom">
                            <xsl:attribute name="rowspan">
                              <xsl:value-of select="last()"/>
                            </xsl:attribute>
                            <p class="tdAns">
                              <span lang="EN-HK" class="person">Proposer</span>
                            </p>
                          </td>
                        </xsl:if>
                        <td class="tdAns">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="LIFESTYLE03a"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="LIFESTYLE03b"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="LIFESTYLE03c"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="LIFESTYLE03d"/>
                            </span>
                          </p>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
              <tr>
                <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
        </table>
      </div>
      <div class="section">
        <table class="dataGroup mid">
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE04'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'4.
                    Do you have any intention of residing outside Singapore for more than 6 months?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE04'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE04_DATA'">
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <xsl:call-template name="lifeStyleTableTmpl">
                    <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                    <xsl:with-param name="answerId" select="'LIFESTYLE04_DATA'"/>
                  </xsl:call-template>
                </td>
              </tr>
              <tr>
                <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
        </table>
      </div>
      <div class="section">
        <table class="dataGroup last">
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE05'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'5.
                    Do you participate or intend to participate in any hazardous
                    activities related to your occupation or recreation such as
                    diving, mountaineering, skydiving, parachuting, hang gliding,
                    motor sports or aviation (excluding flying as a passenger on a
                    regular scheduled airline)?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE05'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE05_DATA'">
              <tr>
                <td style="border: none; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <table class="data">
                    <xsl:choose>
                      <xsl:when test="$lifeAssuredIsProposer = 'true'">
                        <col width="160"/>
                        <col width="140"/>
                        <col width="380"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <col width="84"/>
                        <col width="160"/>
                        <col width="140"/>
                        <col width="296"/>
                      </xsl:otherwise>
                    </xsl:choose>
                    <tr>
                      <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                        <td class="tdAns" width="84">
                          <p class="thAnsCenter">
                            <span lang="EN-HK" class="th">For</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Type of hazardous activities</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Frequency (per year)</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Details</span>
                        </p>
                      </td>
                    </tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <xsl:for-each select="/root/insured">
                        <xsl:for-each select="insurability/LIFESTYLE05_DATA">
                          <tr class="odd">
                            <xsl:if test="position() = 1">
                              <td>
                                <xsl:attribute name="class">
                                  <xsl:choose>
                                    <xsl:when test="count(/root/proposer/insurability/LIFESTYLE05_DATA) > 0">
                                      <xsl:value-of select="'tdFor'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="'tdFor bottom'"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:attribute>
                                <xsl:attribute name="rowspan">
                                  <xsl:value-of select="last()"/>
                                </xsl:attribute>
                                <p>
                                  <span lang="EN-HK" class="person">Life Assured</span>
                                </p>
                              </td>
                            </xsl:if>
                            <td class="tdAns">
                              <p class="tdAns">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:choose>
                                    <xsl:when test="LIFESTYLE05a = 'Other'">
                                      <xsl:value-of select="resultOther"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="LIFESTYLE05a"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </span>
                              </p>
                            </td>
                            <td class="tdAns">
                              <p class="tdAns">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:value-of select="LIFESTYLE05b"/>
                                </span>
                              </p>
                            </td>
                            <td class="tdAns">
                              <p class="tdAns">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:choose>
                                    <xsl:when test="string-length(LIFESTYLE05c) > 0">
                                      <xsl:value-of select="LIFESTYLE05c"/>
                                    </xsl:when>
                                    <xsl:otherwise>-</xsl:otherwise>
                                  </xsl:choose>
                                </span>
                              </p>
                            </td>
                          </tr>
                        </xsl:for-each>
                      </xsl:for-each>
                    </xsl:if>
                    <xsl:for-each select="/root/proposer/insurability/LIFESTYLE05_DATA">
                      <tr class="even">
                        <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                          <td class="tdFor bottom">
                            <xsl:attribute name="rowspan">
                              <xsl:value-of select="last()"/>
                            </xsl:attribute>
                            <p class="tdAns">
                              <span lang="EN-HK" class="person">Proposer</span>
                            </p>
                          </td>
                        </xsl:if>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:choose>
                                <xsl:when test="LIFESTYLE05a = 'Other'">
                                  <xsl:value-of select="resultOther"/>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="LIFESTYLE05a"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="LIFESTYLE05b"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:choose>
                                <xsl:when test="string-length(LIFESTYLE05c) > 0">
                                  <xsl:value-of select="LIFESTYLE05c"/>
                                </xsl:when>
                                <xsl:otherwise>-</xsl:otherwise>
                              </xsl:choose>
                            </span>
                          </p>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
              <tr>
                <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE06'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'6.
                    Do you travel outside Singapore for more than 3 months in a year (other than
                    for holidays or studies)?'"/>
                <xsl:with-param name="extraQuestion" select="'Please provide details of country/city, frequency and
                    duration of the trips?'"/>
                <xsl:with-param name="isHideableExtraQuestion" select="'true'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE06'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE06_DATA'">
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <xsl:call-template name="lifeStyleTableTmpl">
                    <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                    <xsl:with-param name="answerId" select="'LIFESTYLE06_DATA'"/>
                  </xsl:call-template>
                </td>
              </tr>
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
        </table>
      </div>
    </xsl:if>
    <div class="section">
      <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 6) = 'FAMILY']">
        <table class="dataGroupLastNoAutoColor">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Family History'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/showQuestions/insurability/question = 'FAMILY01'">
            <tr>
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'1.
                    Has your biological mother, father, or any sister or brother been
                    diagnosed prior to age 60 with any of the following?'"/>
                <xsl:with-param name="extraQuestion" select="concat(
                    'Cancer, heart disease, stroke, diabetes, Huntington', $apos, 's disease,
                    polycystic kidney disease, Multiple Sclerosis, Alzheimer', $apos, 's
                    or any other inherited conditions.')"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'FAMILY01'"/>
                <xsl:with-param name="isQuestionWithoutNumber" select="boolean(1)" />
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'FAMILY01_DATA'">
            <tr>
              <td width="749" valign="top"
                style="border: solid windowtext 1.0pt; border-top: none; border-bottom: none; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <table class="data">
                  <xsl:choose>
                    <xsl:when test="$lifeAssuredIsProposer = 'true'">
                      <col width="140"/>
                      <col width="410"/>
                      <col width="140"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <col width="84"/>
                      <col width="140"/>
                      <col width="326"/>
                      <col width="140"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">For</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Relationship</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Medical Condition</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Age of Onset</span>
                      </p>
                    </td>
                  </tr>
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <xsl:for-each select="/root/insured">
                      <xsl:for-each select="insurability/FAMILY01_DATA">
                        <tr class="odd">
                          <xsl:if test="position() = 1">
                            <td>
                              <xsl:attribute name="class">
                                <xsl:choose>
                                  <xsl:when test="count(/root/proposer/insurability/FAMILY01_DATA) > 0">
                                    <xsl:value-of select="'tdFor'"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="'tdFor bottom'"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:attribute>
                              <xsl:attribute name="rowspan">
                                <xsl:value-of select="last()"/>
                              </xsl:attribute>
                              <p>
                                <span lang="EN-HK" class="person">Life Assured</span>
                              </p>
                            </td>
                          </xsl:if>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="FAMILY01a"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="FAMILY01b"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="FAMILY01c"/>
                              </span>
                            </p>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:for-each>
                  </xsl:if>

                  <xsl:for-each select="/root/proposer/insurability/FAMILY01_DATA">
                    <tr class="even">
                      <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                        <td class="tdFor bottom">
                          <xsl:attribute name="rowspan">
                            <xsl:value-of select="last()"/>
                          </xsl:attribute>
                          <p class="tdAns">
                            <span lang="EN-HK" class="person">Proposer</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="FAMILY01a"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="FAMILY01b"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="FAMILY01c"/>
                          </span>
                        </p>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
            <tr>
              <td class="tdOneCol">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr>
          </xsl:if>
        </table>
      </xsl:if>
    </div>
    <div class="section">
      <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 6) = 'REG_DR']">
        <table class="dataGroupLastNoAutoColor">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Details of Regular Doctor'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/showQuestions/insurability/question = 'REG_DR01'">
            <tr>
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Do you have a regular doctor?'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'REG_DR01'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'REG_DR01_DATA'">
            <tr>
              <td width="714" valign="top"
                style="width: 535.25pt; border: solid windowtext 1.0pt; border-top: none; border-bottom: none; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <table class="data">
                  <xsl:choose>
                    <xsl:when test="$lifeAssuredIsProposer = 'true'">
                      <col width="140"/>
                      <col width="160"/>
                      <col width="125"/>
                      <col width="265"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <col width="84"/>
                      <col width="140"/>
                      <col width="160"/>
                      <col width="125"/>
                      <col width="181"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">For</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Name of Doctor</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Name &amp; Address of Clinic/ Hospital</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Date of last consultation</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Details of Consultations</span>
                      </p>
                    </td>
                  </tr>
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <xsl:for-each select="/root/insured">
                      <xsl:for-each select="insurability/*[local-name() = 'REG_DR01_DATA']">
                        <tr class="odd">
                          <xsl:if test="position() = 1">
                            <td>
                              <xsl:attribute name="class">
                                <xsl:choose>
                                  <xsl:when test="count(/root/proposer/insurability/*[local-name() = 'REG_DR01_DATA']) > 0">
                                    <xsl:value-of select="'tdFor'"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="'tdFor bottom'"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:attribute>
                              <xsl:attribute name="rowspan">
                                <xsl:value-of select="last()"/>
                              </xsl:attribute>
                              <p class="tdAns">
                                <span lang="EN-HK" class="person">Life Assured</span>
                              </p>
                            </td>
                          </xsl:if>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="REG_DR01a"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="REG_DR01b"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="REG_DR01c"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:choose>
                                  <xsl:when test="REG_DR01d = 'Other'">
                                    <xsl:value-of select="resultOther"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="REG_DR01d"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </span>
                            </p>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:for-each>
                  </xsl:if>
                  <xsl:for-each select="/root/proposer/insurability/*[local-name() = 'REG_DR01_DATA']">
                    <tr class="even">
                      <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                        <td class="tdFor bottom">
                          <xsl:attribute name="rowspan">
                            <xsl:value-of select="last()"/>
                          </xsl:attribute>
                          <p class="tdAns">
                            <span lang="EN-HK" class="person">Proposer</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="REG_DR01a"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="REG_DR01b"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="REG_DR01c"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:choose>
                              <xsl:when test="REG_DR01d = 'Other'">
                                <xsl:value-of select="resultOther"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="REG_DR01d"/>
                              </xsl:otherwise>
                            </xsl:choose>
                          </span>
                        </p>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
            <tr>
              <td width="701" valign="top"
                style="width: 526.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid windowtext 1.0pt; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr>
          </xsl:if>
        </table>
      </xsl:if>
    </div>
    <div class="section">
      <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 6) = 'HEALTH']">
        <table class="dataGroupLastNoAutoColor">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Medical and Health Information'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 6) = 'HEALTH' and not(substring(., 1, 7) = 'HEALTH_')]">
            <tr class="odd">
              <td class="question">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="question">
                  <span lang="EN-HK" class="question">1.
                    Have you ever had, or been told you had, or received treatment for:</span>
                </p>
              </td>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH02'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'a)
                    Chest pain, high blood pressure, heart attack, stroke,
                    diabetes, or any heart, blood or vascular disease or disorder?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH02'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH03'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'b)
                    Cancer, melanoma, tumour, cysts, lump, polyp or growth of any kind?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH03'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH04'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'c)
                    Kidney disease (e.g. stone, cyst), mental or nervous disorder
                    (e.g. anxiety, depression), vision or hearing problems (e.g.
                    cataract, meniere’s disease), digestive system disorder
                    (e.g. gastritis), endocrine disease (e.g. thyroid problem),
                    liver disease (e.g. fatty liver), nervous system disorder (e.g.
                    epilepsy, motor neurone disease), respiratory system disorder
                    (e.g. asthma), skin disease (e.g. psoriasis), urinary system
                    disorder (e.g. blood/protein in urine), spinal or muscle
                    problems (e.g. slipped disc, dystrophy)?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH04'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH05'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'d)
                    Joint, limb or bone disease or disorder, auto-immune disease
                    (e.g. systemic lupus erythematosus) or infectious disease?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH05'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH06'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'e)
                    Hepatitis B or C, HIV infection, tuberculosis, alcohol or drug dependency?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH06'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH07'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'f)
                    Any other illness, disease, disorder, operation, physical
                    disability or accident not mentioned above?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH07'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH01'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'g)
                    Any physical or developmental impairments or abnormalities or premature birth?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH01'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH08'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'2.
                    Are you currently receiving any medical treatment or do you
                    intend seeking or have been advised to seek medical treatment
                    for any health problems or are you awaiting the results of any
                    tests/ investigations?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH08'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH09'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'3.
                    Apart from condition listed above, have you ever seen a doctor
                    or other health professional, or been prescribed medication for
                    any other condition which has lasted for more than (apart from
                    usual flu and colds) 5 days?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH09'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH10'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'4.
                    Have you or your spouse been told to have received any medical
                    advice, counseling or treatment in connection with sexually
                    transmitted disease, AIDS, AIDS Related Complex or any other
                    AIDS related condition?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH10'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH11'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'5.
                    In the PAST FIVE YEARS,
                    have you had any tests done such as X-ray, ultrasound, CT
                    scan, biopsy, electrocardiogram (ECG), blood or urine test?'"/>
              <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH11'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question[. = 'HEALTH_GROUP_DATA']">
            <tr>
              <td width="713" valign="top"
                style="width: 535.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <xsl:call-template name="healthGroupTableTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="tdOneCol">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>

                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH12' or /root/showQuestions/insurability/question = 'HEALTH13'">
            <tr>
              <td
                style="border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="question">
                  <span lang="EN-HK" class="questionBold">6.
                    For Female Applicants only (For age 10 and above)</span>
                </p>
              </td>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH12'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'(a)
                      Have you had any breast disease, menstrual disorders, fibroids,
                      cysts or any other disorders of the female reproductive organs,
                      or abnormal pap smear, mammogram, ultrasound or any
                      gynaecological investigations?'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'HEALTH12'"/>
                </xsl:call-template>
              </tr>
              <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH12_DATA'">
                <tr>
                  <td style="border: none">
                    <xsl:attribute name="colspan">
                      <xsl:value-of select="$colspan"/>
                    </xsl:attribute>
                    <xsl:call-template name="healthTableTmpl">
                      <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                      <xsl:with-param name="answerId" select="'HEALTH12'"/>
                    </xsl:call-template>
                  </td>
                </tr>
              </xsl:if>
            </xsl:if>
            <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH13'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'(b)
                      Are you currently pregnant?'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'HEALTH13'"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH13a'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'i)
                      How many weeks of pregnancy now? (Weeks)'"/>
                  <xsl:with-param name="isQuestionWithIndent" select="boolean(1)"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'HEALTH13a'"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH13b'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'ii)
                      Any complications during pregnancy such as gestational
                      diabetes, hypertension or any other pregnancy related
                      condition?'"/>
                  <xsl:with-param name="isQuestionWithIndent" select="boolean(1)"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'HEALTH13b'"/>
                </xsl:call-template>
              </tr>
              <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH13b_DATA'">
                <tr>
                  <td style="border: none">
                    <xsl:attribute name="colspan">
                      <xsl:value-of select="$colspan"/>
                    </xsl:attribute>
                    <xsl:call-template name="healthTableTmpl">
                      <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                      <xsl:with-param name="answerId" select="'HEALTH13b'"/>
                      <xsl:with-param name="isAnswerIdDataSuffixNumber" select="'boolean(1)'"/>
                    </xsl:call-template>
                  </td>
                </tr>
                <tr>
                  <td class="tdOneCol">
                    <xsl:attribute name="colspan">
                      <xsl:value-of select="$colspan"/>
                    </xsl:attribute>

                    <p class="tdAns">
                      <span lang="EN-HK" class="tdAns"><br/></span>
                    </p>
                  </td>
                </tr>
              </xsl:if>
            </xsl:if>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH14'
              or /root/showQuestions/insurability/question = 'HEALTH15'
              or /root/showQuestions/insurability/question = 'HEALTH16'
              or /root/showQuestions/insurability/question = 'HEALTH17'">
            <tr>
              <td
                style="border: none; border-left: solid windowtext 1.0pt; background: #F2F2F2; padding: 0in 5.4pt 0in 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="question">
                  <span lang="EN-HK" class="questionBold">6.
                    For Juvenile Applicants only (For 0 - 6 months)</span>
                </p>
              </td>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH14'">
            <tr>
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'(a)
                    Was the child born premature or pre-term (before 37 weeks gestation)?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH14'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH14a'">
            <tr>
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Gestational Week:'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH14a'"/>
                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH15'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'(b)
                    Was the Life Assured’s birth weight below 2.5kg? If yes, please
                    state birth weight.'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH15'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH15a'">
            <tr>
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Birth Weight (kgs):'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH15a'"/>
                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH16'">
            <tr>
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'(c)
                    Was the duration of hospital stay after birth is more than 3 days?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH16'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH16a'">
            <tr>
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Duration of Hospital Stay (days):'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH16a'"/>
                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH17'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'(d)
                    Has the child ever suffered from, or currently suffering from,
                    or being followed up or investigated for any residual
                    birth/delivery complications, congenital disorder/birth defect,
                    physical impairment, mental retardation, G6PD deficiency,
                    cerebral palsy, Down’s Syndrome, prolonged jaundice,
                    respiratory distress syndrome or any other serious disorder?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH17'"/>
              </xsl:call-template>
            </tr>

            <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH17_DATA'">
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <xsl:call-template name="healthTableTmpl">
                    <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                    <xsl:with-param name="answerId" select="'HEALTH17'"/>
                  </xsl:call-template>
                </td>
              </tr>
            </xsl:if>

          </xsl:if>

          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH_GIO01'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'1.
                    During the last 3 years, have you ever been hospitalized
                    or have you consulted a medical practitioner for any medical
                    condition that required medical treatment for over 14
                    consecutive days, or are you intending to do so, or have you
                    had or been advised to have any operation, test or treatment?*'"/>
                <xsl:with-param name="extraQuestion" select="'*Consultations,
                    tests or treatment for the following conditions can be
                    ignored: common cold, fever or flu; uncomplicated pregnancy or
                    caesarean sections; contraception, inoculations, minor joint
                    or muscle injuries or uncomplicated bone fractures from which
                    you have fully recovered.'"/>
                <xsl:with-param name="isItalicExtraQuestion" select="boolean(1)"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH_GIO01'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH_GIO01_DATA'">
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <xsl:call-template name="healthTableTmpl">
                    <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                    <xsl:with-param name="answerId" select="'HEALTH_GIO01'"/>
                  </xsl:call-template>
                </td>
              </tr>
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>

                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH_GIO02'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'2.
                    Have you ever had or been told that you have, or have been treated
                    for cancer (including carcinoma-in-situ), growth or tumor of
                    any kind, diabetes, high blood pressure, chest pain, stroke,
                    heart diseases, blood disorder, respiratory diseases, kidney
                    diseases, bowel diseases, hepatitis or liver diseases, nervous
                    or mental disorders, spinal disorders, muscular or joint
                    disorders, AIDS or HIV related conditions, or any other serious
                    illness or impairment?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH_GIO02'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH_GIO02_DATA'">
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <xsl:call-template name="healthTableTmpl">
                    <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                    <xsl:with-param name="answerId" select="'HEALTH_GIO02'"/>
                  </xsl:call-template>
                </td>
              </tr>
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>

                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH_GIO03'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'1.
                    Do you have or ever had any disease, infirmity, illness or physical defect,
                    and /or conditions affecting mobility, sight and / or hearing?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH_GIO03'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH_GIO03_DATA'">
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <xsl:call-template name="healthTableTmpl">
                    <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                    <xsl:with-param name="answerId" select="'HEALTH_GIO03'"/>
                  </xsl:call-template>
                </td>
              </tr>
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>

                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH14'
              or /root/showQuestions/insurability/question = 'HEALTH15'
              or /root/showQuestions/insurability/question = 'HEALTH16'
              or /root/showQuestions/insurability/question = 'HEALTH17'">
            <tr>
              <td
                style="border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="question">
                  <span lang="EN-HK" class="question">
                    <u><b>Important:</b> If ‘Yes’ for any of the above child questions, please submit latest Child Health Booklet including all assessments done to date.</u>
                  </span>
                </p>
              </td>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'insAllNoInd' and (count(/root/proposer/insurability/insAllNoInd[. = 'Yes']) > 0 or count(/root/insured/insurability/insAllNoInd[. = 'Yes']) > 0)">
            <tr>
              <td style="border: solid windowtext 1.0pt; border-top: none">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <table>
                  <tr>
                    <td
                      style="padding: 0in 5.4pt 0in 5.4pt; border: none">
                      <xsl:for-each select="/root/insured">
                        <xsl:if test="position() = 1 and insurability/insAllNoInd[. = 'Yes']">
                          <p class="question">
                            <span class="tdAns">
                              <xsl:copy-of select="$tick"/>
                              I confirm the answer to all of the above medical and health questions for
                              <b><u>life assured</u></b> is &quot;No&quot;
                            </span>
                          </p>
                        </xsl:if>
                      </xsl:for-each>
                      <xsl:if test="/root/proposer/insurability/insAllNoInd[. = 'Yes']">
                        <p class="question">
                          <span class="tdAns">
                            <xsl:copy-of select="$tick"/>
                            <xsl:choose>
                              <xsl:when test="$lifeAssuredIsProposer = 'true'">
                                I confirm the answer to all of the above medical and health questions for
                                <b><u>life assured</u></b> is &quot;No&quot;
                              </xsl:when>
                              <xsl:otherwise>
                                I confirm the answer to all of the above medical and health questions for
                                <b><u>proposer</u></b> is &quot;No&quot;
                              </xsl:otherwise>
                            </xsl:choose>
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE01'">
                        <p class="question">
                          <span class="warningHealth">
                            Note: The above declaration doesn’t apply to smoking question,
                            which is autopopulated from Client Profile
                          </span>
                        </p>
                      </xsl:if>
                    </td>
                  </tr>
                </table>
                <p class="tdAns"></p>
              </td>
            </tr>
          </xsl:if>
        </table>
      </xsl:if>
    </div>
    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
