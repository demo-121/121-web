<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="appform_pdf_planDetails">
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">PLAN DETAILS</span>
    </p>

    <table class="dataGroup">
      <col width="12%"/>
      <col width="32.2%"/>
      <col width="21%"/>
      <col width="17.4%"/>
      <col width="17.4%"/>
      <thead>
        <xsl:for-each select="/root/planDetails/planList[covCode = /root/baseProductCode]">
          <tr>
            <xsl:variable name="basicPlanName">
              <xsl:choose>
                <xsl:when test="/root/lang = 'en'">
                  <xsl:value-of select="./covName/en"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="./covName/zh-Hant"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="$basicPlanName"/>
              <xsl:with-param name="colspan" select="5"/>
            </xsl:call-template>
          </tr>
        </xsl:for-each>
      </thead>
      <tbody>
        <tr class="dataGroup">
          <td class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic"><br/></span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Plan / Rider Name</span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Sum Assured/ Benefits</span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Policy Term</span>
            </p>
          </td>
          <td class="tdLast padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Premium Term</span>
            </p>
          </td>
        </tr>
        <tr class="dataGroup">
          <td class="tdFirst padding">
            <p class="title noPadding">
              <span lang="EN-HK" class="questionItalic">Basic Plan</span>
            </p>
          </td>
          <xsl:for-each select="/root/planDetails/planList[covCode = /root/baseProductCode]">
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="/root/lang = 'en'">
                      <xsl:value-of select="./covName/en"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./covName/zh-Hant"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./sumInsured"/>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./polTermDesc"/>
                </span>
              </p>
            </td>
            <td class="tdLast padding">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./premTermDesc"/>
                </span>
              </p>
            </td>
          </xsl:for-each>
        </tr>
        <xsl:for-each select="/root/planDetails/planList[not(covCode = /root/baseProductCode)]">
          <tr class="dataGroup">
            <xsl:attribute name="class">
              <xsl:choose>
                <xsl:when test="position() mod 2 = 0">
                  <xsl:value-of select="'even'"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="'odd'"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <td class="tdFirst padding">
              <p class="title noPadding">
                <span lang="EN-HK" class="questionItalic">
                  <xsl:choose>
                    <xsl:when test="position() = 1">Rider</xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="/root/lang = 'en'">
                      <xsl:value-of select="./covName/en"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./covName/zh-Hant"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="./saViewInd = 'Yes'">-</xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./sumInsured"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./polTermDesc"/>
                </span>
              </p>
            </td>
            <td class="tdLast padding">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./premTermDesc"/>
                </span>
              </p>
            </td>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>

    <table class="dataGroupLast">
      <thead>
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'Other Plan Details'"/>
            <xsl:with-param name="colspan" select="8"/>
          </xsl:call-template>
        </tr>
      </thead>
      <tbody>
        <col width="25%"/>
        <col width="25%"/>
        <col width="25%"/>
        <col width="25%"/>
        <tr class="dataGroup">
          <td colspan="2" class="tdFirst padding">
            <p class="title">
                <span lang="EN-HK" class="questionItalic">Policy Currency</span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/ccy"/>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="title">
                <span lang="EN-HK" class="questionItalic">Backdating</span>
            </p>
          </td>
          <td colspan="2" class="tdLast padding">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/isBackDate"/>
              </span>
            </p>
          </td>
        </tr>
        <tr class="dataGroup">
          <td colspan="2" class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Payment Mode</span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/paymentMode"/>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">
                <xsl:choose>
                  <xsl:when test="not(/root/planDetails/isBackDate = 'No')">Selected Commencement Date (dd/mm/yyyy)</xsl:when>
                  <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdLast padding">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:choose>
                  <xsl:when test="not(/root/planDetails/isBackDate = 'No')">
                    <xsl:value-of select="/root/planDetails/riskCommenDate"/>
                  </xsl:when>
                  <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
        <tr class="dataGroup">
          <td colspan="2" class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Discount <span style="color:#ec4d33;font-size: 7.0pt;">(*)</span></span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/hasBoughtAxaPlan"/>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Total Premium Amount (including discount, if any)</span>
            </p>
          </td>
          <td colspan="2" class="tdLast padding">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                 <xsl:value-of select="/root/planDetails/totYearPrem"/>
              </span>
            </p>
          </td>
        </tr>
        <tr>
          <td colspan="8" class="tdFirst padding" style="border-bottom: solid windowtext 1.0pt;">
            <p class="statement">
              <span lang="EN-HK" class="warningPolicies">(*) The discounted premium rate for 
              Early Stage CritiCare is offered only with purchase of a regular/single premium 
              basic policy from AXA Insurance Pte Ltd in the past 12 months. For concurrent purchase, 
              Early Stage CritiCare will not be offered at a discounted rate if the application for 
              regular/single premium basic policy is rejected.</span>
            </p>
          </td>
        </tr>  
      </tbody>
    </table>

    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
  </div>
</xsl:template>

</xsl:stylesheet>
