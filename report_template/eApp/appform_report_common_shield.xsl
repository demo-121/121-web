<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="space">
  <xsl:value-of select="'&#160;'"/>
</xsl:variable>


<xsl:variable name="checkboxTick">
  <span style="margin-right: 6px;">
    <img class="checkboxIcon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABOklEQVRYR+2W7W3CMBCGn0xAN2iZgHaDMkFHgA0QE0AnKCN0k9IJ2m4AG7AB6JUSyVhHc8KX5E/8K7Lse5/78OUqBl7VwPqMALcisAPegKegFB2AT+A9t2cBSHwVJJybEcA23bQATsAEeAF+g0CegR9AkZi2AZzrA9EFatq1REaAiAg8AB911SvvWr2lQOJfgArvG3jtEyAV/6vF9ap6icB/4kUATT7XQONN3h7axIsA1EIXdVOaGxAe8SIACeyBmQHhFS8C0GULQvtNtecFZ3Xw4meYQ0hET80jXhyBxqMUQnte8TCANB36VpO59TLyNBSnIDWoSGh5xUMjYBWYZy80Ah7BTlPQKUCXI9kxH3StiUhD4+YeFx13XEOp7AhiCTw6jHqOyHP9U64mYl2MHjw9MFdnRoDBI3ABXmlgIXxZe+kAAAAASUVORK5CYII="/>
  </span>
</xsl:variable>


<xsl:variable name="checkboxUntick">
  <span style="margin-right: 6px;">
    <img class="checkboxIcon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAqUlEQVRYR+2WwQ2CUBAFhwosQexAKrEFO7AEtQNLoCMsQTuwA81LOMjPEoNZ8i+PI4Hd+QOHaah8NZX3Y4A5AzfgALRJn+gB9MC1nBcBaPkpaXE5RgCX75sRwAvYAB1wTwLZAwMgE7tfAO/xgewfNJwbLTGADdiADdiADdiADVQ3sGaSPcvQjYpI0XhOasG/olQvCeIIbJNAdHJl+aSINTs7PBfzGqC6gQ8cPDYhnSgMLQAAAABJRU5ErkJggg=="/>
  </span>
</xsl:variable>


<xsl:template name="infoSectionHeaderNoneTmplDiv">
  <xsl:param name="sectionHeader" />

  <div class="sectionHeader">
    <div class="title">
      <p class="header">
        <span lang="EN-HK" class="sectionHeader">
          <xsl:value-of select="$sectionHeader"/>
        </span>
      </p>
    </div>
  </div>
</xsl:template>


<xsl:template name="comSecHeaderCols01">
  <xsl:param  name="sectionHeader" />

  <div class="sectionHeader">
    <div class="title">
      <p class="header">
        <span lang="EN-HK" class="sectionHeader">
          <xsl:value-of select="$sectionHeader"/>
        </span>
      </p>
    </div>
  </div>
</xsl:template>


<xsl:template name="comSecHeaderCols02WideCol01">
  <xsl:param  name="sectionHeader" />
  <xsl:param  name="sectionSubHeader"/>
  <xsl:param  name="col02Text" />
  <xsl:param  name="col02SubText"/>

  <td class="headerRow"
    style="border: solid windowtext 1.0pt; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$sectionHeader"/>
      </span>
      <xsl:if test="string-length($sectionSubHeader) > 0">
        <span lang="EN-HK" class="sectionHeader normal">
          <xsl:value-of select="$sectionSubHeader"/>
        </span>
      </xsl:if>
    </p>
  </td>

  <td width="99" class="headerRow"
    style="border: solid windowtext 1.0pt; border-left: none;  padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$col02Text"/>
      </span>
      <xsl:if test="string-length($col02SubText) > 0">
        <br/>
        <span lang="EN-HK" class="statement">
          <xsl:value-of select="$col02SubText"/>
        </span>
      </xsl:if>
    </p>
  </td>
</xsl:template>


<xsl:template name="comSecTableRowCols02WideCol01">
  <xsl:param  name="col01Text"/>
  <xsl:param  name="col02Text"/>
  <xsl:param  name="col01Align"/>
  <xsl:param  name="col01PaddingLeft"/>
  <xsl:param  name="col01IsBold"/>
  <xsl:param  name="col01IsRed"/>
  <xsl:param  name="col01WithCheckbox"/>

  <xsl:param  name="col02WithCheckbox"/>
  <xsl:param  name="col02CheckboxVal"/>
  <xsl:param  name="isHideCol02Text"/>



  <!--
  col01Align - optional
  0   = align left; default; if null; will always align left
  1   = align center;
  2   = align right

  col01PaddingLeft - optional

  col01IsRed      - optional
  col01IsBold     - optional
  isHideCol02Text - optional
  -->

  <td style="padding-left: 7.2pt;">

    <xsl:variable name="styleTextAlign">
      <xsl:choose>
        <xsl:when test="not($col01Align)">
          <xsl:value-of select="'text-align: left;'"/>
        </xsl:when>

        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$col01Align = '1'">
              <xsl:value-of select="'text-align: center;'"/>
            </xsl:when>

            <xsl:otherwise>
              <xsl:value-of select="'text-align: right;'"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="stylePaddingLeft">
      <xsl:choose>
        <xsl:when test="string-length($col01PaddingLeft) > 0">
          <xsl:value-of select="concat('padding-left: ', $col01PaddingLeft, ';')"/>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <p class="normal">
      <xsl:attribute name="style">
        <xsl:value-of select="concat($styleTextAlign, $stylePaddingLeft)"/>
      </xsl:attribute>

      <xsl:if test="$col01WithCheckbox = 1">
        <xsl:call-template  name  ="comCheckbox">
          <xsl:with-param   name  ="isChecked"
                            select="1"/>
        </xsl:call-template>
      </xsl:if>

      <span lang="EN-HK">
        <xsl:attribute name="class">
          <xsl:value-of select="'question '"/>
          <xsl:if test="$col01IsBold"><xsl:value-of select="'bold '"/></xsl:if>
          <xsl:if test="$col01IsRed"><xsl:value-of select="'red '"/></xsl:if>
        </xsl:attribute>

        <xsl:value-of select="$col01Text"/>
      </span>
    </p>
  </td>

  <td width="99" class="tdLast padding">
    <xsl:if test="$col02WithCheckbox = 1">
      <xsl:call-template  name  ="comCheckbox">
        <xsl:with-param   name  ="isChecked"
                          select="$col02CheckboxVal"/>
      </xsl:call-template>
    </xsl:if>

    <p class = "userData">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="not($isHideCol02Text = 'true') and string-length($col02Text) = 0">-</xsl:when>
          <xsl:otherwise><xsl:value-of select="$col02Text"/></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
</xsl:template>


<xsl:template name="comSecHeaderCols03WideCol01">
  <xsl:param  name="sectionHeader" />
  <xsl:param  name="sectionSubHeader"/>
  <xsl:param  name="col02Text" />
  <xsl:param  name="col03Text" />

  <td class="headerRow"
    style="border: solid windowtext 1.0pt; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$sectionHeader"/>
      </span>
      <xsl:if test="string-length($sectionSubHeader) > 0">
        <span lang="EN-HK" class="sectionHeader normal">
          <xsl:value-of select="$sectionSubHeader"/>
        </span>
      </xsl:if>
    </p>
  </td>

  <td width="99" class="headerRow"
    style="border: solid windowtext 1.0pt; border-left: none;  padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$col02Text"/>
      </span>
    </p>
  </td>

  <td width="99" class="headerRow"
    style="border: solid windowtext 1.0pt; border-left: none;  padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$col03Text"/>
      </span>
    </p>
  </td>

</xsl:template>


<xsl:template name="comSecTableRowCols03WideCol01">
  <xsl:param  name="col01Text"/>
  <xsl:param  name="col02Text"/>
  <xsl:param  name="col03Text"/>
  <xsl:param  name="col01Align"/>
  <xsl:param  name="col01PaddingLeft"/>
  <xsl:param  name="col01IsBold"/>
  <xsl:param  name="col01IsRed"/>
  <xsl:param  name="col01WithCheckbox"/>

  <xsl:param  name="col02WithCheckbox"/>
  <xsl:param  name="col02CheckboxVal"/>
  <xsl:param  name="isHideCol02Text"/>

  <xsl:param  name="col03WithCheckbox"/>
  <xsl:param  name="col03CheckboxVal"/>


  <!--
  col01Align - optional
  0   = align left; default; if null; will always align left
  1   = align center;
  2   = align right

  col01PaddingLeft - optional

  col01IsRed - optional
  col01IsBold - optional
  isHideCol02Text - optional
  -->

  <td style="padding-left: 7.2pt;">

    <xsl:variable name="styleTextAlign">
      <xsl:choose>
        <xsl:when test="not($col01Align)">
          <xsl:value-of select="'text-align: left;'"/>
        </xsl:when>

        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$col01Align = '1'">
              <xsl:value-of select="'text-align: center;'"/>
            </xsl:when>

            <xsl:otherwise>
              <xsl:value-of select="'text-align: right;'"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="stylePaddingLeft">
      <xsl:choose>
        <xsl:when test="string-length($col01PaddingLeft) > 0">
          <xsl:value-of select="concat('padding-left: ', $col01PaddingLeft, ';')"/>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <p class="normal">
      <xsl:attribute name="style">
        <xsl:value-of select="concat($styleTextAlign, $stylePaddingLeft)"/>
      </xsl:attribute>

      <xsl:if test="$col01WithCheckbox = 1">
        <xsl:call-template  name  ="comCheckbox">
          <xsl:with-param   name  ="isChecked"
                            select="1"/>
        </xsl:call-template>
      </xsl:if>

      <span lang="EN-HK">
        <xsl:attribute name="class">
          <xsl:value-of select="'question '"/>
          <xsl:if test="$col01IsBold"><xsl:value-of select="'bold '"/></xsl:if>
          <xsl:if test="$col01IsRed"><xsl:value-of select="'red '"/></xsl:if>
        </xsl:attribute>

        <xsl:value-of select="$col01Text"/>
      </span>
    </p>
  </td>

  <td width="99" class="" style = "padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <xsl:if test="$col02WithCheckbox = 1">
      <xsl:call-template  name  ="comCheckbox">
        <xsl:with-param   name  ="isChecked"
                          select="$col02CheckboxVal"/>
      </xsl:call-template>
    </xsl:if>

    <p class = "userData">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="not($isHideCol02Text = 'true') and string-length($col02Text) = 0">-</xsl:when>
          <xsl:otherwise><xsl:value-of select="$col02Text"/></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>

  <td width="99" class="tdLast padding">
    <xsl:if test="$col03WithCheckbox = 1">
      <xsl:call-template  name  ="comCheckbox">
        <xsl:with-param   name  ="isChecked"
                          select="$col03CheckboxVal"/>
      </xsl:call-template>
    </xsl:if>

    <p class = "userData">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="string-length($col03Text) = 0">-</xsl:when>
          <xsl:otherwise><xsl:value-of select="$col03Text"/></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
</xsl:template>


<xsl:template name="comCheckbox">
  <xsl:param  name="isChecked" />
  <!--
  col01Align - optional
  0   = not checked
  1   = checked;
  -->
  <xsl:choose>
    <xsl:when test="$isChecked = '1'">
      <xsl:copy-of select="$checkboxTick"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:copy-of select="$checkboxUntick"/>
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>


<xsl:template name="comCheckboxWithText">
  <xsl:param  name="text" />
  <tr>
    <td style = "text-align:left; border:none">
      <p class="userData">
        <xsl:call-template  name  ="comCheckbox">
          <xsl:with-param   name  ="isChecked"
                            select="1"/>
        </xsl:call-template>

        <span lang="EN-HK" class="tdAns">
          <xsl:value-of select="$text"/>
        </span>
      </p>
    </td>
  </tr>

</xsl:template>


<xsl:template name="comSecHeaderNumCols01">
  <xsl:param  name="sectionHeader" />
  <xsl:param  name="colspan" />

  <td width="714" class="headerRow"
    style="border: solid windowtext 1.0pt; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <xsl:if test="$colspan">
      <xsl:attribute name="colspan">
        <xsl:value-of select="$colspan"/>
      </xsl:attribute>
    </xsl:if>
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$sectionHeader"/>
      </span>
    </p>
  </td>
</xsl:template>

<xsl:template name="comSecHeaderNumCols01S">
  <xsl:param  name="sectionHeaderPart1" />
  <xsl:param  name="sectionHeaderPart2" />
  <xsl:param  name="sectionHeaderPart3" />
  <xsl:param  name="colspan" />

  <td width="714" class="headerRow"
    style="border: solid windowtext 1.0pt; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <xsl:if test="$colspan">
      <xsl:attribute name="colspan">
        <xsl:value-of select="$colspan"/>
      </xsl:attribute>
    </xsl:if>
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$sectionHeaderPart1"/>
      </span>
      <span lang="EN-HK" class="sectionHeader normal">
        <xsl:value-of select="$sectionHeaderPart2"/>
      </span>
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$sectionHeaderPart3"/>
      </span>
    </p>
  </td>
</xsl:template>


<xsl:template name="appform_shield_ph_header">
  <xsl:param name="isAwt" />


  <table  style= "width:740px; border-collapse: collapse;">
    <colgroup>
      <col style = "width:75%"></col>
      <col style = "width:25%"></col>
    </colgroup>

    <tr>
      <td>
        <div>
          <p class="img">
            <img class="companyIcon" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAKcApwMBEQACEQEDEQH/xAGiAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgsQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+gEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoLEQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP7+KACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD4A/4Kxf8AKLL/AIKWf9mAftkf+s6/EagD7/oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAPgD/AIKxf8osv+Cln/ZgH7ZH/rOvxGoA+/6ACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD4A/4Kxf8osv+Cln/ZgH7ZH/AKzr8RqAPv8AoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAPgD/grF/yiy/4KWf8AZgH7ZH/rOvxGoA+/6ACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD4A/4Kxf8AKLL/AIKWf9mAftkf+s6/EagD7/oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAPgD/AIKxf8osv+Cln/ZgH7ZH/rOvxGoA+/6ACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD4A/4Kxf8osv+Cln/ZgH7ZH/AKzr8RqAPv8AoAKACgBjyRxKXkdY0UMzM7BVCopdmLHAAVVLMScBQSeAaBpNtJJtt2SWrbeyS7mf/bWj/wDQV07/AMDbb/45Qaewr/8APmr/AOC5/wCQf21o/wD0FdO/8Dbb/wCOUB7Cv/z5q/8Aguf+Qf21o/8A0FdO/wDA22/+OUB7Cv8A8+av/guf+RegnguYxLbzRTxMSFkhdZI2IODh0JU4PBweDx1oM5RlF8souLW6kmmvVPUloEB45PQUAYLeKvDCMyP4i0NGUkMratYKykcEMDOCCD1BGRU80f5o/ejqWBxrSaweKaeqaw9VprumoWNmCeG5ijuLaaKeCVd8U0LrLFIp6MkiFkdT2Kkg+tUc8oyhJxnGUZRdpRknGSa3TTs0/JktBIhIGMkDJAGe5PQfU0ALQAUAJuXcFyNxBYLnkqCASB3ALKCe2RnqKAFoAKACgD4A/wCCsX/KLL/gpZ/2YB+2R/6zr8RqAPv+gAoAKAPy0/4LP+P/ABJ8M/8AgnH+0N4t8H+INY8LeKLTS/DVno2vaDf3Gl6tp8+p+MNBsLl7TULOSK6tXl06e8tzLbyxygS4VhnnnxU3DD1Wm07JJp2teS6+e3nt1P3v6MWS4DP/ABv4Hy3NMDhcxy+pi8dVxWCxtGGIw1aGHyvGVqftaNWMqc1CvClNKcXG8Vofxu/8E0Ph3+2h/wAFF/i14q+GWh/tl/F7wBH4S8JDxTfavqPxF8f6gJUfU7bTYbSC2ttYyXY3DTO7vGqRwsF3yMkbeVh41sRJx9rKNle/NLulbf8Arsf6b+PGe+Fvgjw7lme4nwt4azqWaZn/AGdSw1DJcnocjWHqV5VZznhdlyKCSTblJbJSa/cv/hxB+3H/ANJNviP/AOFP8Tv/AJdV2fU63/P+X/gUj+T/APibnwo/6MLkP/hDkX/zIKP+CD/7cWRu/wCCm3xI25G7Hif4nZ255x/xOhzjpyKPqdb/AJ/y/wDApA/pc+FNnbwFyC9na+CyO1+l/wDY3p8j+ij9mb4L3P7P3wR8AfCe/wDGXiH4hap4S0K0sNY8a+KdQv8AU9c8S6sqb9S1e9utSu769ze3jzTQW0t5cLY2zQ2UD+RbxKO6nDkhGN27LVt3bfV6/wBLY/ibjbiWPF/FOdcR08rwOS0c0xtXEYfKsuo0aGDy/Dt2oYWlChSo037GkowlVVKDrTUqs4885X95qz5Y+Bv+Ckv7Xug/sY/sr/ET4rahcL/b6aTPpnhHTflEuqeIdRC2OmWkbO8YRZr+5tYrmVHa5s7KS61O3trs2DwNjiKqpU5SevRK9m210+dl31uk7WP1vwT8OMZ4oeIOR8NUIP6rUxMK+Y17Nxw+BofvsTVaUZNuFCnUnTi0oVasadCdSn7aMj/OX1L9sT9tjWbTWPHL/Hb46x6Dca61re6pYeOPF9n4etNX1QXV9FpcX2XUI9Ps3khhuZLaxhESrBbyeVEscRx4Xta9ubnna+95L9bbdOx/tvh/DPwnwlTC5LHhHhB4yGCVSjha2VZbUx1XCYf2dCWJkqlGVarGMp041K0nK85x5pOUlf8AuY/4ILfthal+1D+x5pujeM9XvNX+I3wt1O68JeI9Q1LUDfXurR2aW0tjq0plZ7xZbzT7yxlvpbqSc3uqvqN5HMBM1ra+vgqqqUkm25R0d3e/nvf18z/Jf6XPhrQ8PvE/FVssw9LDZJxDRhmmX0KFD2NLDe2dRVcLFQtSkqValXjSVNQ9lho0KU4XiqlX9ya7D+VT8r/+C0nxB8V/DD/gnF+0P4v8D+J9Y8HeKrDTPCMWj+IfD+qXui63YTXvxC8I2dw2m6np89te2s0tjPdW8jQTIz2800bHY7Vz4qUo4epKLaaUdU7NXkl+TfyP3z6MWSZbxB438D5XnGX4bNMtrYrMZ4rBY3DUsXhK0aOTZjVprEUK8Z0qkIVoU6iU4tKpCDS5kj+O7/gmVo/7aH/BQ/46ax8Ho/22v2gPAdtovgvUfFt1rUfxJ8c6pKy2N7p1lHaxW0nia0jJkk1BXZjOpSONyiyNhD5VD21eXKqs46Nt80nbVefr1P8ATHx7xXhb4KcI4XiV+E/Buc1MZmtDLKWEeR5Tho3q0q1V1HUjgKr91UWrcmrau4rU/fOT/gh7+2GEcw/8FQ/2ijKFJjD+I/GgQvj5QxHjjIXOMkZIHOD0ru+p1v8AoKn82/8AM/j5fSv8NW1zfR74HUb6uOEytu3WyeUK7Pxw/bJT/gqv/wAEkPiZ4S8QXv7UvxH8d+DPEd1OfDPifWPFGteL/DepXVtv+1aD4k8NeK7vVLBbx7dftESyW91aug+0afefbrKcWfJUeKwsk/ayafm2nto4u69PLZ7o/pbwxX0dPpKZFmeAj4d5Hk2bYCMXj8BhMBhssx9CnN2p43A5hltPDV/Zqp7kuWpTqJ+5WpujVg6v9S3/AASD/wCCix/4KGfs8XninxPplnoXxX+HWrQ+FfiLpdgWGn3d1JZw3Wl+JdLjdneHTteiNyFtHkaSzvtPv4NothZyz+lhq6r0+baa0kunk12T7d7+i/gD6R/go/BTjeGVYLEVsbw5nWFlmWQ4rENSr06Ua0qWIwGJnp7Svg5+zftFFRqUa1GV/aOpGH6z10H8+BQB8Af8FYv+UWX/AAUs/wCzAP2yP/WdfiNQB9/0AFABQB+LX/BwBqX2D/gmN8bYw6qb/Vfh7aMD1ZH8c6C/y++6NenJXcOmSOPHf7vP1j+Z/T/0PKPtvHvhFtNqlRzqr5XWT41a/wDgTtfrZ7pH4Qf8Gsenif8AaJ/aKvyhP2X4UaOgcA/K0nimyO0t0w+OR1JUds1x5b/En/h/VH9cftBa3LwdwNRv/E4jxUrXtfly6vq+9uz2buf3CV7B/lQFABQBHLLHDFJNK6xxxI0kkjkKiIoJZmY4AVQMkkgAcmgaTbSSbbaSSV229Eklq23oktz+Ar/gvN+2VrX7XP7WWi/syfC+4uNZ8KfDHX7Xw1Bp2mz/AGr/AISH4lapIum/ZfItiYGn0E3k2lCFvMu7XVL7Wba4lHlrb2/i4yq6tVUobRe1/tW19LdV0d9dj/YL6IHhjhfDjw7xviFxDGGFzDP8FPG+3rQcPqWQYeP1h1eaoudRxns4V3OPLRq4ahhKtOL5pVKn7JeKv+CTeh+Fv+CNOq/A2LSbW6+K9r4Th+KF7q2l6e5urr4h2EP9v3Eo82C3uryO6v0bQ4dRubWPUYPB8ttafZUkso4K6nhrYTl+0lzd9f110v8Ay/cfy/gPpFY3H/Sdw3GUsTUp8PVc0lkFHDYmtD2dHIqs5YKEG4ynTpulh5/XJYeFWWHnm6nVVVqq5v8AD3/g3s/amb9n/wDbOm+FXiTU30vwv8bNObQriC5CrZx+LvDS311p7XruRJbBdIuPEkMJiV/O1Q6ZbzxiFmuLfjwNX2dXle0v019dr+rsf1n9NPw+/wBcfDGjxLgMOsRmHCeI+tRlC/tZZXmDo0q6pRs1Uf1qGAlJTcfZ4b6zOEudKnU/0KFIYBh0YAj6EZFe4f40n4t/8HAV4tn/AMEwfjl033OpfDyzTJxxN4+8OlsDoxwg69Oo6Vy43TDVPWH/AKUj+n/odUvaeP8Awfq7U6GeVLWbT5cmxlr6WVr6N2P56f8Ag18sVm/a++LF8etl8HrxB7/adf0VcdP9jcSSMbR1OK4Mu/jN/wDTuX380P8Ag6fM/s/6flbl8N+GaP8Az94opya8qeDxTvr/AHmlprr6n93ZIHUgfWvZP8kT+TT/AIOhfjd8Pz8IPg38DrXUdLv/AB/efEAeNp7K3mgn1HStB0jQ9Y0p5bqNGM9rb315rEKWrughu5LS9SJ2ksp1TzMxnHlhDeV3LdaKzX4t/g7Xs7f6HfQE4WziXFXFPFksPiKWTUMk/smNecJwoYjG4rGYXEKFOUkoVJ0qWFm6kYtypxq0ZSShWpuXd/8ABsH8BfG/gT4A/GT4yeJrC50rQPi94s0GDwbBeQzRy6ppngq01W0u9cgWQIi2FzqGuXNhbyoHe4m0y63bIoojNWXU5RhKbVlJpLu7X19NbX8n8/I+ntxhlOc8ccMcL5fWhiMZwvleMnmlSlKEoUMRm1XDVKWDnKLbdanQwlOvOLsqcMRTteU5KP8AUhXon8EBQB8Af8FYv+UWX/BSz/swD9sj/wBZ1+I1AH3/AEAFABQB+DX/AAca6gtl/wAE2PGUBcq2pfET4b2caggF2Gum7YEEglAlq7HAb5ggIGQa48f/ALu/8S/ry/pdT+tvoT0fa+O2UysmqGR57Vb/AJU8J7NNab81RJbaOWvR/wAoH/BI+4/4KIab45+J2p/8E/7Wwm18eHdOs/Hj6pN4MhsRpct+sunxH/hMyLOS4a6iLxR2ga68tJ3I8lZDXmYR105Oja9kpXUXdXuvi8/5dd76XP8ARH6SUPBSvlXDuH8ZKuIp4R46vVyZYSOaSrPERoSjXk/7K/eKn7N8rda1PmlBX5nFP94f+Ev/AODlL/oHeCf/AAI+A/8A8TXbz5h3j/4DS/yP5G/sz6DH/P8Azr/wDi7/AOTPoD9l3UP+C8niX45eBNN/aK1Xwn4W+D39qLc+NL2xtfhXqV7eaZbDzG0q1XwtbvqdnLqLAW51ETWsVjGz3BkmkjjtLioSxrnFTlFRvraEPu91XXr0PifEOl9EXAcJZtV4Gw+a5hxRKh7PKqVerxFh6NKvUaj9Zqyx9T6vUjh4t1Fh3CpKvJRp2hFzq0/6QVDBVDNvYAAsQFLHucLgDPoOK9A/ic/Ln/grh+2vpn7Fn7JfjXxZa3lh/wAJ94rs7nwp4D0u6d3kvde1W3lhtT9kjwbi2hHm3l8k0tvbyaXY6kgma5FvbXHNiqvsqUnpzPRJ7N9n6/lc/ePo7eFuI8U/EfJ8qnSr/wBkYGvDMc4xFKNoUcFhpqdXmqtNU5z92lQlGM5xxVbDXioOU4/wh/8ABPbxB488L/tOaF+07q37PvxS/aS0fwrruuatrR8I+HNV8QTjxnq1tPPb6vcXsVjc2U2o2t5efb5La4ntbmRJmmtruwvPs17B49ByVT2rhKok3sr6927NX1Ts35n+ufjVgMmzLgHF+H2F404e4ExOYYTB4fCxzLHYfBReU4WpCE8NCi60K0aFSnSVFTjCpSTioVaVek6lGf8AX18OP+Cu3xb+Nni/QfhCP2GPj94Sg8cyt4cfXvFnw7udB8N6PaXVvJHcXep6lf8Ai6yht7W2tVkfZDM99OypbadbXd9Lb20vpRxU5vldKSvpdrReuu3/AAyTbSf+bGf/AEb+G+EsqxfEn/EW+DMyllMY4yOCy7OY4rH4qpTnGUKWGoUcurSnUnO2s4KjBXqV6lOjGc4/yW/8FAfhj4z/AGFf+CiviLXtHs10S7sfHmifG7wSYo4/7PD6hqaa1qmn28UOyM6Zp/iq21/QFtiIxNp9ohVWtbiGWXzq8XRxF0uqkr7Xe666KV16I/0W8Gc+yrxe8DsHgMXVeLp1cnxnCObc0n7e1HDvC4avUlNyf1mtltTBY11U5cuIqu7VWEox/wBE39kf426P+0R+zr8KPi5otxDcWvi7wdoWoyNDfw6kEuptLtJriKW6gYqbuF5fKvreQR3VjfJc2N7DBeW08Mfu05qcIyXVJ/hf7+/Z6H+K/iDwviuDOMuIuG8ZCUK2V5pjMNaVKdG8KeIqQhKNOaT9lJR5qNSPNSrUXCtRnOlUhJ/lZ/wcZ6r9g/4Js+MLL5wdX+IXw+tdy42lYNbju2STI+6xhVxgg+ZGnVd2OXH/AO7vfWUevnfb5fL5n9C/Qnw7reOuU1VytYbJc8qO71XPg3SvHXf3raprllLZ2PxB/wCDZyy8QW3xD/ax8WeFbBdT8RaT8L9A0fRbJ3EazalrWo6he2w3Kjy/K2glyY1chVZRHK8iLXHl9+ao1uo/m9+u1j+qfp51sJPJfDjLsdWdDB4nP8dicTWjHmcKOFw9GnN6tR1+tpJStFuzcopNn378RfD/APwcYa/aa/HpXiLwPY2Wqz30lrpGgjwLG2nWc7yyQ6fa3uv6WztBBCy20b6lqU80iKpuriZjJI27WPs9YrfZpv0V7n47kuM+hJhJ4L61gs5r1cPCiquKxbzh+3qQjGM61SjgsUlzzleo44fDwim37OEEoxX4JWHhbUPgF+2bZ+NP+CyPwv8AjH8QtK1Se6ne5a/ttZ0/XNVgEYsrg6pa6ra6Xrfh3S98LzaV4c1yFYI47aIxXFpDJpd5xx/d1ubFwm46t9dej3s0uye1lbo/69r5hS4y8LauVfRb4g4ZyTEYaNOHs/Y1MJicJh587rQ+rVsNUxGDx2J5ZKGJx+Em5uVSScKko4mn/ft+yP8AGL9nz42fBDwb4u/Zm1fQNT+FS6ZbaVoVr4etf7OttCj0q3gtv7BuNIaC2m0e70qLyLd9Ont4Xgi8kohgeJ29unKE4RlTacLWVlZK2lraWtbay8tD/Hfj/hvjDhXivNsq47wmNwvEv1ipisfLHT9tVxk8TOVT69DFKU4YuliZc044iFSanLmUmqkZxj9NVZ8YFAHwB/wVi/5RZf8ABSz/ALMA/bI/9Z1+I1AH3/QAUAFAH88n/By1em2/4J9wW/y4vviz4Ihwcbt8dxczKV4zwiSk9hxnkgjhzD/d/Wcfykz+zPoMUfaeNLqWf7nhvNpXsrWlGEbN73batbs720PgL/g1S0wPJ+1lq5RD5P8AwrvTt5TLD7QNeudobsG+zBiMc+XnPyiufLVrUdukde2sj9j/AGhdfXw2w1/i/t2vy/4PqlO9ur/eWv0v5n9i+B6D8hXrH+aAYHoPyoArXt7aabZ3WoX9xFaWVlby3V3dTuI4be2gRpZppXPCRxRqzux4Cgk0F06dStUhSpQlUq1Zxp04RV5TnNqMYxS1cpNpJdWz/O0/4LSfte+Jv25P214vg/8ADee71/wd8PPEMHw38F6Jp0BP9veP9R1FNO1aeGFENxdT215Ja+GLcPLcW7XGl32o6aIY9ZnV/CxdV1q3KtVH3Ul3u76W9F8m+p/tH9Fnw1wHhN4VVeK8/jSweZ53gqmfZpi69RcuByPD0HXwsXNy9nThUpRq5jUcYU6ip4ihh8Q6ksJCS/sv/wCCaX7Engz9kr9k74b/AA2udF07UvEM+lQeIPFWoXtlaTS33iLW40v9Vuz+4XKyzyLDatMZbqPSbXSrGeeQWEW31cPRjTpRha/V3s9Xu0f5h+OPijmfiR4iZ7xB9YrUcJ9Ylg8voU6tWKoYHCOVHDUbOejhBc1RQUKcsTUxFaEI+2lf9BrTwp4ZsLiO7stA0i1uoSTFcQafaxSxllKkpIkYZSVJGQQcE1vyxWyX3H4/PG4yrCVOpisRUhL4oTrVJRlbXVOTT1P5SP8Ag52/Zaj1jwF8Nv2oPD+jIdR8IakPCvi+/s7ZvNk8P600UNnLqky7k8nTtYSyjsHKxsJNa1BZpZQbZIvNzGleMaiXw/E+yf8AwbfNv1P9CfoF+IDwee55wBjcU1QzbD/2hllGrUXKsdg1OpVhh4Np+0r4V15VormXJhKLjGNqjl0//Bsf+1PbeJ/g749/Zh12/lGs/DjXJdc8MxXDL9mn8OeKZrvVoLOwAYym+h1eLxZc6iZAITZDS1tzviuAXl9VOLpu909O1nrp53vfysuxwfTy8P6mW8WZPx7hKMfqfEGDjhcdKEXz08wy6NLC1KlZ2UfZVMNLLIYdRbn7b63KouWdNn0r/wAHL2pyWP8AwT10+2DokWq/HHwBprZCeZLI2keMdTWBCfnCFdJeeTbx/o6bucZ0zD+Av8aX/ksn+h8P9BahGt42VJ2blhuEc6xCV3aK+s5bh5TfRtPERgr6++7Le35x/wDBqzYCTxF+1ZfhDugT4VQ+bk4WOS3+IzPEQBgmR0jcE4x5RAzuOOfLd6npH/24/b/2hFVrC+HNLm0l/rFNw7uNTI0pL/Cm0/8AEu5/ZvXrH+Yh+BH/AAcW+CvhtrP/AAT88WeJ/FVppsfi3wt4j8K3Xw+1GVYk1BvEN14h0zTbrTrWUqZZVn8Mah4ju5bNXRZFs/tbbms0FceOjB0JN25k48u+/Mv0vuf139CnNM6wfjVlWAy2dZ4DMsDmVHOqEeaVFYGngq+Jp16kU+WDhmGHwNKFVxbUq3sk17Vs/Jb/AINY/if4qg+Lv7RXwd33U/g3U/Aui/EEB5J2tNM17Q9bg0HZDEM28c+tWXiDzJnbEkiaFEEyqPjky2T5qsOnKpLyaaX4qX4H9GftBeHsulkPAvFK9lTzShm2MyOVowVbFYLF4SWOTlL45QwdbAtRSvGLxkm7NxP7YK9c/wAuAoA+AP8AgrF/yiy/4KWf9mAftkf+s6/EagD7/oAKACgD+b7/AIObY765/Yi8BWOn2F5fT3nxr8PLILSKWcxwW+ja5cFnjiVsK0qRjewAztUHPB4MxTdCNlvUXfpGWn6n9t/QPnRp+LWb1a9elRjT4TxvL7WcYc854vCx91yabcY82kekteh+Dn/BGb/gpT4R/wCCcen/ABn074lfCP4ieL1+Jd94XvLCfwvBBC9mNAt9Vhkiu49QRA4mbUQ8bxyAp5bKyNvDJxYXELDqSlFyUuzX62/N9dD+u/pP+BeZ+N+J4UxGQ8S5Hln9g0cwpV45jOclV+uzoSjKm6Mn8PsbSjKMb8yal7rT/cv/AIiYP2d/+jcvjp/3xon/AMdrr/tGn/z7n98P/kj+Uf8AiRLjb/oueD/vxRoaV/wcmfAfXNU03RdK/Zp+PN5qerX1rpun2kMGjyzXF5ezJb20MUccheSSWWRERFG5mIA5Ip/2hB6KlUb6L3X+TMMV9BvjDB4bEYvEce8GUaGGo1K9arUnio06dKlBznOcnpGMYptt2SSPqT/gsB+234j/AGc/2J9Q1Twno+vaZ8U/izp+neF/D2nafE93feGNQ8U6bqc6XF7fafI8UMmmWuk6zdRXFq9zFdXmkpahfs12bmPbFVXTpNrST93va9+u3R7X2tazufmv0avC7B8e+KWEwuZYnA1+Hsgq18yx9etJUqOYYbL62Hg6dGjXSlOOJqYnC0pU6ijOnSxLqfxKXI/5u/8Ag39/YW1v47/tNal+0H8TvDOtnwb8JnkutEvtWt7m0t9Z8f6i4N7cGe5tydROiaTcTC6jtZre7stY8ReHtUa48u3a2u/PwNFzqOpJaRem697e6ezsnZpa+8npY/uX6Zfi3hOFOBMPwPw/mGE/tPiJRhjaOFqQqVMJk2G0oU3CnNewWKxdODi6sJ0q2FwGOw3s+aoqlL++dESNEjjUKkaqiKOAqKAqqB2AAAHtXtH+Qbbbbbu3q29W2+rHUAfKH7bvwF0T9pT9mD4vfCTW8xw+JvB2s21veLCbo6fcNZTKNQFmATevYRtJe29qpR3u7e3aGWGdIpkyrQVSnKL6p/LztdXa3SPv/C/i7FcD8d8N8SYRc9TLszwtZ0udU/bxjWhL2Dqv+FGtJKjUqapUalSM4yhKcZfwBf8ABLb4h/E39in/AIKH+Bode0fXNGsbjxjqHwc+IltJa3EVgVudZXTYlvrvyngis7PxLYadPPegmOOzWeRna2aQP4uHlKjXSemvLJPztv8AO2z7rqz/AGK+kHk2Q+KnglmtXBYrCYqtSyujxVkdSNSDrfu8J9Ym6VLmUpVauX1q8YUX7zquCSVRRcf6W/8Ag5oubnW/2DvhSmj6fqGoDVf2g/BOorJZwyTxx2qeAPiWVWeOJGKtO13EYJCQuIp1AJYEehmGtCOj1qL5e7L89D+FvoIRpYXxh4gliq9Ci8PwTm1C1WUacpVf7ayLmcHJq8YKlPnilf3oO9k0/wANf+CKP/BRL4e/8E7h8cYfi18OviHrifEyfwTc6XceGNMWZ7ZvC8PimGaK5hvDbriYeIVaORJiV8lkMTCTenFhK8aDlzptSS+G3S/f1P6y+lT4KZ341T4Rq8NZ3kmFeQ082pYiGPruKqLMJ5fOEqcqbl8DwTUoyir86alo0/3h1L/g5M/Zhhs55NL+Cvxvvr5Y3NvbS6Pp9vHLLtOxWkF5IQGbAOF6dxXY8wp292E2/RP8mfyLQ+g74gyqQVfijhKjS5lzz+tVpNRuuZpcr1te35PZ/hz+2t+0d+3r/wAFn/GfhrwH8Hv2bviPpfwf8NarDc6PpKaLqB0ttbnthZf2/wCNfGEttZ+GdOjCyXf9lwTzWkVhb3r2kl3qd2Tez8lSdfFytCEuS60S0Wm8pfCutrvRb33P6s8LOCvB76LWWY/O+KuPMixPFGYYadPEYj6zReIjg41PavBZVlcKlXMK87worE1IU5yr1KMakaeHp2ow/pZ/4I0/8Ewpv+Ce3wm8Qah481Ky1v41fFR9J1HxrdaWA2kaHp+m20p0bwtpk8iedeHTZdR1SXUNSVooL26uVjt4Bb2kdzd+jhcP9Xg02nObTlbZW2in1td3eze2lj+FfpNePX/EbOJ8Esqw1fBcJ8OQxNDJqOJssVjK2JqR+tZliIRk40vrEKGHjQoPmnRpQvOftKk6dP8Aaeuo/mUKAPgD/grF/wAosv8AgpZ/2YB+2R/6zr8RqAPv+gAoAKAMHxB4W8N+K7VLLxLoWk69aRuJI7XWNPtNSt0kHSRYLyKaIOOz7Nw7EUmlJWaujowuMxeBq+2weKxGEq25XUw1erQm46rlcqU4ScdXo3byOP8A+FLfCP8A6Jp4E/8ACS0D/wCV9T7On/Ivx/zPS/1j4g/6Hmb/APhyxv8A8vD/AIUt8I/+iaeBP/CS0D/5X0ezp/yL8f8AMP8AWPiD/oeZv/4csb/8vJ7b4P8Awrs7iG6tfh14Jt7m3kWWCeHwtocU0MqcrJFLHYLJG6nlXRlZTyCDQoQTTUUmtf6uRUz/AD2rCdKrnOa1Kc4uM4TzHGSjKL3jKLrNNPqmrNaPQ0/Evw78D+MZbafxP4Y0fW5rSLybaXULKG5eKHczCNGkViEVndkXopkk2gb2y3GL3SZlgc3zTLYzjgMdicJGo+aaoVZ01KVkrtRa1aSu+to32VrXhnwP4R8GJcJ4W8O6ToQu9n2o6bZQ2rXAjz5YmaJFMgj3NsDEhdzYxk01GMdkl6GeNzPMMxlGWPxmIxbhfk9vVnU5XL4nFSbScrK7Su+p1VM4QoAayq6sjqGR1KsrDIZWGGUg8EEEgjuKA9NPQ8rl+B3wimnkuZPh74VaaWZriST+yLMFpnfzGk4iGGL/ADZGMHkYqeSO/Kr+Z70eKOIo01SjnOYqnGPIo/WqtlC1uX4trO1u2h2ereEfDGu2VrpusaFpWp6fZCMWtjf2Ntd2kAiQpF5VtcRSQIYkJWMrGDGpKoVBINaWtZW7Hl0MbjMLWliMNisRQrz5uetRrVKdWXO+afNUhKM3zy96V37zSbu0mc7/AMKi+F//AEIHg/8A8JvRv/kKlaP8sfuO7/WDPf8Aoc5p/wCF+L/+Wij4R/DAEEeAPB+QQRjw5o45HuLMEfgQaHGL+yvuD/WDPWmv7ZzTXR/7fitvnVOs07w/omkRrFpelWOnxISUitLeOCJC33iscaqilv4iFBPemebWxFfETdSvWq1pyspTq1JTk7KyvKTbdltc2KDEKACgD4A/4Kxf8osv+Cln/ZgH7ZH/AKzr8RqAPv8AoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAPgD/grF/yiy/4KWf8AZgH7ZH/rOvxGoA+/6ACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD4A/4Kxf8AKLL/AIKWf9mAftkf+s6/EagD7/oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAPgD/AIKxf8osv+Cln/ZgH7ZH/rOvxGoA+/6ACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD4A/4Kxf8osv+Cln/ZgH7ZH/AKzr8RqAPv8AoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAPgD/grF/yiy/4KWf8AZgH7ZH/rOvxGoA+/6ACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD4A/4Kxf8AKLL/AIKWf9mAftkf+s6/EagA/wCHsX/BLL/pJZ+wB/4mR+zr/wDPGoAP+HsX/BLL/pJZ+wB/4mR+zr/88agA/wCHsX/BLL/pJZ+wB/4mR+zr/wDPGoAP+HsX/BLL/pJZ+wB/4mR+zr/88agA/wCHsX/BLL/pJZ+wB/4mR+zr/wDPGoAP+HsX/BLL/pJZ+wB/4mR+zr/88agA/wCHsX/BLL/pJZ+wB/4mR+zr/wDPGoAP+HsX/BLL/pJZ+wB/4mR+zr/88agA/wCHsX/BLL/pJZ+wB/4mR+zr/wDPGoAP+HsX/BLL/pJZ+wB/4mR+zr/88agA/wCHsX/BLL/pJZ+wB/4mR+zr/wDPGoAP+HsX/BLL/pJZ+wB/4mR+zr/88agA/wCHsX/BLL/pJZ+wB/4mR+zr/wDPGoAP+HsX/BLL/pJZ+wB/4mR+zr/88agA/wCHsX/BLL/pJZ+wB/4mR+zr/wDPGoAP+HsX/BLL/pJZ+wB/4mR+zr/88agA/wCHsX/BLL/pJZ+wB/4mR+zr/wDPGoAP+HsX/BLL/pJZ+wB/4mR+zr/88agA/wCHsX/BLL/pJZ+wB/4mR+zr/wDPGoAP+HsX/BLL/pJZ+wB/4mR+zr/88agD4g/4Ka/8FNf+Cbfj3/gm3/wUG8C+Bf8AgoN+xB408beNP2IP2r/Cfg7wd4T/AGr/AIDeI/FXizxV4j+A3j7R/D3hrw14e0fx9eavrviDXdXvLTS9G0bS7S61HVNRuraxsbae5niiYA//2Q=="/><span class="pageTitle"><br/></span>
          </p>

          <p>
            <span class="pageTitle">Application Form (EASE)</span>
          </p>

          <p class="sectionGroup">
            <span class="sectionGroup"><br/></span>
          </p>

          <table>
            <tr>
              <td class="warning">
                <p class="statement">
                  <span class="warningLarge">WARNING NOTE:</span>
                </p>
                <p class="statement">
                  <span class="warningLarge"><br/>Pursuant
                    to Section 25(5) of the Insurance Act of Singapore (CAP 142),
                    you are to disclose in this proposal form, fully and faithfully,
                    all the facts which you know or ought to know, or the policy
                    issued below may be void</span>
                </p>
              </td>
            </tr>
          </table>

          <p class="sectionGroup">
            <span class="sectionGroup"><br/></span>
          </p>
        </div>
      </td>

      <td>
        <div>
          <xsl:choose>

            <!--AWT Header-->
            <xsl:when test="$isAwt">
              <table class="office">
                <tr>
                  <td class="officeHeading">
                    <p><span lang="EN-HK" class="officeHeading">FOR OFFICE USE ONLY</span></p>
                  </td>
                </tr>

                <tr>
                  <td class="officeContentFirstRow">
                    <p><span lang="EN-HK" class="officeTitle">Proposer Number:</span></p>
                    <p>
                      <span lang="EN-HK" class="officeContent">
                        <xsl:value-of select="./policyNumber"/>
                      </span>
                    </p>
                  </td>
                </tr>

                <tr>
                  <td class="officeContent">
                    <p><span lang="EN-HK" class="officeTitle">Financial Consultant Code:</span> </p>
                    <p>
                      <span lang="EN-HK" class="officeContent">
                        <xsl:choose>
                          <xsl:when test="/root/agent/agentCodeDisp">
                            <xsl:value-of select="/root/agent/agentCodeDisp"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <br/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                </tr>

                <tr>
                  <td class="officeContent">
                    <p><span lang="EN-HK" class="officeTitle">Financial Consultant Name:</span></p>
                    <p>
                      <span lang="EN-HK" class="officeContent">
                        <xsl:choose>
                          <xsl:when test="/root/agent/name">
                            <xsl:value-of select="/root/agent/name"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <br/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                </tr>

                <xsl:if test="not(/root/proposer/extra[channel = 'FA'])">
                <tr>
                  <td class="officeContent">
                    <p><span lang="EN-HK" class="officeTitle">Name of Organisation:</span></p>
                    <p>
                      <span lang="EN-HK" class="officeContent">
                        <xsl:choose>
                          <xsl:when test="/root/agent/company">
                            <xsl:value-of select="/root/agent/company"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <br/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                </tr>
                </xsl:if>


                <xsl:if test="/root/proposer/personalInfo/branchInfo/branch">
                  <tr>
                    <td class="officeContent">
                      <p><span lang="EN-HK" class="officeTitle">SingPost Branch:</span></p>
                      <p>
                        <span lang="EN-HK" class="officeContent">
                          <xsl:choose>
                            <xsl:when test="/root/proposer/personalInfo/branchInfo/branch">
                              <xsl:value-of select="/root/proposer/personalInfo/branchInfo/branch"/>
                            </xsl:when>
                            <xsl:otherwise>
                              <br/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </span>
                      </p>
                    </td>
                  </tr>
                </xsl:if>

                <xsl:if test="/root/proposer/personalInfo/branchInfo/bankRefId">
                  <tr>
                    <td class="officeContent">
                      <p><span lang="EN-HK" class="officeTitle">Referrer ID:</span></p>
                      <p>
                        <span lang="EN-HK" class="officeContent">
                          <xsl:choose>
                            <xsl:when test="/root/proposer/personalInfo/branchInfo/bankRefId">
                              <xsl:value-of select="/root/proposer/personalInfo/branchInfo/bankRefId"/>
                            </xsl:when>
                            <xsl:otherwise>
                              <br/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </span>
                      </p>
                    </td>
                  </tr>
                </xsl:if>
              </table>
            </xsl:when>

            <!--Shield Header-->
            <xsl:otherwise>
              <table class="office">
                <tr>
                  <td class="officeHeading">
                    <p><span lang="EN-HK" class="officeHeading">FOR OFFICE USE ONLY</span></p>
                  </td>
                </tr>

                <xsl:variable name="countAXAShield" select="count(/root/reportData/iCidMapping/*/covCode[. = 'ASIM'])"/>

                <tr>
                  <td class="officeContentFirstRow">
                    <p><span lang="EN-HK" class="officeTitle">Proposer Number: (AXA Shield)</span></p>
                    <p>
                      <xsl:for-each select="/root/reportData/iCidMapping/*[./covCode = 'ASIM']">
                        <span lang="EN-HK" class="officeContent nowrap">
                          <xsl:value-of select="./policyNumber"/>
                          <xsl:if test="not(position() = last()) and $countAXAShield > 1 and position() mod 2 = 1">,<xsl:value-of select="$space"/></xsl:if>
                        </span>
                      </xsl:for-each>
                    </p>
                  </td>
                </tr>

                <xsl:variable name="countAXABasicCare" select="count(/root/reportData/iCidMapping/*/covCode[. = 'ASP'])"/>

                <xsl:if test="$countAXABasicCare > 0">
                  <tr>
                    <td class="officeContentFirstRow">
                      <p><span lang="EN-HK" class="officeTitle">Proposer Number: (AXA Basic Care)</span></p>
                      <p>
                        <xsl:for-each select="/root/reportData/iCidMapping/*[./covCode = 'ASP']">
                          <span lang="EN-HK" class="officeContent nowrap">
                            <xsl:value-of select="./policyNumber"/>
                            <xsl:if test="not(position() = last()) and $countAXABasicCare > 1 and position() mod 2 = 1">,<xsl:value-of select="$space"/></xsl:if>
                          </span>
                        </xsl:for-each>
                      </p>

                    </td>
                  </tr>
                </xsl:if>

                <tr>
                  <td class="officeContent">
                    <p><span lang="EN-HK" class="officeTitle">Financial Consultant Code:</span> </p>
                    <p>
                      <span lang="EN-HK" class="officeContent">
                        <xsl:choose>
                          <xsl:when test="/root/reportData/agent/agentCodeDisp">
                            <xsl:value-of select="/root/reportData/agent/agentCodeDisp"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <br/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                </tr>

                <tr>
                  <td class="officeContent">
                    <p><span lang="EN-HK" class="officeTitle">Financial Consultant Name:</span></p>
                    <p>
                      <span lang="EN-HK" class="officeContent">
                        <xsl:choose>
                          <xsl:when test="/root/reportData/agent/name">
                            <xsl:value-of select="/root/reportData/agent/name"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <br/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                </tr>

                <tr>
                  <td class="officeContent">
                    <p><span lang="EN-HK" class="officeTitle">Name of Organisation:</span></p>
                    <p>
                      <span lang="EN-HK" class="officeContent">
                        <xsl:choose>
                          <xsl:when test="/root/reportData/agent/company">
                            <xsl:value-of select="/root/reportData/agent/company"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <br/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                </tr>


                <xsl:if test="/root/reportData/proposer/personalInfo/branchInfo/branch">
                  <tr>
                    <td class="officeContent">
                      <p><span lang="EN-HK" class="officeTitle">SingPost Branch:</span></p>
                      <p>
                        <span lang="EN-HK" class="officeContent">
                          <xsl:choose>
                            <xsl:when test="/root/reportData/proposer/personalInfo/branchInfo/branch">
                              <xsl:value-of select="/root/reportData/proposer/personalInfo/branchInfo/branch"/>
                            </xsl:when>
                            <xsl:otherwise>
                              <br/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </span>
                      </p>
                    </td>
                  </tr>
                </xsl:if>
                <xsl:if test="/root/reportData/proposer/personalInfo/branchInfo/bankRefId">
                  <tr>
                    <td class="officeContent">
                      <p><span lang="EN-HK" class="officeTitle">Referrer ID:</span></p>
                      <p>
                        <span lang="EN-HK" class="officeContent">
                          <xsl:choose>
                            <xsl:when test="/root/reportData/proposer/personalInfo/branchInfo/bankRefId">
                              <xsl:value-of select="/root/reportData/proposer/personalInfo/branchInfo/bankRefId"/>
                            </xsl:when>
                            <xsl:otherwise>
                              <br/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </span>
                      </p>
                    </td>
                  </tr>
                </xsl:if>
              </table>
            </xsl:otherwise>
          </xsl:choose>
        </div>
      </td>

    </tr>


  </table>


</xsl:template>


</xsl:stylesheet>
