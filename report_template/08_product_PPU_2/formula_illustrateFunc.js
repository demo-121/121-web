function(quotation, planInfo, planDetails, extraPara) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  if (quotation.baseProductCode === 'ESP') {
    return [];
  } else {
    var planDetail = planDetails[planInfo.covCode];
    var planType = quotation.policyOptions.planType;
    var indexation = quotation.policyOptions.indexation === 'Y';
    var polYrs = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.pAge;
    var premYrs;
    if (planInfo.premTerm === 'SP') {
      premYrs = 1;
    } else if (planInfo.premTerm.endsWith('_YR')) {
      premYrs = Number.parseInt(planInfo.premTerm);
    } else {
      premYrs = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
    var sumInsured = planInfo.sumInsured;
    var illYrs;
    if (planType === 'renew') {
      illYrs = Math.min((Math.floor((60 - quotation.pAge) / polYrs) + 1) * polYrs, 65 - quotation.pAge);
    } else {
      illYrs = polYrs;
    }
    var bpPolYrs = quotation.plans[0].policyTerm.endsWith('_YR') ? Number.parseInt(quotation.plans[0].policyTerm) : Number.parseInt(quotation.plans[0].policyTerm) - quotation.iAge;
    var premRateFirstYr = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.pAge));
    var sumProduct = math.bignumber(0);
    var yearPrems = [0];
    for (var i = 1; i <= illYrs; i++) {
      var age = quotation.pAge + i - 1;
      var yearPrem;
      if (i === 1) {
        yearPrem = math.bignumber(planInfo.yearPrem);
      } else {
        yearPrem = math.bignumber(0);
        if (planType === 'renew') {
          if ((i - 1) % bpPolYrs < polYrs) {
            var rateAge = Math.floor((i - 1) / bpPolYrs) * bpPolYrs + quotation.pAge;
            var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, rateAge));
            var annualBenefit = roundDown(runFunc(planDetail.formulas.getAnnualBenefit, quotation, planInfo, i, extraPara), 1);
            yearPrem = roundDown(math.divide(roundDown(math.multiply(premRate, annualBenefit), 2), 100), 2);
          }
        } else if (i <= polYrs) {
          var annualBenefit = roundDown(runFunc(planDetail.formulas.getAnnualBenefit, quotation, planInfo, i, extraPara), 1);
          var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, age));
          var totIndexPrem = math.bignumber(0);
          if (extraPara.illustrations[quotation.baseProductCode][i - 1]) {
            totIndexPrem = math.add(totIndexPrem, math.bignumber(extraPara.illustrations[quotation.baseProductCode][i - 1].indexPrem));
          }
          if (extraPara.illustrations.SV && extraPara.illustrations.SV[i - 1]) {
            totIndexPrem = math.add(totIndexPrem, math.bignumber(extraPara.illustrations.SV[i - 1].indexPrem));
          }
          sumProduct = math.add(sumProduct, math.multiply(premRate, totIndexPrem));
          yearPrem = roundDown(math.divide(roundDown(math.multiply(premRateFirstYr, annualBenefit), 2), 100), 2);
          yearPrem = math.add(yearPrem, math.divide(sumProduct, 100));
        }
      }
      yearPrems.push(yearPrem);
    }
    var commRates = runFunc(planDetail.formulas.getCommRates, quotation, planInfo, planDetails);
    var totComms = [0];
    for (var i = 1; i <= illYrs; i++) {
      var commRate = commRates[(i - 1) % polYrs];
      if (commRate) {
        totComms[i] = math.multiply(yearPrems[i], commRate);
      }
    }
    var illustration = [];
    var illLength = Math.max(totComms.length, yearPrems.length) - 1;
    for (var i = 0; i < illLength; i++) {
      illustration.push({
        yearPrem: yearPrems[i + 1] ? math.number(yearPrems[i + 1]) : 0,
        totComm: totComms[i + 1] ? math.number(totComms[i + 1]) : 0
      });
    }
    return illustration;
  }
}