function(quotation, planInfo, planDetail) {
  var trunc = function(value, position) {
    return runFunc(planDetail.formulas.trunc, value, position);
  };
  var basicPlan = quotation.plans[0];
  var sumInsured = planInfo.sumInsured;
  sumInsured = math.bignumber(sumInsured);
  var iGender = quotation.iGender;
  var iSmoke = quotation.iSmoke;
  var iAge = quotation.iAge;
  var polTerm = basicPlan.premTerm;
  var premTerm = polTerm;
  var SizeDiscount_Table = planDetail.rates.SizeDiscount_Table;
  var LOADING = null;
  var adj_size = -1;
  var LOW_SA = SizeDiscount_Table.LOW_SA;
  for (var j = 0; j < LOW_SA.length; j++) {
    if (sumInsured >= LOW_SA[j]) adj_size = SizeDiscount_Table.LOADING[j];
  }
  adj_size = math.bignumber(adj_size);
  var Raw_PremiumRate = 0;
  if (polTerm) {
    Raw_PremiumRate = planDetail.rates.premRate[iGender + (iSmoke == 'N' ? 'N' : 'S')][polTerm + 'PT'][iAge];
  }
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    if (plan.covCode == planInfo.covCode) {
      plan.premRate = math.number(Raw_PremiumRate);
      break;
    }
  }
  Raw_PremiumRate = math.bignumber(Raw_PremiumRate);
  var finalPremRate = math.bignumber(math.add(Raw_PremiumRate, adj_size));
  var annualPrem1 = math.bignumber(trunc(math.multiply(finalPremRate, math.divide(sumInsured, 1000)), 2));
  var riderWork = 0;
  var annualPrem2 = math.subtract(annualPrem1, riderWork);
  return math.number(annualPrem2);
}