function(quotation, planInfo, planDetail) { /*esc_validAfterCalcFunc*/
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  var premMin = 360;
  if (planInfo.calcBy && quotation.paymentMode) {
    if (planInfo.calcBy === 'sumAssured') {
      var basicPlan = quotation.plans[0];
      var totalPrem = math.number(basicPlan.yearPrem);
      if (premMin > totalPrem && planInfo.sumInsured && planInfo.yearPrem) {
        quotDriver.context.addError({
          covCode: planInfo.covCode,
          msg: 'The Modal Premium is below the Minimum Requirement. Please increase the Sum Assured.'
        });
      }
    }
  }
  if (planInfo.premium) {
    if (planInfo.policyTerm.indexOf('YR') > -1) {
      planInfo.planCode = Number.parseInt(planInfo.policyTerm) + 'EP2B';
      planInfo.policyTermYr = Number.parseInt(planInfo.policyTerm);
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      planInfo.planCode = 'EP2B' + Number.parseInt(planInfo.policyTerm);
      planInfo.policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
  } else {
    planInfo.planCode = null;
  }
  var hasBoughtAxaPlan = null;
  if (quotation.policyOptions && quotation.policyOptions.hasBoughtAxaPlan) {
    hasBoughtAxaPlan = quotation.policyOptions.hasBoughtAxaPlan;
  }
  if (hasBoughtAxaPlan == "Y") {
    let files = [];
    files.push({
      covCode: quotation.plans[0].covCode,
      productId: quotation.baseProductId,
      hasExtraProdSummary: true,
      attId: 'prod_summary_1'
    });
    quotation.extraProdSummary = files;
  } else {
    quotation.extraProdSummary = null;
  }
  if (hasBoughtAxaPlan == "N") {
    if (quotation.policyOptions) {
      _.unset(quotation.policyOptions, 'termAndCondition');
    }
    if (quotation.policyOptionsDesc) {
      _.unset(quotation.policyOptionsDesc, 'termAndCondition');
    }
  }
}