function(planDetail, sumInsured) { /*esc_getLsd*/
  var lsdMap = [{
    sumInsured: 50000,
    lsd: 0
  }, {
    sumInsured: 100000,
    lsd: 1.30
  }, {
    sumInsured: 250000,
    lsd: 1.90
  }, {
    sumInsured: 500000,
    lsd: 2.20
  }];
  var mapping = _.findLast(lsdMap, m => sumInsured >= m.sumInsured);
  if (mapping) {
    return mapping.lsd;
  }
  return 0;
}