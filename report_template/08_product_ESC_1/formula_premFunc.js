function(quotation, planInfo, planDetail) { /*esc_premFunc*/
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, Number(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  var hasBoughtAxaPlan = null;
  if (quotation.policyOptions && quotation.policyOptions.hasBoughtAxaPlan) {
    hasBoughtAxaPlan = quotation.policyOptions.hasBoughtAxaPlan;
  }
  const campaginDiscount = 0.1;
  const loyaltyDiscount = 0.1;
  var totalDiscount = campaginDiscount;
  if (hasBoughtAxaPlan == "Y") {
    totalDiscount = campaginDiscount + loyaltyDiscount;
  }
  var premium = null;
  var sumInsured = planInfo.sumInsured;
  if (planInfo.premTerm && sumInsured) {
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
    var lsd = runFunc(planDetail.formulas.getLsd, planDetail, sumInsured);
    var premRateAfterLsd = math.subtract(premRate, lsd);
    var discountRateBeforeTrunc = math.multiply(math.bignumber(premRateAfterLsd), math.bignumber(totalDiscount));
    var discountRate = trunc(discountRateBeforeTrunc, 2);
    var netDiscountRate = math.subtract(premRateAfterLsd, discountRate);
    var premAfterLsdBeforeTrunc = math.multiply(netDiscountRate, math.bignumber(sumInsured), math.bignumber(0.001));
    var premAfterLsd = trunc(premAfterLsdBeforeTrunc, 2);
    var modalFactor = 1;
    for (var p in planDetail.payModes) {
      if (planDetail.payModes[p].mode === quotation.paymentMode) {
        modalFactor = planDetail.payModes[p].factor;
      }
    }
    var prem = trunc(math.multiply(premAfterLsd, math.bignumber(modalFactor)), 2);
    premium = math.number(prem);
  }
  return premium;
}