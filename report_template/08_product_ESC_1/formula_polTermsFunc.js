function(planDetail, quotation) { /*esc_polTermsFunc*/
  var options = [];
  var ageChecked = quotation.iAge;
  _.each([15, 20, 25], (val) => {
    if (ageChecked + val <= 75) {
      options.push({
        value: '' + val + '_YR',
        title: val + ' Years'
      });
    }
  });
  _.each([60, 65, 75], (val) => {
    if (ageChecked + 6 <= val) {
      options.push({
        value: '' + val + '_TA',
        title: 'To Age ' + val
      });
    }
  });
  return options;
}