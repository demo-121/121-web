function(quotation, planDetails, extraPara, illustrations) { /*esc_getillustrateSummary*/
  const termAndConditionErrorMsg = "Please agree to the terms and conditions.";
  var hasBoughtAxaPlan = null;
  if (quotation.policyOptions && quotation.policyOptions.hasBoughtAxaPlan) {
    hasBoughtAxaPlan = quotation.policyOptions.hasBoughtAxaPlan;
  }
  var termAndCondition = false;
  if (quotation.policyOptions && quotation.policyOptions.termAndCondition) {
    termAndCondition = quotation.policyOptions.termAndCondition;
  }
  if (hasBoughtAxaPlan == "Y" && !termAndCondition) {
    illustrations.error = {
      errorMsg: termAndConditionErrorMsg
    };
  }
  return illustrations;
}