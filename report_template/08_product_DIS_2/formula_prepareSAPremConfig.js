function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation, planDetails);
  var saMaxSG = {
    SGD: 6000000,
    USD: 4195000,
    EUR: 3896000,
    GBP: 2884000,
    AUD: 5714000
  };
  var saMaxNonSG = {
    SGD: 4000000,
    USD: 2797000,
    EUR: 2597000,
    GBP: 1923000,
    AUD: 3809000
  };
  var bpSa = quotation.plans[0].sumInsured;
  var saMax = (quotation.iResidence === 'R2' ? saMaxSG : saMaxNonSG)[quotation.ccy];
  planDetail.inputConfig.benlim = {
    max: Math.min(bpSa || saMax, saMax),
    min: 100000
  };
}