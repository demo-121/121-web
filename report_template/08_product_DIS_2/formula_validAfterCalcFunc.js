function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  if (planInfo.sumInsured) {
    var aggrMax = {
      SGD: 6000000,
      USD: 4195000,
      EUR: 3896000,
      GBP: 2884000,
      AUD: 5714000
    };
    var aggrMaxNonSg = {
      SGD: 4000000,
      USD: 2797000,
      EUR: 2597000,
      GBP: 1923000,
      AUD: 3809000
    };
    var tpdSa = planInfo.sumInsured;
    var dcbSa = 0;
    for (var p in quotation.plans) {
      var plan = quotation.plans[p];
      if (plan.covCode === 'DCB' && plan.sumInsured) {
        dcbSa = plan.sumInsured;
        break;
      }
    }
    var max = (quotation.iResidence === 'R2' ? aggrMax : aggrMaxNonSg)[quotation.ccy];
    if (dcbSa * Math.min(65 - quotation.iAge, 20) + tpdSa > max) {
      quotDriver.context.addError({
        covCode: planInfo.covCode,
        msg: 'Aggregation SA of DCB and TPD cannot be more than ' + getCurrencyByCcy(max, 0, quotation.compCode, quotation.ccy)
      });
    }
  }
  if (planInfo.premium) {
    var mapping = planDetail.planCodeMapping;
    for (var i = 0; i < mapping.planCode.length; i++) {
      if (planInfo.policyTerm === mapping.policyTerm[i] && planInfo.premTerm === mapping.premTerm[i]) {
        planInfo.planCode = mapping.planCode[i];
        break;
      }
    }
    planInfo.policyTermYr = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    if (planInfo.premTerm === 'SP') {
      planInfo.premTermYr = 1;
    } else if (planInfo.premTerm.endsWith('_YR')) {
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
  } else {
    planInfo.planCode = null;
  }
}