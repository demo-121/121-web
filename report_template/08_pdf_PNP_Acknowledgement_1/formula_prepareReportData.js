function(quotation, planInfo, planDetails, extraPara) {
  var reportData = {};
  Object.assign(reportData, extraPara.company);
  var basicPlan = quotation.plans[0];
  var illustrations = extraPara.illustrations;
  var basicIllustrations = illustrations[basicPlan.covCode];
  reportData.packagedPlanName = basicIllustrations.packagedPlanName;
  return reportData;
}