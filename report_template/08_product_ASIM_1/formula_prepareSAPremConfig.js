function(planDetail, quotation, planDetails) {
  planDetail.inputConfig.canEditPaymentMethod = false;
  if (quotation.policyOptions.axaShield > quotation.policyOptions.awl) {
    planDetail.inputConfig.paymentMethodList = [{
      value: 'CPF_CASH',
      title: {
        en: 'CPF + Cash'
      }
    }];
  } else {
    planDetail.inputConfig.paymentMethodList = [{
      value: 'CPF',
      title: {
        en: 'CPF'
      }
    }];
  }
  planDetail.inputConfig.canEditPayFreq = false;
  planDetail.inputConfig.payFreqList = [{
    value: 'A',
    title: {
      en: 'Annual'
    }
  }];
  planDetail.inputConfig.canEditPremium = false;
}