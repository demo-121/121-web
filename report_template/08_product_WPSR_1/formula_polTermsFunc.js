function(planDetail, quotation) {
  var opts = [];
  for (var i = 10; i <= 30; i++) {
    opts.push({
      value: "yrs" + i,
      title: i + " Years"
    });
  }
  var ageOpts = [55, 60, 65, 85],
    iAge = quotation.iAge;
  for (var i in ageOpts) {
    var ageOpt = ageOpts[i];
    if (ageOpt - iAge >= 10) {
      opts.push({
        value: "age" + ageOpt,
        title: "To Age " + ageOpt
      });
    }
  }
  return opts;
}