function(quotation, planInfo, planDetails, extraPara) {
  var dealerGroup = quotation.agent.dealerGroup;
  var premium = quotation.premium;
  var salesCharges = quotation.policyOptions.singlePremSalesCharge;
  var tableKey = -1;
  if (dealerGroup === 'AGENCY' || dealerGroup === 'SYNERGY') tableKey = 'AGENCY';
  else if (dealerGroup === 'BROKER' || dealerGroup === 'FUSION') tableKey = 'BROKER';
  else if (dealerGroup === 'SINGPOST') tableKey = 'SINGPOST';
  else if (dealerGroup === 'DIRECT') tableKey = 'DIRECT';
  var covCode = planInfo.covCode;
  var illustaion = {};
  illustaion.sumInsured = quotation.sumInsured;
  if (tableKey !== -1) illustaion.tdc = planDetails[covCode].rates.TDC[tableKey][salesCharges][premium < 10000 ? 0 : 1];
  return illustaion;
}