function(quotation, planInfo, planDetails) {
  var premium = quotation.plans[0].premium;
  if (planInfo.calcBy === 'premium' && premium !== null && premium !== undefined) {
    if (planDetails[planInfo.covCode].inputConfig && planDetails[planInfo.covCode].inputConfig.premlim) {
      quotation.plans[0].premium = premium ? Math.floor(premium / 1) * 1 : 0;
      let min = planDetails[planInfo.covCode].inputConfig.premlim.min;
      if (min && min > premium) {
        quotation.plans[0].premium = min;
      }
    }
  }
}