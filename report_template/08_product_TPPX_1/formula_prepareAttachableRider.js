function(planDetail, quotation, planDetails) {
  quotDriver.prepareAttachableRider(planDetail, quotation, planDetails);
  var allowDIS = false;
  var allowSV = false;
  var riderList = planDetail.inputConfig.riderList;
  if (riderList) {
    var singlePrem = quotation.paymentMode === 'L';
    var planType = quotation.policyOptions.planType;
    var firstParty = quotation.sameAs === 'Y';
    var iEmployed = ['O1321', 'O1322', 'O675', 'O1132', 'O1450'].indexOf(quotation.iOccupation) === -1;
    var pEmployed = ['O1321', 'O1322', 'O675', 'O1132', 'O1450'].indexOf(quotation.pOccupation) === -1;
    riderList = riderList.filter(function(rider) {
      switch (rider.covCode) {
        case 'DIS':
          if (singlePrem) {
            if (quotation.plans[0].policyTerm === '15_YR' && quotation.iAge > 55) {
              return false;
            }
          }
          allowDIS = true;
          return true;
        case 'AP':
          return !singlePrem && quotation.iOccupationClass !== 'DCL';
        case 'DCB':
        case 'CIP':
          return !singlePrem;
        case 'SV':
          allowSV = planType === 'toAge' && quotation.plans[0].policyTerm === '99_TA';
          return allowSV;
        case 'WPN':
          return firstParty && !singlePrem;
        case 'UNBN':
        case 'WUN':
          return firstParty && iEmployed && !singlePrem;
        case 'PEN':
        case 'PENCI':
          return !firstParty && !singlePrem;
        case 'PUNB':
        case 'PUN':
        case 'PPU':
          return !firstParty && pEmployed && !singlePrem;
        default:
          return true;
      }
    });
    riderList = riderList.filter(function(rider) {
      switch (rider.covCode) {
        case 'UNBN':
        case 'WUN':
          return dateUtils.getAttainedAge(new Date(), new Date(quotation.iDob)).year >= 18 && dateUtils.getNearestAge(new Date(), new Date(quotation.iDob)) <= 45;
        case 'WPN':
          return dateUtils.getAttainedAge(new Date(), new Date(quotation.iDob)).year >= 16 && dateUtils.getNearestAge(new Date(), new Date(quotation.iDob)) <= 60;
        case 'PUNB':
        case 'PUN':
        case 'PPU':
          return dateUtils.getAttainedAge(new Date(), new Date(quotation.pDob)).year >= 18 && dateUtils.getNearestAge(new Date(), new Date(quotation.pDob)) <= 45;
        case 'PEN':
        case 'PENCI':
          return dateUtils.getAttainedAge(new Date(), new Date(quotation.pDob)).year >= 16 && dateUtils.getNearestAge(new Date(), new Date(quotation.pDob)) <= 60;
        default:
          return true;
      }
    });
    if (!firstParty) {
      riderList = riderList.filter(function(rider) {
        if (['PEN', 'PENCI', 'PUNB', 'PUN', 'PPU'].indexOf(rider.covCode) > -1) {
          var riderDetail = planDetails[rider.covCode];
          var polTermList = quotDriver.runFunc(riderDetail.formulas.polTermsFunc, riderDetail, quotation);
          return _.size(polTermList) > 0 && quotation.extraFlags.nationalityResidence.pRejected === 'N';
        } else {
          return true;
        }
      });
    }
  }
  planDetail.inputConfig.riderList = riderList;
  var extraFlags = quotation.extraFlags;
  if (extraFlags.reset || !extraFlags.riders) {
    extraFlags.riders = {};
  }
  var toAge99 = !!(quotation.plans[0] && quotation.plans[0].policyTerm === '99_TA');
  if (allowSV && toAge99 && !extraFlags.riders.toAge99) {
    if (!_.find(quotation.plans, p => p.covCode === 'SV')) {
      quotation.plans.push({
        covCode: 'SV'
      });
    }
  }
  extraFlags.riders.toAge99 = toAge99;
  var hasDIS = _.find(quotation.plans, p => p.covCode === 'DIS');
  if (allowDIS) {
    if (!hasDIS) {
      if (extraFlags.riders.systemRemovedDIS) {
        quotation.plans.push({
          covCode: 'DIS'
        });
        delete extraFlags.riders.systemRemovedDIS;
      } else if (!extraFlags.init && !extraFlags.reset) {
        extraFlags.riders.userRemovedDIS = true;
      }
    }
  } else if (hasDIS && !extraFlags.riders.userRemovedDIS) {
    extraFlags.riders.systemRemovedDIS = true;
  }
}