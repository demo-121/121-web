function(agent, quotation, planDetails) {
  var files = _.map(quotation.plans, (plan) => {
    var fileId;
    if (plan.covCode === quotation.baseProductCode) {
      fileId = quotation.policyOptions.planType === 'renew' ? 'prod_summary_1' : 'prod_summary_2';
    } else {
      fileId = 'prod_summary';
    }
    return {
      covCode: plan.covCode,
      fileId: fileId
    };
  });
  return [{
    id: 'prodSummary',
    label: 'Product Summary',
    fileName: 'prod_summary.pdf',
    files: files
  }];
}