function(quotation, planInfo, planDetail) {
  if (!quotation.plans || !quotation.plans[0].covClass) {
    return;
  }
  var totRiderPrem = math.bignumber(0);
  var totRiderYearPrem = math.bignumber(0);
  for (var p in quotation.plans) {
    var plan = quotation.plans[p];
    if (plan.covCode !== quotation.baseProductCode) {
      totRiderPrem = math.add(totRiderPrem, plan.premium);
      totRiderYearPrem = math.add(totRiderYearPrem, plan.yearPrem);
    }
    if (plan.covClass) {
      var mappingIndex = planDetail.planCodeMapping.covCode.indexOf(plan.covCode);
      plan.planCode = planDetail.planCodeMapping[plan.covClass][mappingIndex];
    }
  }
  quotation.totRiderPrem = math.number(totRiderPrem);
  quotation.totRiderYearPrem = math.number(totRiderYearPrem);
  quotation.totYearCashPortion = math.number(math.add(totRiderYearPrem, quotation.policyOptions.cashPortion));
  for (var c in planDetail.classList) {
    var classItem = planDetail.classList[c];
    if (classItem.covClass === planInfo.covClass) {
      planInfo.covName = classItem.className;
      break;
    }
  }
  for (var p in planDetail.inputConfig.paymentMethodList) {
    var item = planDetail.inputConfig.paymentMethodList[p];
    if (item.value === planInfo.paymentMethod) {
      planInfo.paymentMethodDesc = item.title.en;
      break;
    }
  }
  for (var p in planDetail.inputConfig.payFreqList) {
    var item = planDetail.inputConfig.payFreqList[p];
    if (item.value === planInfo.payFreq) {
      planInfo.payFreqDesc = item.title.en;
      break;
    }
  }
}