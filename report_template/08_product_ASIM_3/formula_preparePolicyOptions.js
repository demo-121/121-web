function(planDetail, quotation, planDetails) {
  var covClass = quotation.plans[0].covClass;
  var gender = quotation.iGender;
  var age = quotation.iAge;
  if (covClass) {
    var roundDown = function(bigNum, d) {
      var scale = math.pow(10, d);
      return math.divide(math.floor(math.multiply(bigNum, scale)), scale);
    };
    var getGstAmt = function(bigNum) {
      return math.round(math.multiply(bigNum, planDetail.gstRate), 2);
    };
    var medishieldLife = roundDown(math.bignumber(planDetail.rates.medishieldLifeRate[covClass][gender][age]), 2);
    var axaShield = roundDown(math.bignumber(planDetail.rates.axaShieldRate[covClass][gender][age]), 2);
    if (planDetail.gstInd === 'Y') {
      var medishieldLifeGst = getGstAmt(medishieldLife);
      medishieldLife = math.round(math.add(medishieldLife, medishieldLifeGst), 1);
      var axaShieldGst = getGstAmt(axaShield);
      axaShield = math.round(math.add(axaShield, axaShieldGst), 1);
    }
    var awl = null;
    var limits = planDetail.rates.cpfLimit[covClass];
    for (var limKey in limits) {
      if (age <= Number(limKey)) {
        awl = math.bignumber(limits[limKey]);
        break;
      }
    }
    var cashPortion, cpfPortion;
    if (math.number(axaShield) <= math.number(awl)) {
      cashPortion = math.bignumber(0);
      cpfPortion = axaShield;
    } else {
      cashPortion = math.subtract(axaShield, awl);
      cpfPortion = awl;
    }
    var medisave = math.add(medishieldLife, cpfPortion);
    quotation.policyOptions.medishieldLife = math.number(medishieldLife);
    quotation.policyOptions.axaShield = math.number(axaShield);
    quotation.policyOptions.awl = math.number(awl);
    quotation.policyOptions.cashPortion = math.number(cashPortion);
    quotation.policyOptions.cpfPortion = math.number(cpfPortion);
    quotation.policyOptions.medisave = math.number(medisave);
  }
  planDetail.inputConfig.policyOptions = planDetail.policyOptions;
}