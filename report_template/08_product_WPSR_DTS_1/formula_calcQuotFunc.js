function(quotation, planInfo, planDetails) {
  var planDetail = planDetails[planInfo.covCode];
  var rates = planDetail.rates;
  var petRates = rates.premRate;
  var sumSa = quotation.plans[0].premium;
  for (var i = 0; i < quotation.plans.length; i++) {
    if (quotation.plans[i].covCode == 'AP_DTS' && quotation.plans[i].premium !== undefined && quotation.plans[i].premium !== null && quotation.plans[i].premium !== '') {
      sumSa += quotation.plans[i].premium;
    }
  }
  var plancode = "WP";
  var ref = "WPS" + quotation.iAge + quotation.iGender + quotation.iSmoke;
  var getRate = function(ref, columnNum, Rates) {
    var planRates = Rates[ref];
    if (planRates === null) {
      return 0;
    }
    return planRates[columnNum];
  };
  var round = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };

  function calculateRiderPremium(curPlanInfo) {
    var annualPremium = 0;
    var premRate = getRate(ref, (planInfo.policyTerm - 10), petRates);
    annualPremium = trunc(math.multiply(math.bignumber(premRate), sumSa), 2);
    annualPremium = trunc(math.divide(annualPremium, math.bignumber(100)), 2);
    curPlanInfo.yearPrem = math.number(annualPremium);
    curPlanInfo.halfYearPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.51)), 2)); /** Math.floor(annualPremium * 0.51 * 100)/100; */
    curPlanInfo.quarterPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.26)), 2)); /** Math.floor(annualPremium * 0.26 * 100)/100;*/
    curPlanInfo.monthPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.0875)), 2)); /** Math.floor(annualPremium * 0.0875 * 100)/100;*/
    quotation.totYearPrem += curPlanInfo.yearPrem;
    quotation.totHalfyearPrem += curPlanInfo.halfYearPrem;
    quotation.totQuarterPrem += curPlanInfo.quarterPrem;
    quotation.totMonthPrem += curPlanInfo.monthPrem;
    switch (quotation.paymentMode) {
      case 'A':
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
      case 'S':
        {
          curPlanInfo.premium = curPlanInfo.halfYearPrem;quotation.premium += curPlanInfo.halfYearPrem;
          break;
        }
      case 'Q':
        {
          curPlanInfo.premium = curPlanInfo.quarterPrem;quotation.premium += curPlanInfo.quarterPrem;
          break;
        }
      case 'M':
        {
          curPlanInfo.premium = curPlanInfo.monthPrem;quotation.premium += curPlanInfo.monthPrem;
          break;
        }
      default:
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
    }
    curPlanInfo.planCode = plancode;
  }
  calculateRiderPremium(planInfo);
}