function(quotation, planInfo, planDetails, extraPara) {
  var hasWithdrawal = quotation.policyOptions.wdSelect;
  var hasSpTopUp = quotation.policyOptions.topUpSelect;
  var hasRspTopUp = quotation.policyOptions.rspSelect;
  var hasChangeSA = quotation.policyOptions.changeSASelect;
  var hasILoading = quotation.policyOptions.iLoading;
  var hasPLoading = quotation.policyOptions.pLoading;
  var getGender = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getDate = function(targetDate) {
    var _date = new Date(targetDate);
    return _date.getFullYear() + "-" + (_date.getMonth() + 1) + "-" + _date.getDate();
  };
  var getSmoking = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  var getPayMode = function() {
    switch (quotation.paymentMode) {
      case "A":
        return "Annual";
      case "S":
        return "Semi-Annual";
      case "Q":
        return "Quarterly";
      case "M":
        return "Monthly";
    }
  };
  var getPayModeFactor = function() {
    for (var i in planDetails[quotation.baseProductCode].payModes) {
      var payMode = planDetails[quotation.baseProductCode].payModes[i];
      if (payMode.mode === quotation.paymentMode) {
        return payMode.factor;
      }
    }
  };
  var getPlans = function() {
    var plans = [];
    for (var i in quotation.plans) {
      var plan = quotation.plans[i];
      var polTerm = "",
        premTerm = "";
      if (plan.policyTerm) {
        polTerm = plan.polTermDesc;
      }
      if (plan.premTerm) {
        premTerm = plan.premTermDesc;
      }
      plans.push({
        name: plan.covName.en,
        polTerm: polTerm,
        payMode: premTerm,
        sumInsured: getCurrency(plan.sumInsured, '', 2)
      });
    }
    return plans;
  };
  var getPercent = function(value) {
    value = math.number(value || 0);
    return value + '%';
  };
  var getFunds = function() {
    var funds = [];
    for (var i in quotation.fund.funds) {
      var fund = quotation.fund.funds[i];
      var alloc = math.number(fund.alloc || 0);
      var topUpAlloc = math.number(fund.topUpAlloc || 0);
      var fundPrem = quotation.plans[0].premium * alloc * getPayModeFactor() / 100;
      var fundRsp = (quotation.policyOptions.rspAmount || 0) * alloc * 12 / 100;
      funds.push({
        name: fund.fundName.en,
        alloc: alloc + '%',
        topUpAlloc: (hasSpTopUp ? topUpAlloc : 0) + '%',
        code: fund.fundCode,
        premium: getCurrency(fundPrem, '', 2),
        rsp: getCurrency(fundRsp, '', 2),
        topUp: getCurrency((quotation.policyOptions.topUpAmt || 0) * topUpAlloc / 100, '', 2)
      });
    }
    return funds;
  };
  var bPlan = quotation.plans[0];
  let company = extraPara.company;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb
  } = company;
  var ccy = quotation.ccy;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = 'US$';
  } else if (ccy == 'ASD') {
    polCcy = 'Australian Dollars';
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    polCcy = 'British Pound';
    ccySyb = '£';
  }
  return {
    footer: {
      compName: compName,
      compRegNo: compRegNo,
      compAddr: compAddr,
      compAddr2: compAddr2,
      compTel: compTel,
      compFax: compFax,
      compWeb: compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      planCodes: _.join(_.map(quotation.plans, 'planCode'), ', ')
    },
    cover: {
      proposer: {
        name: quotation.pFullName,
        gender: getGender(quotation.pGender),
        dob: new Date(quotation.pDob).format(extraPara.dateFormat),
        age: quotation.pAge,
        smoking: getSmoking(quotation.pSmoke)
      },
      insured: {
        name: quotation.iFullName,
        gender: getGender(quotation.iGender),
        dob: new Date(quotation.iDob).format(extraPara.dateFormat),
        age: quotation.iAge,
        smoking: getSmoking(quotation.iSmoke)
      },
      plans: getPlans(),
      funds: getFunds(),
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      withdrawal: {
        value: hasWithdrawal ? getCurrency(quotation.policyOptions.wdAmount, '', 0) : "N/A",
        from: hasWithdrawal ? quotation.policyOptions.wdFromAge : "N/A",
        to: hasWithdrawal ? quotation.policyOptions.wdToAge : "N/A"
      }
    },
    basicPlan: {
      name: bPlan.covName.en,
      sumInsured: getCurrency(bPlan.sumInsured, '', 2),
      premium: getCurrency(bPlan.premium, '', 2),
      aPremium: getCurrency(bPlan.yearPrem, '', 2),
      sPremium: getCurrency(bPlan.halfYearPrem, '', 2),
      qPremium: getCurrency(bPlan.quarterPrem, '', 2),
      mPremium: getCurrency(bPlan.monthPrem, '', 2),
      spTopUp: getCurrency(hasSpTopUp ? quotation.policyOptions.topUpAmt : 0, '', 2),
      rspTopUp: getCurrency(hasRspTopUp ? quotation.policyOptions.rspAmount : 0, '', 2),
      aRspTopUp: getCurrency(hasRspTopUp ? quotation.policyOptions.rspAmount * 12 : 0, '', 2),
      payMode: getPayMode(),
      basicBenefit: quotation.policyOptionsDesc.basicBenefit.en,
      insureCharge: quotation.policyOptionsDesc.insuranceCharge.en,
      ccy: ccy,
      ccySyb: ccySyb,
      hasOccupation: quotation.policyOptions.occupationClass != 'DCL' ? 'Y' : 'N',
      occupation: quotation.policyOptionsDesc.occupationClass.en,
      hasTopUp: hasSpTopUp ? "Y" : "N",
      hasWithdrawal: hasWithdrawal ? "Y" : "N",
      hasChangeSA: hasChangeSA ? "Y" : "N",
      hasPLoading: hasPLoading ? "Y" : "N",
      hasILoading: hasILoading ? "Y" : "N",
      policyYear: quotation.policyOptions.saPolYr
    }
  };
}