function(planDetail, quotation) {
  var policyTermList = [];
  if (quotation.plans[0].premTerm === undefined) {
    return null;
  }
  var premiumTerm = quotation.plans[0].premTerm;
  policyTermList.push({
    value: premiumTerm,
    title: premiumTerm + 'Years',
    default: true
  });
  quotation.plans[1].policyTerm = premiumTerm;
  return policyTermList;
}