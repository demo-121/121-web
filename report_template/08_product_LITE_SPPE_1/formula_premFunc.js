function(quotation, planInfo, planDetail) {
  console.log('function premFunc');
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var premium = null;
  var sumInsured = planInfo.sumInsured;
  if (planInfo.premTerm && sumInsured) {
    var residenceLoading = math.bignumber(runFunc(planDetail.formulas.getResidenceLoading, quotation, planInfo, planDetail));
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.pAge));
    var rates = planDetail.rates;
    var countryCode = quotation.pResidence;
    var countryGroup = rates.countryGroup[countryCode];
    if (Array.isArray(countryGroup)) {
      countryGroup = countryGroup[0];
    }
    var substandardClass = rates.substandardClass[countryGroup]['PE'];
    var isCountryMatch = (quotation.iResidence === "R51" || quotation.iResidence === "R52" || countryGroup === "Major City of China" || countryGroup === "Other City of China" || countryGroup === "Indonesia" || countryGroup === "Thailand");
    if (isCountryMatch) {
      var premAfterLsd = roundDown(math.multiply(residenceLoading, math.bignumber(sumInsured), 0.01), 2);
      var modalFactor = 1;
      for (var p in planDetail.payModes) {
        if (planDetail.payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetail.payModes[p].factor;
        }
      }
      var prem = roundDown(math.multiply(premAfterLsd, modalFactor), 2);
      planInfo.annualPrem = roundDown(premAfterLsd, 4);
      premium = math.number(prem);
    } else {
      var premAfterLsd = roundDown(math.multiply(premRate, math.bignumber(sumInsured), 0.01), 2);
      var modalFactor = 1;
      for (var p in planDetail.payModes) {
        if (planDetail.payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetail.payModes[p].factor;
          break;
        }
      }
      premium = roundDown(math.multiply(premAfterLsd, modalFactor), 2);
      premium = math.number(premium);
    }
  }
  return premium;
}