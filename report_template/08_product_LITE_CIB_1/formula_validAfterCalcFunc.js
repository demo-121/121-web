function(quotation, planInfo, planDetail) {
  var getDiscountRate = function() {
    var campaign = planDetail.campaign[0];
    if (!campaign) {
      return 0;
    }
    return campaign.discount;
  };
  var addCampaign2Quotation = function() {
    var covCode = planDetail.covCode;
    var plans = quotation.plans;
    plans.forEach(function(plan) {
      if (plan.covCode === covCode) {
        var campaign = planDetail.campaign[0];
        if (!!campaign && campaign.campaignId && campaign.campaignCode) {
          var campaignCode = campaign.campaignCode;
          var campaignId = campaign.campaignId;
          var campaignCodes = [];
          var campaignIds = [];
          campaignCodes.push(campaignCode);
          campaignIds.push(campaignId);
          plan.campaignCodes = campaignCodes;
          plan.campaignIds = campaignIds;
        }
      }
    });
  };
  var discount = getDiscountRate();
  if (typeof(discount) === 'number') {
    if (discount > 0) {
      addCampaign2Quotation();
    }
  }
}