function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var bpInfo = quotation.plans[0];
  var multiFactor = quotation.policyOptions.multiFactor;
  if (multiFactor) {
    var maxAdult = {
      "1": 3000000,
      "2": 1500000,
      "3": 1000000,
      "4": 750000,
      "5": 600000,
      "6": 500000,
      "7": 428000
    };
    var maxJuv = {
      "1": 500000,
      "2": 250000,
      "3": 166000,
      "4": 125000,
      "5": 100000,
      "6": 83000,
      "7": 71000
    };
    var multiFactor = quotation.policyOptions.multiFactor;
    var maxV = (quotation.iAge >= 18 ? maxAdult : maxJuv)[multiFactor];
    var maxValue = quotation.iAge < 18 ? 500000 : 3000000;
    maxValue = maxValue > maxV ? maxV : maxValue;
    if (maxValue > bpInfo.sumInsured) {
      maxValue = bpInfo.sumInsured;
    }
    if (maxValue >= 25000) {
      planDetail.inputConfig.benlim = {
        min: 25000,
        max: maxValue
      };
    }
  }
}