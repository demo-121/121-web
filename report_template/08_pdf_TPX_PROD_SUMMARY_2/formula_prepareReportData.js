function(quotation, planInfo, planDetails, extraPara) {
  var illustrations = extraPara.illustrations;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb
  } = extraPara.company;
  var retVal = {
    'compName': compName,
    'compRegNo': compRegNo,
    'compAddr': compAddr,
    'compAddr2': compAddr2,
    'compTel': compTel,
    'compFax': compFax,
    'compWeb': compWeb
  };
  retVal.headerGender = (quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.headerSmoke = (quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  var psPlanFields = [];
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    var planDetail = planDetails[planInfo.covCode];
    var planName = plan.covName.en;
    if (i === 0) {
      if (quotation.policyOptions.planType !== 'renew') {
        planName += ' (' + quotation.policyOptionsDesc.planType.en + ')';
      }
    }
    var polTerm = {
      planName: planName,
      planInd: planDetail.planInd
    };
    psPlanFields.push(polTerm);
  }
  retVal.psPlanFields = psPlanFields;
  return retVal;
}