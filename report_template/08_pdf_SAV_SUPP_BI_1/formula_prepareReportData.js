function(quotation, planInfo, planDetails, extraPara) {
  var trunc = function(value, position) {
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return Number(math.divide(math.floor(math.multiply(value, scale)), scale));
  };
  var illustrations = extraPara.illustrations;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb,
  } = extraPara.company;
  var retVal = {
    'compName': compName,
    'compRegNo': compRegNo,
    'compAddr': compAddr,
    'compAddr2': compAddr2,
    'compTel': compTel,
    'compFax': compFax,
    'compWeb': compWeb
  };
  var basicPlan = quotation.plans[0];
  var basicIllustrations = illustrations[basicPlan.covCode];
  var illustrateData_DB = [];
  var illustrateData_SV_b4_mature = [];
  var illustrateData_SV_at_mature = [];
  var polTerm = basicPlan.policyTerm ? basicPlan.policyTerm : 15;
  var projections = planDetails[planInfo.covCode].projection;
  var low = 999;
  var high = -1;
  for (var i = 0; i < 2; i++) {
    var id = projections[i].title.en;
    low = Math.min(Number(id), low);
    high = Math.max(Number(id), high);
  }
  for (var i = 0; i < polTerm; i++) {
    var polYr = i + 1;
    var basicIllustration = basicIllustrations[i];
    var polYr_age = polYr + '/' + basicIllustration.age;
    var totalPremPaid = getCurrency(trunc(Number(basicIllustration.totalPremPaid), 0), ' ', 0);
    var row = {
      polYr_age: polYr_age,
      totalPremPaid: totalPremPaid,
      GteedDB: getCurrency(trunc(Number(basicIllustration.GteedDB_SUPP), 0), ' ', 0),
      nonGteedDB_low: getCurrency(trunc(Number(basicIllustration.nonGteedDB_SUPP[low]), 0), ' ', 0),
      totalDB_low: getCurrency(trunc(Number(basicIllustration.totalDB_SUPP[low]), 0), ' ', 0),
      nonGteedDB_high: getCurrency(trunc(Number(basicIllustration.nonGteedDB_SUPP[high]), 0), ' ', 0),
      totalDB_high: getCurrency(trunc(Number(basicIllustration.totalDB_SUPP[high]), 0), ' ', 0),
    };
    if (polYr <= 10 || (polYr > 10 && polYr % 5 === 0) || polYr == polTerm) {
      illustrateData_DB.push(row);
    }
    row = {
      polYr_age: polYr_age,
      totalPremPaid: totalPremPaid,
      GteedSV: getCurrency(trunc(Number(basicIllustration.GteedSV_SUPP), 0), ' ', 0),
      nonGteedSV_low: getCurrency(trunc(Number(basicIllustration.nonGteedSV_SUPP[low]), 0), ' ', 0),
      totalSV_low: getCurrency(trunc(Number(basicIllustration.totalSV_SUPP[low]), 0), ' ', 0),
      nonGteedSV_high: getCurrency(trunc(Number(basicIllustration.nonGteedSV_SUPP[high]), 0), ' ', 0),
      totalSV_high: getCurrency(trunc(Number(basicIllustration.totalSV_SUPP[high]), 0), ' ', 0),
    };
    if (polYr <= 10 || (polYr > 10 && polYr % 5 === 0) || polYr == polTerm) {
      if (i < polTerm - 1) {
        illustrateData_SV_b4_mature.push(row);
      } else if (i == polTerm - 1) {
        illustrateData_SV_at_mature.push(row);
      }
    }
  }
  retVal.illustrateData_DB = illustrateData_DB;
  retVal.illustrateData_SV_b4_mature = illustrateData_SV_b4_mature;
  retVal.illustrateData_SV_at_mature = illustrateData_SV_at_mature;
  retVal.rate_high = (basicIllustrations[0].projRate[high]) * 100;
  retVal.rate_low = (basicIllustrations[0].projRate[low]) * 100;
  retVal.headerName = quotation.sameAs == "Y" ? quotation.pFullName : quotation.iFullName;
  retVal.headerGender = (quotation.sameAs == "Y" ? quotation.pGender : quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.headerSmoke = (quotation.sameAs == "Y" ? quotation.pSmoke : quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.headerAge = quotation.sameAs == "Y" ? quotation.pAge : quotation.iAge;
  var ccy = quotation.ccy;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = 'US$';
  } else if (ccy == 'ASD') {
    polCcy = 'Australian Dollars';
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    polCcy = 'British Pound';
    ccySyb = '£';
  }
  retVal.polCcy = polCcy;
  retVal.ccySyb = ccySyb;
  var paymentModeTitle = '';
  if (quotation.paymentMode == 'A') {
    paymentModeTitle = 'Annual';
  } else if (quotation.paymentMode == 'S') {
    paymentModeTitle = 'Semi-Annual';
  } else if (quotation.paymentMode == 'Q') {
    paymentModeTitle = 'Quarterly';
  } else if (quotation.paymentMode == 'M') {
    paymentModeTitle = 'Monthly';
  }
  retVal.paymentModeTitle = paymentModeTitle;
  retVal.basicAnnualPrem = getCurrency(planInfo.yearPrem, ' ', 2);
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  return retVal;
}