function(quotation, planInfo, planDetail) {
  quotValid.validateSALimit(quotation, planInfo, planDetail);
  quotValid.validateSAMultiple(quotation, planInfo, planDetail);
  quotValid.validatePremiumMultiple(quotation, planInfo, planDetail);
  if (planInfo.premium) {
    var totalPrem = math.bignumber(0);
    _.each(quotation.plans, plan => {
      totalPrem = math.add(totalPrem, math.bignumber(plan.premium || 0));
    });
    totalPrem = math.number(totalPrem);
    if (planDetail.inputConfig && planDetail.inputConfig.premlim) {
      let min = planDetail.inputConfig.premlim.min;
      if (min && min > totalPrem) {
        quotDriver.context.addError({
          covCode: planInfo.covCode,
          code: 'js.err.invalid_premlim_min',
          msgPara: [getCurrencyByCcy(min, 2, quotation.compCode, quotation.ccy), totalPrem]
        });
      }
    }
    var mapping = planDetail.planCodeMapping;
    for (var i = 0; i < mapping.planCode.length; i++) {
      if (planInfo.policyTerm === mapping.policyTerm[i] && planInfo.premTerm === mapping.premTerm[i]) {
        planInfo.planCode = mapping.planCode[i];
        break;
      }
    }
    planInfo.policyTermYr = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    if (planInfo.premTerm === 'SP') {
      planInfo.premTermYr = 1;
    } else if (planInfo.premTerm.endsWith('_YR')) {
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
  } else {
    planInfo.planCode = null;
  }
}