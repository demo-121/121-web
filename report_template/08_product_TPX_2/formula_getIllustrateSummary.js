function(quotation, planDetails, extraPara, illustrations) {
  var bpTdc = math.bignumber(0);
  var totRiderYearPrem = math.bignumber(0);
  var riderTdc = math.bignumber(0);
  for (var i = 0; i < illustrations[quotation.baseProductCode].length; i++) {
    var bpIll = illustrations[quotation.baseProductCode][i];
    bpTdc = math.add(bpTdc, math.bignumber(bpIll.totComm));
    var bpTotYearPrem = math.bignumber(bpIll.totYearPrem);
    var riderYearPremSum = math.bignumber(0);
    var totRiderComm = math.bignumber(0);
    for (var j = 1; j < quotation.plans.length; j++) {
      var covCode = quotation.plans[j].covCode;
      var riderIll = illustrations[covCode] && illustrations[covCode][i];
      if (riderIll) {
        if (riderIll.yearPrem) {
          riderYearPremSum = math.add(riderYearPremSum, math.bignumber(riderIll.yearPrem));
        }
        if (riderIll.totComm) {
          totRiderComm = math.add(totRiderComm, math.bignumber(riderIll.totComm));
        }
      }
    }
    totRiderYearPrem = math.add(totRiderYearPrem, riderYearPremSum);
    riderTdc = math.add(riderTdc, totRiderComm);
    var totYearPrem = math.add(bpTotYearPrem, totRiderYearPrem);
    var totTdc = math.add(bpTdc, math.floor(riderTdc));
    var ill = illustrations[quotation.baseProductCode][i];
    ill.totBpYearPrem = math.number(math.floor(bpTotYearPrem));
    ill.nonRoundBpTdc = math.number(bpTdc);
    ill.bpTdc = math.number(math.round(bpTdc));
    ill.guaranteedDB = math.floor(bpIll.guaranteedDB);
    ill.totRiderYearPrem = math.number(math.floor(totRiderYearPrem));
    ill.riderTdc = math.number(math.floor(riderTdc));
    ill.totYearPrem = math.number(math.floor(totYearPrem));
    ill.totTdc = math.number(math.round(totTdc));
    ill.guaranteedSV = 0;
  }
  return illustrations;
}