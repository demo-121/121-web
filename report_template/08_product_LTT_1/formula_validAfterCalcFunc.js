function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  planInfo.planCode = 'LTTT';
  planInfo.premTerm = planInfo.policyTerm;
  planInfo.premTermDesc = planInfo.polTermDesc;
  if (planInfo.policyTerm) {
    planInfo.policyTermYr = math.number(planInfo.policyTerm.substring(3, planInfo.policyTerm.length));
    planInfo.premTermYr = math.number(planInfo.policyTermYr);
  }
}