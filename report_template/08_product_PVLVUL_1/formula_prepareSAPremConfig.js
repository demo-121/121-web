function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation, planDetails);
  var premium = math.number(quotation.plans[0].premium || 0);
  var sa = quotation.plans[0].sumInsured;
  var rates = planDetail.rates;
  var ccy = quotation.ccy;
  var currnav_rate = rates.exchange[ccy];
  var minsa = premium * 1.5;
  var ccySign = runFunc(planDetail.formulas.getCurrencySign, quotation.ccy);
  if (premium === 0) {
    minsa = 200000 * currnav_rate * 1.5;
  } else if ((premium * 1.5) < (200000 * currnav_rate * 1.5)) {
    minsa = 200000 * currnav_rate * 1.5;
  }
  if (sa !== null && sa < minsa) {
    if (quotation.lessMinSA === 'Y' && sa === quotation.prevSumInsured) {} else {
      quotation.plans[0].sumInsured = minsa;
    }
  }
  quotation.minPrem = 200000 * currnav_rate;
  planDetail.inputConfig.planInfoHintMsg = 'Minimum Sum Assured is ' + getCurrency(minsa, ccySign, 0) + '. Sum Assured must be in multiples of 1,000.';
  return planDetail;
}