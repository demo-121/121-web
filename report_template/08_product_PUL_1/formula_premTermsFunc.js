function(planDetail, quotation) {
  var premTermsList = [];
  for (var premTerm = 5; premTerm <= 30; premTerm++) {
    if (quotation.iAge + premTerm <= 75) {
      premTermsList.push({
        value: premTerm,
        title: premTerm + ' Years'
      });
    }
  }
  if (premTermsList.length > 0) {
    premTermsList[0].default = true;
  }
  return premTermsList;
}