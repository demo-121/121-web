function(quotation, planInfo, planDetails, extraPara) {
  var planDetail = planDetails[planInfo.covCode];
  var rates = planDetail.rates;
  var feesAndCharges = rates.feesAndCharges;
  const ACCOUNT_MAINTENACE_FEE = feesAndCharges.amf;
  const INVESTMENT_MANAGEMENT_FEE = feesAndCharges.imf;
  const LOYALTY_BONUS = feesAndCharges.lb;
  const POLICY_MAINTENANCE_FEE = feesAndCharges.pmf;
  const AROR1 = rates.aror.aror1;
  const AROR2 = rates.aror.aror2;
  const BID_OFFER_FUND_FEE = feesAndCharges.bof;
  const ANNUAL_MGMT_CHARGE = feesAndCharges.amc;
  const DEATH_BENEFIT = rates.deathBenefit.deathBenefit;
  const SURRENDER_PENALTY = rates.surrenderPenalty.rate;
  var TRAILER_COMM = feesAndCharges.tc;
  var POLICY_FEE = feesAndCharges.pf_sgd;
  var DISTRIBUTION_COST = [0.2922, 0.0812, 0.0332, 0.0249, 0.0249, 0.0249, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000];
  var aror1Factor = Math.pow((1 + AROR1), 1 / 12.0);
  var aror2Factor = Math.pow((1 + AROR2), 1 / 12.0);
  var startupBonusRate = 0;
  var annualPremium = 0;
  var curAge = quotation.iAge;
  var mortalityKey = quotation.iGender;
  if (quotation.ccy == 'USD') {
    POLICY_FEE = feesAndCharges.pf_usd;
  }
  if (quotation.iSmoke == 'Y') {
    mortalityKey += 'S';
  } else {
    mortalityKey += 'NS';
  }
  var mortalityTable = rates.mortalityTable[mortalityKey];
  var getStartupBonusRate = function() {
    var temp;
    switch (quotation.paymentMode) {
      case 'A':
        {
          temp = 1;
          break;
        }
      case 'S':
        {
          temp = 2;
          break;
        }
      case 'Q':
        {
          temp = 4;
          break;
        }
      case 'M':
        {
          temp = 12;
          break;
        }
      default:
        {
          temp = 1;
          break;
        }
    }
    annualPremium = quotation.premium * temp;
    var currencyBand;
    if (quotation.ccy == 'USD') {
      currencyBand = rates.startupCurrencyBand.USD;
    } else {
      currencyBand = rates.startupCurrencyBand.SGD;
    }
    var monthlyPremium = annualPremium / 12.0;
    var index;
    var prevBand = currencyBand[0];
    var currBand = 0;
    var bandIndex = -1;
    for (index = 1; index < currencyBand.length; index++) {
      currBand = currencyBand[index];
      if ((monthlyPremium >= prevBand) && (monthlyPremium < currBand)) {
        bandIndex = index - 1;
        break;
      }
      prevBand = currBand;
    }
    if (bandIndex == -1) {
      bandIndex = 6;
    }
    var startupRateBand;
    switch (bandIndex) {
      case 0:
        {
          startupRateBand = rates.startupBonusRate.band_0;
          break;
        }
      case 1:
        {
          startupRateBand = rates.startupBonusRate.band_1;
          break;
        }
      case 2:
        {
          startupRateBand = rates.startupBonusRate.band_2;
          break;
        }
      case 3:
        {
          startupRateBand = rates.startupBonusRate.band_3;
          break;
        }
      case 4:
        {
          startupRateBand = rates.startupBonusRate.band_4;
          break;
        }
      case 5:
        {
          startupRateBand = rates.startupBonusRate.band_5;
          break;
        }
      case 6:
        {
          startupRateBand = rates.startupBonusRate.band_6;
          break;
        }
      default:
        {
          startupRateBand = rates.startupBonusRate.band_0;
          break;
        }
    }
    return startupRateBand[quotation.premTerm - 5];
  };
  var getChannelCommisionTable = function() {
    var channel = quotation.agent.dealerGroup.toUpperCase();
    var channel_table = rates.distributionCost[channel];
    if (quotation.premTerm >= 30) {
      DISTRIBUTION_COST = channel_table.premTerm30;
      return;
    }
    switch (quotation.premTerm) {
      case 5:
        {
          DISTRIBUTION_COST = channel_table.premTerm5;
          break;
        }
      case 6:
        {
          DISTRIBUTION_COST = channel_table.premTerm6;
          break;
        }
      case 7:
        {
          DISTRIBUTION_COST = channel_table.premTerm7;
          break;
        }
      case 8:
        {
          DISTRIBUTION_COST = channel_table.premTerm8;
          break;
        }
      case 9:
        {
          DISTRIBUTION_COST = channel_table.premTerm9;
          break;
        }
      case 10:
        {
          DISTRIBUTION_COST = channel_table.premTerm10;
          break;
        }
      case 11:
        {
          DISTRIBUTION_COST = channel_table.premTerm11;
          break;
        }
      case 12:
        {
          DISTRIBUTION_COST = channel_table.premTerm12;
          break;
        }
      case 13:
        {
          DISTRIBUTION_COST = channel_table.premTerm13;
          break;
        }
      case 14:
        {
          DISTRIBUTION_COST = channel_table.premTerm14;
          break;
        }
      case 15:
        {
          DISTRIBUTION_COST = channel_table.premTerm15;
          break;
        }
      case 16:
        {
          DISTRIBUTION_COST = channel_table.premTerm16;
          break;
        }
      case 17:
        {
          DISTRIBUTION_COST = channel_table.premTerm17;
          break;
        }
      case 18:
        {
          DISTRIBUTION_COST = channel_table.premTerm18;
          break;
        }
      case 19:
        {
          DISTRIBUTION_COST = channel_table.premTerm19;
          break;
        }
      case 20:
        {
          DISTRIBUTION_COST = channel_table.premTerm20;
          break;
        }
      case 21:
        {
          DISTRIBUTION_COST = channel_table.premTerm21;
          break;
        }
      case 22:
        {
          DISTRIBUTION_COST = channel_table.premTerm22;
          break;
        }
      case 23:
        {
          DISTRIBUTION_COST = channel_table.premTerm23;
          break;
        }
      case 24:
        {
          DISTRIBUTION_COST = channel_table.premTerm24;
          break;
        }
      case 25:
        {
          DISTRIBUTION_COST = channel_table.premTerm25;
          break;
        }
      case 26:
        {
          DISTRIBUTION_COST = channel_table.premTerm26;
          break;
        }
      case 27:
        {
          DISTRIBUTION_COST = channel_table.premTerm27;
          break;
        }
      case 28:
        {
          DISTRIBUTION_COST = channel_table.premTerm28;
          break;
        }
      case 29:
        {
          DISTRIBUTION_COST = channel_table.premTerm29;
          break;
        }
      case 30:
        {
          DISTRIBUTION_COST = channel_table.premTerm30;
          break;
        }
      default:
        {
          DISTRIBUTION_COST = channel_table.premTerm5;
          break;
        }
    }
    return;
  };
  var getUnitTable = function(use_aror1) {
    var arorFactor = use_aror1 ? aror1Factor : aror2Factor;
    var unitRow = {
      B_month: 0,
      C_age: curAge,
      D_netAmtAtRisk: 0,
      E_regularPremium: 0,
      F_startupBonus: 0,
      G_allocPremium: 0,
      H_initialUnits: 0,
      I_allocPremium: 0,
      J_accumUnits: 0,
      K_totalFund: 0,
      L_amf: 0,
      M_imf: 0,
      N_pmf: 0,
      O_policyFee: 0,
      P_insureCharge: 0,
      Q_unitFundRet: 0,
      R_bidOfferFee: 0,
      S_assetMgmtFee: 0,
      T_loyaltyBonus: 0,
      U_unitFundValue: 0,
      V_amf: 0,
      W_imf: 0,
      X_pmf: 0,
      Y_policyFee: 0,
      Z_insureCharge: 0,
      AA_unitFundRet: 0,
      AB_bidOfferFee: 0,
      AC_assetMgmtFee: 0,
      AD_pmfRefund: 0,
      AE_unitFundValue: 0,
      AF_pmf: 0,
      AG_policyFee: 0,
      AH_totalFundRet: 0,
      AI_amf: 0,
      AJ_imf: 0,
      AK_insureCharge: 0,
      AL_fundMgmtFee: 0,
      AM_bidOfferFee: 0,
      AN_assetMgmtFee: 0,
      AO_loyaltyBonus: 0,
      AP_pmfRefund: 0,
      AQ_totalFundValue: 0,
      AR_surrPenalty: 0,
      AS_surrValue: 0,
      AX_trailerComm: 0,
      AW_deathBenefit: 0,
      AY_annualPremium: 0,
      AZ_commision: 0,
      BA_accumDistCost: 0,
      BB_accumPremium: 0,
      BC_deduction: 0,
      BE_totalPremium: 0
    };
    const months = (100 - quotation.iAge) * 12;
    var unitRows = [];
    unitRows.push(unitRow);
    var temp;
    var monthCount = 12;
    var maxPremiumMonths = quotation.premTerm * 12;
    switch (quotation.paymentMode) {
      case 'A':
        {
          monthCount = 12;
          break;
        }
      case 'S':
        {
          monthCount = 6;
          break;
        }
      case 'Q':
        {
          monthCount = 3;
          break;
        }
      case 'M':
        {
          monthCount = 1;
          break;
        }
      default:
        {
          break;
        }
    }
    var monthIndex;
    var monthInner;
    var monthCounter = 0;
    for (var month = 1; month <= months; month++) {
      unitRows.push(Object.assign({}, unitRow));
      var currMonth = unitRows[month];
      var prevMonth = unitRows[month - 1];
      var currYear = Math.floor((month - 1) / 12) + 1;
      currMonth.B_month = month;
      monthCounter++;
      if (monthCounter > 12) {
        curAge++;
        monthCounter = 1;
      }
      currMonth.C_age = curAge;
      var D_netAmtAtRisk = 0;
      var E_regularPremium = 0;
      var F_startupBonus = 0;
      var G_allocPremium = 0;
      var H_initialUnits = 0;
      var I_allocPremium = 0;
      var J_accumUnits = 0;
      var K_totalFund = 0;
      var L_amf = 0;
      var M_imf = 0;
      var N_pmf = 0;
      var O_policyFee = 0;
      var P_insureCharge = 0;
      var Q_unitFundRet = 0;
      var R_bidOfferFee = 0;
      var S_assetMgmtFee = 0;
      var T_loyaltyBonus = 0;
      var U_unitFundValue = 0;
      var V_amf = 0;
      var W_imf = 0;
      var X_pmf = 0;
      var Y_policyFee = 0;
      var Z_insureCharge = 0;
      var AA_unitFundRet = 0;
      var AB_bidOfferFee = 0;
      var AC_assetMgmtFee = 0;
      var AD_pmfRefund = 0;
      var AE_unitFundValue = 0;
      var AF_pmf = 0;
      var AG_policyFee = 0;
      var AH_totalFundRet = 0;
      var AI_amf = 0;
      var AJ_imf = 0;
      var AK_insureCharge = 0;
      var AL_fundMgmtFee = 0;
      var AM_bidOfferFee = 0;
      var AN_assetMgmtFee = 0;
      var AO_loyaltyBonus = 0;
      var AP_pmfRefund = 0;
      var AQ_totalFundValue = 0;
      var AR_surrPenalty = 0;
      var AS_surrValue = 0;
      var AW_deathBenefit = 0;
      var AX_trailerComm = 0;
      var AY_annualPremium = 0;
      var AZ_commision = 0;
      var BA_accumDistCost = 0;
      var BB_accumPremium = 0;
      var BC_deduction = 0;
      var BE_totalPremium = 0;
      var sumE = 0;
      for (monthInner = 0; monthInner < month; monthInner++) {
        sumE += unitRows[monthInner].E_regularPremium;
      }
      D_netAmtAtRisk = Math.max(sumE - prevMonth.AQ_totalFundValue, 0);
      if (month == 1) {
        E_regularPremium = quotation.premium;
        monthIndex = 1;
      } else {
        if (month <= maxPremiumMonths) {
          if (monthIndex == monthCount) {
            E_regularPremium = quotation.premium;
            monthIndex = 1;
          } else {
            monthIndex += 1;
          }
        }
      }
      if (quotation.premTerm >= 10) {
        if (month <= 12) {
          F_startupBonus = E_regularPremium * startupBonusRate * quotation.premTerm;
        }
      }
      if (month <= 18) {
        if ((quotation.paymentMode == 'A') && (month == 13)) {
          G_allocPremium = E_regularPremium / 2;
        } else {
          G_allocPremium = E_regularPremium;
        }
      }
      H_initialUnits = prevMonth.U_unitFundValue + G_allocPremium + F_startupBonus;
      I_allocPremium = E_regularPremium - G_allocPremium;
      J_accumUnits = prevMonth.AE_unitFundValue + I_allocPremium;
      K_totalFund = prevMonth.AQ_totalFundValue + E_regularPremium + F_startupBonus;
      if (month <= maxPremiumMonths) {
        L_amf = H_initialUnits * ACCOUNT_MAINTENACE_FEE / 12.0;
      }
      M_imf = H_initialUnits * INVESTMENT_MANAGEMENT_FEE / 12.0;
      if (month <= maxPremiumMonths) {
        AF_pmf = POLICY_MAINTENANCE_FEE / 12.0 * annualPremium * currYear;
      }
      W_imf = J_accumUnits * INVESTMENT_MANAGEMENT_FEE / 12.0;
      if ((month > 18) && (month <= maxPremiumMonths)) {
        if ((J_accumUnits - W_imf - AF_pmf) > 0) {
          X_pmf = AF_pmf;
        } else {
          X_pmf = J_accumUnits - W_imf;
        }
      }
      if (month <= maxPremiumMonths) {
        N_pmf = AF_pmf - X_pmf;
      }
      AG_policyFee = POLICY_FEE;
      if (month > 18) {
        temp = J_accumUnits - (W_imf + X_pmf) - AG_policyFee;
        if (temp > 0) {
          Y_policyFee = AG_policyFee;
        } else {
          Y_policyFee = J_accumUnits - (W_imf + X_pmf);
        }
      }
      O_policyFee = AG_policyFee - Y_policyFee;
      AK_insureCharge = 0;
      if (quotation.policyOptions.deathBenefit == 'enhanced') {
        AK_insureCharge = mortalityTable[currMonth.C_age] / 12.0 * D_netAmtAtRisk;
      }
      temp = J_accumUnits - (W_imf + X_pmf + Y_policyFee);
      if (month > 18) {
        if ((temp - AK_insureCharge) > 0) {
          Z_insureCharge = AK_insureCharge;
        } else {
          Z_insureCharge = temp;
        }
      }
      P_insureCharge = AK_insureCharge - Z_insureCharge;
      var sumL_to_P = L_amf + M_imf + N_pmf + O_policyFee + P_insureCharge;
      Q_unitFundRet = (H_initialUnits - sumL_to_P) * (arorFactor - 1);
      R_bidOfferFee = (H_initialUnits - sumL_to_P + Q_unitFundRet) * BID_OFFER_FUND_FEE / 12.0;
      S_assetMgmtFee = (H_initialUnits - sumL_to_P + Q_unitFundRet) * ANNUAL_MGMT_CHARGE / 12.0;
      if ((quotation.premTerm >= 20) && (month == 120)) {
        var E_total = 0;
        for (var index = 1; index < month; index++) {
          E_total += unitRows[index].E_regularPremium;
        }
        E_total += E_regularPremium;
        T_loyaltyBonus = E_total * LOYALTY_BONUS;
      }
      U_unitFundValue = H_initialUnits - sumL_to_P + Q_unitFundRet - (R_bidOfferFee + S_assetMgmtFee) + T_loyaltyBonus;
      var sumV_to_Z = V_amf + W_imf + X_pmf + Y_policyFee + Z_insureCharge;
      AA_unitFundRet = (J_accumUnits - sumV_to_Z) * (arorFactor - 1);
      AB_bidOfferFee = (J_accumUnits - sumV_to_Z + AA_unitFundRet) * BID_OFFER_FUND_FEE / 12.0;
      AC_assetMgmtFee = (J_accumUnits - sumV_to_Z + AA_unitFundRet) * ANNUAL_MGMT_CHARGE / 12.0;
      if (month == maxPremiumMonths) {
        var N_total = 0,
          X_total = 0;
        for (var index = 1; index < maxPremiumMonths; index++) {
          N_total += unitRows[index].N_pmf;
          X_total += unitRows[index].X_pmf;
        }
        N_total += N_pmf;
        X_total += X_pmf;
        AD_pmfRefund = N_total + X_total;
      }
      AE_unitFundValue = J_accumUnits - sumV_to_Z + AA_unitFundRet - (AB_bidOfferFee + AC_assetMgmtFee) + AD_pmfRefund;
      AH_totalFundRet = Q_unitFundRet + AA_unitFundRet;
      AI_amf = L_amf + V_amf;
      AJ_imf = M_imf + W_imf;
      AM_bidOfferFee = R_bidOfferFee + AB_bidOfferFee;
      AN_assetMgmtFee = S_assetMgmtFee + AC_assetMgmtFee;
      AO_loyaltyBonus = T_loyaltyBonus;
      AP_pmfRefund = AD_pmfRefund;
      var sumAI_to_AN = AI_amf + AJ_imf + AK_insureCharge + AL_fundMgmtFee + AM_bidOfferFee + AN_assetMgmtFee;
      AQ_totalFundValue = K_totalFund - AF_pmf - AG_policyFee + AH_totalFundRet - sumAI_to_AN + AO_loyaltyBonus + AP_pmfRefund;
      if (currYear == 1) {
        temp = 1;
      } else {
        if (month >= maxPremiumMonths) {
          temp = 0;
        } else {
          var year_index = quotation.premTerm - currYear;
          temp = SURRENDER_PENALTY[year_index];
        }
      }
      AR_surrPenalty = U_unitFundValue * temp;
      AS_surrValue = AQ_totalFundValue - AR_surrPenalty;
      if ((currYear <= quotation.premTerm) && (month % 12 == 1)) {
        AY_annualPremium = annualPremium;
        currMonth.AY_annualPremium = AY_annualPremium;
      }
      AX_trailerComm = TRAILER_COMM / 12.0 * K_totalFund;
      temp = AQ_totalFundValue * DEATH_BENEFIT;
      if (quotation.policyOptions.deathBenefit == "basic") {
        AW_deathBenefit = temp;
      } else {
        var AY_total = 0;
        for (var index = 1; index <= month; index++) {
          AY_total += unitRows[index].AY_annualPremium;
        }
        AW_deathBenefit = Math.max(AY_total, temp);
      }
      temp = 0;
      if ((AY_annualPremium > 0) && ((month % 12) == 1)) {
        if (currYear <= 11) {
          temp = DISTRIBUTION_COST[currYear - 1];
        } else {
          temp = DISTRIBUTION_COST[10];
        }
      }
      AZ_commision = AY_annualPremium * temp;
      BA_accumDistCost = AZ_commision + AX_trailerComm + prevMonth.BA_accumDistCost;
      BB_accumPremium = (prevMonth.BB_accumPremium + E_regularPremium) * arorFactor;
      BC_deduction = BB_accumPremium - AS_surrValue;
      var E_total = 0;
      for (var index = 1; index < month; index++) {
        E_total += unitRows[index].E_regularPremium;
      }
      BE_totalPremium = E_total + E_regularPremium;
      currMonth.D_netAmtAtRisk = D_netAmtAtRisk;
      currMonth.E_regularPremium = E_regularPremium;
      currMonth.F_startupBonus = F_startupBonus;
      currMonth.G_allocPremium = G_allocPremium;
      currMonth.H_initialUnits = H_initialUnits;
      currMonth.I_allocPremium = I_allocPremium;
      currMonth.J_accumUnits = J_accumUnits;
      currMonth.K_totalFund = K_totalFund;
      currMonth.L_amf = L_amf;
      currMonth.M_imf = M_imf;
      currMonth.N_pmf = N_pmf;
      currMonth.O_policyFee = O_policyFee;
      currMonth.P_insureCharge = P_insureCharge;
      currMonth.Q_unitFundRet = Q_unitFundRet;
      currMonth.R_bidOfferFee = R_bidOfferFee;
      currMonth.S_assetMgmtFee = S_assetMgmtFee;
      currMonth.T_loyaltyBonus = T_loyaltyBonus;
      currMonth.U_unitFundValue = U_unitFundValue;
      currMonth.V_amf = V_amf;
      currMonth.W_imf = W_imf;
      currMonth.X_pmf = X_pmf;
      currMonth.Y_policyFee = Y_policyFee;
      currMonth.Z_insureCharge = Z_insureCharge;
      currMonth.AA_unitFundRet = AA_unitFundRet;
      currMonth.AB_bidOfferFee = AB_bidOfferFee;
      currMonth.AC_assetMgmtFee = AC_assetMgmtFee;
      currMonth.AD_pmfRefund = AD_pmfRefund;
      currMonth.AE_unitFundValue = AE_unitFundValue;
      currMonth.AF_pmf = AF_pmf;
      currMonth.AG_policyFee = AG_policyFee;
      currMonth.AH_totalFundRet = AH_totalFundRet;
      currMonth.AI_amf = AI_amf;
      currMonth.AJ_imf = AJ_imf;
      currMonth.AK_insureCharge = AK_insureCharge;
      currMonth.AL_fundMgmtFee = AL_fundMgmtFee;
      currMonth.AM_bidOfferFee = AM_bidOfferFee;
      currMonth.AN_assetMgmtFee = AN_assetMgmtFee;
      currMonth.AO_loyaltyBonus = AO_loyaltyBonus;
      currMonth.AP_pmfRefund = AP_pmfRefund;
      currMonth.AQ_totalFundValue = AQ_totalFundValue;
      currMonth.AR_surrPenalty = AR_surrPenalty;
      currMonth.AS_surrValue = AS_surrValue;
      currMonth.AW_deathBenefit = AW_deathBenefit;
      currMonth.AX_trailerComm = AX_trailerComm;
      currMonth.AY_annualPremium = AY_annualPremium;
      currMonth.AZ_commision = AZ_commision;
      currMonth.BA_accumDistCost = BA_accumDistCost;
      currMonth.BB_accumPremium = BB_accumPremium;
      currMonth.BC_deduction = BC_deduction;
      currMonth.BE_totalPremium = BE_totalPremium;
    } /* debug if (!use_aror1){ var s; s = ''; s += 'B_month,'; s += 'C_age,'; s += 'D_netAmtAtRisk,'; s += 'E_regularPremium,'; s += 'F_startupBonus,'; s += 'G_allocPremium,'; s += 'H_initialUnits,'; s += 'I_allocPremium,'; s += 'J_accumUnits,'; s += 'K_totalFund,'; s += 'L_amf,'; s += 'M_imf,'; s += 'N_pmf,'; s += 'O_policyFee,'; s += 'P_insureCharge,'; s += 'Q_unitFundRet,'; s += 'R_bidOfferFee,'; s += 'S_assetMgmtFee,'; s += 'T_loyaltyBonus,'; s += 'U_unitFundValue,'; s += 'V_amf,'; s += 'W_imf,'; s += 'X_pmf,'; s += 'Y_policyFee,'; s += 'Z_insureCharge,'; s += 'AA_unitFundRet,'; s += 'AB_bidOfferFee,'; s += 'AC_assetMgmtFee,'; s += 'AD_pmfRefund,'; s += 'AE_unitFundValue,'; s += 'AF_pmf,'; s += 'AG_policyFee,'; s += 'AH_totalFundRet,'; s += 'AI_amf,'; s += 'AJ_imf,'; s += 'AK_insureCharge,'; s += 'AL_fundMgmtFee,'; s += 'AM_bidOfferFee,'; s += 'AN_assetMgmtFee,'; s += 'AO_loyaltyBonus,'; s += 'AP_pmfRefund,'; s += 'AQ_totalFundValue,'; s += 'AR_surrPenalty,'; s += 'AS_surrValue,'; s += 'AW_deathBenefit,'; s += 'AX_trailerComm,'; s += 'AY_annualPremium,'; s += 'AZ_commision,'; s += 'BA_accumDistCost'; s += 'BB_accumPremium,'; s += 'BC_deduction,'; s += 'BE_totalPremium\n'; for (month = 1; month <= months; month++){ currMonth = unitRows[month]; s += currMonth.B_month + ','; s += currMonth.C_age + ','; s += (currMonth.D_netAmtAtRisk).toFixed(2) + ','; s += (currMonth.E_regularPremium).toFixed(2) + ','; s += (currMonth.F_startupBonus).toFixed(2) + ','; s += (currMonth.G_allocPremium).toFixed(2) + ','; s += (currMonth.H_initialUnits).toFixed(2) + ','; s += (currMonth.I_allocPremium).toFixed(2) + ','; s += (currMonth.J_accumUnits).toFixed(2) + ','; s += (currMonth.K_totalFund).toFixed(2) + ','; s += (currMonth.L_amf).toFixed(2) + ','; s += (currMonth.M_imf).toFixed(2) + ','; s += (currMonth.N_pmf).toFixed(2) + ','; s += (currMonth.O_policyFee).toFixed(2) + ','; s += (currMonth.P_insureCharge).toFixed(2) + ','; s += (currMonth.Q_unitFundRet).toFixed(2) + ','; s += (currMonth.R_bidOfferFee).toFixed(2) + ','; s += (currMonth.S_assetMgmtFee).toFixed(2) + ','; s += (currMonth.T_loyaltyBonus).toFixed(2) + ','; s += (currMonth.U_unitFundValue).toFixed(2) + ','; s += (currMonth.V_amf).toFixed(2) + ','; s += (currMonth.W_imf).toFixed(2) + ','; s += (currMonth.X_pmf).toFixed(2) + ','; s += (currMonth.Y_policyFee).toFixed(2) + ','; s += (currMonth.Z_insureCharge).toFixed(2) + ','; s += (currMonth.AA_unitFundRet).toFixed(2) + ','; s += (currMonth.AB_bidOfferFee).toFixed(2) + ','; s += (currMonth.AC_assetMgmtFee).toFixed(2) + ','; s += (currMonth.AD_pmfRefund).toFixed(2) + ','; s += (currMonth.AE_unitFundValue).toFixed(2) + ','; s += (currMonth.AF_pmf).toFixed(2) + ','; s += (currMonth.AG_policyFee).toFixed(2) + ','; s += (currMonth.AH_totalFundRet).toFixed(2) + ','; s += (currMonth.AI_amf).toFixed(2) + ','; s += (currMonth.AJ_imf).toFixed(2) + ','; s += (currMonth.AK_insureCharge).toFixed(2) + ','; s += (currMonth.AL_fundMgmtFee).toFixed(2) + ','; s += (currMonth.AM_bidOfferFee).toFixed(2) + ','; s += (currMonth.AN_assetMgmtFee).toFixed(2) + ','; s += (currMonth.AO_loyaltyBonus).toFixed(2) + ','; s += (currMonth.AP_pmfRefund).toFixed(2) + ','; s += (currMonth.AQ_totalFundValue).toFixed(2) + ','; s += (currMonth.AR_surrPenalty).toFixed(2) + ','; s += (currMonth.AS_surrValue).toFixed(2) + ','; s += (currMonth.AW_deathBenefit).toFixed(2) + ','; s += (currMonth.AX_trailerComm).toFixed(2) + ','; s += (currMonth.AY_annualPremium).toFixed(2) + ','; s += (currMonth.AZ_commision).toFixed(2) + ','; s += (currMonth.BA_accumDistCost).toFixed(2) + ','; s += (currMonth.BB_accumPremium).toFixed(2) + ','; s += (currMonth.BC_deduction).toFixed(2) + ','; s += (currMonth.BE_totalPremium).toFixed(2) + '\n'; } console.log('\n\n\n\n'); console.log(s); } */
    return unitRows;
  };
  startupBonusRate = getStartupBonusRate();
  getChannelCommisionTable();
  var unitTable4pc = getUnitTable(true);
  curAge = quotation.iAge;
  var unitTable8pc = getUnitTable(false);
  var getReductionYieldTable = function() {
    var yearRow = {
      B_year: 0,
      C_premium: 0,
      D_surrValue: 0,
      E_yield: 0
    };
    var yearRows = [];
    yearRows.push(yearRow);
    var year;
    var max_year = 20;
    var temp = 65 - quotation.iAge;
    var month;
    if (temp > max_year) {
      max_year = temp;
    }
    for (year = 1; year <= 100; year++) {
      yearRows.push(Object.assign({}, yearRow));
      var currYear = yearRows[year];
      var prevYear = yearRows[year - 1];
      currYear.B_year = year;
      var C_premium = 0;
      var D_surrValue = 0;
      var E_yield = 0;
      if (year <= quotation.premTerm) {
        C_premium = -1 * annualPremium;
      }
      if (year <= max_year) {
        month = year * 12;
        D_surrValue = unitTable8pc[month].AS_surrValue;
        E_yield = C_premium;
      } else {
        E_yield = prevYear.D_surrValue;
      }
      currYear.C_premium = C_premium;
      currYear.D_surrValue = D_surrValue;
      currYear.E_yield = E_yield;
    }
    return yearRows;
  };
  var max_year = 20;
  var temp = 65 - quotation.iAge;
  var month;
  if (temp > max_year) {
    max_year = temp;
  }
  month = max_year * 12;
  var totalDistribution = Math.round(unitTable8pc[month].BC_deduction);
  var yieldTable = getReductionYieldTable();
  var IRRCalc = function(CArray) {
    var min = 0.0;
    var max = 1.0;
    var guess;
    var NPV;
    do {
      guess = (min + max) / 2;
      NPV = 0;
      for (var j = 0; j < CArray.length; j++) {
        NPV += CArray[j] / Math.pow((1 + guess), j);
      }
      if (NPV > 0) {
        min = guess;
      } else {
        max = guess;
      }
    } while (Math.abs(NPV) > 0.000001);
    return guess * 100;
  };
  var data = [];
  var year;
  for (year = 1; year <= 100; year++) {
    data.push(yieldTable[year].E_yield);
  }
  var irr = IRRCalc(data);
  var checkSustainable = function() {
    var month;
    for (month = 1; month <= 360; month++) {
      if (unitTable4pc[month].AQ_totalFundValue < 0) {
        return false;
      }
    }
    return true;
  };
  if (checkSustainable() == false) {
    return {
      errorMsg: 'RP Not Sustainable for 30 Years. Please Increase RP!'
    };
  }
  var illustration = [];
  var defaultIllustrationData = {
    policyYear: 0,
    age: 0,
    totalPremiumPaidToDate: 0,
    nonGuaranteedSurValue: null,
    nonGuaranteedAccValue: null,
    totalDeathBenefit: null,
    valueOfPremiums: null,
    effectOfDeductions: null,
    totalDistributionCost: 0
  };
  for (var i = 0; i < Number(planInfo.policyTerm); i++) {
    var currYear = Object.assign({}, defaultIllustrationData);
    illustration.push(currYear);
    var policyYear = i + 1;
    var monthIndex = policyYear * 12;
    var unitMonth4pc = unitTable4pc[monthIndex];
    var unitMonth8pc = unitTable8pc[monthIndex];
    var value4pc = 0;
    var value8pc = 0;
    currYear.policyYear = policyYear;
    currYear.age = quotation.iAge + policyYear;
    currYear.totalPremiumPaidToDate = unitMonth4pc.BE_totalPremium;
    if (unitMonth4pc.AQ_totalFundValue <= 0) {
      value4pc = 0;
    } else {
      value4pc = unitMonth4pc.AS_surrValue;
    }
    if (unitMonth8pc.AQ_totalFundValue <= 0) {
      value8pc = 0;
    } else {
      value8pc = unitMonth8pc.AS_surrValue;
    } /** Rounding starts at 10 cents only. Decimals after 10 cents are truncated*/
    currYear.nonGuaranteedSurValue = [];
    currYear.nonGuaranteedSurValue.push(Math.round(Math.floor(value4pc * 10) / 10));
    currYear.nonGuaranteedSurValue.push(Math.round(Math.floor(value8pc * 10) / 10));
    if (unitMonth4pc.AQ_totalFundValue <= 0) {
      value4pc = 0;
    } else {
      value4pc = unitMonth4pc.AQ_totalFundValue;
    }
    if (unitMonth8pc.AQ_totalFundValue <= 0) {
      value8pc = 0;
    } else {
      value8pc = unitMonth8pc.AQ_totalFundValue;
    }
    currYear.nonGuaranteedAccValue = [];
    currYear.nonGuaranteedAccValue.push(Math.round(Math.floor(value4pc * 10) / 10));
    currYear.nonGuaranteedAccValue.push(Math.round(Math.floor(value8pc * 10) / 10));
    if (unitMonth4pc.AQ_totalFundValue <= 0) {
      value4pc = 0;
    } else {
      value4pc = unitMonth4pc.AW_deathBenefit;
    }
    if (unitMonth8pc.AQ_totalFundValue <= 0) {
      value8pc = 0;
    } else {
      value8pc = unitMonth8pc.AW_deathBenefit;
    }
    currYear.totalDeathBenefit = [];
    currYear.totalDeathBenefit.push(Math.round(Math.floor(value4pc * 10) / 10));
    currYear.totalDeathBenefit.push(Math.round(Math.floor(value8pc * 10) / 10));
    if (unitMonth4pc.AQ_totalFundValue <= 0) {
      value4pc = 0;
    } else {
      value4pc = unitMonth4pc.BB_accumPremium;
    }
    if (unitMonth8pc.AQ_totalFundValue <= 0) {
      value8pc = 0;
    } else {
      value8pc = unitMonth8pc.BB_accumPremium;
    }
    currYear.valueOfPremiums = [];
    currYear.valueOfPremiums.push(Math.round(Math.floor(value4pc * 10) / 10));
    currYear.valueOfPremiums.push(Math.round(Math.floor(value8pc * 10) / 10));
    if (unitMonth4pc.AQ_totalFundValue <= 0) {
      value4pc = 0;
    } else {
      value4pc = unitMonth4pc.BC_deduction;
    }
    if (unitMonth8pc.AQ_totalFundValue <= 0) {
      value8pc = 0;
    } else {
      value8pc = unitMonth8pc.BC_deduction;
    }
    currYear.effectOfDeductions = [];
    currYear.effectOfDeductions.push(Math.round(Math.floor(value4pc * 10) / 10));
    currYear.effectOfDeductions.push(Math.round(Math.floor(value8pc * 10) / 10));
    currYear.totalDistributionCost = Math.round(Math.floor(unitMonth8pc.BA_accumDistCost * 10) / 10);
  }
  return {
    'illustration': illustration,
    'totalDistribution': totalDistribution,
    'reductionYield': Math.round(irr * 100) / 100
  };
}