function(planDetail, quotation, planDetails) {
  quotDriver.preparePolicyOptions(planDetail, quotation, planDetails); /*Update planDetail.policyOptions*/
  planDetail.policyOptions.forEach(function(pPolicyOption) {
    if (pPolicyOption.id === 'insuranceCharge') {
      if (quotation.policyOptions.deathBenefit === 'basic') {
        pPolicyOption.disable = 'Y';
        quotation.policyOptions.insuranceCharge = null;
        pPolicyOption.options = [];
      } else if (quotation.policyOptions.deathBenefit === 'enhanced') {
        pPolicyOption.disable = 'N';
      }
    }
  });
  quotation.policyOptions.paymentMethod = 'cash';
  planDetail.inputConfig.policyOptions = planDetail.policyOptions;
  planDetail.inputConfig.topUpSelect = quotation.policyOptions.topUpSelec = false;
  return planDetail;
}