function(quotation, planInfo, planDetails, extraPara) { /*test pvtvul death**/
  var illust = extraPara.illustrations[planInfo.covCode];
  var company = extraPara.company;
  var sameAs = quotation.sameAs;
  var polType = (quotation.policyOptions.policytype == "joint") ? false : true;
  var dBenefitLeft = [];
  var dBenefitRight = [];
  var dBenefitLeft2 = [];
  var dBenefitRight2 = [];
  var policyTerm = parseInt(planInfo.policyTerm);
  var i;
  var yearCount = 0;
  var yearCount2 = 0;
  var polTerm = 0;
  var incRowCnt = 0;
  var incRowRiderCnt = 0;
  var incRowAnnualCnt = 0;
  var tdcbasicCnt = 0;
  var LA1age = quotation.pAge;
  var LA2age = quotation.iAge;
  var getCurrencyDesc = function(ccy) {
    var sign = "$";
    if (ccy == "SGD") {
      sign = "S$";
    } else if (ccy === "USD") {
      sign = "US$";
    } else if (ccy === "GBP") {
      sign = "£";
    } else if (ccy === "EUR") {
      sign = "€";
    } else if (ccy === "AUD") {
      sign = "A$";
    }
    return sign;
  };
  for (i = 0; i < illust.length; i++) {
    var row = illust[i];
    var policyYear = row.policyYear;
    var InsuranceCharge = row.ICharge;
    var incRow = false;
    if (i < 50) {
      dBenefitLeft.push({
        policyYear: policyYear,
        LA1age: LA1age,
        LA2age: LA2age,
        InsuranceCharge: InsuranceCharge
      });
    } else if (i < 100) {
      dBenefitRight.push({
        policyYear: policyYear,
        LA1age: LA1age,
        LA2age: LA2age,
        InsuranceCharge: InsuranceCharge
      });
    } else if (i < 150) {
      dBenefitLeft2.push({
        policyYear: policyYear,
        LA1age: LA1age,
        LA2age: LA2age,
        InsuranceCharge: InsuranceCharge
      });
    } else if (i < 200) {
      dBenefitLeft2.push({
        policyYear: policyYear,
        LA1age: LA1age,
        LA2age: LA2age,
        InsuranceCharge: InsuranceCharge
      });
    }
    LA1age++;
    LA2age++;
  }
  if (dBenefitLeft.length < 50 || dBenefitRight.length < 50 || dBenefitLeft2.length < 50 || dBenefitRight.length2 < 50) {
    var leftCount = 50 - dBenefitLeft.length;
    var rightCount = 50 - dBenefitRight.length;
    var leftCount2 = 50 - dBenefitLeft2.length;
    var rightCount2 = 50 - dBenefitRight2.length;
    for (var cnt = 0; cnt < leftCount; cnt++) {
      dBenefitLeft.push({
        policyYear: "",
        LA1age: "",
        LA2age: "",
        InsuranceCharge: ""
      });
    }
    for (var cnt = 0; cnt < rightCount; cnt++) {
      dBenefitRight.push({
        policyYear: "",
        LA1age: "",
        LA2age: "",
        InsuranceCharge: ""
      });
    }
    for (var cnt = 0; cnt < leftCount2; cnt++) {
      dBenefitLeft2.push({
        policyYear: "",
        LA1age: "",
        LA2age: "",
        InsuranceCharge: ""
      });
    }
    for (var cnt = 0; cnt < rightCount2; cnt++) {
      dBenefitRight2.push({
        policyYear: "",
        LA1age: "",
        LA2age: "",
        InsuranceCharge: ""
      });
    }
  }
  var result = [];
  result = {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      releaseVersion: "1"
    },
    cover: {
      sameAs: quotation.sameAs,
      ccy: quotation.ccy,
      ccySign: getCurrencyDesc(quotation.ccy)
    },
    illustration: {
      dBenefitLeft: dBenefitLeft,
      dBenefitRight: dBenefitRight,
      dBenefitLeft2: dBenefitLeft2,
      dBenefitRight2: dBenefitRight2
    },
    plans: quotation.plans.map(function(plan) {
      return {
        name: plan.covName.en,
        polTermDesc: plan.polTermDesc,
        sumInsured: getCurrencyDesc(plan.sumInsured, 0),
        initPremium: getCurrencyDesc(plan.premium, 0)
      };
    })
  };
  if (polType) {
    result.hidePagesIndexArray = [0, 1];
  } else if (illust.length < 100) {
    result.hidePagesIndexArray = [1];
  }
  return result;
}