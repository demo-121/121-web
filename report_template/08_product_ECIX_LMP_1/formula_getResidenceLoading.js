function(quotation, planInfo, planDetail, age) {
  var rates = planDetail.rates;
  var countryCode = quotation.iResidence;
  var countryGroup = rates.countryGroup[countryCode];
  var substandardClass = rates.substandardClass[countryGroup]['ECIBX'];
  var rateKey = planInfo.planCodeRL + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS') + substandardClass;
  if (planDetail.rates.residentialLoading[rateKey] && planDetail.rates.residentialLoading[rateKey][quotation.iAge]) {
    return planDetail.rates.residentialLoading[rateKey][quotation.iAge];
  }
  return 0;
}