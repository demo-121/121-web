function(quotation, planInfo, planDetail) {
  quotValid.validateMandatoryFields(quotation, planInfo, planDetail);
  var premium = math.number(quotation.plans[0].premium || 0);
  var sa = math.number(quotation.plans[0].sumInsured || 0);
  var rates = planDetail.rates;
  var ccy = quotation.ccy;
  var currnav_rate = rates.exchange[ccy];
  var minsa = premium * 1.5;
  var ccySign = runFunc(planDetail.formulas.getCurrencySign, quotation.ccy);
  if (premium === 0) {
    minsa = 1000000 * currnav_rate * 1.5;
  } else if ((premium * 1.5) < (1000000 * currnav_rate * 1.5)) {
    minsa = 1000000 * currnav_rate * 1.5;
  }
  planDetail.inputConfig.planInfoHintMsg = 'Minimum Sum Assured is ' + getCurrency(minsa, ccySign, 0) + '. Sum Assured must be in multiples of 1,000.';
  if (quotation.policyOptions.withdrawalOption === 'percentage' && planInfo.premium !== 'undefined' && Number(planInfo.premium) !== 0 && planInfo.sumInsured !== 'undefined' && Number(planInfo.sumInsured) !== 0 && quotation.lessMinSA === 'N') {
    var validmessage = runFunc(planDetail.formulas.valid_Min_WD, quotation, planInfo, planDetail);
    if (validmessage !== 'N' && validmessage !== 'undefined' && validmessage !== null) {
      quotDriver.context.addError({
        covCode: quotation.baseProductCode,
        msg: validmessage
      });
    }
  }
  return planDetail;
}