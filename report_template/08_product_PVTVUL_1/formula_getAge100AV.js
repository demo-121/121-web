function(quotation, planInfo, planDetail, in_IRR) {
  var rates = planDetail.rates;
  var premium = planInfo.premium;
  var sumSa = planInfo.sumInsured;
  var policytype = quotation.policyOptions.policytype;
  var riskClass = quotation.policyOptions.riskClassification;
  var riskClass2 = quotation.policyOptions.riskClassification2;
  var la1_em_perc = Number(quotation.policyOptions.extraMortality);
  var la1_pml = Number(quotation.policyOptions.loading);
  var pml_period = Number(quotation.policyOptions.loadingPeriod);
  var la1_em_perc2 = Number(quotation.policyOptions.extraMortality2);
  var la1_pml2 = Number(quotation.policyOptions.loading2);
  var pml_period2 = Number(quotation.policyOptions.loadingPeriod2);
  if (isNaN(la1_em_perc)) {
    la1_em_perc = 0;
  }
  if (isNaN(la1_pml)) {
    la1_pml = 0;
  }
  if (isNaN(pml_period)) {
    pml_period = 0;
  }
  if (isNaN(la1_em_perc2)) {
    la1_em_perc2 = 0;
  }
  if (isNaN(la1_pml2)) {
    la1_pml2 = 0;
  }
  if (isNaN(pml_period2)) {
    pml_period2 = 0;
  }
  var coiRate = function(rates, countryClass, age, riskClass, smoke, gender) {
    var colnum = 1;
    if (gender + smoke === 'MN') {
      colnum = 1;
    } else if (gender + smoke === 'MY') {
      colnum = 2;
    } else if (gender + smoke === 'FN') {
      colnum = 3;
    } else if (gender + smoke === 'FY') {
      colnum = 4;
    }
    if (riskClass === 'P') {
      colnum += 4;
    } else if (riskClass === 'S') {
      colnum += 8;
    } else if (riskClass === 'SS') {
      colnum += 12;
    }
    age = math.min(120, age);
    return rates.COI[countryClass]['' + age][colnum - 1];
  };
  var polcyYear = function(month) {
    return math.floor((month - 1) / 12) + 1;
  };
  var getPrevTpRate = function(month, rawData, type) {
    if (type === 'X') {
      return rawData[month - 12].TPX;
    } else if (type === 'Y') {
      return rawData[month - 12].TPY;
    } else if (type === 'XY') {
      return rawData[month - 12].TPXY;
    }
  };
  var roundup = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.ceil(math.multiply(value, scale)), scale);
  };
  var calcIllustration = function(month, in_IRR) {
    var age100av = 0;
    var pfd_rate = rates.fee['PolicyFeeDuration'];
    var ef_isp_rate = rates.fee['EF_ISP'];
    var fpisp_rate = rates.fee['PolicyFee'];
    var oafisp_rate = rates.fee['OAF_ISP'];
    var iCountryClass = rates.countryClass[quotation.iResidence];
    var pCountryClass = null;
    if (policytype === 'joint') {
      pCountryClass = rates.countryClass[quotation.pResidence];
    }
    var rawData = [];
    var defaultRawData = {
      plYear: 0,
      plMonth: 0,
      iAge: 0,
      pAge: 0,
      allocated_Premium_BOM: 0,
      Initial_Premium: 0,
      elm_Fee: 0,
      account_Value_EOM: 0,
      account_Value_BOM: 0,
      sum_Risk_BOM: 0,
      account_Value_WD: 0,
      policy_Fee: 0,
      Trailer_Fee: 0,
      Fund_Return: 0,
      Ongoing_Fee: 0,
      Cost_Insurance: 0,
      TPX: 0,
      TPY: 0,
      TPXY: 0,
      QXYT_1: 0
    };
    rawData.push(Object.assign({}, defaultRawData));
    for (var m = 1; m <= month; m++) {
      var prevMonth = rawData[m - 1];
      var currMonth = Object.assign({}, defaultRawData);
      currMonth.plYear = polcyYear(m);
      currMonth.plMonth = m;
      currMonth.iAge = quotation.iAge + currMonth.plYear - 1;
      currMonth.pAge = quotation.pAge + currMonth.plYear - 1;
      var coi_rate = 0;
      if (policytype === 'single') {
        coi_rate = coiRate(rates, iCountryClass, currMonth.iAge, riskClass, quotation.iSmoke, quotation.iGender);
        coi_rate = roundup(coi_rate * (1 + la1_em_perc / 100), 9);
        if (currMonth.iAge < quotation.iAge + pml_period) {
          coi_rate = roundup(coi_rate + la1_pml, 9);
        }
        coi_rate = math.min(coi_rate, 1000);
      } else {
        var coi_rate_x = coiRate(rates, pCountryClass, currMonth.pAge, riskClass, quotation.pSmoke, quotation.pGender);
        coi_rate_x = roundup(coi_rate_x * (1 + (la1_em_perc / 100)), 9);
        if (currMonth.pAge < quotation.pAge + pml_period) {
          coi_rate_x = roundup(coi_rate_x + la1_pml, 9);
        }
        coi_rate_x = math.min(coi_rate_x, 1000);
        var coi_rate_Y = coiRate(rates, iCountryClass, currMonth.iAge, riskClass2, quotation.iSmoke, quotation.iGender);
        coi_rate_Y = roundup(coi_rate_Y * (1 + (la1_em_perc2 / 100)), 9);
        if (currMonth.iAge < quotation.iAge + pml_period2) {
          coi_rate_Y = roundup(coi_rate_Y + la1_pml2, 9);
        }
        coi_rate_Y = math.min(coi_rate_Y, 1000);
        var prevTpx = 1;
        var prevTpy = 1;
        var prevTpxy = 1;
        if (currMonth.plYear !== 1) {
          prevTpx = getPrevTpRate(m, rawData, 'X');
          prevTpy = getPrevTpRate(m, rawData, 'Y');
          prevTpxy = getPrevTpRate(m, rawData, 'XY');
        }
        currMonth.TPX = roundup((1 - (coi_rate_x / 1000)) * prevTpx, 9);
        currMonth.TPY = roundup((1 - (coi_rate_Y / 1000)) * prevTpy, 9);
        currMonth.TPXY = roundup((currMonth.TPX + currMonth.TPY) - (currMonth.TPX * currMonth.TPY), 9);
        var rateTPXY = 0;
        if (Number(prevTpxy) !== 0) {
          rateTPXY = currMonth.TPXY / prevTpxy;
        }
        currMonth.QXYT_1 = roundup(1000 * (1 - rateTPXY), 9);
        coi_rate = math.max(currMonth.QXYT_1, 0.18);
      }
      if (m === 1) {
        currMonth.Initial_Premium = premium;
      }
      currMonth.elm_Fee = roundup(currMonth.Initial_Premium * ef_isp_rate, 9);
      currMonth.allocated_Premium_BOM = roundup(currMonth.Initial_Premium - currMonth.elm_Fee, 9);
      currMonth.account_Value_BOM = roundup(currMonth.allocated_Premium_BOM + prevMonth.account_Value_EOM, 9);
      currMonth.sum_Risk_BOM = roundup(math.max(0, (sumSa - currMonth.account_Value_BOM)), 9);
      if (currMonth.plYear <= pfd_rate) {
        if (math.mod((m - 1), 3) === 0) {
          currMonth.policy_Fee = roundup((premium * fpisp_rate) / 4, 9);
        }
      }
      if (math.mod((m - 1), 3) === 0) {
        currMonth.Ongoing_Fee = roundup((premium * oafisp_rate) / 4, 9);
      }
      if (math.mod((m - 1), 3) === 0) {
        currMonth.Cost_Insurance = roundup((coi_rate / 1000 / 4) * currMonth.sum_Risk_BOM, 9);
      }
      currMonth.Fund_Return = roundup((currMonth.account_Value_BOM - (currMonth.policy_Fee + currMonth.Ongoing_Fee + currMonth.Cost_Insurance + currMonth.Trailer_Fee)) * (math.pow(1 + in_IRR, 1 / 12) - 1), 9);
      currMonth.account_Value_EOM = roundup(currMonth.account_Value_BOM - (currMonth.policy_Fee + currMonth.Ongoing_Fee + currMonth.Cost_Insurance + currMonth.Trailer_Fee) + currMonth.Fund_Return, 9);
      if (m === month) {
        age100av = Number(currMonth.account_Value_EOM);
      }
      rawData.push(currMonth);
    }
    return age100av;
  };
  var age100 = 100 - quotation.iAge;
  if (policytype === 'joint') {
    var minLaAge = math.min(quotation.pAge, quotation.iAge);
    age100 = 100 - minLaAge;
  }
  var month = age100 * 12;
  var age100AccValue = calcIllustration(month, in_IRR);
  return age100AccValue;
}