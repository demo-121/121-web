function(quotation, planInfo, planDetail, year) {
  var rateKey;
  switch (quotation.dealerGroup) {
    case 'SYNERGY':
      rateKey += 'AGENCY';
      break;
    case 'FUSION':
      rateKey += 'BROKER';
      break;
    default:
      rateKey += quotation.dealerGroup;
  }
  var rateTable = planDetail.rates.commRates[rateKey];
  var commRates;
  if (rateTable) {
    var maxTerm = 0;
    _.each(rateTable, (rates, key) => {
      var rateTerm = Number(key);
      if (lookupYr >= rateTerm && rateTerm > maxTerm) {
        commRates = rates;
        maxTerm = rateTerm;
      }
    });
  }
  if (commRates && commRates[year - 1]) {
    return commRates[year - 1];
  }
  return 0;
}