function(quotation, planInfo, planDetail) { /*validAfterCalcFuncLITE*/
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  var bpInfo = quotation.plans[0];
  var maxAdult = {
    "2": 12500000,
    "3": 8333000,
    "4": 6250000,
    "5": 5000000,
    "6": 4166000,
    "7": 3571000
  };
  var maxJuv = {
    "2": 500000,
    "3": 333000,
    "4": 250000,
    "5": 200000,
    "6": 166000,
    "7": 142000
  };
  var multiFactor = quotation.policyOptions.multiFactor;
  var amountbasicplanlookup = 0;
  if (multiFactor) {
    amountbasicplanlookup = (quotation.iAge >= 18 ? maxAdult : maxJuv)[multiFactor];
  }
  var amountminbpInfo = amountbasicplanlookup;
  if (planInfo.sumInsured) {
    var max_value = quotation.iAge >= 18 ? 3000000 : 500000;
    var cipSa = 0;
    var tooLargeECIX_LITE = false;
    var tooLargeCIBX_LITE = false;
    var tooLargeMBECI_LITE = false;
    var tooLargeMBCI_LITE = false;
    var sumofECIX_LITEnCIBX_LITE = 0;
    var valueofLITE_ECIBLITE_MBECI = 0;
    var valueofLITE_CIBLITE_MBCI = 0;
    for (var p in quotation.plans) {
      var plan = quotation.plans[p];
      if (plan.sumInsured) {
        if (plan.covCode === 'LITE_ECIB' || plan.covCode === 'LITE_MBECI') {
          valueofLITE_ECIBLITE_MBECI = valueofLITE_ECIBLITE_MBECI > plan.sumInsured ? valueofLITE_ECIBLITE_MBECI : plan.sumInsured;
        } else if (plan.covCode === 'LITE_CIB' || plan.covCode === 'LITE_MBCI') {
          valueofLITE_CIBLITE_MBCI = valueofLITE_CIBLITE_MBCI > plan.sumInsured ? valueofLITE_CIBLITE_MBCIa : plan.sumInsured;
        } else if (plan.covCode === 'LITE_CRBP') {
          cipSa += plan.sumInsured;
        }
        if (plan.covCode === 'LITE_ECIB' || plan.covCode === 'LITE_CIB') {
          sumofECIX_LITEnCIBX_LITE += plan.sumInsured;
        }
        if (plan.covCode === 'LITE_ECIB' && plan.sumInsured > bpInfo.sumInsured) {
          tooLargeECIX_LITE = true;
        }
        if (plan.covCode === 'LITE_CIB' && plan.sumInsured > bpInfo.sumInsured) {
          tooLargeCIBX_LITE = true;
        }
        if (plan.covCode === 'LITE_MBECI' && plan.sumInsured > bpInfo.sumInsured) {
          tooLargeMBECI_LITE = true;
        }
        if (plan.covCode === 'LITE_MBCI' && plan.sumInsured > bpInfo.sumInsured) {
          tooLargeMBCI_LITE = true;
        }
      }
    }
    cipSa += valueofLITE_ECIBLITE_MBECI + valueofLITE_CIBLITE_MBCI;
    if (sumofECIX_LITEnCIBX_LITE > bpInfo.sumInsured) {
      quotDriver.context.addError({
        covCode: 'LITE_ECIB',
        msg: 'The total sum assured for Early Critical Illness Benefit and Critical Illness Benefit riders has exceeded the maximum sum assured of S$' + getCurrency(bpInfo.sumInsured, '', 0)
      });
      quotDriver.context.addError({
        covCode: 'LITE_CIB',
        msg: 'The total sum assured for Early Critical Illness Benefit and Critical Illness Benefit riders has exceeded the maximum sum assured of S$' + getCurrency(bpInfo.sumInsured, '', 0)
      });
    }
    if (cipSa > max_value && quotation.iAge >= 18) {
      quotDriver.context.addError({
        covCode: 'LITE_ECIB',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 3 million.'
      });
      quotDriver.context.addError({
        covCode: 'LITE_CIB',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 3 million.'
      });
      quotDriver.context.addError({
        covCode: 'LITE_CRBP',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 3 million.'
      });
      quotDriver.context.addError({
        covCode: 'LITE_MBECI',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 3 million.'
      });
      quotDriver.context.addError({
        covCode: 'LITE_MBCI',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 3 million.'
      });
    } else if (cipSa > max_value && quotation.iAge < 18) {
      quotDriver.context.addError({
        covCode: 'LITE_ECIB',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 500,000.'
      });
      quotDriver.context.addError({
        covCode: 'LITE_CIB',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 500,000.'
      });
      quotDriver.context.addError({
        covCode: 'LITE_CRBP',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 500,000.'
      });
      quotDriver.context.addError({
        covCode: 'LITE_MBECI',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 500,000.'
      });
      quotDriver.context.addError({
        covCode: 'LITE_MBCI',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 500,000.'
      });
    }
  }
  var getDiscountRate = function() {
    var campaign = planDetail.campaign[0];
    if (!campaign) {
      return 0;
    }
    return campaign.discount;
  };
  var addCampaign2Quotation = function() {
    var covCode = planDetail.covCode;
    var plans = quotation.plans;
    plans.forEach(function(plan) {
      if (plan.covCode === covCode) {
        var campaign = planDetail.campaign[0];
        if (!!campaign && campaign.campaignId && campaign.campaignCode) {
          var campaignCode = campaign.campaignCode;
          var campaignId = campaign.campaignId;
          var campaignCodes = [];
          var campaignIds = [];
          campaignCodes.push(campaignCode);
          campaignIds.push(campaignId);
          plan.campaignCodes = campaignCodes;
          plan.campaignIds = campaignIds;
        }
      }
    });
  };
  var discount = getDiscountRate();
  if (typeof(discount) === 'number') {
    if (discount > 0) {
      addCampaign2Quotation();
    }
  }
}