function(quotation, planInfo, planDetails) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  if (quotation.policyOptions.multiFactor && planInfo.premTerm) { /* planCode needed for prem calculation */
    var planCode = 'LITE';
    var premTermYr;
    var policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      planCode = Number.parseInt(planInfo.premTerm) + planCode;
      premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      planCode = planCode + Number.parseInt(planInfo.premTerm);
      premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
    if (planInfo.policyTerm.indexOf('TA') > -1) {
      policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    } else {
      policyTermYr = Number.parseInt(planInfo.policyTerm);
    }
    planInfo.premTermYr = premTermYr;
    planInfo.policyTermYr = policyTermYr;
    var mbCodeMap = {
      "5": "A",
      "3.5": "B",
      "2.5": "C"
    };
    var dateToUse;
    if (quotation.riskCommenDate === undefined) {
      dateToUse = new Date();
    } else {
      dateToUse = new Date(quotation.riskCommenDate);
    }
    if (dateUtils.getAttainedAge(dateToUse, new Date(quotation.iDob)).year < 18) {
      planInfo.planTemplateDisappear = 'LMP_BUNDLEDPLAN';
    } else {
      planInfo.planTemplateDisappear = undefined;
    }
    planInfo.planCode = planCode;
    quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
    let cQuot = Object.assign({}, quotation);
    let cPlanInfo = Object.assign({}, planInfo);
    cQuot.paymentMode = 'A';
    var countryCode = quotation.iResidence;
    var isCountryMatch = (countryCode === "R51" || countryCode === "R52" || countryCode === "R53" || countryCode === "R54"); /**let annPrem = runFunc(planDetails['LITE_TPDD'].formulas.premFunc, cQuot, cPlanInfo, planDetails['LITE_TPDD']);*/
    if (planInfo.sumInsured) {
      var modalFactor = 1;
      let annPrem = 0;
      if (global && global.quotCache && global.quotCache.planDetails && global.quotCache.planDetails['LITE_TPDD']) {
        var tpddPlanDetail = global.quotCache.planDetails['LITE_TPDD'];
        annPrem = runFunc(tpddPlanDetail.formulas.premFunc, cQuot, planInfo, tpddPlanDetail);
      }
      for (var p in planDetails['LITE_TPDD'].payModes) {
        if (planDetails['LITE_TPDD'].payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetails['LITE_TPDD'].payModes[p].factor;
          var updated_premium = roundDown(math.multiply(math.bignumber(planInfo.yearPrem), modalFactor), 2);
          var N_annPrem = roundDown(math.multiply(math.bignumber(annPrem), modalFactor), 2);
          planInfo.premium = math.number(math.add(updated_premium, N_annPrem));
        }
        var NmodalFactor = planDetails['LITE_TPDD'].payModes[p].factor;
        var Nupdated_premium = roundDown(math.multiply(math.bignumber(planInfo.yearPrem), NmodalFactor), 2);
        var NannPrem = roundDown(math.multiply(math.bignumber(annPrem), NmodalFactor), 2);
        if (planDetails['LITE_TPDD'].payModes[p].mode === "S") {
          planInfo.alonetotHalfyearPrem = math.number(Nupdated_premium);
          planInfo.totHalfyearPrem = math.number(math.add(Nupdated_premium, NannPrem));
          planInfo.halfYearPrem = planInfo.totHalfyearPrem;
        }
        if (planDetails['LITE_TPDD'].payModes[p].mode === "Q") {
          planInfo.alonetotQuarterPrem = math.number(Nupdated_premium);
          planInfo.totQuarterPrem = math.number(math.add(Nupdated_premium, NannPrem));
          planInfo.quarterPrem = planInfo.totQuarterPrem;
        }
        if (planDetails['LITE_TPDD'].payModes[p].mode === "M") {
          planInfo.alonetotMonthPrem = math.number(Nupdated_premium);
          planInfo.totMonthPrem = math.number(math.add(Nupdated_premium, NannPrem));
          planInfo.monthPrem = planInfo.totMonthPrem;
        }
      }
      planInfo.alonetotYearPrem = planInfo.yearPrem;
      planInfo.totYearPrem = math.number(math.add(math.bignumber(planInfo.yearPrem), annPrem));
      planInfo.yearPrem = planInfo.totYearPrem;
      planInfo.multiplyBenefit = planInfo.sumInsured * quotation.policyOptions.multiFactor;
      planInfo.multiplyBenefitAfter70 = planInfo.multiplyBenefit * 0.5;
      var policyYearRefer = 0;
      if (planInfo.premTerm.indexOf('YR') > -1) {
        policyYearRefer = Number.parseInt(planInfo.premTerm);
      } else {
        policyYearRefer = Number.parseInt(planInfo.premTerm) - quotation.iAge;
      }
      if (planInfo.premium) {
        var crate;
        var channel = quotation.agent.dealerGroup.toUpperCase();
        var commission_rate = planDetails['LITE'].rates.commissionRate[channel];
        planInfo.cummComm = [0];
        var cummComm = 0;
        for (var rate in commission_rate) {
          if (policyYearRefer < 15) {
            crate = commission_rate[rate][0];
          } else if (policyYearRefer < 20) {
            crate = commission_rate[rate][1];
          } else if (policyYearRefer < 25) {
            crate = commission_rate[rate][2];
          } else {
            crate = commission_rate[rate][3];
          }
          cummComm += crate * planInfo.premium;
          planInfo.cummComm.push(crate);
        }
        var SA_bundleInfo = {
          "2": 50000,
          "3": 33333.34,
          "4": 25000,
          "5": 20000,
          "6": 16666.67,
          "7": 14285.72
        }; /*var SA_policy_lookup = SA_bundleInfo[quotation.policyOptions.multiFactor];*/
        var SA_policy_lookup = 100000;
        var bundle_lsd = 0;
        if (SA_policy_lookup < 50000) {
          bundle_lsd = 0;
        } else if (SA_policy_lookup < 100000) {
          bundle_lsd = -0.90;
        } else if (SA_policy_lookup < 200000) {
          bundle_lsd = -1.30;
        } else {
          bundle_lsd = -1.50;
        }
        var premRate = math.bignumber(runFunc(planDetails['LITE'].formulas.getPremRate, quotation, planInfo, planDetails['LITE'], quotation.iAge));
        var process_premRate = math.add(premRate, bundle_lsd);
        var annualPrem_bundle = roundDown(math.multiply(process_premRate, SA_policy_lookup, 0.001), 2);
        var modalFactor = 1;
        for (var p in planDetails['LITE'].payModes) {
          if (planDetails['LITE'].payModes[p].mode === quotation.paymentMode) {
            modalFactor = planDetails['LITE'].payModes[p].factor;
            break;
          }
        }
        var modlePrem_bundle = roundDown(math.multiply(annualPrem_bundle, modalFactor), 2);
        planInfo.annualPrem_bundle = math.number(annualPrem_bundle);
        planInfo.modlePrem_bundle = math.number(modlePrem_bundle);
        planInfo.bundleSA = SA_policy_lookup;
      }
    } else {
      planInfo.multiplyBenefit = null;
      planInfo.multiplyBenefitAfter70 = null;
      planInfo.premium = null;
    }
  }
}