function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var maxAdult = {
    "2": 12500000,
    "3": 8333000,
    "4": 6250000,
    "5": 5000000,
    "6": 4166000,
    "7": 3571000
  };
  var maxJuv = {
    "2": 500000,
    "3": 333000,
    "4": 250000,
    "5": 200000,
    "6": 166000,
    "7": 142000
  };
  var multiFactor = quotation.policyOptions.multiFactor;
  if (multiFactor) {
    var maxV = 0;
    if (multiFactor > 1) {
      maxV = (quotation.iAge >= 18 ? maxAdult : maxJuv)[multiFactor];
    } else {
      maxV = quotation.iAge >= 18 ? 25000000 : 1000000;
    }
    planDetail.inputConfig.benlim = {
      min: 25000,
      max: maxV
    };
  }
  planDetail.othSaInd = 'N';
  planDetail.inputConfig.canViewOthSa = false;
}