function(quotation, planInfo, planDetails, extraPara) {
  var basicPlan = quotation.plans[0];
  var hasSpTopUp = quotation.policyOptions.topUpSelect;
  var hasRspTopUp = quotation.policyOptions.rspSelect;
  var hasWithdrawal = quotation.policyOptions.wdSelect;
  var company = extraPara.company;
  var illust = extraPara.illustrations[planInfo.covCode];
  var getSmokingDesc = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  var getGenderDesc = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getCurrencyDesc = function(value, decimals) {
    return quotation.ccy + ' ' + getCurrency(value, '', decimals);
  };
  return {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      planCode: basicPlan.planCode,
      releaseVersion: "1"
    },
    insured: {
      name: quotation.pFullName,
      gender: getGenderDesc(quotation.pGender),
      dob: new Date(quotation.pDob).format(extraPara.dateFormat),
      age: quotation.pAge,
      smoking: getSmokingDesc(quotation.pSmoke)
    },
    cover: {
      sameAs: quotation.sameAs,
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      basicTDC: getCurrencyDesc(illust[0].basic_TDC, 0)
    }
  };
}