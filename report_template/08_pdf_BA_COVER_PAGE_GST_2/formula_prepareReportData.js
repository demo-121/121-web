function(quotation, planInfo, planDetails, extraPara) {
  var trunc = function(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, Number(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  let company = extraPara.company;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb
  } = company;
  var retVal = {};
  if (company) {
    var retVal = {
      'compName': compName,
      'compRegNo': compRegNo,
      'compAddr': compAddr,
      'compAddr2': compAddr2,
      'compTel': compTel,
      'compFax': compFax,
      'compWeb': compWeb,
    };
  }
  retVal.proposerGenderTitle = (quotation.pGender) == 'M' ? 'Male' : 'Female';
  retVal.proposerSmokerTitle = (quotation.pSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.insurerGenderTitle = (quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.insurerSmokerTitle = (quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  var ccy = quotation.ccy;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = 'US$';
  } else if (ccy == 'ASD') {
    polCcy = 'Australian Dollars';
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    polCcy = 'British Pound';
    ccySyb = '£';
  }
  retVal.polCcy = polCcy;
  retVal.ccySyb = ccySyb;
  var paymentModeTitle = '';
  if (quotation.paymentMode == 'A') {
    paymentModeTitle = 'Annual';
  } else if (quotation.paymentMode == 'S') {
    paymentModeTitle = 'Semi-Annual';
  } else if (quotation.paymentMode == 'Q') {
    paymentModeTitle = 'Quarterly';
  } else if (quotation.paymentMode == 'M') {
    paymentModeTitle = 'Monthly';
  }
  retVal.paymentModeTitle = paymentModeTitle;
  var plans = quotation.plans;
  var planCodes = "";
  var planFields = [];
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    var policyTerm = plan.policyTerm;
    var covCode = plan.covCode;
    var sumAssured = getCurrency(plan.sumInsured, ' ', 0);
    var planDetail = planDetails[covCode];
    var policyTermList = planDetail.inputConfig.policyTermList;
    var planInd = planDetail.planInd;
    var planName = !plan.covName ? "" : (typeof plan.covName == 'string' ? plan.covName : (plan.covName.en || plan.covName[Object.keys(plan.covName)[0]]));
    var polTermTitle = -1;
    planCodes += (i > 0 ? ", " : "") + plan.planCode;
    for (var j = 0; j < policyTermList.length; j++) {
      var polTermOpt = policyTermList[j];
      if (polTermOpt.value == policyTerm) polTermTitle = polTermOpt.title;
    }
    let postfix = "";
    if (planDetail.saPostfix && typeof planDetail.saPostfix !== 'undefined') {
      if (typeof planDetail.saPostfix == 'string') {
        postfix = planDetail.saPostfix;
      } else if (planDetail.saPostfix.en) {
        postfix = planDetail.saPostfix.en;
      } else if (Object.keys(planDetail.saPostfix) > 0) {
        postfix = planDetail.saPostfix[Object.keys(planDetail.saPostfix)[0]];
      }
    }
    if (polTermTitle != -1) {
      var info = {
        covCode: covCode,
        planInd: planInd,
        policyTerm: polTermTitle,
        permTerm: polTermTitle,
        sumAssured: sumAssured,
        planName: planName + (postfix ? " (" + postfix + ")" : "")
      };
      info.premium = getCurrency(plan.premium, ' ', 2);
      info.APremium = getCurrency(plan.yearPrem, ' ', 2);
      info.HPremium = getCurrency(plan.halfYearPrem, ' ', 2);
      info.QPremium = getCurrency(plan.quarterPrem, ' ', 2);
      info.MPremium = getCurrency(plan.monthPrem, ' ', 2);
      info.LPremium = getCurrency(plan.yearPrem, ' ', 2);
      planFields.push(info);
    }
  }
  retVal.planFields = planFields;
  retVal.planCodes = planCodes;
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  retVal.iDob = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.proposerDob = new Date(quotation.pDob).format(extraPara.dateFormat);
  retVal.insurerDob = new Date(quotation.iDob).format(extraPara.dateFormat);
  retVal.insurerOCls = planInfo.covClass;
  retVal.tax = {
    yearTax: getCurrency(quotation.tax.yearTax, ' ', 2),
    halfYearTax: getCurrency(quotation.tax.halfYearTax, ' ', 2),
    quarterTax: getCurrency(quotation.tax.quarterTax, ' ', 2),
    monthTax: getCurrency(quotation.tax.monthTax, ' ', 2)
  };
  return retVal;
}