function(planDetail, quotation) {
  var policyTermList = [];
  if (quotation.policyOptions.retirementAge === undefined) {
    return null;
  }
  if (quotation.policyOptions.payoutTerm === undefined) {
    return null;
  }
  if (quotation.plans[0].premTerm === undefined) {
    return null;
  }
  var premiumTerm = quotation.plans[0].premTerm;
  var testAge = 50 - premiumTerm;
  var policyTerm = 0;
  var polTermDesc = '';
  if (quotation.iAge <= testAge) {
    policyTerm = premiumTerm;
    polTermDesc = policyTerm + ' Years';
  } else if ((premiumTerm < 25) && (quotation.iAge <= 45) && (quotation.iAge > testAge)) {
    policyTerm = 50;
    polTermDesc = 'To Age 50';
  } else if ((premiumTerm == 25) && (quotation.iAge > 25) && (quotation.iAge <= 40)) {
    policyTerm = 50;
    polTermDesc = 'To Age 50';
  }
  policyTermList.push({
    value: policyTerm,
    title: polTermDesc,
    default: true
  });
  return policyTermList;
}