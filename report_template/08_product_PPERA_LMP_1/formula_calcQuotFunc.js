function(quotation, planInfo, planDetails) {
  if (quotation.plans[0].premTerm) {
    var ppt_yr = 0;
    if (quotation.plans[0].premTerm.indexOf('YR') == -1) {
      ppt_yr = 65 - quotation.pAge;
    } else {
      ppt_yr = Number.parseInt(quotation.plans[0].premTerm)
    }
    var setSame = quotation.pAge + ppt_yr < 65;
    if (planDetails['PPERA_LMP'].inputConfig.policyTermList) {
      var array = planDetails['PPERA_LMP'].inputConfig.policyTermList;
      for (var i = 0; i < array.length; i++) {
        if (Number.parseInt(array[i].value) == Number.parseInt(ppt_yr) && setSame) {
          planInfo.policyTerm = array[i].value;
          planInfo.polTermDesc = array[i].title;
        } else if (!setSame && Number.parseInt(array[i].value) == 65) {
          planInfo.policyTerm = array[i].value;
          planInfo.polTermDesc = array[i].title;
        }
      }
    }
  }
  if (planDetails['PPERA_LMP'].inputConfig) {
    if (planDetails['PPERA_LMP'].inputConfig.premTermList) {
      var array = planDetails['PPERA_LMP'].inputConfig.premTermList;
      for (var i = 0; i < array.length; i++) {
        if (Number.parseInt(array[i].value) == Number.parseInt(planInfo.policyTerm)) {
          planInfo.premTerm = array[i].value;
          planInfo.premTermDesc = array[i].title;
        }
      }
    }
  }
  if (quotation.policyOptions.multiFactor && planInfo.premTerm) {
    if (planInfo.policyTerm && planInfo.premTerm) {
      if (quotation.sameAs === "N") {
        var plans = quotation.plans;
        var sumofprod = 0;
        for (var p in plans) {
          if (plans[p].covCode !== "WPN_LMP" && plans[p].covCode !== "PPERA_LMP" && plans[p].covCode !== "PPEPS_LMP" && plans[p].premium) {
            sumofprod += plans[p].yearPrem;
          }
        }
        planInfo.sumInsured = Number.parseInt(sumofprod * 1000) / 1000;
      } else {
        planInfo.sumInsured = 0;
      }
    } /* planCode needed for prem calculation */
    var planCode = 'PE';
    var premTermYr;
    var policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
    if (planInfo.policyTerm.indexOf('TA') > -1) {
      policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    } else {
      policyTermYr = Number.parseInt(planInfo.policyTerm);
    }
    if (policyTermYr > 65 - quotation.pAge) {
      policyTermYr = 65 - quotation.pAge;
    }
    planInfo.premTermYr = policyTermYr;
    planInfo.policyTermYr = policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      planCode = Number.parseInt(planInfo.premTerm) + planCode;
    } else {
      planCode = planCode + Number.parseInt(planInfo.premTerm);
    }
    planInfo.planCode = planCode;
    var bpInfo = quotation.plans[0];
    var amount = bpInfo.sumInsured > 0 ? bpInfo.sumInsured : 0;
    quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
    if (planInfo.sumInsured) {
      planInfo.multiplyBenefit = planInfo.sumInsured * quotation.policyOptions.multiFactor;
      planInfo.multiplyBenefitAfter70 = planInfo.multiplyBenefit * 0.5;
      if (planInfo.premium) {
        var crate;
        var channel = quotation.agent.dealerGroup.toUpperCase();
        var commission_rate = planDetails['PPERA_LMP'].rates.commissionRate[channel];
        planInfo.cummComm = [0];
        var cummComm = 0;
        for (var rate in commission_rate) {
          if (policyTermYr < 6) {
            crate = commission_rate[rate][0];
          } else if (policyTermYr < 10) {
            crate = commission_rate[rate][1];
          } else if (policyTermYr < 15) {
            crate = commission_rate[rate][2];
          } else if (policyTermYr < 20) {
            crate = commission_rate[rate][3];
          } else if (policyTermYr < 25) {
            crate = commission_rate[rate][4];
          } else {
            crate = commission_rate[rate][5];
          }
          cummComm += crate * planInfo.premium;
          planInfo.cummComm.push(crate);
        }
      }
    } else {
      planInfo.multiplyBenefit = null;
      planInfo.multiplyBenefitAfter70 = null;
      planInfo.premium = null;
    }
  }
}