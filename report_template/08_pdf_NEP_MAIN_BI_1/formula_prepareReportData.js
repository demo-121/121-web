function(quotation, planInfo, planDetails, extraPara) {
  var illust = extraPara.illustrations[planInfo.covCode];
  var deathBenefit = [];
  var surrenderValue = [];
  var lastRowsurrenderValue = [];
  var deductions = [];
  var distributionCost = [];
  var retireIncSched = [];
  var supdeathBenefit = [];
  var supsurrenderValue = [];
  var supdistributionCost = [];
  var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
  var tempaccterm = quotation.policyOptions.accumulationPeriod;
  var arryaccterm = tempaccterm.split(' ');
  var accPeriod = parseInt(arryaccterm[0]);
  var positPeriod = parseInt(quotation.policyOptions.depositInput);
  var policyTerm = parseInt(planInfo.policyTerm);
  var premTerm = parseInt(planInfo.premTerm);
  var sizePlans = quotation.plans.length;
  var riderPolicyTerm = 0;
  if (sizePlans > 1) {
    riderPolicyTerm = parseInt(quotation.plans[1].policyTerm);
    if (riderPolicyTerm === 50) {
      riderPolicyTerm = 50 - quotation.iAge;
    }
  }
  var i;
  var yearCount = 0;
  var yearCount2 = 0;
  var incRowCnt = 0;
  var incRowRiderCnt = 0;
  var incRowAnnualCnt = 0;
  var tdcbasicCnt = 0;
  for (i = 0; i < illust.length; i++) {
    var row = illust[i];
    var policyYear = row.policyYear;
    var age = row.age;
    var policyYearAge = policyYear + ' / ' + age;
    var premium = getCurrency(Math.round(row.totalPremiumPaidToDate), '', 0);
    var incRow = false;
    var incRowAnnual = false;
    var incRowRider = false;
    var tdcbasic = false;
    if (i < 10) {
      if (policyYear <= policyTerm) {
        incRow = true;
        tdcbasic = true;
        incRowCnt++;
        tdcbasicCnt++;
      }
      if (policyYear <= riderPolicyTerm) {
        incRowRider = true;
        incRowRiderCnt++;
      }
    } else {
      yearCount++;
      if (i < (premTerm + accPeriod)) {
        if (yearCount == 5) {
          yearCount = 0;
          if (policyYear <= policyTerm) {
            incRow = true;
            incRowCnt++;
          }
        }
      } else {
        if (policyYear <= policyTerm) {
          incRow = true;
          incRowCnt++;
        }
      }
    }
    if (i >= 10) {
      yearCount2++;
      if (yearCount2 == 5) {
        yearCount2 = 0;
        if (policyYear <= riderPolicyTerm) {
          incRowRider = true;
          incRowRiderCnt++;
        }
        if (policyYear <= policyTerm) {
          tdcbasic = true;
          tdcbasicCnt++;
        }
      } else {
        if ((i + 5) > policyTerm && policyYear === policyTerm) {
          tdcbasic = true;
          tdcbasicCnt++;
        }
        if ((i + 5) > riderPolicyTerm && policyYear === riderPolicyTerm) {
          incRowRider = true;
          incRowRiderCnt++;
        }
      }
    }
    var minAnnualRow = premTerm + accPeriod + positPeriod;
    if (policyTerm != minAnnualRow) {
      minAnnualRow = minAnnualRow + 1;
    }
    if (policyYear >= minAnnualRow) {
      incRowAnnual = true;
      incRowAnnualCnt++;
    } /** if(riderPolicyTerm ===50){ if(age <=50){ incRowRider = true; } }else{ if(policyYear <= riderPolicyTerm){ incRowRider = true; } } */
    var incRowCntLineBreak = false;
    if (incRowCnt !== 0 && incRowCnt % 5 === 0 && incRowCnt !== illust.length) {
      incRowCntLineBreak = true;
    }
    var incRowRiderCntLineBreak = false;
    if (incRowRiderCnt !== 0 && incRowRiderCnt % 5 === 0 && incRowRiderCnt !== illust.length) {
      incRowRiderCntLineBreak = true;
    }
    var incRowAnnualCntLineBreak = false;
    if (incRowAnnualCnt !== 0 && incRowAnnualCnt % 5 === 0 && incRowAnnualCnt !== illust.length) {
      incRowAnnualCntLineBreak = true;
    }
    var tdcbasicCntLineBreak = false;
    if (tdcbasicCnt !== 0 && tdcbasicCnt % 5 === 0 && tdcbasicCnt !== illust.length) {
      tdcbasicCntLineBreak = true;
    }
    if (incRow) {
      deathBenefit.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        guaranteed: getCurrency(Math.round(row.guaranteedDeathBenefit), '', 0),
        nonGuaranteedLow: getCurrency(Math.round(row.nonguaranteedDeathBenefit['3.25']), '', 0),
        totalLow: getCurrency(Math.round(row.totalDeathBenefit['3.25']), '', 0),
        nonGuaranteedHigh: getCurrency(Math.round(row.nonguaranteedDeathBenefit['4.75']), '', 0),
        totalHigh: getCurrency(Math.round(row.totalDeathBenefit['4.75']), '', 0),
      });
      if (incRowCntLineBreak) {
        deathBenefit.push({
          policyYearAge: "",
          premiumsPaidToDate: "",
          guaranteed: "",
          nonGuaranteedLow: "",
          totalLow: "",
          nonGuaranteedHigh: "",
          totalHigh: "",
        });
      }
      surrenderValue.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        retireIncPayout: getCurrency(Math.round(row.guranteedAnnualRetireIncPayout), '', 0),
        guaranteed: getCurrency(Math.round(row.guranteedSvBenefit), '', 0),
        nonGuaranteedLow: getCurrency(Math.round(row.nonguranteedSvBenefit['3.25']), '', 0),
        totalLow: getCurrency(Math.round(row.totalSurrenderValue['3.25']), '', 0),
        nonGuaranteedHigh: getCurrency(Math.round(row.nonguranteedSvBenefit['4.75']), '', 0),
        totalHigh: getCurrency(Math.round(row.totalSurrenderValue['4.75']), '', 0)
      });
      if (incRowCntLineBreak) {
        if (i + 1 !== illust.length) {
          surrenderValue.push({
            policyYearAge: "",
            premiumsPaidToDate: "",
            retireIncPayout: "",
            guaranteed: "",
            nonGuaranteedLow: "",
            totalLow: "",
            nonGuaranteedHigh: "",
            totalHigh: ""
          });
        }
        if (incRowCnt % 25 === 0 && i + 1 != illust.length - 1) {
          var lineBreakCnt = 4;
          if (incRowCnt > 25) {
            lineBreakCnt = 7;
          }
          for (var k = 0; k < lineBreakCnt; k++) {
            surrenderValue.push({
              policyYearAge: "",
              premiumsPaidToDate: "",
              retireIncPayout: "",
              guaranteed: "",
              nonGuaranteedLow: "",
              totalLow: "",
              nonGuaranteedHigh: "",
              totalHigh: ""
            });
          }
        }
      } else { /* if(i+1 === illust.length-1){ surrenderValue.push({ policyYearAge: "", premiumsPaidToDate: "", retireIncPayout: "", guaranteed: "", nonGuaranteedLow: "", totalLow: "", nonGuaranteedHigh: "", totalHigh: "" }); }*/ }
      deductions.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        valueOfPremiumsLow: getCurrency(Math.round(row.valueOfPremiums['3.25']), '', 0),
        effectOfDeductionsLow: getCurrency(Math.round(row.effectOfDeduction['3.25']), '', 0),
        totalSurrenderValueLow: getCurrency(Math.round(row.todSurrenderValue['3.25']), '', 0),
        valueOfPremiumsHigh: getCurrency(Math.round(row.valueOfPremiums['4.75']), '', 0),
        effectOfDeductionsHigh: getCurrency(Math.round(row.effectOfDeduction['4.75']), '', 0),
        totalSurrenderValueHigh: getCurrency(Math.round(row.todSurrenderValue['4.75']), '', 0)
      });
      if (incRowCntLineBreak) {
        deductions.push({
          policyYearAge: "",
          premiumsPaidToDate: "",
          valueOfPremiumsLow: "",
          effectOfDeductionsLow: "",
          totalSurrenderValueLow: "",
          valueOfPremiumsHigh: "",
          effectOfDeductionsHigh: "",
          totalSurrenderValueHigh: ""
        });
      } /** SUPPLEMENTARY POLICY ILLUSTRATION */
      supdeathBenefit.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        guaranteed: getCurrency(Math.round(row.suppGteedDeathBenefit), '', 0),
        nonGuaranteedLow: getCurrency(Math.round(row.suppNonGteedDeathBenefit['3.25']), '', 0),
        totalLow: getCurrency(Math.round(row.suppTotalDeathBenefit['3.25']), '', 0),
        nonGuaranteedHigh: getCurrency(Math.round(row.suppNonGteedDeathBenefit['4.75']), '', 0),
        totalHigh: getCurrency(Math.round(row.suppTotalDeathBenefit['4.75']), '', 0),
      });
      if (incRowCntLineBreak) {
        supdeathBenefit.push({
          policyYearAge: "",
          premiumsPaidToDate: "",
          guaranteed: "",
          nonGuaranteedLow: "",
          totalLow: "",
          nonGuaranteedHigh: "",
          totalHigh: ""
        });
      }
      supsurrenderValue.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        retireIncPayout: getCurrency(Math.round(row.guranteedAnnualRetireIncPayout), '', 0),
        guaranteed: getCurrency(Math.round(row.suppGteedSurrenderValue), '', 0),
        nonGuaranteedLow: getCurrency(Math.round(row.suppNonGteedSurrenderValue['3.25']), '', 0),
        totalLow: getCurrency(Math.round(row.suppTotalSurrenderValue['3.25']), '', 0),
        nonGuaranteedHigh: getCurrency(Math.round(row.suppNonGteedSurrenderValue['4.75']), '', 0),
        totalHigh: getCurrency(Math.round(row.suppTotalSurrenderValue['4.75']), '', 0)
      });
      if (incRowCntLineBreak) {
        if (i + 1 !== illust.length) {
          supsurrenderValue.push({
            policyYearAge: "",
            premiumsPaidToDate: "",
            retireIncPayout: "",
            guaranteed: "",
            nonGuaranteedLow: "",
            totalLow: "",
            nonGuaranteedHigh: "",
            totalHigh: ""
          });
        }
        if (incRowCnt % 25 === 0 && i + 1 != illust.length - 1) {
          var lineBreakCnt = 4;
          if (incRowCnt > 25) {
            lineBreakCnt = 6;
          }
          for (var k = 0; k < lineBreakCnt; k++) {
            supsurrenderValue.push({
              policyYearAge: "",
              premiumsPaidToDate: "",
              retireIncPayout: "",
              guaranteed: "",
              nonGuaranteedLow: "",
              totalLow: "",
              nonGuaranteedHigh: "",
              totalHigh: ""
            });
          }
        }
      } else { /* if(i+1 === illust.length-1){ surrenderValue.push({ policyYearAge: "", premiumsPaidToDate: "", retireIncPayout: "", guaranteed: "", nonGuaranteedLow: "", totalLow: "", nonGuaranteedHigh: "", totalHigh: "" }); }*/ }
    }
    if (incRowAnnual) {
      retireIncSched.push({
        policyYearAge: policyYearAge,
        guaranteed: getCurrency(Math.round(row.guranteedAnnualPayout), '', 0),
        nonGuaranteedLow: getCurrency(Math.round(row.nonGuranteedAnnualPayout['3.25']), '', 0),
        totalLow: getCurrency(Math.round(row.totalAnnualPayout['3.25']), '', 0),
        nonGuaranteedHigh: getCurrency(Math.round(row.nonGuranteedAnnualPayout['4.75']), '', 0),
        totalHigh: getCurrency(Math.round(row.totalAnnualPayout['4.75']), '', 0)
      });
      if (incRowAnnualCntLineBreak) {
        retireIncSched.push({
          policyYearAge: "",
          guaranteed: "",
          nonGuaranteedLow: "",
          totalLow: "",
          nonGuaranteedHigh: "",
          totalHigh: ""
        });
      }
    }
    if (tdcbasic) {
      distributionCost.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        distributionCost: getCurrency(Math.round(row.totalDistributionCost), '', 0)
      });
      if (tdcbasicCntLineBreak) {
        distributionCost.push({
          policyYearAge: "",
          premiumsPaidToDate: "",
          distributionCost: ""
        });
      }
    }
    if (incRowRider) {
      supdistributionCost.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: getCurrency(Math.round(row.suppRiderPremiumPaid), '', 0),
        distributionCost: getCurrency(Math.round(row.suppTotalDistributionCost), '', 0)
      });
      if (incRowRiderCntLineBreak) {
        supdistributionCost.push({
          policyYearAge: "",
          premiumsPaidToDate: "",
          distributionCost: ""
        });
      }
    }
    incRowCntLineBreak = false;
    incRowRiderCntLineBreak = false;
    incRowAnnualCntLineBreak = false;
    tdcbasicCntLineBreak = false;
  }
  lastRowsurrenderValue.push({
    policyYearAge: surrenderValue[surrenderValue.length - 1].policyYearAge,
    premiumsPaidToDate: surrenderValue[surrenderValue.length - 1].premiumsPaidToDate,
    retireIncPayout: surrenderValue[surrenderValue.length - 1].retireIncPayout,
    guaranteed: surrenderValue[surrenderValue.length - 1].guaranteed,
    nonGuaranteedLow: surrenderValue[surrenderValue.length - 1].nonGuaranteedLow,
    totalLow: surrenderValue[surrenderValue.length - 1].totalLow,
    nonGuaranteedHigh: surrenderValue[surrenderValue.length - 1].nonGuaranteedHigh,
    totalHigh: surrenderValue[surrenderValue.length - 1].totalHigh
  });
  var result = {
    illustration: {
      deathBenefit: deathBenefit,
      surrenderValue: surrenderValue,
      lastRowsurrenderValue: lastRowsurrenderValue,
      deductions: deductions,
      distributionCost: distributionCost,
      retireIncSched: retireIncSched
    },
    illustrationSup: {
      deathBenefit: supdeathBenefit,
      surrenderValue: supsurrenderValue,
      distributionCost: supdistributionCost
    }
  };
  return result;
}