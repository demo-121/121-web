function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var bpInfo = quotation.plans[0];
  var amount = bpInfo.sumInsured > 0 ? bpInfo.sumInsured : 0;
  amount = parseInt((amount * 0.1) / 1000);
  amount = amount * 1000;
  var multiFactor = quotation.policyOptions.multiFactor;
  if (multiFactor) {
    var maxValue = amount > 100000 ? 100000 : amount;
    if (maxValue >= 5000) {
      planDetail.inputConfig.benlim = {
        min: 5000,
        max: maxValue
      };
    }
  }
}