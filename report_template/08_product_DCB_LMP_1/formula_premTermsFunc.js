function(planDetail, quotation) {
  var premTermsList = [];
  var hasTa60 = false;
  _.each([10, 15, 20, 25], (val) => {
    if (quotation.iAge + val <= 80) {
      premTermsList.push({
        value: val + '_YR',
        title: val + ' Years'
      });
      if (quotation.iAge + val === 60) {
        hasTa60 = true;
      }
    }
  });
  if (quotation.iAge < 60 && !hasTa60) {
    premTermsList.push({
      value: '60_TA',
      title: 'To Age 60'
    });
  }
  return premTermsList;
}