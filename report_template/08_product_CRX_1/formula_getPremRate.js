function(quotation, planInfo, planDetail, age) {
  var rateKey = planInfo.sumInsured < 750000 ? 'CIB' : 'CRL';
  if (quotation.policyOptions.planType === 'renew') {
    rateKey += 'R_' + (planInfo.premTerm === 'SP' ? 'SP' : 'RP');
  } else {
    rateKey += 'NR_' + (planInfo.premTerm.endsWith('_YR') ? Number.parseInt(planInfo.premTerm) + 'P' : 'RP');
  }
  rateKey += '_' + quotation.iGender;
  rateKey += quotation.iSmoke === 'Y' ? 'S' : 'NS';
  var rates = planDetail.rates.premRate[rateKey];
  var premRate = math.bignumber(rates[Number.parseInt(planInfo.policyTerm)][age] || 0);
  var substdClsMapping = planDetail.rates.substdCls[quotation.iResidence];
  if (substdClsMapping) {
    var substdCls = substdClsMapping[quotation.iResidenceCity] || substdClsMapping.ALL;
    if (substdCls) {
      var substdRates = planDetail.rates.residentialLoading[rateKey + '_' + substdCls];
      if (substdRates) {
        premRate = math.add(premRate, substdRates[Number.parseInt(planInfo.policyTerm)][age] || 0);
      }
    }
  }
  return math.number(premRate);
}