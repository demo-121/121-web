function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  if (planInfo.sumInsured) {
    var aggrMax = {
      SGD: 3000000,
      USD: 2097000,
      EUR: 1948000,
      GBP: 1442000,
      AUD: 2857000
    };
    var aggrMaxChild = {
      SGD: 500000,
      USD: 349000,
      EUR: 324000,
      GBP: 240000,
      AUD: 476000
    };
    var crxSa = planInfo.sumInsured;
    var cipSa = 0;
    for (var p in quotation.plans) {
      var plan = quotation.plans[p];
      if (plan.covCode === 'CIP' && plan.sumInsured) {
        cipSa = plan.sumInsured;
      }
    }
    var max = (quotation.iAge >= 16 ? aggrMax : aggrMaxChild)[quotation.ccy];
    if (crxSa + cipSa > max) {
      quotDriver.context.addError({
        covCode: planInfo.covCode,
        msg: 'The total sum assured for Critical Illness Plus and Advance Critical Illness Payout riders has exceeded the maximum aggregated limit of ' + getCurrencyByCcy(max, 0, quotation.compCode, quotation.ccy)
      });
    }
  }
  if (planInfo.premium) {
    var mapping = planDetail.planCodeMapping;
    for (var i = 0; i < mapping.planCode.length; i++) {
      var sa = planInfo.sumInsured > 750000;
      if (planInfo.policyTerm === mapping.policyTerm[i] && planInfo.premTerm === mapping.premTerm[i] && planInfo.sumInsured >= mapping.sumInsured[i]) {
        planInfo.planCode = mapping.planCode[i];
      }
    }
    planInfo.policyTermYr = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    if (planInfo.premTerm === 'SP') {
      planInfo.premTermYr = 1;
    } else if (planInfo.premTerm.endsWith('_YR')) {
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
  } else {
    planInfo.planCode = null;
  }
}