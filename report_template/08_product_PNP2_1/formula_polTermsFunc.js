function(planDetail, quotation) {
  var options = [];
  options.push({
    value: "3",
    title: '3 Years',
    default: true
  });
  options.push({
    value: "6",
    title: '6 Years'
  });
  return options;
}