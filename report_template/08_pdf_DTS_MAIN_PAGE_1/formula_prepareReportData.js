function(quotation, planInfo, planDetails, extraPara) {
  var illust = extraPara.illustrations[planInfo.covCode];
  var deathBenefit = [];
  var basicTDC = [];
  var riderTDC = [];
  var policyTerm = parseInt(planInfo.policyTerm);
  var premTerm = parseInt(planInfo.premTerm);
  var yearCount = 0;
  for (var i = 0; i < illust.length; i++) {
    var row = illust[i];
    var policyYear = row.policyYear;
    var age = row.age;
    var policyYearAge = policyYear + ' / ' + age;
    var premium = getCurrency(Math.round(row.totalPremiumPaidToDate), '', 0);
    var incRow = false;
    if (i < 20) {
      incRow = true;
    } else {
      yearCount++;
      if (yearCount == 5) {
        yearCount = 0;
        incRow = true;
      } else if (policyYear == policyTerm) {
        incRow = true;
      }
    }
    if (incRow) {
      deathBenefit.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        guaranteed: getCurrency(Math.round(row.guaranteedDeathBenefit), '', 0),
        guaranteedsurr: getCurrency(Math.round(row.guaranteedSurrender), '', 0)
      });
      basicTDC.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        basictdc: getCurrency(Math.round(row.totalDistributionCost), '', 0)
      });
      riderTDC.push({
        policyYearAge: policyYearAge,
        ridertotalPremiun: getCurrency(Math.round(row.suppRiderPremiumPaid), '', 0),
        ridertdc: getCurrency(Math.round(row.suppTotalDistributionCost), '', 0)
      });
    }
  }
  var result = {
    illustration: {
      deathBenefit: deathBenefit,
      basicTDC: basicTDC,
      riderTDC: riderTDC
    }
  };
  return result;
}