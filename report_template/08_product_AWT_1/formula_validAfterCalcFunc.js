function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  if (planInfo.premium) {
    var mapping = planDetail.planCodeMapping;
    for (var i = 0; i < mapping.planCode.length; i++) {
      if (planInfo.premTerm === mapping.premTerm[i]) {
        planInfo.planCode = mapping.planCode[i];
        break;
      }
    }
    planInfo.policyTermYr = Number.parseInt(planInfo.policyTerm);
    planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
  } else {
    planInfo.planCode = null;
  }
}