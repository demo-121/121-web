function(quotation, planDetail, planDetails) {
  quotDriver.prepareQuotConfigs(quotation, planDetail, planDetails);
  planDetail.inputConfig.payModes = [];
  planDetail.payModes.forEach(function(payMode) {
    if (quotation.ccy !== 'USD' || payMode.mode !== 'M') {
      planDetail.inputConfig.payModes.push(payMode);
    }
  });
}