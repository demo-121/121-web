function(quotation, planInfo, planDetails, extraPara) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, Number(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  var numberTrunc = function(value, digit) {
    value = value.toString();
    if (value.indexOf('.') === -1) {
      return Number.parseInt(value);
    } else {
      value = value.substr(0, value.indexOf('.') + digit + 1);
      return Number.parseFloat(value);
    }
  };
  var paddingZeroRight = function(value) {
    value = value.toString();
    if (value.indexOf('.') > -1) {
      var decimal = value.split('.');
      if (decimal[1].length === 1) {
        decimal[1] = decimal[1] + '0';
        return decimal[0] + '.' + decimal[1];
      } else {
        return value;
      }
    } else {
      return value + '.00';
    }
  };
  var planDetail = planDetails[planInfo.covCode];
  var policyTerm = Number.parseInt(planInfo.policyTerm);
  var illustrations = {};
  illustrations.benefitsSa = [];
  if (planInfo.covCode == "PNP") {
    illustrations.packagedPlanName = "Mum’s Advantage";
  } else if (planInfo.covCode == "PNPP") {
    illustrations.packagedPlanName = "Mum’s Advantage Plus";
  } else if (planInfo.covCode == "PNP2") {
    illustrations.packagedPlanName = "Family Advantage";
  } else if (planInfo.covCode == "PNPP2") {
    illustrations.packagedPlanName = "Family Advantage Plus";
  }
  var benefitsSATable = planDetails[planInfo.covCode].rates.benefitsSA;
  var benefitsSATableIndex = 0;
  if (policyTerm == 3) {
    benefitsSATableIndex = 1;
  } else if (policyTerm == 6) {
    benefitsSATableIndex = 2;
  }
  for (var i = 1; i <= 5; i++) {
    illustrations.benefitsSa.push({
      benefitsName: benefitsSATable[i][0],
      benefitsSA: benefitsSATable[i][benefitsSATableIndex]
    });
  }
  illustrations.tdc = [];
  var commRates = runFunc(planDetail.formulas.getCOMM_TTL, quotation, planInfo, planDetail);
  var totalBasicPremium = math.multiply(planInfo.yearPrem, commRates);
  for (var i = 1; i <= policyTerm; i++) {
    illustrations.tdc.push({
      policyYearAge: i,
      distributionCost: getCurrency(totalBasicPremium, '', 0)
    });
  }
  illustrations.Basic_Total_Distribution = getCurrency(totalBasicPremium, ' ', 0);
  illustrations.totalBasicTDC_Premium = paddingZeroRight(math.number(numberTrunc(math.multiply(math.divide(totalBasicPremium, planInfo.premium), 100), 2)));
  return illustrations;
}