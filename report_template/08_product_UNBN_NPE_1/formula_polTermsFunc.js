function(planDetail, quotation) {
  var policyTermList = [];
  if (quotation.plans[0].premTerm === undefined) {
    return null;
  }
  var premiumTerm = quotation.plans[0].premTerm;
  var testAge = 50 - premiumTerm;
  var policyTerm = 0;
  var polTermDesc = '';
  if (premiumTerm === 30) {
    if (quotation.iAge <= 25) {
      policyTerm = 25;
      polTermDesc = policyTerm + ' Years';
    } else {
      policyTerm = 50;
      polTermDesc = 'To Age 50';
    }
  } else if (quotation.iAge <= testAge) {
    policyTerm = premiumTerm;
    polTermDesc = policyTerm + ' Years';
  } else if (quotation.iAge > testAge) {
    policyTerm = 50;
    polTermDesc = 'To Age 50';
  }
  policyTermList.push({
    value: policyTerm,
    title: polTermDesc,
    default: true
  });
  quotation.plans[1].policyTerm = policyTerm;
  return policyTermList;
}