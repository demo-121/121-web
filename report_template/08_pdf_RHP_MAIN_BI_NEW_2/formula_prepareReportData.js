function(quotation, planInfo, planDetails, extraPara) {
  var illust = extraPara.illustrations[planInfo.covCode];
  var deathBenefit = [];
  var surrenderValue = [];
  var deductions = [];
  var distributionCost = [];
  var retireIncSched = [];
  var retirementAge = parseInt(quotation.policyOptions.retirementAge);
  var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
  var policyTerm = planInfo.policyTerm;
  var payoutAgeMax = 0;
  if ((payoutTerm == 15) || (payoutTerm == 20)) {
    payoutAgeMax = retirementAge + payoutTerm;
  } else {
    payoutAgeMax = 99;
  }
  var i;
  var yearCount = 0;
  for (i = 0; i < illust.length; i++) {
    var row = illust[i];
    var policyYear = row.policyYear;
    var age = row.age;
    var policyYearAge = policyYear + ' / ' + age;
    var premium = getCurrency(Math.round(row.totalPremiumPaidToDate), '', 0);
    var incRow = false;
    var incRowRetire = false;
    if ((policyYear <= policyTerm) && (age <= payoutAgeMax)) {
      if (age < retirementAge) {
        if (i < 10) {
          incRow = true;
        } else {
          yearCount++;
          if (yearCount == 5) {
            yearCount = 0;
            incRow = true;
          }
        }
      } else {
        incRow = true;
      }
    }
    if ((age >= retirementAge) && (age <= payoutAgeMax)) {
      if (payoutAgeMax != 99) {
        if (age < payoutAgeMax) {
          incRowRetire = true;
        }
      } else {
        incRowRetire = true;
      }
    }
    if (incRow) {
      deathBenefit.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        guaranteed: getCurrency(Math.round(row.guaranteedDeathBenefit), '', 0),
        nonGuaranteedLow: getCurrency(0, '', 0),
        totalLow: getCurrency(Math.round(row.totalDeathBenefit['3.25']), '', 0),
        nonGuaranteedHigh: getCurrency(0, '', 0),
        totalHigh: getCurrency(Math.round(row.totalDeathBenefit['4.75']), '', 0),
      });
      surrenderValue.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        retireIncPayout: getCurrency(Math.round(row.guranteedAnnualRetireIncPayout), '', 0),
        guaranteed: getCurrency(Math.round(row.guranteedSvBenefit), '', 0),
        nonGuaranteedLow: getCurrency(0, '', 0),
        totalLow: getCurrency(Math.round(row.totalSurrenderValue['3.25']), '', 0),
        nonGuaranteedHigh: getCurrency(0, '', 0),
        totalHigh: getCurrency(Math.round(row.totalSurrenderValue['4.75']), '', 0)
      });
      deductions.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        valueOfPremiumsLow: getCurrency(Math.round(row.valueOfPremiums['3.25']), '', 0),
        effectOfDeductionsLow: getCurrency(Math.round(row.effectOfDeduction['3.25']), '', 0),
        totalSurrenderValueLow: getCurrency(Math.round(row.totalSurrenderValue['3.25']), '', 0),
        valueOfPremiumsHigh: getCurrency(Math.round(row.valueOfPremiums['4.75']), '', 0),
        effectOfDeductionsHigh: getCurrency(Math.round(row.effectOfDeduction['4.75']), '', 0),
        totalSurrenderValueHigh: getCurrency(Math.round(row.totalSurrenderValue['4.75']), '', 0)
      });
      distributionCost.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        distributionCost: getCurrency(Math.round(row.totalDistributionCost), '', 0)
      });
    }
    if (incRowRetire) {
      retireIncSched.push({
        policyYearAge: policyYearAge,
        guaranteed: getCurrency(Math.round(row.guranteedAnnualRetireIncPayout), '', 0),
        nonGuaranteedLow: getCurrency(Math.round(row.nonGuranteedAnnualRetireInc['3.25']), '', 0),
        totalLow: getCurrency(Math.round(row.totalAnnualRetireInc['3.25']), '', 0),
        nonGuaranteedHigh: getCurrency(Math.round(row.nonGuranteedAnnualRetireInc['4.75']), '', 0),
        totalHigh: getCurrency(Math.round(row.totalAnnualRetireInc['4.75']), '', 0)
      });
    }
  }
  var result = {
    illustration: {
      deathBenefit: deathBenefit,
      surrenderValue: surrenderValue,
      deductions: deductions,
      distributionCost: distributionCost,
      retireIncSched: retireIncSched
    }
  };
  return result;
}