function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  var targetAge = (99 - quotation.iAge);
  planInfo.policyTerm = "yrs" + targetAge;
  planInfo.polTermDesc = targetAge + " Years";
  planInfo.premTerm = planInfo.policyTerm;
  planInfo.premTermDesc = targetAge + " Years";
  planInfo.policyTermYr = targetAge;
  planInfo.premTermYr = targetAge;
  planInfo.premium = 0;
  planInfo.planCode = 'LARB';
  if (planInfo.sumInsured == quotation.plans[0].sumInsured && quotation.plans[0].sumInsured) {
    for (var i in quotation.plans) {
      if (quotation.plans[i].covCode === "WPSR") {
        quotDriver.context.addError({
          covCode: quotation.plans[0].covCode,
          code: 'Waiver of Premium Special cannot be attached when Living Accelerator Rider Sum Assured is equal to Basic Sum Assured'
        });
      }
    }
  }
}