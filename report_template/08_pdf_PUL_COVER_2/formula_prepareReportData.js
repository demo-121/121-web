function(quotation, planInfo, planDetails, extraPara) { /* PUL COVER PREPAREREPORTDATA */
  var basicPlan = quotation.plans[0];
  var company = extraPara.company;

  function stripDecimals(n) {
    return Math.trunc(n || 0);
  };
  var getNonRoundingNumbers = function(value) {
    return stripDecimals(value * 100) / 100;
  };
  var getGenderDesc = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getSmokingDesc = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  var ccy = quotation.ccy;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = 'US$';
  } else if (ccy == 'ASD') {
    polCcy = 'Australian Dollars';
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    polCcy = 'British Pound';
    ccySyb = '£';
  }
  return {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      planCode: basicPlan.planCode
    },
    cover: {
      sameAs: quotation.sameAs,
      proposer: {
        name: quotation.pFullName,
        gender: getGenderDesc(quotation.pGender),
        dob: new Date(quotation.pDob).format(extraPara.dateFormat),
        age: quotation.pAge,
        smoking: getSmokingDesc(quotation.pSmoke)
      },
      insured: {
        name: quotation.iFullName,
        gender: getGenderDesc(quotation.iGender),
        dob: new Date(quotation.iDob).format(extraPara.dateFormat),
        age: quotation.iAge,
        smoking: getSmokingDesc(quotation.iSmoke)
      },
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      plans: quotation.plans.map(function(plan) {
        return {
          name: plan.covName.en,
          polTermDesc: plan.polTermDesc,
          premTermDesc: plan.premTermDesc
        };
      }),
      funds: quotation.fund.funds.map(function(fund) {
        return {
          name: fund.fundName.en,
          code: fund.fundCode,
          alloc: fund.alloc + '%',
          annualPremium: getCurrency(basicPlan.yearPrem * fund.alloc / 100, '', 2),
          premium: getCurrency(basicPlan.premium * fund.alloc / 100, '', 2)
        };
      }),
      withdrawal: {
        value: "N/A",
        from: "N/A",
        to: "N/A"
      }
    },
    basicPlan: {
      name: basicPlan.covName.en,
      sumInsured: getCurrency(getNonRoundingNumbers(basicPlan.sumInsured), '', 2),
      premium: getCurrency(getNonRoundingNumbers(basicPlan.premium), '', 2),
      aPremium: getCurrency(getNonRoundingNumbers(basicPlan.yearPrem), '', 2),
      sPremium: getCurrency(getNonRoundingNumbers(basicPlan.halfYearPrem), '', 2),
      qPremium: getCurrency(getNonRoundingNumbers(basicPlan.quarterPrem), '', 2),
      mPremium: getCurrency(getNonRoundingNumbers(basicPlan.monthPrem), '', 2),
      paymentModeDesc: (quotation.paymentMode == "A" ? "Annual" : (quotation.paymentMode == "S" ? "Semi-Annual" : (quotation.paymentMode == "Q" ? "Quarterly" : "Monthly"))),
      deathBenefit: quotation.policyOptions.deathBenefit,
      deathBenefitDesc: quotation.policyOptionsDesc.deathBenefit,
      insuranceChargeDesc: quotation.policyOptions.insuranceCharge ? quotation.policyOptionsDesc.insuranceCharge : 'N/A',
      ccy: quotation.ccy,
      ccySymbol: quotation.ccy === 'SGD' ? 'S$' : (quotation.ccy === 'USD' ? 'US$' : '$'),
      polCcy: polCcy
    }
  };
}