function(planDetail, quotation) {
  var policyTermList = [];
  var hasTa65 = false;
  _.each([10, 15, 20, 25, 30], (val) => {
    if (quotation.iAge + val <= 80) {
      policyTermList.push({
        value: '' + val,
        title: val + ' years'
      });
      if (quotation.iAge + val === 65) {
        hasTa65 = true;
      }
    }
  });
  if (quotation.iAge < 65 && !hasTa65) {
    policyTermList.push({
      value: '65',
      title: 'To Age 65'
    });
  }
  return policyTermList;
}