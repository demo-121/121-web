function(quotation, planInfo, planDetails) {
  if (quotation.plans[0].premTerm) {
    planInfo.premTerm = quotation.plans[0].premTerm;
    planInfo.premTermDesc = quotation.plans[0].premTermDesc;
  }
  var multiFactor = quotation.policyOptions.multiFactor;
  var maxValue = 3500000;
  if (multiFactor) {
    var value = quotation.plans[0].sumInsured * multiFactor;
    planInfo.sumInsured = maxValue < value ? maxValue : value;
  } /*if(quotation.plans[0].sumInsured > maxValue){ planInfo.sumInsured = maxValue; }else{ planInfo.sumInsured = quotation.plans[0].sumInsured; }*/
  if (multiFactor && planInfo.premTerm) { /* planCode needed for prem calculation */
    var planCode = 'ADBB';
    var planCode2 = 'LITE_ADB';
    var premTermYr;
    var policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      planCode = Number.parseInt(planInfo.premTerm) + planCode;
      premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      planCode = planCode + Number.parseInt(planInfo.premTerm);
      premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
    if (planInfo.policyTerm.indexOf('TA') > -1) {
      policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    } else {
      policyTermYr = Number.parseInt(planInfo.policyTerm);
    }
    planInfo.premTermYr = premTermYr;
    planInfo.policyTermYr = policyTermYr;
    planInfo.planCode = planCode;
    planInfo.packagedRider = true;
    quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
    if (planInfo.sumInsured) {
      planInfo.multiplyBenefit = planInfo.sumInsured; /* * quotation.policyOptions.multiFactor;*/
      planInfo.multiplyBenefitAfter70 = planInfo.multiplyBenefit * 0.5;
      var policyYearRefer = 0;
      if (planInfo.premTerm.indexOf('YR') > -1) {
        policyYearRefer = Number.parseInt(planInfo.premTerm);
      } else {
        policyYearRefer = Number.parseInt(planInfo.premTerm) - quotation.iAge;
      }
      if (planInfo.premium) {
        var crate;
        var channel = quotation.agent.dealerGroup.toUpperCase();
        var commission_rate = planDetails[planCode2].rates.commissionRate[channel];
        planInfo.cummComm = [0];
        var cummComm = 0;
        for (var rate in commission_rate) {
          if (policyYearRefer < 15) {
            crate = commission_rate[rate][0];
          } else if (policyYearRefer < 20) {
            crate = commission_rate[rate][1];
          } else if (policyYearRefer < 25) {
            crate = commission_rate[rate][2];
          } else {
            crate = commission_rate[rate][3];
          }
          cummComm += crate * planInfo.premium;
          planInfo.cummComm.push(crate);
        }
      }
      var SA_bundleInfo = {
        "2.5": 40000,
        "3.5": 28571.43,
        "5": 20000
      };
      var SA_policy_lookup = SA_bundleInfo[quotation.policyOptions.multiFactor];
      var premRate = runFunc(planDetails[planCode2].formulas.getPremRate, quotation, planInfo, planDetails[planCode2], quotation.iAge);
      var process_premRate = premRate + 0;
      var temp = process_premRate * SA_policy_lookup * 0.001;
      temp = Math.round(temp * 10000) / 10000; /** Get rid of this kind of error 91.19999999999999*/
      var annualPrem_bundle = Math.floor(temp * 100) / 100;
      var modalFactor = 1;
      for (var p in planDetails[planCode2].payModes) {
        if (planDetails[planCode2].payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetails[planCode2].payModes[p].factor;
        }
      }
      var modlePrem_bundle = Math.round(annualPrem_bundle * modalFactor * 100) / 100;
      planInfo.annualPrem_bundle = annualPrem_bundle;
      planInfo.modlePrem_bundle = modlePrem_bundle;
    } else {
      planInfo.multiplyBenefit = null;
      planInfo.multiplyBenefitAfter70 = null;
      planInfo.premium = null;
    }
  }
}