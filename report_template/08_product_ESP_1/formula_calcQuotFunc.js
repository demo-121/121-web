function(quotation, planInfo, planDetails) {
  const planDetail = planDetails[planInfo.covCode];
  const saFunc = planDetail.formulas && planDetail.formulas.saFunc;
  const premFunc = planDetail.formulas && planDetail.formulas.premFunc;
  quotCalc.getPremTerm(quotation, planDetail, planInfo);
  quotCalc.getPolicyTerm(quotation, planDetail, planInfo);
  var calcPlanSumAssured = function(quotation, planInfo, planDetails) {
    const planDetail = planDetails[planInfo.covCode];
    const premFunc = planDetail.formulas && planDetail.formulas.premFunc;
    const saInput = planDetail.inputConfig.saInput;
    var cPlanInfo = Object.assign({}, planInfo);
    cPlanInfo.premAdjAmt = 1;
    var closestPrem = null;
    var closestSA = null;
    if (planDetail.premAdj) {
      for (var i = 0; i < planDetail.premAdj.length; i++) {
        cPlanInfo.premAdjAmt = planDetail.premAdj[i].adjRate;
        var sa;
        if (planDetail.formulas.saFuncWithPlanDetails) {
          sa = quotDriver.runFunc(planDetail.formulas.saFuncWithPlanDetails, quotation, cPlanInfo, planDetail, planDetails);
        } else {
          sa = quotDriver.runFunc(planDetail.formulas.saFunc, quotation, cPlanInfo, planDetail);
        }
        cPlanInfo.sumInsured = sa;
        if (premFunc) {
          var p = quotDriver.runFunc(planDetail.formulas.premFunc, quotation, cPlanInfo, planDetail);
          if (p === cPlanInfo.premium) {
            if (saInput && saInput.decimal) {
              var scale = math.pow(10, saInput.decimal);
              sa = math.number(math.divide(math.floor(math.multiply(math.bignumber(sa || 0), scale)), scale));
            }
            if (saInput && saInput.factor) {
              sa = math.number(math.multiply(math.ceil(math.divide(math.bignumber(sa || 0), saInput.factor)), saInput.factor));
            }
            cPlanInfo.sumInsured = sa;
            closestPrem = quotDriver.runFunc(planDetail.formulas.premFunc, quotation, cPlanInfo, planDetail);
            closestSA = sa;
          }
        } else {
          closestSA = sa;
          closestPrem = planInfo.premium;
          break;
        }
      }
    } else {
      if (planDetail.formulas.saFuncWithPlanDetails) {
        closestSA = quotDriver.runFunc(planDetail.formulas.saFuncWithPlanDetails, quotation, cPlanInfo, planDetail, planDetails);
      } else {
        closestSA = quotDriver.runFunc(planDetail.formulas.saFunc, quotation, cPlanInfo, planDetail);
      }
      if (saInput && saInput.decimal) {
        var scale = math.pow(10, saInput.decimal);
        closestSA = math.number(math.divide(math.floor(math.multiply(math.bignumber(closestSA || 0), scale)), scale));
      }
      if (saInput && saInput.factor) {
        closestSA = math.number(math.multiply(math.ceil(math.divide(math.bignumber(closestSA || 0), saInput.factor)), saInput.factor));
      }
      cPlanInfo.sumInsured = closestSA;
      if (premFunc) {
        closestPrem = quotDriver.runFunc(planDetail.formulas.premFunc, quotation, cPlanInfo, planDetail);
      } else {
        closestPrem = planInfo.premium;
      }
    }
    planInfo.premium = closestPrem;
    planInfo.sumInsured = closestSA;
    quotation.premium = math.number(math.add(math.bignumber(quotation.premium || 0), closestPrem));
    quotation.sumInsured = math.number(math.add(math.bignumber(quotation.sumInsured || 0), closestSA));
    quotCalc.updateQuotationPremium(quotation, planInfo, planDetail);
  };
  if ((planInfo.calcBy === 'sumAssured' || !planInfo.calcBy) && _.isNumber(planInfo.sumInsured) && premFunc && !_.isNaN(planInfo.sumInsured)) {
    quotCalc.calcPlanPrem(quotation, planInfo, planDetails);
  } else if ((planInfo.calcBy == 'premium' || !planInfo.calcBy) && _.isNumber(planInfo.premium) && saFunc) {
    calcPlanSumAssured(quotation, planInfo, planDetails);
  } else if (isFinite(planInfo.premium)) {
    quotation.premium = math.number(math.add(math.bignumber(quotation.premium), planInfo.premium));
    quotCalc.updateQuotationPremium(quotation, planInfo, planDetail);
  } else {}
}