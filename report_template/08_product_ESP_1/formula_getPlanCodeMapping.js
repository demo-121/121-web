function(planInfo) {
  if (planInfo.premTerm < 10) {
    return '0' + planInfo.premTerm + 'AES' + planInfo.policyTerm;
  } else {
    return planInfo.premTerm + 'AES' + planInfo.policyTerm;
  }
}