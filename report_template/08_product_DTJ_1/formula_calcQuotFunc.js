function(quotation, planInfo, planDetails) {
  var currentPlan = planDetails[planInfo.covCode];
  var round = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };
  var resetInputs = function() {
    quotation.plans[0].premTerm = undefined; /** force UI to be blank*/
    quotation.plans[0].policyTerm = undefined; /** force UI to be blank*/
    planInfo.premium = undefined; /** force UI to be blank*/
    planInfo.sumInsured = undefined; /** force UI to be blank*/
    quotation.prevPaymentMode = quotation.paymentMode;
    quotDriver.prepareAmountConfig(currentPlan, quotation, planDetails);
  };

  function calculateBasicPlan() {
    if (quotation.plans[0].policyTerm === undefined || quotation.plans[0].policyTerm === '' || quotation.plans[0].policyTerm === null) {
      return;
    }
    if (quotation.policyOptions.interRate === undefined || quotation.policyOptions.interRate === '' || quotation.policyOptions.interRate === null) {
      return;
    }
    if (planInfo.sumInsured === undefined) {
      quotation.prevPaymentMode = quotation.paymentMode;
      return;
    }
    if (quotation.prevPaymentMode) {
      if (quotation.prevPaymentMode === 'L') {
        if (quotation.paymentMode !== 'L') {
          resetInputs();
          return;
        }
      } else {
        if (quotation.paymentMode === 'L') {
          resetInputs();
          return;
        }
      }
    }
    var rates = currentPlan.rates;
    var premRateList = rates.premRate;
    var modalFactor = 0;
    var premRateL1 = 0;
    var premRateL2 = 0;
    var sumSa = 0;
    var sumAssured = 0;
    var result = null;
    var planCodeBasic = '';
    var policyTerm = parseInt(quotation.plans[0].policyTerm);
    var interRate = parseInt(quotation.policyOptions.interRate);
    var getRate = function(ref, columnNum, Rates) {
      var planRates = Rates[ref];
      if (planRates === null) {
        return 0;
      }
      return planRates[columnNum];
    };
    var getModalFactor = function() {
      switch (quotation.paymentMode) {
        case 'A':
          {
            return 1;
          }
        case 'S':
          {
            return 0.51;
          }
        case 'Q':
          {
            return 0.26;
          }
        case 'M':
          {
            return 0.0875;
          }
        default:
          {
            break;
          }
      }
      return 1;
    };
    modalFactor = getModalFactor();
    var ref = ''; /** RD00MN16 */
    if (interRate < 10) {
      interRate = "0" + interRate;
    }
    var tempTerm = policyTerm;
    if (policyTerm < 10) {
      tempTerm = "0" + policyTerm;
    }
    if (quotation.paymentMode != 'L') {
      planCodeBasic = policyTerm + "JRD" + interRate;
      ref = "" + tempTerm + "RD" + interRate + quotation.pGender + quotation.pSmoke;
      premRateL1 = getRate(ref, quotation.pAge - 16, premRateList['RP']);
      ref = "" + tempTerm + "RD" + interRate + quotation.iGender + quotation.iSmoke;
      premRateL2 = getRate(ref, quotation.iAge - 16, premRateList['RP']);
    } else {
      planCodeBasic = tempTerm + "JSD" + interRate;
      ref = "" + tempTerm + "SD" + interRate + quotation.pGender + quotation.pSmoke;
      premRateL1 = getRate(ref, quotation.pAge - 16, premRateList['SP']);
      ref = "" + tempTerm + "SD" + interRate + quotation.iGender + quotation.iSmoke;
      premRateL2 = getRate(ref, quotation.iAge - 16, premRateList['SP']);
    }
    var calculatePremiumFromSa = function(sa) {
      var annualPremium = 0;
      var regularPremium = 0;
      var lsd = 0;
      var minSa = 100000;
      var maxSa = 500000;
      var policyTerm = parseInt(quotation.plans[0].policyTerm);
      if (policyTerm >= 10) {
        maxSa = 5000000;
      }
      if (sa < minSa) { /** Single Life Min SA is 50000 , joint Life Min SA is 100000 */
        sa = minSa;
      } else if (sa > maxSa) {
        sa = maxSa;
      } else { /** set sa is multiples 1000 */
        sa = math.divide(sa, 1000);
        sa = trunc(sa, 0);
        sa = math.multiply(sa, 1000);
      }
      if (quotation.paymentMode != 'L') {
        if (sa < 200000) {
          lsd = 0.6;
        } else if (sa < 500000) {
          lsd = 0;
        } else if (sa < 1000000) {
          lsd = -0.15;
        } else {
          lsd = -0.24;
        }
      } else {
        if (sa < 200000) {
          lsd = 4;
        } else if (sa < 500000) {
          lsd = 0;
        } else if (sa < 1000000) {
          lsd = -1.5;
        } else {
          lsd = -2.4;
        }
      }
      var xk = 0;
      var yk = 0;
      if (quotation.pGender == 'M' && quotation.pSmoke == 'N') {
        xk = quotation.pAge;
      } else if (quotation.pGender == 'M' && quotation.pSmoke == 'Y') {
        xk = quotation.pAge + 3;
      } else if (quotation.pGender == 'F' && quotation.pSmoke == 'N') {
        xk = quotation.pAge - 3;
      } else if (quotation.pGender == 'F' && quotation.pSmoke == 'Y') {
        xk = quotation.pAge;
      }
      if (quotation.iGender == 'M' && quotation.iSmoke == 'N') {
        yk = quotation.iAge;
      } else if (quotation.iGender == 'M' && quotation.iSmoke == 'Y') {
        yk = quotation.iAge + 3;
      } else if (quotation.iGender == 'F' && quotation.iSmoke == 'N') {
        yk = quotation.iAge - 3;
      } else if (quotation.iGender == 'F' && quotation.iSmoke == 'Y') {
        yk = quotation.iAge;
      }
      var discount = 0.85;
      if (math.max(xk, yk) < 35) {
        discount = 0.75;
      }
      var np = round(math.add(math.multiply(discount, math.add(premRateL1, premRateL2)), lsd), 2);
      annualPremium = math.divide(round(math.multiply(math.bignumber(np), sa), 2), math.bignumber(1000));
      annualPremium = round(annualPremium, 2); /* compute annualPremium*/
      regularPremium = math.multiply(annualPremium, math.bignumber(modalFactor));
      regularPremium = trunc(regularPremium, 2); /* Math.floor(temp * 100)/100; ROUNDDOWN to 2 decimal places*/
      return {
        sa: sa,
        annualPremium: annualPremium,
        regularPremium: regularPremium
      };
    };
    sumAssured = planInfo.sumInsured;
    result = calculatePremiumFromSa(sumAssured);
    sumAssured = math.number(result.sa);
    quotation.sumInsured = sumAssured;
    quotation.annualPremium = math.number(result.annualPremium);
    planInfo.yearPrem = math.number(result.annualPremium);
    planInfo.halfYearPrem = math.number(trunc(math.multiply(math.bignumber(result.annualPremium), math.bignumber(0.51)), 2)); /** Math.floor(result.annualPremium * 0.51 * 100)/100;*/
    planInfo.quarterPrem = math.number(trunc(math.multiply(math.bignumber(result.annualPremium), math.bignumber(0.26)), 2)); /** Math.floor(result.annualPremium * 0.26 * 100)/100;*/
    planInfo.monthPrem = math.number(trunc(math.multiply(math.bignumber(result.annualPremium), math.bignumber(0.0875)), 2)); /** Math.floor(result.annualPremium * 0.0875 * 100)/100;*/
    quotation.totYearPrem = planInfo.yearPrem;
    quotation.totHalfyearPrem = planInfo.halfYearPrem;
    quotation.totQuarterPrem = planInfo.quarterPrem;
    quotation.totMonthPrem = planInfo.monthPrem;
    if (quotation.paymentMode === 'L') {
      planInfo.singlePrem = math.number(result.annualPremium);
      quotation.totSinglePrem = planInfo.singlePrem;
    }
    switch (quotation.paymentMode) {
      case 'A':
        {
          planInfo.premium = planInfo.yearPrem;quotation.premium = planInfo.yearPrem;
          break;
        }
      case 'S':
        {
          planInfo.premium = planInfo.halfYearPrem;quotation.premium = planInfo.halfYearPrem;
          break;
        }
      case 'Q':
        {
          planInfo.premium = planInfo.quarterPrem;quotation.premium = planInfo.quarterPrem;
          break;
        }
      case 'M':
        {
          planInfo.premium = planInfo.monthPrem;quotation.premium = planInfo.monthPrem;
          break;
        }
      default:
        {
          planInfo.premium = planInfo.yearPrem;quotation.premium = planInfo.yearPrem;
          break;
        }
    }
    planInfo.planCode = planCodeBasic;
    planInfo.sumInsured = sumAssured;
  }
  calculateBasicPlan();
  planInfo.noApplication = true;
  planInfo.noApplicationMsg = "Please print the generated PI and proceed with paper submission.";
}