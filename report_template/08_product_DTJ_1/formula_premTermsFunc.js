function(planDetail, quotation) {
  var premTermList = [];
  var policyTerm = parseInt(quotation.plans[0].policyTerm);
  var premTerm = 0;
  var premTermDesc = '';
  if (quotation.paymentMode == 'L') {
    premTerm = 1;
    premTermDesc = 'Single Premium', premTermList.push({
      value: premTerm,
      title: premTermDesc
    });
  } else {
    premTerm = policyTerm - 3;
    premTermDesc = premTerm + ' Years';
    premTermList.push({
      value: premTerm,
      title: premTermDesc
    });
  }
  if (quotation.plans[0].policyTerm === undefined) {
    quotation.plans[0].premTerm = undefined;
  } else {
    quotation.plans[0].premTerm = premTerm;
  }
  return premTermList;
}