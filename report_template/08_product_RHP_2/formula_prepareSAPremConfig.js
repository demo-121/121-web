function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation, planDetails);
  var premMin = {
    A: 2400,
    S: 1224,
    Q: 624,
    M: 210
  };
  if (quotation.paymentMode !== 'L') {
    planDetail.inputConfig.premlim = {
      min: premMin[quotation.paymentMode]
    };
    planDetail.inputConfig.planInfoHintMsg = 'Minimum Retirement Income is S$6,000';
  } else {
    if (quotation.policyOptions.paymentMethod === 'cash') {
      planDetail.inputConfig.premlim = {
        min: 20000
      };
      planDetail.inputConfig.planInfoHintMsg = 'Retirement Income must meet minimum premium of S$20,000.';
    } else {
      planDetail.inputConfig.premlim = {
        min: 12500
      };
      planDetail.inputConfig.planInfoHintMsg = 'Retirement Income must meet minimum premium of S12,500.';
    }
  }
}