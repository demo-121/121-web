function(quotation, planInfo, planDetails) {
  const PLAN_TYPE_BASIC = 0;
  const PLAN_TYPE_RIDER_TPD = 1;
  const CALC_TYPE_INPUT_PREMIUM = 1;
  const CALC_TYPE_INPUT_RETIRE_INC = 2;
  const PAYOUT_TERM_LIFETIME = 99;
  const MINIMUM_RETIREMENT_INCOME_RP = 6000;
  const MINIMUM_RETIREMENT_INCOME_SP = 20000;
  const MINIMUM_RETIREMENT_INCOME_SRS = 12500;
  var premiumTerm = quotation.plans[0].premTerm;
  var getPlanCode = function(planType) {
    var s = '';
    var payoutType = quotation.policyOptions.payoutType;
    var retirementAge = quotation.policyOptions.retirementAge;
    var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
    switch (premiumTerm) {
      case 1:
        {
          s += '01';
          break;
        }
      case 5:
        {
          s += '05';
          break;
        }
      case 10:
        {
          s += '10';
          break;
        }
      case 15:
        {
          s += '15';
          break;
        }
      case 20:
        {
          s += '20';
          break;
        }
      case 25:
        {
          s += '25';
          break;
        }
      default:
        {
          s += '05';
          break;
        }
    }
    switch (planType) {
      case PLAN_TYPE_BASIC:
        {
          if (payoutType === 'Inflated') {
            s += 'SI';
          } else {
            s += 'SL';
          }
          break;
        }
      case PLAN_TYPE_RIDER_TPD:
        {
          if (payoutType === 'Inflated') {
            s += 'TI';
          } else {
            s += 'TL';
          }
          break;
        }
      default:
        {
          if (payoutType === 'Inflated') {
            s += 'SI';
          } else {
            s += 'SL';
          }
          break;
        }
    }
    s += retirementAge;
    switch (payoutTerm) {
      case 15:
        {
          s += 'A';
          break;
        }
      case 20:
        {
          s += 'B';
          break;
        }
      default:
        {
          s += 'C';
          break;
        }
    }
    return s;
  };

  function calculateBasicPlan() {
    var currentPlan = planDetails[planInfo.covCode];
    var rates = currentPlan.rates;
    var LEP2_GRI_rates = rates.LEP2_GRI_rates;
    var LEP2_premRates = rates.LEP2_premRates;
    var commModalFactor = rates.commModalFactor;
    var planCodeBasic = '';
    var griRate = 0;
    var premRate = 0;
    var modalFactor = 0;
    var retirementAge = parseInt(quotation.policyOptions.retirementAge);
    var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
    if (quotation.lastCalcType === null) {
      quotation.lastCalcType = CALC_TYPE_INPUT_PREMIUM;
    }
    var get_LEP2_GRI_rate = function(rlPlan, insuredAge) {
      var planRates = LEP2_GRI_rates[rlPlan];
      if (planRates === null) {
        return 0;
      }
      return planRates[insuredAge];
    };
    var get_LEP2_premRate = function(rlPlan, insuredAge) {
      var ref = rlPlan;
      ref += quotation.iGender;
      ref += quotation.iSmoke;
      var planRates = LEP2_premRates[ref];
      if (planRates === null) {
        return 0;
      }
      return planRates[insuredAge];
    };
    var getModalFactor = function() {
      switch (quotation.paymentMode) {
        case 'A':
          {
            return commModalFactor.annual;
          }
        case 'S':
          {
            return commModalFactor.semiAnnual;
          }
        case 'Q':
          {
            return commModalFactor.quarterly;
          }
        case 'M':
          {
            return commModalFactor.monthly;
          }
        default:
          {
            break;
          }
      }
      return commModalFactor.annual;
    }; /** Will return true if current inputs are same with previous inputs*/
    var checkIfSameInputs = function() {
      if (quotation.prevPaymentMode !== quotation.paymentMode) {
        return false;
      }
      if (quotation.prevRetirementAge !== retirementAge) {
        return false;
      }
      if (quotation.prevPayoutTerm !== payoutTerm) {
        return false;
      }
      if (quotation.prevPayoutType !== quotation.policyOptions.payoutType) {
        return false;
      }
      if (quotation.prevPaymentTerm !== planInfo.premTerm) {
        return false;
      }
      if (Math.floor(quotation.prevPremium * 100) !== Math.floor(planInfo.premium * 100)) {
        return false;
      }
      if (Math.floor(quotation.prevRetireInc * 100) !== Math.floor(planInfo.retirementIncome * 100)) {
        return false;
      }
      return true;
    };
    var resetInputs = function() {
      quotation.plans[0].premTerm = undefined; /** force UI to be blank*/
      planInfo.retirementIncome = NaN; /** force UI to be blank*/
      planInfo.premium = NaN; /** force UI to be blank*/
      quotation.prevPaymentMode = quotation.paymentMode;
    };
    var calculatePremiumFromRetirementIncome = function(retirementIncome) {
      var temp;
      if (quotation.policyOptions.paymentMethod === 'srs') {
        if (retirementIncome < MINIMUM_RETIREMENT_INCOME_SRS) {
          quotDriver.context.addError({
            covCode: quotation.baseProductCode,
            msg: 'Please increase the Retirement Income to meet the minimum premium of S$12,500'
          });
        }
      } else { /** cash*/
        if (quotation.paymentMode !== 'L') { /** RP*/
          if (retirementIncome < MINIMUM_RETIREMENT_INCOME_RP) {
            quotDriver.context.addError({
              covCode: quotation.baseProductCode,
              msg: 'Minimum Retirement Income is S$6,000'
            });
          }
        }
      }
      var basicSumAssured = retirementIncome / griRate * 100;
      var roundedBSA;
      var annualPremium;
      var regularPremium;
      temp = Math.ceil(basicSumAssured / 100) * 100; /* To make multiple by 100*/
      roundedBSA = temp; /* compute annualPremium*/
      temp = Math.floor(premRate * roundedBSA * 100) / 100; /* TRUNCATE to 2 decimal places*/
      temp = temp / 1000;
      temp = Math.round(temp * 100) / 100; /* ROUND to 2 decimal places*/
      annualPremium = temp; /* compute annualPremium*/
      temp = annualPremium * modalFactor;
      temp = Math.floor(temp * 100) / 100; /* ROUNDDOWN to 2 decimal places*/
      regularPremium = temp;
      return {
        basicSumAssured: basicSumAssured,
        roundedBSA: roundedBSA,
        annualPremium: annualPremium,
        regularPremium: regularPremium
      };
    };
    var calculateRetirementIncomeFromPremium = function(premium) {
      var temp;
      var regularPremium = premium * 0.98;
      var annualPremium = regularPremium / modalFactor;
      var roundedBSA;
      var retirementIncome;
      var apAfterRoundedBSA;
      var rpAfterRoundedBSA;
      var parPremium;
      temp = annualPremium / premRate * 1000;
      temp = Math.ceil(temp / 100) * 100;
      roundedBSA = Math.floor(temp);
      retirementIncome = Math.round(roundedBSA * griRate) / 100; /* compute annualPremium after RoundedBSA*/
      temp = Math.floor(premRate * roundedBSA * 100) / 100; /* TRUNCATE to 2 decimal places*/
      temp = temp / 1000.0;
      temp = Math.round(temp * 1000) / 1000; /* ROUND to 3 decimal places*/
      temp = Math.round(temp * 100) / 100; /* ROUND to 2 decimal places*/
      apAfterRoundedBSA = temp; /* compute regularPremium after RoundedBSA*/
      temp = apAfterRoundedBSA * modalFactor;
      temp = Math.floor(temp * 100) / 100; /* ROUNDDOWN to 2 decimal places*/
      rpAfterRoundedBSA = temp;
      quotation.lastCalcType = CALC_TYPE_INPUT_RETIRE_INC;
      return {
        annualPremium: annualPremium,
        roundedBSA: roundedBSA,
        retirementIncome: retirementIncome,
        apAfterRoundedBSA: apAfterRoundedBSA,
        rpAfterRoundedBSA: rpAfterRoundedBSA
      };
    };
    var calcType = 0;
    var isSamePremiumInput = false;
    var isSameRetireIncInput = false; /** Check if there is premTerm*/
    var curPlanDetails = planDetails[planInfo.covCode];
    if (quotation.plans[0].premTerm > 0) { /** Enable RI and premium input*/
      curPlanDetails.inputConfig.canEditPremium = true;
      curPlanDetails.inputConfig.canEditOthSa = true;
    } else { /** Disable RI and premium input*/
      curPlanDetails.inputConfig.canEditPremium = false;
      curPlanDetails.inputConfig.canEditOthSa = false;
    }
    if ((planInfo.premium === undefined) && (planInfo.retirementIncome === undefined)) {
      quotation.prevPaymentMode = quotation.paymentMode;
      return;
    }
    if (planInfo.premium === undefined) {
      planInfo.premium = 0;
      calcType = CALC_TYPE_INPUT_RETIRE_INC;
    }
    if (planInfo.retirementIncome === undefined) {
      planInfo.retirementIncome = 0;
      calcType = CALC_TYPE_INPUT_PREMIUM;
    }
    if ((planInfo.premium == 0) && (planInfo.retirementIncome == 0)) {
      quotation.prevPaymentMode = quotation.paymentMode;
      return;
    }
    if (quotation.prevPremium === undefined) {
      quotation.prevPremium = 0;
    }
    if (quotation.prevRetireInc === undefined) {
      quotation.prevRetireInc = 0;
    }
    if ((planInfo.premium == null) && (planInfo.retirementIncome == 0)) {
      planInfo.retirementIncome = NaN; /** force UI to be blank*/
      quotation.prevPaymentMode = quotation.paymentMode;
      return;
    }
    if ((planInfo.premium == null) && (planInfo.retirementIncome == null)) {
      planInfo.retirementIncome = NaN; /** force UI to be blank*/
      quotation.prevPaymentMode = quotation.paymentMode;
      return;
    }
    if (quotation.prevPaymentMode) { /** Change from RP to SP or vice versa*/
      if (quotation.prevPaymentMode === 'L') {
        if (quotation.paymentMode !== 'L') {
          resetInputs();
          quotation.prevPaymentMode = quotation.paymentMode;
          return;
        }
      } else {
        if (quotation.paymentMode === 'L') {
          resetInputs();
          quotation.prevPaymentMode = quotation.paymentMode;
          return;
        }
      }
    } /** compare money as integers*/
    var premium_1 = Math.floor(planInfo.premium * 100);
    var premium_2 = Math.floor(quotation.prevPremium * 100);
    var retire_1 = Math.floor(planInfo.retirementIncome * 100);
    var retire_2 = Math.floor(quotation.prevRetireInc * 100);
    if (checkIfSameInputs()) {
      calcType = quotation.prevCalcType;
      if (quotation.prevCalcType == CALC_TYPE_INPUT_PREMIUM) {
        isSamePremiumInput = true;
      } else {
        isSameRetireIncInput = true;
      }
    } else {
      if (planInfo.retirementIncome) { /**Auto adjust retirementIncome if regular premium*/
        if (quotation.paymentMode !== 'L') {
          if (planInfo.retirementIncome < MINIMUM_RETIREMENT_INCOME_RP) {
            planInfo.retirementIncome = MINIMUM_RETIREMENT_INCOME_RP;
            retire_1 = planInfo.retirementIncome * 100;
          }
        }
      }
      if (calcType == 0) {
        if ((quotation.prevPremInput === undefined) && (quotation.prevCalcType === undefined) && (planInfo.premium > 0)) {
          calcType = CALC_TYPE_INPUT_PREMIUM;
          isSamePremiumInput = false;
        } else {
          if (quotation.prevCalcType == CALC_TYPE_INPUT_PREMIUM) {
            if (premium_1 == premium_2) {
              if (retire_1 != retire_2) {
                calcType = CALC_TYPE_INPUT_RETIRE_INC;
              } else {
                calcType = CALC_TYPE_INPUT_PREMIUM;
                isSamePremiumInput = true;
              }
            } else {
              calcType = CALC_TYPE_INPUT_PREMIUM;
              isSamePremiumInput = false;
            }
          }
          if (quotation.prevCalcType == CALC_TYPE_INPUT_RETIRE_INC) {
            if (retire_1 == retire_2) {
              if (premium_1 != premium_2) {
                calcType = CALC_TYPE_INPUT_PREMIUM;
              } else {
                calcType = CALC_TYPE_INPUT_RETIRE_INC;
              }
            } else {
              calcType = CALC_TYPE_INPUT_RETIRE_INC;
            }
          }
        }
      }
    }
    planCodeBasic = getPlanCode(PLAN_TYPE_BASIC);
    griRate = get_LEP2_GRI_rate(planCodeBasic, quotation.iAge);
    premRate = get_LEP2_premRate(planCodeBasic, quotation.iAge);
    modalFactor = getModalFactor();
    var result = null;
    var sumAssured = 0;
    var benefitTerm = retirementAge;
    var policyTerm = 0;
    if (payoutTerm == PAYOUT_TERM_LIFETIME) {
      benefitTerm += 99 - retirementAge;
      benefitTerm -= quotation.iAge;
      policyTerm = 99;
      planInfo.polTermDesc = 'Until Age 99';
    } else {
      benefitTerm += payoutTerm;
      benefitTerm -= quotation.iAge;
      policyTerm = benefitTerm;
      planInfo.polTermDesc = benefitTerm + ' Years';
    }
    quotation.benefitTerm = benefitTerm;
    planInfo.policyTerm = policyTerm;
    quotation.policyTerm = policyTerm;
    if (calcType == CALC_TYPE_INPUT_PREMIUM) { /** premium input change*/
      var inputPremium = isSamePremiumInput ? quotation.prevPremInput : planInfo.premium;
      result = calculateRetirementIncomeFromPremium(inputPremium);
      sumAssured = result.roundedBSA;
      quotation.annualPremium = result.annualPremium;
      quotation.annualPremiumBSA = result.apAfterRoundedBSA;
      quotation.sumInsured = sumAssured;
      quotation.prevPremInput = inputPremium;
      quotation.prevRetireInc = result.retirementIncome;
      quotation.prevCalcType = CALC_TYPE_INPUT_PREMIUM;
      planInfo.retirementIncome = result.retirementIncome;
      planInfo.yearPrem = result.apAfterRoundedBSA;
      planInfo.halfYearPrem = Math.floor(result.apAfterRoundedBSA * 0.51 * 100) / 100;
      planInfo.quarterPrem = Math.floor(result.apAfterRoundedBSA * 0.26 * 100) / 100;
      planInfo.monthPrem = Math.floor(result.apAfterRoundedBSA * 0.0875 * 100) / 100;
      quotation.totYearPrem = planInfo.yearPrem;
      quotation.totHalfyearPrem = planInfo.halfYearPrem;
      quotation.totQuarterPrem = planInfo.quarterPrem;
      quotation.totMonthPrem = planInfo.monthPrem;
      if (premiumTerm == 1) {
        planInfo.singlePrem = result.apAfterRoundedBSA;
        quotation.totSinglePrem = planInfo.singlePrem;
      }
      if (quotation.paymentMode === 'L') {} else {
        if (planInfo.retirementIncome < MINIMUM_RETIREMENT_INCOME_RP) {
          quotDriver.context.addError({
            covCode: quotation.baseProductCode,
            msg: 'Please increase the premium to meet minimum Retirement Income of S$6,000'
          });
        }
      }
    } else { /** retirement income input change*/
      var inputRetireInc = isSameRetireIncInput ? quotation.prevRetireInc : planInfo.retirementIncome;
      result = calculatePremiumFromRetirementIncome(inputRetireInc);
      sumAssured = result.roundedBSA;
      quotation.sumInsured = sumAssured;
      quotation.annualPremium = result.annualPremium;
      quotation.prevRetireInc = inputRetireInc;
      quotation.prevCalcType = CALC_TYPE_INPUT_RETIRE_INC;
      planInfo.yearPrem = result.annualPremium;
      planInfo.halfYearPrem = Math.floor(result.annualPremium * 0.51 * 100) / 100;
      planInfo.quarterPrem = Math.floor(result.annualPremium * 0.26 * 100) / 100;
      planInfo.monthPrem = Math.floor(result.annualPremium * 0.0875 * 100) / 100;
      quotation.totYearPrem = planInfo.yearPrem;
      quotation.totHalfyearPrem = planInfo.halfYearPrem;
      quotation.totQuarterPrem = planInfo.quarterPrem;
      quotation.totMonthPrem = planInfo.monthPrem;
      if (premiumTerm == 1) {
        planInfo.singlePrem = result.annualPremium;
        quotation.totSinglePrem = planInfo.singlePrem;
      }
    }
    switch (quotation.paymentMode) {
      case 'A':
        {
          planInfo.premium = planInfo.yearPrem;quotation.premium = planInfo.yearPrem;
          break;
        }
      case 'S':
        {
          planInfo.premium = planInfo.halfYearPrem;quotation.premium = planInfo.halfYearPrem;
          break;
        }
      case 'Q':
        {
          planInfo.premium = planInfo.quarterPrem;quotation.premium = planInfo.quarterPrem;
          break;
        }
      case 'M':
        {
          planInfo.premium = planInfo.monthPrem;quotation.premium = planInfo.monthPrem;
          break;
        }
      default:
        {
          planInfo.premium = planInfo.yearPrem;quotation.premium = planInfo.yearPrem;
          break;
        }
    }
    planInfo.planCode = planCodeBasic;
    planInfo.sumAssured = sumAssured;
    quotation.prevPaymentMode = quotation.paymentMode;
    quotation.prevRetirementAge = retirementAge;
    quotation.prevPayoutTerm = payoutTerm;
    quotation.prevPayoutType = quotation.policyOptions.payoutType;
    quotation.prevPaymentTerm = planInfo.premTerm;
    quotation.prevPremium = planInfo.premium;
  }
  calculateBasicPlan();
}