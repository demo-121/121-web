function(planDetail, quotation, planDetails) {
  quotDriver.preparePolicyOptions(planDetail, quotation, planDetails);
  var policyOptions = _.cloneDeep(planDetail.policyOptions);
  var validRetirementAges = [];
  var validAgeOptions = [];
  var isRetirementAgeValidOption = function(age) {
    var i;
    for (i = 0; i < validRetirementAges.length; i++) {
      if (age == validRetirementAges[i]) {
        return true;
      }
    }
    return false;
  };

  function prepareRetirementAgeList() {
    var hasPremTerm = 0;
    var basicPlanPremTerm = 0;
    var retirementAge = 0;
    var isRetirementAgeAlreadyPushed = function(age) {
      var i;
      for (i = 0; i < validRetirementAges.length; i++) {
        if (age == validRetirementAges[i]) {
          return true;
        }
      }
      return false;
    };
    if (quotation.plans[0].premTerm) {
      hasPremTerm = 1;
    }
    if (hasPremTerm > 0) {
      basicPlanPremTerm = quotation.plans[0].premTerm;
    } /** Check if Single premium*/
    if (quotation.paymentMode === 'L') {
      basicPlanPremTerm = 0;
    }
    if (quotation.paymentMode === 'L') { /** Single premium */
      retirementAge = quotation.iAge + 5; /** Calculate valid ages*/
      if (retirementAge <= 50) {
        validRetirementAges = [50, 55, 60, 65, 70];
      } else if ((retirementAge > 50) && (retirementAge <= 55)) {
        validRetirementAges = [55, 60, 65, 70];
      } else if ((retirementAge > 55) && (retirementAge <= 60)) {
        validRetirementAges = [60, 65, 70];
      } else if (retirementAge >= 61 && retirementAge <= 64) {
        validRetirementAges = [retirementAge, 65, 70];
      } else if (retirementAge == 65) {
        validRetirementAges = [65, 70];
      } else {
        validRetirementAges = [70];
      }
    } else { /** Regular premium */
      var temp;
      var index;
      var curPremterm;
      var validPremterms = [5, 10, 15, 20, 25];
      for (index = 0; index < validPremterms.length; index++) {
        curPremterm = validPremterms[index];
        temp = quotation.iAge + curPremterm + 5;
        if (temp <= 50) {
          if (!isRetirementAgeAlreadyPushed(50)) {
            validRetirementAges.push(50);
          }
        }
        if (temp <= 55) {
          if (!isRetirementAgeAlreadyPushed(55)) {
            validRetirementAges.push(55);
          }
        }
        if (temp <= 60) {
          if (!isRetirementAgeAlreadyPushed(60)) {
            validRetirementAges.push(60);
          }
        }
        if (temp == 61) {
          if (!isRetirementAgeAlreadyPushed(61)) {
            validRetirementAges.push(61);
          }
        }
        if (temp == 62) {
          if (!isRetirementAgeAlreadyPushed(62)) {
            validRetirementAges.push(62);
          }
        }
        if (temp == 63) {
          if (!isRetirementAgeAlreadyPushed(63)) {
            validRetirementAges.push(63);
          }
        }
        if (temp == 64) {
          if (!isRetirementAgeAlreadyPushed(64)) {
            validRetirementAges.push(64);
          }
        }
        if (temp <= 65) {
          if (!isRetirementAgeAlreadyPushed(65)) {
            validRetirementAges.push(65);
          }
        }
        if (temp <= 70) {
          if (!isRetirementAgeAlreadyPushed(70)) {
            validRetirementAges.push(70);
          }
        }
      }
    } /** Default retirement age options*/
    var defaultAgeOptions = policyOptions[1].options;
    var index;
    for (index = 0; index < defaultAgeOptions.length; index++) {
      var cur_entry = defaultAgeOptions[index];
      if (isRetirementAgeValidOption(parseInt(cur_entry.value))) {
        validAgeOptions.push(cur_entry);
      }
    }
    policyOptions[1].options = validAgeOptions;
  }
  prepareRetirementAgeList(); /** Update retirementAge selected option*/
  var prevRetirementAge = parseInt(quotation.policyOptions.retirementAge);
  if (!isRetirementAgeValidOption(prevRetirementAge)) {
    quotation.policyOptions.retirementAge = validAgeOptions[0].value;
  } /** init payment method */
  if (!quotation.policyOptions.paymentMethod) {
    quotation.policyOptions.paymentMethod = 'cash';
  } /** if not single premium, remove srs in payment type */
  if (quotation.paymentMode != 'L') {
    policyOptions[0].options.splice(1, 1);
    if (quotation.policyOptions.paymentMethod === 'srs') {
      quotation.policyOptions.paymentMethod = 'cash';
    }
  } /** update default value*/
  _.forEach(policyOptions, po => {
    po.value = _.get(po, 'options[0].value', '');
  });
  planDetail.inputConfig.policyOptions = policyOptions;
}