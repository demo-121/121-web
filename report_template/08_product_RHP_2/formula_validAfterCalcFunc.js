function(quotation, planInfo, planDetail) {
  const CALC_TYPE_INPUT_PREMIUM = 1;
  const CALC_TYPE_INPUT_RETIRE_INC = 2;
  quotValid.validateMandatoryFields(quotation, planInfo, planDetail);
  if (quotation.plans[0].premium === undefined) {
    return;
  }
  var totalPrem = 0;
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    if (plan.premium) {
      totalPrem += plan.premium;
    }
  }
  totalPrem = math.number(totalPrem);
  if (planDetail.inputConfig && planDetail.inputConfig.premlim) {
    let min = planDetail.inputConfig.premlim.min;
    if (min && min > totalPrem) {
      if (quotation.prevCalcType == CALC_TYPE_INPUT_RETIRE_INC) {
        var minPrem = getCurrency(min, 'S$', 0);
        quotDriver.context.addError({
          covCode: quotation.baseProductCode,
          msg: 'Please increase the Retirement Income to meet the minimum premium of ' + minPrem
        });
      } else {
        var err = {
          covCode: planInfo.covCode,
          msgPara: [],
          code: ''
        };
        err.msgPara.push(getCurrency(min, 'S$', 0));
        err.code = 'js.err.invalid_premlim_min';
        err.msgPara.push(totalPrem);
        quotDriver.context.addError(err);
      }
    }
  }
}