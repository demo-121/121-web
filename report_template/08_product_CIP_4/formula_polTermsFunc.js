function(planDetail, quotation) {
  var bpPolTerm = Number.parseInt(quotation.plans[0].policyTerm);
  if (!bpPolTerm) {
    return [];
  }
  var policyTermList = [];
  if (quotation.policyOptions.planType === 'renew') {
    policyTermList.push({
      value: bpPolTerm + '_YR',
      title: bpPolTerm + ' Years'
    });
  } else if (quotation.policyOptions.planType === 'toAge') {
    var values = [50, 55, 60, 65, 70, 75, 99];
    for (var v in values) {
      var value = values[v];
      if (value - quotation.iAge >= 5 && value <= bpPolTerm) {
        policyTermList.push({
          value: value + '_TA',
          title: 'To Age ' + value
        });
      }
    }
  }
  return policyTermList;
}