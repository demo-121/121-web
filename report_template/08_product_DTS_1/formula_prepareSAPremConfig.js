function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation, planDetails);
  planDetail.inputConfig.planInfoHintMsg = 'Sum Assured must be from S$50,000 and in multiples of S$1000';
}