function(planDetail, quotation, planDetails) {
  var covCode = planDetail.covCode;
  var basicSa = math.number(quotation.plans[0].sumInsured || 0);
  planDetail.inputConfig.canEditSumAssured = false;
  planDetail.inputConfig.canViewSumAssured = true;
  planDetail.inputConfig.canViewPremium = false;
  for (var i in quotation.plans) {
    var plan = quotation.plans[i];
    if (plan.covCode == covCode && basicSa) {
      plan.sumInsured = math.number(basicSa * 0.1 > 75000 ? 75000 : basicSa * 0.1);
    }
  }
  return planDetail;
}