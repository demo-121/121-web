function(quotation, planInfo, planDetails, extraPara) { /*mainFormula*/
  try {
    var trunc = function(value, position) {
      if (!position) position = 0;
      var scale = math.pow(10, position);
      return math.number(math.divide(math.floor(math.multiply(value, scale)), scale));
    };
    let {
      compName,
      compRegNo,
      compAddr,
      compAddr2,
      compTel,
      compFax,
      compWeb
    } = extraPara.company;
    var retVal = {
      'compName': compName,
      'compRegNo': compRegNo,
      'compAddr': compAddr,
      'compAddr2': compAddr2,
      'compTel': compTel,
      'compFax': compFax,
      'compWeb': compWeb
    };
    retVal.rate_low = "3.25";
    retVal.rate_high = "4.75";
    retVal.headerGender = (quotation.sameAs == "Y" ? quotation.pGender : quotation.iGender) == 'M' ? 'Male' : 'Female';
    retVal.headerSmoke = (quotation.sameAs == "Y" ? quotation.pSmoke : quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
    retVal.headerAge = quotation.sameAs == "Y" ? quotation.pAge : quotation.iAge;
    var ccy = quotation.ccy;
    var polCcy = '';
    var ccySyb = '';
    if (ccy == 'SGD') {
      polCcy = 'Singapore Dollars';
      ccySyb = 'S$';
    } else if (ccy == 'USD') {
      polCcy = 'US Dollars';
      ccySyb = 'US$';
    } else if (ccy == 'ASD') {
      polCcy = 'Australian Dollars';
      ccySyb = 'A$';
    } else if (ccy == 'EUR') {
      polCcy = 'Euro';
      ccySyb = '€';
    } else if (ccy == 'GBP') {
      polCcy = 'British Pound';
      ccySyb = '£';
    }
    retVal.polCcy = polCcy;
    retVal.ccySyb = ccySyb;
    var basicPlan = quotation.plans[0];
    var additionalPlanTitle = ' ' + basicPlan.premTerm.toString() + ' PAY';
    retVal.basicPlanName = basicPlan.covName.en + additionalPlanTitle;
    retVal.basicSumAssured = getCurrency(basicPlan.sumInsured, ' ', 0);
    retVal.policyCurrency = polCcy;
    retVal.policyTerm = basicPlan.polTermDesc;
    retVal.premiumPaymentTerm = basicPlan.premTermDesc;
    var paymentModeTitle = '';
    if (quotation.paymentMode == 'A') {
      paymentModeTitle = 'Annual';
    } else if (quotation.paymentMode == 'S') {
      paymentModeTitle = 'Semi-Annual';
    } else if (quotation.paymentMode == 'Q') {
      paymentModeTitle = 'Quarterly';
    } else if (quotation.paymentMode == 'M') {
      paymentModeTitle = 'Monthly';
    }
    retVal.paymentFrequency = paymentModeTitle;
    retVal.guaranteedCashPayoutType = quotation.policyOptionsDesc.guaranteedCashPayoutType.en;
    var illustrations = extraPara.illustrations;
    retVal.deduct_low = getCurrency(illustrations.secondTdc.EOTD_L, ' ', 0);
    retVal.deduct_high = getCurrency(illustrations.secondTdc.EOTD_H, ' ', 0);
    retVal.reductInYield_low = illustrations.secondTdc.REDUC_YIELD_L;
    retVal.reductInYield_high = illustrations.secondTdc.REDUC_YIELD_H;
    retVal.basicAnnualPremium = getCurrency(illustrations.premiumTPDLookup, ' ', 2);
    retVal.illustrations = {};
    retVal.illustrations.totgteedCashPayoutsMinus2 = {};
    retVal.illustrations.totgteedCashPayoutsMinus1 = {};
    retVal.illustrations.totgteedCashPayoutsEnd = {};
    retVal.illustrations.totPremiumPaid = getCurrency(trunc(Number(illustrations.totPremiumPaid), 0), ' ', 0);
    retVal.illustrations.totgteedCashPayoutsMinus2.value = getCurrency(trunc(Number(illustrations.totgteedCashPayoutsMinus2.value), 0), ' ', 0);
    retVal.illustrations.totgteedCashPayoutsMinus1.value = getCurrency(trunc(Number(illustrations.totgteedCashPayoutsMinus1.value), 0), ' ', 0);
    retVal.illustrations.totgteedCashPayoutsEnd.value = getCurrency(trunc(Number(illustrations.totgteedCashPayoutsEnd.value), 0), ' ', 0);
    retVal.illustrations.nonGteedBonus475Return = getCurrency(trunc(Number(illustrations.nonGteedBonus475Return), 0), ' ', 0);
    retVal.illustrations.totProjectedPayout = getCurrency(trunc(Number(illustrations.totProjectedPayout), 0), ' ', 0);
    retVal.illustrations.totalPayoutRatio = illustrations.totalPayoutRatio;
    retVal.illustrations.gteedInternalRateOfReturn = illustrations.gteedInternalRateOfReturn;
    retVal.illustrations.gteedInternalRateOf475ProjectReturn = illustrations.gteedInternalRateOf475ProjectReturn;
    retVal.illustrations.totProjectedCashPayouts3Pa = getCurrency(trunc(Number(illustrations.totProjectedCashPayouts3Pa), 0), ' ', 0);
    retVal.illustrations.nonGteedBonus475Return = getCurrency(trunc(Number(illustrations.nonGteedBonus475Return), 0), ' ', 0);
    retVal.illustrations.totProjectedPayout2 = getCurrency(trunc(Number(illustrations.totProjectedPayout2), 0), ' ', 0);
    retVal.illustrations.totalPayoutRatio2 = illustrations.totalPayoutRatio2;
    retVal.illustrations.totalInternalRateofReturn = illustrations.totalInternalRateofReturn;
    retVal.illustrations.totgteedCashPayoutsMinus2.year = illustrations.totgteedCashPayoutsMinus2.year;
    retVal.illustrations.totgteedCashPayoutsMinus1.year = illustrations.totgteedCashPayoutsMinus1.year;
    retVal.illustrations.totgteedCashPayoutsEnd.year = illustrations.totgteedCashPayoutsEnd.year;
    var illustrateData_DB = [];
    var illustrateData_SV_b4_mature = [];
    var illustrateData_SV_at_mature = [];
    var illustrateData_deduct = [];
    var illustrateData_totalDistribCost = [];
    var dataArr = [];
    dataArr = illustrations.mainBIDB ? illustrations.mainBIDB : [];
    if (dataArr instanceof Array) {
      var row = {};
      var polTerm = dataArr.length;
      dataArr.forEach(function(illustration, i) {
        var polYear = illustration.polYear;
        var age = illustration.age;
        var polYr_age = polYear + '/' + age;
        if (polYear <= 10 || polYear <= 40 && polYear % 5 === 0 || polYear === polTerm) {
          row = {
            polYr_age: polYr_age,
            totalPremPaid: getCurrency(trunc(Number(illustration.totPremiumPaid), 0), ' ', 0),
            GteedDB: getCurrency(trunc(Number(illustration.guaranteed), 0), ' ', 0),
            nonGteedDB_low: getCurrency(trunc(Number(illustration.nongteed325), 0), ' ', 0),
            totalDB_low: getCurrency(trunc(Number(illustration.toal325), 0), ' ', 0),
            nonGteedDB_high: getCurrency(trunc(Number(illustration.nongteed475), 0), ' ', 0),
            totalDB_high: getCurrency(trunc(Number(illustration.total475), 0), ' ', 0)
          };
          illustrateData_DB.push(row);
        }
      });
    }
    retVal.illustrateData_DB = illustrateData_DB;
    dataArr = illustrations.mainBISV ? illustrations.mainBISV : [];
    if (dataArr instanceof Array) {
      var row = {};
      var polTerm = dataArr.length;
      dataArr.forEach(function(illustration, i) {
        var polYear = illustration.polYear;
        var age = illustration.age;
        var polYr_age = polYear + '/' + age;
        if (polYear <= 10 || polYear <= 40 && polYear % 5 === 0 || polYear === polTerm) {
          row = {
            polYr_age: polYr_age,
            totalPremPaid: getCurrency(trunc(Number(illustration.totPremiumPaid), 0), ' ', 0),
            GteedSV: getCurrency(trunc(Number(illustration.guaranteed), 0), ' ', 0),
            nonGteedSV_low: getCurrency(trunc(Number(illustration.nongteed325), 0), ' ', 0),
            totalSV_low: getCurrency(trunc(Number(illustration.toal325), 0), ' ', 0),
            nonGteedSV_high: getCurrency(trunc(Number(illustration.nongteed475), 0), ' ', 0),
            totalSV_high: getCurrency(trunc(Number(illustration.total475), 0), ' ', 0)
          };
          if (polYear < polTerm) {
            illustrateData_SV_b4_mature.push(row);
          } else {
            illustrateData_SV_at_mature.push(row);
          }
        }
      });
    }
    retVal.illustrateData_SV_b4_mature = illustrateData_SV_b4_mature;
    retVal.illustrateData_SV_at_mature = illustrateData_SV_at_mature;
    dataArr = illustrations.tableofDeductions ? illustrations.tableofDeductions : [];
    if (dataArr instanceof Array) {
      var row = {};
      var polTerm = dataArr.length;
      dataArr.forEach(function(illustration, i) {
        var polYear = illustration.polYear;
        var age = illustration.age;
        var polYr_age = polYear + '/' + age;
        if (polYear <= 10 || polYear <= 40 && polYear % 5 === 0 || polYear === polTerm) {
          row = {
            polYr_age: polYr_age,
            totalPremPaid: getCurrency(trunc(Number(illustration.totPremiumPaid), 0), ' ', 0),
            effectOfDeduct_high: getCurrency(trunc(Number(illustration.effectofDeduction475), 0), ' ', 0),
            VOP_high: getCurrency(trunc(Number(illustration.premiumPaid475), 0), ' ', 0),
            totalSV_high: getCurrency(trunc(Number(illustration.totalSurrenderValue475), 0), ' ', 0),
            effectOfDeduct_low: getCurrency(trunc(Number(illustration.effectofDeduction325), 0), ' ', 0),
            VOP_low: getCurrency(trunc(Number(illustration.premiumPaid325), 0), ' ', 0),
            totalSV_low: getCurrency(trunc(Number(illustration.totalSurrenderValue325), 0), ' ', 0),
          };
          illustrateData_deduct.push(row);
        }
      });
    }
    retVal.illustrateData_deduct = illustrateData_deduct;
    dataArr = illustrations.tdc ? illustrations.tdc : [];
    if (dataArr instanceof Array) {
      var row = {};
      var polTerm = dataArr.length;
      dataArr.forEach(function(illustration, i) {
        var polYear = illustration.polYear;
        var age = illustration.age;
        var polYr_age = polYear + '/' + age;
        if (polYear <= 10 || polYear <= 40 && polYear % 5 === 0 || polYear === polTerm) {
          row = {
            polYr_age: polYr_age,
            totalPremPaid: getCurrency(trunc(Number(illustration.totPremiumPaid), 0), ' ', 0),
            totalDistribCost: getCurrency(trunc(Number(illustration.totDistributionProjected), 0), ' ', 0),
          };
          illustrateData_totalDistribCost.push(row);
        }
      });
    }
    retVal.illustrateData_totalDistribCost = illustrateData_totalDistribCost;
  } catch (err) {} finally {
    retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
    retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
    return retVal;
  }
}