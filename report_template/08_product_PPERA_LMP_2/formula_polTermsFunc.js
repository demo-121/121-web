function(planDetail, quotation) {
  var policyTermList = [];
  var ppt_yr = 0;
  if (quotation.plans[0].premTerm.indexOf('YR') == -1) {
    ppt_yr = 65 - quotation.pAge;
  } else {
    ppt_yr = Number.parseInt(quotation.plans[0].premTerm)
  }
  var setSame = quotation.pAge + ppt_yr < 65;
  var hasTa65 = false;
  _.each([15, 20, 25], (val) => {
    if (quotation.pAge + val <= 80) {
      policyTermList.push({
        value: '' + val,
        title: val + ' years',
        default: setSame && val == Number.parseInt(quotation.plans[0].premTerm)
      });
      if (quotation.pAge + val === 65) {
        hasTa65 = true;
      }
    }
  });
  if (quotation.pAge + ppt_yr >= 65) {
    policyTermList.push({
      value: '65',
      title: 'To Age 65',
      default: true
    });
  } else {
    if (quotation.pAge < 65 && !hasTa65) {
      policyTermList.push({
        value: '65',
        title: 'To Age 65'
      });
    }
  }
  return policyTermList;
}