function(planDetail, quotation, planDetails) {
  planDetail.inputConfig.canEditPaymentMethod = false;
  planDetail.inputConfig.paymentMethodList = [{
    value: 'CASH',
    title: {
      en: 'Cash'
    }
  }];
  planDetail.inputConfig.canEditPayFreq = false;
  planDetail.inputConfig.payFreqList = [{
    value: 'A',
    title: {
      en: 'Annual'
    }
  }, {
    value: 'M',
    title: {
      en: 'Monthly'
    }
  }];
  planDetail.inputConfig.canEditPremium = false;
}