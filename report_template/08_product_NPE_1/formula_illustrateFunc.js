function(quotation, planInfo, planDetails, extraPara) {
  var trunc = function(value, position) {
    return runFunc(planDetails[planInfo.covCode].formulas.trunc, value, position);
  };
  var illustrations = {};
  illustrations.projRate = {};
  illustrations.reductInYield = {};
  var basicPlanCode = planInfo.covCode;
  var planDetail = planDetails[basicPlanCode];
  var Premium_Lookup = math.bignumber(runFunc(planDetail.formulas.getPremium, quotation, planInfo, planDetails[planInfo.covCode]));
  var projections = planDetail.projection;
  var retVal = [];
  var lowId = 999;
  var highId = -1;
  for (var i = 0; i < 2; i++) {
    var projection = projections[i];
    var id = projection.title.en;
    lowId = Math.min(Number(id), lowId);
    highId = Math.max(Number(id), highId);
    var netCashFlow = [];
    var extraParamater = {
      'tbl_Bonus': projection.rates,
      'netCashFlow': netCashFlow,
      'Premium_Lookup': Premium_Lookup
    };
    illustrations[id] = runFunc(planDetail.formulas.getIllustration, quotation, planInfo, planDetails, extraParamater);
    netCashFlow.push(illustrations[id][planInfo.premTerm - 1].totalSV);
    illustrations.reductInYield[id] = runFunc(planDetail.formulas.IRR, netCashFlow);
    illustrations.projRate[id] = projection.rates.ProjectInvestReturn[0];
  }
  for (var i = 0; i < illustrations[highId].length; i++) {
    var illust_low = illustrations[lowId][i];
    var illust_high = illustrations[highId][i];
    var GteedDB_SUPP = math.number(math.add(math.bignumber(illust_low.GteedDB), math.bignumber(illust_high.totalSBsupp)));
    var GteedSV_SUPP = math.number(math.add(math.bignumber(illust_low.GteedSV), math.bignumber(illust_high.totalSBsupp)));
    var row = {
      age: illust_high.age,
      totalPremPaid: illust_high.totalPremPaid,
      GteedDB: illust_high.GteedDB,
      GteedDB_SUPP: GteedDB_SUPP,
      GteedAnnualCashback: illust_high.GteedAnnualCashback,
      GteedSV: illust_low.GteedSV,
      GteedSV_SUPP: GteedSV_SUPP,
      totalDistribCost: illust_high.totalDistribCost,
      nonGteedDB: {},
      totalDB: {},
      nonGteedDB_SUPP: {},
      totalDB_SUPP: {},
      nonGteedSV: {},
      totalSV: {},
      nonGteedSV_SUPP: {},
      totalSV_SUPP: {},
      effectOfDeduct: {},
      VOP: {},
      reductYield: {},
      projRate: {},
    };
    for (var j = 0; j < 2; j++) {
      var rateId = (j === 0) ? lowId : highId;
      var ill = illustrations[rateId][i];
      var totalDB_SUPP = math.number(math.add(math.bignumber(trunc(ill.nonGteedDB_SUPP, 0)), math.bignumber(trunc(GteedDB_SUPP, 0))));
      var totalSV_SUPP = math.number(math.add(math.bignumber(trunc(ill.nonGteedSV_SUPP, 0)), math.bignumber(trunc(GteedSV_SUPP, 0))));
      row.nonGteedDB[rateId] = ill.nonGteedDB;
      row.totalDB[rateId] = ill.totalDB;
      row.nonGteedDB_SUPP[rateId] = ill.nonGteedDB_SUPP;
      row.totalDB_SUPP[rateId] = totalDB_SUPP;
      row.nonGteedSV[rateId] = ill.nonGteedSV;
      row.totalSV[rateId] = ill.totalSV;
      row.nonGteedSV_SUPP[rateId] = ill.nonGteedSV_SUPP;
      row.totalSV_SUPP[rateId] = totalSV_SUPP;
      row.effectOfDeduct[rateId] = ill.effectOfDeduct;
      row.VOP[rateId] = ill.VOP;
      row.reductYield[rateId] = illustrations.reductInYield[rateId];
      row.projRate[rateId] = illustrations.projRate[rateId];
    }
    retVal.push(row);
  }
  return retVal;
}