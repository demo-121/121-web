function(quotation, planInfo, planDetails) {
  var rateKey, lookupYr;
  if (planInfo.policyTerm === planInfo.premTerm) {
    rateKey = 'RP_';
    lookupYr = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
  } else {
    rateKey = 'LP_';
    if (planInfo.premTerm === 'SP') {
      lookupYr = 1;
    } else if (planInfo.premTerm.endsWith('_YR')) {
      lookupYr = Number.parseInt(planInfo.premTerm);
    } else {
      lookupYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
  }
  switch (quotation.dealerGroup) {
    case 'SYNERGY':
      rateKey += 'AGENCY';
      break;
    case 'FUSION':
      rateKey += 'BROKER';
      break;
    default:
      rateKey += quotation.dealerGroup;
  }
  var rateTable = planDetails[quotation.baseProductCode].rates.riderCommRates[rateKey];
  var commRates = [];
  if (rateTable) {
    var maxTerm = 0;
    _.each(rateTable, (rates, key) => {
      var rateTerm = Number(key);
      if (lookupYr >= rateTerm && rateTerm > maxTerm) {
        commRates = [].concat(rates);
        maxTerm = rateTerm;
      }
    });
  }
  if (commRates[commRates.length - 1]) {
    for (var i = commRates.length; i < 99; i++) {
      commRates.push(commRates[i - 1]);
    }
  }
  return commRates;
}