function(quotation, planInfo, planDetails, extraPara) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var planDetail = planDetails[planInfo.covCode];
  var planType = quotation.policyOptions.planType;
  var indexation = quotation.policyOptions.indexation === 'Y';
  var polYrs = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
  var premYrs;
  if (planInfo.premTerm === 'SP') {
    premYrs = 1;
  } else if (planInfo.premTerm.endsWith('_YR')) {
    premYrs = Number.parseInt(planInfo.premTerm);
  } else {
    premYrs = Number.parseInt(planInfo.premTerm) - quotation.iAge;
  }
  var sumInsured = planInfo.sumInsured;
  var illYrs = polYrs;
  var yearPrems = [0];
  var indexPrems = [];
  for (var i = 1; i <= illYrs; i++) {
    var age = quotation.iAge + i - 1;
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, age) || 0);
    var yearPrem;
    var indexPrem = math.bignumber(0);
    if (i === 1) {
      yearPrem = math.bignumber(planInfo.yearPrem);
    } else {
      yearPrem = yearPrems[i - 1];
      if (planInfo.policyTerm !== planInfo.premTerm && i > premYrs) {
        yearPrem = math.bignumber(0);
      } else {
        if (indexation) {
          var bpIlls = extraPara.illustrations[quotation.baseProductCode];
          var dbDiff = i === 1 ? math.bignumber(0) : math.subtract(math.bignumber(bpIlls[i - 1].guaranteedDB), math.bignumber(bpIlls[i - 2].guaranteedDB));
          indexPrem = math.multiply(premRate, dbDiff, 0.001);
          yearPrem = math.add(yearPrem, indexPrem);
        }
      }
    }
    yearPrems.push(yearPrem);
    indexPrems.push(indexPrem);
  }
  var commRates = runFunc(planDetail.formulas.getCommRates, quotation, planInfo, planDetails);
  var totComms = [0];
  var premIncSum = [0];
  for (var i = 1; i <= illYrs; i++) {
    var premInc = math.subtract(yearPrems[i], yearPrems[i - 1]);
    premIncSum.push(math.add(premInc, premIncSum[i - 1]));
    if (math.number(premInc)) {
      for (var j = 0; j < commRates.length; j++) {
        if (commRates[j]) {
          var comm = math.multiply(indexation ? premInc : premIncSum[i], commRates[j]);
          if (!totComms[i + j]) {
            totComms[i + j] = math.bignumber(0);
          }
          if (math.number(comm) > 0) {
            totComms[i + j] = math.add(totComms[i + j], comm);
          }
        }
      }
    }
  }
  var illustration = [];
  var illLength = Math.max(totComms.length, yearPrems.length) - 1;
  for (var i = 0; i < illLength; i++) {
    illustration.push({
      yearPrem: yearPrems[i + 1] ? math.number(yearPrems[i + 1]) : 0,
      totComm: totComms[i + 1] ? math.number(totComms[i + 1]) : 0,
      indexPrem: indexPrems[i] ? math.number(indexPrems[i]) : 0
    });
  }
  return illustration;
}