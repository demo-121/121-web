function(quotation, planInfo, planDetail) {
  console.log('\n\nCritical Illness Premium eraser ');
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var premium = null;
  var sumInsured = planInfo.sumInsured;
  if (planInfo.premTerm && sumInsured) {
    var residenceLoading = math.bignumber(runFunc(planDetail.formulas.getResidenceLoading, quotation, planInfo, planDetail));
    console.log('sumInsured = ' + sumInsured);
    var rates = planDetail.rates;
    var countryCode = quotation.iResidence;
    var countryGroup = rates.countryGroup[countryCode];
    if (Array.isArray(countryGroup)) {
      countryGroup = countryGroup[0];
    }
    var substandardClass = rates.substandardClass[countryGroup]['WP'];
    if (Array.isArray(substandardClass)) {
      substandardClass = substandardClass[0];
    }
    console.log('substandardClass = ' + substandardClass);
    var loadingFactor = math.bignumber(rates.WPRLoadingFactor[substandardClass]);
    console.log('loadingFactor = ' + math.number(loadingFactor));
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
    console.log('premRate = ' + math.number(premRate));
    var isCountryMatch = (countryGroup === "Major City of China" || countryGroup === "Other City of China" || countryGroup === "Indonesia" || countryGroup === "Thailand");
    if (isCountryMatch) {
      console.log('isCountryMatch');
      residenceLoading = math.round(math.multiply(residenceLoading, loadingFactor, 1), 2);
      console.log('residenceLoading = ' + math.number(residenceLoading));
      var premAfterLsd = math.round(math.multiply(residenceLoading, math.bignumber(sumInsured)), 2);
      console.log('premAfterLsd = ' + math.number(premAfterLsd));
      premAfterLsd = roundDown(math.multiply(premAfterLsd, 0.001), 2);
      console.log('premAfterLsd = ' + math.number(premAfterLsd));
      var modalFactor = 1;
      for (var p in planDetail.payModes) {
        if (planDetail.payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetail.payModes[p].factor;
        }
      }
      var prem = roundDown(math.multiply(premAfterLsd, modalFactor), 2);
      premium = math.number(prem);
    } else {
      console.log('isCountryMatch = false');
      var premAfterLsd = roundDown(math.multiply(premRate, math.bignumber(sumInsured), 0.01), 2);
      var modalFactor = 1;
      for (var p in planDetail.payModes) {
        if (planDetail.payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetail.payModes[p].factor;
        }
      }
      var prem = roundDown(math.multiply(premAfterLsd, modalFactor), 2);
      premium = math.number(prem);
    }
  }
  console.log('premium = ' + premium);
  return premium;
}