function(quotation, planInfo, planDetails) {
  var planDetail = planDetails[planInfo.covCode];
  var premInput = planDetail.inputConfig.premInput;
  var closestPrem = null;
  var annPrem = quotCalc.getAnnPrem(quotation, planInfo, planDetail);
  var decimal = 0;
  var factor = 0;
  if (Number(planInfo.premium)) {
    closestPrem = planInfo.premium;
    if (premInput && premInput.decimal) {
      var scale = math.pow(10, premInput.decimal);
      closestPrem = Number(math.divide(math.floor(math.multiply(Number(closestPrem), scale)), scale));
      decimal = premInput.decimal;
    }
    if (premInput && premInput.factor) {
      closestPrem = Number(math.multiply(math.ceil(math.divide(Number(closestPrem), premInput.factor)), premInput.factor));
      factor = premInput.factor;
    }
  }
  if (premInput && premInput.length > 0) { /*set decimal & factor based on quotation.ccy*/
    for (var i in premInput) {
      if (premInput[i].ccy === quotation.ccy) {
        decimal = premInput[i].decimal;
        factor = premInput[i].factor;
        break;
      }
    }
  } /*calculate total year prem, half year prem, quarter Prem, month Prem for all basic paln and rider*/
  planDetail.payModes.forEach(function(payMode) {
    var prem = payMode.operator == "D" ? math.divide(Number(annPrem), payMode.factor) : math.multiply(Number(annPrem), payMode.factor);
    if (decimal) {
      var scale = math.pow(10, decimal);
      prem = math.divide(math.floor(math.multiply(prem, scale)), scale);
    }
    prem = math.number(prem);
    if (payMode.mode == 'L' || payMode.mode == 'A') {
      planInfo.yearPrem = prem;
      quotation.totYearPrem = Number(math.add(Number(quotation.totYearPrem), prem));
    } else if (payMode.mode == 'S') {
      planInfo.halfYearPrem = prem;
      quotation.totHalfyearPrem = Number(math.add(Number(quotation.totHalfyearPrem), prem));
    } else if (payMode.mode == 'Q') {
      planInfo.quarterPrem = prem;
      quotation.totQuarterPrem = Number(math.add(Number(quotation.totQuarterPrem), prem));
    } else if (payMode.mode == 'M') {
      planInfo.monthPrem = prem;
      quotation.totMonthPrem = Number(math.add(Number(quotation.totMonthPrem), prem));
    }
  });
  quotation.premium = Number(math.add(Number(quotation.premium), closestPrem));
}