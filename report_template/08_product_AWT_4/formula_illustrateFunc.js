function(quotation, planInfo, planDetails, extraPara) {
  var planDetail = planDetails[planInfo.covCode];
  var rates = planDetail.rates;
  var annPrem = quotCalc.getAnnPrem(quotation, planInfo, planDetail);
  var premTerm = Number(planInfo.premTerm);
  var dealerGroup = quotation.agent.dealerGroup;
  var round = function(value, position) {
    var num = Number(value);
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(num, scale)), scale);
  }; /*Column A*/
  var calcYear = function(month) {
    return math.floor((month - 1) / 12) + 1;
  }; /*UColumn D*/
  var calcRegularBasicPremium = function(month) {
    if (month > premTerm * 12) {
      return math.bignumber(0);
    } else if (quotation.paymentMode === 'M') {
      return math.bignumber(planInfo.premium);
    } else if (quotation.paymentMode === 'Q') {
      return math.bignumber(month % 3 === 1 ? planInfo.premium : 0);
    } else if (quotation.paymentMode === 'S') {
      return math.bignumber(month % 6 === 1 ? planInfo.premium : 0);
    } else if (quotation.paymentMode === 'A') {
      return math.bignumber(month % 12 === 1 ? planInfo.premium : 0);
    } else {
      return math.bignumber(0);
    }
  }; /*Column E*/
  var calcStartupBonus = function(month) {
    var startupBonus = 0;
    var startupBonusRate = 0;
    var refTable = {};
    var refIndex = -1;
    if (quotation.ccy === 'SGD') {
      refTable = (annPrem < 12000) ? rates.startupBonus.sgd_pb0 : rates.startupBonus.sgd_pb12000;
    } else {
      refTable = (annPrem < 8400) ? rates.startupBonus.usd_pb0 : rates.startupBonus.usd_pb8400;
    }
    refIndex = refTable.premTerm.indexOf(premTerm);
    startupBonusRate = refTable.rate[refIndex];
    var rstStartupBonus = math.bignumber(0);
    if (calcYear(month) === 1) {
      rstStartupBonus = math.multiply(calcRegularBasicPremium(month), math.bignumber(startupBonusRate));
    }
    return rstStartupBonus;
  }; /*Column F_SC Contibution to Reg Prem Account*/
  var calcContributionToRegularPremAcc = function(month) {
    return math.add(calcRegularBasicPremium(month), calcStartupBonus(month));
  }; /*Column G*/
  var calcSpTopup = function(month) { /*always monthly*/
    return math.bignumber(quotation.policyOptions.topUpSelect && month === 1 ? quotation.policyOptions.topUpAmt : 0);
  }; /*Column H*/
  var calcRspTopup = function(month) { /*always monthly*/
    return math.bignumber(quotation.policyOptions.rspSelect ? quotation.policyOptions.rspAmount : 0);
  };
  var calcInsuranceCharge = function(age, NAR) {
    if (quotation.policyOptions.deathBenefit === 'basic') {
      return math.bignumber(0);
    } else {
      var mortTable = rates.mortalityTable;
      var mortIndex = mortTable.age.indexOf(age);
      var mortalityCode = quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS');
      var coiRate = mortTable[mortalityCode][mortIndex];
      return math.divide(math.multiply(math.bignumber(coiRate), NAR), math.bignumber(12));
    }
  }; /*Total Death Denefit incl Non-Guaranteed - Column AE*/
  var calcTotalDeathBenefit = function(year, totalAccountValue, accumulatedStartupBonus, totalModalPremium, totalWithdrawalAmount) {
    var dbRate = planDetail.rates.deathBenefit.rate;
    if (quotation.policyOptions.deathBenefit === 'basic') {
      if (year === 1) {
        return math.multiply(dbRate, math.subtract(totalAccountValue, accumulatedStartupBonus));
      } else {
        return math.multiply(dbRate, totalAccountValue);
      }
    } else {
      if (year === 1) {
        return math.max(math.subtract(totalModalPremium, totalWithdrawalAmount), math.multiply(dbRate, math.subtract(totalAccountValue, accumulatedStartupBonus)));
      } else {
        return math.max(math.subtract(totalModalPremium, totalWithdrawalAmount), math.multiply(dbRate, totalAccountValue));
      }
    }
  };
  var calcSumOfWithdrawalFromLastNumOfMonth = function(rawData, currMonth, numOfMonth) { /*sum of Withdrawal from (currMonth - numOfMonth + 1) to currMonth*/
    var sumWithdrawalAmount = math.bignumber(0);
    if (currMonth > numOfMonth) {
      for (var i = 0; i < numOfMonth; i++) {
        var data = rawData[currMonth - i];
        var newSumWithdrawalAmount = math.add(sumWithdrawalAmount, data.wuACcWithdrawal);
        sumWithdrawalAmount = newSumWithdrawalAmount;
      }
    }
    return sumWithdrawalAmount;
  };
  var convertBignumberToReadable = function(data) { /* for (var row in data) { for (var col in data[row]) { data[row][col] = math.number(data[row][col]); } } */ };
  var calcIllustration = function(month, arorkey, illustration) {
    var sustainTestResult = true;
    var arorRate = rates.aror[arorkey];
    var rawData = [];
    var defaultRawData = { /*Common*/
      commission: math.bignumber(0),
      cAcYear: 0,
      cBcMonth: 0,
      cCcAge: 0,
      cDcRegularPremium: math.bignumber(0),
      cEcStartupBonus: math.bignumber(0),
      cGcSpTopup: math.bignumber(0),
      cHcRspTopup: math.bignumber(0),
      cFcContributionToRegPremAcc: math.bignumber(0),
      cIcContributionToTopup: math.bignumber(0),
      /*TUC_PC*/ cAOcAccumulatedStartupBonus: math.bignumber(0),
      /*STUB_ACC*/ cAMcTotalModalPremium: math.bignumber(0),
      /*TP*/ /*Unit no withdrawal*/ uMcInsuranceCharge: math.bignumber(0),
      /*COI*/ uNcAMF: math.bignumber(0),
      uOcIMF: math.bignumber(0),
      uLcNetAmountAtRisk: math.bignumber(0),
      /*NAR*/ uWcLoyaltyBonus: math.bignumber(0),
      uRcUnitFundGrowthRegAcc: math.bignumber(0),
      /*FV_GTH_A*/ uScUnitFundGrowthTopup: math.bignumber(0),
      /*FV_GTH_B*/ uUcFundMgmtChargeRegAcc: math.bignumber(0),
      /*FHC_A*/ uVcFundMgmtChargeTopup: math.bignumber(0),
      /*FHC_B*/ uJcRegularPremAccBOM: math.bignumber(0),
      /*FV_MAIN_BOM_A*/ uXcRegularPremAccEOM: math.bignumber(0),
      /*FV_MAIN_EOM_A*/ uKcTopupPremAccBOM: math.bignumber(0),
      /*FV_MAIN_BOM_B*/ uYcTopupPremAccEOM: math.bignumber(0),
      /*FV_MAIN_EOM_B*/ uZcTotalAccountValue: math.bignumber(0),
      /*FV_UNI*/ uACcNonGuaranteedDeathBenefit: math.bignumber(0),
      uADcGuaranteedDeathBenefit: math.bignumber(0),
      /*GDB*/ uAEcTotalDeathBenefit: math.bignumber(0),
      /*TDB*/ uAAcSurrPanalty: math.bignumber(0),
      /*EEC*/ uABcSurrValue: math.bignumber(0),
      /*SV*/ uAJcAccumulatedPremPaid: math.bignumber(0),
      /*TAP*/ uAKcEffectOfDeduction: math.bignumber(0),
      /*EOD*/ uAFcTrailerComm: math.bignumber(0),
      /*TRA_COMM*/ uAGcYearlyBasicCommOverrideAllow: math.bignumber(0),
      uAHcCommissionTopupRSP: math.bignumber(0),
      uAIcAccumulatedTotalDistCost: math.bignumber(0),
      /*For Withdrawal*/ wuAnnualWithdrawalAmount: math.bignumber(0),
      wuTotalWithdrawalAmount: math.bignumber(0),
      wuLcNetAmountAtRisk: math.bignumber(0),
      wuMcInsuranceCharge: math.bignumber(0),
      wuNcAMF: math.bignumber(0),
      wuOcIMF: math.bignumber(0),
      wuWcLoyaltyBonus: math.bignumber(0),
      wuRcUnitFundGrowthRegAcc: math.bignumber(0),
      wuScUnitFundGrowthTopup: math.bignumber(0),
      wuUcFundMgmtChargeRegAcc: math.bignumber(0),
      wuVcFundMgmtChargeTopup: math.bignumber(0),
      wuZcTotalAccountValue: math.bignumber(0),
      wuACcWithdrawal: math.bignumber(0),
      /*FUND_WD */ wuJcRegularPremAccBOM: math.bignumber(0),
      wuXcRegularPremAccEOM: math.bignumber(0),
      wuKcTopupPremAccBOM: math.bignumber(0),
      wuYcTopupPremAccEOM: math.bignumber(0),
      wuAEcTotalDeathBenefit: math.bignumber(0),
      wuAAcSurrPanalty: math.bignumber(0),
      wuABcSurrValue: math.bignumber(0),
      rYieldAnnuallsedPremium: math.bignumber(0),
      rYieldSurrenderValue: math.bignumber(0),
      rYieldCalculation: math.bignumber(0),
      rYieldReducedReturnRate: math.bignumber(0),
      rYieldTotalEffectDeduction: math.bignumber(0)
    };
    rawData.push(Object.assign({}, defaultRawData));
    var feesAndCharges = rates.feesAndCharges;
    var amfIndex = feesAndCharges.premTerm.indexOf(premTerm);
    var amfRate = math.bignumber(feesAndCharges.amf[amfIndex]);
    var imfRate = math.bignumber(feesAndCharges.imf[0]);
    var fmcRate = math.bignumber(feesAndCharges.fmc[0]);
    var trailerComm = math.bignumber(rates.trailerComm[dealerGroup]);
    var GST = math.bignumber(rates.ungroupedRate.GST);
    var SPTDCLoad = math.bignumber(rates.ungroupedRate.SPTDCLoad);
    var agencySPOverrides = math.bignumber(rates.commissionTable.agencySPOverrides);
    var commTopupRSP = math.bignumber(rates.commissionTopupRSP[dealerGroup]);
    commTopupRSP = math.multiply(commTopupRSP, ['FUSION', 'BROKER'].indexOf(dealerGroup) >= 0 ? math.add(GST, math.bignumber(1)) : (['AGENCY', 'SYNERGY'].indexOf(dealerGroup) >= 0 ? math.add(agencySPOverrides, math.bignumber(1)) : math.bignumber(1)));
    commTopupRSP = math.multiply(commTopupRSP, math.add(SPTDCLoad, math.bignumber(1)));
    var vPowerArorRate = math.pow(math.add(math.bignumber(arorRate), math.bignumber(1)), math.divide(math.bignumber(1), math.bignumber(12)));
    var vPowerArorRateSubOne = math.subtract(vPowerArorRate, math.bignumber(1));
    var rtEarlyEncashmentCharge = rates.earlyEncashmentCharge;
    var rtCommission = rates.commission[dealerGroup];
    var reduceYieldTargetYear = math.max(20, 65 - quotation.iAge);
    var yieldCalculation = [];
    for (var m = 1; m <= month; m++) {
      var prevMonth = rawData[m - 1];
      var currMonth = Object.assign({}, defaultRawData);
      rawData.push(currMonth);
      currMonth.cAcYear = calcYear(m);
      currMonth.cBcMonth = m;
      currMonth.cCcAge = quotation.iAge + currMonth.cAcYear - 1;
      currMonth.cDcRegularPremium = calcRegularBasicPremium(m);
      currMonth.cEcStartupBonus = calcStartupBonus(m);
      currMonth.cGcSpTopup = calcSpTopup(m);
      currMonth.cHcRspTopup = calcRspTopup(m);
      var vMonthlyPremiumIncome = math.add(math.add(currMonth.cDcRegularPremium, currMonth.cGcSpTopup), currMonth.cHcRspTopup); /*MP*/
      var vPrevTotalModalPremium = prevMonth.cAMcTotalModalPremium;
      currMonth.cAMcTotalModalPremium = math.add(prevMonth.cAMcTotalModalPremium, vMonthlyPremiumIncome);
      currMonth.cFcContributionToRegPremAcc = math.add(currMonth.cDcRegularPremium, currMonth.cEcStartupBonus);
      currMonth.cIcContributionToTopup = math.add(math.multiply(currMonth.cGcSpTopup, math.subtract(1, rates.salesChargeTopup.topup)), math.multiply(currMonth.cHcRspTopup, math.subtract(1, rates.salesChargeTopup.rsp))); /*Unit 4% or 8%*/
      currMonth.uLcNetAmountAtRisk = math.max(0, math.subtract(vPrevTotalModalPremium, prevMonth.uZcTotalAccountValue));
      currMonth.uMcInsuranceCharge = calcInsuranceCharge(currMonth.cCcAge, currMonth.uLcNetAmountAtRisk);
      currMonth.uJcRegularPremAccBOM = math.add(prevMonth.uXcRegularPremAccEOM, currMonth.cFcContributionToRegPremAcc);
      currMonth.uKcTopupPremAccBOM = math.add(prevMonth.uYcTopupPremAccEOM, currMonth.cIcContributionToTopup);
      currMonth.uNcAMF = m <= premTerm * 12 ? math.divide(math.multiply(currMonth.uJcRegularPremAccBOM, amfRate), math.bignumber(12)) : 0;
      currMonth.uOcIMF = math.divide(math.multiply(currMonth.uJcRegularPremAccBOM, imfRate), math.bignumber(12));
      var vKsubM = math.subtract(currMonth.uKcTopupPremAccBOM, currMonth.uMcInsuranceCharge);
      var minKsubM = math.min(vKsubM, math.bignumber(0));
      var vJaddKsubMsubNsubO = math.subtract(math.subtract(math.add(currMonth.uJcRegularPremAccBOM, minKsubM), currMonth.uNcAMF), currMonth.uOcIMF);
      currMonth.uWcLoyaltyBonus = currMonth.cAcYear >= rates.loyaltyBonus.startYear ? math.multiply(math.divide(rates.loyaltyBonus.rate, math.bignumber(12.0)), vJaddKsubMsubNsubO) : math.bignumber(0);
      var vJaddKsubMsubNsubOaddW = math.add(vJaddKsubMsubNsubO, currMonth.uWcLoyaltyBonus);
      currMonth.uRcUnitFundGrowthRegAcc = math.multiply(vJaddKsubMsubNsubOaddW, vPowerArorRateSubOne);
      currMonth.uUcFundMgmtChargeRegAcc = math.divide(math.multiply(math.add(vJaddKsubMsubNsubOaddW, currMonth.uRcUnitFundGrowthRegAcc), fmcRate), math.bignumber(12.0));
      currMonth.uXcRegularPremAccEOM = math.subtract(math.add(vJaddKsubMsubNsubOaddW, currMonth.uRcUnitFundGrowthRegAcc), currMonth.uUcFundMgmtChargeRegAcc);
      currMonth.uScUnitFundGrowthTopup = math.multiply(vKsubM, vPowerArorRateSubOne);
      currMonth.uVcFundMgmtChargeTopup = math.divide(math.multiply(math.add(vKsubM, currMonth.uScUnitFundGrowthTopup), fmcRate), math.bignumber(12.0));
      currMonth.uYcTopupPremAccEOM = math.max(math.subtract(math.add(vKsubM, currMonth.uScUnitFundGrowthTopup), currMonth.uVcFundMgmtChargeTopup), math.bignumber(0));
      currMonth.uZcTotalAccountValue = math.add(currMonth.uXcRegularPremAccEOM, currMonth.uYcTopupPremAccEOM);
      currMonth.cAOcAccumulatedStartupBonus = math.multiply(math.add(prevMonth.cAOcAccumulatedStartupBonus, currMonth.cEcStartupBonus), vPowerArorRate);
      currMonth.uAEcTotalDeathBenefit = calcTotalDeathBenefit(currMonth.cAcYear, currMonth.uZcTotalAccountValue, currMonth.cAOcAccumulatedStartupBonus, currMonth.cAMcTotalModalPremium, 0);
      currMonth.uADcGuaranteedDeathBenefit = quotation.policyOptions.deathBenefit === 'basic' ? math.bignumber(0) : currMonth.cAMcTotalModalPremium;
      currMonth.uACcNonGuaranteedDeathBenefit = math.max(math.subtract(currMonth.uAEcTotalDeathBenefit, currMonth.uADcGuaranteedDeathBenefit), math.bignumber(0));
      var eecIndex = rtEarlyEncashmentCharge.premTerm.indexOf(premTerm);
      var eecRate = currMonth.cBcMonth >= premTerm * 12 ? 0 : rtEarlyEncashmentCharge['polYear' + currMonth.cAcYear][eecIndex];
      eecRate = math.bignumber(eecRate);
      var commissionIndex = rtCommission ? rtCommission.year.indexOf(currMonth.cAcYear) : -1;
      var commission = rtCommission ? rtCommission['premTerm' + planInfo.premTerm][(commissionIndex < 0 ? 10 : commissionIndex)] : -1;
      commission = commission > 0 && currMonth.cAcYear <= premTerm ? commission : 0;
      commission = math.bignumber(commission);
      currMonth.commission = commission;
      currMonth.uAAcSurrPanalty = m >= premTerm * 12 ? math.bignumber(0) : math.multiply(eecRate, currMonth.uXcRegularPremAccEOM);
      currMonth.uABcSurrValue = math.max(math.subtract(currMonth.uZcTotalAccountValue, currMonth.uAAcSurrPanalty), math.bignumber(0));
      currMonth.uAJcAccumulatedPremPaid = math.multiply(math.add(prevMonth.uAJcAccumulatedPremPaid, vMonthlyPremiumIncome), vPowerArorRate);
      currMonth.uAKcEffectOfDeduction = math.subtract(currMonth.uAJcAccumulatedPremPaid, currMonth.uABcSurrValue);
      currMonth.commTopupRSP = commTopupRSP;
      currMonth.uAFcTrailerComm = m > 24 ? math.divide(math.multiply(trailerComm, math.max(math.add(currMonth.uJcRegularPremAccBOM, currMonth.uKcTopupPremAccBOM), math.bignumber(0))), 12) : math.bignumber(0);
      currMonth.uAGcYearlyBasicCommOverrideAllow = math.multiply(currMonth.cDcRegularPremium, commission);
      currMonth.uAHcCommissionTopupRSP = math.add(math.multiply(currMonth.cGcSpTopup, commTopupRSP), math.multiply(currMonth.cHcRspTopup, commTopupRSP));
      currMonth.uAIcAccumulatedTotalDistCost = math.add(math.add(math.add(prevMonth.uAIcAccumulatedTotalDistCost, currMonth.uAFcTrailerComm), currMonth.uAGcYearlyBasicCommOverrideAllow), currMonth.uAHcCommissionTopupRSP); /*Withdrawal Unit 4% or 8%*/
      if (quotation.policyOptions.wdSelect) {
        if (currMonth.cCcAge >= quotation.policyOptions.wdFromAge && currMonth.cCcAge <= quotation.policyOptions.wdToAge && m % 12 === 1) {
          var accValueNoWithdrawal = math.add(math.add(prevMonth.wuZcTotalAccountValue, currMonth.cFcContributionToRegPremAcc), currMonth.cIcContributionToTopup);
          var wdAmount = math.bignumber(quotation.policyOptions.wdAmount);
          currMonth.wuACcWithdrawal = math.largerEq(math.subtract(accValueNoWithdrawal, wdAmount), math.bignumber(rates.minAccValue[quotation.ccy])) ? wdAmount : accValueNoWithdrawal;
        }
      }
      currMonth.wuTotalWithdrawalAmount = math.add(prevMonth.wuTotalWithdrawalAmount, currMonth.wuACcWithdrawal);
      currMonth.wuAnnualWithdrawalAmount = m % 12 === 1 ? currMonth.wuACcWithdrawal : math.add(prevMonth.wuAnnualWithdrawalAmount, currMonth.wuACcWithdrawal);
      currMonth.wuLcNetAmountAtRisk = math.max(math.bignumber(0), math.subtract(math.subtract(vPrevTotalModalPremium, currMonth.wuTotalWithdrawalAmount), prevMonth.wuZcTotalAccountValue));
      currMonth.wuMcInsuranceCharge = calcInsuranceCharge(currMonth.cCcAge, currMonth.wuLcNetAmountAtRisk);
      var wvpYaddcI = math.add(prevMonth.wuYcTopupPremAccEOM, currMonth.cIcContributionToTopup);
      currMonth.wuJcRegularPremAccBOM = math.subtract(math.add(prevMonth.wuXcRegularPremAccEOM, currMonth.cFcContributionToRegPremAcc), math.subtract(currMonth.wuACcWithdrawal, math.min(currMonth.wuACcWithdrawal, wvpYaddcI)));
      currMonth.wuKcTopupPremAccBOM = math.subtract(math.add(prevMonth.wuYcTopupPremAccEOM, currMonth.cIcContributionToTopup), math.min(currMonth.wuACcWithdrawal, wvpYaddcI));
      currMonth.wuNcAMF = m <= premTerm * 12 ? math.divide(math.multiply(currMonth.wuJcRegularPremAccBOM, amfRate), math.bignumber(12)) : math.bignumber(0);
      currMonth.wuOcIMF = math.divide(math.multiply(currMonth.wuJcRegularPremAccBOM, imfRate), math.bignumber(12));
      var wvKsubM = math.subtract(currMonth.wuKcTopupPremAccBOM, currMonth.wuMcInsuranceCharge);
      var wvJaddKsubMsubNsubO = math.subtract(math.subtract(math.add(currMonth.wuJcRegularPremAccBOM, math.min(wvKsubM, math.bignumber(0))), currMonth.wuNcAMF), currMonth.wuOcIMF);
      currMonth.wuWcLoyaltyBonus = currMonth.cAcYear >= rates.loyaltyBonus.startYear && math.equal(calcSumOfWithdrawalFromLastNumOfMonth(rawData, m, 12), math.bignumber(0)) ? math.divide(math.multiply(math.bignumber(rates.loyaltyBonus.rate), wvJaddKsubMsubNsubO), math.bignumber(12.0)) : math.bignumber(0);
      var wvJaddKsubMsubNsubOaddW = math.add(wvJaddKsubMsubNsubO, currMonth.wuWcLoyaltyBonus);
      currMonth.wuRcUnitFundGrowthRegAcc = math.multiply(wvJaddKsubMsubNsubOaddW, vPowerArorRateSubOne);
      currMonth.wuUcFundMgmtChargeRegAcc = math.divide(math.multiply(math.add(wvJaddKsubMsubNsubOaddW, currMonth.wuRcUnitFundGrowthRegAcc), fmcRate), 12);
      currMonth.wuXcRegularPremAccEOM = math.max(math.bignumber(0), math.subtract(math.add(wvJaddKsubMsubNsubOaddW, currMonth.wuRcUnitFundGrowthRegAcc), currMonth.wuUcFundMgmtChargeRegAcc));
      currMonth.wuScUnitFundGrowthTopup = math.multiply(wvKsubM, vPowerArorRateSubOne);
      currMonth.wuVcFundMgmtChargeTopup = math.divide(math.multiply(math.add(wvKsubM, currMonth.wuScUnitFundGrowthTopup), fmcRate), 12);
      currMonth.wuYcTopupPremAccEOM = math.max(math.subtract(math.add(wvKsubM, currMonth.wuScUnitFundGrowthTopup), currMonth.wuVcFundMgmtChargeTopup), math.bignumber(0));
      currMonth.wuZcTotalAccountValue = math.add(currMonth.wuXcRegularPremAccEOM, currMonth.wuYcTopupPremAccEOM);
      currMonth.wuAEcTotalDeathBenefit = calcTotalDeathBenefit(currMonth.cAcYear, currMonth.wuZcTotalAccountValue, currMonth.cAOcAccumulatedStartupBonus, currMonth.cAMcTotalModalPremium, currMonth.wuTotalWithdrawalAmount);
      currMonth.wuAAcSurrPanalty = m >= premTerm * 12 ? math.bignumber(0) : math.multiply(eecRate, currMonth.wuXcRegularPremAccEOM);
      currMonth.wuABcSurrValue = math.max(math.subtract(currMonth.wuZcTotalAccountValue, currMonth.wuAAcSurrPanalty), math.bignumber(0));
      var illustrationIndex = currMonth.cAcYear - 1;
      if (arorkey === 'aror2') {
        if (m % 12 === 1) {
          currMonth.rYieldAnnuallsedPremium = math.multiply(-1, vMonthlyPremiumIncome);
        } else {
          currMonth.rYieldAnnuallsedPremium = math.subtract(prevMonth.rYieldAnnuallsedPremium, vMonthlyPremiumIncome);
        }
        if (m === currMonth.cAcYear * 12) {
          currMonth.rYieldSurrenderValue = currMonth.cAcYear <= reduceYieldTargetYear ? currMonth.uABcSurrValue : math.bignumber(0);
          currMonth.rYieldCalculation = currMonth.cAcYear <= reduceYieldTargetYear ? currMonth.rYieldAnnuallsedPremium : (currMonth.cAcYear > 1 ? rawData[(currMonth.cAcYear - 1) * 12].rYieldSurrenderValue : math.bignumber(0));
          yieldCalculation.push(currMonth.rYieldCalculation);
          if (illustrationIndex === illustration.length - 1) {
            currMonth.rYieldReducedReturnRate = runFunc(planDetail.formulas.IRR, yieldCalculation);
          }
        }
      } /*Sustainability Test*/
      if (sustainTestResult && (currMonth.cAcYear <= 30 || currMonth.cCcAge <= 65) && currMonth.uZcTotalAccountValue < 0) {
        sustainTestResult = false;
      } /*Prepare illustration*/
      if (m === currMonth.cAcYear * 12 && illustrationIndex < illustration.length) {
        var iData = illustration[illustrationIndex];
        var arorRateKey = arorRate * 100;
        if (illustrationIndex === 0 && quotation.policyOptions.wdSelect) {
          iData.warningMsg = 'The projected Account Value under different projected investment rates of return may or may not be sufficient to carry out your requested Regular Withdrawal amount throughout the requested Regular Withdrawal duration. Please refer to the "Important Notes" section and the "Supplementary Illustration (Regular Withdrawal)" page in the Policy Illustration for more details of how long the Account Value may sustain your requested Regular Withdrawal.';
        }
        iData.policyYear = currMonth.cAcYear;
        iData.age = quotation.iAge + currMonth.cAcYear;
        iData.totalPremiumPaidToDate = round(currMonth.cAMcTotalModalPremium, 0);
        iData.guaranteedDeathBenefit = round(currMonth.uADcGuaranteedDeathBenefit, 0);
        if (!iData.nonGuaranteedDeathBenefit) {
          iData.nonGuaranteedDeathBenefit = {};
        }
        iData.nonGuaranteedDeathBenefit[arorRateKey] = round(currMonth.uACcNonGuaranteedDeathBenefit, 0);
        if (!iData.totalDeathBenefit) {
          iData.totalDeathBenefit = {};
        }
        iData.totalDeathBenefit[arorRateKey] = round(currMonth.uAEcTotalDeathBenefit, 0);
        if (!iData.surrenderValue) {
          iData.surrenderValue = {};
        }
        iData.surrenderValue[arorRateKey] = round(currMonth.uABcSurrValue, 0);
        if (!iData.totalAccountValue) {
          iData.totalAccountValue = {};
        }
        iData.totalAccountValue[arorRateKey] = round(currMonth.uZcTotalAccountValue, 0);
        if (!iData.accumulatedPremPaid) {
          iData.accumulatedPremPaid = {};
        }
        iData.accumulatedPremPaid[arorRateKey] = round(currMonth.uAJcAccumulatedPremPaid, 0);
        if (!iData.effectOfDeduction) {
          iData.effectOfDeduction = {};
        }
        iData.effectOfDeduction[arorRateKey] = round(currMonth.uAKcEffectOfDeduction, 0);
        if (!iData.accumulatedTotalDistCost) {
          iData.accumulatedTotalDistCost = {};
        }
        iData.accumulatedTotalDistCost[arorRateKey] = round(currMonth.uAIcAccumulatedTotalDistCost, 0);
        if (!iData.annualWithdrawalAmount) {
          iData.annualWithdrawalAmount = {};
        }
        iData.annualWithdrawalAmount[arorRateKey] = round(currMonth.wuAnnualWithdrawalAmount, 0);
        if (!iData.nonGuaranteedAccValueAfterWD) {
          iData.nonGuaranteedAccValueAfterWD = {};
        }
        iData.nonGuaranteedAccValueAfterWD[arorRateKey] = round(currMonth.wuABcSurrValue, 0);
        if (!iData.totalDeathBenefitAfterWD) {
          iData.totalDeathBenefitAfterWD = {};
        }
        iData.totalDeathBenefitAfterWD[arorRateKey] = round(currMonth.wuAEcTotalDeathBenefit, 0);
        iData.rYieldAnnuallsedPremium = math.number(currMonth.rYieldAnnuallsedPremium);
        iData.rYieldSurrenderValue = math.number(currMonth.rYieldSurrenderValue);
        iData.rYieldCalculation = math.number(currMonth.rYieldCalculation);
        iData.rYieldReducedReturnRate = round(currMonth.rYieldReducedReturnRate * 100, 2);
        if (!iData.rYieldTotalEffectDeduction) {
          iData.rYieldTotalEffectDeduction = {};
        }
        iData.rYieldTotalEffectDeduction[arorRateKey] = round(currMonth.uAKcEffectOfDeduction, 0);
      }
    }
    convertBignumberToReadable(rawData);
    return sustainTestResult;
  };
  const month = (100 - quotation.iAge) * 12;
  var illustration = [];
  var defaultIllustrationData = {
    policyYear: 0,
    age: 0,
    totalPremiumPaidToDate: 0,
    guaranteedDeathBenefit: 0,
    nonGuaranteedDeathBenefit: null,
    totalDeathBenefit: null,
    surrenderValue: null,
    accumulatedPremPaid: null,
    effectOfDeduction: null,
    accumulatedTotalDistCost: null,
    annualWithdrawalAmount: null,
    nonGuaranteedAccValueAfterWD: null,
    totalDeathBenefitAfterWD: null,
    rYieldAnnuallsedPremium: 0,
    rYieldSurrenderValue: 0,
    rYieldCalculation: 0,
    rYieldReducedReturnRate: 0,
    rYieldTotalEffectDeduction: null
  };
  for (var i = 0; i < Number(planInfo.policyTerm); i++) {
    illustration.push(Object.assign({}, defaultIllustrationData));
  }
  for (var arorkey in rates.aror) {
    var sustainTestResult = calcIllustration(month, arorkey, illustration); /* if (!sustainTestResult && arorkey === 'aror1') { return { errorMsg: 'Please increase your premium or increase your premium payment term, subject to Entry Age + Premium Payment Term is less than or equal to 75 years.' }; } */
  }
  return illustration;
}