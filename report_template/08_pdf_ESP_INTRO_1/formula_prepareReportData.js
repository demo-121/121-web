function(quotation, planInfo, planDetails, extraPara) { /*ESP CoverFormula*/
  var trunc = function(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, Number(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  let company = extraPara.company;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb
  } = company;
  var retVal = {};
  if (company) {
    var retVal = {
      'compName': compName,
      'compRegNo': compRegNo,
      'compAddr': compAddr,
      'compAddr2': compAddr2,
      'compTel': compTel,
      'compFax': compFax,
      'compWeb': compWeb,
      'coverPage': {
        'genDate': new Date(extraPara.systemDate).format(extraPara.dateFormat)
      }
    };
  }
  retVal.proposerGenderTitle = (quotation.pGender) == 'M' ? 'Male' : 'Female';
  retVal.proposerSmokerTitle = (quotation.pSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.insurerGenderTitle = (quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.insurerSmokerTitle = (quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  var ccy = quotation.ccy;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = 'US$';
  } else if (ccy == 'ASD') {
    polCcy = 'Australian Dollars';
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    polCcy = 'British Pound';
    ccySyb = '£';
  }
  retVal.polCcy = polCcy;
  retVal.ccySyb = ccySyb;
  var paymentModeTitle = '';
  if (quotation.paymentMode == 'A') {
    paymentModeTitle = 'Annual';
  } else if (quotation.paymentMode == 'S') {
    paymentModeTitle = 'Semi-Annual';
  } else if (quotation.paymentMode == 'Q') {
    paymentModeTitle = 'Quarterly';
  } else if (quotation.paymentMode == 'M') {
    paymentModeTitle = 'Monthly';
  }
  retVal.paymentModeTitle = paymentModeTitle;
  var plans = quotation.plans;
  retVal.totalAPrem = quotation.totYearPrem;
  retVal.totalSPrem = quotation.totHalfyearPrem;
  retVal.totalQPrem = quotation.totQuarterPrem;
  retVal.totalMPrem = quotation.totMonthPrem;
  retVal.guaranteedCashPayoutType = quotation.policyOptionsDesc.guaranteedCashPayoutType.en;
  var planCodes = "";
  for (var i = 0; i < plans.length; i++) {
    planCodes = planCodes + plans[i].planCode + ", ";
  }
  var basicPlan = plans[0];
  var additionalPlanTitle = ' ' + basicPlan.premTerm.toString() + ' PAY';
  var planFields = [];
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    var policyTerm = plan.policyTerm;
    var permTermTitle = plan.premTermDesc;
    var covCode = plan.covCode;
    var sumAssured = getCurrency(plan.sumInsured, ' ', 0);
    var planDetail = planDetails[covCode];
    var policyTermList = planDetail.inputConfig.policyTermList;
    var planInd = planDetail.planInd;
    var planName = !plan.covName ? "" : (typeof plan.covName == 'string' ? plan.covName : (plan.covName.en || plan.covName[Object.keys(plan.covName)[0]]));
    if (covCode === 'ESP' || covCode === 'ESR') {
      planName = plan.covName.en + additionalPlanTitle;
    }
    var polTermTitle = -1;
    for (var j = 0; j < policyTermList.length; j++) {
      var polTermOpt = policyTermList[j];
      if (polTermOpt.value == policyTerm) polTermTitle = polTermOpt.title;
    }
    if (polTermTitle != -1) {
      var polTerm = {
        covCode: covCode,
        planInd: planInd,
        policyTerm: polTermTitle,
        permTerm: permTermTitle,
        sumAssured: sumAssured,
        planName: planName
      };
      planFields.push(polTerm);
    }
  }
  var ESRSumAssured = "";
  var numOfPlans = planFields.length;
  for (var j = 0; j < numOfPlans; j++) {
    let plan = planFields[j];
    if (j === 0) {
      ESRSumAssured = plan.sumAssured;
    }
    if (plan.covCode === "ESR") {
      plan.sumAssured = ESRSumAssured;
      plan.planInd = 'B';
    }
  }
  retVal.planFields = planFields;
  retVal.planCodes = planCodes.substring(0, planCodes.length - 2);
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  retVal.iDob = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.proposerDob = new Date(quotation.pDob).format(extraPara.dateFormat);
  retVal.insurerDob = new Date(quotation.iDob).format(extraPara.dateFormat);
  var premiumInfos = [];
  plans.forEach(function(plan, i) {
    var covCode = plan.covCode;
    let premiumInfo = {};
    if (covCode === 'ESP' || covCode === 'ESR') {
      premiumInfo.planName = plan.covName.en + additionalPlanTitle;
    } else {
      premiumInfo.planName = plan.covName.en;
    }
    premiumInfo.yearPrem = getCurrency(plan.yearPrem, ' ', 2);
    premiumInfo.halfYearPrem = getCurrency(plan.halfYearPrem, ' ', 2);
    premiumInfo.quarterPrem = getCurrency(plan.quarterPrem, ' ', 2);
    premiumInfo.monthPrem = getCurrency(plan.monthPrem, ' ', 2);
    premiumInfos.push(premiumInfo);
  });
  retVal.premiumInfos = premiumInfos;
  return retVal;
}