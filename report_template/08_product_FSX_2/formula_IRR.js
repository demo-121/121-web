function(values) { /* var irrResult = function(values, dates, rate) { var r = math.add(math.bignumber(rate), 1); var result = math.bignumber(values[0]); for (var i = 1; i < values.length; i++) { result = math.add(result, math.divide(math.bignumber(values[i]), math.pow(r, math.divide(math.subtract(dates[i], dates[0]), 365)))); } return math.bignumber(result); }; var irrResultDeriv = function(values, dates, rate) { var r = math.add(math.bignumber(rate), 1); var result = math.bignumber(0); for (var i = 1; i < values.length; i++) { var frac = math.bignumber(math.divide(math.subtract(dates[i], dates[0]), 365)); result = math.subtract(result, math.divide(math.bignumber(math.multiply(frac, math.bignumber(values[i]))), math.bignumber(math.pow(r, math.add(frac, 1))))); } return math.bignumber(result); }; var dates = []; var positive = false; var negative = false; for (var i = 0; i < values.length; i++) { if (i === 0) { dates[i] = math.bignumber(0); } else { dates[i] = math.add(math.bignumber(dates[i - 1]), 365); } if (values[i] > 0) positive = true; if (values[i] < 0) negative = true; } if (!positive || !negative) return null; var irr = null; var guess = math.bignumber(-1); var maxGuess = math.bignumber(1); var trialInterval = math.bignumber(0.1); do { guess = math.add(guess, trialInterval); var resultRate = math.bignumber(guess); var epsMax = 1e-10; var iterMax = 50; var newRate, epsRate, resultValue, dValue; var iteration = 0; var contLoop = true; do { resultValue = irrResult(values, dates, resultRate); dValue = irrResultDeriv(values, dates, resultRate); newRate = math.subtract(resultRate, math.divide(resultValue, dValue)); epsRate = math.bignumber(Math.abs(math.subtract(newRate, resultRate))); resultRate = newRate; contLoop = (epsRate > epsMax) && (Math.abs(resultValue) > epsMax); } while (contLoop && (++iteration < iterMax)); if (!contLoop) { irr = resultRate; } } while (guess <= maxGuess && irr === null); return math.number(irr); */ /* Copyright (c) 2012 Sutoiku, Inc. (MIT License) Some algorithms have been ported from Apache OpenOffice:*/ /************************************************************** * * Licensed to the Apache Software Foundation (ASF) under one * or more contributor license agreements. See the NOTICE file * distributed with this work for additional information * regarding copyright ownership. The ASF licenses this file * to you under the Apache License, Version 2.0 (the * "License"); you may not use this file except in compliance * with the License. You may obtain a copy of the License at * * http://www.apache.org/licenses/LICENSE-2.0 * * Unless required by applicable law or agreed to in writing, * software distributed under the License is distributed on an * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY * KIND, either express or implied. See the License for the * specific language governing permissions and limitations * under the License. * *************************************************************/ /* Credits: algorithm inspired by Apache OpenOffice Calculates the resulting amount*/
  var irrResult = function(values, dates, rate) {
    var r = rate + 1;
    var result = values[0];
    for (var i = 1; i < values.length; i++) {
      result += values[i] / Math.pow(r, (dates[i] - dates[0]) / 365);
    }
    return result;
  }; /* Calculates the first derivation*/
  var irrResultDeriv = function(values, dates, rate) {
    var r = rate + 1;
    var result = 0;
    for (var i = 1; i < values.length; i++) {
      var frac = (dates[i] - dates[0]) / 365;
      result -= frac * values[i] / Math.pow(r, frac + 1);
    }
    return result;
  }; /* Initialize dates and check that values contains at least one positive value and one negative value*/
  var dates = [];
  var positive = false;
  var negative = false;
  for (var i = 0; i < values.length; i++) {
    dates[i] = (i === 0) ? 0 : dates[i - 1] + 365;
    if (values[i] > 0) positive = true;
    if (values[i] < 0) negative = true;
  } /* Return error if values does not contain at least one positive value and one negative value */
  if (!positive || !negative) return '#NUM!'; /* Initialize guess and resultRate */ /*var guess = (typeof guess === 'undefined') ? 0.1 : guess;*/
  var guess = 0.00000001;
  var resultRate = guess; /* Set maximum epsilon for end of iteration*/
  var epsMax = 1e-10; /* Set maximum number of iterations*/
  var iterMax = 50; /* Implement Newton's method */
  var newRate, epsRate, resultValue;
  var iteration = 0;
  var contLoop = true;
  do {
    resultValue = irrResult(values, dates, resultRate);
    newRate = resultRate - resultValue / irrResultDeriv(values, dates, resultRate);
    epsRate = Math.abs(newRate - resultRate);
    resultRate = newRate;
    contLoop = (epsRate > epsMax) && (Math.abs(resultValue) > epsMax);
  } while (contLoop && (++iteration < iterMax));
  if (contLoop) return '#NUM!'; /* Return internal rate of return */
  return resultRate;
}