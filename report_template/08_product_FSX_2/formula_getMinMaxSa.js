function(quotation, planInfo, planDetail) {
  var payMode = quotation.paymentMode;
  var iAge = quotation.iAge;
  var coi = quotation.policyOptions.insuranceCharge;
  var annualPremium = math.number(quotation.plans[0].premium || 0) * (payMode == "M" ? 12 : payMode == "Q" ? 4 : payMode == "S" ? 2 : payMode == "A" ? 1 : 0);
  var min = 5 * annualPremium;
  var max = 59 * annualPremium;
  if (coi == "yrt") {
    if (iAge >= 0 && iAge <= 35) {
      max = 59 * annualPremium;
    } else if (iAge >= 36 && iAge <= 50) {
      max = 29 * annualPremium;
    } else if (iAge >= 51 && iAge <= 70) {
      max = 14 * annualPremium;
    }
  } else {
    if (iAge >= 0 && iAge <= 35) {
      max = 24 * annualPremium;
    } else if (iAge >= 36 && iAge <= 50) {
      max = 14 * annualPremium;
    } else if (iAge >= 51 && iAge <= 70) {
      max = 9 * annualPremium;
    }
  }
  return {
    min,
    max
  };
}