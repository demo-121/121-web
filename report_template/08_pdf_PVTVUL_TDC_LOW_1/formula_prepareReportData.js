function(quotation, planInfo, planDetails, extraPara) { /*test pvtvul tdc**/
  var illust = extraPara.illustrations[planInfo.covCode];
  var company = extraPara.company;
  var sameAs = quotation.sameAs;
  var tdc_Low = [];
  var tdc_Low2 = [];
  var policyTerm = parseInt(planInfo.policyTerm);
  var polType = (quotation.policyOptions.policytype == "joint") ? true : false;
  var i;
  var yearCount = 0;
  var yearCount2 = 0;
  var polTerm = 0;
  var initPremiumTop = illust[0].totalPremium;
  var sumAssuredTop = illust[0].sumAssured.IRR_UI;
  var incRowCnt = 0;
  var incRowRiderCnt = 0;
  var incRowAnnualCnt = 0;
  var tdcbasicCnt = 0;
  var LA1age = quotation.pAge + 1;
  var iAge = quotation.iAge + 1;
  var showTextThisPage1 = false;
  var showTextThisPage2 = false;
  var getSmokingDesc = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  var getGenderDesc = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getCurrencyDesc = function(ccy) {
    var sign = "$";
    if (ccy === 'SGD') {
      sign = 'S$ ';
    } else if (ccy === 'USD') {
      sign = 'US$ ';
    } else if (ccy === 'GBP') {
      sign = '£ ';
    } else if (ccy === 'EUR') {
      sign = '€ ';
    } else if (ccy === 'AUD') {
      sign = 'A$ ';
    } else if (ccy === 'JPY') {
      sign = '¥ ';
    } else if (ccy === 'CHF') {
      sign = 'CHF ';
    }
    return sign;
  };
  for (i = 0; i < illust.length; i++) {
    var row = illust[i];
    var policyYear = row.policyYear;
    var LA2age = row.age;
    var LA12age = LA1age + ' / ' + LA2age;
    var endYearAge = row.policyYear + ' / ' + LA2age;
    var totPremiumPtd = row.totalPremium;
    var valuePremiumPtd = row.valueOf_Premium.IRR_Low;
    var eodUI = row.effect_Deduct.IRR_Low;
    var surrenderValueUI = row.surrender_Value.IRR_Low;
    var incRow = false;
    if (policyYear <= policyTerm) {
      incRow = true;
      incRowCnt++;
    }
    var incRowCntLineBreak = false;
    var incRowCntLineBreak5 = false;
    if (incRowCnt !== 0 && incRowCnt % 5 === 0 && incRowCnt !== illust.length) {
      incRowCntLineBreak = true;
    }
    if (incRowCnt !== 0 && ((incRowCnt + 15) % 25) === 0 && incRowCnt !== illust.length) {
      incRowCntLineBreak5 = true;
    }
    if (polType) {
      if (incRow) {
        if (i < 110) {
          if (policyYear < 10) {
            tdc_Low.push({
              policyYear: policyYear,
              LA12age: LA2age,
              totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
              valuePremiumPtd: getCurrency(valuePremiumPtd, '', 0),
              eodUI: getCurrency(eodUI, '', 0),
              surrenderValueUI: getCurrency(surrenderValueUI, '', 0)
            });
            if (incRowCntLineBreak) {
              tdc_Low.push({
                policyYear: "",
                LA12age: "",
                totPremiumPtd: "",
                valuePremiumPtd: "",
                eodUI: "",
                surrenderValueUI: ""
              });
            }
          } else {
            if (policyYear % 5 === 0 || iAge == 100) {
              tdc_Low.push({
                policyYear: policyYear,
                LA12age: LA2age,
                totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
                valuePremiumPtd: getCurrency(valuePremiumPtd, '', 0),
                eodUI: getCurrency(eodUI, '', 0),
                surrenderValueUI: getCurrency(surrenderValueUI, '', 0)
              });
              if (incRowCntLineBreak5 || policyYear == 10) {
                tdc_Low.push({
                  policyYear: "",
                  LA12age: "",
                  totPremiumPtd: "",
                  valuePremiumPtd: "",
                  eodUI: "",
                  surrenderValueUI: ""
                });
              }
            }
          }
        } else {
          if (policyYear < 10) {
            tdc_Low2.push({
              policyYear: policyYear,
              LA12age: LA2age,
              totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
              valuePremiumPtd: getCurrency(valuePremiumPtd, '', 0),
              eodUI: getCurrency(eodUI, '', 0),
              surrenderValueUI: getCurrency(surrenderValueUI, '', 0)
            });
            if (incRowCntLineBreak) {
              tdc_Low2.push({
                policyYear: "",
                LA12age: "",
                totPremiumPtd: "",
                valuePremiumPtd: "",
                eodUI: "",
                surrenderValueUI: ""
              });
            }
          } else {
            if (policyYear % 5 === 0 || iAge == 100) {
              tdc_Low2.push({
                policyYear: policyYear,
                LA12age: LA2age,
                totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
                valuePremiumPtd: getCurrency(valuePremiumPtd, '', 0),
                eodUI: getCurrency(eodUI, '', 0),
                surrenderValueUI: getCurrency(surrenderValueUI, '', 0)
              });
              if (incRowCntLineBreak5 || policyYear == 10) {
                tdc_Low2.push({
                  policyYear: "",
                  LA12age: "",
                  totPremiumPtd: "",
                  valuePremiumPtd: "",
                  eodUI: "",
                  surrenderValueUI: ""
                });
              }
            }
          }
        }
      }
    } else {
      if (incRow) {
        if (i < 110) {
          if (policyYear < 10) {
            tdc_Low.push({
              endYearAge: endYearAge,
              totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
              valuePremiumPtd: getCurrency(valuePremiumPtd, '', 0),
              eodUI: getCurrency(eodUI, '', 0),
              surrenderValueUI: getCurrency(surrenderValueUI, '', 0)
            });
            if (incRowCntLineBreak) {
              tdc_Low.push({
                endYearAge: "",
                totPremiumPtd: "",
                valuePremiumPtd: "",
                eodUI: "",
                surrenderValueUI: ""
              });
            }
          } else {
            if (policyYear % 5 === 0 || iAge == 100) {
              tdc_Low.push({
                endYearAge: endYearAge,
                totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
                valuePremiumPtd: getCurrency(valuePremiumPtd, '', 0),
                eodUI: getCurrency(eodUI, '', 0),
                surrenderValueUI: getCurrency(surrenderValueUI, '', 0)
              });
              if (incRowCntLineBreak5 || policyYear == 10) {
                tdc_Low.push({
                  endYearAge: "",
                  totPremiumPtd: "",
                  valuePremiumPtd: "",
                  eodUI: "",
                  surrenderValueUI: ""
                });
              }
            }
          }
        } else {
          if (policyYear < 10) {
            tdc_Low2.push({
              endYearAge: endYearAge,
              totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
              valuePremiumPtd: getCurrency(valuePremiumPtd, '', 0),
              eodUI: getCurrency(eodUI, '', 0),
              surrenderValueUI: getCurrency(surrenderValueUI, '', 0)
            });
            if (incRowCntLineBreak) {
              tdc_Low2.push({
                endYearAge: "",
                totPremiumPtd: "",
                valuePremiumPtd: "",
                eodUI: "",
                surrenderValueUI: ""
              });
            }
          } else {
            if (policyYear % 5 === 0 || iAge == 100) {
              tdc_Low2.push({
                endYearAge: endYearAge,
                totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
                valuePremiumPtd: getCurrency(valuePremiumPtd, '', 0),
                eodUI: getCurrency(eodUI, '', 0),
                surrenderValueUI: getCurrency(surrenderValueUI, '', 0)
              });
              if (incRowCntLineBreak5 || policyYear == 10) {
                tdc_Low2.push({
                  endYearAge: "",
                  totPremiumPtd: "",
                  valuePremiumPtd: "",
                  eodUI: "",
                  surrenderValueUI: ""
                });
              }
            }
          }
        }
      }
    }
    incRowCntLineBreak = false;
    if (iAge == 100) {
      break;
    }
    LA1age++;
    iAge++;
  }
  var result = [];
  if (tdc_Low.length < 24) {
    showTextThisPage1 = true;
  } else {
    showTextThisPage2 = true;
  }
  result = {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      releaseVersion: "1"
    },
    cover: {
      sameAs: quotation.sameAs,
      ccy: quotation.ccy,
      initPremiumTop: getCurrency(initPremiumTop, '', 0),
      sumAssuredTop: getCurrency(sumAssuredTop, '', 0),
      ccySign: getCurrencyDesc(quotation.ccy),
      polType: quotation.policyOptions.policytype,
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      insured: {
        name: quotation.pFullName,
        gender: getGenderDesc(quotation.pGender),
        dob: new Date(quotation.pDob).format(extraPara.dateFormat),
        age: quotation.pAge,
        smoking: getSmokingDesc(quotation.pSmoke)
      }
    },
    illustration: {
      tdc_Low: tdc_Low,
      tdc_Low2: tdc_Low2,
      showTextThisPage1: showTextThisPage1,
      showTextThisPage2: showTextThisPage2,
    }
  };
  if (showTextThisPage1) {
    result.hidePagesIndexArray = [1];
  }
  return result;
}