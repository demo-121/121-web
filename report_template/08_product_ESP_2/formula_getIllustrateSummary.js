function(quotation, planDetails, extraPara, illustrations) { /** Early Saver Plus getillustrateSummary*/
  var numberTrunc = function(value = 0, digit) {
    value = value.toString();
    if (value.indexOf('.') === -1) {
      return math.bignumber(Number.parseInt(value));
    } else {
      value = value.substr(0, value.indexOf('.') + digit + 1);
      return math.bignumber(Number.parseFloat(value));
    }
  };
  var paddingZeroRight = function(value) {
    value = value.toString();
    if (value.indexOf('.') > -1) {
      var decimal = value.split('.');
      if (decimal[1].length === 1) {
        decimal[1] = decimal[1] + '0';
        return decimal[0] + '.' + decimal[1];
      } else {
        return value;
      }
    } else {
      return value;
    }
  };
  var Basic_BT = Number.parseInt(quotation.plans[0].policyTerm);
  var totalRiderTDC = [];
  var riderTDC = {};
  var rates;
  var Re_TDC_Factor = 1;
  var totPremiumPaidArr = [];
  var totProjectedCashPayouts3PaArr = [];
  var basicIllustration = illustrations[quotation.baseProductCode];
  var mainBIDBArr = [];
  var mainBIDB = {};
  var mainBISVArr = [];
  var mainBISV = {};
  var tableofDeductionsArr = [];
  var tableofDeductions = {};
  var tdcArr = [];
  var tdcPage = {};
  var secondTdc = {};
  var suppBIDBArr = [];
  var suppBIDB = {};
  var suppBISVArr = [];
  var suppBISV = {};
  var netCFforAccArr = [];
  var previousRiderTotalPremPaid = 0;
  var previousRidertdc = 0;
  var commRates = runFunc(planDetails[quotation.baseProductCode].formulas.getCommRates, quotation, quotation.plans[0], planDetails, 'RIDER');
  for (var i = 0; i < Basic_BT; i++) {
    var totalPremiumPaid = 0;
    var tdc = 0;
    _.each(quotation.plans, plan => {
      if (plan.covCode !== quotation.baseProductCode && planDetails[quotation.baseProductCode].rates.commonTable[plan.covCode]) {
        rates = commRates[i];
        if (i + 1 <= Number.parseInt(plan.premTerm)) {
          totalPremiumPaid += plan.yearPrem;
          tdc += numberTrunc(math.multiply(math.multiply(math.bignumber(plan.yearPrem), rates || 0), Re_TDC_Factor), 2);
        }
      }
    });
    previousRiderTotalPremPaid = (i - 1 > -1) ? math.bignumber(totalRiderTDC[i - 1].totalPremiumPaid) : 0;
    previousRidertdc = (i - 1 > -1) ? math.bignumber(totalRiderTDC[i - 1].tdc) : 0;
    riderTDC = {
      polYear: i + 1,
      age: i + 1 + quotation.iAge,
      totalPremiumPaid: math.number(math.add(previousRiderTotalPremPaid, math.bignumber(totalPremiumPaid)) || 0),
      tdc: math.number(math.add(previousRidertdc, math.bignumber(tdc)) || 0)
    };
    totalRiderTDC.push(riderTDC);
    totProjectedCashPayouts3PaArr.push(basicIllustration[i].high.accOnCashback);
    totPremiumPaidArr.push(basicIllustration[i].high.tpdBasicPrem);
    netCFforAccArr.push(basicIllustration[i].high.netCFforAccum); /**main bi (db)*/
    var mainBIDBguaranteed, mainBIDBnongteed325, mainBIDBnongteed475;
    mainBIDBguaranteed = numberTrunc(basicIllustration[i].high.gteedDBMain, 0);
    mainBIDBnongteed325 = numberTrunc(basicIllustration[i].low.nongteedDBMain, 0);
    mainBIDBnongteed475 = numberTrunc(basicIllustration[i].high.nongteedDBMain, 0);
    mainBIDB = {
      polYear: i + 1,
      age: i + 1 + quotation.iAge,
      totPremiumPaid: math.number(numberTrunc(basicIllustration[i].high.tpdBasicPrem, 0)),
      guaranteed: math.number(mainBIDBguaranteed),
      nongteed325: math.number(mainBIDBnongteed325),
      toal325: math.number(math.add(mainBIDBguaranteed, mainBIDBnongteed325)),
      nongteed475: math.number(mainBIDBnongteed475),
      total475: math.number(math.add(mainBIDBguaranteed, mainBIDBnongteed475))
    };
    mainBIDBArr.push(mainBIDB); /**main bi (sv)*/
    var mainBISVguaranteed, mainBISVnongteed325, mainBISVnongteed475;
    mainBISVguaranteed = numberTrunc(basicIllustration[i].low.gteedSVMain, 0);
    mainBISVnongteed325 = numberTrunc(basicIllustration[i].low.nongteedSVMain, 0);
    mainBISVnongteed475 = numberTrunc(basicIllustration[i].high.nongteedSVMain, 0);
    mainBISV = {
      polYear: i + 1,
      age: i + 1 + quotation.iAge,
      totPremiumPaid: math.number(numberTrunc(basicIllustration[i].high.tpdBasicPrem, 0)),
      guaranteed: math.number(mainBISVguaranteed),
      nongteed325: math.number(mainBISVnongteed325),
      toal325: math.number(math.add(mainBISVguaranteed, mainBISVnongteed325)),
      nongteed475: math.number(mainBISVnongteed475),
      total475: math.number(math.add(mainBISVguaranteed, mainBISVnongteed475))
    };
    mainBISVArr.push(mainBISV); /** Table of deductions*/
    tableofDeductions = {
      polYear: i + 1,
      age: i + 1 + quotation.iAge,
      totPremiumPaid: math.number(numberTrunc(basicIllustration[i].high.tpdBasicPrem, 0)),
      premiumPaid325: math.number(numberTrunc(basicIllustration[i].low.vopPtd, 0)),
      effectofDeduction325: math.number(numberTrunc(basicIllustration[i].low.effectOfDeduction, 0)),
      totalSurrenderValue325: math.number(mainBISV.toal325),
      premiumPaid475: math.number(numberTrunc(basicIllustration[i].high.vopPtd, 0)),
      effectofDeduction475: math.number(numberTrunc(basicIllustration[i].high.effectOfDeduction, 0)),
      totalSurrenderValue475: math.number(mainBISV.total475)
    };
    tableofDeductionsArr.push(tableofDeductions); /** TDC */
    tdcPage = {
      polYear: i + 1,
      age: i + 1 + quotation.iAge,
      totPremiumPaid: math.number(numberTrunc(basicIllustration[i].high.tpdBasicPrem, 0)),
      totDistributionProjected: math.number(numberTrunc(basicIllustration[i].high.totDistCost, 0))
    };
    tdcArr.push(tdcPage); /** Second of TDC*/
    if (i + 1 === Basic_BT) {
      secondTdc = {
        EOTD_L: math.number(numberTrunc(basicIllustration[i].low.effectOfDeduction, 0)),
        REDUC_YIELD_L: paddingZeroRight(numberTrunc(math.multiply(math.bignumber(illustrations[quotation.baseProductCode][quotation.plans[0].policyTerm - 1].low.totalNonGteedSurrenderYieldinPrecent), 100), 2)),
        EOTD_H: math.number(numberTrunc(basicIllustration[i].high.effectOfDeduction, 0)),
        REDUC_YIELD_H: paddingZeroRight(numberTrunc(math.multiply(math.bignumber(illustrations[quotation.baseProductCode][quotation.plans[0].policyTerm - 1].high.totalNonGteedSurrenderYieldinPrecent), 100), 2))
      };
    } /** supp BI (DB) */
    var suppBIDBguaranteed, suppBIDBnongteed325, suppBIDBnongteed475;
    suppBIDBguaranteed = numberTrunc(basicIllustration[i].low.gteedDBSupp, 0);
    suppBIDBnongteed325 = numberTrunc(basicIllustration[i].low.nongteedDBSupp, 0);
    suppBIDBnongteed475 = numberTrunc(basicIllustration[i].high.nongteedDBSupp, 0);
    suppBIDB = {
      polYear: i + 1,
      age: i + 1 + quotation.iAge,
      totPremiumPaid: math.number(numberTrunc(basicIllustration[i].high.tpdBasicPrem, 0)),
      guaranteed: math.number(suppBIDBguaranteed),
      nongteed325: math.number(suppBIDBnongteed325),
      toal325: math.number(math.add(suppBIDBguaranteed, suppBIDBnongteed325)),
      nongteed475: math.number(suppBIDBnongteed475),
      total475: math.number(math.add(suppBIDBguaranteed, suppBIDBnongteed475))
    };
    suppBIDBArr.push(suppBIDB); /** supp BI (SV) */
    var suppBISVguaranteed, suppBISVnongteed325, suppBISVnongteed475;
    suppBISVguaranteed = numberTrunc(basicIllustration[i].low.gsvSupp, 0);
    suppBISVnongteed325 = numberTrunc(basicIllustration[i].low.nongteedSVSupp, 0);
    suppBISVnongteed475 = numberTrunc(basicIllustration[i].high.nongteedSVSupp, 0);
    suppBISV = {
      polYear: i + 1,
      age: i + 1 + quotation.iAge,
      totPremiumPaid: math.number(numberTrunc(basicIllustration[i].high.tpdBasicPrem, 0)),
      guaranteed: math.number(suppBISVguaranteed),
      nongteed325: math.number(suppBISVnongteed325),
      toal325: math.number(math.add(suppBISVguaranteed, suppBISVnongteed325)),
      nongteed475: math.number(suppBISVnongteed475),
      total475: math.number(math.add(suppBISVguaranteed, suppBISVnongteed475))
    };
    suppBISVArr.push(suppBISV);
    var ill = illustrations[quotation.baseProductCode][i];
    ill.guaranteedSV = {
      3.25: mainBISV.toal325,
      4.75: mainBISV.total475
    };
    ill.totYearPrem = mainBIDB.totPremiumPaid;
    ill.guaranteedDB = {
      3.25: mainBIDB.toal325,
      4.75: mainBIDB.total475
    };
  }
  netCFforAccArr.push(illustrations[quotation.baseProductCode][Basic_BT - 1].extraNetCFforAccum);
  illustrations.riders = totalRiderTDC; /**main bi (db)*/
  illustrations.mainBIDB = mainBIDBArr; /**main bi (sv)*/
  illustrations.mainBISV = mainBISVArr; /**Table of Deduction*/
  illustrations.tableofDeductions = tableofDeductionsArr; /**TDC*/
  illustrations.tdc = tdcArr;
  illustrations.secondTdc = secondTdc; /** supp BI (DB)*/
  illustrations.suppBIDB = suppBIDBArr; /** supp BI (SV)*/
  illustrations.suppBISV = suppBISVArr; /**Only basic plan plus package rider*/
  illustrations.premiumTPDLookup = math.number(math.add(math.bignumber(quotation.plans[0].yearPrem), math.bignumber(quotation.plans[1].yearPrem))); /**Gtd Cash Payout*/
  illustrations.totPremiumPaid = math.number(numberTrunc(math.max(...totPremiumPaidArr), 0));
  illustrations.totgteedCashPayoutsMinus2 = {
    year: quotation.plans[0].policyTerm - 2,
    value: math.number(numberTrunc(basicIllustration[quotation.plans[0].policyTerm - 3].high.gteedAnnualCashback, 0))
  };
  illustrations.totgteedCashPayoutsMinus1 = {
    year: quotation.plans[0].policyTerm - 1,
    value: math.number(numberTrunc(basicIllustration[quotation.plans[0].policyTerm - 2].high.gteedAnnualCashback, 0))
  };
  illustrations.totgteedCashPayoutsEnd = {
    year: quotation.plans[0].policyTerm,
    value: math.number(numberTrunc(illustrations[quotation.baseProductCode][quotation.plans[0].policyTerm - 1].high.gteedSVMain, 0))
  };
  illustrations.nonGteedBonus475Return = math.number(numberTrunc(illustrations[quotation.baseProductCode][quotation.plans[0].policyTerm - 1].high.nongteedSVMain, 0));
  illustrations.totProjectedPayout = math.number(math.add(illustrations.totgteedCashPayoutsMinus2.value, illustrations.totgteedCashPayoutsMinus1.value, illustrations.totgteedCashPayoutsEnd.value, illustrations.nonGteedBonus475Return));
  illustrations.totalPayoutRatio = paddingZeroRight(math.multiply(numberTrunc(math.divide(illustrations.totProjectedPayout, illustrations.totPremiumPaid), 2), 100)) + '%';
  illustrations.gteedInternalRateOfReturn = paddingZeroRight(math.multiply(numberTrunc(illustrations[quotation.baseProductCode][quotation.plans[0].policyTerm - 1].high.totalgteedSurrenderYieldinPrecent, 4), 100)) + '%';
  illustrations.gteedInternalRateOf475ProjectReturn = paddingZeroRight(math.multiply(numberTrunc(illustrations[quotation.baseProductCode][quotation.plans[0].policyTerm - 1].high.totalNonGteedSurrenderYieldinPrecent, 4), 100)) + '%';
  illustrations.totProjectedCashPayouts3Pa = math.number(numberTrunc(math.add(math.max(...totProjectedCashPayouts3PaArr), illustrations[quotation.baseProductCode][quotation.plans[0].policyTerm - 1].high.gteedSVMain), 0));
  illustrations.totProjectedPayout2 = math.number(math.add(illustrations.totProjectedCashPayouts3Pa, illustrations.nonGteedBonus475Return));
  illustrations.totalPayoutRatio2 = paddingZeroRight(math.multiply(numberTrunc(math.divide(illustrations.totProjectedPayout2, illustrations.totPremiumPaid), 2), 100)) + '%';
  illustrations.totalInternalRateofReturn = paddingZeroRight(math.multiply(numberTrunc(runFunc(planDetails[quotation.baseProductCode].formulas.IRR, netCFforAccArr), 4), 100)) + '%';
  return illustrations;
}