function(planDetail, quotation) {
  var premTerm = Number.parseInt(quotation.plans[0].premTerm);
  if (!premTerm) {
    return [];
  }
  var policyTermList = [];
  var initialValue;
  if (premTerm === 5) {
    initialValue = 10;
  } else {
    initialValue = 15;
  }
  var policyTerm = quotation.plans[0].policyTerm;
  var hasPolicyTermInList = false;
  for (var i = initialValue; i < 26; i++) {
    if (i === Number.parseInt(policyTerm)) {
      hasPolicyTermInList = true;
    }
    policyTermList.push({
      value: i,
      title: i + ' Years'
    });
  } /** Initialization when policyTerm not in List*/
  if (!hasPolicyTermInList) {
    var plans = quotation.plans;
    if (plans && plans[0].policyTerm) {
      for (var l = 0; l < plans.length; l++) {
        var plan = plans[l];
        plan.policyTerm = null;
        plan.polTermDesc = "";
        plan.sumInsured = null;
        plan.yearPrem = null;
        plan.premium = null;
      }
    }
  }
  return policyTermList;
}