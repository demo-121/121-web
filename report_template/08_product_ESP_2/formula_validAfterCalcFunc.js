function(quotation, planInfo, planDetail) {
  quotValid.validateMandatoryFields(quotation, planInfo, planDetail); /**ESP validAfterCalcFunc*/
  if (planInfo.calcBy && quotation.paymentMode) {
    var totalPrem = 0;
    for (var i = 0; i < quotation.plans.length; i++) {
      var plan = quotation.plans[i];
      if (plan.premium && (plan.covCode === 'ESP' || plan.covCode === 'ESR')) {
        totalPrem = math.add(totalPrem, math.bignumber(plan.premium));
      }
    }
    totalPrem = math.number(totalPrem);
    let min = planDetail.premlim[0].limits.filter(limit => {
      return limit.payMode === quotation.paymentMode;
    })[0].min;
    let saMin = planDetail.benlim[0].limits.filter(limit => {
      return limit.payMode === quotation.paymentMode;
    })[0].min;
    if (planDetail.inputConfig && planDetail.inputConfig.premlim) { /**if (min && min > totalPrem && planInfo.sumInsured && planInfo.premium) { quotDriver.context.addError({ covCode: planInfo.covCode, msg: 'Minimum premium is ' + getCurrencyByCcy(min, 0, quotation.compCode, quotation.ccy) }); }*/ }
    if (planInfo.calcBy === 'premium') {
      let saMin = planDetail.benlim[0].limits.filter(limit => {
        return limit.payMode === quotation.paymentMode;
      })[0].min;
      if (saMin > quotation.plans[0].sumInsured && planInfo.sumInsured && planInfo.premium) {
        quotDriver.context.addError({
          covCode: planInfo.covCode,
          msg: 'Please increase the premium to meet the minimum Sum Assured of ' + getCurrencyByCcy(saMin, 0, quotation.compCode, quotation.ccy)
        });
      }
    }
    if (planInfo.calcBy === 'sumAssured') {
      if (min > totalPrem && planInfo.sumInsured && planInfo.premium) {
        quotDriver.context.addError({
          covCode: planInfo.covCode,
          msg: 'Please increase the Sum Assured to meet minimum premium of ' + getCurrencyByCcy(min, 2, quotation.compCode, quotation.ccy)
        });
      }
    }
  }
  var planCodeMap = quotDriver.runFunc(planDetail.formulas.getPlanCodeMapping, planInfo);
  if (planInfo.policyTerm) {
    planInfo.planCode = planCodeMap;
  }
  if (quotation.plans[0].premium) {
    quotation.extraFlags.previousPremium = quotation.plans[0].premium;
  }
  if (!planInfo.sumInsured) {
    planInfo.premium = planInfo.yearPrem = planInfo.halfYearPrem = planInfo.quarterPrem = planInfo.monthPrem = planInfo.sumInsured;
  }
}