function(planDetail, mode) {
  if (planDetail.payModes) {
    var result = 0;
    for (var i in planDetail.payModes) {
      var paymentModeObj = planDetail.payModes[i];
      if (paymentModeObj.mode === mode) {
        result = paymentModeObj.factor;
      }
    }
    return result;
  } else {
    return 0;
  }
}