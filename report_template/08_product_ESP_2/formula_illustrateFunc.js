function(quotation, planInfo, planDetails, extraPara) { /**Early Saver Plus illustrateFunc*/
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var numberTrunc = function(value, digit) {
    value = value.toString();
    if (value.indexOf('.') === -1) {
      return math.bignumber(Number.parseInt(value));
    } else {
      value = value.substr(0, value.indexOf('.') + digit + 1);
      return math.bignumber(Number.parseFloat(value));
    }
  };
  var round = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var parsePrecentageToDecimal = function(value) {
    value = math.bignumber(parseFloat(value));
    if (!isNaN(value)) {
      value = math.divide(value, 100);
    } else {
      value = 0;
    }
    return value;
  }; /**Gen plan code*/
  var illYrs = Number.parseInt(quotation.plans[0].policyTerm);
  var basic_PT = Number.parseInt(quotation.plans[0].premTerm);
  var basic_BT = Number.parseInt(quotation.plans[0].policyTerm);
  var totYearPrem;
  var illustration = [];
  var planDetail = planDetails[planInfo.covCode];
  var commRates = runFunc(planDetail.formulas.getCommRates, quotation, planInfo, planDetails, 'BASIC');
  var gcvRateTables = planDetails[planInfo.covCode].rates.GCV;
  var ngcvRateTables = planDetails[planInfo.covCode].rates.NGCV;
  var commRate;
  var maxCommRate;
  var maxTerm;
  var tpdBasicPreviousPrem = 0;
  var previousTotDistCost = 0;
  var totDistCost = 0;
  var sumOfgteedAnnualCashback = 0;
  var sumOfgteedAnnualCashbackWithoutCurrentCashBack = 0;
  var previousAccOnCashback = 0;
  var previousAccOnCashback325 = 0;
  var rbFA, previousrbFA, previousrbFA325, rbFA325;
  previousrbFA = 0;
  previousrbFA325 = 0;
  var nongteedYieldCalculation, gteedYieldCaculation, nongteedYieldCalculation325;
  var previousGteedAnnualCashback = 0;
  var previousGteedAnnualCashback325 = 0;
  nongteedYieldCalculation = [];
  nongteedYieldCalculation325 = [];
  gteedYieldCaculation = [];
  var previoustotalSVSupp = 0;
  var planCode = runFunc(planDetail.formulas.getRatePlanCodeMapping, planInfo);
  for (var i = 0; i < illYrs; i++) {
    maxTerm = 0; /**Column I Cal 4.75*/
    var gteedCashback = (i === illYrs - 2 || i === illYrs - 3) ? 0.4 : 0;
    var gteedAnnualCashback = numberTrunc(math.multiply(math.bignumber(quotation.plans[0].sumInsured), math.bignumber(gteedCashback)), 2);
    sumOfgteedAnnualCashback = math.add(math.bignumber(sumOfgteedAnnualCashback), gteedAnnualCashback); /**Column L Cal 4.75*/
    var premiumPaid = (i + 1 <= basic_PT) ? planInfo.yearPrem : 0;
    var tpdPaidToDatePrem = (i + 1 <= basic_PT) ? quotation.plans[1].yearPrem : 0;
    premiumPaid = math.bignumber(premiumPaid);
    tpdPaidToDatePrem = math.bignumber(tpdPaidToDatePrem);
    tpdBasicPreviousPrem = math.bignumber(tpdBasicPreviousPrem);
    var tpdBasicPrem = math.add(tpdBasicPreviousPrem, premiumPaid, tpdPaidToDatePrem);
    tpdBasicPreviousPrem = tpdBasicPrem; /**Channel Common Rates _.each(commRates, (rates, key) => { var rateTerm = Number(key); if (rateTerm === i + 1) { commRate = rates[0]; } });*/
    commRate = commRates[i]; /** Using Channel Common Rate to calc total distribution cost Calc 4.75*/
    if (commRate) {
      totDistCost = math.add(math.multiply(commRate, math.add(math.bignumber(planInfo.yearPrem), math.bignumber(quotation.plans[1].yearPrem))), previousTotDistCost);
      previousTotDistCost = totDistCost;
    } else {
      totDistCost = previousTotDistCost;
    } /**Column Q Cal 4.75*/
    var totPremCashAdv = math.max(0, math.subtract(math.multiply(1.01, tpdBasicPrem), sumOfgteedAnnualCashbackWithoutCurrentCashBack)); /** Column Z Gteer SV Main Calc 4.75*/
    var gcvRate = math.bignumber(gcvRateTables[planCode][i + 2]);
    var gteedSVMain = math.divide(math.multiply(gcvRate, math.bignumber(quotation.plans[0].sumInsured)), 1000); /** Column AA GSV Supp Calc 3.25*/
    var gsvSupp = math.add(gteedSVMain, sumOfgteedAnnualCashbackWithoutCurrentCashBack); /** Gteer Db Main Calc 4.75*/
    var gteedDBMain = math.max(totPremCashAdv, gteedSVMain); /** Non-Gteed Db Main*/
    var rbintRate = parsePrecentageToDecimal(planDetails[planInfo.covCode].rates.bonus475[i + 1][1]);
    var rbRate = parsePrecentageToDecimal(planDetails[planInfo.covCode].rates.bonus475[i + 1][0]);
    var tbSurrRate = planDetails[planInfo.covCode].rates.BonusSV475[planCode][i] || 0; /**Start to calc 3.25*/
    var rbintRate325 = parsePrecentageToDecimal(planDetails[planInfo.covCode].rates.bonus325[i + 1][1]);
    var rbRate325 = parsePrecentageToDecimal(planDetails[planInfo.covCode].rates.bonus325[i + 1][0]);
    var tbSurrRate325 = planDetails[planInfo.covCode].rates.BonusSV325[planCode][i] || 0; /**Column T*/
    var rb = numberTrunc(math.add(round(math.multiply(previousrbFA, rbintRate), 2), round(math.multiply(rbRate, math.bignumber(quotation.plans[0].sumInsured)), 2)), 0);
    var rb325 = numberTrunc(math.add(round(math.multiply(previousrbFA325, rbintRate325), 2), round(math.multiply(rbRate325, math.bignumber(quotation.plans[0].sumInsured)), 2)), 0); /**Column U*/
    rbFA = math.add(previousrbFA, rb);
    rbFA325 = math.add(previousrbFA325, rb325);
    var ngcvRate = math.bignumber(ngcvRateTables[planCode][i + 4]); /**Column AB*/
    var nongteedRB = round(math.multiply(math.divide(ngcvRate, 100), rbFA), 2);
    var nongteedRB325 = round(math.multiply(math.divide(ngcvRate, 100), rbFA325), 2); /**Column AC*/
    var nongteedTB = numberTrunc(math.multiply(nongteedRB, tbSurrRate), 4);
    var nongteedTB325 = numberTrunc(math.multiply(nongteedRB325, tbSurrRate325), 4); /**Column AD*/
    var nongteedSVMain = numberTrunc(math.add(nongteedRB, nongteedTB), 2);
    var nongteedSVMain325 = numberTrunc(math.add(nongteedRB325, nongteedTB325), 2); /**Column AF total SV Main*/
    var totalSVMain = numberTrunc(math.add(gteedSVMain, nongteedSVMain), 2);
    var totalSVMain325 = numberTrunc(math.add(gteedSVMain, nongteedSVMain325), 2); /**Column X total DB Main*/
    var totalDBMain = math.max(totalSVMain, totPremCashAdv);
    var totalDBMain325 = math.max(totalSVMain325, totPremCashAdv); /**Column V non-gteed DB Main*/
    var nongteedDBMain = math.subtract(totalDBMain, gteedDBMain);
    var nongteedDBMain325 = math.subtract(totalDBMain325, gteedDBMain); /**Column S gteed db supp*/
    var gteedDBSupp = math.add(gteedDBMain, sumOfgteedAnnualCashbackWithoutCurrentCashBack);
    var gteedDBSupp325 = math.add(gteedDBMain, sumOfgteedAnnualCashbackWithoutCurrentCashBack); /**Column AR accumulation on cashback*/
    var accOnCashback = math.add(math.multiply(previousAccOnCashback, 1.03), gteedAnnualCashback);
    var accOnCashback325 = math.add(math.multiply(previousAccOnCashback325, 1.03), gteedAnnualCashback); /**Column AS interest on cashAdvance Option*/
    var intonCashAdvOption = math.subtract(accOnCashback, sumOfgteedAnnualCashback);
    var intonCashAdvOption325 = math.subtract(accOnCashback325, sumOfgteedAnnualCashback); /**Column Y total db supp*/
    var totalDBSupp = math.add(totalDBMain, sumOfgteedAnnualCashbackWithoutCurrentCashBack, intonCashAdvOption);
    var totalDBSupp325 = math.add(totalDBMain325, sumOfgteedAnnualCashbackWithoutCurrentCashBack, intonCashAdvOption325); /**Column W non-gteed DB Supp*/
    var nongteedDBSupp = math.subtract(totalDBSupp, gteedDBSupp);
    var nongteedDBSupp325 = math.subtract(totalDBSupp325, gteedDBSupp325); /**Column AE non-gteed SV Supp*/
    var nongteedSVSupp = numberTrunc(math.add(nongteedRB, nongteedTB, intonCashAdvOption), 2);
    var nongteedSVSupp325 = numberTrunc(math.add(nongteedRB325, nongteedTB325, intonCashAdvOption325), 2); /**Column AK negativeYield*/
    var negativeYield = (i + 1 <= basic_PT) ? math.subtract(math.multiply(math.bignumber(quotation.plans[0].yearPrem), -1), math.bignumber(quotation.plans[1].yearPrem)) : 0; /** CoLumn AH VOP p-t-d 4.75 325 */
    var vopPtd475;
    var vopPtd325;
    var firstfindingVOPIndex = i - 1;
    var firstfindingVOPIndexValue = 0;
    var firstfindingVOPIndexValue325 = 0;
    var highProjectRate = math.bignumber(0.0475);
    var lowProjectRate = math.bignumber(0.0325);
    if (firstfindingVOPIndex > -1) {
      firstfindingVOPIndexValue = illustration[firstfindingVOPIndex].high.vopPtd;
      firstfindingVOPIndexValue325 = illustration[firstfindingVOPIndex].low.vopPtd;
    }
    if (i + 1 <= basic_PT) {
      vopPtd475 = numberTrunc(math.multiply(math.add(math.bignumber(quotation.plans[1].yearPrem), math.bignumber(quotation.plans[0].yearPrem), firstfindingVOPIndexValue), math.add(1, highProjectRate)), 2);
      vopPtd325 = numberTrunc(math.multiply(math.add(math.bignumber(quotation.plans[1].yearPrem), math.bignumber(quotation.plans[0].yearPrem), firstfindingVOPIndexValue325), math.add(1, lowProjectRate)), 2);
    } else {
      vopPtd475 = numberTrunc(math.multiply(firstfindingVOPIndexValue, math.add(1, highProjectRate)), 2);
      vopPtd325 = numberTrunc(math.multiply(firstfindingVOPIndexValue325, math.add(1, lowProjectRate)), 2);
    } /** CoLumn AI accum cash Adv 4.75 325*/
    var accumCashAdv, accumCashAdv325;
    var firstfindingaccumCashAdvIndex = i - 1;
    var firstfindingaccumCashAdvValue = 0;
    var firstfindingaccumCashAdvValue325 = 0;
    if (firstfindingaccumCashAdvIndex > -1) {
      firstfindingaccumCashAdvValue = illustration[firstfindingaccumCashAdvIndex].high.accumCashAdv;
      firstfindingaccumCashAdvValue325 = illustration[firstfindingaccumCashAdvIndex].low.accumCashAdv;
    }
    accumCashAdv = math.add(math.multiply(firstfindingaccumCashAdvValue, math.add(1, highProjectRate)), gteedAnnualCashback);
    accumCashAdv325 = math.add(math.multiply(firstfindingaccumCashAdvValue325, math.add(1, lowProjectRate)), gteedAnnualCashback); /** CoLumn AJ effect of deduction 4.75 */
    var effectOfDeduction = math.subtract(vopPtd475, math.subtract(math.add(totalSVMain, accumCashAdv), gteedAnnualCashback)); /** CoLumn AJ effect of deduction 325 */
    var effectOfDeduction325 = math.subtract(vopPtd325, math.subtract(math.add(totalSVMain325, accumCashAdv325), gteedAnnualCashback)); /** CoLumn AK -ve premium for ph yield */
    var negativePremiumForPHyield = (i + 1 <= basic_PT) ? math.subtract(math.multiply(-1, math.bignumber(quotation.plans[0].yearPrem)), math.bignumber(quotation.plans[1].yearPrem)) : 0; /** CoLumn AG total SV Supp*/
    var totalSVSupp = numberTrunc(math.add(gsvSupp, nongteedSVSupp), 2); /** CoLumn AU net CF for accumlated*/
    var netCFforAccum = math.add(negativePremiumForPHyield, numberTrunc(((i === basic_BT) ? totalSVSupp : 0), 2));
    var netCFforPHYield = math.add(negativeYield, previousGteedAnnualCashback);
    nongteedYieldCalculation.push(netCFforPHYield);
    gteedYieldCaculation.push(netCFforPHYield);
    nongteedYieldCalculation325.push(netCFforPHYield); /**Column AN and AO*/
    var totalNonGteedSurrenderYieldinPrecent, totalgteedSurrenderYieldinPrecent, totalNonGteedSurrenderYieldinPrecent325;
    if (i + 1 === basic_BT) {
      nongteedYieldCalculation.push(numberTrunc(totalSVMain, 0));
      gteedYieldCaculation.push(numberTrunc(gteedSVMain, 0));
      nongteedYieldCalculation325.push(numberTrunc(totalSVMain325, 0)); /**totalNonGteedSurrenderYieldinPrecent = round(math.multiply(runFunc(planDetail.formulas.IRR, nongteedYieldCalculation), 100), 2); totalgteedSurrenderYieldinPrecent = round(math.multiply(runFunc(planDetail.formulas.IRR, gteedYieldCaculation), 100), 2);*/
      totalNonGteedSurrenderYieldinPrecent = runFunc(planDetail.formulas.IRR, nongteedYieldCalculation);
      totalgteedSurrenderYieldinPrecent = runFunc(planDetail.formulas.IRR, gteedYieldCaculation);
      totalNonGteedSurrenderYieldinPrecent325 = runFunc(planDetail.formulas.IRR, nongteedYieldCalculation325);
    }
    sumOfgteedAnnualCashbackWithoutCurrentCashBack = math.add(math.bignumber(sumOfgteedAnnualCashbackWithoutCurrentCashBack), gteedAnnualCashback); /**Set the previous value for next iteration use*/
    previousrbFA = rbFA;
    previousAccOnCashback = accOnCashback;
    previousGteedAnnualCashback = gteedAnnualCashback;
    previoustotalSVSupp = totalSVSupp;
    previousrbFA325 = rbFA325;
    previousAccOnCashback325 = accOnCashback325;
    previousGteedAnnualCashback325 = gteedAnnualCashback;
    var diffProjectRateIllustration = {
      'high': {},
      'low': {}
    };
    var indIllustration = {
      gteedAnnualCashback: math.number(gteedAnnualCashback || 0),
      tpdBasicPrem: math.number(tpdBasicPrem || 0),
      totDistCost: math.number(totDistCost || 0),
      gteedDBMain: math.number(gteedDBMain || 0),
      nongteedDBMain: math.number(nongteedDBMain || 0),
      nongteedDBSupp: math.number(nongteedDBSupp || 0),
      gteedSVMain: math.number(gteedSVMain || 0),
      nongteedSVMain: math.number(nongteedSVMain || 0),
      nongteedSVSupp: math.number(nongteedSVSupp || 0),
      totalNonGteedSurrenderYieldinPrecent: math.number(totalNonGteedSurrenderYieldinPrecent || 0),
      totalgteedSurrenderYieldinPrecent: math.number(totalgteedSurrenderYieldinPrecent || 0),
      accOnCashback: math.number(accOnCashback || 0),
      vopPtd: vopPtd475 || 0,
      accumCashAdv: accumCashAdv || 0,
      effectOfDeduction: math.number(effectOfDeduction || 0),
      netCFforAccum: math.number(netCFforAccum || 0),
      negativePremiumForPHyield: negativePremiumForPHyield || 0,
      totalSVSupp: totalSVSupp || 0
    };
    var indIllustration325 = {
      gteedAnnualCashback: math.number(gteedAnnualCashback || 0),
      tpdBasicPrem: math.number(tpdBasicPrem || 0),
      totDistCost: math.number(totDistCost || 0),
      gteedDBMain: math.number(gteedDBMain || 0),
      gteedDBSupp: math.number(gteedDBSupp325 || 0),
      nongteedDBMain: math.number(nongteedDBMain325 || 0),
      nongteedDBSupp: math.number(nongteedDBSupp325 || 0),
      gteedSVMain: math.number(gteedSVMain || 0),
      nongteedSVMain: math.number(nongteedSVMain325 || 0),
      nongteedSVSupp: math.number(nongteedSVSupp325 || 0),
      totalNonGteedSurrenderYieldinPrecent: math.number(totalNonGteedSurrenderYieldinPrecent325 || 0),
      totalgteedSurrenderYieldinPrecent: 0,
      accOnCashback: math.number(accOnCashback325 || 0),
      gsvSupp: math.number(gsvSupp || 0),
      vopPtd: vopPtd325 || 0,
      accumCashAdv: accumCashAdv325 || 0,
      effectOfDeduction: math.number(effectOfDeduction325) || 0
    };
    diffProjectRateIllustration['high'] = indIllustration;
    diffProjectRateIllustration['low'] = indIllustration325;
    illustration.push(diffProjectRateIllustration);
  }
  var extraNetCFforAccum = math.add(illustration[basic_BT - 1].high.negativePremiumForPHyield, numberTrunc(illustration[basic_BT - 1].high.totalSVSupp, 2));
  if (illustration.length > 1) {
    illustration[illustration.length - 1].extraNetCFforAccum = extraNetCFforAccum;
  }
  return illustration;
}