function(quotation, planInfo, planDetails, extraPara) {
  var basicPlan = quotation.plans[0];
  var company = extraPara.company;
  var illust = extraPara.illustrations[planInfo.covCode];
  var retireSol = illust[0].retirementSolution;
  var accumulated = retireSol.accumulated;
  var paidOut = retireSol.paidOut;
  var annualPremium = quotation.plans[0].yearPrem + quotation.plans[1].yearPrem;

  function stripDecimals(n) {
    return n | 0;
  };
  var getNonRoundingNumbers = function(value) {
    return stripDecimals(value * 100) / 100;
  };
  var getGenderDesc = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getSmokingDesc = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  var quickQuote = 'N';
  var gurAnnualRetirementIncome = illust[0].gteedAnnualRetirementIncome[planInfo.covCode];
  var TPD_multiplier = gurAnnualRetirementIncome * 5;
  var payoutTermDesc = '';
  var payoutTypeDesc = '';
  var paymentTermDesc = '';
  var paymentModeDesc = '';
  var aPremiumTotalAck = 0;
  var sPremiumTotalAck = 0;
  var qPremiumTotalAck = 0;
  var mPremiumTotalAck = 0;
  var hasOptRiders = 0;
  var hasPetRider = 0;
  var temp;
  temp = parseInt(quotation.policyOptions.payoutTerm);
  if (temp == 99) {
    payoutTermDesc = "Lifetime (Till age 99)";
  } else {
    payoutTermDesc = quotation.policyOptions.payoutTerm + ' Years';
  }
  if (quotation.policyOptions.payoutType === 'Level') {
    payoutTypeDesc = 'Level';
  } else {
    payoutTypeDesc = 'Inflated at 3.50% p.a.';
  }
  if (quotation.premTerm == 1) {
    paymentTermDesc = 'Single Premium';
    paymentModeDesc = 'Single';
  } else {
    paymentTermDesc = quotation.premTerm + ' Years';
    switch (quotation.paymentMode) {
      case 'A':
        {
          paymentModeDesc = 'Annual';
          break;
        }
      case 'S':
        {
          paymentModeDesc = 'Semi-Annual';
          break;
        }
      case 'Q':
        {
          paymentModeDesc = 'Quarterly';
          break;
        }
      case 'M':
        {
          paymentModeDesc = 'Monthly';
          break;
        }
      default:
        {
          break;
        }
    }
  }
  if (quotation.plans.length > 2) {
    hasOptRiders = 1;
    if (quotation.plans[2].covCode === 'PET_RHP') {
      hasPetRider = 1;
    }
  }
  var plans = [];
  var plansAck = []; /** Will not include optional riders*/
  var index;
  for (index = 0; index < quotation.plans.length; index++) {
    var curPlan = quotation.plans[index];
    if (index == 0) { /** This is always the basic plan*/
      aPremiumTotalAck += curPlan.yearPrem;
      sPremiumTotalAck += curPlan.halfYearPrem;
      qPremiumTotalAck += curPlan.quarterPrem;
      mPremiumTotalAck += curPlan.monthPrem;
      plans.push({
        name: curPlan.covName.en + ' (' + quotation.policyOptions.payoutType + ')',
        polTermDesc: curPlan.polTermDesc,
        premTermDesc: curPlan.premTermDesc,
        retireIncome: getCurrency(Math.round(gurAnnualRetirementIncome), '', 0),
        aPremium: getCurrency(curPlan.yearPrem, '', 2),
        sPremium: getCurrency(curPlan.halfYearPrem, '', 2),
        qPremium: getCurrency(curPlan.quarterPrem, '', 2),
        mPremium: getCurrency(curPlan.monthPrem, '', 2),
        singlePremium: getCurrency(curPlan.singlePrem, '', 2)
      });
      plansAck.push({
        name: curPlan.covName.en + ' (' + quotation.policyOptions.payoutType + ')',
        polTermDesc: curPlan.polTermDesc,
        premTermDesc: curPlan.premTermDesc,
        retireIncome: getCurrency(Math.round(gurAnnualRetirementIncome), '', 0),
        aPremium: getCurrency(curPlan.yearPrem, '', 2),
        sPremium: getCurrency(curPlan.halfYearPrem, '', 2),
        qPremium: getCurrency(curPlan.quarterPrem, '', 2),
        mPremium: getCurrency(curPlan.monthPrem, '', 2),
        singlePremium: getCurrency(curPlan.singlePrem, '', 2)
      });
    } else if (index == 1) { /** This is always the TPD rider*/
      aPremiumTotalAck += curPlan.yearPrem;
      sPremiumTotalAck += curPlan.halfYearPrem;
      qPremiumTotalAck += curPlan.quarterPrem;
      mPremiumTotalAck += curPlan.monthPrem;
      plans.push({
        name: curPlan.covName.en,
        polTermDesc: curPlan.polTermDesc,
        premTermDesc: curPlan.premTermDesc,
        retireIncome: getCurrency(Math.round(TPD_multiplier), '', 0),
        aPremium: getCurrency(curPlan.yearPrem, '', 2),
        sPremium: getCurrency(curPlan.halfYearPrem, '', 2),
        qPremium: getCurrency(curPlan.quarterPrem, '', 2),
        mPremium: getCurrency(curPlan.monthPrem, '', 2),
        singlePremium: getCurrency(curPlan.singlePrem, '', 2)
      });
      plansAck.push({
        name: curPlan.covName.en,
        polTermDesc: curPlan.polTermDesc,
        premTermDesc: curPlan.premTermDesc,
        retireIncome: getCurrency(Math.round(TPD_multiplier), '', 0),
        aPremium: getCurrency(curPlan.yearPrem, '', 2),
        sPremium: getCurrency(curPlan.halfYearPrem, '', 2),
        qPremium: getCurrency(curPlan.quarterPrem, '', 2),
        mPremium: getCurrency(curPlan.monthPrem, '', 2),
        singlePremium: getCurrency(curPlan.singlePrem, '', 2)
      });
    } else {
      plans.push({
        name: curPlan.covName.en,
        polTermDesc: curPlan.polTermDesc,
        premTermDesc: curPlan.premTermDesc,
        aPremium: getCurrency(curPlan.yearPrem, '', 2),
        sPremium: getCurrency(curPlan.halfYearPrem, '', 2),
        qPremium: getCurrency(curPlan.quarterPrem, '', 2),
        mPremium: getCurrency(curPlan.monthPrem, '', 2),
        singlePremium: getCurrency(curPlan.singlePrem, '', 2)
      });
    }
  }
  var singlePremTotal = 0;
  if (quotation.premTerm == 1) {
    singlePremTotal = quotation.totSinglePrem;
  }
  var planCodes = basicPlan.planCode + ', ' + quotation.plans[1].planCode;
  if (hasOptRiders > 0) {
    planCodes += ', ' + quotation.plans[2].planCode;
  }
  planCodes += ' ';
  var reportData = {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      planCode: planCodes,
      sumAssured: quotation.sumInsured,
      releaseVersion: "1"
    },
    cover: {
      sameAs: quotation.sameAs,
      proposer: {
        name: quotation.pFullName,
        gender: getGenderDesc(quotation.pGender),
        dob: new Date(quotation.pDob).format(extraPara.dateFormat),
        age: quotation.pAge,
        smoking: getSmokingDesc(quotation.pSmoke)
      },
      insured: {
        name: quotation.iFullName,
        gender: getGenderDesc(quotation.iGender),
        dob: new Date(quotation.iDob).format(extraPara.dateFormat),
        age: quotation.iAge,
        smoking: getSmokingDesc(quotation.iSmoke)
      },
      retireSol: {
        poPremiums: getCurrency(paidOut.totalPremiumsPaid, '', 2),
        poGRetireInc: getCurrency(paidOut.totalGuranteedRetireInc, '', 2),
        poNGRetireInc: getCurrency(paidOut.nonGuranteedRetireInc, '', 2),
        poPayout: getCurrency(paidOut.totalProjectedPayout, '', 2),
        poPayoutRatio: Math.round(paidOut.totalProjectedPayoutRatio * 100),
        poGR: getCurrency(Math.round(paidOut.guranteedRateOfReturn * 100 * 100) / 100, '', 2),
        poROR: getCurrency(Math.round(paidOut.totalRateOfReturn * 100 * 100) / 100, '', 2),
        acPremiums: getCurrency(accumulated.totalPremiumsPaid, '', 2),
        acGRetireInc: getCurrency(accumulated.totalGuranteedRetireInc, '', 2),
        acNGRetireInc: getCurrency(accumulated.nonGuranteedRetireInc, '', 2),
        acPayout: getCurrency(accumulated.totalProjectedPayout, '', 2),
        acPayoutRatio: Math.round(accumulated.totalProjectedPayoutRatio * 100),
        acROR: getCurrency(Math.round(accumulated.totalRateOfReturn * 100 * 100) / 100, '', 2)
      },
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      premTerm: quotation.premTerm,
      plans: plans,
      plansAck: plansAck
    },
    common: {
      retirementAge: quotation.policyOptions.retirementAge,
      retirementInc: getCurrency(gurAnnualRetirementIncome, '', 2),
      payoutTerm: payoutTermDesc,
      payoutType: payoutTypeDesc,
      annualPremium: getCurrency(annualPremium, '', 2),
      paymentTerm: paymentTermDesc,
      paymentMode: paymentModeDesc,
      polCurrency: 'Singapore Dollars'
    },
    basicPlan: {
      name: basicPlan.covName.en,
      sumInsured: getCurrency(basicPlan.sumInsured, '', 2),
      premium: getCurrency(basicPlan.premium, '', 2),
      aPremium: getCurrency(basicPlan.yearPrem, '', 2),
      sPremium: getCurrency(basicPlan.halfYearPrem, '', 2),
      qPremium: getCurrency(basicPlan.quarterPrem, '', 2),
      mPremium: getCurrency(basicPlan.monthPrem, '', 2),
      aPremiumTotal: getCurrency(quotation.totYearPrem, '', 2),
      sPremiumTotal: getCurrency(quotation.totHalfyearPrem, '', 2),
      qPremiumTotal: getCurrency(quotation.totQuarterPrem, '', 2),
      mPremiumTotal: getCurrency(quotation.totMonthPrem, '', 2),
      aPremiumTotalAck: getCurrency(aPremiumTotalAck, '', 2),
      sPremiumTotalAck: getCurrency(sPremiumTotalAck, '', 2),
      qPremiumTotalAck: getCurrency(qPremiumTotalAck, '', 2),
      mPremiumTotalAck: getCurrency(mPremiumTotalAck, '', 2),
      singlePremTotal: getCurrency(singlePremTotal, '', 2),
      hasOptRiders: hasOptRiders,
      hasPetRider: hasPetRider,
      paymentModeDesc: '',
      ccy: quotation.ccy,
      ccySymbol: 'SGD'
    }
  };
  return reportData;
}