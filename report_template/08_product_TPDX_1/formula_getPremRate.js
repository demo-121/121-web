function(quotation, planInfo, planDetail, age) {
  var rateKey = planInfo.planCode + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS');
  return planDetail.rates.premRate[rateKey][quotation.iAge];
}