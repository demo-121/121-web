function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  if (planInfo.premium) {
    if (planInfo.policyTerm.indexOf('YR') > -1) {
      planInfo.planCode = Number.parseInt(planInfo.policyTerm) + 'HER';
      planInfo.policyTermYr = Number.parseInt(planInfo.policyTerm);
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      planInfo.planCode = 'HER' + Number.parseInt(planInfo.policyTerm);
      planInfo.policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
  } else {
    planInfo.planCode = null;
  }
}