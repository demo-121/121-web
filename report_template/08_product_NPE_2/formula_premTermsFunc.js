function(planDetail, quotation) {
  var premTermsList = [];
  var resetInputs = function() {
    quotation.plans[0].premTerm = undefined; /** force UI to be blank*/
    quotation.plans[0].incomePayout = undefined; /** force UI to be blank*/
    quotation.plans[0].premium = undefined; /** force UI to be blank*/
    quotation.prevPaymentMode = quotation.paymentMode;
    quotation.policyOptions.accumulationPeriod = undefined;
    quotation.annualPremium = undefined;
  };
  if (quotation.prevPaymentMode) { /** Change from RP to SP or vice versa*/
    if (quotation.prevPaymentMode === 'L') {
      if (quotation.paymentMode !== 'L') {
        resetInputs();
      }
    } else {
      if (quotation.paymentMode === 'L') {
        resetInputs();
      }
    }
  }
  if (quotation.paymentMode == 'L') {
    if (quotation.plans[0].premTerm) {
      premTermsList.push({
        value: 1,
        title: 'Single Premium',
        default: true
      });
    } else {
      premTermsList.push({
        value: 1,
        title: 'Single Premium'
      });
    }
  } else {
    _.forEach([5, 10, 15, 20, 25, 30], (paymentTerm) => {
      if (70 - quotation.iAge >= paymentTerm) {
        premTermsList.push({
          value: paymentTerm,
          title: paymentTerm + ' Years'
        });
      }
    });
  }
  return premTermsList;
}