function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation, planDetails);
  if (quotation.paymentMode !== 'L') {
    planDetail.inputConfig.planInfoHintMsg_othSa = 'Minimum Guaranteed Annual Income is S$6,000';
  } else {
    planDetail.inputConfig.planInfoHintMsg_othSa = 'Minimum Guaranteed Annual Income is S$1,500';
  }
}