function(planDetail, quotation, planDetails) {
  quotDriver.preparePolicyOptions(planDetail, quotation, planDetails);
  var policyOptions = _.cloneDeep(planDetail.policyOptions);
  var updNumPol = planDetail.formulas.updNumPol;
  var qpos = quotation.policyOptions;
  var pgm = {
    "depos": "depositOption"
  };
  var checkNumMandatory = function(_po, _poId) {
    return _poId == _po.id && _po.mandatory === "Y";
  };
  for (var i = policyOptions.length - 1; i >= 0; i--) {
    var po = policyOptions[i],
      pgId = po.groupID,
      poId = po.id,
      pv = qpos[poId];
    if ("depositInput" == poId || "depositOption" == poId) {
      if (pgm[pgId] && poId != pgm[pgId]) {
        po.disable = (!qpos[pgm[pgId]]) ? "Y" : "N";
        po.mandatory = po.type != "emptyBlock" && qpos[pgm[pgId]] ? "Y" : "N";
        if (po.disable == "Y" && po.type == "text") {
          policyOptions.pop();
        }
      }
      if (checkNumMandatory(po, "depositInput")) {
        var prTerm = quotation.plans[0].premTerm;
        if (prTerm === undefined || prTerm === null || prTerm === '') {
          prTerm = 0;
        } else {
          prTerm = parseInt(prTerm);
        }
        var accumuPeriod = 0;
        if (quotation.paymentMode === 'L' || prTerm <= 10) {
          accumuPeriod = 5;
        }
        var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
        var max = 120;
        if (payoutTerm === 120) {
          max = 120 - quotation.iAge - prTerm - accumuPeriod;
        } else {
          max = payoutTerm;
        }
        if (isNaN(pv) && quotation.policyOptions.depositInput !== undefined && quotation.policyOptions.depositInput !== null && quotation.policyOptions.depositInput !== '') {
          pv = 0;
        }
        if (isNaN(max)) {
          max = 99;
        }
        po.hintMsg = 'Minimum value is 1. Max value is ' + max + '.';
        qpos[poId] = quotDriver.runFunc(updNumPol, po, pv, 1, max);
      }
    } else if ("accumulationPeriod" == poId) { /** update Accumulation Period */
      var premTerm = parseInt(quotation.plans[0].premTerm);
      if (quotation.plans[0].premTerm === undefined || quotation.policyOptions.payoutTerm === undefined) {
        quotation.policyOptions.accumulationPeriod = '';
      } else if (quotation.paymentMode === 'L' || premTerm === 5 || premTerm === 10) {
        quotation.policyOptions.accumulationPeriod = '5 Years';
      } else {
        quotation.policyOptions.accumulationPeriod = '0 Years';
      }
    } else if ("paymentMethod" == poId) { /** init payment method */
      if (!quotation.policyOptions.paymentMethod) {
        quotation.policyOptions.paymentMethod = 'cash';
      } /** if not single premium, remove srs in payment type */
      if (quotation.paymentMode != 'L') {
        policyOptions[0].options.splice(1, 1);
        if (quotation.policyOptions.paymentMethod === 'srs') {
          quotation.policyOptions.paymentMethod = 'cash';
        }
      } else {
        if (quotation.sameAs != 'Y') {
          policyOptions[0].options.splice(1, 1);
          if (quotation.policyOptions.paymentMethod === 'srs') {
            quotation.policyOptions.paymentMethod = 'cash';
          }
        }
      }
    } else if ("payoutTerm" == poId) { /** update payoutTerm */
      var payTerm = parseInt(quotation.policyOptions.payoutTerm);
      var prTerm = quotation.plans[0].premTerm;
      var tTermValue = '';
      if (prTerm === undefined || prTerm === null || prTerm === '') {
        prTerm = 0;
      } else {
        prTerm = parseInt(prTerm);
      }
      var accumuPeriod = 0;
      if (quotation.paymentMode === 'L' || prTerm <= 10) {
        accumuPeriod = 5;
      }
      if (payTerm == 120) {
        payTerm = payTerm - quotation.iAge - prTerm - accumuPeriod;
        tTermValue = payTerm + ' Years';
      } else {
        tTermValue = payTerm + ' Years';
      }
      quotation.policyOptions["payoutTermDesc"] = tTermValue;
    }
  } /** update default value _.forEach(policyOptions, po=>{ po.value = _.get(po, 'options[0].value', ''); }); */
  planDetail.inputConfig.policyOptions = policyOptions;
}