function(quotation, planDetail, planDetails) {
  quotDriver.prepareQuotConfigs(quotation, planDetail, planDetails);
  var payModes = [];
  _.each(planDetail.inputConfig.payModes, (payMode) => {
    if (quotation.iAge > 65) {
      if (payMode.mode === 'L') {
        payMode.default = "Y";
        payModes.push(payMode);
      }
    } else {
      payModes.push(payMode);
    } /** Warning on change only if quotation.plans[0].premTerm already defined*/
    if ((payMode.mode === quotation.paymentMode) && (quotation.plans[0].premTerm)) {
      payMode.onChange = {
        to: payMode.mode === 'L' ? ['A', 'S', 'Q', 'M'] : 'L',
        action: 'resetQuot',
        params: {
          keepPolicyOptions: true
        }
      };
    }
  });
  if (payModes.length == 1) {
    quotation.paymentMode = "L";
  }
  planDetail.inputConfig.payModes = payModes;
}