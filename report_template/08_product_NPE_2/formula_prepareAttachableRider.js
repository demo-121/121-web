function(planDetail, quotation, planDetails) {
  var covCodePET = 'PET_NPE';
  var covCodeUNBN = 'UNBN_NPE';
  var covCodeWUN = 'WUN_NPE';
  var riders = [];
  var index;
  var max_index = planDetail.riderList.length;
  var curRider; /** Student, Housewife, Retiree, Unemployed*/
  var excOccupation = ['O1321', 'O1322', 'O675', 'O1132', 'O1450'];
  var isExcludedOccupation = function(occupation) {
    var i;
    for (i = 0; i < excOccupation.length; i++) {
      if (occupation === excOccupation[i]) {
        return true;
      }
    }
    return false;
  };
  if (quotation.plans.length <= 1) { /** Default basic plan */
    for (index = 0; index < max_index; index++) {
      curRider = planDetail.riderList[index];
      if (quotation.sameAs === 'Y') { /** First party*/
        if (curRider.covCode === covCodePET) {
          riders.push(curRider);
        }
        if ((curRider.covCode === covCodeUNBN) || (curRider.covCode === covCodeWUN)) {
          if (!isExcludedOccupation(quotation.iOccupation)) {
            riders.push(curRider);
          }
        }
      }
    }
    planDetail.riderList = riders;
    quotDriver.prepareAttachableRider(planDetail, quotation, planDetails);
  } else { /** Only one optional rider can be added at a time.*/
    var riderCovCode = quotation.plans[1].covCode; /** Remove riders in planDetail that is not covCodeTPD or riderCovCode*/ /* for (index = 0; index < max_index; index++){ var curRider = planDetail.riderList[index]; if (curRider.covCode === covCodeTPD){riders.push(curRider);} if (curRider.covCode === riderCovCode){riders.push(curRider);} if (riders.length == 2){ planDetail.riderList = riders; break; } }*/
    quotDriver.prepareAttachableRider(planDetail, quotation, planDetails);
  }
}