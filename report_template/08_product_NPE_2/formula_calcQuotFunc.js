function(quotation, planInfo, planDetails) {
  const PLAN_TYPE_BASIC = 0;
  const PLAN_TYPE_RIDER_TPD = 1;
  const CALC_TYPE_INPUT_PREMIUM = 1;
  const CALC_TYPE_INPUT_PAYOUT_INC = 2;
  const MINIMUM_RETIREMENT_INCOME_RP = 6000;
  const MINIMUM_RETIREMENT_INCOME_SP = 1500;
  var round = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };

  function calculateBasicPlan() {
    if (quotation.plans[0].premTerm === undefined || quotation.plans[0].premTerm === '' || quotation.plans[0].premTerm === null) {
      return;
    }
    if (quotation.policyOptions.payoutTerm === undefined || quotation.policyOptions.payoutTerm === '' || quotation.policyOptions.payoutTerm === null) {
      return;
    }
    var currentPlan = planDetails[planInfo.covCode];
    var rates = currentPlan.rates;
    var commModalFactor = rates.commModalFactor;
    var GTD_FACTOR_RATE = rates.GTD_FACTOR_RATE;
    var PREM_RATE = rates.PREM_RATE;
    var modalFactor = 0;
    var Gtd_Payout_Factor = 0;
    var premRate = 0;
    var result = null;
    var sumAssured = 0;
    var premiumTerm = parseInt(quotation.plans[0].premTerm);
    var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
    var planCodeBasic = '';
    var tempPayoutTerm = payoutTerm;
    if (payoutTerm < 100) {
      tempPayoutTerm = '0' + payoutTerm;
    }
    if (premiumTerm < 10) {
      planCodeBasic = '0' + premiumTerm + 'FE' + tempPayoutTerm;
    } else {
      planCodeBasic = premiumTerm + 'FE' + tempPayoutTerm;
    }
    var ref = planCodeBasic + quotation.iGender + quotation.iSmoke;
    var getRate = function(ref, insuredAge, Rates) {
      var planRates = Rates[ref];
      if (planRates === null) {
        return 0;
      }
      return planRates[insuredAge];
    };
    var getModalFactor = function() {
      switch (quotation.paymentMode) {
        case 'A':
          {
            return commModalFactor.annual;
          }
        case 'S':
          {
            return commModalFactor.semiAnnual;
          }
        case 'Q':
          {
            return commModalFactor.quarterly;
          }
        case 'M':
          {
            return commModalFactor.monthly;
          }
        default:
          {
            break;
          }
      }
      return commModalFactor.annual;
    };
    premRate = getRate(ref, quotation.iAge, PREM_RATE);
    Gtd_Payout_Factor = getRate(ref, quotation.iAge, GTD_FACTOR_RATE);
    modalFactor = getModalFactor(); /** Will return true if current inputs are same with previous inputs*/
    var checkIfSameInputs = function() {
      if (quotation.prevPaymentMode !== quotation.paymentMode) {
        return false;
      }
      if (Math.floor(quotation.prevPremium * 100) !== Math.floor(planInfo.premium * 100)) {
        return false;
      }
      if (Math.floor(quotation.previncomePayout * 100) !== Math.floor(planInfo.incomePayout * 100)) {
        return false;
      }
      return true;
    };
    var resetInputs = function() {
      quotation.plans[0].premTerm = undefined; /** force UI to be blank*/
      planInfo.incomePayout = NaN; /** force UI to be blank*/
      planInfo.premium = NaN; /** force UI to be blank*/
      quotation.prevPaymentMode = quotation.paymentMode;
    };
    var calculatePremiumFromIncomePayout = function(incomePayout) {
      var temp;
      var tempincomePayout;
      var basicSumAssured = math.divide(math.bignumber(incomePayout), math.bignumber(Gtd_Payout_Factor)); /** incomePayout / Gtd_Payout_Factor */
      var roundedBSA;
      var annualPremium;
      var regularPremium;
      temp = Math.ceil(math.number(trunc(basicSumAssured, 2)) / 100) * 100; /* Math.ceil(basicSumAssured/100) * 100; To make multiple by 100*/
      roundedBSA = temp;
      tempincomePayout = math.multiply(roundedBSA, math.bignumber(Gtd_Payout_Factor)); /** roundedBSA * Gtd_Payout_Factor */
      tempincomePayout = round(tempincomePayout, 2); /* compute annualPremium*/
      temp = math.divide(math.multiply(math.bignumber(premRate), roundedBSA), math.bignumber(1000)); /** premRate * roundedBSA /1000 */
      temp = trunc(temp, 2); /* Math.floor(temp * 100)/100; TRUNCATE to 2 decimal places*/ /** temp = temp/1000; temp = Math.round(temp * 100)/100; ROUND to 2 decimal places*/
      annualPremium = temp; /* compute annualPremium*/
      temp = math.multiply(annualPremium, math.bignumber(modalFactor));
      temp = trunc(temp, 2); /* Math.floor(temp * 100)/100; ROUNDDOWN to 2 decimal places*/
      regularPremium = temp;
      return {
        basicSumAssured: basicSumAssured,
        roundedBSA: roundedBSA,
        incomePayout: tempincomePayout,
        annualPremium: annualPremium,
        regularPremium: regularPremium
      };
    };
    var calculateIncomePayoutFromPremium = function(regularPremium) {
      var temp;
      var annualPremium = math.divide(math.bignumber(regularPremium), math.bignumber(modalFactor)); /**regularPremium / modalFactor;*/
      var roundedBSA;
      var incomePayout;
      var apAfterRoundedBSA;
      var rpAfterRoundedBSA;
      var parPremium;
      temp = math.multiply(math.divide(annualPremium, math.bignumber(premRate)), math.bignumber(1000)); /**annualPremium / premRate * 1000;*/
      roundedBSA = Math.ceil(math.number(trunc(temp, 2)) / 100) * 100;
      incomePayout = math.multiply(roundedBSA, math.bignumber(Gtd_Payout_Factor));
      incomePayout = round(incomePayout, 2); /**Math.round(incomePayout*100)/100;*/ /* compute annualPremium after RoundedBSA*/
      temp = math.divide(math.multiply(math.bignumber(premRate), roundedBSA), math.bignumber(1000)); /**premRate * roundedBSA /1000;*/
      temp = trunc(temp, 2); /* Math.floor(temp * 100)/100; TRUNCATE to 2 decimal places*/ /** temp = temp/1000.0; temp = Math.round(temp * 1000)/1000; ROUND to 3 decimal places temp = Math.round(temp * 100)/100; ROUND to 2 decimal places*/
      apAfterRoundedBSA = temp; /* compute regularPremium after RoundedBSA*/
      temp = math.multiply(apAfterRoundedBSA, math.bignumber(modalFactor)); /**apAfterRoundedBSA * modalFactor;*/
      temp = trunc(temp, 2); /* Math.floor(temp * 100)/100;ROUNDDOWN to 2 decimal places*/
      rpAfterRoundedBSA = temp;
      return {
        annualPremium: annualPremium,
        roundedBSA: roundedBSA,
        incomePayout: incomePayout,
        apAfterRoundedBSA: apAfterRoundedBSA,
        rpAfterRoundedBSA: rpAfterRoundedBSA
      };
    };
    var calcType = 0;
    var isSamePremiumInput = false;
    var isSameRetireIncInput = false; /** Check if there is premTerm*/
    var curPlanDetails = planDetails[planInfo.covCode];
    if (quotation.plans[0].premTerm > 0) { /** Enable RI and premium input*/
      curPlanDetails.inputConfig.canEditPremium = true;
      curPlanDetails.inputConfig.canEditOthSa = true;
    } else { /** Disable RI and premium input*/
      curPlanDetails.inputConfig.canEditPremium = false;
      curPlanDetails.inputConfig.canEditOthSa = false;
    }
    if ((planInfo.premium === undefined) && (planInfo.incomePayout === undefined)) {
      quotation.prevPaymentMode = quotation.paymentMode;
      return;
    }
    if (planInfo.premium === undefined) {
      planInfo.premium = 0;
      calcType = CALC_TYPE_INPUT_PAYOUT_INC;
    }
    if (planInfo.incomePayout === undefined) {
      planInfo.incomePayout = 0;
      calcType = CALC_TYPE_INPUT_PREMIUM;
    }
    if ((planInfo.premium == 0) && (planInfo.incomePayout == 0)) {
      quotation.prevPaymentMode = quotation.paymentMode;
      return;
    }
    if (quotation.prevPremium === undefined) {
      quotation.prevPremium = 0;
    }
    if (quotation.previncomePayout === undefined) {
      quotation.previncomePayout = 0;
    }
    if ((planInfo.premium == null) && (planInfo.incomePayout == 0)) {
      planInfo.incomePayout = NaN; /** force UI to be blank*/
      quotation.prevPaymentMode = quotation.paymentMode;
      return;
    }
    if ((planInfo.premium == null) && (planInfo.incomePayout == null)) {
      planInfo.incomePayout = NaN; /** force UI to be blank*/
      quotation.prevPaymentMode = quotation.paymentMode;
      return;
    }
    if (quotation.prevPaymentMode) { /** Change from RP to SP or vice versa*/
      if (quotation.prevPaymentMode === 'L') {
        if (quotation.paymentMode !== 'L') {
          resetInputs();
          quotation.prevPaymentMode = quotation.paymentMode;
          return;
        }
      } else {
        if (quotation.paymentMode === 'L') {
          resetInputs();
          quotation.prevPaymentMode = quotation.paymentMode;
          return;
        }
      }
    } /** compare money as integers*/
    var premium_1 = Math.floor(planInfo.premium * 100);
    var premium_2 = Math.floor(quotation.prevPremium * 100);
    var retire_1 = Math.floor(planInfo.incomePayout * 100);
    var retire_2 = Math.floor(quotation.previncomePayout * 100);
    if (checkIfSameInputs()) {
      calcType = quotation.prevCalcType;
      if (quotation.prevCalcType == CALC_TYPE_INPUT_PREMIUM) {
        isSamePremiumInput = true;
      } else {
        isSameRetireIncInput = true;
      }
    } else {
      if (planInfo.incomePayout) { /**Auto adjust retirementIncome if regular premium*/
        if (quotation.paymentMode !== 'L') {
          if (planInfo.incomePayout < MINIMUM_RETIREMENT_INCOME_RP) {
            planInfo.incomePayout = MINIMUM_RETIREMENT_INCOME_RP;
            retire_1 = planInfo.incomePayout * 100;
          }
        } else {
          if (planInfo.incomePayout < MINIMUM_RETIREMENT_INCOME_SP) {
            planInfo.incomePayout = MINIMUM_RETIREMENT_INCOME_SP;
            retire_1 = planInfo.incomePayout * 100;
          }
        }
      }
      if (calcType == 0) {
        if ((quotation.prevPremInput === undefined) && (quotation.prevCalcType === undefined) && (planInfo.premium > 0)) {
          calcType = CALC_TYPE_INPUT_PREMIUM;
          isSamePremiumInput = false;
        } else {
          if (quotation.prevCalcType == CALC_TYPE_INPUT_PREMIUM) {
            if (premium_1 == premium_2) {
              if (retire_1 != retire_2) {
                calcType = CALC_TYPE_INPUT_PAYOUT_INC;
              } else {
                calcType = CALC_TYPE_INPUT_PREMIUM;
                isSamePremiumInput = true;
              }
            } else {
              calcType = CALC_TYPE_INPUT_PREMIUM;
              isSamePremiumInput = false;
            }
          }
          if (quotation.prevCalcType == CALC_TYPE_INPUT_PAYOUT_INC) {
            if (retire_1 == retire_2) {
              if (premium_1 != premium_2) {
                calcType = CALC_TYPE_INPUT_PREMIUM;
              } else {
                calcType = CALC_TYPE_INPUT_PAYOUT_INC;
              }
            } else {
              calcType = CALC_TYPE_INPUT_PAYOUT_INC;
            }
          }
        }
      }
    }
    if (calcType == CALC_TYPE_INPUT_PREMIUM) { /** premium input change*/
      var inputPremium = isSamePremiumInput ? quotation.prevPremInput : planInfo.premium;
      result = calculateIncomePayoutFromPremium(inputPremium);
      sumAssured = math.number(result.roundedBSA);
      quotation.annualPremium = math.number(result.annualPremium);
      quotation.annualPremiumBSA = math.number(result.apAfterRoundedBSA);
      quotation.sumInsured = sumAssured;
      quotation.prevPremInput = inputPremium;
      quotation.previncomePayout = math.number(result.incomePayout);
      quotation.prevCalcType = CALC_TYPE_INPUT_PREMIUM;
      planInfo.incomePayout = math.number(result.incomePayout);
      planInfo.yearPrem = math.number(result.apAfterRoundedBSA);
      planInfo.halfYearPrem = math.number(trunc(math.multiply(math.bignumber(result.apAfterRoundedBSA), math.bignumber(0.51)), 2)); /** Math.floor(result.apAfterRoundedBSA * 0.51 * 100)/100;*/
      planInfo.quarterPrem = math.number(trunc(math.multiply(math.bignumber(result.apAfterRoundedBSA), math.bignumber(0.26)), 2)); /** Math.floor(result.apAfterRoundedBSA * 0.26 * 100)/100;*/
      planInfo.monthPrem = math.number(trunc(math.multiply(math.bignumber(result.apAfterRoundedBSA), math.bignumber(0.0875)), 2)); /** Math.floor(result.apAfterRoundedBSA * 0.0875 * 100)/100;*/
      quotation.totYearPrem = planInfo.yearPrem;
      quotation.totHalfyearPrem = planInfo.halfYearPrem;
      quotation.totQuarterPrem = planInfo.quarterPrem;
      quotation.totMonthPrem = planInfo.monthPrem;
      if (premiumTerm == 1) {
        planInfo.singlePrem = math.number(result.apAfterRoundedBSA);
        quotation.totSinglePrem = planInfo.singlePrem;
      }
      if (quotation.paymentMode === 'L') {
        if (planInfo.incomePayout < MINIMUM_RETIREMENT_INCOME_SP) {
          quotDriver.context.addError({
            covCode: quotation.baseProductCode,
            msg: 'Please increase the modal premium to meet minimum Guaranteed Annual Income of S$1,500'
          });
        }
      } else {
        if (planInfo.incomePayout < MINIMUM_RETIREMENT_INCOME_RP) {
          quotDriver.context.addError({
            covCode: quotation.baseProductCode,
            msg: 'Please increase the modal premium to meet minimum Guaranteed Annual Income of $6,000'
          });
        }
      }
    } else { /** retirement income input change*/
      var inputRetireInc = isSameRetireIncInput ? quotation.previncomePayout : planInfo.incomePayout;
      result = calculatePremiumFromIncomePayout(inputRetireInc);
      sumAssured = math.number(result.roundedBSA);
      quotation.sumInsured = sumAssured;
      planInfo.incomePayout = math.number(result.incomePayout);
      quotation.annualPremium = math.number(result.annualPremium);
      quotation.previncomePayout = math.number(result.incomePayout);
      quotation.prevCalcType = CALC_TYPE_INPUT_PAYOUT_INC;
      planInfo.yearPrem = math.number(result.annualPremium);
      planInfo.halfYearPrem = math.number(trunc(math.multiply(math.bignumber(result.annualPremium), math.bignumber(0.51)), 2)); /** Math.floor(result.annualPremium * 0.51 * 100)/100;*/
      planInfo.quarterPrem = math.number(trunc(math.multiply(math.bignumber(result.annualPremium), math.bignumber(0.26)), 2)); /** Math.floor(result.annualPremium * 0.26 * 100)/100;*/
      planInfo.monthPrem = math.number(trunc(math.multiply(math.bignumber(result.annualPremium), math.bignumber(0.0875)), 2)); /** Math.floor(result.annualPremium * 0.0875 * 100)/100;*/
      quotation.totYearPrem = planInfo.yearPrem;
      quotation.totHalfyearPrem = planInfo.halfYearPrem;
      quotation.totQuarterPrem = planInfo.quarterPrem;
      quotation.totMonthPrem = planInfo.monthPrem;
      if (premiumTerm == 1) {
        planInfo.singlePrem = math.number(result.annualPremium);
        quotation.totSinglePrem = planInfo.singlePrem;
      }
    }
    switch (quotation.paymentMode) {
      case 'A':
        {
          planInfo.premium = planInfo.yearPrem;quotation.premium = planInfo.yearPrem;
          break;
        }
      case 'S':
        {
          planInfo.premium = planInfo.halfYearPrem;quotation.premium = planInfo.halfYearPrem;
          break;
        }
      case 'Q':
        {
          planInfo.premium = planInfo.quarterPrem;quotation.premium = planInfo.quarterPrem;
          break;
        }
      case 'M':
        {
          planInfo.premium = planInfo.monthPrem;quotation.premium = planInfo.monthPrem;
          break;
        }
      default:
        {
          planInfo.premium = planInfo.yearPrem;quotation.premium = planInfo.yearPrem;
          break;
        }
    }
    planInfo.planCode = planCodeBasic;
    planInfo.sumAssured = sumAssured;
    quotation.prevPaymentMode = quotation.paymentMode;
    quotation.prevPayoutTerm = payoutTerm;
    quotation.prevPaymentTerm = planInfo.premTerm;
    quotation.prevPremium = planInfo.premium;
  }
  calculateBasicPlan();
}