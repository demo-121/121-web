function(quotation, planDetails, extraPara, illustrations) {
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };
  var basicIllust = illustrations[quotation.baseProductCode];
  var maxAge = quotation.iAge + parseInt(quotation.plans[0].policyTerm);
  var maxIndex = basicIllust.findIndex(function(item) {
    return item.age === maxAge;
  });
  for (var i = basicIllust.length - 1; i > maxIndex; i--) {
    basicIllust.splice(i, 1);
  }
  if (quotation.plans.length > 1) {
    var ridePolicyTerm = parseInt(quotation.plans[1].policyTerm);
    if (ridePolicyTerm === 50) {
      ridePolicyTerm = 50 - quotation.iAge;
    }
    var riderIllust = illustrations[quotation.plans[1].covCode];
    for (var i = 0; i < ridePolicyTerm; i++) {
      var basicIllData = basicIllust[i];
      var riderIllData = riderIllust[i];
      basicIllData.suppRiderPremiumPaid = math.number(trunc(riderIllData.accumPremium, 0));
      basicIllData.suppTotalDistributionCost = math.number(trunc(riderIllData.tdc, 0));
    }
  }
  return illustrations;
}