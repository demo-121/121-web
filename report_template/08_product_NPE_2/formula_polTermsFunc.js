function(planDetail, quotation) {
  const PAYOUT_TERM_LIFETIME = 120;
  var policyTermList = [];
  if (quotation.policyOptions.payoutTerm === undefined) {
    return null;
  }
  if (quotation.policyOptions.accumulationPeriod === undefined) {
    return null;
  }
  if (quotation.plans[0].premTerm === undefined) {
    return null;
  }
  var premTerm = parseInt(quotation.plans[0].premTerm);
  var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
  var tempaccterm = quotation.policyOptions.accumulationPeriod;
  var arryaccterm = tempaccterm.split(' ');
  var accTerm = parseInt(arryaccterm[0]);
  var policyTerm = 0;
  var polTermDesc = '';
  if (payoutTerm === PAYOUT_TERM_LIFETIME) {
    payoutTerm = 120 - quotation.iAge;
    payoutTerm = payoutTerm - premTerm;
    payoutTerm = payoutTerm - accTerm;
  }
  policyTerm = premTerm + accTerm + payoutTerm;
  polTermDesc = policyTerm + ' Years'; /**Needed so that RHP policy term will appear in UI.*/
  quotation.plans[0].policyTerm = policyTerm;
  policyTermList.push({
    value: policyTerm,
    title: polTermDesc,
    default: true
  });
  return policyTermList;
}