function(quotation, planInfo, planDetails, extraPara) { /*test vul cover*/
  var basicPlan = quotation.plans[0];
  var hasSpTopUp = quotation.policyOptions.topUpSelect;
  var hasRspTopUp = quotation.policyOptions.rspSelect;
  var hasWithdrawal = quotation.policyOptions.wdSelect;
  var company = extraPara.company;
  var policyOption = quotation.policyOptions;
  var baseProductCode = quotation.baseProductCode;
  var productVersion = quotation.productVersion;
  var grossRate = quotation.policyOptions.grossInvRate;
  var charge = quotation.policyOptions.charge;
  var withdrawalFee = 100;
  var establishFee = '0.0%';
  var policyFee = '0.0%';
  var illust = extraPara.illustrations[planInfo.covCode];
  var row = illust[0];
  if (baseProductCode == 'PVTVUL') {
    withdrawalFee = 500;
    establishFee = '4.50%';
    policyFee = '1.75%';
  } else if (baseProductCode == 'PVLVUL') {
    withdrawalFee = 100;
    establishFee = '13.00%';
    policyFee = '0.80%';
  }
  if (isNaN(grossRate)) {
    grossRate = 0;
  }
  if (isNaN(charge)) {
    charge = 0;
  }
  var in_IRR = (grossRate - charge) + '.00%';
  var getGenderDesc = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getSmokingDesc = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  var getCurrencyDesc = function(value, decimals) {
    return quotation.ccy + ' ' + getCurrency(value, '', decimals);
  };
  var getCurrencyWdDesc = function(value, decimals) {
    return getCurrency(value, '', decimals) + '%';
  };
  var policyCcyDisplay;
  if (quotation.ccy === 'SGD') {
    policyCcyDisplay = 'Singapore Dollars';
  } else if (quotation.ccy === 'USD') {
    policyCcyDisplay = 'US Dollars';
  }
  var residencyCountryOptions, residencyCityOptions, iRCountry, iRCity, pRCountry, pRCity, occOptions;
  residencyCountryOptions = extraPara.optionsMap.residency.options;
  residencyCityOptions = extraPara.optionsMap.city.options;
  occOptions = extraPara.optionsMap.occupation.options;
  for (var v in residencyCountryOptions) {
    if (residencyCountryOptions[v].value === quotation.pResidence) {
      pRCountry = residencyCountryOptions[v].title.en;
    }
    if (residencyCountryOptions[v].value === quotation.iResidence) {
      iRCountry = residencyCountryOptions[v].title.en;
    }
  }
  var reportData = [];
  reportData = {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      releaseVersion: "1"
    },
    cover: {
      sameAs: quotation.sameAs,
      baseProductCode: baseProductCode,
      ccy: quotation.ccy,
      ccySymbol: quotation.ccy === 'SGD' ? 'S$' : (quotation.ccy === 'USD' ? 'US$' : '$'),
      polType: policyOption.policytype,
      withdrawalFee: getCurrency(withdrawalFee, '', 0),
      establishFee: establishFee,
      policyFee: policyFee,
      in_IRR: in_IRR,
      day1SVUI: getCurrency(row.day1SV.IRR_UI, '', 0),
      day1SVZero: getCurrency(row.day1SV.IRR_Zero, '', 0),
      day1SVLow: getCurrency(row.day1SV.IRR_Low, '', 0),
      day1SVHigh: getCurrency(row.day1SV.IRR_High, '', 0),
      endowAgeUI: row.endowAge.IRR_UI,
      endowAgeZero: row.endowAge.IRR_Zero,
      endowAgeLow: row.endowAge.IRR_Low,
      endowAgeHigh: row.endowAge.IRR_High,
      breakYearUI: row.breakYear.IRR_UI,
      breakYearZero: row.breakYear.IRR_Zero,
      breakYearLow: row.breakYear.IRR_Low,
      breakYearHigh: row.breakYear.IRR_High,
      proposer: {
        name: quotation.pFullName,
        gender: getGenderDesc(quotation.pGender),
        dob: new Date(quotation.pDob).format(extraPara.dateFormat),
        age: quotation.pAge,
        smoking: getSmokingDesc(quotation.pSmoke),
        country: quotation.pResidenceCountryName ? quotation.pResidenceCountryName : '',
        city: quotation.pResidenceCityName ? quotation.pResidenceCityName : ''
      },
      insured: {
        name: quotation.iFullName,
        gender: getGenderDesc(quotation.iGender),
        dob: new Date(quotation.iDob).format(extraPara.dateFormat),
        age: quotation.iAge,
        smoking: getSmokingDesc(quotation.iSmoke),
        country: quotation.iResidenceCountryName ? quotation.iResidenceCountryName : '',
        city: quotation.iResidenceCityName ? quotation.iResidenceCityName : ''
      },
      policyOptions: {
        numOfWD: policyOption.numOfWD ? policyOption.numOfWD : null,
        riskClassification: policyOption.riskClassification,
        riskClassificationDesc: quotation.policyOptionsDesc.riskClassification.en,
        extraMortality: policyOption.extraMortality,
        extraMortalityDesc: quotation.policyOptionsDesc.extraMortality.en,
        loading: policyOption.loading,
        loadingPeriod: quotation.policyOptionsDesc.loadingPeriod,
        riskClassification2: policyOption.riskClassification2,
        riskClassificationDesc2: quotation.policyOptionsDesc.riskClassification2.en,
        extraMortality2: policyOption.extraMortality2,
        extraMortality2Desc: quotation.policyOptionsDesc.extraMortality2.en,
        loading2: policyOption.loading2,
        loadingPeriod2: quotation.policyOptionsDesc.loadingPeriod2,
        withdrawalOption: policyOption.withdrawalOption,
        withdrawalOptionDesc: quotation.policyOptionsDesc.withdrawalOption.en,
        invRateForWD: policyOption.invRateForWD,
        startYr1: policyOption.startYr1,
        startYr2: policyOption.startYr2,
        startYr3: policyOption.startYr3,
        startYr4: policyOption.startYr4,
        startYr5: policyOption.startYr5,
        endYr1: policyOption.endYr1,
        endYr2: policyOption.endYr2,
        endYr3: policyOption.endYr3,
        endYr4: policyOption.endYr4,
        endYr5: policyOption.endYr5,
        wdAmt1: policyOption.wdAmt1,
        wdAmt2: policyOption.wdAmt2,
        wdAmt3: policyOption.wdAmt3,
        wdAmt4: policyOption.wdAmt4,
        wdAmt5: policyOption.wdAmt5
      },
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      plans: quotation.plans.map(function(plan) {
        return {
          name: plan.covName.en,
          polTermDesc: plan.polTermDesc,
          sumInsured: getCurrencyDesc(plan.sumInsured, 0),
          initPremium: getCurrencyDesc(plan.premium, 0)
        };
      })
    }
  };
  return reportData;
}