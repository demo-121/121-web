function(planDetail, sumInsured) {
  var lsdMap = [{
    sumInsured: 25000,
    lsd: 0
  }, {
    sumInsured: 50000,
    lsd: 0.08
  }, {
    sumInsured: 100000,
    lsd: 0.16
  }];
  var mapping = _.findLast(lsdMap, m => sumInsured >= m.sumInsured);
  if (mapping) {
    return mapping.lsd;
  }
  return 0;
}