function(planDetail, quotation, planDetails) {
  quotDriver.prepareClassConfig(planDetail, quotation, planDetails);
  if (!quotation.plans[0].covClass) {
    if (quotation.iOccupationClass != "DCL") {
      quotation.plans[0].covClass = quotation.iOccupationClass + "";
    } else {
      quotDriver.context.addError({
        code: "issued.occupation.declined",
        msg: 'Invalid Occupation',
        covCode: planDetail.conCode
      });
    }
  }
}