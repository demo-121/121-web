function(planDetail, quotation) {
  var policyTermList = [];
  if (quotation.plans[0].premTerm === undefined) {
    return null;
  }
  var premiumTerm = quotation.plans[0].premTerm;
  var policyTerm = 0;
  var polTermDesc = '';
  if (premiumTerm === 30) {
    policyTerm = 25;
    polTermDesc = policyTerm + ' Years';
  } else {
    policyTerm = premiumTerm;
    polTermDesc = policyTerm + ' Years';
  }
  policyTermList.push({
    value: policyTerm,
    title: polTermDesc,
    default: true
  });
  quotation.plans[1].policyTerm = policyTerm;
  return policyTermList;
}