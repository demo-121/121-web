function(quotation, planDetails, extraPara, illustrations) {
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };
  var basicIllust = illustrations[quotation.baseProductCode];
  var riderPlanInfo = quotation.plans.find(function(p) {
    return ['PET_RHP', 'UNBN_RHP', 'WUN_RHP'].indexOf(p.covCode) >= 0;
  });
  var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
  var retirementAge = parseInt(quotation.policyOptions.retirementAge);
  var maxAge = retirementAge + (payoutTerm === 99 ? 99 - retirementAge : payoutTerm);
  var maxIndex = basicIllust.findIndex(function(item) {
    return item.age === maxAge;
  });
  for (var i = basicIllust.length - 1; i > maxIndex; i--) {
    basicIllust.splice(i, 1);
  }
  if (riderPlanInfo) {
    var riderIllust = illustrations[riderPlanInfo.covCode];
    for (var i = 0; i < basicIllust.length; i++) {
      var basicIllData = basicIllust[i];
      var riderIllData = riderIllust[i];
      basicIllData.suppRiderPremiumPaid = math.number(trunc(riderIllData.accumPremium, 0));
      basicIllData.suppTotalDistributionCost = math.number(trunc(riderIllData.tdc, 0));
    }
  }
  return illustrations;
}