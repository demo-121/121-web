function(quotation, planInfo, planDetail) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var premium = null;
  var sumInsured = planInfo.sumInsured;
  if (planInfo.premTerm && sumInsured) {
    var countryCode = quotation.iResidence;
    var isCountryMatch = (countryCode === "R51" || countryCode === "R52" || countryCode === "R53" || countryCode === "R54");
    if (isCountryMatch) {
      var residenceLoading = math.bignumber(runFunc(planDetail.formulas.getResidenceLoading, quotation, planInfo, planDetail));
      var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
      var premBeforeLsd = roundDown(math.multiply(premRate, sumInsured, 0.001), 2);
      var lsd = 0;
      var premAfterLsd = math.add(premBeforeLsd, roundDown(math.multiply(sumInsured, 0.001, lsd), 2));
      premAfterLsd = math.add(premAfterLsd, roundDown(math.multiply(residenceLoading, sumInsured, 0.001), 2));
      if (quotation.iAge > 55) {
        premAfterLsd = math.bignumber(0);
      }
      var modalFactor = 1;
      for (var p in planDetail.payModes) {
        if (planDetail.payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetail.payModes[p].factor;
        }
      }
      var prem = roundDown(math.multiply(premAfterLsd, modalFactor), 2);
      premium = math.number(prem);
    } else {
      var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
      var premBeforeLsd = roundDown(math.multiply(premRate, sumInsured, 0.001), 2);
      if (quotation.iAge > 55) {
        premBeforeLsd = math.bignumber(0);
      }
      var modalFactor = 1;
      for (var p in planDetail.payModes) {
        if (planDetail.payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetail.payModes[p].factor;
        }
      }
      var prem = roundDown(math.multiply(premBeforeLsd, modalFactor), 2);
      premium = math.number(prem);
    }
  }
  return premium;
}