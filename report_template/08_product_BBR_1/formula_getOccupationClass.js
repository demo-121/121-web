function(quotation, planDetail) {
  if (quotation && quotation.plans && quotation.plans[0]) {
    var cls = quotation.plans[0].covClass;
    if (cls) {
      return parseInt(cls, 10) - 1;
    }
    return 0;
  }
}