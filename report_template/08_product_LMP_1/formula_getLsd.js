function(planDetail, sumInsured, age) {
  var lsdMap = [{
    band: 25000,
    lsd: 0
  }, {
    band: 50000,
    lsd: 1
  }, {
    band: 100000,
    lsd: 1.5
  }, {
    band: 200000,
    lsd: 1.8
  }];
  var mapping = _.findLast(lsdMap, m => sumInsured >= m.band);
  if (mapping) {
    return mapping.lsd;
  }
  return 0;
}