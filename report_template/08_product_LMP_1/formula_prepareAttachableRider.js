function(planDetail, quotation, planDetails) {
  quotDriver.prepareAttachableRider(planDetail, quotation, planDetails);
  var riderList = planDetail.inputConfig.riderList;
  var sum = 0;
  for (var i = 0; i < quotation.plans.length; i++) {
    if (quotation.plans[i].covCode === 'ECIX_LMP' || quotation.plans[i].covCode === 'CIBX_LMP') {
      if (quotation.plans[i].sumInsured) {
        sum += quotation.plans[i].sumInsured;
      }
    }
  }
  var appear = true;
  if (quotation.plans[0].sumInsured) {
    if (sum < quotation.plans[0].sumInsured && quotation.plans[0].sumInsured !== 0) {
      appear = true;
    } else {
      appear = false;
    }
  }
  var nohelpTooOld = (quotation.plans[0].premTerm === "65_TA" && quotation.pAge < quotation.iAge);
  var proposerTooOld = quotation.pAge > 60;
  var disable = nohelpTooOld || proposerTooOld;
  var allowResidence = ['R51', 'R2', 'R1', 'R3', 'R4', 'R5'].indexOf(quotation.iResidence) >= 0 || (quotation.iResidence === 'R53' && quotation.iResidenceCity === 'T110') || (quotation.iResidence === 'R54' && quotation.iResidenceCity === 'T120');
  if (riderList) {
    if (!riderList[0]) {
      riderList.push({
        autoAttach: "Y",
        compulsory: "Y",
        covCode: "MBX",
        saRate: "",
        saRule: "0"
      });
      riderList.push({
        autoAttach: "Y",
        compulsory: "Y",
        covCode: "ADBX",
        saRate: "",
        saRule: "0"
      });
      riderList.push({
        autoAttach: "Y",
        compulsory: "Y",
        covCode: "TPDX",
        saRate: "",
        saRule: "0"
      });
      riderList.push({
        autoAttach: "Y",
        compulsory: "N",
        covCode: "ECIX_LMP",
        saRate: "",
        saRule: "0"
      });
      riderList.push({
        autoAttach: "Y",
        compulsory: "N",
        covCode: "PPEPS_LMP",
        saRate: "",
        saRule: "0"
      });
    }
    var firstParty = quotation.sameAs === 'Y';
    riderList = riderList.filter(function(rider) {
      switch (rider.covCode) {
        case 'PPERA_LMP':
          return !firstParty && !disable;
        case 'PPEPS_LMP':
          return !firstParty && !disable;
        case 'DCB_LMP':
          return quotation.plans[0].sumInsured >= 50000;
        case 'WPN_LMP':
          return firstParty && appear;
        case 'ECIX_LMP':
          return allowResidence;
        default:
          return true;
      }
    });
  }
  planDetail.inputConfig.riderList = riderList;
}