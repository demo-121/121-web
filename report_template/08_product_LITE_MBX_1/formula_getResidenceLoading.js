function(quotation, planInfo, planDetail) {
  var rates = planDetail.rates;
  var countryGroup = rates.countryGroup[quotation.iResidence];
  if (Array.isArray(countryGroup)) {
    countryGroup = countryGroup[0];
  }
  var substandardClass = rates.substandardClass[countryGroup]['MBD'];
  if (Array.isArray(substandardClass)) {
    substandardClass = substandardClass[0];
  }
  var rateKey = planInfo.planCode + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS') + substandardClass;
  if (planDetail.rates.residentialLoading[rateKey] && planDetail.rates.residentialLoading[rateKey][quotation.iAge]) {
    return planDetail.rates.residentialLoading[rateKey][quotation.iAge];
  }
  return 0;
}