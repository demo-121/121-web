function(quotation, planInfo, planDetails, extraPara) {
  let company = extraPara.company;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb
  } = company;
  var retVal = {};
  if (company) {
    var retVal = {
      'compName': compName,
      'compRegNo': compRegNo,
      'compAddr': compAddr,
      'compAddr2': compAddr2,
      'compTel': compTel,
      'compFax': compFax,
      'compWeb': compWeb,
    };
  }
  retVal.proposerGenderTitle = (quotation.pGender) == 'M' ? 'Male' : 'Female';
  retVal.proposerSmokerTitle = (quotation.pSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.insurerGenderTitle = (quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.insurerSmokerTitle = (quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  var ccy = quotation.ccy;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = 'US$';
  } else if (ccy == 'AUD') {
    polCcy = 'Australian Dollars';
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    polCcy = 'British Pound';
    ccySyb = '?';
  }
  retVal.polCcy = polCcy;
  retVal.ccySyb = ccySyb;
  var paymentModeTitle = '';
  if (quotation.paymentMode == 'A') {
    paymentModeTitle = 'Annual';
  } else if (quotation.paymentMode == 'S') {
    paymentModeTitle = 'Semi-Annual';
  } else if (quotation.paymentMode == 'Q') {
    paymentModeTitle = 'Quarterly';
  } else if (quotation.paymentMode == 'M') {
    paymentModeTitle = 'Monthly';
  } else if (quotation.paymentMode == 'L') {
    paymentModeTitle = 'Single Premium';
  }
  retVal.paymentModeTitle = paymentModeTitle;
  retVal.paymentMode = quotation.paymentMode;
  retVal.interRate = quotation.policyOptions.interRate + '%';
  var plans = quotation.plans;
  retVal.totalAPrem = quotation.totYearPrem;
  retVal.totalSPrem = quotation.totHalfyearPrem;
  retVal.totalQPrem = quotation.totQuarterPrem;
  retVal.totalMPrem = quotation.totMonthPrem;
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  retVal.iDob = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.proposerDob = new Date(quotation.pDob).format(extraPara.dateFormat);
  retVal.insurerDob = new Date(quotation.iDob).format(extraPara.dateFormat); /** var plans = quotation.plans; var planCodesArr = []; for (var i = 0; i < plans.length; i++) { planCodesArr.push(plans[i].planCode); } planCodes = planCodesArr.join(", "); let campaignCode = ""; if(plans[0].hasDiscount){ let numOfPlans = plans.length; let campaignCodeArr = []; for (let i = 0; i < numOfPlans; i++) { let campaignCodes = plans[i].campaignCodes; if(campaignCodes){ let numOfCampaignCodes = campaignCodes.length; for (let j = 0; j < numOfCampaignCodes; j++) { let code = campaignCodes[j]; let bInsideArr = false; for (let k = 0; k < campaignCodeArr.length; k++) { if(code===campaignCodeArr[k]){ bInsideArr = true; } } if(!bInsideArr) {campaignCodeArr.push(code);} } } } campaignCode = campaignCodeArr.join(", "); if(campaignCode!==""){ planCodes += ", " + campaignCode; } } var planFields = []; var thirdPartyRider = ['PEN', 'PENCI', 'PUNB', 'PUN', 'PPU']; for (var i = 0; i < quotation.plans.length; i++) { var plan = quotation.plans[i]; var policyTerm = plan.policyTerm; var premTerm = plan.premTerm; var covCode = plan.covCode; var sumAssured = getCurrency(plan.sumInsured,' ',0); var planDetail = planDetails[covCode]; var policyTermList = planDetail.inputConfig.policyTermList; var premTermList = planDetail.inputConfig.premTermList; var planInd = planDetail.planInd; var planName = plan.covName.en; var polTermTitle = -1; if (i === 0) { if (quotation.policyOptions.planType !== 'renew'){ planName += ' (' + quotation.policyOptionsDesc.planType.en + ')'; } retVal.basicPlanName = planName; retVal.basicPlanPremTermDesc = plan.premTermDesc; retVal.basicPlanPolicyTermDesc = plan.polTermDesc; retVal.basicPlanCurrency = polCcy; retVal.basicPlanPolTerm = plan.policyTermYr; } var paymentTermTitle = -1; for (var j = 0; j < policyTermList.length; j++) { var polTermOpt = policyTermList[j]; if (polTermOpt.value == policyTerm){polTermTitle = polTermOpt.title;} } for (var k = 0; k < premTermList.length; k++) { var premTermOpt = premTermList[k]; if (premTermOpt.value == premTerm){paymentTermTitle = premTermOpt.title;} } var calcAge = (thirdPartyRider.indexOf(covCode) > -1) ? quotation.pAge : quotation.iAge; if (premTerm.indexOf('TA') > -1) { paymentTermTitle = (Number.parseInt(premTerm) - calcAge) + ' Years'; } if (polTermTitle != -1 && paymentTermTitle!= -1) { var polTerm = { covCode: covCode, planInd: planInd, policyTerm: polTermTitle, permTerm: paymentTermTitle, sumAssured: sumAssured, planName: planName }; planFields.push(polTerm); } } */
  var planCodes = "";
  let planInfoList = quotation.plans;
  let planInfos = [];
  for (let i = 0; i < planInfoList.length; i++) {
    let planInfo = planInfoList[i];
    let planDetail = planDetails[planInfo.covCode];
    let plan = {};
    plan.planName = planInfo.covName.en;
    plan.sumAssured = getCurrency(planInfo.sumInsured, ' ', 0);
    plan.policyTerm = planInfo.policyTerm + ' Years';
    plan.premium = getCurrency(planInfo.premium, ' ', 2);
    plan.APremium = getCurrency(planInfo.yearPrem, ' ', 2);
    plan.HPremium = getCurrency(planInfo.halfYearPrem, ' ', 2);
    plan.QPremium = getCurrency(planInfo.quarterPrem, ' ', 2);
    plan.MPremium = getCurrency(planInfo.monthPrem, ' ', 2);
    plan.LPremium = getCurrency(planInfo.yearPrem, ' ', 2);
    if (parseInt(planInfo.premTerm) === 1) {
      plan.permTerm = 'Single Premium';
    } else {
      plan.permTerm = planInfo.premTerm + ' Years';
    }
    plan.payMode = quotation.paymentMode;
    if (i === 0) {
      planCodes = planInfo.planCode;
    } else {
      planCodes = planCodes + ',' + planInfo.planCode;
    }
    planInfos.push(plan);
  }
  retVal.planInfos = planInfos;
  retVal.planCodes = planCodes;
  var residencyCountryOptions, residencyCityOptions, iRCountry, iRCity, pRCountry, pRCity, occOptions;
  residencyCountryOptions = extraPara.optionsMap.residency.options;
  residencyCityOptions = extraPara.optionsMap.city.options;
  occOptions = extraPara.optionsMap.occupation.options;
  if (quotation.iResidence === 'R128') {
    retVal.iOtherCountry = 'Y';
  }
  if (quotation.pResidence === 'R128') {
    retVal.pOtherCountry = 'Y';
  }
  for (var v in residencyCityOptions) {
    if (residencyCityOptions[v].value === quotation.pResidenceCity) {
      pRCity = residencyCityOptions[v].title.en;
    }
    if (residencyCityOptions[v].value === quotation.iResidenceCity) {
      iRCity = residencyCityOptions[v].title.en;
    }
  }
  for (var v in residencyCountryOptions) {
    if (residencyCountryOptions[v].value === quotation.pResidence) {
      pRCountry = residencyCountryOptions[v].title.en;
    }
    if (residencyCountryOptions[v].value === quotation.iResidence) {
      iRCountry = residencyCountryOptions[v].title.en;
    }
  }
  var iOcc = '';
  var iOccTitle = '';
  var hasAP = false;
  for (var v in quotation.plans) {
    if (quotation.plans[v].covCode == 'AP_DTS') {
      hasAP = true;
    }
  }
  if (hasAP) {
    iOcc = ': ' + quotation.iOccupationClass;
    iOccTitle = 'Occupation Class';
  }
  var hasrider = 'N';
  var isSignPrem = 'N';
  if (quotation.plans.length > 1) {
    hasrider = 'Y';
  }
  if (quotation.paymentMode == 'L') {
    isSignPrem = 'Y';
  }
  retVal.hasrider = hasrider;
  retVal.isSignPrem = isSignPrem;
  retVal.occTitle = iOccTitle;
  retVal.occValue = iOcc;
  retVal.proposerCountry = (pRCountry) ? pRCountry : "";
  if (pRCity) {
    retVal.proposerCity = pRCity;
    retVal.proposerCitySelected = 'Y';
  } else {
    retVal.proposerCity = '';
    retVal.proposerCitySelected = 'N';
  }
  retVal.insurerCountry = (iRCountry) ? iRCountry : "";
  if (iRCity) {
    retVal.insurerCity = iRCity;
    retVal.insurerCitySelected = 'Y';
  } else {
    retVal.insurerCity = '';
    retVal.insurerCitySelected = 'N';
  } /** retVal.planType = quotation.policyOptions.planType; retVal.indexation = (quotation.policyOptions.indexation === 'Y') ? 'Yes' : 'No'; var index = retVal.basicPlanPolTerm - 1; var curRow = extraPara.illustrations[quotation.baseProductCode][index]; var temp = curRow.bpTdc / curRow.totBpYearPrem; temp = Math.round(temp * 10000)/100; retVal.basicPlanTDC = getCurrency(curRow.bpTdc, ' ', 0); retVal.basicPlanTDCPercent = temp; retVal.showCampaignNotes = 'Y'; if(global.quotCache && global.quotCache.planDetails){ let basicPlan = global.quotCache.planDetails.TPX; if(!basicPlan){ basicPlan = global.quotCache.planDetails.TPPX; } if(!!basicPlan){ if (!basicPlan.campaign){retVal.showCampaignNotes = 'N';} } } retVal.showCampaignNotesInSecondPage = 'Y'; if (retVal.showCampaignNotes === 'Y') { if ((quotation.plans.length < 3 && quotation.sameAs === 'N') || (quotation.plans.length < 5 && quotation.sameAs === 'Y')) { retVal.showCampaignNotesInSecondPage = 'N'; } else { retVal.showCampaignNotesInSecondPage = 'Y'; } if (extraPara.quickQuote === false) { retVal.showCampaignNotesInSecondPage = 'N'; } } */
  return retVal;
}