function(quotation, planInfo, planDetails, extraPara) {
  var trunc = function(value, position) {
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return Number(math.divide(math.floor(math.multiply(value, scale)), scale));
  };
  var illustrations = extraPara.illustrations;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb,
  } = extraPara.company;
  var retVal = {
    'compName': compName,
    'compRegNo': compRegNo,
    'compAddr': compAddr,
    'compAddr2': compAddr2,
    'compTel': compTel,
    'compFax': compFax,
    'compWeb': compWeb
  };
  var basicPlan = quotation.plans[0];
  var basicIllustrations = illustrations[basicPlan.covCode];
  retVal.headerName = quotation.sameAs == "Y" ? quotation.pFullName : quotation.iFullName;
  retVal.headerGender = (quotation.sameAs == "Y" ? quotation.pGender : quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.headerSmoke = (quotation.sameAs == "Y" ? quotation.pSmoke : quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.headerAge = quotation.sameAs == "Y" ? quotation.pAge : quotation.iAge;
  var illustrationData = (quotation.baseProductCode === 'TPX') ? extraPara.illustrations.TPX : extraPara.illustrations.TPPX;
  var biP1TableData = [];
  var biP2TableData = [];
  var biP3TableData = [];
  var GSV1 = 0;
  var GSV2 = 0;
  var arrowSyb = '';
  var basicPlanPolicyTerm = Number.parseInt(quotation.plans[0].policyTerm);
  var iAge = quotation.iAge;
  var basicPlanPolicyTerm = (quotation.plans[0].policyTerm.indexOf('TA') > -1) ? Number.parseInt(quotation.plans[0].policyTerm) - quotation.iAge : Number.parseInt(quotation.plans[0].policyTerm);
  var longestRiderPolicyTerm = 0;
  var hasSV = false;
  for (var v in quotation.plans) {
    if (v !== 0) {
      var policyTerm = quotation.plans[v].policyTerm;
      var covCode = quotation.plans[v].covCode;
      var policyTermValueAndType = (policyTerm) ? policyTerm.split('_') : [];
      if (policyTermValueAndType[0] && policyTermValueAndType[1] && policyTermValueAndType[1] === 'YR' && Number.parseInt(policyTerm) > longestRiderPolicyTerm) {
        longestRiderPolicyTerm = Number.parseInt(policyTerm);
      } else if (policyTermValueAndType[0] && policyTermValueAndType[1] && policyTermValueAndType[1] === 'TA' && (Number.parseInt(policyTerm) - quotation.iAge) > longestRiderPolicyTerm) {
        longestRiderPolicyTerm = Number.parseInt(policyTerm) - quotation.iAge;
      }
      if (quotation.plans[v].covCode == 'SV') {
        hasSV = true;
      }
    }
  }
  basicPlanPolicyTerm = ((basicPlanPolicyTerm - 1) <= 0) ? 0 : basicPlanPolicyTerm - 1;
  longestRiderPolicyTerm = ((longestRiderPolicyTerm - 1) <= 0) ? 0 : longestRiderPolicyTerm - 1;
  var hasArrowSyb = false;
  for (var v in illustrationData) {
    var orginalBIRowData = illustrationData[v];
    var showIndex = Number.parseInt(v);
    var polYr = showIndex + 1;
    iAge += 1;
    arrowSyb = (polYr - 1 > basicPlanPolicyTerm && quotation.policyOptions.planType === 'renew') ? '^' : '';
    var polYr_age = polYr + '/' + iAge + arrowSyb;
    var totalPremPaid = orginalBIRowData.totYearPrem;
    var totalDistribCost = orginalBIRowData.totTdc;
    var GDB = orginalBIRowData.guaranteedDB;
    var GSV = orginalBIRowData.guaranteedSV;
    var totBpYearPrem = orginalBIRowData.totBpYearPrem;
    var totbpTdc = orginalBIRowData.bpTdc;
    var biRowData = {
      polYr_age,
      totalPremPaid: getCurrency(totalPremPaid, ' ', 0),
      totalDistribCost: getCurrency(totalDistribCost, ' ', 0),
      GDB: getCurrency(GDB, ' ', 0),
      GSV: getCurrency(GSV, ' ', 0)
    };
    var biRowP2Data = {
      polYr_age,
      totalPremPaid: getCurrency(totBpYearPrem, ' ', 0),
      totalDistribCost: getCurrency(totbpTdc, ' ', 0),
      GDB: getCurrency(GDB, ' ', 0),
      GSV: getCurrency(GSV, ' ', 0)
    };
    var biRowP3Data = {
      polYr_age,
      totalPremPaid: getCurrency(totBpYearPrem, ' ', 0),
      totalDistribCost: getCurrency(totbpTdc, ' ', 0),
      GDB: getCurrency(GDB, ' ', 0),
      GSV: getCurrency(GSV, ' ', 0)
    };
    if (showIndex < 10 || math.mod(showIndex + 1, 5) == 0 || showIndex == illustrationData.length - 1) {
      if (arrowSyb === '^') {
        hasArrowSyb = true;
      }
      biP1TableData.push(biRowData);
      biP2TableData.push(biRowP2Data);
      biP3TableData.push(biRowP3Data);
      retVal.polYr_age1 = polYr_age;
      retVal.totalPremPaid1 = getCurrency(totalPremPaid, ' ', 0);
      retVal.totalDistribCost1 = getCurrency(totalDistribCost, ' ', 0);
      retVal.GDB1 = (hasSV) ? getCurrency(GDB, ' ', 0) : getCurrency(0, ' ', 0);
      retVal.polYr_age2 = polYr_age;
      retVal.totalPremPaid2 = getCurrency(totBpYearPrem, ' ', 0);
      retVal.totalDistribCost2 = getCurrency(totbpTdc, ' ', 0);
      retVal.GDB2 = getCurrency(0, ' ', 0);
    }
  }
  retVal.illustrateData_totalDistribCost = biP3TableData;
  retVal.illustrateData_basic = biP2TableData;
  retVal.illustrateData_basicnrider = biP1TableData;
  var ccy = quotation.ccy;
  var polCcy = ccy;
  var ccySyb = '';
  if (ccy == 'SGD') {
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    ccySyb = 'US$';
  } else if (ccy == 'AUD') {
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    ccySyb = '£';
  }
  retVal.polCcy = polCcy;
  retVal.ccySyb = ccySyb;
  var paymentModeTitle = '';
  if (quotation.paymentMode == 'A') {
    paymentModeTitle = 'Annual';
  } else if (quotation.paymentMode == 'S') {
    paymentModeTitle = 'Semi-Annual';
  } else if (quotation.paymentMode == 'Q') {
    paymentModeTitle = 'Quarterly';
  } else if (quotation.paymentMode == 'M') {
    paymentModeTitle = 'Monthly';
  }
  retVal.paymentModeTitle = paymentModeTitle;
  retVal.basicAnnualPrem = getCurrency(planInfo.yearPrem, ' ', 2);
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  retVal.planName = (quotation.policyOptions.planType === 'renew') ? quotation.baseProductName.en + ' (Renewable)' : quotation.baseProductName.en + ' (To Age)';
  retVal.indexPolicyOptions = quotation.policyOptions.indexation;
  retVal.quoPlanType = (quotation.policyOptions.planType === 'renew') ? 'Y' : 'N';
  if (hasArrowSyb && quotation.policyOptions.planType === 'renew') {
    retVal.noteType = 'notes';
  } else if (quotation.policyOptions.planType === 'toAge' && quotation.policyOptions.indexation === 'Y') {
    retVal.noteType = 'specialfeature';
  } else {
    retVal.noteType = '';
  }
  return retVal;
}