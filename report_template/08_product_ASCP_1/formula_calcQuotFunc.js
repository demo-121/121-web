function(quotation, planInfo, planDetails) {
  planInfo.paymentMethod = 'CASH';
  if (!planInfo.payFreq || planInfo.payFreq === '') {
    planInfo.payFreq = 'M';
  }
  var covClass = quotation.plans[0].covClass;
  var gender = quotation.iGender;
  var age = quotation.iAge;
  if (covClass) {
    var planDetail = planDetails[planInfo.covCode];
    var modalFactor = math.bignumber(1);
    var covMonth = 12;
    var payModes = planDetail.payModes;
    for (var i in payModes) {
      if (payModes[i].mode === planInfo.payFreq) {
        modalFactor = math.bignumber(payModes[i].factor);
        covMonth = payModes[i].covMonth;
        break;
      }
    }
    var hvgst = planDetails[quotation.baseProductCode].gstInd === 'Y';
    var regularPrem = hvgst ? math.bignumber(planDetail.rates.premRateGST[covClass][planInfo.payFreq][age]) : math.bignumber(planDetail.rates.premRate[covClass][planInfo.payFreq][age]);
    var prem = regularPrem;
    if (planDetails[quotation.baseProductCode].gstInd === 'Y') {
      /*var gstRate = math.bignumber(planDetails[quotation.baseProductCode].gstRate); var gstAmt = math.round(math.multiply(regularPrem, gstRate), 2); prem = math.round(math.add(prem, gstAmt), 1);*/
      var gstAmt = prem;
      if (planInfo.payFreq === 'M') {
        planInfo.tax = {
          monthTax: math.number(gstAmt),
          yearTax: math.number(math.divide(math.floor(math.multiply(gstAmt, 12 / covMonth, 100)), 100))
        };
      } else if (planInfo.payFreq === 'A') {
        planInfo.tax = {
          yearTax: math.number(gstAmt)
        };
      }
    }
    planInfo.premium = math.number(prem);
    planInfo.yearPrem = math.number(math.divide(math.floor(math.multiply(prem, 12 / covMonth, 100)), 100));
    planInfo.covClass = covClass;
  }
}