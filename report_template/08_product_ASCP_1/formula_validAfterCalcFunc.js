function(quotation, planInfo, planDetail) {
  for (var c in planDetail.classList) {
    var classItem = planDetail.classList[c];
    if (classItem.covClass === planInfo.covClass) {
      planInfo.covName = classItem.className;
      break;
    }
  }
  for (var p in planDetail.inputConfig.paymentMethodList) {
    var item = planDetail.inputConfig.paymentMethodList[p];
    if (item.value === planInfo.paymentMethod) {
      planInfo.paymentMethodDesc = item.title.en;
      break;
    }
  }
  for (var p in planDetail.inputConfig.payFreqList) {
    var item = planDetail.inputConfig.payFreqList[p];
    if (item.value === planInfo.payFreq) {
      planInfo.payFreqDesc = item.title.en;
      break;
    }
  }
}