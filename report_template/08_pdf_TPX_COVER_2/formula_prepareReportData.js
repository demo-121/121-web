function(quotation, planInfo, planDetails, extraPara) {
  var trunc = function(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, Number(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  var numberTrunc = function(value, digit) {
    value = value.toString();
    if (value.indexOf('.') === -1) {
      return Number.parseInt(value);
    } else {
      value = value.substr(0, value.indexOf('.') + digit + 1);
      return Number.parseFloat(value);
    }
  };
  var paddingZeroRight = function(value) {
    value = value.toString();
    if (value.indexOf('.') > -1) {
      var decimal = value.split('.');
      if (decimal[1].length === 1) {
        decimal[1] = decimal[1] + '0';
        return decimal[0] + '.' + decimal[1];
      } else {
        return value;
      }
    } else {
      return value + '.00';
    }
  };
  let company = extraPara.company;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb
  } = company;
  var retVal = {};
  if (company) {
    var retVal = {
      'compName': compName,
      'compRegNo': compRegNo,
      'compAddr': compAddr,
      'compAddr2': compAddr2,
      'compTel': compTel,
      'compFax': compFax,
      'compWeb': compWeb,
    };
  }
  retVal.proposerGenderTitle = (quotation.pGender) == 'M' ? 'Male' : 'Female';
  retVal.proposerSmokerTitle = (quotation.pSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.insurerGenderTitle = (quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.insurerSmokerTitle = (quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  var ccy = quotation.ccy;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = '$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = '$';
  } else if (ccy == 'AUD') {
    polCcy = 'Australian Dollars';
    ccySyb = '$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    polCcy = 'British Pound';
    ccySyb = '£';
  }
  retVal.polCcy = ccy;
  retVal.ccySyb = ccySyb;
  var paymentModeTitle = '';
  if (quotation.paymentMode == 'A') {
    paymentModeTitle = 'Annual';
  } else if (quotation.paymentMode == 'S') {
    paymentModeTitle = 'Semi-Annual';
  } else if (quotation.paymentMode == 'Q') {
    paymentModeTitle = 'Quarterly';
  } else if (quotation.paymentMode == 'M') {
    paymentModeTitle = 'Monthly';
  } else if (quotation.paymentMode == 'L') {
    paymentModeTitle = 'Single Premium';
  }
  retVal.paymentModeTitle = paymentModeTitle;
  var plans = quotation.plans;
  retVal.totalAPrem = quotation.totYearPrem;
  retVal.totalSPrem = quotation.totHalfyearPrem;
  retVal.totalQPrem = quotation.totQuarterPrem;
  retVal.totalMPrem = quotation.totMonthPrem;
  var planCodes = "";
  var plans = quotation.plans;
  for (var i = 0; i < plans.length; i++) {
    planCodes = planCodes + plans[i].planCode + ", ";
  }
  var planFields = [];
  var thirdPartyRider = ['PEN', 'PENCI', 'PUNB', 'PUN', 'PPU'];
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    var policyTerm = plan.policyTerm;
    var premTerm = plan.premTerm;
    var covCode = plan.covCode;
    var sumAssured = getCurrency(plan.sumInsured, ' ', 0);
    var planDetail = planDetails[covCode];
    var policyTermList = planDetail.inputConfig.policyTermList;
    var premTermList = planDetail.inputConfig.premTermList;
    var planInd = planDetail.planInd;
    var planName = plan.covName.en;
    var polTermTitle = -1;
    if (quotation.policyOptions.planType !== 'renew') {
      planName += ' (' + quotation.policyOptionsDesc.planType.en + ')';
    }
    if (i === 0) {
      retVal.basicPlanName = planName;
      retVal.basicPlanPremTermDesc = plan.premTermDesc;
      retVal.basicPlanPolicyTermDesc = plan.polTermDesc;
      retVal.basicPlanCurrency = polCcy;
      retVal.basicPlanPolTerm = plan.policyTermYr;
    }
    var paymentTermTitle = -1;
    for (var j = 0; j < policyTermList.length; j++) {
      var polTermOpt = policyTermList[j];
      if (polTermOpt.value == policyTerm) {
        polTermTitle = polTermOpt.title;
      }
    }
    for (var k = 0; k < premTermList.length; k++) {
      var premTermOpt = premTermList[k];
      if (premTermOpt.value == premTerm) {
        paymentTermTitle = premTermOpt.title;
      }
    }
    var calcAge = (thirdPartyRider.indexOf(covCode) > -1) ? quotation.pAge : quotation.iAge;
    if (premTerm.indexOf('TA') > -1) {
      paymentTermTitle = (Number.parseInt(premTerm) - calcAge) + ' Years';
    }
    if (polTermTitle != -1 && paymentTermTitle != -1) {
      var polTerm = {
        covCode: covCode,
        planInd: planInd,
        policyTerm: polTermTitle,
        permTerm: paymentTermTitle,
        sumAssured: sumAssured,
        planName: planName
      };
      planFields.push(polTerm);
    }
  }
  let planInfoList = quotation.plans;
  let planInfos = [];
  for (let i = 0; i < planInfoList.length; i++) {
    let planInfo = planInfoList[i];
    let planDetail = planDetails[planInfo.covCode];
    let plan = {};
    plan.planName = !planInfo.covName ? "" : (typeof planInfo.covName == 'string' ? planInfo.covName : (planInfo.covName.en || planInfo.covName[Object.keys(planInfo.covName)[0]]));
    plan.postfix = !planInfo.saPostfix ? "" : (typeof planInfo.saPostfix == 'string' ? planInfo.saPostfix : (planInfo.saPostfix.en || planInfo.saPostfix[Object.keys(planInfo.saPostfix)[0]]));
    plan.sumAssured = getCurrency(planInfo.sumInsured, ' ', 0);
    plan.policyTerm = (extraPara.langMap && extraPara.langMap[planInfo.policyTerm]) || planInfo.policyTerm;
    plan.premium = getCurrency(planInfo.premium, ' ', 2);
    plan.APremium = getCurrency(planInfo.yearPrem, ' ', 2);
    plan.HPremium = getCurrency(planInfo.halfYearPrem, ' ', 2);
    plan.QPremium = getCurrency(planInfo.quarterPrem, ' ', 2);
    plan.MPremium = getCurrency(planInfo.monthPrem, ' ', 2);
    plan.LPremium = getCurrency(planInfo.yearPrem, ' ', 2);
    plan.permTerm = (extraPara.langMap && extraPara.langMap[planInfo.premTerm]) || planInfo.premTerm;
    plan.payMode = quotation.paymentMode;
    plan.planInd = planDetail.planInd;
    plan.covCode = planDetail.covCode;
    plan.productLine = planDetail.productLine;
    if (i === 0) {
      plan.planName = plan.planName + ' (' + quotation.policyOptionsDesc.planType.en + ')';
    }
    planInfos.push(plan);
  }
  retVal.planInfos = planInfos;
  retVal.planFields = planFields;
  retVal.planCodes = planCodes.substring(0, planCodes.length - 2);
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  retVal.iDob = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.proposerDob = new Date(quotation.pDob).format(extraPara.dateFormat);
  retVal.insurerDob = new Date(quotation.iDob).format(extraPara.dateFormat);
  var residencyCountryOptions, residencyCityOptions, iRCountry, iRCity, pRCountry, pRCity, occOptions;
  residencyCountryOptions = extraPara.optionsMap.residency.options;
  residencyCityOptions = extraPara.optionsMap.city.options;
  occOptions = extraPara.optionsMap.occupation.options;
  if (quotation.iResidence === 'R128') {
    retVal.iOtherCountry = 'Y';
  }
  if (quotation.pResidence === 'R128') {
    retVal.pOtherCountry = 'Y';
  }
  for (var v in residencyCityOptions) {
    if (residencyCityOptions[v].value === quotation.pResidenceCity) {
      pRCity = residencyCityOptions[v].title.en;
    }
    if (residencyCityOptions[v].value === quotation.iResidenceCity) {
      iRCity = residencyCityOptions[v].title.en;
    }
  }
  for (var v in residencyCountryOptions) {
    if (residencyCountryOptions[v].value === quotation.pResidence) {
      pRCountry = residencyCountryOptions[v].title.en;
    }
    if (residencyCountryOptions[v].value === quotation.iResidence) {
      iRCountry = residencyCountryOptions[v].title.en;
    }
  }
  var iOcc = '';
  var iOccTitle = '';
  var hasAP = false;
  for (var v in quotation.plans) {
    if (quotation.plans[v].covCode == 'AP') {
      hasAP = true;
    }
  }
  if (hasAP) {
    iOcc = ': ' + quotation.iOccupationClass;
    iOccTitle = 'Occupation Class';
  }
  retVal.occTitle = iOccTitle;
  retVal.occValue = iOcc;
  retVal.proposerCountry = (pRCountry) ? pRCountry : "";
  if (pRCity) {
    retVal.proposerCity = pRCity;
    retVal.proposerCitySelected = 'Y';
  } else {
    retVal.proposerCity = '';
    retVal.proposerCitySelected = 'N';
  }
  retVal.insurerCountry = (iRCountry) ? iRCountry : "";
  if (iRCity) {
    retVal.insurerCity = iRCity;
    retVal.insurerCitySelected = 'Y';
  } else {
    retVal.insurerCity = '';
    retVal.insurerCitySelected = 'N';
  }
  retVal.paymentMode = quotation.paymentMode;
  retVal.planType = quotation.policyOptions.planType;
  retVal.indexation = (quotation.policyOptions.indexation === 'Y') ? 'Yes' : 'No'; /** Get total premium paid and TDC at end of policy year*/
  var maturityAge;
  var bpPolicyTerm = quotation.plans[0].policyTerm;
  if (quotation.plans[0].policyTerm.indexOf('_TA') > -1) {
    maturityAge = parseInt(bpPolicyTerm);
  } else {
    maturityAge = quotation.iAge + parseInt(bpPolicyTerm);
  }
  var index = maturityAge - quotation.iAge - 1;
  var curRow = extraPara.illustrations[quotation.baseProductCode][index];
  retVal.basicPlanTDC = getCurrency(curRow.bpTdc, ' ', 0);
  var precentage = math.multiply(math.divide(math.bignumber(curRow.nonRoundBpTdc), math.bignumber(curRow.totBpYearPrem)), 100);
  retVal.basicPlanTDCPercent = paddingZeroRight(numberTrunc(math.number(precentage), 2));
  return retVal;
}