function(quotation, planInfo, planDetail) {
  var trunc = function(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.bignumber(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  var sumInsured = trunc(math.bignumber(planInfo.sumInsured), 2);
  var pGender = quotation.pGender;
  var pSmoke = quotation.pSmoke;
  var pAge = quotation.pAge;
  var polTerm = quotation.plans[0].policyTerm;
  polTerm = (pAge <= 65) ? Math.min(polTerm, 65 - pAge) : 0;
  var Raw_PremiumRate = 0;
  if (polTerm) {
    Raw_PremiumRate = planDetail.rates.premRate[pGender + (pSmoke == 'N' ? 'N' : 'S')][polTerm + 'PT'][pAge];
  }
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    if (plan.covCode == planInfo.covCode) {
      plan.premRate = math.number(Raw_PremiumRate);
      break;
    }
  }
  Raw_PremiumRate = math.bignumber(Raw_PremiumRate);
  var r1_discount = math.bignumber(0);
  var annualPrem = math.number(trunc(math.multiply(math.multiply(Raw_PremiumRate, math.subtract(math.bignumber(1), r1_discount)), math.divide(sumInsured, math.bignumber(100))), 2));
  return math.number(annualPrem);
}