function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var bpInfo = quotation.plans[0];
  var sa = bpInfo.sumInsured;
  sa = sa > 1200000 ? 1200000 : sa;
  var amount = sa > 0 ? sa : 0;
  let ECIBobj = quotation.plans.find(o => o.covCode === 'ECIX_LMP');
  let CIBpobj = quotation.plans.find(o => o.covCode === 'CIBX_LMP');
  var multiFactor = quotation.policyOptions.multiFactor;
  if (multiFactor) {
    var maxValue = quotation.iAge < 18 ? 500000 : 3000000;
    maxValue = parseInt(maxValue / (quotation.policyOptions.multiFactor * 1000));
    maxValue *= 1000;
    if (maxValue > bpInfo.sumInsured) {
      maxValue = bpInfo.sumInsured;
    }
    if (maxValue >= 25000) {
      planDetail.inputConfig.benlim = {
        min: 25000,
        max: maxValue
      };
    }
  }
}