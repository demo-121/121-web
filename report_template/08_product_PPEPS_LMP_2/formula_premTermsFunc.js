function(planDetail, quotation) {
  var premTermsList = [];
  var hasTa65 = false;
  _.each([15, 20, 25], (val) => {
    if (quotation.iAge + val <= 80) {
      premTermsList.push({
        value: val + '_YR',
        title: val + ' Years'
      });
    }
  });
  premTermsList.push({
    value: '65_TA',
    title: 'To Age 65'
  });
  return premTermsList;
}