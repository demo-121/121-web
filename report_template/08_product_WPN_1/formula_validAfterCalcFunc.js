function(quotation, planInfo, planDetail) {
  var paddingZeroStr = function(number) {
    if (number < 10 && number >= 0) {
      return '0' + number;
    } else if (number < 0) {
      return '00';
    } else {
      return number;
    }
  };
  if (planInfo.policyTerm) {
    var planType = quotation.policyOptions.planType;
    var policyTermValue = paddingZeroStr(Number.parseInt(planInfo.policyTerm));
    if (quotation.policyOptions.planType === 'renew') {
      planInfo.planCode = policyTermValue + 'WPR';
    } else if (planInfo.policyTerm.indexOf('TA') > -1) {
      planInfo.planCode = 'WPR' + policyTermValue;
    } else {
      planInfo.planCode = policyTermValue + 'WPN';
    }
    if (planInfo.premium) {
      planInfo.policyTermYr = Number.parseInt(planInfo.policyTerm);
      if (planInfo.premTerm === 'SP') {
        planInfo.premTermYr = 1;
      } else {
        planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
      }
    }
  }
  var crxPlan = _.find(quotation.plans, p => p.covCode === 'CRX');
  if (crxPlan && crxPlan.sumInsured && crxPlan.sumInsured === quotation.plans[0].sumInsured) {
    quotDriver.context.addError({
      covCode: planInfo.covCode,
      msg: 'Critical Illness PremiumEraser is not allowed if Advance CI Payout rider Sum Assured is the same as your Basic Plan\'s Sum Assured.'
    });
  }
  if (!quotation.plans[0].premium) {
    planInfo.premium = planInfo.yearPrem = planInfo.halfYearPrem = planInfo.quarterPrem = planInfo.monthPrem = quotation.plans[0].premium;
  }
}