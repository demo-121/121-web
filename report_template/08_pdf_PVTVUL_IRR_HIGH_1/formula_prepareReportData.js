function(quotation, planInfo, planDetails, extraPara) { /*test pvtvul mainBI**/
  var illust = extraPara.illustrations[planInfo.covCode];
  var company = extraPara.company;
  var sameAs = quotation.sameAs;
  var illustrationIRR_High = [];
  var illustrationIRR_High2 = [];
  var illustrationIRR_High3 = [];
  var illustrationIRR_High4 = [];
  var policyTerm = parseInt(planInfo.policyTerm);
  var polType = (quotation.policyOptions.policytype == "joint") ? true : false;
  var policyOption = quotation.policyOptions;
  var baseProductCode = quotation.baseProductCode;
  var productVersion = quotation.productVersion;
  var withdrawalFee = baseProductCode == 'PVTVUL' ? 500 : 100;
  var grossRate = policyOption.grossInvRate;
  var charge = policyOption.charge;
  var initPremiumTop = illust[0].totalPremium;
  var sumAssuredTop = illust[0].sumAssured.IRR_UI;
  var lapseDisclosure = 0;
  var lapseFlag = true;
  if (isNaN(grossRate)) {
    grossRate = 0;
  }
  if (isNaN(charge)) {
    charge = 0;
  }
  var in_IRR = (grossRate - charge) + '.0%';
  var i;
  var yearCount = 0;
  var yearCount2 = 0;
  var polTerm = 0;
  var incRowCnt = 0;
  var incRowRiderCnt = 0;
  var incRowAnnualCnt = 0;
  var tdcbasicCnt = 0;
  var LA1age = quotation.pAge + 1;
  var showTextThisPage2 = false;
  var showTextThisPage3 = false;
  var showTextThisPage4 = false;
  var getSmokingDesc = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  var getGenderDesc = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getCurrencyDesc = function(ccy) {
    var sign = "$";
    if (ccy === 'SGD') {
      sign = 'S$ ';
    } else if (ccy === 'USD') {
      sign = 'US$ ';
    } else if (ccy === 'GBP') {
      sign = '£ ';
    } else if (ccy === 'EUR') {
      sign = '€ ';
    } else if (ccy === 'AUD') {
      sign = 'A$ ';
    } else if (ccy === 'JPY') {
      sign = '¥ ';
    } else if (ccy === 'CHF') {
      sign = 'CHF ';
    }
    return sign;
  };
  for (i = 0; i < illust.length; i++) {
    var row = illust[i];
    var policyYear = row.policyYear;
    var LA2age = row.age;
    var LA12age = LA1age + ' / ' + LA2age;
    var endYearAge = row.policyYear + ' / ' + LA2age;
    var totPremiumPtd = row.totalPremium;
    var sumAssured = row.sumAssured.IRR_High;
    var polFundValueUI = row.policy_Fund.IRR_High;
    var deathBenefitUI = row.death_Benefit.IRR_High;
    var surrenderValueUI = row.surrender_Value.IRR_High;
    var fees = row.fees.IRR_UI;
    var InsuranceCharge = row.insurance_Charge.IRR_High;
    var incRow = false;
    LA1age++;
    if (policyYear <= policyTerm) {
      incRow = true;
      incRowCnt++;
    }
    if (surrenderValueUI == 0 && lapseFlag) {
      lapseDisclosure = i;
      lapseFlag = false;
    }
    var incRowCntLineBreak = false;
    if (incRowCnt !== 0 && incRowCnt % 5 === 0 && incRowCnt !== illust.length) {
      incRowCntLineBreak = true;
    }
    if (polType) {
      if (incRow) {
        if (i < 30) {
          illustrationIRR_High.push({
            policyYear: policyYear,
            LA12age: LA2age,
            totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
            sumAssured: getCurrency(sumAssured, '', 0),
            polFundValueUI: getCurrency(polFundValueUI, '', 0),
            deathBenefitUI: getCurrency(deathBenefitUI, '', 0),
            surrenderValueUI: getCurrency(surrenderValueUI, '', 0),
            fees: getCurrency(fees, '', 0),
            InsuranceCharge: getCurrency(InsuranceCharge, '', 0)
          });
          if (incRowCntLineBreak) {
            illustrationIRR_High.push({
              policyYear: "",
              LA12age: "",
              totPremiumPtd: "",
              sumAssured: "",
              polFundValueUI: "",
              deathBenefitUI: "",
              surrenderValueUI: "",
              fees: "",
              InsuranceCharge: "",
            });
          }
        } else if (i >= 30 && i < 60) {
          illustrationIRR_High2.push({
            policyYear: policyYear,
            LA12age: LA2age,
            totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
            sumAssured: getCurrency(sumAssured, '', 0),
            polFundValueUI: getCurrency(polFundValueUI, '', 0),
            deathBenefitUI: getCurrency(deathBenefitUI, '', 0),
            surrenderValueUI: getCurrency(surrenderValueUI, '', 0),
            fees: getCurrency(fees, '', 0),
            InsuranceCharge: getCurrency(InsuranceCharge, '', 0)
          });
          if (incRowCntLineBreak) {
            illustrationIRR_High2.push({
              policyYear: "",
              LA12age: "",
              totPremiumPtd: "",
              sumAssured: "",
              polFundValueUI: "",
              deathBenefitUI: "",
              surrenderValueUI: "",
              fees: "",
              InsuranceCharge: "",
            });
          }
        } else if (i >= 60 && i < 90) {
          illustrationIRR_High3.push({
            policyYear: policyYear,
            LA12age: LA2age,
            totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
            sumAssured: getCurrency(sumAssured, '', 0),
            polFundValueUI: getCurrency(polFundValueUI, '', 0),
            deathBenefitUI: getCurrency(deathBenefitUI, '', 0),
            surrenderValueUI: getCurrency(surrenderValueUI, '', 0),
            fees: getCurrency(fees, '', 0),
            InsuranceCharge: getCurrency(InsuranceCharge, '', 0)
          });
          if (incRowCntLineBreak) {
            illustrationIRR_High3.push({
              policyYear: "",
              LA12age: "",
              totPremiumPtd: "",
              sumAssured: "",
              polFundValueUI: "",
              deathBenefitUI: "",
              surrenderValueUI: "",
              fees: "",
              InsuranceCharge: "",
            });
          }
        } else if (i >= 90 && i < 120) {
          illustrationIRR_High4.push({
            policyYear: policyYear,
            LA12age: LA2age,
            totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
            sumAssured: getCurrency(sumAssured, '', 0),
            polFundValueUI: getCurrency(polFundValueUI, '', 0),
            deathBenefitUI: getCurrency(deathBenefitUI, '', 0),
            surrenderValueUI: getCurrency(surrenderValueUI, '', 0),
            fees: getCurrency(fees, '', 0),
            InsuranceCharge: getCurrency(InsuranceCharge, '', 0)
          });
          if (incRowCntLineBreak) {
            illustrationIRR_High4.push({
              policyYear: "",
              LA12age: "",
              totPremiumPtd: "",
              sumAssured: "",
              polFundValueUI: "",
              deathBenefitUI: "",
              surrenderValueUI: "",
              fees: "",
              InsuranceCharge: "",
            });
          }
        }
      }
    } else {
      if (incRow) {
        if (i < 30) {
          illustrationIRR_High.push({
            endYearAge: endYearAge,
            totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
            sumAssured: getCurrency(sumAssured, '', 0),
            polFundValueUI: getCurrency(polFundValueUI, '', 0),
            deathBenefitUI: getCurrency(deathBenefitUI, '', 0),
            surrenderValueUI: getCurrency(surrenderValueUI, '', 0),
            fees: getCurrency(fees, '', 0),
            InsuranceCharge: getCurrency(InsuranceCharge, '', 0)
          });
          if (incRowCntLineBreak) {
            illustrationIRR_High.push({
              endYearAge: "",
              totPremiumPtd: "",
              sumAssured: "",
              polFundValueUI: "",
              deathBenefitUI: "",
              surrenderValueUI: "",
              fees: "",
              InsuranceCharge: "",
            });
          }
        } else if (i >= 30 && i < 60) {
          illustrationIRR_High2.push({
            endYearAge: endYearAge,
            totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
            sumAssured: getCurrency(sumAssured, '', 0),
            polFundValueUI: getCurrency(polFundValueUI, '', 0),
            deathBenefitUI: getCurrency(deathBenefitUI, '', 0),
            surrenderValueUI: getCurrency(surrenderValueUI, '', 0),
            fees: getCurrency(fees, '', 0),
            InsuranceCharge: getCurrency(InsuranceCharge, '', 0)
          });
          if (incRowCntLineBreak) {
            illustrationIRR_High2.push({
              endYearAge: "",
              totPremiumPtd: "",
              sumAssured: "",
              polFundValueUI: "",
              deathBenefitUI: "",
              surrenderValueUI: "",
              fees: "",
              InsuranceCharge: "",
            });
          }
        } else if (i >= 60 && i < 90) {
          illustrationIRR_High3.push({
            endYearAge: endYearAge,
            totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
            sumAssured: getCurrency(sumAssured, '', 0),
            polFundValueUI: getCurrency(polFundValueUI, '', 0),
            deathBenefitUI: getCurrency(deathBenefitUI, '', 0),
            surrenderValueUI: getCurrency(surrenderValueUI, '', 0),
            fees: getCurrency(fees, '', 0),
            InsuranceCharge: getCurrency(InsuranceCharge, '', 0)
          });
          if (incRowCntLineBreak) {
            illustrationIRR_High3.push({
              endYearAge: "",
              totPremiumPtd: "",
              sumAssured: "",
              polFundValueUI: "",
              deathBenefitUI: "",
              surrenderValueUI: "",
              fees: "",
              InsuranceCharge: "",
            });
          }
        } else if (i >= 90 && i < 120) {
          illustrationIRR_High4.push({
            endYearAge: endYearAge,
            totPremiumPtd: getCurrency(totPremiumPtd, '', 0),
            sumAssured: getCurrency(sumAssured, '', 0),
            polFundValueUI: getCurrency(polFundValueUI, '', 0),
            deathBenefitUI: getCurrency(deathBenefitUI, '', 0),
            surrenderValueUI: getCurrency(surrenderValueUI, '', 0),
            fees: getCurrency(fees, '', 0),
            InsuranceCharge: getCurrency(InsuranceCharge, '', 0)
          });
          if (incRowCntLineBreak) {
            illustrationIRR_High4.push({
              endYearAge: "",
              totPremiumPtd: "",
              sumAssured: "",
              polFundValueUI: "",
              deathBenefitUI: "",
              surrenderValueUI: "",
              fees: "",
              InsuranceCharge: "",
            });
          }
        }
      }
    }
    incRowCntLineBreak = false;
  }
  var result = [];
  if (illustrationIRR_High2.length < 35) {
    showTextThisPage2 = true;
  } else if (illustrationIRR_High3.length < 35) {
    showTextThisPage3 = true;
  } else if (illustrationIRR_High4.length < 35) {
    showTextThisPage4 = true;
  }
  result = {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      releaseVersion: "1"
    },
    cover: {
      sameAs: quotation.sameAs,
      ccy: quotation.ccy,
      initPremiumTop: getCurrency(initPremiumTop, '', 0),
      sumAssuredTop: getCurrency(sumAssuredTop, '', 0),
      ccySign: getCurrencyDesc(quotation.ccy),
      polType: quotation.policyOptions.policytype,
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      in_IRR: in_IRR,
      lapseDisclosure: lapseDisclosure,
      insured: {
        name: quotation.pFullName,
        gender: getGenderDesc(quotation.pGender),
        dob: new Date(quotation.pDob).format(extraPara.dateFormat),
        age: quotation.pAge,
        smoking: getSmokingDesc(quotation.pSmoke)
      }
    },
    illustration: {
      illustrationIRR_High: illustrationIRR_High,
      illustrationIRR_High2: illustrationIRR_High2,
      illustrationIRR_High3: illustrationIRR_High3,
      illustrationIRR_High4: illustrationIRR_High4,
      showTextThisPage2: showTextThisPage2,
      showTextThisPage3: showTextThisPage3,
      showTextThisPage4: showTextThisPage4
    }
  };
  if (showTextThisPage2) {
    result.hidePagesIndexArray = [2, 3];
  } else if (showTextThisPage3) {
    result.hidePagesIndexArray = [3];
  }
  return result;
}