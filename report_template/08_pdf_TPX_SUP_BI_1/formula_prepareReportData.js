function(quotation, planInfo, planDetails, extraPara) {
  var trunc = function(value, position) {
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return Number(math.divide(math.floor(math.multiply(value, scale)), scale));
  };
  var illustrations = extraPara.illustrations;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb,
  } = extraPara.company;
  var retVal = {
    'compName': compName,
    'compRegNo': compRegNo,
    'compAddr': compAddr,
    'compAddr2': compAddr2,
    'compTel': compTel,
    'compFax': compFax,
    'compWeb': compWeb
  };
  var basicPlan = quotation.plans[0];
  var basicIllustrations = illustrations[basicPlan.covCode];
  retVal.headerName = quotation.sameAs == "Y" ? quotation.pFullName : quotation.iFullName;
  retVal.headerGender = (quotation.sameAs == "Y" ? quotation.pGender : quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.headerSmoke = (quotation.sameAs == "Y" ? quotation.pSmoke : quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.headerAge = quotation.sameAs == "Y" ? quotation.pAge : quotation.iAge;
  var illustrationData = (quotation.baseProductCode === 'TPX') ? extraPara.illustrations.TPX : extraPara.illustrations.TPPX;
  var displayRowIndex = 0;
  for (var v in extraPara.illustrations) {
    if (v != quotation.baseProductCode && extraPara.illustrations[v] && extraPara.illustrations[v].length > displayRowIndex) {
      displayRowIndex = extraPara.illustrations[v].length;
    }
  }
  var biTableData = [];
  var biTdcTableData = [];
  var totalGSV = 0;
  var arrowSyb = '';
  var basicPlanPolicyTerm = Number.parseInt(quotation.plans[0].policyTerm);
  var iAge = quotation.iAge;
  var basicPlanPolicyTerm = (quotation.plans[0].policyTerm.indexOf('TA') > -1) ? Number.parseInt(quotation.plans[0].policyTerm) - quotation.iAge : Number.parseInt(quotation.plans[0].policyTerm);
  var longestRiderPolicyTerm = 0;
  var hasSV = false;
  for (var v in quotation.plans) {
    if (v !== 0) {
      var policyTerm = quotation.plans[v].policyTerm;
      var covCode = quotation.plans[v].covCode;
      var policyTermValueAndType = (policyTerm) ? policyTerm.split('_') : [];
      if (policyTermValueAndType[0] && policyTermValueAndType[1] && policyTermValueAndType[1] === 'YR' && Number.parseInt(policyTerm) > longestRiderPolicyTerm) {
        longestRiderPolicyTerm = Number.parseInt(policyTerm);
      } else if (policyTermValueAndType[0] && policyTermValueAndType[1] && policyTermValueAndType[1] === 'TA' && (Number.parseInt(policyTerm) - quotation.iAge) > longestRiderPolicyTerm) {
        longestRiderPolicyTerm = Number.parseInt(policyTerm) - quotation.iAge;
      }
      if (quotation.plans[v].covCode == 'SV') {
        hasSV = true;
      }
    }
  }
  basicPlanPolicyTerm = ((basicPlanPolicyTerm - 1) <= 0) ? 0 : basicPlanPolicyTerm - 1;
  longestRiderPolicyTerm = ((longestRiderPolicyTerm - 1) <= 0) ? 0 : longestRiderPolicyTerm - 1;
  var hasArrowSyb = false;
  for (var v in illustrationData) {
    var orginalBIRowData = illustrationData[v];
    var showIndex = Number.parseInt(v);
    var polYr = showIndex + 1;
    iAge += 1;
    arrowSyb = (polYr - 1 > basicPlanPolicyTerm && quotation.policyOptions.planType === 'renew') ? '^' : '';
    var polYr_age = polYr + '/' + iAge + arrowSyb;
    var totalPremPaid = orginalBIRowData.totRiderYearPrem;
    var totalDistribCost = orginalBIRowData.riderTdc;
    var GDB = orginalBIRowData.guaranteedDB;
    var GSV = orginalBIRowData.guaranteedSV;
    var totBpYearPrem = orginalBIRowData.guaranteedDB;
    var totbpTdc = orginalBIRowData.guaranteedDB;
    var biRowData = {
      polYr_age,
      totalPremPaid: getCurrency(totalPremPaid, ' ', 0),
      totalDistribCost: getCurrency(totalDistribCost, ' ', 0),
      GSV: getCurrency(GSV, ' ', 0)
    };
    if ((showIndex < 10 || math.mod(showIndex + 1, 5) == 0 || showIndex == displayRowIndex - 1) && v <= displayRowIndex - 1) {
      if (arrowSyb === '^') {
        hasArrowSyb = true;
      }
      biTableData.push(biRowData);
      biTdcTableData.push(biRowData);
      retVal.polYr_age = polYr_age;
      retVal.totalPremPaid = getCurrency(totalPremPaid, ' ', 0);
      retVal.totalDistribCost = getCurrency(totalDistribCost, ' ', 0);
      retVal.GDB = (hasSV) ? getCurrency(GDB, ' ', 0) : getCurrency(0, ' ', 0);
    }
  }
  retVal.illustrateData_rider = biTableData;
  retVal.illustrateData_totalRiderDistribCost = biTdcTableData;
  var ccy = quotation.ccy;
  var polCcy = ccy;
  var ccySyb = '';
  if (ccy == 'SGD') {
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    ccySyb = 'US$';
  } else if (ccy == 'AUD') {
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    ccySyb = '£';
  }
  retVal.polCcy = polCcy;
  retVal.ccySyb = ccySyb;
  var paymentModeTitle = '';
  if (quotation.paymentMode == 'A') {
    paymentModeTitle = 'Annual';
  } else if (quotation.paymentMode == 'S') {
    paymentModeTitle = 'Semi-Annual';
  } else if (quotation.paymentMode == 'Q') {
    paymentModeTitle = 'Quarterly';
  } else if (quotation.paymentMode == 'M') {
    paymentModeTitle = 'Monthly';
  }
  retVal.paymentModeTitle = paymentModeTitle;
  retVal.basicAnnualPrem = getCurrency(planInfo.yearPrem, ' ', 2);
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  retVal.quoPlanType = (quotation.policyOptions.planType === 'renew') ? 'Y' : 'N';
  if (quotation.plans && quotation.plans.length == 1) {
    retVal.hidePagesIndexArray = [0, 1];
  }
  if (hasArrowSyb && quotation.policyOptions.planType === 'renew') {
    retVal.noteType = 'notes';
  } else if (quotation.policyOptions.planType === 'toAge' && quotation.policyOptions.indexation === 'Y') {
    retVal.noteType = 'specialfeature';
  } else {
    retVal.noteType = '';
  }
  return retVal;
}