function(quotation, planInfo, planDetails, extraPara) { /*critic_care*/
  var get_currency_symbol = () => {
    var ccy = quotation.ccy;
    var ccySyb = '';
    if (ccy == 'SGD') {
      ccySyb = 'S$';
    } else if (ccy == 'USD') {
      ccySyb = 'US$';
    } else if (ccy == 'ASD') {
      ccySyb = 'A$';
    } else if (ccy == 'EUR') {
      ccySyb = '€';
    } else if (ccy == 'GBP') {
      ccySyb = '£';
    }
    return ccySyb;
  };
  var get_policy_currency = () => {
    var ccy = quotation.ccy;
    var polCcy = '';
    var ccySyb = '';
    if (ccy == 'SGD') {
      polCcy = 'Singapore Dollars';
      ccySyb = 'S$';
    } else if (ccy == 'USD') {
      polCcy = 'US Dollars';
      ccySyb = 'US$';
    } else if (ccy == 'ASD') {
      polCcy = 'Australian Dollars';
      ccySyb = 'A$';
    } else if (ccy == 'EUR') {
      polCcy = 'Euro';
      ccySyb = '€';
    } else if (ccy == 'GBP') {
      polCcy = 'British Pound';
      ccySyb = '£';
    }
    return polCcy;
  };
  var reportData = {};
  Object.assign(reportData, extraPara.company);
  reportData.sex = quotation.iGender == 'M' ? 'Male' : 'Female';
  reportData.sysDateDesc = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  reportData.riskCommenDateDesc = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  reportData.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  reportData.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  reportData.insurerGenderTitle = (quotation.sameAs == "Y" ? quotation.pGender : quotation.iGender) == 'M' ? 'Male' : 'Female';
  reportData.insurerSmokerTitle = (quotation.sameAs == "Y" ? quotation.pSmoke : quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  reportData.ccySyb = get_currency_symbol();
  reportData.policyCurrency = get_policy_currency();
  reportData.paymentModeDesc = (quotation.paymentMode == "A" ? "Annual" : (quotation.paymentMode == "S" ? "Semi-Annual" : (quotation.paymentMode == "Q" ? "Quarterly" : "Monthly")));
  var basicPlan = quotation.plans[0];
  var selectedPolicyTermYr = basicPlan.policyTermYr;
  reportData.basicPlanName = basicPlan.covName.en;
  reportData.basicSumAssured = getCurrency(basicPlan.sumInsured, '', 0);
  reportData.basicAnnualPremium = getCurrency(basicPlan.yearPrem, ' ', 2);
  reportData.premiumPaymentTerm = basicPlan.premTermDesc;
  reportData.polTermDesc = basicPlan.polTermDesc;
  var basic_plan_benefit_init = () => {
    var illustrations = extraPara.illustrations[planInfo.covCode];
    var basicPlan = illustrations[0];
    var returnPlan = {};
    returnPlan.cibReset = getCurrency(basicPlan.cibReset, '', 0);
    returnPlan.surgery = getCurrency(basicPlan.surgery, '', 0);
    returnPlan.reconstructSurgery = getCurrency(basicPlan.reconstructSurgery, '', 0);
    returnPlan.supportBenefit = getCurrency(basicPlan.supportBenefit, '', 0);
    returnPlan.gtdDB = getCurrency(basicPlan.gtdDB, '', 0);
    returnPlan.bieannialPolAmt = getCurrency(basicPlan.bieannialPolAmt, '', 0);
    returnPlan.wp = getCurrency(basicPlan.wp, '', 0);
    returnPlan.childCov = getCurrency(basicPlan.childCov, '', 0);
    returnPlan.totalBenefit = getCurrency(basicPlan.totalBenefit, '', 0);
    return returnPlan;
  };
  var illustration_init = () => {
    var iAge = quotation.iAge;
    var illustrations = extraPara.illustrations[planInfo.covCode];
    var deathBenefit = [];
    var totalDistributionCostMainBi = [];
    var totalDistributionCostLast = [];
    var totalDistributionCostRowDefault = {
      policyYearAge: "",
      totalPremiumPaidToDate: "",
      totalDistributionCostToDate: "",
      totalmaxGuaranteedCIB: ""
    };
    var deathBenefitRowDefault = {
      policyYearAge: "",
      totYearPrem: "",
      gtdDB: "",
      cibReset: "",
      cib: "",
      surgery: "",
      reconstructSurgery: "",
      supportBenefit: ""
    };
    var showNote = 'N';
    if (illustrations instanceof Array) {
      var row = {};
      var polTerm = illustrations.length;
      illustrations.forEach(function(illustration, i) {
        var policyYear = i + 1;
        var age = iAge + i + 1;
        if (age <= 75) {
          var policyYearAge = policyYear + '/' + age;
          if (policyYear > selectedPolicyTermYr) {
            policyYearAge += "^";
            showNote = 'Y';
          }
          var totalDistributionCostRow = {
            policyYearAge: policyYearAge,
            totalPremiumPaidToDate: getCurrency(illustration.totYearPrem, '', 0),
            totalDistributionCostToDate: getCurrency(illustration.tdc, '', 0),
            totalmaxGuaranteedCIB: getCurrency(illustration.maxGuaranteedCIB, '', 0),
          };
          var deathBenefitRow = {
            policyYearAge: policyYearAge,
            totYearPrem: getCurrency(illustration.totYearPrem, '', 0),
            gtdDB: getCurrency(illustration.gtdDB, '', 0),
            cibReset: getCurrency(illustration.cibReset, '', 0),
            cib: getCurrency(illustration.cib, '', 0),
            surgery: getCurrency(illustration.surgery, '', 0),
            reconstructSurgery: getCurrency(illustration.reconstructSurgery, '', 0),
            supportBenefit: getCurrency(illustration.supportBenefit, '', 0)
          };
          if (policyYear <= 10 || policyYear <= polTerm && policyYear % 5 === 0 || policyYear === polTerm || age === 65) {
            totalDistributionCostMainBi.push(totalDistributionCostRow);
            deathBenefit.push(deathBenefitRow);
          }
          if ([55, 60, 65, 70, 75].indexOf(age) >= 0) {
            totalDistributionCostLast.push(Object.assign({}, totalDistributionCostRow, {
              policyYearAge: 'Age ' + age
            }));
          }
        }
      });
    }
    return {
      totalDistributionCostMainBi: totalDistributionCostMainBi.concat(totalDistributionCostLast),
      deathBenefit: deathBenefit,
      showNote
    };
  };
  reportData.illustration = illustration_init();
  reportData.basicPlan = basic_plan_benefit_init();
  return reportData;
}