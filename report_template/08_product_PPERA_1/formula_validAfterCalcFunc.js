function(quotation, planInfo, planDetail) {
  var planCodeMap = planDetail.planCodeMapping;
  if (planInfo.policyTerm) {
    let policyTerm = (planInfo.policyTerm == 'toAge65') ? 'Until Age 65' : planInfo.policyTerm;
    var idx = planCodeMap && planCodeMap.policyTerm ? planCodeMap.policyTerm.indexOf(policyTerm) : -1;
    if (idx >= 0) planInfo.planCode = planCodeMap.planCode[idx];
  }
}