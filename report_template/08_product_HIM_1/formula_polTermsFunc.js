function(planDetail, quotation) {
  var options = [];
  if (quotation.iAge <= 55) {
    options.push({
      value: '10_YR',
      title: '10 Years'
    });
  }
  if (quotation.iAge <= 45) {
    options.push({
      value: '20_YR',
      title: '20 Years'
    });
  }
  options.push({
    value: '65_TA',
    title: 'To Age 65'
  });
  return options;
}