function(planDetail, quotation, planDetails) { /*Get default Policy Options [planDetail=template, quotation=values]*/
  quotDriver.preparePolicyOptions(planDetail, quotation, planDetails);
  var pdPolicyOptions = planDetail.policyOptions;
  var adjustNum = planDetail.formulas.adjustNum; /*item={GroupId: {triggerId}}*/
  var groupTriggerIdMap = {
    topUp: {
      id: "topUpSelect"
    },
    rsp: {
      id: "rspSelect"
    },
    wd1: {
      id: "wdSelect"
    },
    wd2: {
      id: "wdSelect"
    },
    wd3: {
      id: "wdSelect"
    }
  }; /*Update planDetail.policyOptions*/
  planDetail.inputConfig.policyOptions.forEach(function(pPolicyOption, poIndex) {
    var trigger = groupTriggerIdMap[pPolicyOption.groupID];
    if (trigger && trigger.id !== pPolicyOption.id) {
      pPolicyOption.disable = quotation.policyOptions[trigger.id] ? 'N' : 'Y';
      pPolicyOption.mandatory = quotation.policyOptions[trigger.id] && pPolicyOption.type !== 'note' && pPolicyOption.type !== 'emptyBlock' ? 'Y' : 'N';
    }
    if (pPolicyOption.id === 'insuranceCharge') {
      if (quotation.policyOptions.deathBenefit === 'basic') {
        pPolicyOption.options = [];
        pPolicyOption.disable = 'Y';
        quotation.policyOptions.insuranceCharge = null;
      } else if (quotation.policyOptions.deathBenefit === 'enhanced') {
        pPolicyOption.options = pdPolicyOptions[poIndex].options;
        pPolicyOption.disable = 'N';
      }
    } else if (pPolicyOption.id === 'topUpSelect') {
      pPolicyOption.clearFunds = "Y";
      pPolicyOption.warningMsg = "quotation.fund.warning.changeTopUp";
    } else if (pPolicyOption.id === 'topUpAmt') {
      if (quotation.ccy === 'SGD') {
        pPolicyOption.min = 5000;
      } else if (quotation.ccy === 'USD') {
        pPolicyOption.min = 3500;
      }
      pPolicyOption.hintMsg = 'Minimum Top-up Premium is ' + quotation.ccy + pPolicyOption.min + '. Top-up Premium must be in multiples of ' + pPolicyOption.factor + '.';
      quotation.policyOptions.topUpAmt = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.topUpAmt, pPolicyOption.min, null, pPolicyOption.factor);
    } else if (pPolicyOption.id === 'rspSelect') {
      if (quotation.ccy === 'USD') {
        quotation.policyOptions.rspSelect = null;
      }
      pPolicyOption.disable = quotation.ccy === 'USD' ? 'Y' : 'N';
    } else if (pPolicyOption.id === 'rspPayFreq') {
      if (!quotation.policyOptions.rspSelect) {
        pPolicyOption.options = [];
        quotation.policyOptions.rspPayFreq = null;
      } else {
        pPolicyOption.options = pdPolicyOptions[poIndex].options;
      }
    } else if (pPolicyOption.id === 'rspAmount') {
      pPolicyOption.min = 100;
      pPolicyOption.hintMsg = 'Minimum RSP Amount is ' + quotation.ccy + pPolicyOption.min + '. RSP Amount must be in multiples of ' + pPolicyOption.factor + '.';
      quotation.policyOptions.rspAmount = pPolicyOption.disable === 'Y' || quotation.ccy === 'USD' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.rspAmount, pPolicyOption.min, null, pPolicyOption.factor);
    } else if (pPolicyOption.id === 'wdAmount') {
      if (quotation.policyOptions.wdFreq === 'A') {
        pPolicyOption.min = 1200;
      } else if (quotation.policyOptions.wdFreq === 'S') {
        pPolicyOption.min = 600;
      } else if (quotation.policyOptions.wdFreq === 'Q') {
        pPolicyOption.min = 300;
      } else if (quotation.policyOptions.wdFreq === 'M') {
        pPolicyOption.min = 100;
      }
      pPolicyOption.hintMsg = 'Minimum annual Withdrawal Amount is ' + quotation.ccy + pPolicyOption.min + '. Withdrawal amount must be in multiples of ' + pPolicyOption.factor + '.';
      quotation.policyOptions.wdAmount = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.wdAmount, pPolicyOption.min, null, pPolicyOption.factor);
    } else if (pPolicyOption.id === 'wdFreq') {
      if (!quotation.policyOptions.wdSelect) {
        pPolicyOption.options = [];
        quotation.policyOptions.wdFreq = null;
      } else {
        pPolicyOption.options = pdPolicyOptions[poIndex].options;
      }
    } else if (pPolicyOption.id === 'wdFromAge') {
      var premTerm = quotation.plans.length > 0 ? Number(quotation.plans[0].premTerm) : 5;
      var minFromAge = quotation.iAge + premTerm;
      pPolicyOption.hintMsg = "The earliest age for withdrawal is " + minFromAge + ".";
      quotation.policyOptions.wdFromAge = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.wdFromAge, minFromAge, 98, pPolicyOption.factor);
    } else if (pPolicyOption.id === 'wdToAge') {
      pPolicyOption.hintMsg = "Regular Withdrawal end age must be equal to or greater than start age.";
      quotation.policyOptions.wdToAge = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.wdToAge, math.min(98, quotation.policyOptions.wdFromAge), null, pPolicyOption.factor);
    }
  });
  quotation.policyOptions.paymentMethod = 'cash';
  planDetail.inputConfig.topUpSelect = quotation.policyOptions.topUpSelect && quotation.policyOptions.topUpAmt;
  return planDetail;
}