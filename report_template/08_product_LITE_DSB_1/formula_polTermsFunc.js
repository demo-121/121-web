function(planDetail, quotation) {
  var policyTermList = [];
  var hasTa60 = false;
  _.each([10, 15, 20, 25], (val) => {
    var temp = quotation.iAge + val;
    if (temp <= 60) {
      policyTermList.push({
        value: '' + val,
        title: val + ' years'
      });
      if (temp == 60) {
        hasTa60 = true;
      }
    }
  });
  if ((quotation.iAge <= 55) && (hasTa60 == false)) {
    policyTermList.push({
      value: '60',
      title: 'To Age 60'
    });
  }
  return policyTermList;
}