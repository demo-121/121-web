function(planDetail, quotation) {
  var policyTermList = [];
  if (quotation.policyOptions.retirementAge === undefined) {
    return null;
  }
  if (quotation.policyOptions.payoutTerm === undefined) {
    return null;
  }
  if (quotation.plans[0].premTerm === undefined) {
    return null;
  }
  var premiumTerm = quotation.plans[0].premTerm;
  var policyTerm = 0;
  var polTermDesc = '';
  if (quotation.iAge <= 45) {
    policyTerm = premiumTerm;
    polTermDesc = policyTerm + ' Years';
  }
  policyTermList.push({
    value: policyTerm,
    title: polTermDesc,
    default: true
  });
  return policyTermList;
}