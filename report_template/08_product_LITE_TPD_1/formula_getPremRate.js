function(quotation, planInfo, planDetail, age) {
  var planCode = "MBT";
  var premTermYr = "";
  if (planInfo.premTerm.indexOf('YR') > -1) {
    planCode = Number.parseInt(planInfo.premTerm) + planCode;
    premTermYr = Number.parseInt(planInfo.premTerm);
  } else {
    planCode = planCode + Number.parseInt(planInfo.premTerm);
    premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
  }
  var MB_codeList = {
    "2": "A",
    "3": "B",
    "4": "C",
    "5": "D",
    "6": "E",
    "7": "F"
  };
  var MB_code = MB_codeList[quotation.policyOptions.multiFactor];
  var rateKey = planCode + MB_code + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS');
  return planDetail.rates.premRate[rateKey][quotation.iAge];
}