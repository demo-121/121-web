function(planDetail, quotation) {
  var polTerm = 65 - quotation.iAge;
  return [{
    value: "yrs" + polTerm,
    title: polTerm + " Years",
    default: true
  }];
}