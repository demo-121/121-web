function(quotation, planInfo, planDetails) {
  if (quotation.plans[0].premTerm) {
    planInfo.premTerm = quotation.plans[0].premTerm;
    planInfo.premTermDesc = quotation.plans[0].premTermDesc;
  }
  var oldMap = {
    "2.5": 1400000,
    "3.5": 1000000,
    "5": 700000
  };
  var youngMap = {
    "2.5": 400000,
    "3.5": 285000,
    "5": 200000
  };
  var max_value = quotation.iAge < 18 ? youngMap[quotation.policyOptions.multiFactor] : oldMap[quotation.policyOptions.multiFactor];
  if (quotation.plans[0].sumInsured > max_value) {
    planInfo.sumInsured = max_value;
  } else {
    planInfo.sumInsured = quotation.plans[0].sumInsured;
  }
  if (quotation.policyOptions.multiFactor && planInfo.premTerm) { /* planCode needed for prem calculation */
    var planCode = 'ADBX';
    var premTermYr;
    var policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
    if (planInfo.policyTerm.indexOf('TA') > -1) {
      policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    } else {
      policyTermYr = Number.parseInt(planInfo.policyTerm);
    }
    planInfo.premTermYr = premTermYr;
    planInfo.policyTermYr = policyTermYr;
    var mbCodeMap = {
      "5": "A",
      "3.5": "B",
      "2.5": "C"
    };
    var mbCodeMapOlder = {
      "2.50": "A",
      "2.75": "B",
      "1.25": "C"
    };
    var group = quotation.iAge < 71 ? mbCodeMap[quotation.policyOptions.multiFactor] : mbCodeMapOlder[quotation.policyOptions.multiFactor];
    if (planInfo.premTerm.indexOf('YR') > -1) {
      planCode = Number.parseInt(planInfo.premTerm) + planCode + group;
    } else {
      planCode = planCode + group + Number.parseInt(planInfo.premTerm);
    }
    planInfo.planCode = planCode;
    planInfo.packagedRider = true;
    quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
    if (planInfo.sumInsured) {
      planInfo.multiplyBenefit = planInfo.sumInsured * quotation.policyOptions.multiFactor;
      planInfo.multiplyBenefitAfter70 = planInfo.multiplyBenefit * 0.5;
      var policyYearRefer = 0;
      if (planInfo.premTerm.indexOf('YR') > -1) {
        policyYearRefer = Number.parseInt(planInfo.premTerm);
      } else {
        policyYearRefer = Number.parseInt(planInfo.premTerm) - quotation.iAge;
      }
      if (planInfo.premium) {
        var crate;
        var channel = quotation.agent.dealerGroup.toUpperCase();
        var commission_rate = planDetails['ADBX'].rates.commissionRate[channel];
        planInfo.cummComm = [0];
        var cummComm = 0;
        for (var rate in commission_rate) {
          if (policyYearRefer > 25) {
            crate = commission_rate[rate][0];
          } else if (policyYearRefer >= 20 && policyYearRefer <= 24) {
            crate = commission_rate[rate][1];
          } else {
            crate = commission_rate[rate][2];
          }
          cummComm += crate * planInfo.premium;
          planInfo.cummComm.push(crate);
        }
      }
    } else {
      planInfo.multiplyBenefit = null;
      planInfo.multiplyBenefitAfter70 = null;
      planInfo.premium = null;
    }
  }
}