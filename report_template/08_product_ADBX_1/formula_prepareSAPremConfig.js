function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var bpInfo = quotation.plans[0];
  var amount = bpInfo.sumInsured > 0 ? bpInfo.sumInsured : 0;
  var oldMap = {
    "2.5": 1400000,
    "3.5": 1000000,
    "5": 700000
  };
  var youngMap = {
    "2.5": 400000,
    "3.5": 285000,
    "5": 200000
  };
  var multiFactor = quotation.policyOptions.multiFactor;
  var max_value = quotation.iAge < 18 ? youngMap[multiFactor] : oldMap[multiFactor];
  if (multiFactor) {
    planDetail.inputConfig.benlim = {
      min: 25000,
      max: max_value
    };
  }
}