function(quotation, planInfo, planDetail) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var countryCode = quotation.iResidence;
  var isCountryMatch = (countryCode === "R51" || countryCode === "R52" || countryCode === "R53" || countryCode === "R54");
  var premium = null;
  var sumInsured = planInfo.sumInsured;
  if (quotation.paymentMode == 'A') {
    if (planInfo.premTerm && sumInsured) {
      if (isCountryMatch) {
        var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
        var premBeforeLsd = roundDown(math.multiply(premRate, sumInsured, 0.001), 2);
        var lsd = 0;
        var premAfterLsd = math.subtract(premBeforeLsd, roundDown(math.multiply(sumInsured, 0.001, lsd), 2));
        var residenceLoading = math.bignumber(runFunc(planDetail.formulas.getResidenceLoading, quotation, planInfo, planDetail));
        premAfterLsd = math.add(premAfterLsd, roundDown(math.multiply(residenceLoading, sumInsured, 0.001), 2));
        premium = premAfterLsd;
      } else {
        var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
        var premAfterLsd = roundDown(math.multiply(premRate, sumInsured, 0.001), 2);
        premium = premAfterLsd;
      }
      return math.number(premium);
    }
  } else { /** This assumes that the planInfo.yearPrem is calculated first.*/
    var modalFactor = 1;
    for (var p in planDetail.payModes) {
      if (planDetail.payModes[p].mode === quotation.paymentMode) {
        modalFactor = planDetail.payModes[p].factor;
        break;
      }
    }
    premium = roundDown(math.multiply(math.bignumber(planInfo.yearPrem), modalFactor), 2);
    return math.number(premium);
  }
  return null;
}