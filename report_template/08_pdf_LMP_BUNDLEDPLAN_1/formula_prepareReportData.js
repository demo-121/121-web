function(quotation, planInfo, planDetails, extraPara) { /*lmp*/
  var round = function(value, position) {
    var num = Number(value);
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(num, scale)), scale);
  };
  var getDiff = function(diff) {
    if (diff < 0) {
      diff = diff * -1;
      diff = getCurrency(diff, '$', 0);
      diff = "-" + diff;
    } else {
      diff = getCurrency(diff, '$', 0);
    }
    return diff;
  };
  var getDiffStr = function(diff) {
    var sign = diff.charAt(0);
    if (sign == '-') {
      diff = diff.substr(1);
      diff = '-$' + diff;
    } else {
      diff = '$' + diff;
    }
    return diff;
  };
  var basicPlan = quotation.plans[0];
  var company = extraPara.company;
  var get_currency_symbol = function() {
    var ccy = quotation.ccy;
    var ccySyb = '';
    if (ccy == 'SGD') {
      ccySyb = 'S$';
    } else if (ccy == 'USD') {
      ccySyb = 'US$';
    } else if (ccy == 'ASD') {
      ccySyb = 'A$';
    } else if (ccy == 'EUR') {
      ccySyb = '€';
    } else if (ccy == 'GBP') {
      ccySyb = '£';
    }
    return ccySyb;
  };
  var get_policy_currency = function() {
    var ccy = quotation.ccy;
    var polCcy = '';
    var ccySyb = '';
    if (ccy == 'SGD') {
      polCcy = 'Singapore Dollars';
      ccySyb = 'S$';
    } else if (ccy == 'USD') {
      polCcy = 'US Dollars';
      ccySyb = 'US$';
    } else if (ccy == 'ASD') {
      polCcy = 'Australian Dollars';
      ccySyb = 'A$';
    } else if (ccy == 'EUR') {
      polCcy = 'Euro';
      ccySyb = '€';
    } else if (ccy == 'GBP') {
      polCcy = 'British Pound';
      ccySyb = '£';
    }
    return polCcy;
  };
  var conditions_init = function() {
    var residencyCountryOptions, residencyCityOptions, iRCountry = "",
      iRCity = "",
      pRCountry = "",
      pRCity = "",
      iOtherCountry = "",
      pOtherCountry = "";
    residencyCountryOptions = extraPara.optionsMap.residency.options;
    residencyCityOptions = extraPara.optionsMap.city.options;
    if (quotation.iResidence === 'R128') {
      iOtherCountry = 'Y';
    }
    if (quotation.pResidence === 'R128') {
      pOtherCountry = 'Y';
    }
    for (let v in residencyCityOptions) {
      if (residencyCityOptions[v].value === quotation.pResidenceCity) {
        pRCity = residencyCityOptions[v].title.en;
      }
      if (residencyCityOptions[v].value === quotation.iResidenceCity) {
        iRCity = residencyCityOptions[v].title.en;
      }
    }
    for (let v in residencyCountryOptions) {
      if (residencyCountryOptions[v].value === quotation.pResidence) {
        pRCountry = residencyCountryOptions[v].title.en;
      }
      if (residencyCountryOptions[v].value === quotation.iResidence) {
        iRCountry = residencyCountryOptions[v].title.en;
      }
    }
    var show_suppBiPage = 'N';
    var plans = quotation.plans;
    plans.forEach(plan => {
      var covCode = plan.covCode;
      if (covCode !== "LMP" && covCode !== "ADBX" && covCode !== "TPDX" && covCode !== "MBX") {
        show_suppBiPage = 'Y';
      }
    });
    return {
      same_as: quotation.sameAs,
      iOtherCountry,
      pOtherCountry,
      pRCity,
      iRCity,
      pRCountry,
      iRCountry,
      show_suppBiPage
    };
  };
  var cover_init = function() {
    return {
      genDate: new Date(quotation.createDate).format(extraPara.dateFormat)
    };
  };
  var proposer_init = function() {
    return {
      assured_name: quotation.pFullName,
      assured_gender: quotation.pGender == "M" ? "Male" : "Female",
      assured_age: quotation.pAge,
      comm_date: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      assured_dob: new Date(quotation.pDob).format(extraPara.dateFormat),
      assured_smoke: quotation.pSmoke == "N" ? "Non-Smoker" : "Smoker",
    };
  };
  var life_assured_init = function() {
    return {
      l_assured_name: quotation.iFullName,
      l_assured_gender: quotation.iGender == "M" ? "Male" : "Female",
      l_assured_age: quotation.iAge,
      l_comm_date: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      l_assured_dob: new Date(quotation.iDob).format(extraPara.dateFormat),
      l_assured_smoke: quotation.iSmoke == "N" ? "Non-Smoker" : "Smoker"
    };
  };
  var basic_plan_init = function() {
    return {
      paymentModeDesc: (quotation.paymentMode == "A" ? "Annual" : (quotation.paymentMode == "S" ? "Semi-Annual" : (quotation.paymentMode == "Q" ? "Quarterly" : "Monthly"))),
      policy_currency: get_policy_currency(),
      currency_symbol: get_currency_symbol()
    };
  };
  var your_plan_init = function() {
    var multiFactorDesc = quotation.policyOptionsDesc.multiFactor.en;
    return {
      plans: quotation.plans.map((plan) => {
        if (!plan.sumInsured) {
          plan.sumInsured = 0;
        }
        if (!plan.multiplyBenefit) {
          plan.multiplyBenefit = 0;
        }
        let sum_assured = "";
        let multiplyBenefit = "";
        let covCode = plan.covCode;
        let planName = plan.covName.en;
        if (covCode === "MBX") {
          sum_assured = "-";
          planName += " - " + multiFactorDesc;
        } else {
          sum_assured = getCurrency(round(plan.sumInsured, 0), '', 0);
        }
        if (covCode === "MBX" || covCode === "ADBX" || covCode === "TPDX" || covCode === "ECIX_LMP" || covCode === "CIBX_LMP") {
          multiplyBenefit = getCurrency(round(plan.multiplyBenefit, 0), '', 0);
        } else {
          multiplyBenefit = "-";
        }
        if (covCode === "WPN_LMP" || covCode === "PPEPS_LMP" || covCode === "PPERA_LMP") {
          sum_assured = "-";
        }
        return {
          covCode,
          name: planName,
          polTermDesc: plan.polTermDesc,
          premTermDesc: plan.premTermDesc,
          sum_assured: sum_assured,
          multiplyBenefit: multiplyBenefit
        };
      })
    };
  };
  var your_premimun_init = function() {
    var multiFactorDesc = quotation.policyOptionsDesc.multiFactor.en;
    var filterPlans = quotation.plans.filter(function(plan, index, array) {
      return plan.yearPrem != null && plan.halfYearPrem != null && plan.quarterPrem != null && plan.monthPrem != null;
    });
    return {
      plans: filterPlans.map((plan) => {
        let covCode = plan.covCode;
        let planName = plan.covName.en;
        if (covCode === "MBX") {
          planName += " - " + multiFactorDesc;
        }
        return {
          covCode,
          name: planName,
          annual_pre: getCurrency(plan.yearPrem, ' ', 2),
          semi_annual_pre: getCurrency(plan.halfYearPrem, ' ', 2),
          quarterly_pre: getCurrency(plan.quarterPrem, ' ', 2),
          monthly_pre: getCurrency(plan.monthPrem, ' ', 2),
        };
      })
    };
  };
  var total_premimu_init = function() {
    return {
      total_annual_pre: getCurrency(quotation.totYearPrem, ' ', 2),
      total_semi_annual_pre: getCurrency(quotation.totHalfyearPrem, ' ', 2),
      total_quarterly_pre: getCurrency(quotation.totQuarterPrem, ' ', 2),
      total_monthly_pre: getCurrency(quotation.totMonthPrem, ' ', 2)
    };
  };
  var footer_init = function() {
    let planCodes = "";
    let numPlans = quotation.plans.length;
    quotation.plans.forEach((plan, i) => {
      let covCode = plan.covCode;
      if (covCode != 'MBX' && covCode != 'ADBX' && covCode != 'TPDX') {
        if (covCode != 'WPN_LMP') {
          planCodes += plan.planCode;
        }
        if (covCode === 'WPN_LMP') {
          planCodes += 'WP';
        }
        if (i < numPlans - 1) {
          planCodes += ', '
        }
      }
    });
    return {
      compName: company.compName,
      compName_cap: company.compName.toUpperCase(),
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      planCode: planCodes
    };
  };
  var others_init = function() {
    return {
      age65orLater: ((quotation.iAge + 40) > 65) ? (quotation.iAge + 40) : 65,
      lowIRR: 3.25,
      highIRR: 4.75
    };
  };
  var mainbi_init = function() {
    var paymentModeTitle = '';
    if (quotation.paymentMode == 'A') {
      paymentModeTitle = 'Annual';
    } else if (quotation.paymentMode == 'S') {
      paymentModeTitle = 'Semi-Annual';
    } else if (quotation.paymentMode == 'Q') {
      paymentModeTitle = 'Quarterly';
    } else if (quotation.paymentMode == 'M') {
      paymentModeTitle = 'Monthly';
    }
    var plans = quotation.plans;
    var MB1_Death = "";
    var MB2_Death = "";
    var MB1_TPD = "";
    var MB2_TPD = "";
    var MB1_ADB = "";
    var MB2_ADB = "";
    plans.forEach(plan => {
      let covCode = plan.covCode;
      if (covCode === "MBX") {
        MB1_Death = getCurrency(plan.multiplyBenefit, ' ', 0);
        MB2_Death = getCurrency(plan.multiplyBenefitAfter70, ' ', 0);
      } else if (covCode === "TPDX") {
        MB1_TPD = getCurrency(plan.multiplyBenefit, ' ', 0);
        MB2_TPD = getCurrency(plan.multiplyBenefitAfter70, ' ', 0);
      } else if (covCode === "ADBX") {
        MB1_ADB = getCurrency(plan.multiplyBenefit, ' ', 0);
        MB2_ADB = getCurrency(plan.multiplyBenefitAfter70, ' ', 0);
      }
    });
    return {
      Basic_Policy_Name: basicPlan.covName.en,
      Sum_Assured: getCurrency(basicPlan.sumInsured, ' ', 0),
      Multiplying_Factor: quotation.policyOptionsDesc.multiFactor.en,
      MB1_Death: MB1_Death,
      MB1_TPD: MB1_TPD,
      MB1_ADB: MB1_ADB,
      MB2_Death: MB2_Death,
      MB2_TPD: MB2_TPD,
      MB2_ADB: MB2_ADB,
      Payment_Frequency: paymentModeTitle,
      Annual_Premium: getCurrency(quotation.totYearPrem, ' ', 2),
      Multiplier_Benefit_1: getCurrency(basicPlan.multiplyBenefit, ' ', 0)
    };
  };
  var illustration_init = function() {
    var iAge = quotation.iAge;
    var illustrations = extraPara.illustrations[planInfo.covCode];
    var deathBenefit = [];
    var surrenderValue = [];
    var maturityValue = [];
    var deduction = [];
    var totalDistributionCostMainBi = [];
    var deathBenefitLast = [];
    var surrenderValueLast = [];
    var deductionsLast = [];
    var totalDistributionCostLast = [];
    var totalDistributionCostSuppBi = [];
    var totalDistributionCostSuppBiLast = [];
    var deathBenefitRowDefault = {
      policyYearAge: "",
      totalPremiumPaidToDate: "",
      multiplierBenefit: "",
      guaranteedDeathBenefit: "",
      nonGuaranteedDeathBenefitLow: "",
      totalDeathBenefitLow: "",
      nonGuaranteedDeathBenefitHigh: "",
      totalDeathBenefitHigh: ""
    };
    var surrenderValueRowDefault = {
      policyYearAge: "",
      totalPremiumPaidToDate: "",
      guaranteed: "",
      nonGuaranteedLow: "",
      totalLow: "",
      nonGuaranteedHigh: "",
      totalHigh: ""
    };
    var deductionsRowDefault = {
      policyYearAge: "",
      totalPremiumPaidToDate: "",
      valueOfPremiumPaidToDateLow: "",
      effectOfDeductionToDateLow: "",
      totalSurrenderValueLow: "",
      valueOfPremiumPaidToDateHigh: "",
      effectOfDeductionToDateHigh: "",
      totalSurrenderValueHigh: ""
    };
    var totalDistributionCostRowDefault = {
      policyYearAge: "",
      totalPremiumPaidToDate: "",
      totalDistributionCostToDate: ""
    };
    var Annual_Premium = 0;
    if (illustrations instanceof Array) {
      var row = {};
      var polTerm = illustrations.length;
      var sLow = '3.25';
      var sHigh = '4.75';
      illustrations.forEach(function(illustration, i) {
        var policyYear = i + 1;
        var age = iAge + i + 1;
        var policyYearAge = policyYear + ' / ' + age;
        if (policyYear === 1) {
          Annual_Premium = getCurrency(illustration.annualPrem, '', 2);
        }
        var deathBenefitRow = {
          policyYearAge: policyYearAge,
          totalPremiumPaidToDate: getCurrency(round(illustration.totYearPrem, 0), '', 0),
          multiplierBenefit: getCurrency(round(illustration.multiplyBenefit, 0), '', 0),
          guaranteedDeathBenefit: getCurrency(round(illustration.gtdDB, 0), '', 0),
          nonGuaranteedDeathBenefitLow: getCurrency(round(illustration.nonGtdDB[sLow], 0), '', 0),
          totalDeathBenefitLow: getCurrency(round(illustration.totDB[sLow], 0), '', 0),
          nonGuaranteedDeathBenefitHigh: getCurrency(round(illustration.nonGtdDB[sHigh], 0), '', 0),
          totalDeathBenefitHigh: getCurrency(round(illustration.totDB[sHigh], 0), '', 0)
        };
        var surrenderValueRow = {
          policyYearAge: policyYearAge,
          totalPremiumPaidToDate: getCurrency(round(illustration.totYearPrem, 0), '', 0),
          guaranteed: getCurrency(round(illustration.gtdSV, 0), '', 0),
          nonGuaranteedLow: getCurrency(round(illustration.nonGtdSV[sLow], 0), '', 0),
          totalLow: getCurrency(round(illustration.totSV[sLow], 0), '', 0),
          nonGuaranteedHigh: getCurrency(round(illustration.nonGtdSV[sHigh], 0), '', 0),
          totalHigh: getCurrency(round(illustration.totSV[sHigh], 0), '', 0)
        };
        var deductionsRow = {
          policyYearAge: policyYearAge,
          totalPremiumPaidToDate: getCurrency(round(illustration.totYearPrem, 0), '', 0),
          valueOfPremiumPaidToDateLow: getCurrency(round(illustration.premVal[sLow], 0), '', 0),
          effectOfDeductionToDateLow: getCurrency(round(illustration.deduction[sLow], 0), '', 0),
          totalSurrenderValueLow: getCurrency(round(illustration.totSV[sLow], 0), '', 0),
          valueOfPremiumPaidToDateHigh: getCurrency(round(illustration.premVal[sHigh], 0), '', 0),
          effectOfDeductionToDateHigh: getCurrency(round(illustration.deduction[sHigh], 0), '', 0),
          totalSurrenderValueHigh: getCurrency(round(illustration.totSV[sHigh], 0), '', 0)
        };
        var totalDistributionCostRow = {
          policyYearAge: policyYearAge,
          totalPremiumPaidToDate: getCurrency(round(illustration.totYearPrem, 0), '', 0),
          totalDistributionCostToDate: getCurrency(round(illustration.tdc, 0), '', 0)
        };
        var totalDistributionCostSuppBiRow = {
          policyYearAge: policyYearAge,
          totalPremiumPaidToDate: getCurrency(round(illustration.riderPrem, 0), '', 0),
          totalDistributionCostToDate: getCurrency(round(illustration.riderComm, 0), '', 0)
        };
        if (policyYear <= 10 || policyYear <= polTerm && policyYear % 5 === 0 || policyYear === polTerm) {
          deathBenefit.push(deathBenefitRow);
          surrenderValue.push(surrenderValueRow);
          if (policyYear === polTerm) {
            maturityValue.push(Object.assign({}, surrenderValueRow, {
              policyYearAge: 'Age ' + age
            }));
          }
          deduction.push(deductionsRow);
          totalDistributionCostMainBi.push(totalDistributionCostRow);
          totalDistributionCostSuppBi.push(totalDistributionCostSuppBiRow);
        }
        if ([55, 60, 65].indexOf(age) >= 0) {
          deathBenefitLast.push(Object.assign({}, deathBenefitRow, {
            policyYearAge: 'Age ' + age
          }));
          surrenderValueLast.push(Object.assign({}, surrenderValueRow, {
            policyYearAge: 'Age ' + age
          }));
          deductionsLast.push(Object.assign({}, deductionsRow, {
            policyYearAge: 'Age ' + age
          }));
          totalDistributionCostLast.push(Object.assign({}, totalDistributionCostRow, {
            policyYearAge: 'Age ' + age
          }));
          totalDistributionCostSuppBiLast.push(Object.assign({}, totalDistributionCostSuppBiRow, {
            policyYearAge: 'Age ' + age
          }));
        }
      });
    }
    return {
      deathBenefit: deathBenefit.concat(deathBenefitRowDefault).concat(deathBenefitLast),
      surrenderValue: surrenderValue.concat(surrenderValueRowDefault).concat(surrenderValueLast),
      maturityValue: maturityValue,
      deduction: deduction.concat(deductionsRowDefault).concat(deductionsLast),
      totalDistributionCostMainBi: totalDistributionCostMainBi.concat(totalDistributionCostRowDefault).concat(totalDistributionCostLast),
      totalDistributionCostSuppBi: totalDistributionCostSuppBi,
      Annual_Premium
    };
  };
  var bundle_init = function() {
    var illustrations = extraPara.illustrations[planInfo.covCode];
    var firstIllustration = illustrations[0];
    var sLow = '3.25';
    var sHigh = '4.75';
    var lastrow_label = firstIllustration.bundleYearAge.charAt(0) == '@' ? (firstIllustration.bundleYearAge.slice(-2) - quotation.iAge) + "/" + firstIllustration.bundleYearAge.slice(-2) : firstIllustration.bundleYearAge;
    var lastrow_label_last_part = lastrow_label.slice(-2);
    var diff = firstIllustration.bundleTotPremPaid - firstIllustration.standard_tp_totalPremPaid;
    var diff_v1 = firstIllustration.lastRowEOY && illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].tp_rateReturn050s, '', 0) : 0;
    var diff_v2 = firstIllustration.lastRowEOY && illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].tp_rateReturn250s, '', 0) : 0;
    diff = getDiff(diff);
    diff_v1 = getDiffStr(diff_v1);
    diff_v2 = getDiffStr(diff_v2);
    return {
      display: quotation.iAge >= 18 ? 'Y' : 'N',
      bundle_gender: quotation.iGender == "M" ? "Male" : "Female",
      bundle_age: quotation.iAge,
      premTermYr: planInfo.premTermYr,
      policyTermYr: planInfo.policyTermYr,
      sLow: sLow,
      sHigh: sHigh,
      bundle_smoke: quotation.iSmoke == "N" ? "Non-Smoker" : "Smoker",
      bundle_plan: "AXA Life MultiProtect",
      bundle_alternative_plan: "Term Protector (To Age 99)",
      bundle_total_annual_premium: firstIllustration.abundleTotAnnPrem,
      bundle_alternative_total_annual_premium: firstIllustration.standard_tp_annualPrem,
      bundle_alternative_total_annual_premium_money: getCurrency(firstIllustration.standard_tp_annualPrem, '', 0),
      bundle_alternative_diff_annual_premium: firstIllustration.bundleTotPremPaid - firstIllustration.standard_tp_totalPremPaid,
      bundle_alternative_diff_annual_premium_money: diff,
      bundle_plan_year: firstIllustration.abundleTotAnnPrem,
      bundle_plan_year_money: getCurrency(firstIllustration.abundleTotAnnPrem, '', 0),
      bundle_plan_to_year: quotation.iAge + planInfo.premTermYr,
      bundle_plan_total_pay: firstIllustration.bundleTotPremPaid,
      bundle_plan_total_pay_money: getCurrency(firstIllustration.bundleTotPremPaid, '', 0),
      bundle_alternative_totalPremPaid: firstIllustration.standard_tp_totalPremPaid,
      bundle_alternative_totalPremPaid_money: getCurrency(firstIllustration.standard_tp_totalPremPaid, '', 0),
      bundle_guaranteed_deathb: getCurrency(firstIllustration.bundleGuaranteedDeathBs, '', 0),
      bundle_db: {
        last_row_index: lastrow_label,
        lastrow_label_last_part: lastrow_label_last_part,
        first_db: {
          index: 10 + quotation.iAge,
          db: illustrations[9] ? getCurrency(illustrations[9].bundleGuaranteedDeathBs, '', 0) : '',
          db_A: illustrations[9] ? getCurrency(illustrations[9].bundleGuaranteedBasicTotDBs[sLow], '', 0) : '',
          db_B: illustrations[9] ? getCurrency(illustrations[9].bundleGuaranteedBasicTotDBs[sHigh], '', 0) : ''
        },
        second_db: {
          index: 20 + quotation.iAge,
          db: illustrations[19] ? getCurrency(illustrations[19].bundleGuaranteedDeathBs, '', 0) : '',
          db_A: illustrations[19] ? getCurrency(illustrations[19].bundleGuaranteedBasicTotDBs[sLow], '', 0) : '',
          db_B: illustrations[19] ? getCurrency(illustrations[19].bundleGuaranteedBasicTotDBs[sHigh], '', 0) : '',
        },
        third_db: {
          index: 30 + quotation.iAge,
          db: illustrations[29] ? getCurrency(illustrations[29].bundleGuaranteedDeathBs, '', 0) : '',
          db_A: illustrations[29] ? getCurrency(illustrations[29].bundleGuaranteedBasicTotDBs[sLow], '', 0) : '',
          db_B: illustrations[29] ? getCurrency(illustrations[29].bundleGuaranteedBasicTotDBs[sHigh], '', 0) : '',
        },
        last_db: {
          db: firstIllustration.lastRowEOY && illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].bundleGuaranteedDeathBs, '', 0) : '',
          db_A: firstIllustration.lastRowEOY && illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].bundleGuaranteedBasicTotDBs[sLow], '', 0) : '',
          db_B: firstIllustration.lastRowEOY && illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].bundleGuaranteedBasicTotDBs[sHigh], '', 0) : '',
        }
      },
      bundle_alternative_db: {
        last_row_index: lastrow_label,
        lastrow_label_last_part: lastrow_label_last_part,
        first_db: {
          db: illustrations[9] ? getCurrency(illustrations[9].tp_guaranteedDBs, '', 0) : ''
        },
        second_db: {
          db: illustrations[19] ? getCurrency(illustrations[19].tp_guaranteedDBs, '', 0) : ''
        },
        third_db: {
          db: illustrations[29] ? getCurrency(illustrations[29].tp_guaranteedDBs, '', 0) : ''
        },
        last_db: {
          db: firstIllustration.lastRowEOY && illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].tp_guaranteedDBs, '', 0) : ''
        }
      },
      bundle_sv: {
        first_sv: {
          sv: illustrations[9] ? getCurrency(illustrations[9].bundleGtdSVs, '', 0) : '',
          sv_A: illustrations[9] ? getCurrency(illustrations[9].bundleGuaranteedBasicTotSVs[sLow], '', 0) : '',
          sv_B: illustrations[9] ? getCurrency(illustrations[9].bundleGuaranteedBasicTotSVs[sHigh], '', 0) : ''
        },
        second_sv: {
          sv: illustrations[19] ? getCurrency(illustrations[19].bundleGtdSVs, '', 0) : '',
          sv_A: illustrations[19] ? getCurrency(illustrations[19].bundleGuaranteedBasicTotSVs[sLow], '', 0) : '',
          sv_B: illustrations[19] ? getCurrency(illustrations[19].bundleGuaranteedBasicTotSVs[sHigh], '', 0) : ''
        },
        third_sv: {
          sv: illustrations[29] ? getCurrency(illustrations[29].bundleGtdSVs, '', 0) : '',
          sv_A: illustrations[29] ? getCurrency(illustrations[29].bundleGuaranteedBasicTotSVs[sLow], '', 0) : '',
          sv_B: illustrations[29] ? getCurrency(illustrations[29].bundleGuaranteedBasicTotSVs[sHigh], '', 0) : ''
        },
        last_sv: {
          sv: firstIllustration.lastRowEOY && illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].bundleGtdSVs, '', 0) : '',
          sv_A: illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].bundleGuaranteedBasicTotSVs[sLow], '', 0) : '',
          sv_B: illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].bundleGuaranteedBasicTotSVs[sHigh], '', 0) : ''
        }
      },
      bundle_alternative_sv: {
        first_sv: {
          prem_diff_return_05rate: illustrations[9] ? getCurrency(illustrations[9].tp_rateReturn050s, '', 0) : '',
          prem_diff_return_25rate: illustrations[9] ? getCurrency(illustrations[9].tp_rateReturn250s, '', 0) : '',
          prem_diff_return_40rate: illustrations[9] ? getCurrency(illustrations[9].tp_rateReturn400s, '', 0) : ''
        },
        second_sv: {
          prem_diff_return_05rate: illustrations[19] ? getCurrency(illustrations[19].tp_rateReturn050s, '', 0) : '',
          prem_diff_return_25rate: illustrations[19] ? getCurrency(illustrations[19].tp_rateReturn250s, '', 0) : '',
          prem_diff_return_40rate: illustrations[19] ? getCurrency(illustrations[19].tp_rateReturn400s, '', 0) : ''
        },
        third_sv: {
          prem_diff_return_05rate: illustrations[29] ? getCurrency(illustrations[29].tp_rateReturn050s, '', 0) : '',
          prem_diff_return_25rate: illustrations[29] ? getCurrency(illustrations[29].tp_rateReturn250s, '', 0) : '',
          prem_diff_return_40rate: illustrations[29] ? getCurrency(illustrations[29].tp_rateReturn400s, '', 0) : ''
        },
        last_sv: {
          prem_diff_return_05rate: firstIllustration.lastRowEOY && illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].tp_rateReturn050s, '', 0) : '',
          prem_diff_return_25rate: firstIllustration.lastRowEOY && illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].tp_rateReturn250s, '', 0) : '',
          prem_diff_return_40rate: firstIllustration.lastRowEOY && illustrations[firstIllustration.lastRowEOY - 1] ? getCurrency(illustrations[firstIllustration.lastRowEOY - 1].tp_rateReturn400s, '', 0) : ''
        }
      },
      sign_prem_diff_return_05rate: diff_v1,
      sign_prem_diff_return_25rate: diff_v2
    };
  };
  return {
    proposer_details: proposer_init(),
    life_assured_details: life_assured_init(),
    basic_plan: basic_plan_init(),
    your_plan: your_plan_init(),
    your_premimun: your_premimun_init(),
    total_premimu: total_premimu_init(),
    conditions: conditions_init(),
    cover: cover_init(),
    mainbi: mainbi_init(),
    illustration: illustration_init(),
    others: others_init(),
    footer: footer_init(),
    genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
    bundle_info: bundle_init()
  };
}