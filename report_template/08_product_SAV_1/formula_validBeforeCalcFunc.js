function(quotation, planInfo, planDetails) {
  if (planInfo.covCode === quotation.baseProductCode) {
    var sumInsured = quotation.plans[0].sumInsured;
    if (planInfo.calcBy === 'sumAssured') {
      quotation.plans[0].sumInsured = (sumInsured !== null) ? Math.ceil(sumInsured / 100) * 100 : null;
    }
    var premium = quotation.plans[0].premium;
    if (planInfo.calcBy === 'premium' && premium !== null && premium !== undefined) {
      if (planDetails[planInfo.covCode].inputConfig && planDetails[planInfo.covCode].inputConfig.premlim) {
        let min = planDetails[planInfo.covCode].inputConfig.premlim.min;
        if (min && min > premium) {
          quotation.plans[0].premium = min;
        }
      }
    }
  }
}