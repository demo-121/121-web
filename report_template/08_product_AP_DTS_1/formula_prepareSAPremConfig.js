function(planDetail, quotation, planDetails) {
  if (quotation.plans[0].policyTerm === undefined || quotation.plans[0].sumInsured === undefined || quotation.plans[0].sumInsured === null || quotation.plans[0].sumInsured === '') {
    planDetail.saInputInd = 'N';
  } else {
    planDetail.saInputInd = 'Y';
  }
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var basicAssred = parseInt(quotation.plans[0].sumInsured);
  var maxriderSA = math.min((10 * basicAssred), 1000000);
  if (planDetail.saInputInd == 'Y') {
    planDetail.inputConfig.benlim = {
      min: 20000,
      max: maxriderSA
    };
  }
}