function(planDetail, quotation) {
  var policyTermsList = [];
  var policyTerm = parseInt(quotation.plans[0].policyTerm);
  var polrider = math.min((65 - quotation.iAge), (policyTerm - 3));
  policyTermsList.push({
    value: polrider,
    title: polrider + 'Years'
  });
  if (quotation.plans[0].policyTerm !== undefined) {
    for (var i = 0; i < quotation.plans.length; i++) {
      var cratePlan = quotation.plans[i];
      if (cratePlan.covCode == 'AP_DTS') {
        quotation.plans[i].policyTerm = polrider;
      }
    }
  }
  return policyTermsList;
}