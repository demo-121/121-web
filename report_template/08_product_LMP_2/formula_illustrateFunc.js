function(quotation, planInfo, planDetails, extraPara) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var planDetail = planDetails[planInfo.covCode];
  var illYrs = 99 - quotation.iAge;
  var sumInsured = math.bignumber(planInfo.sumInsured);
  var declaredRB325 = math.floor(math.multiply(sumInsured, 0.0045));
  var declaredRB475 = math.floor(math.multiply(sumInsured, 0.01));
  var compoundMult325 = math.bignumber(1.0045);
  var compoundMult475 = math.bignumber(1.01);
  var invRetMult325 = math.bignumber(1.0325);
  var invRetMult475 = math.bignumber(1.0475);
  var totalPremiumRider = math.bignumber(quotation.plans[0].yearPrem + quotation.plans[1].yearPrem + quotation.plans[2].yearPrem + quotation.plans[3].yearPrem);
  var totYearPrems = [0];
  var multiplyBenefits = [0];
  var accumRBs325 = [0];
  var accumRBs475 = [0];
  var nonGtdDBs325 = [0];
  var nonGtdDBs475 = [0];
  var totDBs325 = [0];
  var totDBs475 = [0];
  var gtdSVs = [0];
  var nonGtdSVs325 = [0];
  var nonGtdSVs475 = [0];
  var totSVs325 = [0];
  var totSVs475 = [0];
  var premVals325 = [0];
  var premVals475 = [0];
  var deductions325 = [0];
  var deductions475 = [0];
  var totComms = [0];
  var riderPremTots = [0];
  var ridercummComms = [0];
  var illustration = [];
  var lasti = 0;
  var annualPrem = 0;
  var yearPrem = 0;
  for (var j = 0; j < 4; j++) {
    var rider = quotation.plans[j];
    yearPrem = math.add(math.bignumber(rider.yearPrem), yearPrem);
  }
  annualPrem = yearPrem;
  for (var i = 1; i <= illYrs; i++) {
    var age = quotation.iAge + i; /* TODO: add premium of packaged riders */
    var ppt_yr = 0;
    if (planInfo.premTerm.indexOf('YR') == -1) {
      ppt_yr = 65 - quotation.iAge;
    } else {
      ppt_yr = Number.parseInt(planInfo.premTerm);
    }
    if (i > ppt_yr) {
      yearPrem = 0;
    }
    totYearPrems.push(math.add(totYearPrems[i - 1], yearPrem));
    var premVal325 = roundDown(math.multiply(math.add(yearPrem, premVals325[i - 1]), invRetMult325), 2);
    var premVal475 = roundDown(math.multiply(math.add(yearPrem, premVals475[i - 1]), invRetMult475), 2);
    premVals325.push(premVal325);
    premVals475.push(premVal475);
    var accumRB325 = math.floor(math.add(declaredRB325, roundDown(math.multiply(accumRBs325[i - 1], compoundMult325), 2)));
    accumRBs325.push(accumRB325);
    var accumRB475 = math.floor(math.add(declaredRB475, roundDown(math.multiply(accumRBs475[i - 1], compoundMult475), 2)));
    accumRBs475.push(accumRB475);
    var termBonus325 = math.bignumber(planDetail.rates.termBonus.categories['3.25'].DEATH[i - 1]);
    var nonGtdDB325 = roundDown(math.multiply(accumRB325, math.add(termBonus325, 1)), 2);
    nonGtdDBs325.push(nonGtdDB325);
    var termBonus475 = math.bignumber(planDetail.rates.termBonus.categories['4.75'].DEATH[i - 1]);
    var nonGtdDB475 = roundDown(math.multiply(accumRB475, math.add(termBonus475, 1)), 2);
    nonGtdDBs475.push(nonGtdDB475);
    var multiplyBenefit;
    var totDB325;
    var totDB475;
    if (age <= 70) {
      multiplyBenefit = math.bignumber(planInfo.multiplyBenefit);
      totDB325 = math.max(multiplyBenefit, math.add(sumInsured, nonGtdDB325));
      totDB475 = math.max(multiplyBenefit, math.add(sumInsured, nonGtdDB475));
    } else if (age <= 80) {
      multiplyBenefit = math.bignumber(planInfo.multiplyBenefitAfter70);
      totDB325 = math.max(multiplyBenefit, math.add(sumInsured, nonGtdDB325));
      totDB475 = math.max(multiplyBenefit, math.add(sumInsured, nonGtdDB475));
    } else {
      multiplyBenefit = math.bignumber(0);
      totDB325 = math.add(sumInsured, nonGtdDB325);
      totDB475 = math.add(sumInsured, nonGtdDB475);
    }
    multiplyBenefits.push(multiplyBenefit);
    totDBs325.push(totDB325);
    totDBs475.push(totDB475);
    var cvRateKey = 'LEPX';
    if (planInfo.premTerm.indexOf('YR') > -1) {
      cvRateKey = Number.parseInt(planInfo.premTerm) + cvRateKey;
    } else {
      cvRateKey = cvRateKey + Number.parseInt(planInfo.premTerm);
    }
    cvRateKey = cvRateKey + quotation.iGender;
    var gcv = math.bignumber(planDetail.rates.gcv[cvRateKey][quotation.iAge][i]);
    var gtdSV = roundDown(math.multiply(sumInsured, gcv, 0.001), 2);
    gtdSVs.push(gtdSV);
    var ngcv = math.bignumber(planDetail.rates.ngcv[cvRateKey][quotation.iAge][i]);
    var nonGtdSV325 = roundDown(math.multiply(roundDown(math.multiply(accumRB325, ngcv), 2), math.bignumber(math.add(planDetail.rates.termBonus.categories['3.25'].SURRENDER[i - 1], 1)), 0.001), 2);
    var nonGtdSV475 = roundDown(math.multiply(roundDown(math.multiply(accumRB475, ngcv), 2), math.bignumber(math.add(planDetail.rates.termBonus.categories['4.75'].SURRENDER[i - 1], 1)), 0.001), 2);
    nonGtdSVs325.push(nonGtdSV325);
    nonGtdSVs475.push(nonGtdSV475);
    var totSV325 = roundDown(math.add(gtdSV, nonGtdSV325), 2);
    var totSV475 = roundDown(math.add(gtdSV, nonGtdSV475), 2);
    totSVs325.push(totSV325);
    totSVs475.push(totSV475);
    var deduction325 = roundDown(math.subtract(premVal325, totSV325), 2);
    var deduction475 = roundDown(math.subtract(premVal475, totSV475), 2);
    deductions325.push(deduction325);
    deductions475.push(deduction475);
    var cum = 0;
    if (quotation.plans[0].cummComm && quotation.plans[0].cummComm[i]) {
      cum = math.bignumber(quotation.plans[0].cummComm[i]);
    }
    var totComm = roundDown(math.multiply(cum, yearPrem), 2);
    totComms.push(roundDown(math.add(totComm, totComms[i - 1]), 2));
    var riderPremTot = math.bignumber(0);
    var ridercummComm = math.bignumber(0);
    for (var j = 4; j < quotation.plans.length; j++) {
      var rider = quotation.plans[j];
      if (i <= Number.parseInt(rider.premTermYr)) {
        riderPremTot = roundDown(math.add(riderPremTot, math.bignumber(rider.yearPrem)), 2);
      }
      var cum = 0;
      if (rider.cummComm && rider.cummComm[i]) {
        cum = rider.cummComm[i];
      }
      ridercummComm = roundDown(math.add(ridercummComm, math.multiply(cum, math.bignumber(rider.yearPrem))), 2);
    }
    riderPremTots.push(math.add(riderPremTots[i - 1], riderPremTot));
    ridercummComms.push(math.add(ridercummComms[i - 1], ridercummComm));
  }
  var bundleAgeAttaineds = [0];
  var bundleAnnualPremiums = [0];
  var bundleCumPremiums = [0];
  var bundleValPremiums325 = [0];
  var bundleValPremiums475 = [0];
  var bundleGtdSVs = [0];
  var bundledeclaredRBs325 = [0];
  var bundledeclaredRBs475 = [0];
  var bundleAccumRBs325 = [0];
  var bundleAccumRBs475 = [0];
  var bundleBasicNonGtdDBs325 = [0];
  var bundleBasicNonGtdDBs475 = [0];
  var bundleBasicNonGtdSVs325 = [0];
  var bundleBasicNonGtdSVs475 = [0];
  var bundleGuaranteedDeathBs = [0];
  var bundleGuaranteedBasicTotDBs325 = [0];
  var bundleGuaranteedBasicTotDBs475 = [0];
  var bundleGuaranteedBasicTotSVs325 = [0];
  var bundleGuaranteedBasicTotSVs475 = [0];
  var tp_standardPremRates = [0];
  var tp_annualPrems = [0];
  var tp_cummPrems = [0];
  var tp_guaranteedDBs = [0];
  var tp_premDiffs = [0];
  var tp_rateReturn050s = [0];
  var tp_rateReturn250s = [0];
  var tp_rateReturn400s = [0];
  var tp_return050 = math.bignumber(1.005);
  var tp_return250 = math.bignumber(1.025);
  var tp_return400 = math.bignumber(1.04);
  var bundleYearAge = 0;
  var lastRowEOY = 0;
  var abundleTotAnnPrem = math.bignumber(0);
  var bundleTotPremPaid = math.bignumber(0);
  if (quotation.iAge < 25) {
    bundleYearAge = "@65";
    lastRowEOY = 65 - quotation.iAge;
  } else if (quotation.iAge < 60) {
    var countAge = quotation.iAge + 40;
    bundleYearAge = "40/" + countAge;
    lastRowEOY = 40;
  } else {
    bundleYearAge = "@99";
    lastRowEOY = 39;
  }
  var bundleSA = quotation.plans[0].bundleSA;
  var bundle_PPT;
  if (planInfo.premTerm.indexOf('YR') > -1) {
    bundle_PPT = Number.parseInt(planInfo.premTerm);
  } else {
    bundle_PPT = 65 - quotation.iAge;
  }
  for (var j = 0; j < 4; j++) {
    var rider = quotation.plans[j];
    abundleTotAnnPrem = math.add(math.bignumber(rider.annualPrem_bundle), abundleTotAnnPrem);
  }
  bundleTotPremPaid = math.multiply(bundle_PPT, abundleTotAnnPrem);
  var grossper1000SA = math.bignumber(0);
  var standard_rateFPNRKey = "FPNR_" + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS') + "_Regular";
  if (planDetail.rates.rateFPNR[standard_rateFPNRKey] && planDetail.rates.rateFPNR[standard_rateFPNRKey][quotation.iAge]) {
    grossper1000SA = math.bignumber(planDetail.rates.rateFPNR[standard_rateFPNRKey][quotation.iAge][6]);
  }
  var standard_tp_annualPrem = roundDown(math.multiply(grossper1000SA, 100000, 0.001), 2);
  var standard_tp_totalPremPaid = math.multiply(standard_tp_annualPrem, planInfo.policyTermYr);
  for (var i = 1; i <= 100; i++) {
    bundleAgeAttaineds.push(planInfo.policyTermYr > i ? quotation.iAge + i : quotation.iAge + planInfo.policyTermYr);
    var bundleTotAnnPrem = math.bignumber(0);
    if (i <= bundle_PPT) {
      bundleTotAnnPrem = abundleTotAnnPrem;
    }
    bundleAnnualPremiums.push(bundleTotAnnPrem);
    var bundleCumPremium = math.add(bundleCumPremiums[i - 1], bundleTotAnnPrem);
    bundleCumPremiums.push(bundleCumPremium);
    var bundleValPremium325 = roundDown(math.multiply(math.add(bundleValPremiums325[i - 1], bundleTotAnnPrem), invRetMult325), 2);
    var bundleValPremium475 = roundDown(math.multiply(math.add(bundleValPremiums475[i - 1], bundleTotAnnPrem), invRetMult475), 2);
    bundleValPremiums325.push(bundleValPremium325);
    bundleValPremiums475.push(bundleValPremium475);
    var cvRateKey = 'LEPX';
    if (planInfo.premTerm.indexOf('YR') > -1) {
      cvRateKey = Number.parseInt(planInfo.premTerm) + cvRateKey;
    } else {
      cvRateKey = cvRateKey + Number.parseInt(planInfo.premTerm);
    }
    cvRateKey = cvRateKey + quotation.iGender;
    var gcv = math.bignumber(planDetail.rates.gcv[cvRateKey][quotation.iAge][i]);
    var bundleGtdSV = roundDown(math.multiply(bundleSA, gcv, 0.001), 2);
    bundleGtdSVs.push(bundleGtdSV);
    var bundledeclaredRB325 = roundDown(math.multiply(bundleSA, 0.0045), 0);
    bundledeclaredRBs325.push(bundledeclaredRB325);
    var bundledeclaredRB475 = roundDown(math.multiply(bundleSA, 0.01), 0);
    bundledeclaredRBs475.push(bundledeclaredRB475);
    var bundleAccumRB325 = math.floor(math.add(bundledeclaredRB325, roundDown(math.multiply(bundleAccumRBs325[i - 1], compoundMult325), 2)));
    bundleAccumRBs325.push(bundleAccumRB325);
    var bundleAccumRB475 = math.floor(math.add(bundledeclaredRB475, roundDown(math.multiply(bundleAccumRBs475[i - 1], compoundMult475), 2)));
    bundleAccumRBs475.push(bundleAccumRB475);
    var bundleTermBonus325 = math.bignumber(planDetail.rates.termBonus.categories['3.25'].DEATH[i - 1]);
    var bundleBasicNonGtdDB325 = roundDown(math.multiply(bundleAccumRB325, math.add(bundleTermBonus325, 1)), 2);
    bundleBasicNonGtdDBs325.push(bundleBasicNonGtdDB325);
    var bundleTermBonus475 = math.bignumber(planDetail.rates.termBonus.categories['4.75'].DEATH[i - 1]);
    var bundleBasicNonGtdDB475 = roundDown(math.multiply(bundleAccumRB475, math.add(bundleTermBonus475, 1)), 2);
    bundleBasicNonGtdDBs475.push(bundleBasicNonGtdDB475);
    var ngcv = math.bignumber(planDetail.rates.ngcv[cvRateKey][quotation.iAge][i]);
    var bundleBasicNonGtdSV325 = roundDown(math.multiply(roundDown(math.multiply(bundleAccumRB325, ngcv), 2), math.bignumber(math.add(planDetail.rates.termBonus.categories['3.25'].SURRENDER[i - 1], 1)), 0.001), 2);
    var bundleBasicNonGtdSV475 = roundDown(math.multiply(roundDown(math.multiply(bundleAccumRB475, ngcv), 2), math.bignumber(math.add(planDetail.rates.termBonus.categories['4.75'].SURRENDER[i - 1], 1)), 0.001), 2);
    bundleBasicNonGtdSVs325.push(bundleBasicNonGtdSV325);
    bundleBasicNonGtdSVs475.push(bundleBasicNonGtdSV475);
    var bundleGuaranteedBasicTotDB325 = math.bignumber(0);
    var bundleGuaranteedBasicTotDB475 = math.bignumber(0);
    var multiplyBenefit_bundle = math.bignumber(quotation.policyOptions.multiFactor);
    var bundleGuaranteedDeathB = math.bignumber(0);
    if (i <= planInfo.policyTermYr) {
      if (quotation.iAge + i <= 70) {
        var a = math.multiply(multiplyBenefit_bundle, bundleSA);
        var b = math.add(bundleSA, bundleBasicNonGtdDB325);
        var c = math.add(bundleSA, bundleBasicNonGtdDB475);
        bundleGuaranteedBasicTotDB325 = math.max(a, b);
        bundleGuaranteedBasicTotDB475 = math.max(a, c);
        bundleGuaranteedDeathB = a;
      } else {
        var a = math.multiply(multiplyBenefit_bundle, bundleSA, 0.5);
        var b = math.add(bundleSA, bundleBasicNonGtdDB325);
        var c = math.add(bundleSA, bundleBasicNonGtdDB475);
        if (quotation.iAge + i > 70 && quotation.iAge + i <= 80) {
          bundleGuaranteedBasicTotDB325 = math.max(a, b);
          bundleGuaranteedBasicTotDB475 = math.max(a, c);
          bundleGuaranteedDeathB = a;
        } else {
          bundleGuaranteedBasicTotDB325 = b;
          bundleGuaranteedBasicTotDB475 = c;
          bundleGuaranteedDeathB = bundleSA;
        }
      }
    }
    bundleGuaranteedBasicTotDBs325.push(bundleGuaranteedBasicTotDB325);
    bundleGuaranteedBasicTotDBs475.push(bundleGuaranteedBasicTotDB475);
    bundleGuaranteedDeathBs.push(bundleGuaranteedDeathB);
    var bundleGuaranteedBasicTotSV325 = math.bignumber(0);
    var bundleGuaranteedBasicTotSV475 = math.bignumber(0);
    if (i <= planInfo.policyTermYr) {
      bundleGuaranteedBasicTotSV325 = roundDown(math.add(bundleGtdSV, bundleBasicNonGtdSV325), 2);
      bundleGuaranteedBasicTotSV475 = roundDown(math.add(bundleGtdSV, bundleBasicNonGtdSV475), 2);
    }
    bundleGuaranteedBasicTotSVs325.push(bundleGuaranteedBasicTotSV325);
    bundleGuaranteedBasicTotSVs475.push(bundleGuaranteedBasicTotSV475);
    var tp_standardPremRate = math.bignumber(0);
    var tp_annualPrem = math.bignumber(0);
    var tp_guaranteedDB = math.bignumber(0);
    if (i <= planInfo.policyTermYr) {
      var rateFPNRKey = "FPNR_" + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS') + "_Regular";
      if (planDetail.rates.rateFPNR[rateFPNRKey] && planDetail.rates.rateFPNR[rateFPNRKey][quotation.iAge + i - 1]) {
        tp_standardPremRate = math.bignumber(planDetail.rates.rateFPNR[rateFPNRKey][quotation.iAge + i - 1][6]);
      }
      tp_annualPrem = standard_tp_annualPrem;
      tp_guaranteedDB = 100000;
    }
    tp_standardPremRates.push(tp_standardPremRate);
    tp_annualPrems.push(tp_annualPrem);
    var tp_cummPrem = math.add(tp_cummPrems[i - 1], tp_annualPrem);
    tp_cummPrems.push(tp_cummPrem);
    tp_guaranteedDBs.push(tp_guaranteedDB);
    var tp_premDiff = math.subtract(bundleTotAnnPrem, tp_annualPrem);
    tp_premDiffs.push(tp_premDiff);
    tp_rateReturn050s.push(math.multiply(tp_return050, math.add(tp_rateReturn050s[i - 1], tp_premDiff)));
    tp_rateReturn250s.push(math.multiply(tp_return250, math.add(tp_rateReturn250s[i - 1], tp_premDiff)));
    tp_rateReturn400s.push(math.multiply(tp_return400, math.add(tp_rateReturn400s[i - 1], tp_premDiff)));
  } /** debug*/ /* var s = ''; for (var i = 1; i <= 100; i++) { s += i + ','; s += bundleAgeAttaineds[i] + ','; s += bundleAnnualPremiums[i] + ','; s += bundleCumPremiums[i] + ','; s += tp_premDiffs[i] + ','; s += tp_rateReturn050s[i] + ','; s += tp_rateReturn250s[i] + ','; s += tp_rateReturn400s[i] + '\n'; } console.log(s); */
  var c_surrender_age = 0;
  var c_policy_year = 0;
  if (quotation.iAge < 26) {
    c_surrender_age = 65;
    c_policy_year = 65 - quotation.iAge;
  } else {
    if (quotation.iAge > 58) {
      c_surrender_age = 99;
      c_policy_year = 99 - quotation.iAge;
    } else {
      c_surrender_age = quotation.iAge + 40;
      c_policy_year = 40;
    }
  }
  var c_low_irr = math.bignumber(0.0325);
  var c_high_irr = math.bignumber(0.0475);
  var c_row_ages = [0];
  var project_case_flow325s = [0];
  var project_case_flow475s = [0];
  var tot_project_case_flow325 = 0;
  var tot_project_case_flow475 = 0;
  for (var c_eoy = 0; c_eoy < 100; c_eoy++) {
    var ppt_yr = 0;
    if (planInfo.premTerm.indexOf('YR') == -1) {
      ppt_yr = 65 - quotation.iAge;
    } else {
      ppt_yr = Number.parseInt(planInfo.premTerm)
    }
    var c_row_age = 0;
    if (c_eoy > c_policy_year) {
      c_row_age = 0;
    } else {
      c_row_age = quotation.iAge + c_eoy;
    }
    c_row_ages.push(c_row_age);
    var project_case_flow325 = 0;
    var project_case_flow475 = 0;
    if (c_row_age == 0) {
      project_case_flow325 = 0;
      project_case_flow475 = 0;
    } else {
      if (c_eoy < ppt_yr) {
        project_case_flow325 = math.multiply(-1, annualPrem);
        project_case_flow475 = math.multiply(-1, annualPrem);
      } else if (c_eoy == c_policy_year) {
        project_case_flow325 = totSVs475[c_eoy];
        project_case_flow475 = totSVs325[c_eoy];
      } else {
        project_case_flow325 = 0;
        project_case_flow475 = 0;
      }
    }
    project_case_flow325s.push(project_case_flow325);
    project_case_flow475s.push(project_case_flow475);
    tot_project_case_flow325 = math.add(tot_project_case_flow325, project_case_flow325);
    tot_project_case_flow475 = math.add(tot_project_case_flow475, project_case_flow475);
  }
  var c_age_older = c_surrender_age;
  var projectIRR325 = math.multiply(parseInt(math.multiply(10000, math.bignumber(runExcelFunc("IRR", project_case_flow325s, 0.000001)))), 0.01);
  var projectIRR475 = math.multiply(parseInt(math.multiply(10000, math.bignumber(runExcelFunc("IRR", project_case_flow475s, 0.000001)))), 0.01);
  var fair_basic_total_distribution = math.floor(roundDown(math.max(totComms), 2));
  var fair_total_basic_premium = math.floor(roundDown(math.max(totYearPrems), 2));
  var basic_tdc_div_premium = math.multiply(math.floor(math.multiply(10000, math.divide(fair_basic_total_distribution, fair_total_basic_premium))), 0.01);
  for (var i = 1; i <= illYrs; i++) {
    illustration.push({
      fairAgeOld: c_age_older,
      fairYTSHigh: math.number(projectIRR325),
      fairYTSLow: math.number(projectIRR475),
      fairBasicTotalDistribution: math.number(fair_basic_total_distribution),
      fairTotalBasicPremium: math.number(fair_total_basic_premium),
      fairbBasicTDCPremium: math.number(basic_tdc_div_premium),
      annualPrem: math.number(roundDown(totYearPrems[i], 2)),
      totYearPrem: Math.floor(math.number(totYearPrems[i])),
      multiplyBenefit: Math.floor(math.number(multiplyBenefits[i])),
      gtdDB: planInfo.sumInsured,
      nonGtdDB: {
        3.25: Math.floor(math.number(nonGtdDBs325[i])),
        4.75: Math.floor(math.number(nonGtdDBs475[i]))
      },
      totDB: {
        3.25: Math.floor(math.number(totDBs325[i])),
        4.75: Math.floor(math.number(totDBs475[i]))
      },
      gtdSV: Math.floor(math.number(gtdSVs[i])),
      nonGtdSV: {
        3.25: Math.floor(math.number(nonGtdSVs325[i])),
        4.75: Math.floor(math.number(nonGtdSVs475[i]))
      },
      totSV: {
        3.25: Math.floor(math.number(totSVs325[i])),
        4.75: Math.floor(math.number(totSVs475[i]))
      },
      premVal: {
        3.25: Math.floor(math.number(premVals325[i])),
        4.75: Math.floor(math.number(premVals475[i]))
      },
      deduction: {
        3.25: Math.floor(math.number(deductions325[i])),
        4.75: Math.floor(math.number(deductions475[i]))
      },
      tdc: Math.floor(math.number(totComms[i])),
      riderPrem: Math.floor(math.number(riderPremTots[i])),
      riderComm: Math.floor(math.number(ridercummComms[i])),
      bundleAgeAttaineds: math.number(roundDown(bundleAgeAttaineds[i], 2)),
      bundleAnnualPremiums: math.number(roundDown(bundleAnnualPremiums[i], 2)),
      bundleCumPremiums: math.number(roundDown(bundleCumPremiums[i], 2)),
      bundleValPremiums: {
        3.25: parseInt(math.number(roundDown(bundleValPremiums325[i], 2))),
        4.75: parseInt(math.number(roundDown(bundleValPremiums475[i], 2)))
      },
      bundleGtdSVs: parseInt(math.number(roundDown(bundleGtdSVs[i], 2))),
      bundledeclaredRBs: {
        3.25: parseInt(math.number(roundDown(bundledeclaredRBs325[i], 2))),
        4.75: parseInt(math.number(roundDown(bundledeclaredRBs475[i], 2)))
      },
      bundleAccumRBs: {
        3.25: parseInt(math.number(roundDown(bundleAccumRBs325[i], 2))),
        4.75: parseInt(math.number(roundDown(bundleAccumRBs475[i], 2)))
      },
      bundleBasicNonGtdDBs: {
        3.25: parseInt(math.number(roundDown(bundleBasicNonGtdDBs325[i], 2))),
        4.75: parseInt(math.number(roundDown(bundleBasicNonGtdDBs475[i], 2)))
      },
      bundleBasicNonGtdSVs: {
        3.25: parseInt(math.number(roundDown(bundleBasicNonGtdSVs325[i], 2))),
        4.75: parseInt(math.number(roundDown(bundleBasicNonGtdSVs475[i], 2)))
      },
      bundleGuaranteedDeathBs: parseInt(math.number(roundDown(bundleGuaranteedDeathBs[i], 2))),
      bundleGuaranteedBasicTotDBs: {
        3.25: parseInt(math.number(roundDown(bundleGuaranteedBasicTotDBs325[i], 2))),
        4.75: parseInt(math.number(roundDown(bundleGuaranteedBasicTotDBs475[i], 2)))
      },
      bundleGuaranteedBasicTotSVs: {
        3.25: parseInt(math.number(roundDown(bundleGuaranteedBasicTotSVs325[i], 2))),
        4.75: parseInt(math.number(roundDown(bundleGuaranteedBasicTotSVs475[i], 2)))
      },
      tp_standardPremRates: math.number(roundDown(tp_standardPremRates[i], 2)),
      tp_annualPrems: math.number(roundDown(tp_annualPrems[i], 2)),
      tp_cummPrems: math.number(roundDown(tp_cummPrems[i], 2)),
      tp_guaranteedDBs: math.number(roundDown(tp_guaranteedDBs[i], 2)),
      tp_premDiffs: math.number(roundDown(tp_premDiffs[i], 2)),
      tp_rateReturn050s: parseInt(math.number(tp_rateReturn050s[i])),
      tp_rateReturn250s: parseInt(math.number(tp_rateReturn250s[i])),
      tp_rateReturn400s: parseInt(math.number(tp_rateReturn400s[i])),
      bundleYearAge: bundleYearAge,
      lastRowEOY: lastRowEOY,
      abundleTotAnnPrem: math.number(abundleTotAnnPrem),
      bundleTotPremPaid: math.number(bundleTotPremPaid),
      standard_tp_annualPrem: math.number(standard_tp_annualPrem),
      standard_tp_totalPremPaid: math.number(standard_tp_totalPremPaid),
      bundle_PPT: bundle_PPT
    });
  }
  return illustration;
}