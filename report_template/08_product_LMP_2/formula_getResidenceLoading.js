function(quotation, planInfo, planDetail) {
  var rates = planDetail.rates;
  var countryCode = quotation.iResidence;
  var countryGroup = rates.countryGroup[countryCode][0];
  var substandardClass = rates.substandardClass[countryGroup]['LEPX'];
  if (Array.isArray(substandardClass)) {
    substandardClass = substandardClass[0];
  }
  var rateKey = planInfo.planCode + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS') + substandardClass;
  var temp = 0;
  console.log(planDetail.rates.residentialLoading);
  if (planDetail.rates.residentialLoading[rateKey] && planDetail.rates.residentialLoading[rateKey][quotation.iAge]) {
    temp = planDetail.rates.residentialLoading[rateKey][quotation.iAge];
  }
  return temp;
}