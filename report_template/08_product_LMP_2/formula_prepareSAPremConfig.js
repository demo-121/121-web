function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var maxAdult = {
    "2.5": 10000000,
    "3.5": 7142000,
    "5": 5000000
  };
  var maxJuv = {
    "2.5": 400000,
    "3.5": 285000,
    "5": 200000
  };
  var multiFactor = quotation.policyOptions.multiFactor;
  if (multiFactor) {
    planDetail.inputConfig.benlim = {
      min: 25000,
      max: (quotation.iAge >= 18 ? maxAdult : maxJuv)[multiFactor]
    };
  }
  planDetail.othSaInd = 'N';
  planDetail.inputConfig.canViewOthSa = false;
}