function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  var bpInfo = quotation.plans[0];
  var maxAdult = {
    "2.5": 1200000,
    "3.5": 857000,
    "5": 600000
  };
  var maxJuv = {
    "2.5": 200000,
    "3.5": 142000,
    "5": 100000
  };
  var multiFactor = quotation.policyOptions.multiFactor;
  var amountbasicplanlookup = 0;
  if (multiFactor) {
    amountbasicplanlookup = (quotation.iAge >= 18 ? maxAdult : maxJuv)[multiFactor];
  }
  var amountminbpInfo = amountbasicplanlookup;
  if (planInfo.sumInsured) {
    var max_value = quotation.iAge <= 18 ? 500000 : 3000000;
    var cipSa = 0;
    var tooLargeECIX_LMP = false;
    var tooLargeCIBX_LMP = false;
    var sumofECIX_LMPnCIBX_LMP = 0;
    for (var p in quotation.plans) {
      var plan = quotation.plans[p];
      if (plan.sumInsured) {
        if (plan.covCode === 'ECIX_LMP' || plan.covCode === 'CIBX_LMP') {
          cipSa += plan.multiplyBenefit;
        } else if (plan.covCode === 'CIP_LMP') {
          cipSa += plan.sumInsured;
        }
        if (plan.covCode === 'ECIX_LMP' || plan.covCode === 'CIBX_LMP') {
          sumofECIX_LMPnCIBX_LMP += plan.sumInsured;
        }
        if (plan.covCode === 'ECIX_LMP' && plan.sumInsured > bpInfo.sumInsured) {
          tooLargeECIX_LMP = true;
        }
        if (plan.covCode === 'CIBX_LMP' && plan.sumInsured > bpInfo.sumInsured) {
          tooLargeCIBX_LMP = true;
        }
      }
    }
    if (tooLargeECIX_LMP) {
      quotDriver.context.addError({
        covCode: 'ECIX_LMP',
        msg: 'The sum assured for Early Critical Illness Benefit riders has exceeded the basic plan sum assured of S$' + getCurrency(bpInfo.sumInsured, '', 0)
      });
    } else if (tooLargeCIBX_LMP) {
      quotDriver.context.addError({
        covCode: 'CIBX_LMP',
        msg: 'The sum assured for Critical Illness Benefit riders has exceeded the basic plan sum assured of S$' + getCurrency(bpInfo.sumInsured, '', 0)
      });
    } else if (multiFactor && sumofECIX_LMPnCIBX_LMP > amountminbpInfo) {
      quotDriver.context.addError({
        covCode: 'ECIX_LMP',
        msg: 'The total sum assured for Early Critical Illness Benefit and Critical Illness Benefit riders has exceeded the maximum sum assured of S$' + getCurrency(amountminbpInfo, '', 0)
      });
      quotDriver.context.addError({
        covCode: 'CIBX_LMP',
        msg: 'The total sum assured for Early Critical Illness Benefit and Critical Illness Benefit riders has exceeded the maximum sum assured of S$' + getCurrency(amountminbpInfo, '', 0)
      });
    } else if (sumofECIX_LMPnCIBX_LMP > bpInfo.sumInsured) {
      quotDriver.context.addError({
        covCode: 'ECIX_LMP',
        msg: 'The total sum assured for Early Critical Illness Benefit and Critical Illness Benefit riders has exceeded the maximum sum assured of S$' + getCurrency(bpInfo.sumInsured, '', 0)
      });
      quotDriver.context.addError({
        covCode: 'CIBX_LMP',
        msg: 'The total sum assured for Early Critical Illness Benefit and Critical Illness Benefit riders has exceeded the maximum sum assured of S$' + getCurrency(bpInfo.sumInsured, '', 0)
      });
    } else if (cipSa > max_value && quotation.iAge > 18) {
      quotDriver.context.addError({
        covCode: 'ECIX_LMP',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 3 million.'
      });
      quotDriver.context.addError({
        covCode: 'CIBX_LMP',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 3 million.'
      });
      quotDriver.context.addError({
        covCode: 'CIP_LMP',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 3 million.'
      });
    } else if (cipSa > max_value && quotation.iAge < 18) {
      quotDriver.context.addError({
        covCode: 'ECIX_LMP',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 0.5 million.'
      });
      quotDriver.context.addError({
        covCode: 'CIBX_LMP',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 0.5 million.'
      });
      quotDriver.context.addError({
        covCode: 'CIP_LMP',
        msg: 'The total sum assured for Early Critical Illness Benefit, Critical Illness Benefit and Critical Illness Plus riders has exceeded the maximum aggregated limit of SGD 0.5 million.'
      });
    }
  }
}