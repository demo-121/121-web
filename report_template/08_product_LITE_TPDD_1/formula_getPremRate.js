function(quotation, planInfo, planDetail, age) {
  var planCode = 'TPDD';
  if (planInfo.premTerm.indexOf('YR') > -1) {
    planCode = Number.parseInt(planInfo.premTerm) + planCode;
  } else {
    planCode = planCode + Number.parseInt(planInfo.premTerm);
  }
  var rateKey = planCode + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS');
  return planDetail.rates.premRate[rateKey][quotation.iAge];
}