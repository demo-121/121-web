function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var bpInfo = quotation.plans[0];
  var sa = bpInfo.sumInsured;
  sa = sa > 1200000 ? 1200000 : sa;
  var amount = sa > 0 ? sa : 0;
  var singaporeResideneMap = {
    "5": 1200000,
    "3.5": 1714000,
    "2.5": 2400000
  };
  var nonsingaporeResideneMap = {
    "5": 800000,
    "3.5": 1142000,
    "2.5": 1600000
  };
  var juveniles = {
    "5": 200000,
    "3.5": 285000,
    "2.5": 400000
  };
  var max_value = 0;
  if (quotation.iAge < 18) {
    max_value = juveniles[quotation.policyOptions.multiFactor];
  } else {
    if (quotation.iResidence == 'R2') {
      max_value = singaporeResideneMap[quotation.policyOptions.multiFactor];
    } else {
      max_value = nonsingaporeResideneMap[quotation.policyOptions.multiFactor];
    }
  }
  if (amount < max_value) {
    planDetail.inputConfig.benlim = {
      min: amount,
      max: max_value
    };
  }
  planDetail.othSaInd = 'N';
  planDetail.inputConfig.canViewOthSa = false;
}