function(quotation, planInfo, planDetail) {
  var planCode = 'TPDD';
  if (planInfo.premTerm.indexOf('YR') > -1) {
    planCode = Number.parseInt(planInfo.premTerm) + planCode;
  } else {
    planCode = planCode + Number.parseInt(planInfo.premTerm);
  }
  var rates = planDetail.rates;
  var countryCode = quotation.iResidence;
  var countryGroup = rates.countryGroup[countryCode];
  if (Array.isArray(countryGroup)) {
    countryGroup = countryGroup[0];
  }
  var substandardClass = rates.substandardClass[countryGroup]['TPDD'];
  if (Array.isArray(substandardClass)) {
    substandardClass = substandardClass[0];
  }
  var rateKey = planCode + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS') + substandardClass;
  if (planDetail.rates.residentialLoading[rateKey] && planDetail.rates.residentialLoading[rateKey][quotation.iAge]) {
    return planDetail.rates.residentialLoading[rateKey][quotation.iAge];
  }
  return 0;
}