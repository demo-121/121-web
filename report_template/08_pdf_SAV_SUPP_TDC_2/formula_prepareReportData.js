function(quotation, planInfo, planDetails, extraPara) {
  var trunc = function(value, position) {
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return Number(math.divide(math.floor(math.multiply(value, scale)), scale));
  };
  var illustrations = extraPara.illustrations;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb,
  } = extraPara.company;
  var retVal = {
    'compName': compName,
    'compRegNo': compRegNo,
    'compAddr': compAddr,
    'compAddr2': compAddr2,
    'compTel': compTel,
    'compFax': compFax,
    'compWeb': compWeb
  };
  var basicPlan = quotation.plans[0];
  var basicIllustrations = illustrations[basicPlan.covCode];
  var illustrateData_rider = [];
  var polTerm = basicPlan.policyTerm ? basicPlan.policyTerm : 15;
  var projections = planDetails[quotation.baseProductCode].projection;
  var low = 999;
  var high = -1;
  for (var i = 0; i < 2; i++) {
    var id = projections[i].title.en;
    low = Math.min(Number(id), low);
    high = Math.max(Number(id), high);
  }
  for (var i = 0; i < polTerm; i++) {
    var polYr = i + 1;
    var basicIllustration = basicIllustrations[i];
    var polYr_age = polYr + '/' + basicIllustration.age;
    var totalPremPaid = getCurrency(trunc(Number(basicIllustration.totalPremPaid), 0), ' ', 0);
    var totalPremPaid_rider = 0;
    var totalDistribCost = 0;
    var minRiderTerm = 999;
    for (var j = 1; j < quotation.plans.length; j++) {
      var rider = quotation.plans[j];
      var riderIllustrate = illustrations[rider.covCode];
      var riderTerm = math.min(riderIllustrate.length - 1, i);
      totalPremPaid_rider = totalPremPaid_rider + riderIllustrate[riderTerm].annaulPrem;
      totalDistribCost = totalDistribCost + riderIllustrate[riderTerm].totalDistribCost;
    }
    var row = {
      polYr_age: polYr_age,
      totalPremPaid: getCurrency(trunc(totalPremPaid_rider, 0), ' ', 0),
      totalDistribCost: getCurrency(trunc(Number(totalDistribCost), 0), ' ', 0)
    };
    var riderPolTerm = polTerm;
    if (quotation.plans.length > 1 && (quotation.plans[1].covCode === 'PPERA' || quotation.plans[1].covCode === 'PPEPS')) riderPolTerm = Math.min(polTerm, 65 - quotation.pAge);
    if (i < riderPolTerm) {
      if (polYr <= 10 || (polYr > 10 && polYr % 5 === 0) || polYr == riderPolTerm) illustrateData_rider.push(row);
    }
  }
  retVal.illustrateData_rider = illustrateData_rider;
  retVal.rate_high = (basicIllustrations[0].projRate[high]) * 100;
  retVal.rate_low = (basicIllustrations[0].projRate[low]) * 100;
  retVal.headerName = quotation.sameAs == "Y" ? quotation.pFullName : quotation.iFullName;
  retVal.headerGender = (quotation.sameAs == "Y" ? quotation.pGender : quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.headerSmoke = (quotation.sameAs == "Y" ? quotation.pSmoke : quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.headerAge = quotation.sameAs == "Y" ? quotation.pAge : quotation.iAge;
  var ccy = quotation.ccy;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = 'US$';
  } else if (ccy == 'ASD') {
    polCcy = 'Australian Dollars';
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    polCcy = 'British Pound';
    ccySyb = '£';
  }
  retVal.polCcy = polCcy;
  retVal.ccySyb = ccySyb;
  var paymentModeTitle = '';
  if (quotation.paymentMode == 'A') {
    paymentModeTitle = 'Annual';
  } else if (quotation.paymentMode == 'S') {
    paymentModeTitle = 'Semi-Annual';
  } else if (quotation.paymentMode == 'Q') {
    paymentModeTitle = 'Quarterly';
  } else if (quotation.paymentMode == 'M') {
    paymentModeTitle = 'Monthly';
  }
  retVal.paymentModeTitle = paymentModeTitle;
  retVal.basicAnnualPrem = getCurrency(planInfo.yearPrem, ' ', 2);
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  return retVal;
}