function(planDetail, quotation, planDetails) {
  quotDriver.preparePolicyOptions(planDetail, quotation, planDetails);
  var pos = _.cloneDeep(planDetail.policyOptions),
    qpos = quotation.policyOptions,
    runFunc = quotDriver.runFunc;
  var formulas = planDetail.formulas,
    updNumPol = formulas.updNumPol,
    loadType = runFunc(formulas.getLoadType, quotation.plans);
  var checkNumMandatory = function(_po, _poId, _pv) {
    return _poId == _po.id && _po.mandatory === "Y";
  };
  var pgm = {
    "topUp": "topUpSelect",
    "rsp": "rspSelect",
    "withdrawal": "wdSelect",
    "withdrawal2": "wdSelect",
    "sa": "changeSASelect",
    "iLoading": "iLoading",
    "pLoading": "pLoading"
  };
  var agentAuthGrp = quotation.agent.dealerGroup;
  for (var i = pos.length - 1; i >= 0; i--) {
    var po = pos[i],
      poId = po.id,
      pgId = po.groupID,
      pv = qpos[poId],
      basicBenefit = qpos.basicBenefit;
    if (poId == 'changeSANote' && !qpos.changeSASelect) {
      pos.splice(i, 1);
    } else if ((quotation.sameAs == "Y" && ["pLoading"].indexOf(pgId) > -1) || (["yrt", "age99"].indexOf(quotation.policyOptions.insuranceCharge) == -1 && "sa" == pgId)) {
      pos.splice(i, 1);
    } else {
      if (pgm[pgId] && poId != pgm[pgId]) {
        po.disable = (["rspPayFreq", "wdFreq"].indexOf(poId) > -1 || !qpos[pgm[pgId]]) ? "Y" : "N";
        po.mandatory = po.type != "emptyBlock" && qpos[pgm[pgId]] ? "Y" : "N";
        if (po.disable == "Y" && po.type == "text") {
          qpos[poId] = null;
        }
      }
      if ("topUpSelect" == poId) {
        po.clearFunds = "Y";
        po.warningMsg = "quotation.fund.warning.changeTopUp";
      } else if ("insuranceCharge" == poId) {
        if (["AGENCY", "SINGPOST", "DIRECT", "SYNERGY"].indexOf(agentAuthGrp) > -1) {
          for (var j = po.options.length - 1; j >= 0; j--) {
            if (po.options[j].value != "yrt") {
              po.options.splice(j, 1);
            }
          }
        } else if (["BROKER", "FUSION"].indexOf(agentAuthGrp) > -1) {
          for (var j = po.options.length - 1; j >= 0; j--) {
            var poValue = po.options[j].value;
            if (poValue != "yrt") {
              var ageTerm = Number(poValue.replace("age", ""));
              if (qpos.basicBenefit == "C" || ageTerm - quotation.iAge < 10) {
                po.options.splice(j, 1);
              }
            }
          }
        }
      }
      if (!qpos.iLoading) {
        qpos.iTPD = "1";
        qpos.iCi = "0";
        qpos.iLife = "0";
      }
      if (!qpos.pLoading) {
        qpos.pTPD = "1";
        qpos.pCi = "0";
        qpos.pLife = "0";
      }
      if (poId == "occupationClass") {
        po.mandatory = "N";
        qpos[poId] = (quotation.iOccupationClass === 'IC (Appear Class 4) With UW Error Message') ? '4' : quotation.iOccupationClass;
      }
      if (po.type == "picker") {
        qpos[poId] = quotDriver.runFunc(formulas.initPicker, quotation, po);
      } else if (checkNumMandatory(po, "topUpAmt", pv)) {
        po.hintMsg = 'Minimum Top-up Premium is ' + getCurrency(po.min, '$', 0) + '. Top-Up Premium must be in multiples of $10.';
        qpos[poId] = quotDriver.runFunc(updNumPol, po, pv, po.min);
      } else if (checkNumMandatory(po, "rspAmount", pv)) {
        po.hintMsg = 'Minimum RSP Amount is $' + getCurrency(po.min, '$', 0) + '. RSP Amount must be in multiples of $50.';
        qpos[poId] = quotDriver.runFunc(updNumPol, po, pv, po.min);
      } else if (checkNumMandatory(po, "wdAmount", pv)) {
        po.hintMsg = 'Minimum annual Withdrawal Amount is $' + getCurrency(po.min, '$', 0) + '. Withdrawal Amount must be in multiples of $10.';
        qpos[poId] = quotDriver.runFunc(updNumPol, po, pv, po.min);
      } else if (checkNumMandatory(po, "wdFromAge", pv)) {
        po.hintMsg = "The earliest age for withdrawal is " + (quotation.iAge + 1) + ".";
        qpos[poId] = quotDriver.runFunc(updNumPol, po, pv, math.number(quotation.iAge) + 1, 98);
      } else if (checkNumMandatory(po, "wdToAge", pv)) {
        po.hintMsg = "Regular Withdrawal end age must be greater than start age.";
        qpos[poId] = quotDriver.runFunc(updNumPol, po, pv, math.number(qpos.wdFromAge) + 1);
      } else if (checkNumMandatory(po, "saAmt", pv)) {
        var saAmt = math.number(pv);
        var minSa = 5 * quotCalc.getAnnPrem(quotation, quotation.plans[0], planDetail);
        po.hintMsg = 'Minimum Sum Assured / Benefit is ' + getCurrency(minSa, '$', 0) + '. The New Sum Assured must be in multiples of $100.';
        qpos[poId] = quotDriver.runFunc(updNumPol, po, pv, minSa);
        if (qpos[poId] == quotation.plans[0].sumInsured) {
          quotDriver.context.addError({
            key: "saAmt",
            type: "policyOption",
            msg: "Please enter a value that is different from the original Sum Assured"
          });
        }
      } else if (checkNumMandatory(po, "saPolYr", pv)) {
        var saPolYr = math.number(pv),
          newSa = qpos.saAmt,
          oldSa = quotation.plans[0].sumInsured;
        var iAge = math.number(quotation.iAge);
        var minPolYr = newSa < oldSa ? 5 : 2;
        var maxPolYr = (iAge < 25) ? (65 - iAge) : 40;
        po.hintMsg = 'Policy Year should be from ' + minPolYr + ' to ' + maxPolYr + '.';
        qpos[poId] = quotDriver.runFunc(updNumPol, po, pv, minPolYr, maxPolYr);
      }
    }
  }
  if (!quotation.policyOptions) {
    quotation.policyOptions = {};
  }
  quotation.policyOptions.paymentMethod = 'cash';
  planDetail.inputConfig.policyOptions = pos;
  planDetail.inputConfig.topUpSelect = quotation.policyOptions.topUpSelect;
  return planDetail;
}