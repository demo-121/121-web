function(quotation, planDetail, planDetails) {
  var payMode = quotation.paymentMode;
  var iAge = quotation.iAge;
  var coi = quotation.policyOptions.insuranceCharge;
  var annualPremium = math.number(quotation.plans[0].premium || 0) * (payMode == "M" ? 12 : payMode == "Q" ? 4 : payMode == "S" ? 2 : payMode == "A" ? 1 : 0);
  var min = 10 * annualPremium;
  if (coi == "yrt") {
    if (iAge >= 0 && iAge <= 35) {
      min = 60 * annualPremium;
    } else if (iAge >= 36 && iAge <= 50) {
      min = 30 * annualPremium;
    } else if (iAge >= 51 && iAge <= 70) {
      min = 15 * annualPremium;
    }
  } else {
    if (iAge >= 0 && iAge <= 35) {
      min = 25 * annualPremium;
    } else if (iAge >= 36 && iAge <= 50) {
      min = 15 * annualPremium;
    } else if (iAge >= 51 && iAge <= 70) {
      min = 10 * annualPremium;
    }
  }
  return {
    min
  };
}