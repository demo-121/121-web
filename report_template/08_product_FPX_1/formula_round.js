function(value, position) {
  var scale = math.pow(10, position);
  return math.divide(math.round(math.multiply(value, scale)), scale);
}