function(plans) {
  var lm = {
      "FPX": ["Life", "TPD"],
      "FSX": ["Life", "TPD"],
      "CAB": ["Ci"],
      "LAB": ["Ci"],
      "LTB": ["Life", "TPD", "Ci"],
      "WPS": ["Ci"],
      "WPL": ["TPD"],
      "WPP": ["TPD", "Ci"]
    },
    loadType = [];
  for (var i in plans) {
    var plan = plans[i];
    var planId = plan.covCode;
    if (lm[planId]) {
      for (var j in lm[planId]) {
        var lt = lm[planId][j];
        if (loadType.indexOf(lt) == -1) {
          loadType.push(lt);
        }
      }
    }
  }
  return loadType;
}