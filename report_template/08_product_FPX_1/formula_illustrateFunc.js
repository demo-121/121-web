function(quotation, planInfo, planDetails, extraPara) {
  var round = function(value, position) {
    return runFunc(planDetails[planInfo.covCode].formulas.round, value, position);
  };
  var illustrations = {};
  illustrations.projRate = {};
  illustrations.reductInYield = {};
  var basicPlanCode = quotation.baseProductCode;
  var planDetail = planDetails[basicPlanCode];
  var projections = planDetail.projection;
  var retVal = [];
  var lowId = 999;
  var highId = -1;
  var projs = ['0.04', '0.08'];
  var maxSuCol = 65 - quotation.iAge > 30 ? 65 - quotation.iAge : 30;
  var tags = ['basic', 'changeSA', 'withdrawal'];
  var firstYearAJ = 0;
  for (var i = 0; i < 2; i++) {
    var id = Number(projections[i].title.en);
    lowId = Math.min(Number(id), lowId);
    highId = Math.max(Number(id), highId);
    var netCashFlow = [];
    for (var j = 0; j < 3; j++) {
      var tag = tags[j];
      var extraParamater = {
        'isChangeSA': tag == 'changeSA',
        'isWithdrawal': tag == 'withdrawal',
        'PIRR': id / 100,
        'firstYearAJ': firstYearAJ
      };
      if (!illustrations[tag]) illustrations[tag] = {};
      illustrations[tag][id] = runFunc(planDetail.formulas.getIllustration, quotation, planInfo, planDetails, extraParamater);
      if (illustrations[tag][id].firstYearAJ) firstYearAJ = illustrations[tag][id].firstYearAJ;
    }
    illustrations.projRate[id] = projs[i];
  }
  var yieldIllust = illustrations.basic[highId];
  illustrations.reductYield = runExcelFunc("IRR", yieldIllust.yieldCal, 0.00000001); /*runFunc(planDetails[planInfo.covCode].formulas.IRR,yieldIllust.yieldCal); */
  illustrations.yieldValue = yieldIllust.yieldValue;
  illustrations.yieldAtAge = yieldIllust.yieldAtAge;
  illustrations.yieldAtRate = yieldIllust.yieldAtRate;
  illustrations.warningMsg = "1st year insurance Charge ('COI') = " + parseFloat(Math.round(yieldIllust.COI_AP * 100 * 100) / 100).toFixed(2) + "% of Annualised Premium";
  for (var i = 0; i < illustrations.basic[highId].length; i++) {
    var illust_low = illustrations.basic[lowId][i];
    var illust_high = illustrations.basic[highId][i];
    var illust_changeSA_low = illustrations.changeSA[lowId][i];
    var illust_changeSA_high = illustrations.changeSA[highId][i];
    var illust_withdrawal_low = illustrations.withdrawal[lowId][i];
    var illust_withdrawal_high = illustrations.withdrawal[highId][i];
    var row = {
      polYear: illust_high.polYear,
      age: illust_high.age,
      totalPremPaid: illust_low.tpp,
      GteedDB: illust_high.gdb,
      GteedSV: 0,
      totalDistribCost: illust_high.tdc,
      sa: illust_changeSA_low.sa,
      amountWithdrawn: illust_withdrawal_low.amountWithdrawn,
      nonGteedDB: {},
      totalDB: {},
      nonGteedSV: {},
      totalSV: {},
      totalDB_withdrawal: {},
      nonGteedSV_withdrawal: {},
      totalDB_changeSA: {},
      nonGteedSV_changeSA: {},
      effectOfDeduct: {},
      VOP: {},
    };
    if (i === 0) {
      row.reductYield = illustrations.reductYield;
      row.yieldValue = illustrations.yieldValue;
      row.yieldAtAge = illustrations.yieldAtAge;
      row.yieldAtRate = illustrations.yieldAtRate;
      row.warningMsg = illustrations.warningMsg;
      row.projRate = {};
    }
    var ngdbZeroCnt = 0;
    var ngsvWdZeroCnt = 0;
    for (var j = 0; j < 2; j++) {
      var rateId = (j === 0) ? lowId : highId;
      var ill = illustrations.basic[rateId][i];
      var ill_changeSA = illustrations.changeSA[rateId][i];
      var ill_withdrawal = illustrations.withdrawal[rateId][i];
      row.nonGteedDB[rateId] = ill.ngdb;
      if (i < maxSuCol && rateId == lowId && ill.ngdb <= 0) {
        return {
          errorMsg: 'Please increase your Modal Premium, or reduce the Sum Assured to ensure the policy\'s sustainability'
        };
      }
      if (i < maxSuCol && rateId == lowId && ill_withdrawal.ngdb <= 0) {
        return {
          errorMsg: 'Please lower the withdrawal amount or choose a later start age for withdrawal'
        };
      }
      if (ill.ngdb === 0) {
        ngdbZeroCnt++;
      }
      if (ill_withdrawal.tsv === 0) {
        ngsvWdZeroCnt++;
      }
      row.totalDB[rateId] = round(Number(ill.tdb), 0);
      row.nonGteedSV[rateId] = ill.ngsv;
      row.totalSV[rateId] = round(Number(ill.tsv), 0);
      row.totalDB_withdrawal[rateId] = ill_withdrawal.tdb;
      row.nonGteedSV_withdrawal[rateId] = ill_withdrawal.tsv;
      row.totalDB_changeSA[rateId] = ill_changeSA.tdb;
      row.nonGteedSV_changeSA[rateId] = ill_changeSA.tsv;
      row.effectOfDeduct[rateId] = ill.eod;
      row.VOP[rateId] = ill.vop;
      if (i === 0) {
        row.projRate[rateId] = illustrations.projRate[rateId];
      }
      row.GteedDB = (ngdbZeroCnt < 2) ? illust_high.gdb : 0;
      row.amountWithdrawn = (ngsvWdZeroCnt < 2) ? illust_withdrawal_low.amountWithdrawn : 0;
    }
    retVal.push(row);
    if (illust_high.age === 99) break;
  }
  return retVal;
}