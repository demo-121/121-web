function(planDetail, quotation) {
  var bpPolTerm = Number.parseInt(quotation.plans[0].policyTerm);
  if (!bpPolTerm) {
    return [];
  }
  var policyTermList = [];
  if (quotation.policyOptions.planType === 'renew') {
    if (bpPolTerm + quotation.iAge > 65) {
      policyTermList.push({
        value: '65_TA',
        title: 'To Age 65'
      });
    } else {
      policyTermList.push({
        value: bpPolTerm + '_YR',
        title: bpPolTerm + ' Years'
      });
    }
  } else if (quotation.policyOptions.planType === 'toAge') {
    if (bpPolTerm > 65) {
      policyTermList.push({
        value: '65_TA',
        title: 'To Age 65'
      });
    } else {
      policyTermList.push({
        value: bpPolTerm + '_TA',
        title: 'To Age ' + bpPolTerm
      });
    }
  }
  return policyTermList;
}