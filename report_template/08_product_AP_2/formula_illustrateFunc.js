function(quotation, planInfo, planDetails, extraPara) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var planDetail = planDetails[planInfo.covCode];
  var planType = quotation.policyOptions.planType;
  var indexation = quotation.policyOptions.indexation === 'Y';
  var polYrs = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
  var premYrs;
  if (planInfo.premTerm === 'SP') {
    premYrs = 1;
  } else if (planInfo.premTerm.endsWith('_YR')) {
    premYrs = Number.parseInt(planInfo.premTerm);
  } else {
    premYrs = Number.parseInt(planInfo.premTerm) - quotation.iAge;
  }
  var sumInsured = planInfo.sumInsured;
  var illYrs;
  if (planType === 'renew') {
    illYrs = Math.min((Math.floor((60 - quotation.iAge) / polYrs) + 1) * polYrs, 65 - quotation.iAge);
  } else {
    illYrs = polYrs;
  }
  var yearPrems = [0];
  for (var i = 1; i <= illYrs; i++) {
    var age = quotation.iAge + i - 1;
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, age) || 0);
    var yearPrem;
    if (i === 1) {
      yearPrem = math.bignumber(planInfo.yearPrem);
    } else {
      yearPrem = yearPrems[i - 1];
      if (planType === 'renew') {
        if (i % polYrs === 1) {
          yearPrem = roundDown(math.multiply(premRate, sumInsured, 0.001), 2);
        } else if (i % polYrs > premYrs) {
          yearPrem = math.bignumber(0);
        }
      }
    }
    yearPrems.push(yearPrem);
  }
  var commRates = runFunc(planDetail.formulas.getCommRates, quotation, planInfo, planDetails);
  var totComms = [0];
  for (var i = 1; i <= illYrs; i++) {
    var commRate = commRates[(i - 1) % polYrs];
    if (commRate) {
      totComms[i] = math.multiply(yearPrems[i], commRate);
    }
  }
  var illustration = [];
  var illLength = Math.max(totComms.length, yearPrems.length) - 1;
  for (var i = 0; i < illLength; i++) {
    illustration.push({
      yearPrem: yearPrems[i + 1] ? math.number(yearPrems[i + 1]) : 0,
      totComm: totComms[i + 1] ? math.number(totComms[i + 1]) : 0
    });
  }
  return illustration;
}