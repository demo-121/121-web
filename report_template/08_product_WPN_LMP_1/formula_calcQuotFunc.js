function(quotation, planInfo, planDetails) {
  if (planDetails['WPN_LMP'].inputConfig) {
    if (quotation.plans[0].premTerm) {
      var array = planDetails['WPN_LMP'].inputConfig.policyTermList;
      for (var i = 0; i < array.length; i++) {
        if (Number.parseInt(array[i].value) == Number.parseInt(quotation.plans[0].premTerm)) {
          planInfo.policyTerm = array[i].value;
          planInfo.polTermDesc = array[i].title;
        }
      }
    }
    if (planDetails['WPN_LMP'].inputConfig.premTermList) {
      var array = planDetails['WPN_LMP'].inputConfig.premTermList;
      for (var i = 0; i < array.length; i++) {
        if (Number.parseInt(array[i].value) == Number.parseInt(planInfo.policyTerm)) {
          planInfo.premTerm = array[i].value;
          planInfo.premTermDesc = array[i].title;
        }
      }
    }
  }
  if (quotation.policyOptions.multiFactor && planInfo.premTerm) {
    if (planInfo.policyTerm && planInfo.premTerm) {
      if (quotation.sameAs === "Y") {
        var plans = quotation.plans;
        var sumofprod = 0;
        for (var p in plans) {
          if (plans[p].covCode !== "WPN_LMP" && plans[p].covCode !== "PPERA_LMP" && plans[p].covCode !== "PPEPS_LMP" && plans[p].premium) {
            sumofprod += plans[p].yearPrem;
          }
        }
        planInfo.sumInsured = Number.parseInt(sumofprod * 1000) / 1000;
      } else {
        planInfo.sumInsured = 0;
      }
    } /* planCode needed for prem calculation */
    var planCode = 'WP';
    var planCodeREF = 'CIPE';
    var premTermYr;
    var policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
    if (planInfo.policyTerm.indexOf('TA') > -1) {
      policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    } else {
      policyTermYr = Number.parseInt(planInfo.policyTerm);
    }
    planInfo.premTermYr = premTermYr;
    planInfo.policyTermYr = policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      planCodeREF = Number.parseInt(planInfo.premTerm) + planCodeREF;
    } else {
      planCodeREF = planCodeREF + Number.parseInt(planInfo.premTerm);
    }
    planInfo.planCodeREF = planCodeREF;
    planInfo.planCode = planCode;
    quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
    if (planInfo.sumInsured) {
      planInfo.multiplyBenefit = planInfo.sumInsured * quotation.policyOptions.multiFactor;
      planInfo.multiplyBenefitAfter70 = planInfo.multiplyBenefit * 0.5;
      var policyYearRefer = 0;
      if (planInfo.premTerm.indexOf('YR') > -1) {
        policyYearRefer = Number.parseInt(planInfo.premTerm);
      } else {
        policyYearRefer = Number.parseInt(planInfo.premTerm) - quotation.iAge;
      }
      if (planInfo.premium) {
        var crate;
        var channel = quotation.agent.dealerGroup.toUpperCase();
        var commission_rate = planDetails['WPN_LMP'].rates.commissionRate[channel];
        planInfo.cummComm = [0];
        var cummComm = 0;
        for (var rate in commission_rate) {
          if (policyYearRefer < 6) {
            crate = commission_rate[rate][0];
          } else if (policyYearRefer < 10) {
            crate = commission_rate[rate][1];
          } else if (policyYearRefer < 15) {
            crate = commission_rate[rate][2];
          } else if (policyYearRefer < 20) {
            crate = commission_rate[rate][3];
          } else if (policyYearRefer < 25) {
            crate = commission_rate[rate][4];
          } else {
            crate = commission_rate[rate][5];
          }
          cummComm += crate * planInfo.premium;
          planInfo.cummComm.push(crate);
        }
      }
    } else {
      planInfo.multiplyBenefit = null;
      planInfo.multiplyBenefitAfter70 = null;
      planInfo.premium = null;
    }
  }
}