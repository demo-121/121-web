var async = require('async');
const _ = require('lodash');
const schedule = require('node-schedule');
const moment = require('moment');
const logger = global.logger || console;

const {callApiByGet} = require('../utils/RemoteUtils');
const dao = require('../cbDaoFactory').create();

const sysParameter = 'sysParameter';

// Couchbase File Name
const currentDate  = new Date();
const jobDetailsDocKey = `DataPatchJob_${currentDate.getUTCFullYear()}_${currentDate.getUTCMonth() + 1}_${currentDate.getDate()}`;

const addTrxLogs = function(runTime, trxLogs, errLogs) {
  let nowString = moment().toDate().toISOString();
  let runTimeLong = runTime.getTime();
  let runTimeString = runTime.toISOString();
  return new Promise((resolve) => {
    dao.getDoc(jobDetailsDocKey, (doc) => {
      if (doc && !doc.error) {
        let trxData = _.get(doc, `log.${runTimeLong}`);
        if (trxData) {
          let currData = _.cloneDeep(trxData);
          trxData.transaction = _.has(currData, 'transaction') ? _.concat([], currData.transaction, trxLogs) : trxLogs;
          if (!_.isEmpty(errLogs)) {
            trxData.error = _.has(currData, 'error') ? _.concat([], currData.error, errLogs) : errLogs;
          }
        } else {
          trxData = Object.assign(
            {},
            {transaction: trxLogs},
            !_.isEmpty(errLogs) ? {error: errLogs} : {}
          );
        }
        trxData.completeTime = nowString;
        trxData.runTime = runTimeString;

        doc.log[runTimeLong] = trxData;

        logger.log(`Jobs :: DataPatchJob :: update doc ${jobDetailsDocKey} - runTime=${runTimeString} completedTime=${nowString}`);
        dao.updDoc(jobDetailsDocKey, doc, (result) => {
          doc._rev = result.rev;
          resolve(doc);
        });
      } else if (doc && doc.error === 'not_found') {
        let newDoc = {
          log: {}
        };

        newDoc.log[runTimeLong] = Object.assign(
          {},
          {
            runTime: runTimeString,
            completeTime: nowString,
            transaction: trxLogs
          },
          !_.isEmpty(errLogs) ? { error: errLogs } : {}
        );

        logger.log(`Jobs :: DataPatchJob :: new doc ${jobDetailsDocKey} - runTime=${runTimeString} completedTime=${nowString}`);
        dao.updDoc(jobDetailsDocKey, newDoc, (result) => {
          newDoc._rev = result.rev;
          resolve(newDoc);
        });
      } else {
        logger.log(`Jobs :: DataPatchJob :: cannot find job details - runTime=${runTimeString} completedTime=${nowString}`);
        resolve(false);
      }
    });
  });
};

const checkIsJobExecutable = function(runTime, sysParam) {
  return new Promise((resolve, reject) => {
    let fromTime = _.get(sysParam, 'fromTime');
    let toTime = _.get(sysParam, 'toTime');
    if (fromTime && toTime) {
      let runDateTime = moment(runTime);
      // The range of fromTime and toTime needs to include the scheduleCron Time
      let fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
      let toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
      //call API to check if this node server can perform Invalidation or not
      if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
        callApiByGet('/fairBiJobRunner', (result) => {
          resolve(_.get(result, 'success'));
        });
      } else {
        resolve(false);
      }
    } else {
      reject(new Error(`Fail to get fromTime="${fromTime}" toTime="${toTime}" in ${sysParameter}`));
    }
  });
};

const dataPatch = (datapatchDoc, docId, runTime, resolve) => {
  async.waterfall([
    (callback) => {
      logger.log(`Jobs :: DataPatchJob :: Get Doc ${docId}}`);
      dao.getDoc(docId, doc => {
        if (doc && !doc.error) {
          callback(null, doc);
        } else {
          callback(`Cannot get Doc ::: ${docId}`);
        }
      });
    }, (doc, callback) => {
      let expectedPolicyStatus = _.get(datapatchDoc, 'originalPolicyStatus');
      let originalRLSSubmittedDate = _.get(datapatchDoc, 'rlsSubmittedDate');
      let specialHandle = _.get(datapatchDoc, 'specialHandle');

      let status = _.get(doc, 'approvalStatus');
      let expiryDate = _.get(doc, 'expiredDate');
      let dataptachBefore = {
        status, expiryDate
      };

      let datapatchAfter = {};
      if (expectedPolicyStatus === 'E' && specialHandle) {
        doc = _.omit(doc, ['expiredDate', 'submisssionFlag']);
        doc.approvalStatus = 'SUBMITTED';
        datapatchAfter = {
          omitExpiredDate: true,
          status:doc.approvalStatus
        };
      } else if (expectedPolicyStatus === 'E') {
        doc.expiredDate = originalRLSSubmittedDate;
        datapatchAfter = {
          expiredDate: doc.expiredDate
        };
      } else {
        doc = _.omit(doc, 'expiredDate');
        doc.approvalStatus = expectedPolicyStatus;
        datapatchAfter = {
          omitExpiredDate: true,
          status: doc.approvalStatus
        };
      }
      dao.updDoc(docId, doc, (result) => {
        callback(null, {doc, dataptachBefore, datapatchAfter});
      });
    }
  ], (err, result) => {
      if (err) {
        logger.error('Jobs :: DataPatchJob :: Error in dataPatch: ', err);
        addTrxLogs(runTime, [],[
          {
            docId: docId,
            error: err && err.toString()
          }
        ]).then(() => {
          resolve(false);
        }).catch(error => {
          logger.error(`Jobs :: DataPatchJob :: Error in dataPatch:: addTrxLogs :: ${error}`);
          resolve(false);
        });
      } else {
        logger.info('Jobs :: DataPatchJob :: Logs in dataPatch: ', docId);
        addTrxLogs(runTime, [{
          docId: docId,
          beforeDataPatch: result.dataptachBefore,
          afterDataPatch: result.datapatchAfter
        }]).then(() => {
          resolve(true);
        }).catch(error => {
          logger.error(`Jobs :: DataPatchJob :: Error in dataPatch:: addTrxLogs :: ${error}`);
          resolve(false);
        });
      }
  });
};

const doDataPatch = function(dataPatchFile = {}, runTime, index = 0) {
  let impactedCases = dataPatchFile.impactedCases;
  if (index < impactedCases.length) {
    let datapatchDoc = impactedCases[index];
    let docId = _.get(datapatchDoc, 'docId');
    return new Promise((resolve, reject) => {
      dataPatch(datapatchDoc, docId, runTime, resolve);
    }).then((success) => {
      logger.log(`Data Patch Status for ${docId} ${success}`);
      return doDataPatch(dataPatchFile, runTime, index + 1);
    }).catch(error => {
      logger.error(`Jobs :: DataPatchJob :: ERROR caught at Promise doDataPatch ${moment().toDate()}`, error);
      return doDataPatch(dataPatchFile, runTime, index + 1);
    });
  } else {
    logger.log('Jobs :: DataPatchJob :: COMPLETED DataPatchJobg');
    return Promise.resolve(dataPatchFile);
  }
};

const executeJob = function (runTime, sysParam) {
  logger.log(`Jobs :: DataPatchJob :: JOB STARTED at ${runTime}`);
  const datapatchId = _.get(sysParam, 'dataPatchDocId');
  return new Promise((resolve, reject) => {
    dao.getDoc(datapatchId, (datapatchFile) => {
      resolve(datapatchFile);
    });
  }).then(dataPatchFile => {
    if (dataPatchFile && !dataPatchFile.error) {
      if (!dataPatchFile.completed && dataPatchFile.impactedCases) {
        return dataPatchFile;
      } else {
         throw Error('Completed Before :: Skipped');
      }
    } else {
      throw Error('Jobs :: DataPatchJob :: Cannot find data patch doc');
    }
  }).then(dataPatchFile => {
    return doDataPatch(dataPatchFile, runTime);
  }).then((dataPatchFile) => {
    dataPatchFile.completed = true;
    dataPatchFile.completedTime = new Date().toISOString();
    return new Promise(resolve => {
      dao.updDoc(datapatchId, dataPatchFile, (result) => {
        resolve();
      });
    });
  }).then(() => {
    logger.log(`Jobs :: DataPatchJob :: JOB ENDED at ${moment().toDate()}`);
  }).catch(error => {
    logger.error(`Jobs :: DataPatchJob :: ERROR caught at FINAL ${moment().toDate()}`, error);

    let errors = [];
    let errorStr = error instanceof Error ? error.toString() : _.toString(error);
    let errorLog = _.isString(errorStr) ? errorStr : 'Unknown error type: ' + typeof errorStr;
    errors.push(errorLog);
    addTrxLogs(runTime, [], errors);
  });
};

module.exports.execute = () => {
  return new Promise((resolve, reject) => {
    dao.getDoc(sysParameter, (param) => {
      if (param && !param.error) {
        let dataPatchSysParam = _.get(param, 'datapatch');
        let scheduleCron = _.get(dataPatchSysParam, 'scheduleCron');
        let fromTime = _.get(dataPatchSysParam, 'fromTime');
        let toTime = _.get(dataPatchSysParam, 'toTime');

        if (dataPatchSysParam && scheduleCron) {
          schedule.scheduleJob(scheduleCron, (runTime) => {
            checkIsJobExecutable(runTime, dataPatchSysParam).then((run) => {
              let runDateTime = moment(runTime);
              let fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
              let toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
              if (run) {
                executeJob(runTime, dataPatchSysParam);
              } else if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
                logger.log(`Jobs :: DataPatchJob :: JOB SKIPPED at ${runTime}`);
              }
            });
          });
          resolve();
        } else {
          reject(new Error(`Fail to get scheduleCron="${scheduleCron}" in ${sysParameter}`));
        }
      } else {
        reject(new Error('Fail to get doc', sysParameter));
      }
    });
  });
};
