var async = require('async');
const _ = require('lodash');
const schedule = require('node-schedule');
const moment = require('moment');
const logger = global.logger || console;

const {callApiByGet} = require('../utils/RemoteUtils');
const {getAllCustomerWithMutipleBaseProductCode, getAllAgents, insertNoticeToAgent, getAllValidBundleId, getImpactedClientIds} = require('./invalidateFunctions');
const dao = require('../cbDaoFactory').create();
const bundleDao = require('../cbDao/bundle');
const agentDao = require('../cbDao/agent');
const prodDao = require('../cbDao/product');

const generalInvalidationParameter = 'generalInvalidationParameter';
const rules = require('./inflightRules/rules.json');
const {invalidationJobHandlers, inflightMsgHandling} = require('./inflightRules/invalidationJobs');

// Couchbase File Name
const currentDate  = new Date();


const addTrxLogs = function(runTime, sysParam, trxLogs, errLogs) {
  let nowString = moment().toDate().toISOString();
  let runTimeLong = runTime.getTime();
  let runTimeString = runTime.toISOString();
  let {jobDetailsDocName} = sysParam;
  return new Promise((resolve) => {
    dao.getDoc(jobDetailsDocName, (doc) => {
      if (doc && !doc.error) {
        let trxData = _.get(doc, `log.${runTimeLong}`);
        if (trxData) {
          let currData = _.cloneDeep(trxData);
          trxData.transaction = _.has(currData, 'transaction') ? _.concat([], currData.transaction, trxLogs) : trxLogs;
          if (!_.isEmpty(errLogs)) {
            trxData.error = _.has(currData, 'error') ? _.concat([], currData.error, errLogs) : errLogs;
          }
        } else {
          trxData = Object.assign(
            {},
            {transaction: trxLogs},
            !_.isEmpty(errLogs) ? {error: errLogs} : {}
          );
        }
        trxData.completeTime = nowString;
        trxData.runTime = runTimeString;

        doc.log[runTimeLong] = trxData;

        logger.log(`Jobs :: GeneralInvalidationJob:: InflightInvalidateJob :: update doc ${jobDetailsDocName} - runTime=${runTimeString} completedTime=${nowString}`);
        dao.updDoc(jobDetailsDocName, doc, (result) => {
          doc._rev = result.rev;
          resolve(doc);
        });
      } else if (doc && doc.error === 'not_found') {
        let newDoc = {
          log: {}
        };

        newDoc.log[runTimeLong] = Object.assign(
          {},
          {
            runTime: runTimeString,
            completeTime: nowString,
            transaction: trxLogs
          },
          !_.isEmpty(errLogs) ? { error: errLogs } : {}
        );

        logger.log(`Jobs :: GeneralInvalidationJob:: InflightInvalidateJob :: new doc ${jobDetailsDocName} - runTime=${runTimeString} completedTime=${nowString}`);
        dao.updDoc(jobDetailsDocName, newDoc, (result) => {
          newDoc._rev = result.rev;
          resolve(newDoc);
        });
      } else {
        logger.log(`Jobs :: GeneralInvalidationJob:: InflightInvalidateJob :: cannot find job details - runTime=${runTimeString} completedTime=${nowString}`);
        resolve(false);
      }
    });
  });
};


const invalidateCustomer = function(runTime, cid, sysParam, channels, productsVersion) {
  return bundleDao.getCurrentBundle(cid).then(bundle => {
    if (!bundle) {
      throw new Error('Bundle is null');
    }
    const agent = {
      agentCode: bundle.agentCode,
      channel: {
        code: bundle.dealerGroup
      },
      compCode: bundle.compCode
    };
    const isFaChannel = _.get(channels, `channels.${bundle.dealerGroup}.type`) === 'FA';

    let promises = [];
    //get data for processing case
    let docMapping = {};
    let isAppMapping = {};
    let allValidDocIdsInBundle = [];
    let allValidBundleApplications = [];
    let hasFullySignedCaseInBundle = false;
    _.forEach(bundle.applications, app => {
      let isApplication = _.has(app, 'applicationDocId');
      let docId = '';

      if (isApplication && (_.get(app, 'appStatus') === 'APPLYING' || _.get(app, 'appStatus') === 'SUBMITTED')) {
        docId = _.get(app, 'applicationDocId', '');
      }
      else if (!isApplication && !_.get(app, 'appStatus')) {
        //exclude INVALIDATED and INVALIDATED_SIGNED application
        docId = _.get(app, 'quotationDocId', '');
      }
      if (docId.length > 0) {
        promises.push(new Promise((resolve, reject) => {
          dao.getDoc(docId, (doc) => {
            if (doc && !doc.error) {
              docMapping[docId] = doc;
              isAppMapping[docId] = isApplication;
              allValidDocIdsInBundle.push(docId);


              let isFullySigned = _.get(doc, 'isFullySigned', false);
              let policyNumber  = [_.get(doc, 'policyNumber', '')];
              //get shield policyNumber
              if (isApplication && doc.type) {
                if (doc.type === 'masterApplication') {
                  if (docId.indexOf('SA') > -1) {
                    policyNumber = [docId.replace('SA', 'SP')];
                    if (doc.iCidMapping) {
                      _.each(Object.keys(doc.iCidMapping), cid => {
                        _.each(doc.iCidMapping[cid], cidMapping => {
                          if (cidMapping.policyNumber) {
                            policyNumber.push(cidMapping.policyNumber);
                          }
                        })
                      })
                    }
                  }
                }
              }

              let modifyBundleApp = Object.assign(app,{docId,isFullySigned,policyNumber});
              allValidBundleApplications.push(modifyBundleApp);
              if (isFullySigned){
                hasFullySignedCaseInBundle = true;
              }
              resolve({docId, isApplication, doc});
            } else {
              reject(new Error(`Fail to get doc ${docId}`));
            }
          });
        }));
      }
    });

    return Promise.all(promises).then(docs => {

      let invalidatedIdList = [];
      let policyLists = [];
      let policyId2docIdMapping = {};

      let agentCode = bundle.agentCode;
      let inflightInputData = {
        bundleApplications: allValidBundleApplications,
        docs,
        sysParam,
        docMapping,
        agentCode: agentCode
      };
      let invalidateJobResults = {};

      const inflightJobs = _.get(sysParam, 'jobs');
      _.forEach(inflightJobs, job => {
          let invalidateJobFunc = invalidationJobHandlers.inflightJobFunct;

          let jobParams = {
            ruleIds: job.ruleIds,
            rulesParamsMapping: job.rulesParamsMapping,
            rulesCombinations: job.rulesCombinations
          };
          if (invalidateJobFunc && typeof invalidateJobFunc === 'function') {
            invalidateJobResults = invalidateJobFunc(inflightInputData, jobParams);
          }
      });

      let {invalidateDocIds, docIds2CombinationsResult} = invalidateJobResults;
      let inflightMsgIds = [];
      let hasNotSubmittedNonImpactCases = false;
      let hasImpactCases = false;
      let hasNotSubmittedImpactCases = false;
      if (invalidateDocIds && invalidateDocIds.length > 0){
        //invalidateDocIds pass all the invalidate rules combination
        //has some case satisfy the invalidate rules, check all the cases in the bundle

        //special handle the submitted impact cases, no applying impact cases, and non-impact not submitted cases
        let isInvalidateNotSubmittedNonImpactCases = true;
        _.forEach(allValidBundleApplications, app => {
          let {docId} = app;
          let isImpactCase = _.indexOf(invalidateDocIds, docId) > -1;
          if (isImpactCase){
            //impact case
            hasImpactCases = true;
            if (_.get(app, 'appStatus') !== 'SUBMITTED'){
              hasNotSubmittedImpactCases = true;
            }
          } else if (!isImpactCase && _.get(app, 'appStatus') !== 'SUBMITTED'){
            //non-impact cases, not submitted case
            hasNotSubmittedNonImpactCases = true;
          }
        });
        if (hasImpactCases && !hasNotSubmittedImpactCases && hasNotSubmittedNonImpactCases ) {
          isInvalidateNotSubmittedNonImpactCases = false;
        }

        _.forEach(allValidBundleApplications, app => {
          let {docId,isFullySigned,policyNumber} = app;
          let isImpactCase = _.indexOf(invalidateDocIds, docId) > -1;


          if (!isFullySigned){   // not yet fully signed
            if (isImpactCase ||  (!isImpactCase && hasFullySignedCaseInBundle && isInvalidateNotSubmittedNonImpactCases) ){
              invalidatedIdList.push(docId);  //invalidated
              let messageIds = inflightMsgHandling(inflightInputData, docIds2CombinationsResult, docId);
              inflightMsgIds = _.union(inflightMsgIds, messageIds);
            }
          } else if (isFullySigned){
            if (_.get(app, 'appStatus') === 'SUBMITTED'){
              //SUBMITTED case add policy number, handle approval status later
              if (isImpactCase && policyNumber){
                policyLists.push(...policyNumber);
                policyId2docIdMapping[policyNumber[0]] = docId;
              }
            }
            else {
              //this full signed and not submitted case
              //invalidated if it is imapact case
              //for non-imapact case, need to check isInvalidateNotSubmittedNonImpactCases
              if (isImpactCase ||  (!isImpactCase && isInvalidateNotSubmittedNonImpactCases) ){
                invalidatedIdList.push(docId);
                let messageIds = inflightMsgHandling(inflightInputData, docIds2CombinationsResult, docId);
                inflightMsgIds = _.union(inflightMsgIds, messageIds);
              }
            }
          }
        });

        //inflight message handling
        if (agentCode){
          let {agentMsgMapping} = sysParam;
          if (agentMsgMapping){
            if (!agentMsgMapping[agentCode]) {
              agentMsgMapping[agentCode] = [];
            }
            if (agentMsgMapping[agentCode]) {
              agentMsgMapping[agentCode] = _.union(agentMsgMapping[agentCode], inflightMsgIds);
            }
          }
        }
      }

      let handleCases = {
        isCreateNewBundle: false,
        invalidatedIdList: invalidatedIdList,
        policyLists: policyLists,
        policyId2docIdMapping
      };

      return {handleCases, invalidateJobResults, inflightInputData, agentCode};
    }).then(allInflightDetails => {
      let {handleCases, invalidateJobResults, inflightInputData, agentCode} = allInflightDetails;
      let {docIds2CombinationsResult} = invalidateJobResults;

      // Expired The poclies
      let { policyLists, policyId2docIdMapping} = handleCases;
      let policyPromises = [];
      _.each(policyLists, policyId =>{
        policyPromises.push(new Promise((resolve, reject) => {
              dao.getDoc(policyId, function(foundCase){
                  resolve(foundCase);
              });
          }).then(appCase => {
            if (appCase && appCase.submittedDate && ['R','A','E'].indexOf(appCase.approvalStatus) === -1) {
              appCase.approvalStatus = 'E';
              appCase.expiredDate = new Date().toISOString();

              let docId = policyId2docIdMapping[policyId];
              let inflightMsgIds = inflightMsgHandling(inflightInputData, docIds2CombinationsResult, docId);
              //inflight message handling for submitted case, (not approved,not rejected, not expired cases)
              if (agentCode){
                let {agentMsgMapping} = sysParam;
                if (agentMsgMapping){
                  if (!agentMsgMapping[agentCode]) {
                    agentMsgMapping[agentCode] = [];
                  }
                  if (agentMsgMapping[agentCode]) {
                    agentMsgMapping[agentCode] = _.union(agentMsgMapping[agentCode], inflightMsgIds);
                  }
                }
              }

              return new Promise(resolve => {
                  dao.updDoc(policyId, appCase, function(result) {
                      if (result && !result.error) {
                          resolve(policyId);
                      } else {
                          resolve(false);
                      }
                  });
              });
            } else {
              return Promise.resolve();
            }
          }
        ));
      });
      return Promise.all(policyPromises).then(() =>{
        return handleCases;
      });
    }).then(handleCases => {
      if (!handleCases) {
        return null;
      }

      let { isCreateNewBundle, invalidatedIdList, policyLists} = handleCases;

      if (isCreateNewBundle) {
        return bundleDao.createNewBundle(cid, agent, false).then(bundles => {
          return addTrxLogs(runTime, sysParam, [{
            cid,
            bundleId: _.get(bundle, 'id'),
            agentCode: bundle.agentCode,
            action: 'createNewBundle',
            newBundleId: _.get(_.find(bundles, b => {return b.isValid; }), 'id'),
            triggerByDocIds: invalidatedIdList
          }]);
        }).then(lResult => {
          return agent.agentCode;
        });
      } else if (invalidatedIdList.length > 0) {
        return recursiveInvalidateCase(cid, invalidatedIdList).then(docIds => {
          return addTrxLogs(runTime, sysParam, [{
            cid,
            bundleId: _.get(bundle, 'id'),
            agentCode: bundle.agentCode,
            action: 'invalidateApplicationById',
            triggerByDocIds: docIds
          }]);
        }).then(lResult => {
          if (!isFaChannel) {
            return bundleDao.rollbackApplication2Step1(cid);
          } else {
            return;
          }
        }).then(() => {
          return agent.agentCode;
        });
      } else if (policyLists && policyLists.length > 0) {
        return addTrxLogs(runTime, sysParam, [{
          cid,
          bundleId: _.get(bundle, 'id'),
          agentCode: bundle.agentCode,
          action: 'expiredNotCompletedPolicy',
          triggerByDocIds: policyLists
        }]).then(result => {
          return agent.agentCode;
        });
      }
      return null;
    }).then(agentCode => {
      if (agentCode) {
        let {agentMsgMapping} = sysParam;
        return {agentCode, agentMsgMapping};
      } else {
        return addTrxLogs(runTime, sysParam, [{
          cid,
          bundleId: _.get(bundle, 'id'),
          agentCode: _.get(bundle, 'agentCode'),
          action: 'checked'
        }]).then(lResult => {
          return null;
        });
      }
    });
  }).catch(error => {
    logger.error(`Jobs :: InflightInvalidateJob :: ERROR caught at ${moment().toDate()} - `, cid, error);

    let errors = [];
    let errorStr = error instanceof Error ? error.toString() : _.toString(error);
    let errorLog = _.isString(errorStr) ? errorStr : 'Unknown error type: ' + typeof errorStr;
    errors.push('[' + cid + '] - ' + errorLog + ' - at ' + moment().toISOString());
    return addTrxLogs(runTime, sysParam, [{
      cid,
      action: 'checked',
      error: errorLog
    }], errors).then(result => {
      return null;
    });
  });
};

const recursiveInvalidateCustomer = function (runTime, sysParam, channels, productsVersion, cids, agents, handledAgentCodes = [], index = 0) {
  if (index !== cids.length) {
    logger.log('Jobs :: InflightInvalidateJob - [GeneralInvalidationJob] :: EXECUTE Inflight Case Handling Process - START ' + (index + 1) + '/' + cids.length + ' (' + cids[index] + ')');
    return invalidateCustomer(runTime, cids[index], sysParam, channels, productsVersion).then(agentDetails => {
      let willHandleNotification = agentDetails && agentDetails.agentCode;
      // let willHandleNotification = agentDetails && agentDetails.agentCode && handledAgentCodes.indexOf(agentDetails.agentCode) < 0 ;
      logger.log('Jobs :: InflightInvalidateJob - [GeneralInvalidationJob] :: EXECUTE Inflight Case Handling Process - END ' + (index + 1) + '/' + cids.length + ' (' + cids[index] + ')' + (willHandleNotification ? ' handleNotificationAgentCode=' + agentDetails.agentCode : ''));

      if (willHandleNotification) {
        //handledAgentCodes.push(agentDetails.agentCode);
        agentDetails.agent = _.find(agents, agent => { return agent.agentCode === agentDetails.agentCode; });
        return agentDetails;
      } else {
        return;
      }
    }).then(agentDetails => {
      if (agentDetails && agentDetails.agent) {
        let {agent, agentCode, agentMsgMapping} = agentDetails;
        let {messages} = sysParam;
        let messageIds = agentMsgMapping[agentCode];

        let inflightMessageAdded = _.filter(messages, function (msg) {
          if (messageIds.indexOf(msg.messageId) > -1) {
            return msg;
          }
        });
        let invalidateAgentMsgId = _.map(inflightMessageAdded, 'messageId');
        let invalidateAgentMsg = _.map(inflightMessageAdded, 'message');
        let messageGroup = _.map(inflightMessageAdded, 'messageGroup');
        let messageGroupPriority = _.map(inflightMessageAdded, 'messageGroupPriority');


        return insertNoticeToAgent(agent.id, invalidateAgentMsgId, invalidateAgentMsg, messageGroup, messageGroupPriority, true).then((iResult) => {
          if (iResult && !iResult.error) {
            return;
          } else {
            throw Error(`Fail to insert notification to agent ${agent.agentCode}`);
          }
        });
      } else {
        return;
      }
    }).then(() => {
      const timeout = _.get(sysParam, 'perCaseTimeout', 0);
      if (timeout) {
        async.waterfall([
          (callback) => {
            logger.log('Jobs :: InflightInvalidateJob - [GeneralInvalidationJob] :: Set Timeout ');
            setTimeout(function() {
              logger.log('Jobs :: InflightInvalidateJob - [GeneralInvalidationJob] :: Successfully callback in time out');
              callback(null, {success: true});
            }, timeout);
          }
        ], (err, result) => {
            if (err) {
              logger.error('Jobs :: InflightInvalidateJob - [GeneralInvalidationJob] :: Error in searchApprovalCaseByAppId: ', err);
              throw Error(`Jobs :: InflightInvalidateJob :: Fail to setTimeout ${cids[index]}`);
            } else {
              return recursiveInvalidateCustomer(runTime, sysParam, channels, productsVersion, cids, agents, handledAgentCodes, index + 1);
            }
        });
      } else {
        return recursiveInvalidateCustomer(runTime, sysParam, channels, productsVersion, cids, agents, handledAgentCodes, index + 1);
      }
    }).catch(error => {
      logger.error('Jobs :: InflightInvalidateJob - [GeneralInvalidationJob] :: Error in recursiveInvalidateCustomer: ', error);
      return recursiveInvalidateCustomer(runTime, sysParam, channels, productsVersion, cids, agents, handledAgentCodes, index + 1);
    });
  } else {
    logger.log('Jobs :: InflightInvalidateJob - [GeneralInvalidationJob] :: COMPLETED Inflight Case Handling');
    return;
  }
};



const createImpactedClientList = function(docName, cids) {
  return new Promise((resolve, reject) => {
    try {
      dao.getDoc(docName, (doc) => {
        if (doc && !doc.error) {
          doc.lastUpdatedDate = new Date().toISOString();
          doc.impactedClientList = cids;

          logger.log(`Jobs :: GeneralInvalidationJob:: InflightInvalidateJob :: update doc ${docName} - Create Impacted Client List`);
          dao.updDoc(docName, doc, (result) => {
            doc._rev = result.rev;
            resolve(doc);
          });
        } else if (doc && doc.error === 'not_found') {
    let updatedDoc = {
      lastUpdatedDate: new Date().toISOString(),
      impactedClientList: cids
    };
      logger.log(`Jobs :: GeneralInvalidationJob:: InflightInvalidateJob :: update doc ${docName} - Create Impacted Client List`);
      dao.updDoc(docName, updatedDoc, (result) => {
        updatedDoc._rev = result.rev;
        resolve(updatedDoc);
      });
        } else {
          resolve(false);
        }
      });
    } catch (err) {
      logger.log(`Error :: Jobs :: GeneralInvalidationJob:: InflightInvalidateJob :: update doc ${docName} - Create Impacted Client List`);
      reject(err);
    }
  });
};

const checkIsJobExecutable = function(runTime, sysParam) {
  return new Promise((resolve, reject) => {
    let fromTime = _.get(sysParam, 'fromTime');
    let toTime = _.get(sysParam, 'toTime');
    if (fromTime && toTime) {
      let runDateTime = moment(runTime);
      // The range of fromTime and toTime needs to include the scheduleCron Time
      let fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
      let toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
      //call API to check if this node server can perform Invalidation or not
      if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
        callApiByGet('/fairBiJobRunner', (result) => {
          resolve(_.get(result, 'success'));
        });
      } else {
        resolve(false);
      }
    } else {
      reject(new Error(`Fail to get fromTime="${fromTime}" toTime="${toTime}" in ${generalInvalidationParameter}`));
    }
  });
};

const prepareChannels = function() {
  return new Promise((resolve, reject) => {
    agentDao.getChannels(channels => {
      if (channels) {
        resolve(channels);
      } else {
        reject(new Error('Fail to get channels'));
      }
    });
  });
};

const prepareProductList = function(sysParam) {
  let promises = [];
  //TODO Add Product Version Handling in inFlight
  /**
   * This is not included in the inflight job revamp
   * Job will invalidate all cases when the case meet the requirement, will not check product version
   * rules --> rules.json should move to generalInvalidationParameter.json
   */
  _.forEach(_.get(sysParam, 'handleCovCode', {}), (isHandle, covCode) => {
    if (isHandle && _.get(rules, `${covCode}.invalidateAllVersion`)) {
      promises.push(
        Promise.resolve({
          covCode,
          productVersion: 99999999999999999999
        })
      );
    } else if (isHandle) {
      promises.push(prodDao.getPlanByCovCode('01', covCode, 'B').then(doc => {
        if (doc && !doc.error) {
          return doc;
        } else {
          throw new Error(`Fail to get product for ${covCode}`);
        }
      }));
    }
  });
  return Promise.all(promises).then(products => {
    let productsVersion = {};
    _.forEach(products, product => {
      productsVersion[product.covCode] = _.get(product, 'productVersion', '1.0');
    });
    return productsVersion;
  });
};

const recursiveInvalidateCase = function (cid, invalidateList, result = [], index = 0) {
  if (index !== invalidateList.length) {
    let docId = invalidateList[index];
    return bundleDao.onInvalidateApplicationById(cid, docId).then(iResult => {
      if (iResult && !iResult.error) {
        result[index] = docId;
        return recursiveInvalidateCase(cid, invalidateList, result, index + 1);
      } else {
        throw Error(`Fail to invalidate case ${docId}`);
      }
    });
  } else {
    return result;
  }
};



const executeJob = function (runTime, sysParam) {
    logger.log(`Jobs :: GeneralInvalidationJob:: InflightInvalidateJob :: JOB STARTED at ${runTime}`);
    prepareChannels().then(channels => {
      return {
        channels
      };
    }).then(param => {
      return prepareProductList(sysParam).then(productsVersion => {
        return _.set(param, 'productsVersion', productsVersion);
      });
    }).then(param => {
      /**
       * Every Inflight job should create new couchbase view to limit the scope of the cases
       * Reduce the run time and impacted cases
       */
      return getAllCustomerWithMutipleBaseProductCode().then(allCustomers => {
        return _.set(param, 'allCustomers', allCustomers);
      });
    }).then(param => {
      let allCustomers = _.get(param, 'allCustomers', []);
      return getAllValidBundleId().then(bundleIds => {
        let cids = _.map(allCustomers, customer => {
          if (customer.bundleId && customer.clientId && bundleIds.indexOf(customer.bundleId) > -1){
            return customer.clientId;
          }
        });
        cids = _.compact(_.union(cids));
        return _.set(param, 'cids', cids);
      });
    }).then(param => {
      let {impactedListDocName} = sysParam;
      return createImpactedClientList(impactedListDocName, _.get(param, 'cids')).then(() => {
        return param;
      });
    }).then(param => {
      logger.log(`Jobs :: GeneralInvalidationJob:: InflightInvalidateJob :: GetAllAgents :: JOB ENDED at ${moment().toDate()}`);
      return getAllAgents().then(agents => {
        return _.set(param, 'agents', agents);
      });
    }).then(param => {
      return recursiveInvalidateCustomer(runTime, sysParam, param.channels, param.productsVersion, param.cids, param.agents);
    }).then(() => {
      // To-do
      logger.log(`Expire Case if needed ${moment().toDate()}`);
    }).then(() => {
      logger.log(`Jobs :: GeneralInvalidationJob:: InflightInvalidateJob :: JOB ENDED at ${moment().toDate()}`);
    }).catch(error => {
      logger.error(`Jobs :: InflightInvalidateJob :: ERROR caught at FINAL ${moment().toDate()}`, error);
      let errors = [];
      let errorStr = error instanceof Error ? error.toString() : _.toString(error);
      let errorLog = _.isString(errorStr) ? errorStr : 'Unknown error type: ' + typeof errorStr;
      errors.push(errorLog);
      addTrxLogs(runTime, sysParam, [], errors);
    });
};

module.exports.execute = () => {
  return new Promise((resolve, reject) => {
    dao.getDoc(generalInvalidationParameter, (invalidationParam) => {
      if (invalidationParam && !invalidationParam.error) {
        let scheduleCron = _.get(invalidationParam, 'scheduleCron');
        let fromTime = _.get(invalidationParam, 'fromTime');
        let toTime = _.get(invalidationParam, 'toTime');

        let jobDetailsDocId = _.get(invalidationParam, 'jobDetailsDocId', 'GeneralInvalidationJob');
        let impactedListDocId = _.get(invalidationParam, 'impactedListDocId', 'InvalidationImpactedList');

        const jobDetailsDocName = `${jobDetailsDocId}_${currentDate.getUTCFullYear()}_${currentDate.getUTCMonth() + 1}_${currentDate.getDate()}`;
        const impactedListDocName = `${impactedListDocId}_${currentDate.getUTCFullYear()}_${currentDate.getUTCMonth() + 1}_${currentDate.getDate()}`;
        _.set(invalidationParam, 'jobDetailsDocName', jobDetailsDocName);
        _.set(invalidationParam, 'impactedListDocName', impactedListDocName);
        _.set(invalidationParam, 'agentMsgMapping', {});

        if (invalidationParam && scheduleCron) {
          schedule.scheduleJob(scheduleCron, (runTime) => {
            checkIsJobExecutable(runTime, invalidationParam).then((run) => {
              let runDateTime = moment(runTime);
              let fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
              let toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
              if (run) {
                executeJob(runTime, invalidationParam);
              } else if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
                logger.log(`Jobs :: End:: InflightInvalidateJob :: JOB SKIPPED at ${runTime}`);
              }
            });
          });
          resolve();
        } else {
          reject(new Error(`Fail to get scheduleCron="${scheduleCron}" in ${generalInvalidationParameter}`));
        }

      } else {
        reject(new Error('Fail to get doc', generalInvalidationParameter));
      }
    });
  });
};
