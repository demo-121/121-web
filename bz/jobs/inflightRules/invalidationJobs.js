const _ = require('lodash');
const logger = global.logger || console;
const {mainRulesHandler} = require('./invalidationRules');

module.exports.inflightMsgHandling = (inflightInputData, docIds2CombinationsResult, docId) => {
  let messageIds = [];
  if (docIds2CombinationsResult && docIds2CombinationsResult[docId]) {
    let bCombinationsResult = docIds2CombinationsResult[docId];
    _.forEach(bCombinationsResult, function (bCombination, combinationId) {
      if (bCombination && messageIds.indexOf(combinationId) === -1) {
        messageIds.push(combinationId);
      }
    });
  }
  return messageIds;
};

const inflightJobFunct = (inflightInputData, jobParams) => {
  let {ruleIds, rulesParamsMapping, rulesCombinations} = jobParams;

  let allRulesResults = mainRulesHandler(inflightInputData, jobParams);
  let {docIds2RulesResult, rules2docIdsResult} = allRulesResults;

  let invalidateDocIds = [];
  let invalidateResult = {};
  let bCombinationResult = {};
  let docIds2CombinationsResult = {};
  if (docIds2RulesResult) {

    _.forEach(rulesCombinations, function (rulesCombination) {
      let {ruleIdsLookUp, lookUpFormula, combinationId} = rulesCombination;
      let bCombinationBundle = false;
      _.forEach(docIds2RulesResult, function (rulesResult, docId) {
        let lookUpFormulaTemplate = lookUpFormula.slice();
        ruleIdsLookUp.forEach((ruleId, index) => {
            let reg = new RegExp(`#${index + 1}` + '\\b','g');
            let strReplace = _.get(rulesResult,ruleId) ? `rulesResult.${ruleId}` : 'false';
            lookUpFormulaTemplate = lookUpFormulaTemplate.replace(reg, strReplace);
        });
        let bCombination = false;
        try {
          bCombination = eval(lookUpFormulaTemplate);
        } catch (e) {}
        invalidateResult[docId] = invalidateResult[docId] ? (invalidateResult[docId] || bCombination) : bCombination;
        if (bCombination && invalidateDocIds.indexOf(docId) === -1){
          invalidateDocIds.push(docId);
        }
        bCombinationBundle = bCombinationBundle || bCombination;

        if (!_.has(docIds2CombinationsResult,docId)){
          docIds2CombinationsResult[docId] = {};
        }
        docIds2CombinationsResult[docId][combinationId] = bCombination;
      });
      bCombinationResult[combinationId] = bCombinationBundle;
    });
  }
  return {
    invalidateDocIds,
    invalidateResult,
    docIds2RulesResult,
    rules2docIdsResult,
    docIds2CombinationsResult,
    bCombinationResult
  };
};

module.exports.invalidationJobHandlers = {
  inflightJobFunct: inflightJobFunct
};
