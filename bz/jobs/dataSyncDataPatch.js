var async = require('async');
const _ = require('lodash');
const schedule = require('node-schedule');
const moment = require('moment');
const logger = global.logger || console;

const dao = require('../cbDaoFactory').create();
const agentDao = require('../cbDao/agent');

const patchDataSyncParameter = 'patchDataSyncParameter';
const {getAllAgents, checkIsJobExecutable, addTrxLogs, getAllAgentRelatedDocumentIds} = require('./invalidateFunctions');

// Couchbase File Name
const currentDate  = new Date();
const jobDetailsDocKey = `DataPatchJob_DataSync_${currentDate.getUTCFullYear()}_${currentDate.getUTCMonth() + 1}_${currentDate.getDate()}`;
const auditLogMappingFileName = 'DataPatchJob_DataSync_Mapping';
let failIds = [];
let successIds = [];

const createAuditLogMapping = function(callback) {
  return new Promise(resolve => {
    dao.getDoc(auditLogMappingFileName, doc => {
      if (doc.fileMapping) {
        doc.fileMapping.push(jobDetailsDocKey);
      } else {
        doc.fileMapping = [jobDetailsDocKey];
      }

      doc.type = 'datapatch';
      doc = _.omit(doc, ['error', 'reason']);
      dao.updDoc(auditLogMappingFileName, doc, (result) => {
        callback(null, true);
      });
    });
  });
};

const asyncUpdateBundleDocArray = function(docIds, index, agentCode, doc, callback) {
  if (index < docIds.length) {
    asyncUpdateBundleDoc(docIds, index, agentCode, doc, callback);
  } else {
    callback(null, doc);
  }
};

const asyncUpdateDocArray = function(docIds, index, callback) {
  if (index < docIds.length) {
    asyncUpdateDoc(docIds, index, callback);
  } else {
    callback(null, true);
  }
};

const asyncUpdateDoc = function(docIds, index, callback) {
  let docId = docIds[index];
  async.waterfall([
    (cb) => {
      dao.getDoc(_.trim(docId), doc => {
        if (doc && !doc.error) {
          cb(null, doc);
        } else {
          cb('Failed to Get Doc');
        }
      });
    }, (doc, cb) => {
      if (doc && doc.type === 'bundle') {
        // Check type === bundle update -FE -PDA -NA doc without agentId and agentCode
        const bundleFE = `${docId}-FE`;
        const bundlePDA = `${docId}-PDA`;
        const bundleNA = `${docId}-NA`;
        const bundleUpdateIds = [];
        const agentCode = doc.agentId || doc.agentCode;
        if (docIds.indexOf(bundleFE === -1)) {
          bundleUpdateIds.push(bundleFE);
        }
        if (docIds.indexOf(bundlePDA === -1)) {
          bundleUpdateIds.push(bundlePDA);
        }
        if (docIds.indexOf(bundleNA === -1)) {
          bundleUpdateIds.push(bundleNA);
        }

        if (bundleUpdateIds.length > 0) {
          asyncUpdateBundleDocArray(bundleUpdateIds, 0, agentCode, doc, cb);
        } else {
          cb(null, doc);
        }
      } else {
        cb(null, doc);
      }
    }, (doc, cb) => {
      dao.updDoc(docId, doc, (result) => {
        if (result && result.ok) {
          cb(null, true);
        } else {
          cb('Failed to Update Doc');
        }
      });
    }
  ], (err, result) => {
    if (err) {
      logger.error(`Jobs :: DataPatchJob_DataSync :: Failed to update Doc ::: ${docId}`);
      failIds.push(docId);
      asyncUpdateDocArray(docIds, index + 1, callback);
    } else {
      logger.log(`Jobs :: DataPatchJob_DataSync :: Success to update Doc ::: ${docId}`);
      successIds.push(docId);
      asyncUpdateDocArray(docIds, index + 1, callback);
    }
  });
};

const asyncUpdateBundleDoc = function(docIds, index, agentCode, masterDoc, callback) {
  let docId = docIds[index];
  async.waterfall([
    (cb) => {
      dao.getDoc(_.trim(docId), doc => {
        if (doc && !doc.error) {
          cb(null, doc);
        } else {
          cb('Failed to Get Bundle Doc');
        }
      });
    }, (doc, cb) => {
      doc.agentCode = agentCode;
      dao.updDoc(docId, doc, (result) => {
        if (result && result.ok) {
          cb(null, true);
        } else {
          cb('Failed to Update Bundle Doc');
        }
      });
    }
  ], (err, result) => {
    if (err) {
      logger.error(`Jobs :: DataPatchJob_DataSync :: Failed to update Doc Bundle ::: ${docId}`);
      failIds.push(docId);
      asyncUpdateBundleDocArray(docIds, index + 1, agentCode, masterDoc, callback);
    } else {
      logger.log(`Jobs :: DataPatchJob_DataSync :: Success to update Doc Bundle ::: ${docId}`);
      successIds.push(docId);
      asyncUpdateBundleDocArray(docIds, index + 1, agentCode, masterDoc, callback);
    }
  });
};

const doDataPatch = function(dataPatchFile = {}, runTime, index = 0, sysParam) {
  let agentCodes = dataPatchFile.agentCodes;
  let patchedVersion = _.get(sysParam, 'patchForVersion');
  if (index < agentCodes.length) {
    failIds = [];
    successIds = [];
    let agentCode = agentCodes[index];
    let docIdsForLogging = [];
    let agentProfile = {};
    logger.log(`Jobs :: DataPatchJob_DataSync :: Start ${index + 1} / ${agentCodes.length}}`);
    return new Promise((resolve, reject) => {
      async.waterfall([
        (callback) => {
          // Create Mapping Audit Logs
          if (index === 0) {
            createAuditLogMapping(callback);
          } else {
            callback(null, true);
          }
        }, (success, callback) => {
          agentDao.getAgentsByAgentCode('01', [agentCode]).then(mapping => {
            agentProfile = mapping[agentCode];
            dao.getDoc(`U_${_.get(agentProfile, 'profileId')}`, doc => {
              let dataSyncPatchedVersion = _.keys(_.get(doc, 'patchDataSync'));
              if (doc && !doc.error && dataSyncPatchedVersion.indexOf(patchedVersion) === -1) {
                callback(null, true);
              } else {
                callback('Cannot find agent profile by view :: getAgentsByAgentCode / Patched Before');
              }
            });
          });
        }, (success, callback) => {
          getAllAgentRelatedDocumentIds('01', [agentCode]).then(result => {
            callback(null, result);
          });
        }, (docIds, callback) => {
          docIdsForLogging = docIds;
          logger.log(`Jobs :: DataPatchJob_DataSync :: Get Doc ${docIds}}`);
          // Update agent's related documents
          asyncUpdateDocArray(docIds, 0, callback);
        }
      ], (err, result) => {
          if (err) {
            logger.error(`Jobs :: DataPatchJob_DataSync :: Error in dataPatch agentCode: ${agentCode}`, err);
            let errors = [err];
            errors.push(agentCode);
            addTrxLogs(runTime, [], errors, jobDetailsDocKey).then(() => {            
              resolve(false);
            }).catch(error => {
              logger.error(`Jobs :: DataPatchJob_DataSync :: Error in dataPatch:: addTrxLogs agentCode:: ${agentCode} :: ${error}`);
              resolve(true);
            });
          } else {
            logger.info('Jobs :: DataPatchJob_DataSync :: Logs in dataPatch agentCode: ', agentCode);
            let agentProfileId = _.get(agentProfile, 'profileId');
            // Update Transaction files
            addTrxLogs(runTime, [{
              agentCode,
              agentProfileId,
              targetIds: docIdsForLogging
            }
            ],[], jobDetailsDocKey).then(() => {
              // Update Agent Profile
              dao.getDoc(`U_${agentProfileId}`, aProfile => {
                let agentProfileDoc = aProfile;
                if (!agentProfileDoc.patchDataSync) {
                  agentProfileDoc.patchDataSync = {};
                }
                agentProfileDoc.patchDataSync[sysParam.patchForVersion] = true;
                dao.updDoc(`U_${agentProfileId}`, agentProfileDoc, updResult => {
                  logger.log(`Jobs :: DataPatchJob_DataSync :: Data Patch update agent profile record :: ${agentCode} ${updResult.ok}`);
                  resolve(true);
                });
              });
            }).catch(error => {
              logger.error(`Jobs :: DataPatchJob_DataSync :: Error in dataPatch:: addTrxLogs agentCode:: ${agentCode} :: ${error}`);
              resolve(true);
            });
          }
      });
    }).then((success) => {
      logger.log(`Jobs :: DataPatchJob_DataSync :: Data Patch Status for agent :: ${agentCode} ${success}`);
      logger.log(`Jobs :: DataPatchJob_DataSync :: Completed ${index + 1} / ${agentCodes.length}`);
      return doDataPatch(dataPatchFile, runTime, index + 1, sysParam);
    }).catch(error => {
      logger.error(`Jobs :: DataPatchJob_DataSync :: ERROR caught at Promise doDataPatch ${moment().toDate()}`, error);
      logger.log(`Jobs :: DataPatchJob_DataSync :: Completed ${index + 1} / ${agentCodes.length}`);
      return doDataPatch(dataPatchFile, runTime, index + 1, sysParam);
    });
  } else {
    logger.log('Jobs :: DataPatchJob_DataSync :: COMPLETED DataPatchJob_DataSyncg');
    return Promise.resolve(dataPatchFile);
  }
};

const executeJob = function (runTime, sysParam) {
  logger.log(`Jobs :: DataPatchJob_DataSync :: JOB STARTED at ${runTime}`);
  const datapatchId = _.get(sysParam, 'dataPatchDocId');
  const allowToRunAll = _.get(sysParam, 'allowToRunAll');

  return new Promise((resolve, reject) => {
    dao.getDoc(_.trim(datapatchId), (datapatchFile) => {
      resolve(datapatchFile);
    });
  }).then(dataPatchFile => {
    if (dataPatchFile && !dataPatchFile.error) {
      if (dataPatchFile.agentCodes) {
        return dataPatchFile;
      } else {
         throw Error('Jobs :: DataPatchJob_DataSync :: patchDataSyncagentCodes :: INCORRECT FORMAT');
      }
    } else if (!datapatchId && allowToRunAll) {
      /**
       * No datapatchId && allowToRunAll === true
       * Get all Agents by view and patch all data that has not patch for the specific couchbase version
       */
      return getAllAgents().then(agents => {
        dataPatchFile.agentCodes = [];
        _.each(agents, agent => {
          if (agent && agent.agentCode) {
            dataPatchFile.agentCodes.push(agent.agentCode);
          }
        });
      });
    } else {
      throw Error(`Jobs :: DataPatchJob_DataSync :: Cannot find data patch doc / Not allow to run data patch for all agents :: ${JSON.stringify(sysParam)}`);
    }
  }).then(dataPatchFile => {
    if (dataPatchFile && dataPatchFile.agentCodes) {
      return doDataPatch(dataPatchFile, runTime, 0, sysParam);
    } else {
      throw Error('Jobs :: DataPatchJob_DataSync :: No agent ids found');
    }
  }).then(() => {
    logger.log(`Jobs :: DataPatchJob_DataSync :: JOB ENDED at ${moment().toDate()}`);
  }).catch(error => {
    // Add Audit Logs when error caught
    logger.error(`Jobs :: DataPatchJob_DataSync :: ERROR caught at FINAL ${moment().toDate()}`, error);

    let errors = [];
    let errorStr = error instanceof Error ? error.toString() : _.toString(error);
    let errorLog = _.isString(errorStr) ? errorStr : 'Unknown error type: ' + typeof errorStr;
    errors.push(errorLog);
    addTrxLogs(runTime, [], errors, jobDetailsDocKey);
  });
};

module.exports.execute = () => {
  return new Promise((resolve, reject) => {
    dao.getDoc(_.trim(patchDataSyncParameter), (param) => {
      logger.log(param);
      if (param && !param.error) {
        let scheduleCron = _.get(param, 'scheduleCron');
        let fromTime = _.get(param, 'fromTime');
        let toTime = _.get(param, 'toTime');

        if (scheduleCron) {
          schedule.scheduleJob(scheduleCron, (runTime) => {
            checkIsJobExecutable(runTime, param).then((run) => {
              let runDateTime = moment(runTime);
              let fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
              let toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
              if (run) {
                executeJob(runTime, param);
              } else if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
                logger.log(`Jobs :: DataPatchJob_DataSync :: JOB SKIPPED at ${runTime}`);
              }
            });
          });
          resolve();
        } else {
          reject(new Error(`Jobs :: DataPatchJob_DataSync :: Fail to get scheduleCron="${scheduleCron}" in ${patchDataSyncParameter}`));
        }
      } else {
        reject(new Error('Jobs :: DataPatchJob_DataSync :: Fail to get doc ', patchDataSyncParameter));
      }
    });
  });
};
