const _ = require('lodash');

const logger = global.logger || console;
const globalInit = require('./globalInit');
const invalidateByFunds = require('./invalidateByFunds');
const invalidateCiKids = require('./invalidateCiKids');
const fairBiInvalidate = require('./fairBiInvalidate');
const generalEndCampaignInvalidate = require('./generalEndCampaignInvalidate');
const generalEndFundInvalidate = require('./generalEndFundInvalidate');
const shieldInflightDataPatch = require('./shieldInflightDataPatch');
const dataSyncDataPatch = require('./dataSyncDataPatch');
const generalInvalidation = require('./generalInvalidation');

const jobs = [
  globalInit,
  dataSyncDataPatch,
  invalidateByFunds,
  invalidateCiKids,
  fairBiInvalidate,
  generalEndCampaignInvalidate,
  generalEndFundInvalidate,
  shieldInflightDataPatch,
  generalInvalidation
];

module.exports.execInitServerJobs = () => {
  logger.log('Jobs :: execInitServerJobs :: executing init server jobs');
  let promises = [];
  _.each(jobs, (job) => {
    if (_.isFunction(job && job.execute)) {
      promises.push(job.execute());
    }
  });
  return Promise.all(promises).then(() => {
    logger.log('Jobs :: execInitServerJobs :: init server jobs completed');
  }).catch((err) => {
    logger.error('Jobs :: execInitServerJobs :: uncaught error:\n', err);
  });
};
