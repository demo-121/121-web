var { callApi } = require('./utils/RemoteUtils.js');
var dao = require('./cbDaoFactory').create();
var applicationDao = require('./cbDao/application');
var approvalDao = require('./cbDao/approval');
var companyDao = require('./cbDao/company');
var CommonFunctions = require('./CommonFunctions');
var nUtil = require('./utils/NotificationsUtils');
var SuppDocsUtils = require('./utils/SuppDocsUtils');
var EmailUtils = require('./utils/EmailUtils');
var productUtils = require('../common/ProductUtils');

var _ = require('lodash');
var _get = require('lodash/fp/get');
var _getOr = require('lodash/fp/getOr');

const logger = global.logger;

module.exports.sendEmail = (data, session, cb) => {
  callApi('/email/sendEmail',
    data.email,
    cb);
};

const getSysDocAttachment = (id, attId, title) => {
  return new Promise((resolve) => {
    applicationDao.getAppAttachment(id, attId, (pdf) => {
      resolve({fileName: title, data: _get('data', pdf)});
    });
  });
};

const getProtectedPdf = (pwd, pdf, fileName) => {
  return new Promise((resolve) => {
    CommonFunctions.protectBase64Pdf(pdf, pwd, (data) => {
      resolve({
        fileName,
        data
      });
    });
  });
};

module.exports.getEmailObject4AllDoc = (data, session, cb) => {
  const {appId} = data;
  const {email, name} = session.agent;
  companyDao.getCompanyInfo((companyInfo) => {
    const emailObj = nUtil.getEmailObject(nUtil.TYPE.SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS, {
      sender: companyInfo.fromEmail,
      recipients: email,
      agentName: name
    });
    getAttachmentList(appId).then((suppDocs) => {
      cb({
        success: true,
        email: emailObj,
        availableAttachments: _.map(suppDocs, (doc, docKey) => {
          return { id: docKey, title: doc.title };
        })
      });
    }).catch((error)=>{
      logger.error("Error in getEmailObject4AllDoc->getAttachmentList: ", error);
    });
  });
};

const getSuppDocPdf = (appId, items, title) => {
  let promises = _.map(items, (item) => getSysDocAttachment(appId, item.id, item.title));
  return Promise.all(promises).then((attachments) => {
    let imgs = _.map(attachments, att => att.data);
    let pdfData = CommonFunctions.convertBase64Imgs2Pdf(imgs);
    return { fileName: title, data: pdfData };
  }).catch((error)=>{
      logger.error("Error in getSuppDocPdf->Promise.all: ", error);
  });
};

const getAttachmentList = (appId, approvalId) => {
  return approvalDao.getApprovalCaseByAppId(appId).then((approval) => {
    return new Promise((resolve) => {
      dao.getDoc('suppDocsNames', (suppDocsNames) => {
        let docsNameList = suppDocsNames.values;
        applicationDao.getApplication(appId, (app) => {
          let result = {};
          let isFACase = _.get(approval, 'isFACase', false);
          let isShield = _.get(approval, 'isShield', false);
          Object.assign(result, SuppDocsUtils.getAppSysDocs(app, docsNameList, isFACase, isShield));
          Object.assign(result, SuppDocsUtils.getAppMandDocs(app, docsNameList, isShield));
          Object.assign(result, SuppDocsUtils.getAppOptionalDocs(app, docsNameList, isShield));
          Object.assign(result, SuppDocsUtils.getAppOtherDocs(app, isShield));
          Object.assign(result, SuppDocsUtils.getApprovalSysDocs(app, approval, docsNameList));
          Object.assign(result, SuppDocsUtils.getApprovalCoeDocs(app, approval, docsNameList));

          resolve(result);
        });
      });
    });
  }).catch((error)=>{
      logger.error("Error in getAttachmentList ->: ", error);
  });
};

module.exports.sendAllDoc = (data, session, cb) => {
  const {appId, suppDocIds} = data;
  const {email, name, agentCode} = session.agent;
  companyDao.getCompanyInfo((companyInfo) => {
    applicationDao.getApplication(appId, (application) => {
      const quoId = _get('quotation.id', application);
      const fnaId = _get('bundleId', application);
      const policyNumber = _get('policyNumber', application);
      const isShield = _get('quotation.quotType', application) === 'SHIELD';
      const emailObj = nUtil.getEmailObject(nUtil.TYPE.SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS, {
        sender: companyInfo.fromEmail,
        recipients: email,
        agentName: name
      });
      getAttachmentList(appId).then((suppDocs) => {
        let promises = [];
        _.each(suppDocs, (suppDoc, docKey) => {
          if (!_.find(suppDocIds, v => v === docKey)) {
            return;
          }
          if (docKey.indexOf('appPdf') > -1 && isShield && suppDoc) {
            const shieldAppPdfName = _transformInvalidFileName(suppDoc.title);
            promises.push(getSysDocAttachment(appId, docKey, `${shieldAppPdfName}.pdf`));
          } else if (docKey === 'appPdf') {
            promises.push(getSysDocAttachment(appId, docKey, 'eapp_form.pdf'));
          } else if (docKey === 'fnaReport') {
            promises.push(getSysDocAttachment(fnaId, docKey, 'fna_report.pdf'));
          } else if (docKey === 'proposal'  && isShield) {
            promises.push(getSysDocAttachment(quoId, docKey, 'AXA Shield Product Summary.pdf'));
          } else if (docKey === 'proposal') {
            let pdfName = _getOr('policy_illustration.pdf', 'fileName', productUtils.getPILabelByQuotType(_get('quotation.quotType', application)));
            promises.push(getSysDocAttachment(quoId, docKey, pdfName));
          } else if (docKey === 'eCpdPdf' && _.get(application, '_attachments.eCpdPdf.length')) {
            promises.push(getSysDocAttachment(appId, docKey, 'e-CPD.pdf'));

          } else if (docKey === 'eapproval_coe') {
            let coeDocs = suppDocs[docKey];
            if (coeDocs){
              let imgItems = [];
              let countCoe = 0;
              let coeDocName = 'Certificate-of-Endorsement';
              _.each(coeDocs.items, (item, index) => {
                if (item.id && item.fileType && item.title) {
                  if (item.fileType.indexOf('pdf') > -1) {
                    let coePdfFileName = countCoe > 0 ? coeDocName + `(${countCoe})` : coeDocName;
                    promises.push(getSysDocAttachment(appId, item.id, `${coePdfFileName}.pdf`));
                    countCoe += 1;
                  } else if (item.fileType.indexOf('image') > -1) {
                    imgItems.push(item);
                  }
                }
              });
              if (imgItems.length) {
                let coePdfFileName = countCoe > 0 ? coeDocName + `(${countCoe})` : coeDocName;
                promises.push(getSuppDocPdf(appId, imgItems, `${coePdfFileName}.pdf`));
                countCoe += 1;
              }
            }
          }  else {
            let filename = (suppDocs[docKey] && suppDocs[docKey].filename) || docKey;
            let imgItems = [];
            _.each(suppDoc.items, (item, index) => {
              if (item.fileSize && item.fileType) {
                if (item.fileType.indexOf('pdf') > -1) {
                  promises.push(getSysDocAttachment(appId, item.id, filename + '_' + (index + 1) + '.pdf'));
                } else if (item.fileType.indexOf('image') > -1) {
                  imgItems.push(item);
                }
              }
            });
            if (imgItems.length) {
              promises.push(getSuppDocPdf(appId, imgItems, filename + '.pdf'));
            }
          }
        });
        return Promise.all(promises);
      }).then((attachments) => {
        return Promise.all(_.map(attachments, (attachment) => {
          return getProtectedPdf(agentCode.substr(agentCode.length - 6, 6), attachment.data, attachment.fileName);
        }));
      }).then((attachments) => {
        EmailUtils.sendEmail(Object.assign(emailObj, {
          attachments: attachments.concat(emailObj.attachments)
        }), (result) => {
          cb({ success: true });
        });
      }).catch((error)=>{
        logger.error("Error in sendAllDoc->getAttachmentList: ", error);
      });
    });
  });
};

const _transformInvalidFileName = (fileName) => {
  return fileName.replace(/[|:/\?*"<>]/g, '');
}

module.exports.getAttachmentList = getAttachmentList;
module.exports.transformInvalidFileName = _transformInvalidFileName;
