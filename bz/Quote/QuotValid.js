const _ = require('lodash');
const utils = require('./utils');
const math = require('./math2.js');


var QuotValid = function (context, quotDriver) {
    // let runFunc = utils.runFunc,
    let getCurrency = utils.getCurrency,
        debug = utils.debug;
    this.context = context;
    this.quotDriver = quotDriver;

    let ErrCode = {
        missingSA:                  'js.err.sa_missing',
        wrongClassType:             'js.err.wrong_class_type',
        wrongWPClassFix:            'js.err.wrong_wp_class_fix',
        wrongWPClassBP:             'js.err.wrong_wp_class_basic',
        invalidSumInsured:          'js.err.invalid_sum_insured',
        invalidPremium:             'js.err.invalid_premium',
        invalidSAMult:              'js.err.invalid_sa_mult',
        invalidPremMult:            'js.err.invalid_prem_mult',
        invalidSADecimal:           'js.err.invalid_sa_decimal',
        invalidPremDecimal:         'js.err.invalid_prem_decimal',
        invalidBenelimMin:          'js.err.invalid_benelim_min',
        invalidBenelimMax:          'js.err.invalid_benelim_max',
        invalidBenelimMinMax:       'js.err.invalid_benelim_min_max',
        invalidPremlimMin:          'js.err.invalid_premlim_min',
        invalidPremlimMax:          'js.err.invalid_premlim_max',
        invalidPremlimMinMax:       'js.err.invalid_premlim_min_max',
        invalidPremlimMinMulti:     'js.err.invalid_premlim_min_multi',
        invalidPremlimMaxMulti:     'js.err.invalid_premlim_max_multi',
        invalidPremlimMinMaxMulti:  'js.err.invalid_premlim_min_max_multi',
        invalidCoexistShould:       'js.err.invalid_coexist_should',
        invalidCoexistShouldNot:    'js.err.invalid_coexist_shouldnot',
        invalidCoexistRequire:      'js.err.invalid_coexist_require',
        invalidDA:                  'js.err.invalid_da',
        overBasicSARange:           'js.err.basicSA_over_range',
        backDatebeyondToday:        'js.err.backDatebeyondToday',
        backDateBeforeEffDate:      'js.err.backDateBeforeEffDate',
        backDateOlderThanSixMonths: 'js.err.backDateOlderThanSixMonths'
    }

    // validate benefit limit
    this.validateSALimit = (quotation, planInfo, planDetail) => {
        if (planDetail && planInfo && planDetail.inputConfig) {
            if (planDetail.inputConfig.benlim && _.isNumber(planInfo.sumInsured))  {
              let {
                min,
                max
              } = planDetail.inputConfig.benlim;
              let {
                covCode,
                sumInsured
              } = planInfo;

              if ((min && min > sumInsured) || (max && max < sumInsured)) {
                let err = {
                  covCode: covCode,
                  msgPara: [],
                  code: ''
                };
                let errCode = '';
                if (min && max) {
                  errCode = ErrCode.invalidBenelimMinMax;
                  err.msgPara.push(planInfo.covName);
                  err.msgPara.push(getCurrency(min, '$', 2));
                  err.msgPara.push(getCurrency(max, '$', 2));
                } else if (min) {
                  errCode = ErrCode.invalidBenelimMin;
                  err.msgPara.push(getCurrency(min, '$', 2));
                } else if (max) {
                  errCode = ErrCode.invalidBenelimMax;
                  err.msgPara.push(getCurrency(max, '$', 2));
                }
                err.msgPara.push(planDetail.inputConfig.premInput);

                err.code = errCode;
                err.msgPara.push(sumInsured);
                this.context.addError(err);
              }
            }
        } else {
            this.context.addError({
                code: "Internal Error (validateSALimit)"
            });
        }
    };

    // validate premium limit
    this.validatePreimumLimit = (quotation, planInfo, planDetail) => {
        if (planDetail && planInfo && planDetail.inputConfig) {
            if (planDetail.inputConfig.premlim && _.isNumber(planInfo.premium)) {
                let {
                    min,
                    max
                } = planDetail.inputConfig.premlim;
                let {
                    covCode,
                    premium
                } = planInfo;

                if ((min && min > premium) || (max && max < premium)) {
                    let err = {
                        covCode: covCode,
                        msgPara: [],
                        code: ''
                    };
                    let errCode = '';
                    if (min && max) {
                        errCode = ErrCode.invalidPremlimMinMax;
                        err.msgPara.push(planInfo.covName);
                        err.msgPara.push( getCurrency(min, '$', 2));
                        err.msgPara.push( getCurrency(max, '$', 2));
                    } else if (min) {
                        errCode = ErrCode.invalidPremlimMin;
                        err.msgPara.push(getCurrency ? getCurrency(min, '$', 2) : min);
                    } else if (max) {
                        errCode = ErrCode.invalidPremlimMax;
                        err.msgPara.push(getCurrency ? getCurrency(max, '$', 2) : max);
                    }
                    err.code = errCode;
                    err.msgPara.push(premium);
                    this.context.addError(err);
                }
            }
        } else {
            this.context.addError({
                code: "Internal Error (validatePreimumLimit)"
            });
        }
    };

    let getDecimalPlace =  (num) => {
        let strs = num.toString().split('.');
        return strs.length > 1 ? strs[1].length : 0;
    };

    // validate SA Multiple
    this.validateSAMultiple = (quotation, planInfo, planDetail) => {
        let saInput = _.find(planDetail.saInput, (config) => config.ccy === quotation.ccy);
        if (saInput && _.isNumber(planInfo.sumInsured)) {
        if (_.isNumber(saInput.factor) && saInput.factor !== 0) {
            if (math.mod(planInfo.sumInsured, saInput.factor) !== 0) {
            this.context.addError({
                covCode: planInfo.covCode,
                code: ErrCode.invalidSAMult,
                msgPara: [planInfo.covName, getCurrency(saInput.factor, '$', 0), planInfo.sumInsured]
            });
            }
        } else if (_.isNumber(saInput.decimal) && saInput.decimal) {
            if (getDecimalPlace(planInfo.sumInsured) > saInput.decimal) {
            this.context.addError({
                covCode: planInfo.covCode,
                code: ErrCode.invalidSADecimal,
                msgPara: [planInfo.covName, saInput.decimal, planInfo.sumInsured]
            });
            }
        }
        }
    };

    this.validatePremiumMultiple =  (quotation, planInfo, planDetail) => {
        let premInput = _.find(planDetail.premInput, (config) => config.ccy === quotation.ccy);
        if (premInput && _.isNumber(planInfo.premium)) {
            if (_.isNumber(premInput.factor) && premInput.factor !== 0) {
                if (math.mod(planInfo.premium, premInput.factor) !== 0) {
                    this.context.addError({
                        covCode: planInfo.covCode,
                        code: ErrCode.invalidPremMult,
                        msgPara: [planInfo.covName, getCurrency(premInput.factor, '$', 0), planInfo.premium]
                    });
                }
            } else if (_.isNumber(premInput.decimal) && premInput.decimal) {
                if (getDecimalPlace(planInfo.premium) > premInput.decimal) {
                    this.context.addError({
                        covCode: planInfo.covCode,
                        code: ErrCode.invalidPremDecimal,
                        msgPara: [planInfo.covName, premInput.decimal, planInfo.premium]
                    });
                }
            }
        }
    };

    // validate plan coexist rule
    this.validateCoexist = (quotation, planInfos, planDetails) => {
        let basicPlanDetail = planDetails[quotation.baseProductCode];
        // let planInfos = quotation.plans;

        if (basicPlanDetail.coexist) {
            for (let c in basicPlanDetail.coexist) {
                let coex = basicPlanDetail.coexist[c];
                if ((!coex.country || coex.country == '*' || coex.country == quotation.iResidence) &&
                    (!coex.dealerGroup || coex.dealerGroup == '*' || coex.dealerGroup == quotation.agent.dealerGroup) &&
                    (!coex.gender || coex.gender == '*' || coex.gender == quotation.iGender) &&
                    (!coex.smoke || coex.smoke == '*' || coex.smoke == quotation.iSmoke) &&
                    (!coex.ageFr || coex.ageFr <= quotation.iAge) &&
                    (!coex.ageTo || coex.ageTo >= quotation.iAge)) {

                    for (let i = 1; i < planInfos.length; i++) {
                        let gA = coex.groupA.indexOf(planInfos[i].covCode);
                        for (let j = 1; j < planInfos.length; j++) {
                            if (i == j) continue;
                            let gB = coex.groupB.indexOf(planInfos[j].covCode);

                            if (gA >= 0 && gB >= 0 && coex.shouldCoExist === 'N') {
                                this.context.addError({
                                    code: ErrCode.invalidCoexistShouldNot,
                                    msgPara: [planInfos[i].covName, planInfos[j].covName]
                                });
                            }

                            if (((gA >= 0 && gB < 0) || (gB >= 0 && gA < 0)) && coex.shouldCoExist === 'Y') {
                                this.context.addError({
                                    code: ErrCode.invalidCoexistShould,
                                    msgPara: [planInfos[i].covName, planInfos[j].covName]
                                });
                            }
                        }
                    }

                    if (coex.shouldCoExist === 'REQUIRE') {
                      _.each(planInfos, (plan) => {
                        if (coex.groupB.indexOf(plan.covCode) > -1) {
                          if (!_.find(planInfos, p => coex.groupA.indexOf(p.covCode) > -1)) {
                            this.context.addError({
                              code: ErrCode.invalidCoexistRequire,
                              msgPara: [plan.covName]
                            });
                          }
                        }
                      });
                      }
                }
            }
        }
    };


    // validateDA = function(quotation, planInfos, planDetails, validate, err) {
    //     if (!err) err = {};
    //     if (!err.msgPara) err.msgPara = [];
    //     // validate Death Aggregation
    //     if (validate.deathAggregate) {
    //         for (let k = 0; k < validate.deathAggregate.length; k++) {
    //             let deathAggregate = validate.deathAggregate[k];
    //             if (deathAggregate.ccy == quotation.ccy && deathAggregate.ageFr <= quotation.iAge && deathAggregate.ageTo >= quotation.iAge) {
    //                 let daLim = deathAggregate.maxSumIns;
    //                 let daSum = 0;
    //                 for (let j = 0; j < planInfos.length; j++) {
    //                     let planInfo = planInfos[j];
    //                     let planDetail = planDetails[planInfo.docId];
    //                     if (planDetail.deathAggregateSeq) {
    //                         let para = {
    //                             sumInsured: planInfo.sumInsured
    //                         };
    //                         let riskSA = runFunc(planDetail.deathAggregateFunc, planDetail, para);
    //                         daSum = daSum + riskSA;
    //                     }
    //                 }
    //                 if (daSum > daLim) {
    //                     err.isInvalidDA = true;
    //                 }
    //             }
    //         }
    //     }
    // };

    this.validateMandatoryFields = (quotation, planInfo, planDetail) => {
        if (planDetail.inputConfig) {
            let config = planDetail.inputConfig;
            if (config.canEditPolicyTerm) {
                if (planInfo.policyTerm == null) {
                    this.context.addError({
                        type: 'mandatory',
                        key: planInfo.covCode,
                        id: 'policyTerm'
                    });
                }
            }
            if (config.canEditPremTerm) {
                if (planInfo.premTerm == null) {
                    this.context.addError({
                        type: 'mandatory',
                        key: planInfo.covCode,
                        id: 'premTerm'
                    });
                }
            }
            if (config.canEditClassType) {
                if (planInfo.covClass == null) {
                    this.context.addError({
                        type: 'mandatory',
                        key: planInfo.covCode,
                        id: 'covClass'
                    });
                }
            }

            if (config.canEditSumAssured) {
                if (!_.isNumber(planInfo.sumInsured)) {
                    this.context.addError({
                        type: 'mandatory',
                        key: planInfo.covCode,
                        id: 'sumInsured'
                    });
                }
            }
            if (config.canEditPremium) {
                if (!_.isNumber(planInfo.premium)) {
                    this.context.addError({
                        type: 'mandatory',
                        key: planInfo.covCode,
                        id: 'premium'
                    });
                }
            }
        }
    }

    this.validatePolicyOptions = function (quotation, bpDetail) {
        if (bpDetail && bpDetail.inputConfig) {
        _.each(bpDetail.inputConfig.policyOptions, (opt) => {
            if (opt.errorMsg) {
                this.context.addError({
                    type: 'policyOption',
                    key: opt.id,
                    msg: opt.errorMsg
                });
            }
            if (opt.mandatory === 'Y' && !quotation.policyOptions[opt.id]) {
                this.context.addError({
                    type: 'mandatory',
                    key: opt.id
                });
            }
        });
        }
    };

    this.validateFundAlloc = function (quotation, bpDetail) {
      if (bpDetail && bpDetail.fundInd === 'Y') {
        let hasFundError = !quotation.fund;
        if (hasFundError || _.reduce(quotation.fund.funds, (sum, fund) => sum + (fund.alloc || 0), 0) !== 100) {
          hasFundError = true;
        }
        if (hasFundError || bpDetail.inputConfig.topUpSelect && _.reduce(quotation.fund.funds, (sum, fund) => sum + (fund.topUpAlloc || 0), 0) !== 100) {
          hasFundError = true;
        }
        if (hasFundError) {
          this.context.addError({
            type: 'mandatory',
            key: 'fund'
          });
        }
      }
    };

    this.validateGlobal = function(quotation, planInfos, planDetails) {
        let bpDetail  = planDetails[quotation.baseProductCode];
        // validateDA(quotation, planInfos, planDetails, validate, err);
        this.validateCoexist(quotation, planInfos, planDetails);
        this.validatePolicyOptions(quotation, bpDetail);
        this.validateFundAlloc(quotation, bpDetail);
    };

    this.validatePlanAfterCalc = (quotation, planInfo, planDetail) => {
        this.validateSALimit(quotation, planInfo, planDetail);
        this.validatePreimumLimit(quotation, planInfo, planDetail);
        this.validateSAMultiple(quotation, planInfo, planDetail);
        this.validatePremiumMultiple(quotation, planInfo, planDetail);
    };

    this.validatePlanBeforeCalc = (quotation, planInfo, planDetails) => {
        let basicPlanInfo = quotation.plans[0]
        let planDetail = planDetails[planInfo.covCode];

        this.validateMandatoryFields(quotation, planInfo, planDetail);

        // validateSAMultiple(quotation, planInfo, planDetail);
        // validateSALimit(quotation, planInfo, planDetail);
        // validatePreimumLimit(quotation, planInfo, planDetail);

        let basicPlanDetails = planDetails[basicPlanInfo.covCode];
        let riderList = basicPlanDetails.inputConfig.riderList;
        // let planDetail = planDetails[planInfo.covCode];

        // for rider,
        if (planDetail.planType == "R") {
            // check for attachable rider list rule on basic plan
            for (let r in riderList) {
                let rider = riderList[r];
                if (rider.covCode == planInfo.covCode) {
                    // check if basic SA over the limited range
                    if ((_.isNumber(rider.basicSAMin) && rider.basicSAMin > basicPlanInfo.sumInsured) ||
                        (_.isNumber(rider.basicSAMax) && rider.basicSAMax < basicPlanInfo.sumInsured)) {
                        this.context.addError({
                            covCode: planInfo.covCode,
                            code: ErrCode.overBasicSARange,
                            msgPara: [planDetail.covName, rider.basicSAMin, rider.basicSAMax]
                        })
                    }

                    // check class rule for follow Basic plan
                    if (rider.classRule && rider.classRule == "B" && basicPlanInfo.classType) {
                        if (planInfo.classType != basicPlanInfo.classType) {
                            this.context.addError({
                                covCode: planInfo.covCode,
                                code: ErrCode.wrongClassType,
                                msgPara: [planDetail.covName, basicPlanInfo.covName]
                            })
                        }
                    }

                    // check class rule for fix type
                    if (rider.classRule && rider.classRule == "F" && rider.classFix) {
                        if (planInfo.classType != rider.classFix) {
                            this.context.addError({
                                covCode: planInfo.covCode,
                                code: ErrCode.wrongClassType,
                                msgPara: [planDetail.covName, rider.classFix]
                            })
                        }
                    }

                    break;
                }
            }
        }
    };
};

module.exports = QuotValid;
