const _ = require('lodash');
const QuotDriver = require('./QuotDriver');

const {formatDate, parseDate} = require('../../common/DateUtils');
const {getAgeByMethod, isCrossBorder} = require('../../common/ProductUtils');
module.exports = function () {

  this.quotDriver = new QuotDriver();

  this.getAvailableApplicants = (quotation, planDetails, profiles, fnaInfo) => {
    if (quotation && planDetails) {
      const bpDetail = planDetails[quotation.baseProductCode];
      if (bpDetail.formulas && bpDetail.formulas.getAvailableApplicants) {
        return this.quotDriver.runFunc(bpDetail.formulas.getAvailableApplicants, quotation, planDetails, profiles, fnaInfo);
      }
    }
    return profiles;
  };

  this.getEligibleClients = (quotation, planDetails, profiles, fnaInfo) => {
    if (quotation && planDetails) {
      const bpDetail = planDetails[quotation.baseProductCode];
      if (bpDetail.formulas && bpDetail.formulas.getEligibleClients) {
        return this.quotDriver.runFunc(bpDetail.formulas.getEligibleClients, quotation, planDetails, profiles, fnaInfo);
      }
    }
    return profiles;
  };

  this.getAvailableClasses = (quotation, planDetails, profiles) => {
    if (quotation && planDetails) {
      const bpDetail = planDetails[quotation.baseProductCode];
      if (bpDetail.formulas && bpDetail.formulas.getAvailableClasses) {
        return this.quotDriver.runFunc(bpDetail.formulas.getAvailableClasses, quotation, planDetails, profiles);
      }
    }
    return {};
  };

  this.calculateQuotation = (quotationJson, planDetailsJson) => {
    let resultObj = {};
    try {
      let quotation = 0, planDetails = 0;
      // Initialize variables based on the input parameters
      if (typeof quotationJson === 'string') {
        if (JSON && typeof JSON.parse === 'function') {
          quotation = JSON.parse(quotationJson);
          planDetails = JSON.parse(planDetailsJson);
        } else {
          eval('quotation = ' + quotationJson);
          eval('planDetails = ' + planDetailsJson);
        }
      } else if (typeof quotationJson === 'object') {
        quotation = quotationJson;
        planDetails = planDetailsJson;
      }

      if (quotation && planDetails) {
        const bpDetail = planDetails[quotation.baseProductCode];

        let riskCommenDate = (quotation.isBackDate === 'Y') ? parseDate(quotation.riskCommenDate) : new Date();
        quotation.riskCommenDate = formatDate(riskCommenDate);
        quotation.iCrossBorderWithoutPass = isCrossBorder(quotation,null, false, false);
        quotation.pCrossBorderWithoutPass = isCrossBorder(quotation,null, true, false);
        quotation.isCrossBorderWithoutPass = quotation.pCrossBorderWithoutPass || quotation.iCrossBorderWithoutPass;
        quotation.pAge = getAgeByMethod(bpDetail.calcAgeMethod, riskCommenDate, parseDate(quotation.pDob));

        const inputConfigs = {};
        _.each(quotation.insureds, (subQuot, iCid) => {
          subQuot.pAge = quotation.pAge;
          subQuot.iAge = getAgeByMethod(bpDetail.calcAgeMethod, riskCommenDate, parseDate(subQuot.iDob));

          let retVal = this.quotDriver.validateQuotation(subQuot, planDetails);
          quotation.insureds[iCid] = retVal.quotation || subQuot;

          planDetails = retVal.planDetails || planDetails;

          inputConfigs[subQuot.iCid] = {};
          _.each(subQuot.plans, (plan) => {
            inputConfigs[subQuot.iCid][plan.covCode] = planDetails[plan.covCode].inputConfig;
          });
        });

        if (bpDetail.formulas && bpDetail.formulas.calcShieldSummary) {
          this.quotDriver.runFunc(bpDetail.formulas.calcShieldSummary, quotation);
        }

        resultObj.quotation = quotation;
        resultObj.inputConfigs = inputConfigs;
      } else {
        resultObj.error = {
          message: 'Quotation or Plan details missing.'
        };
      }
    } catch (ex) {
      console.error(ex);
    }
    return this.quotDriver.context.returnResult(resultObj);
  };

  this.generateIllustrationData = function () {
    return this.quotDriver.context.returnResult({
      reportData: null,
      illustrations: null
    });
  };

};
