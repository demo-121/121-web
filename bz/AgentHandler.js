var cDao = require("./cbDao/agent");
var ip = require('ip');
var logger = global.logger || console;

module.exports.getDocById = function(data, session, cb) {
  logger.log('call getDocById');
  cDao.getDocById(data.id, function(doc) {
    cb({
      success: true,
      data: doc
    });
  });
};

module.exports.readAllNotifications = function(data, session, cb) {
  logger.log('read all notifications');

  cDao.readAllNotifications(data.notifications, data.messageGroup, session.agent).then(resp => {
    cb({
      success: true,
      notification: resp.notifications
    });
  });
}

module.exports.getAgentProfile = function(data, session, cb) {
  logger.log('call getAgentProfile');
  cDao.getAgentProfile(data.id, function(doc) {
    cb({
      success: true,
      template: doc
    });
  });
};

module.exports.addAgentProfile = function(data, session, cb) {
  logger.log('call addAgentProfile');
  cDao.addAgentProfile(data.id, data.user, function(doc) {
    cb({
      success: true,
      template: doc
    });
  });
};

module.exports.updateAgentLoginAud = function(data, session) {
  if (data.agentCode){
    var date = new Date();
    var month = date.getUTCMonth() + 1;
    var year = date.getUTCFullYear();
    var id = "AUD"+"-"+data.agentCode+"-"+year+"-"+month;
    logger.log('updateAgentLoginAu');

    cDao.getDocById(id, function(doc){
      var aud = {};
  //new log format
      var curTime = Math.floor(Date.now() / 1000);

      var newLog = {
        loginSuccess: data.success,
        createDate: curTime,
        loginDate:curTime,
        userIPAddr:data.userIpAddr,
        servIPAddr:ip.address(),
        sysVer:data.sysVer,
        success:data.success,
        timdeDiff: data.timdeDiff
      };
      if (!data.success) {
        newLog.failReason = data.errorMsg;
      }

      if (doc.error) {
        //has no record
        aud.userCode = data.userCode;
        aud.compCode = data.compCode;
        aud.createDate = curTime;

      } else {
        aud = doc;
      }
      if (!aud.agentCode)
        aud.agentCode = data.agentCode;
      if (!aud.compCode)
        aud.compCode = data.compCode;
      if (session.id)
        aud[session.id] = newLog;

      cDao.updateDoc(id, aud, function(doc) {
        var ret = {
          success: true,
          template: doc
        };
      });
    });
  }
};

module.exports.updateAgentLogoutAud = function(data, session, callback) {
  if (session.agentCode) {
    var date = new Date();
    var month = date.getUTCMonth() + 1;
    var year = date.getUTCFullYear();
    var id = "AUD"+"-"+session.agentCode+"-"+year+"-"+month;
    logger.log('updateAgentLogoutAud');

    cDao.getDocById(id, function(doc){
      var aud = {};
  //new log format
      var curTime = Math.floor(Date.now() / 1000);

      if(!doc.error){
        aud = doc;
        aud.isLogin = false;
        if(aud[session.id]){
          aud[session.id].logoutDate = curTime;
          cDao.updateDoc(id, aud, function(doc) {
            var ret = {
              success: true,
              template: doc
            };
          });
        }
      }
    });
  }
  callback();
};

var updateAgentLocalTime = function(data, session, cb) {
  var id = "AUD"+"-"+session.agentCode+"-localTime";
  logger.log('updateAgentLocalTime');

  cDao.getDocById(id, function(doc){
    var aud = {};
    var curTime = new Date();
    var curTimeStr = curTime.toString();

    if(doc.error){
      //has no record
      aud.userCode = data.userCode;
      aud.compCode = data.compCode;
      aud.agentCode = data.agentCode;
      aud.createDate = curTimeStr;
    }else{
      aud = doc;
    }

    aud.localTime = data.localTime;

    logger.log('updateAgentLocalTime');

    cDao.updateDoc(id, aud, function(doc) {
      var ret = {
        success: true,
        template: doc
      };
      cb(ret);
    });
  });
};
module.exports.updateAgentLocalTime = updateAgentLocalTime;

module.exports.checkDateBack = function(data, session, callback) {
  logger.log('call checkDateBack');
  var id = "AUD"+"-" + session.agentCode + "-localTime";
  var lastLogTimeObject = {};
  var ret = {
    success: true
  };
  cDao.getDocById(id, function(doc){
    lastLogTimeObject = doc;
    if(lastLogTimeObject.localTime){
      if(Date.parse(lastLogTimeObject.localTime) < Date.parse(data.localTime)){
        updateAgentLocalTime(data, session, function(){
          callback(ret);
        });
      }else{
          callback({success: false});
      }
    }else{
      updateAgentLocalTime(data, session, function(){
        callback(ret);
      });
    }
  });
};

module.exports.updateProfilePic = function (data, session, callback) {
  cDao.updateProfilePic('UX_' + session.agent.profileId, data.type, data.data, (result) => {
    callback({
      success: result.success,
      agent: result
    });
  });
};
