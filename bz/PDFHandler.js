"use strict";
var _               = require('lodash');
const getByLang     = require('./utils/CommonUtils').getByLang;
var commonFunction  = require('./CommonFunctions');
var XSLT            = require('./xslt/XSLT.js');
var logger          = global.logger || console;
var moment          = require('moment');
const ConfigConstant = require('../app/constants/ConfigConstants');


var _getStaticPdf = function(pdfCodes, reportData, reportTemplates, basicPlanDetails, lang, incognitoMode, callback){
  var pageNum = 1;
  var startPages = {};
  let pageNumberGroup = [];
  let pageNumberGroupIndex = 0;
  _.each(pdfCodes, (pdfCode) => {
    let pdfTemplateConfig = _.find(basicPlanDetails.reportTemplate, (reportTemplate) => {return reportTemplate.pdfCode === pdfCode;}) || {};
    
    if (pdfTemplateConfig.resetPaging === 'Y') {
      pageNumberGroupIndex += 1;
      pageNum = 1;
    }
    startPages[pdfCode] = pageNum;
    var reportTemplate = reportTemplates[pdfCode];

    if (reportTemplate) {
      var templates = getByLang(reportTemplate.template, lang);
      pageNum += templates.length;
    }

    pageNumberGroup[pageNumberGroupIndex] = pageNum;
  });

  var promises = [];
  pageNumberGroupIndex = 0;
  _.each(pdfCodes, (pdfCode) => {
    if (reportTemplates[pdfCode]) {
      let totalPageNumber;
      let pdfTemplateConfig = _.find(basicPlanDetails.reportTemplate, (reportTemplate) => {return reportTemplate.pdfCode === pdfCode;}) || {};
      if (pdfTemplateConfig.resetPaging === 'Y') {
        pageNumberGroupIndex += 1;
      }
      totalPageNumber = pageNumberGroup[pageNumberGroupIndex] || 1;
      promises.push(new Promise((resolve) => {
        var data = Object.assign({}, reportData.root, reportData[pdfCode]);
        XSLT.data2Xml({ root: data }, (xml) => { 
          var xsl = XSLT.prepareReportXSL(reportTemplates[pdfCode], lang,
                startPages[pdfCode], totalPageNumber - 1);
          
          XSLT.xsltTransform(xml, xsl, (xslt) => {
            resolve(xslt);
          });
        });
      }));
    }
  });

  Promise.all(promises).then((result) => {
    let styles = [];
    _.each(reportTemplates, (reportTemplate) => {
      if (reportTemplate.style) {        
        if (incognitoMode) { // incognitoMode will remove the header and footer (to keep the other style, rename the background)
          styles.push(reportTemplate.style.replace(/background/g,'background_incognito').replace(''));
        } else {
          styles.push(reportTemplate.style);
        }
      }
    });
    let contentHtml = _.join(result, '');
    let html = XSLT.prepareHtml(contentHtml, _.join(styles, '\n'));

    
    return XSLT.html2Pdf(html).then(callback);
  }).catch((err) => {
    logger.error(err);
    callback(false);
  });
}


var _getDynamicPdf = function(pdfCodes, reportData = {root: {}}, reportTemplates, basicPlanDetails, lang, incognitoMode, callback){
  var _handleHeaderFooter = function(options, template, type){
    let templateHeader        = _.get(template, `header.${lang}`);
    let templateFooter        = _.get(template, `footer.${lang}`);

    if (incognitoMode) {
      templateHeader = "<P></P>";
      templateFooter = "<P></P>";
    }

    if ((templateFooter !== undefined) && (templateFooter.length > 0)) {
        templateFooter   = templateFooter
            .replace("[[@PAGE_NO]]", "<span class=\"page_no\"/>")
            .replace("[[@TOTAL_PAGE]]", "<span class=\"total_page_no\"/>");
    }

    options.header[type]   = templateHeader;
    options.footer[type]   = templateFooter;
  };


  /** This is a list of  
   * {
   *    pdfOptions {
          numCoverPage: 0,
          header: {
            first:    "",
            default:  ""
          },
          footer: {
            first:    ""
            default:  ""
          }
        },

        pdfCodes: []
      }
   * 
  */
  
  var pdfToGenerate = [];
  var curPdf = null;

  var templateIndex = 0;
  /** Group PDF by resetPage Flag*/
  _.forEach(basicPlanDetails.reportTemplate, (curTemplate)=>{
    if (templateIndex == 0){
      curPdf = {
        pdfOptions: {
          header: {
            first:    "",
            default:  ""
          },
          footer: {
            first:    "",
            default:  ""
          }
        },
     
        pdfCodes: [curTemplate.pdfCode]
      };

      pdfToGenerate.push(curPdf);
    }
    else {
      if ((curTemplate.resetPaging) && (curTemplate.resetPaging === 'Y')){
        curPdf = {
          pdfOptions: {
            header: {
              first:    "",
              default:  ""
            },
            footer: {
              first:    "",
              default:  ""
            }
          },
       
          pdfCodes: [curTemplate.pdfCode]
        };
  
        pdfToGenerate.push(curPdf);
      }
      else {
        curPdf.pdfCodes.push(curTemplate.pdfCode);
      }
    }

    templateIndex++; 
  });


  /** Populate header and footer for each PDF to generate. 
   * The firstHeader and firstFooter is alwasy on the first template;
   * The defaulHeader and defaultFooter on teh seocnd template.*/
  _.forEach(pdfToGenerate, (curPdfTogenerate)=>{
    
    let index = 0;
    _.forEach(curPdfTogenerate.pdfCodes, (pdfCode)=>{
      let reportTemplate = reportTemplates[pdfCode];
      if (index == 0) {
        _handleHeaderFooter(curPdfTogenerate.pdfOptions, reportTemplate, "first", lang);
      }
      
      if (index == 1) {
        _handleHeaderFooter(curPdfTogenerate.pdfOptions, reportTemplate, "default", lang);
      }

      index++;
    });
  });
    

  var _generatePdf = function(curPdfTogenerate, callback_after_generate){
    let pdfOptions  = curPdfTogenerate.pdfOptions;
    let curPdfCodes = curPdfTogenerate.pdfCodes;
    let reportHtml  = "", reportStyle = "";
    let data        = Object.assign({}, reportData.root);
    
    reportHtml  += '<header id="firstHeader" style="width: calc(100% - 120px);margin-left:60px; margin-right: 120px;">'     + _.get(pdfOptions, "header.first")  + '</header>';
    if ((pdfOptions.header.default !== undefined) && (pdfOptions.header.default.length > 0)){ 
      reportHtml  += '<header id="defaultHeader" style="width: calc(100% - 120px);margin-left:60px; margin-right: 120px;">' + _.get(pdfOptions, "header.default")  + '</header>';
    }
    
    reportHtml  += '<footer id="firstFooter" style="width: calc(100% - 120px);margin-left:60px; margin-right: 60px;">'      + _.get(pdfOptions, "footer.first")  + '</footer>';
    if ((pdfOptions.footer.default !== undefined) && (pdfOptions.footer.default.length > 0)){
      reportHtml  += '<footer id="defaultFooter" style="width: calc(100% - 120px);margin-left:60px; margin-right: 60px;">'    + _.get(pdfOptions, "footer.default")  + '</footer>';
    }

    _.forEach(curPdfCodes, (pdfCode)=>{
      let reportTemplate = reportTemplates[pdfCode];
      data         = Object.assign({}, data, reportData[pdfCode]);

      reportHtml  += XSLT.prepareDynamicReportHtml(reportTemplate.template, lang);
      reportStyle += reportTemplate.style;
    });
    
    XSLT.data2Xml({root: data}, (xml) => {
      var xsl = XSLT.prepareDynamicReportXsl(reportHtml, pdfOptions, false);
  
      var startTime = new Date();
      XSLT.xsltTransform(xml, xsl, (xslt) => {
        var elapsedTimeMs = new Date() - startTime;
        logger.log("EXEC: xsltTransform: Elapsed " + elapsedTimeMs + "ms");
        let html = XSLT.prepareDynamicHtml(xslt, pdfOptions, reportStyle);
        commonFunction.convertHtml2Pdf(html, pdfOptions, callback_after_generate);
      });
    });
  };


  var promises = [];

  _.forEach(pdfToGenerate, (curPdfTogenerate)=>{
    promises.push(new Promise((resolve) => {
      _generatePdf(curPdfTogenerate, (pdf) => {resolve(pdf);});
    }));
  });


  Promise.all(promises).then((result) => {
    commonFunction.mergePdfs(result,callback);
  }).catch((err) => {
    logger.error(err);
    callback(false);
  });


}


module.exports.getReportPdf = function (pdfCodes, reportData, reportTemplates, isDynamic, basicPlanDetails, lang, incognitoMode, callback) {
    // for incognitoMode, change the name to YOU.
    if (reportData && incognitoMode)
    {
      for (var i in reportData) {
        reportData[i].headerName = 'YOU';
        if (reportData[i].mainInfo) {
          reportData[i].mainInfo.name = 'YOU';
          reportData[i].mainInfo.pName = 'YOU';
          reportData[i].mainInfo.iName = 'YOU';
        }
      }
    }

  if (!isDynamic){
    _getStaticPdf(pdfCodes, reportData, reportTemplates, basicPlanDetails, lang, incognitoMode, callback);
  }
  else {
    _getDynamicPdf(pdfCodes, reportData, reportTemplates, basicPlanDetails, lang, incognitoMode, callback);
  }
};


module.exports.getFnaReportPdf = function (reportData, reportTemplates, callback) {
  var data = Object.assign({}, reportData.root);
  new Promise((resolve) => {
    XSLT.data2Xml({ root: data }, (xml) => {
        var xsl = XSLT.prepareFnaReportXsl(reportTemplates, 'en', 1, 1);
        XSLT.xsltTransform(xml, xsl, (xslt) => {
          resolve(xslt);
      });
    })
  }).then((result) => {
    let contentHtml = _.join(result, '').split('[[@newLine]]').join('<br/>');
    let style = "";
    let params = {
      header: {
        height: "40px",
        contents: {}
      },
      footer: {
        height: "40px",
        contents: {}
      }
    };

    _.forEach(reportTemplates, (t,index)=>{
      if(index===0){
        var hf = _.at(t, ['header.en','footer.en']);
        if(hf[0]){
          params.header.contents.first = `<div class=\"pageMargin\">${hf[0]}</div>`;
        }
        if(hf[1]){
          params.footer.contents.first = `<div class=\"pageMargin\">${hf[1]}</div>`;
        }
      }
      else if(index===1){
        var hf = _.at(t, ['header.en','footer.en']);
        if(hf[0]){
          params.header.contents.default = `<div class=\"pageMargin\">${hf[0]}</div>`;
        }
        if(hf[1]){
          params.footer.contents.default = `<div class=\"pageMargin\">${hf[1]}</div>`;
        }
      }
      style += _.at(t, 'style')[0] || "";
    })

    let html = XSLT.prepareHtml(contentHtml, style);

    XSLT.html2Pdf(html, params)
      .then(callback)
      .catch(err=>{
        logger.error(err);
      });
  }).catch((err) => {
    callback(false);
  })

}


module.exports.getSupervisorTemplatePdf = function (reportData, reportTemplates, lang, callback) {
  new Promise((resolve) => {
    if (reportData) {
      reportData.policyId = (!_.isEmpty(reportData.proposalNumber)) ? reportData.proposalNumber : reportData.policyId;
      reportData.supervisorApproveRejectDate = (reportData.supervisorApproveRejectDate) ?
        moment(reportData.supervisorApproveRejectDate).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3) : '';
      reportData.approveRejectDate = (reportData.approveRejectDate) ?
        moment(reportData.approveRejectDate).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3) : '';
    }

    XSLT.data2Xml({ root: reportData }, (xml) => {
      var xsl = XSLT.prepareSupervisorXSL(reportTemplates.template[lang], lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, (xslt) => {
        resolve(xslt);
      });
    });
  }).then(function(result) {
    let style = '';
    if (reportTemplates.style) {
      style += reportTemplates.style;
    }
    let html = XSLT.prepareHtml(result, style);
    let params = {
      header: {
        height: '40px',
        contents: {}
      },
      footer: {
        height: '40px',
        contents: {}
      }
    };

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch((err) => {
    logger.error(err);
    callback(false);
  });
};


module.exports.getAppFormPdf = function (reportData, reportTemplates, lang, callback) {
  new Promise((resolve) => {
    XSLT.data2Xml(reportData, (xml) => {
      var xsl = XSLT.prepareAppFormXSL(reportTemplates, lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, (xslt) => {
        resolve(xslt);
      });
    });
  }).then(function(result) {
    let params = {
      header: {
        height: '40px',
        contents: {
        }
      },
      footer: {
        height: '90px',
        contents: {
        }
      }
    };

    let style = '';
    reportTemplates.forEach(function(template, index) {
      if (template.style) {
        style += template.style;
      }
      if (index == 0) {
        params.header.contents.first = '<div class=\"appFormFooter\">' + template.header[lang] + '</div>'
        params.footer.contents.first = '<div class=\"appFormFooter\">' + template.footer[lang]  + '</div>'
      } else {
        params.header.contents.default = '<div class=\"appFormFooter\">' + template.header[lang] + '</div>'
        params.footer.contents.default = '<div class=\"appFormFooter\">' + template.footer[lang]  + '</div>'
      }
    });

    let html = XSLT.prepareHtml(result, style);

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch((err) => {
    callback(false);
  })
}


module.exports.getPremiumPaymentPdf = function (reportData, reportTemplates, lang, callback) {
  new Promise((resolve) => {
    XSLT.data2Xml(reportData, (xml) => {
      var xsl = XSLT.prepareAppFormXSL(reportTemplates, lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, (xslt) => {
        resolve(xslt);
      });
    })
  }).then(function(result) {
    let params = {
      header: {
        height: '40px',
        contents: {
        }
      },
      footer: {
        height: '90px',
        contents: {
        }
      }
    };

    let style = '';
    reportTemplates.forEach(function(template, index) {
      if (template.style) {
        style += template.style;
      }
      // params.header.contents.default = '<div class=\"pageMargin\">' + template.header[lang] + '</div>';
      // params.footer.contents.default = '<div class=\"pageMargin\">' + template.footer[lang]  + '</div>';
    });

    let html = XSLT.prepareHtmlOnePage(result, style);

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch((err) => {
    callback(false);
  })
}


module.exports.getECpdPdf = function (reportData, reportTemplates, lang, callback) {
  new Promise((resolve) => {
    XSLT.data2Xml(reportData, (xml) => {
      var xsl = XSLT.prepareAppFormXSL(reportTemplates, lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, (xslt) => {
        resolve(xslt);
      });
    })
  }).then(function(result) {
    let params = {
      header: {
        height: '40px',
        contents: {
        }
      },
      footer: {
        height: '90px',
        contents: {
        }
      }
    };

    let style = '';
    reportTemplates.forEach(function(template, index) {
      if (template.style) {
        style += template.style;
      }
      params.header.contents.default = '<div class=\"pageFooter\">' + template.header[lang] + '</div>';
      params.footer.contents.default = '<div class=\"pageFooter\">' + template.footer[lang]  + '</div>';
    });

    let html = XSLT.prepareHtmlOnePage(result, style);

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch((err) => {
    callback(false);
  })
}