module.exports = {    
    applicationByQuotationId: function(doc) {
        if (doc && doc.type === 'application') {
            var emitObj = {
                id: doc.id,
                bundleId: doc.bundleId,
                quotationId: doc.quotationDocId,
                policyNumber: doc.policyNumber
            };
            emit(['01', doc.quotationDocId], emitObj);
        }
    }
};
