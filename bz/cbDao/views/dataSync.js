module.exports = {
    getCPRelatedInfomation: function (doc, meta) {
        if (doc && (doc.agentCode || doc.agentId) && ('approval' === doc.type || 'approval' === doc.TYPE
            || 'cust' === doc.type || 'cust' === doc.TYPE
            || 'bundle' === doc.type || 'bundle' === doc.TYPE
            || 'fe' === doc.type || 'fe' === doc.TYPE
            || 'na' === doc.type || 'na' === doc.TYPE
            || 'pda' === doc.type || 'pda' === doc.TYPE
            || 'application' === doc.type || 'application' === doc.TYPE
            || 'quotation' === doc.type || 'quotation' === doc.TYPE
            || 'masterApplication' === doc.type || 'masterApplication' === doc.TYPE
            || 'masterApproval' === doc.type || 'masterApproval' === doc.TYPE
            || 'API_RESPONSE' === doc.type || 'API_RESPONSE' === doc.TYPE)
        ) {
            if (doc.agentCode) {
                emit(['01', doc.agentCode], {
                    id: doc.id,
                    type: doc.type,
                    agentCode: doc.agentCode,
                    lstChgDate: doc.lstChgDate,
                    pCid: doc.pCid,
                    customerId: doc.customerId,
                    cid: doc.cid,
                    payment: doc.payment,
                    isSubmittedStatus: doc.isSubmittedStatus
                });
            } else if (doc.agentId) {
                emit(['01', doc.agentId], {
                    id: doc.id,
                    type: doc.type,
                    agentCode: doc.agentId,
                    lstChgDate: doc.lstChgDate,
                    pCid: doc.pCid,
                    customerId: doc.customerId,
                    cid: doc.cid,
                    payment: doc.payment,
                    isSubmittedStatus: doc.isSubmittedStatus
                });
            }
            
        }
    },
    agentDocuments: function(doc, meta) {
        if (doc && doc.agentCode) {
            emit(['01', doc.agentCode]);
        } else if (doc && doc.agentId) {
            emit(['01', doc.agentId]);
        }
    },
    notMatchRevDocuments: function(doc,meta) {
        if (doc && doc._attachments && doc._rev && doc.agentCode) {
            const docRevNumber = parseInt(doc._rev.substring(0, doc._rev.indexOf('-') ));
            for (var key in doc._attachments) {
                if (doc._attachments[key] && doc._attachments[key].revpos > docRevNumber) {
                    emit(['01', doc.agentCode], {
                        name: key,
                        attactment: doc._attachments[key]
                    });
                }
            }
        }
      },
      withoutAgentCodeDocuments: function (doc) {
        if (doc && (doc.type === 'approval'
        || doc.type === 'cust'
        || doc.type === 'bundle'
        || doc.type === 'fe'
        || doc.type === 'na'
        || doc.type === 'pda'
        || doc.type === 'application'
        || doc.type === 'quotation'
        || doc.type === 'masterApplication'
        || doc.type === 'masterApproval'
        || doc.type === 'agent')
        ) {
            if (!doc.agentCode) {
                var key = doc.agentId || doc.agentCode || doc._id;
                emit(['01', key], doc);
            }
        }
    },
    withoutAgentCodeAndAgentIdDocuments: function (doc) {
      if (doc && (doc.type === 'approval'
      || doc.type === 'cust'
      || doc.type === 'bundle'
      || doc.type === 'fe'
      || doc.type === 'na'
      || doc.type === 'pda'
      || doc.type === 'application'
      || doc.type === 'quotation'
      || doc.type === 'masterApplication'
      || doc.type === 'masterApproval'
      || doc.type === 'agent')
      ) {
          if (!doc.agentCode && !doc.agentId) {
              emit(['01', 'idOnly'], doc._id);
              emit(['01', 'wholeDoc'], doc);
          }
      }
    },
    paymentAndSubmissionDoubleCheck: function (doc) {
        if (doc && doc.agentCode && doc.lstChgDate && doc.type && doc.type === 'application') {
            emit(['01', doc.agentCode, doc.lstChgDate], {
                type: doc.type,
                payment: doc.payment,
                applicationSubmittedDate: doc.applicationSubmittedDate,
                pCid: doc.pCid
            });
        }
    }
};
