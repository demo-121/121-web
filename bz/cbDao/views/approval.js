module.exports = {
    approvalDetails: function (doc) {
        if (doc && doc.type === 'approval') {
          var emitObj = {
              compCode: doc.compCode,
              displayCaseNo:  doc.policyId,
              caseNo: doc.policyId,
              product: doc.productName,
              agentId: doc.agentId,
              agentName: doc.agentName,
              managerName: doc.managerName,
              managerId: doc.managerId,
              directorId: doc.directorId,
              directorName: doc.directorName,
              approveManagerId: doc.approveRejectManagerId,
              approveManagerName: doc.approveRejectManagerName,
              approveRejectManagerId: doc.approveRejectManagerId,
              approveRejectManagerName: doc.approveRejectManagerName,
              submittedDate: doc.submittedDate,
              approvalStatus: doc.approvalStatus,
              onHoldReason: doc.onHoldReason,
              approvalCaseId: doc.approvalCaseId,
              applicationId: doc.applicationId,
              quotationId:  doc.quotationId,
              customerId: doc.customerId,
              customerName: doc.customerName,
              lastEditedBy: doc.lastEditedBy,
              lastEditedDate: doc.lastEditedDate,
              approveRejectDate: doc.approveRejectDate,
              caseLockedManagerCodebyStatus: doc.caseLockedManagerCodebyStatus,
              customerICNo: doc.customerICNo,
              agentProfileId: doc.agentProfileId,
              expiredDate: doc.expiredDate,
              masterApprovalId: doc.masterApprovalId || '',
              isShield: doc.isShield,
              proposalNumber: doc.proposalNumber || doc.policyId,
              accept: doc.accept,
              reject: doc.reject
            };
          emit(['01', doc.approvalStatus, doc.approvalCaseId], emitObj);
        }
    },
    
    shieldPolicy: function(doc) {
        if (doc && doc.type === 'masterApproval') {
            var emitObj = {
                approvalStatus: doc.approvalStatus,
                id: doc.approvalCaseId,
                quotationDocId: doc.quotationId,
                policiesMapping: doc.policiesMapping,
                submittedDate: doc.submittedDate,
                customerId: doc.customerId
            };
            emit(['01', doc.approvalStatus, doc.approvalCaseId], emitObj);
        }
    },

    shieldQuotation: function(doc){
        if (doc && doc.type === 'quotation' && doc.quotType === 'SHIELD') {
            var emitObj = {
                bundleId: doc.bundleId,
                proposerId: doc.pCid,
                insureds: doc.insureds,
                productVersion: doc.productVersion
            };
            emit(['01', doc.id], emitObj);
        }
    }
};
