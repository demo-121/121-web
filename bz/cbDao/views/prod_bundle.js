module.exports = {
    getBundleApps: function (doc, meta) {
        if (doc.type === 'bundle' && doc.applications && doc.applications.length) {
             var applications = [];
          for (var i in doc.applications) {
               var application = doc.applications[i];
            if (!application.invalidateDate) {
                 applications.push(application); 
            }
          }
          
          if (applications.length) {
            var signDateTime = (new Date(doc.fnaSignDate)).getTime();
                 emit(["01", signDateTime], {id: doc.id, data: applications});   
          }
        }
    },
    getProducts: function (doc, meta) {
        if (doc.type === 'quotation') {
          var plans = [];
          for (var i in doc.plans) {
           plans.push(doc.plans[i].covCode); 
          }
             emit(["01", doc.bundleId], {id: doc.id, data: plans});
        } else if (doc.type === 'application') {
          var plans = [];
          for (var i in doc.quotation.plans) {
           plans.push(doc.quotation.plans[i].covCode); 
          }
             emit(["01", doc.bundleId], {policyNumber: doc.policyNumber, id: doc.id, data: plans});
        }
    },
     missingEapprovalForm: function (doc, meta) {
         if (doc && ((doc.type === 'approval' && !doc.isShield) || doc.type === 'masterApproval')) {
              if (!doc._attachments || (doc._attachments && !doc._attachments.eapproval_supervisor_pdf)) {
                   if (
                        doc.accept && ['A','PFAFA', 'PDocFAF','PDisFAF'].indexOf(doc.approvalStatus) > -1 ||
                        doc.reject && ['R','PFAFA', 'PDocFAF','PDisFAF'].indexOf(doc.approvalStatus) > -1
                   ) {
                    emit(["01", doc._id], doc);
                   }
              }
         }
    },
    policyStatus: function (doc, meta) {
         if (doc && doc.type === 'approval') {
               var STATUS = {
                    A:          'Approved',
                    R:          'Rejected',
                    E:          'Expired',
                    SUBMITTED:  'Pending Supervisor Approval',
                    PDoc:       'Pending Document',
                    PDis:       'Pending Discussion',
                    PFAFA:      'Pending FA firm approval',
                    PDocFAF:    'Pending Document (FA Firm)',
                    PDisFAF:    'Pending Discussion (FA Firm)',
                    PCdaA:      'Pending CDA approval',
                    PDocCda:    'Pending Document (CDA)',
                    PDisCda:    'Pending Discussion (CDA)'
                };
                
               var finalStageStatusArr = ['A','R','E'];
               
               var pendingStageStatusArr = ['SUBMITTED','PDoc','PDis','PFAFA','PDisFAF','PCdaA','PDocCda','PDisCda','PDocFAF'];
               var approvalStatus = doc.approvalStatus;
               var longUTCSubmitDate;
               if (doc.submittedDate) {
                    longUTCSubmitDate = Date.parse(doc.submittedDate);
               }
               if (finalStageStatusArr.indexOf(approvalStatus) > -1) {
                    emit(['finalStage', longUTCSubmitDate, doc.approvalCaseId], doc);
               }

               if (finalStageStatusArr.indexOf(approvalStatus) > -1 && doc.submisssionFlag !== true) {
                    emit(['pendingSubmitToRLS', longUTCSubmitDate, doc.approvalCaseId], doc);
               }

               if (pendingStageStatusArr.indexOf(approvalStatus) > -1) {
                    emit(['pendingStage', longUTCSubmitDate, doc.approvalCaseId], doc);
               }
               
         }
    },
    inflightPendingCase: function (doc, meta) {
          if (doc && doc.type === 'approval'
               && doc.productName && doc.productName === 'AXA Retire Treasure'
               && doc.approvalStatus && ['A', 'R', 'E'].indexOf(doc.approvalStatus) === -1
          ) {
               emit(["01", doc._id], doc);
          }

          if (
               doc && doc.type === 'masterApproval'&& doc.policiesMapping
               && doc.approvalStatus && ['A', 'R', 'E'].indexOf(doc.approvalStatus) === -1
          ) {
               var needEmit = false;
               for (var i = 0; i < doc.policiesMapping.length; i++) {
                    var policy = doc.policiesMapping[i];
                    var policyName = policy && policy.covName && policy.covName.en;
                    if (
                         policyName === 'AXA General Care (Plan A)'
                         || policyName === 'AXA General Care (Plan B)'
                         || policyName === 'AXA Basic Care (Plan A)'
                         || policyName === 'AXA Basic Care (Plan B)'
                         || policyName === 'AXA Basic Care (Standard Plan)'
                         || policyName === 'AXA Home Care'
                    ) {
                         needEmit = true
                    }
               }
               emit(["01", doc._id], doc);
          }
    },
}