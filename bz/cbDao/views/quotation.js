module.exports = {    
    quotationByTime: function(doc) {
        if (doc && doc.type === 'quotation') {
            var isQuickQuote = !!doc.quickQuote;
            var lastUpdateDate = doc.lastUpdateDate;
            var baseProductCode = doc.baseProductCode;
            var createDate = doc.createDate;
            if (lastUpdateDate) {
                lastUpdateDate = new Date(lastUpdateDate).getTime();
            }

            if (createDate) {
                createDate = new Date(createDate).getTime();
            }
            var emitObj = {
                id: doc.id,
                bundleId: doc.bundleId,
                proposerId: doc.pCid,
                lastUpdateDate: doc.lastUpdateDate,
                createDate: doc.createDate,
                proposerFullName: doc.pFullName
            };
            emit(['01', 'updateDate', isQuickQuote, baseProductCode, lastUpdateDate], emitObj);
            emit(['01', 'createDate', isQuickQuote, baseProductCode, createDate], emitObj);
        }
    }
};
