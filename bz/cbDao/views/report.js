module.exports = {
  allChannelAppCases: function (doc, meta) {
    if (doc.type === 'application') {
      var quotation = doc.quotation;
      var payment = doc.payment;
      var applicationForm = doc.applicationForm;

      var lastUpdateDate = doc.applicationSubmittedDate || doc.applicationSignedDate || doc.applicationStartedDate || doc.lastUpdateDate;
      var applicationStartedDate = doc.applicationStartedDate;
      if (applicationStartedDate) {
        var dtApplicationStartedDate = new Date(applicationStartedDate).getTime();
        var dtApplicationSubmittedDate = null;
        if (doc.applicationSubmittedDate && new Date(doc.applicationSubmittedDate)) {
          dtApplicationSubmittedDate = new Date(doc.applicationSubmittedDate).getTime();
        }

        var proposerIc = null,
          proposerName = null,
          proposerMobileNo = null,
          proposerMobileCountryCode = null,
          proposerEmailAddress = null,
          dob = null,
          educationLevel = null,
          language = null,
          languageOther = null,
          cid = null,
          pDocType = null,
          pDocTypeOther = null,
          insDocType = null,
          insDocTypeOther = null;
        var lifeAssuredIc = null,
          lifeAssuredName = null;
        var agentName = null,
          channel = null,
          premiumFrequency = null,
          riskProfile = null,
          rop = null,
          currency = null,
          agentCode = null,
          rspAmount = null,
          rspPayFreq = null,
          productLine = null;
        var plans = null;
        var paymentMethod = null;
        var dateOfRoadshow = null,
          venue = null,
          trustedIndividualName = null,
          trustedIndividualMobileNo = null,
          trustedIndividualMobileCountryCode = null;

        var quotation = doc.quotation;
        var payment = doc.payment;
        var applicationForm = doc.applicationForm;

        var pAge = null;
        if (quotation) {
          premiumFrequency = quotation.paymentMode;
          currency = quotation.ccy;
          productLine = quotation.productLine;

          if (quotation.agent) {
            agentName = quotation.agent.name;
            agentCode = quotation.agent.agentCode;
            channel = quotation.agent.dealerGroup;
          }
          if (quotation.extraFlags && quotation.extraFlags.fna) {
            riskProfile = quotation.extraFlags.fna.riskProfile;
          }
          if (quotation.clientChoice && quotation.clientChoice.recommendation && quotation.clientChoice.recommendation.rop) {
            rop = quotation.clientChoice.recommendation.rop.choiceQ1;
          }
          if (quotation.policyOptionsDesc) {
            rspAmount = quotation.policyOptionsDesc.rspAmount;
            rspPayFreq = quotation.policyOptionsDesc.rspPayFreq;
          }

        }

        if (applicationForm && applicationForm.values) {
          var values = applicationForm.values;
          if (values.proposer) {
            var proposer = values.proposer;
            if (proposer.personalInfo) {
              var personalInfo = proposer.personalInfo;
              pDocType = personalInfo.idDocType;
              pDocTypeOther = personalInfo.idDocTypeOther;
              proposerIc = personalInfo.idCardNo;
              proposerName = personalInfo.fullName;
              proposerMobileCountryCode = personalInfo.mobileCountryCode;
              proposerMobileNo = personalInfo.mobileNo;
              proposerEmailAddress = personalInfo.email;
              dob = personalInfo.dob;
              pAge = personalInfo.pAge;
              educationLevel = personalInfo.education;
              language = personalInfo.language;
              languageOther = personalInfo.languageOther;
              cid = personalInfo.cid;
              if (personalInfo.bundle) {
                var bundle = personalInfo.bundle;
                var fnId = null;
                for (var i = 0; i < bundle.length; i++) {
                  var item = bundle[i];
                  if (item.isValid) {
                    fnId = item.id;
                  }
                }
              }
            }
            if (proposer.declaration) {
              var declaration = proposer.declaration;
              dateOfRoadshow = declaration.ROADSHOW02;
              venue = declaration.ROADSHOW03;
              if (declaration.trustedIndividuals) {
                trustedIndividualName = declaration.trustedIndividuals.fullName;
                trustedIndividualMobileNo = declaration.trustedIndividuals.mobileNo;
                trustedIndividualMobileCountryCode = declaration.trustedIndividuals.mobileCountryCode;
              }
            }
          }
          if (values.insured && values.insured[0]) {
            var insured = values.insured[0];
            if (insured.personalInfo) {
              var personalInfo = insured.personalInfo;
              lifeAssuredIc = personalInfo.idCardNo;
              lifeAssuredName = personalInfo.fullName;
              insDocType = personalInfo.idDocType;
              insDocTypeOther = personalInfo.idDocTypeOther;
            }
          }

          if (values.planDetails) {
            var planDetails = values.planDetails;
            if (!currency)
              currency = planDetails.ccy;
            if (planDetails.planList) {
              var planList = planDetails.planList;
              plans = new Array(planList.length);
              for (var i = 0; i < planList.length; i++) {
                var plan = planList[i];
                plans[i] = ({
                  covName: plan.covName,
                  sumInsured: plan.sumInsured,
                  premium: plan.premium
                });
              }
            }
          }
        }
        if (payment) {
          paymentMethod = payment.initPayMethod;
        }
        var emitObj = {
          id: doc.id,
          parentId: doc.parentId,
          policyNumber:doc.policyNumber,
          type: doc.type,
          applicationSubmittedDate: dtApplicationSubmittedDate,
          applicationStartedDate:dtApplicationStartedDate,
          lastUpdateDate: lastUpdateDate,
          premiumFrequency: premiumFrequency,
          agentName: agentName,
          currency: currency,
          channel: channel,
          riskProfile: riskProfile,
          rop: rop,
          pDocType : pDocType,
          pDocTypeOther : pDocTypeOther,
          proposerIc : proposerIc,
          proposerName : proposerName,
          proposerMobileCountryCode : proposerMobileCountryCode,
          proposerMobileNo : proposerMobileNo,
          proposerEmailAddress : proposerEmailAddress,
          dob : dob,
          educationLevel : educationLevel,
          language : language,
          pAge:pAge,
          languageOther:languageOther,
          dateOfRoadshow : dateOfRoadshow,
          venue : venue,
          trustedIndividualName : trustedIndividualName,
          trustedIndividualMobileNo : trustedIndividualMobileNo,
          trustedIndividualMobileCountryCode:trustedIndividualMobileCountryCode,
          insDocType : insDocType,
          insDocTypeOther : insDocTypeOther,
          lifeAssuredIc : lifeAssuredIc,
          lifeAssuredName : lifeAssuredName,
          plans : plans,
          paymentMethod : paymentMethod,
          cid: cid,
          fnId : fnId,
          agentCode : agentCode,
          productLine:productLine,
          rspAmount:rspAmount,
          rspPayFreq:rspPayFreq
        };
        
        emit(['01', 'applicationId', doc.id], emitObj);
        emit(['01', 'appStartDate', dtApplicationStartedDate], emitObj);

        if (dtApplicationSubmittedDate != null) {
          emit(['01', 'appSubmissionDate', dtApplicationSubmittedDate], emitObj);
        }


      }
    }
  },

  allChannelPolicyCases: function (doc, meta) {
    if (doc && doc.type === 'approval' && doc.approveRejectDate && doc.approveRejectDate !== '') {
      var approveRejectDate = new Date(doc.approveRejectDate).getTime();
      emit(['01', 'approveRejectDate', approveRejectDate], {
        applicationId: doc.applicationId
      });
    }
  }
};
