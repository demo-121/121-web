const dao = require('../cbDaoFactory').create();

module.exports.getAuditLog = function(docId, callback) {
  dao.getDoc(docId, function(result) {
    // if (result) {
      callback(result);
    // }
  })
}

module.exports.updateAudById = function(id, data, callback) {
  dao.updDoc(id, data, function(result) {
    // if (result) {
      callback(result);
    // }
  });
}
