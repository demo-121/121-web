const _ = require('lodash');
const _c = require('./common');
const _a = require('./applications');

const dao = require('../../cbDaoFactory').create();
const cDao = require('../client');
const nDao = require('../needs');
const quotDao = require('../quotation');
const prodDao = require('../product');
const QuotDriver = require('../../Quote/QuotDriver');
const QuotUtils = require('../../handler/quote/QuotUtils');

const logger = global.logger || console;

var _getAppIdByQuot = function (cid, quotId) {
  return _c.getCurrentBundle(cid).then((bundle) => {
    if (!bundle) {
      return null;
    }
    let application = _.find(bundle.applications, app => app.quotationDocId === quotId);
    return application && application.applicationDocId;
  }).catch((error)=>{
      logger.error("Error in _getAppIdByQuot->getCurrentBundle: ", error);
  });
};
module.exports.getAppIdByQuotId = _getAppIdByQuot;

const invalidateQuotationsByFunc = (cid, validFunc) => {
  return _c.getCurrentBundle(cid).then((bundle) => {
    if (!bundle) {
      return;
    }
    let promises = [];
    _.each(bundle.applications, (application) => {
      if (application.appStatus !== 'INVALIDATED' && application.quotationDocId) {
        promises.push(new Promise((resolve, reject) => {
          quotDao.getQuotation(application.quotationDocId, resolve);
        }).then((quotation) => {
          return Promise.resolve(validFunc(quotation)).then((valid) => {
            if (valid !== true) {
              _a.invalidateApplication(bundle, application.quotationDocId);
            }
          });
        }));
      }
    });
    return Promise.all(promises).then(() => {
      return new Promise((resolve) => {
        logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; fn:invalidateQuotationsByFunc]`);
        _c.updateBundle(bundle, resolve);
      });
    });
  });
};

module.exports.invalidateQuotationsByFE = (cid, fe) => {
  return prodDao.getProductSuitability().then((suitability) => {
    return invalidateQuotationsByFunc(cid, (quotation) => {
      return new QuotDriver().validateQuotSuitability(suitability, { fe }, quotation);
    });
  });
};

module.exports.invalidateQuotationsByNA = (cid, fna) => {
  return prodDao.getProductSuitability().then((suitability) => {
    return invalidateQuotationsByFunc(cid, (quotation) => {
      const promises = [];
      promises.push(nDao.getItem(cid, nDao.ITEM_ID.PDA));
      promises.push(nDao.getItem(cid, nDao.ITEM_ID.FE));
      promises.push(new Promise((resolve) => {
        prodDao.getProduct(quotation.baseProductId, resolve);
      }));
      promises.push(QuotUtils.getQuotClients(quotation));
      return Promise.all(promises).then(([pda, fe, product, profiles]) => {
        return new QuotDriver().validateProdSuitability(suitability, { pda, fna, fe }, product, profiles);
      });
    });
  });
};

module.exports.onCreateQuotation = (agent, cid, confirm) => {
  return _c.getCurrentBundle(cid).then((bundle) => {
    if (bundle && bundle.status >= _c.BUNDLE_STATUS.SIGN_FNA) {
      logger.log('Bundle :: onCreateQuotation :: bundle fna is signed', bundle.id, bundle.status);
      let hasInProgress = _.find(bundle.applications, application => ['SUBMITTED', 'INVALIDATED', 'INVALIDATED_SIGNED'].indexOf(application.appStatus) === -1);
      if (confirm || (bundle.status === _c.BUNDLE_STATUS.SUBMIT_APP && !hasInProgress)) {
        logger.log('Bundle :: onCreateQuotation :: create new bundle');
        return _c.createNewBundle(cid, agent).then((bundles) => {
          let allowCreateQuotResult = {
            bundle: _.find(bundles, b => b.isValid),
            isCreateNewBundle: true
          };
          return cDao.getProfile(cid, true).then((profile) => {
            allowCreateQuotResult.profile = profile;
            return allowCreateQuotResult;
          });
        });
      } else {
        return { code: _c.CHECK_CODE.INVALID_BUNDLE };
      }
    } else {
      return { bundle };
    }
  });
};

const _getProposalFuncPerms = function (cid, quotId, isFaChannel) {
  return _c.getCurrentBundle(cid).then((bundle) => {
    let perms = {
      requote: true,
      clone: false
    };
    if (bundle) {
      let application = _.find(bundle.applications, app => app.quotationDocId === quotId);
      if (!isFaChannel && bundle.status >= _c.BUNDLE_STATUS.SIGN_FNA) {
        perms.requote = false;
      } else {
        perms.requote = !(application && (application.applicationDocId || application.appStatus === 'INVALIDATED'));
      }
      perms.requoteInvalid = application && application.appStatus === 'INVALIDATED';
    }
    return perms;
  });
};
module.exports.getProposalFuncPerms = _getProposalFuncPerms;

var _onSaveQuotation = function (session, cid, quotId, isNewQuotation) {
  return _c.getCurrentBundle(cid).then((bundle) => {
    if (!bundle) {
      return;
    }
    if (bundle.status < _c.BUNDLE_STATUS.HAVE_BI) {
      bundle.status = _c.BUNDLE_STATUS.HAVE_BI;
    }
    if (isNewQuotation) {
      if (!_.find(bundle.applications, app => app.quotationDocId === quotId)) {
        logger.log('Bundle :: adding quotation to bundle: bundleId = ' + bundle.id + ', quotId = ' + quotId);
        bundle.applications.push({ quotationDocId: quotId });
      }
      bundle.clientChoice.isRecommendationCompleted = false;
    }
    bundle.clientChoice.clientChoiceStep = 0;
    bundle.clientChoice.isAppListChanged = true;
    bundle.clientChoice.isClientChoiceCompleted = false;
    bundle.clientChoice.isBudgetCompleted = false;
    bundle.clientChoice.isAcceptanceCompleted = false;
    //budget will be recalculated if isAppListChanged = true

    logger.log('Bundle :: client choice not completed: bundleId = ' + bundle.id);

    return new Promise((resolve) => {
      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; fn:_onSaveQuotation]`);
      _c.updateBundle(bundle, resolve);
    }).then(() => {
      if (session.agent.channel.type !== 'FA') {
        return _a.rollbackApplication2Step1(cid);
      } else {
        return true;
      }
    }).catch((error)=>{
      logger.error("Error in initClientChoice->getCurrentBundle: ", error);
    });
  }).catch((error)=>{
      logger.error("Error in initClientChoice->getCurrentBundle: ", error);
  });
};
module.exports.onSaveQuotation = _onSaveQuotation;

const invalidateQuotation = (bundleId, quotIds) => {
  return new Promise((resolve) => {
    _c.getBundle(null, bundleId, (bundle) => {
      _.each(quotIds, (quotId) => {
        _a.invalidateApplication(bundle, quotId, 'invalidFund');
      });
      logger.log(`INFO: BundleUpdate - [cid:${_.get(bundle, 'pCid')}; bundleId:${bundleId}; quotIds:${JSON.stringify(quotIds)}; fn:invalidateQuotation]`);
      _c.updateBundle(bundle, () => {
        resolve();
      });
    });
  });
};

module.exports.invalidateApplicationsByFund = (fundCodes) => {
  return new Promise((resolve) => {
    logger.log('Bundle :: invalidateApplicationsByFund :: fundCodes:', fundCodes);
    dao.getViewRange(
      'main',
      'inProgressQuotFunds',
      null,
      null,
      null,
      resolve
    );
  }).then((result) => {
    if (result) {
      let quotMap = {};
      let qId, bId;
      _.each(result.rows, (row) => {
        let {key, value} = row;
        if (value.bundleId) {
          qId = key[1];
          bId = value.bundleId;
        } else if (key[2]) {
          if (value.quotationId === qId && fundCodes.indexOf(value.fundCode) > -1) {
            logger.log('Bundle :: invalidateApplicationsByFund :: bundleId = ' + bId + ', quot = ' + qId + ', fund: ' + value.fundCode);
            if (!quotMap[bId]) {
              quotMap[bId] = [];
            }
            if (quotMap[bId].indexOf(qId) === -1) {
              quotMap[bId].push(qId);
            }
          }
        }
      });
      return Promise.all(_.map(quotMap, (quotIds, bundleId) => {
        return invalidateQuotation(bundleId, quotIds);
      }));
    }
  }).catch((error)=>{
      logger.error("Error in invalidateApplicationsByFund->invalidateApplicationsByFund: ", error);
  });
};
