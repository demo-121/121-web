const dao = require('../cbDaoFactory').create();
const commonDao = require('./common');
const cFunctions = require('../utils/CommonUtils');
const moment = require('moment');
const utils = require('../utils/RemoteUtils');
const DateUtils = require('../../common/DateUtils');
var logger = global.logger || console;
var _ = require('lodash');

const dateString = 'YYYY-MM-DD HH:mm:ss';

module.exports.getDownloadMaterial = function(compCode, agentId, callback){
    let result = [];
    dao.getViewRange(
        'main',
        'downloadMaterial',
        null,
        null,
        null,
        function(pList){
            if (pList && pList.rows){
                for (var i=0; i<pList.rows.length; i++){
                    let dateNow = new Date();
                    let effDate = moment(pList.rows[i].value.effDate, dateString);
                    let expDate = moment(pList.rows[i].value.expDate, dateString);

                    if(dateNow < expDate && dateNow > effDate){
                        result.push(pList.rows[i].value);
                    }            
                }
            }
            callback(result);
        }
    );

}