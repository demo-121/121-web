const dao = require('../cbDaoFactory').create();
const _ = require('lodash');
var async = require('async');
const notifyHandler = require('../handler/SystemNotificationHandler');
var logger = global.logger || console;

module.exports.getDocById = function(docId, callback) {
  logger.log('getDocById');
  dao.getDoc(docId, function(result) {
    if (result) {
      callback(result);
    }
  });
};

var getAgentDocId = function (id) {
  // var agentId = "A-" + (new Buffer(email).toString('base64'));
  var agentId = "U_" + id;
  return agentId;
};

var getAgentSuppDocId = (id) => {
  return 'UX_' + id;
};

module.exports.getAgentSuppDocId = getAgentSuppDocId;

const getAgentById = (profileId, callback) => {
  dao.getDoc(getAgentDocId(profileId), callback);
};
module.exports.getAgentById = getAgentById;

const mergeArray = (fir, sec) => {
  let supp = fir;
  let notifications = supp.notifications || [];
  for(let i in sec) {
      let haveCampaign = false;
      if(sec[i].messageId) {
          for(let j in fir.notifications) {
              if( (fir.notifications[j].messageId == sec[i].messageId)) {
                  haveCampaign = true;
                  break;
              }
          }
      }
      if(!haveCampaign) {
        if(!sec[i].read) {
          sec[i].read = false;
        }
        notifications.push(sec[i]);
      }
  }
  supp.notifications = notifications;
  return supp;
}

const getLoginAgentById = (profileId, callback) => {

  dao.getDoc(getAgentDocId(profileId), (agent) => {
    if (agent && !agent.error) {

  notifyHandler.getValidSystemNotification(agent.compCode, (notifications) => {
    dao.getDoc(getAgentSuppDocId(profileId), (supp) => {

      supp = mergeArray(supp, notifications);
      let loginAgent = Object.assign({}, agent);

      if (supp && !supp.error) {
        if (supp._attachments && supp._attachments.agentProfilePic){
          supp.agentProfilePic = supp._attachments.agentProfilePic;
        }
        loginAgent = Object.assign(loginAgent, supp);
        getEligProducts(loginAgent, (eligProducts) => {
          loginAgent.eligProducts = eligProducts;
          callback(loginAgent);
        });
      } else if (supp.error === 'not_found') {
        let docId = getAgentSuppDocId(profileId);
        dao.updDoc(docId,{},(resp)=>{
          if (resp && !resp.error){
            loginAgent._id = resp.id;
            loginAgent._rev = resp.rev;
            getEligProducts(loginAgent, (eligProducts) => {
              loginAgent.eligProducts = eligProducts;
              callback(loginAgent);
            });
          }
          else {
            callback(resp);
          }
        });
      }
      else {
        getEligProducts(loginAgent, (eligProducts) => {
          loginAgent.eligProducts = eligProducts;
          callback(loginAgent);
        });
      }
    });
  });

    } else {
      callback(agent);
    }
  });
};
module.exports.getLoginAgentById = getLoginAgentById;

const getEligProducts = function (agent, callback) {
  dao.getDoc('eligProducts', (eligProducts) => {
    if (eligProducts && eligProducts.mapping) {
      getChannels(function(channels){
        const channelType = channels.channels[agent.channel].type === 'AGENCY' ? 'AG' : 'FA';
        const mapping = eligProducts.mapping[channelType];
        const covCodes = new Set();
        _.each(mapping['999999999'], (covCode) => {
          covCodes.add(covCode);
        });
        _.each(mapping[agent.profileId], (covCode) => {
          covCodes.add(covCode);
        });
        callback(Array.from(covCodes));
      });
    } else {
      callback(agent.eligProducts);
    }
  });
};

module.exports.updateDoc = function(docId, data, callback) {
  logger.log('updateDoc');
  dao.updDoc(docId, data, function(result) {
    if (result) {
      callback(result);
    } else {
      callback(false);
    }
  });
};


module.exports.getAgentProfile = function(docId, callback) {
  logger.log('getAgentProfile');
  dao.getDoc(docId, function(result) {
    if (result) {
      callback(result);
    }
  });
};

module.exports.addAgentProfile = function(docId, user, callback) {
  logger.log('addAgentProfile');
  dao.updDoc(docId, user, function(result) {
    if (result) {
      callback(result);
    }
  });
};

module.exports.updateAgentProfile = function(docId, user, callback) {
  logger.log('updateAgentProfile');
  dao.updDoc(docId, user, function(result) {
    if (result) {
      callback(result);
    }
  });
};

module.exports.agentLoginAud = function(docId, user, callback) {
  logger.log('agentLoginAud');
  dao.updDoc(docId, user, function(result) {
    if (result) {
      callback(result);
    }
  });
};

const getChannels = function(callback) {
  dao.getDocFromCacheFirst("channels", function(result) {
    if (result) {
      callback(result);
    } else {
      callback(false);
    }
  });
};
module.exports.getChannels = getChannels;

module.exports.updateProfilePic = function (docId, type, data, callback) {
  dao.getDoc(docId, (agent) => {
    dao.uploadAttachmentByBase64(docId, 'agentProfilePic', agent._rev, data, type, (result) => {
      if (result && !result.error) {
        dao.getDoc(docId, function(agentNew) {
          if (agentNew && agentNew._attachments && agentNew._attachments.agentProfilePic) {
            callback({
              success: true,
              agentProfilePic: agentNew._attachments.agentProfilePic
            });
          } else {
            callback({success:false});
          }
        });
      } else {
        callback({success:false});
      }
    });
  });
};

module.exports.searchAgents = function(compCode, idS, idE, callback){
    let result = [];
    return new Promise((resolve) => {
        dao.getViewRange(
            'main',
            'agents',
            '["'+ compCode + '","' + idS + '"' +']',
            '["'+ compCode + '","' + idE + '"' +']',
            null,
            function(pList){
              if(pList && pList.rows){
                  for(var i=0; i<pList.rows.length; i++){
                  pList.rows[i].value.docId = pList.rows[i].id;
                  result.push(pList.rows[i].value);
                  }
              }
              resolve(callback({success: true, result}));
            }
        );
    }).catch((e) => {
        logger.error(e);
        callback({success: false});
    });
};

let _getAgents = (compCode, agentCodes) => {
  let currentDate = new Date();
  let keys = _.map(agentCodes, agentCode => '["' + compCode + '","agentCode","' + agentCode + '"]');
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then((result) => {
    let agentMap = {};
    if (result) {
      _.each(result.rows, (row) => {
        let agent = row.value || {};
        agent.isProxying = agent.rawData && (
          currentDate >= new Date(agent.rawData.proxyStartDate) &&
          currentDate.getTime() < new Date(agent.rawData.proxyEndDate).getTime() + (1000 * 60 * 60 * 24)
        );
        agentMap[row.value.agentCode] = agent;
      });
    }
    return agentMap;
  }).catch((error)=>{
    logger.error("Error in getAgents->getViewByKeys: ", error);
  });
};

module.exports.getAgents = _getAgents;

let getProxyDownline = (compCode, userIds) => {
  let keys = _.map(userIds, userId => '["' + compCode + '","proxy","' + userId + '"]');
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then((result) => {
    let relatedDownline = [];
    if (result) {
      _.each(result.rows, (row) => {
        let agent = row.value || {};
        if (agent.agentCode) {
          relatedDownline.push(agent.agentCode);
        }
      });
    }
    return relatedDownline;
  }).catch((error)=>{
    logger.error("Error in getAgents->getViewByKeys: ", error);
    return [];
  });
};
module.exports.getProxyDownline = getProxyDownline;

let _geAllAgents = (compCode) => {
  return new Promise((resolve) => {
    let currentDate = new Date();
    dao.getViewRange(
      'main',
      'agentDetails',
      '["' + compCode + '","agentCode","0"]',
      '["' + compCode + '","agentCode","ZZZ"]',
      null,
      (result) => {
        let agentMap = {};
        if (result) {
          _.each(result.rows, (row) => {
            let agent = row.value || {};
            /**
             * Align with the view 'agents'
             */
            agent.agentName = agent.name;
            if (agent.rawData && agent.rawData.faAdvisorRole !== undefined) {
              agent.faadminCode = agent.rawData.upline2Code;
            }
            /**
             * Align with the view 'agents'
             */
            agent.isProxying = agent.rawData && (
              currentDate >= new Date(agent.rawData.proxyStartDate) &&
              currentDate.getTime() < new Date(agent.rawData.proxyEndDate).getTime() + (1000 * 60 * 60 * 24)
            );
            agentMap[row.value.agentCode] = agent;
            if (row.value.rawData) {
              agentMap[row.value.rawData.userId] = agent;
            }
          });
        }
        resolve(agentMap);
      }
    );
  }).catch(e =>{
    logger.error('Get All Agents Details: ' + e);
  });
};

module.exports.geAllAgents = _geAllAgents;

module.exports.getAgentsByUserId = (compCode, userIds) => {
  let currentDate = new Date();
  let keys = _.map(userIds, userId => '["' + compCode + '","userId","' + userId + '"]');
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then((result) => {
    let agentMap = {};
    if (result) {
      _.each(result.rows, (row) => {
        let agent = row.value || {};
        agent.isProxying = agent.rawData && (
          currentDate >= new Date(agent.rawData.proxyStartDate) &&
          currentDate.getTime() < new Date(agent.rawData.proxyEndDate).getTime() + (1000 * 60 * 60 * 24)
        );
        if (agent.rawData && agent.rawData.userId) {
          agentMap[agent.rawData.userId] = agent;
        }
        agentMap[agent.agentCode] = agent;
      });
    }
    return agentMap;
  }).catch((error)=>{
    logger.error("Error in getAgentsByUserId->getViewByKeys: ", error);
  });
};

module.exports.getAgentsByAgentCode = (compCode, agentCodes) => {
  let currentDate = new Date();
  let keys = _.map(agentCodes, agentCode => '["' + compCode + '","agentCode","' + agentCode + '"]');
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then((result) => {
    let agentMap = {};
    if (result) {
      _.each(result.rows, (row) => {
        let agent = row.value || {};
        agent.isProxying = agent.rawData && (
          currentDate >= new Date(agent.rawData.proxyStartDate) &&
          currentDate.getTime() < new Date(agent.rawData.proxyEndDate).getTime() + (1000 * 60 * 60 * 24)
        );
        if (agent.rawData && agent.rawData.userId) {
          agentMap[agent.rawData.userId] = agent;
        }
        agentMap[agent.agentCode] = agent;
      });
    }
    return agentMap;
  }).catch((error)=>{
    logger.error("Error in getAgentsByAgentCode->getViewByKeys: ", error);
  });
};

module.exports.getMangerProfileByAgentCode = (compCode, agentCode, cb) => {
  async.waterfall([
    (callback) => {
      _getAgents(compCode, [agentCode]).then(agentMap => {
        callback(null, agentMap[agentCode]);
      });
    }, (agentProfile, callback) => {
      _getAgents(compCode, [_.get(agentProfile, 'managerCode')]).then(agentMap => {
        callback(null, agentMap[_.get(agentProfile, 'managerCode')]);
      });
    }
  ], (err, managerProfile) => {
      if (err) {
        logger.error("ERROR:: getMangerProfileByAgentCode: ", err);
        cb({});
      } else {
        cb(managerProfile);
      }
  });
};

module.exports.getDirectorProfileByAgentCode = (compCode, agentCode) => {
  return new Promise(resolve => {
    async.waterfall([
        (callback) => {
          _getAgents(compCode, [agentCode]).then(agentMap => {
            callback(null, agentMap[agentCode]);
          });
        }, (agentProfile, callback) => {
          _getAgents(compCode, [_.get(agentProfile, 'managerCode')]).then(agentMap => {
            callback(null, agentMap[_.get(agentProfile, 'managerCode')]);
          });
        }, (managerProfile, callback) => {
          _getAgents(compCode,  [_.get(managerProfile, 'managerCode')]).then(agentMap => {
            callback(null, agentMap[_.get(managerProfile, 'managerCode')]);
          });
        }
      ], (err, directorProfile) => {
          if (err) {
            logger.error("ERROR:: getDirectorProfileByAgentCode: ", err);
            resolve({});
          } else {
            resolve(directorProfile);
          }
      });
  });
};

module.exports.getManagerDirectorProfileByAgentCode = (compCode, agentCode) => {
    return new Promise(resolve => {
      async.waterfall([
        (callback) => {
          _getAgents(compCode, [agentCode]).then(agentMap => {
            callback(null, agentMap[agentCode]);
          });
        }, (agentProfile, callback) => {
          let profiles = {};
          _getAgents(compCode, [_.get(agentProfile, 'managerCode')]).then(agentMap => {
            profiles.agentProfile = agentProfile;
            profiles.managerProfile = agentMap[_.get(agentProfile, 'managerCode')];
            callback(null, profiles);
          });
        }, (profiles, callback) => {
          _getAgents(compCode,  [_.get(profiles, 'managerProfile.managerCode')]).then(agentMap => {
            profiles.directorProfile = agentMap[_.get(profiles, 'managerProfile.managerCode')];
            callback(null, profiles);
          });
        }
      ], (err, profiles) => {
          if (err) {
              resolve({});
          } else {
              resolve(profiles);
          }
      });
    });
};

const recursiveSearchUpline = (compCode, agentCodes, searchingKeys, callback) => {
  searchingKeys = _.clone(searchingKeys);
  searchUpline(compCode, searchingKeys).then(uplineAgentCodes => {
      // Filter out any non searched agent code
      searchingKeys = _.filter(uplineAgentCodes, agentCode => agentCodes.indexOf(agentCode) === -1);
      agentCodes = _.union(agentCodes, uplineAgentCodes);
      if (searchingKeys.length) {
        recursiveSearchUpline(compCode, agentCodes, searchingKeys, callback);
      } else {
        callback(null, agentCodes);
      }
  }).catch(error => {
    logger.error(`ERROR in recursiveSearchUpline: ${error}`);
    callback(error);
  });
};
module.exports.recursiveSearchUpline = recursiveSearchUpline;

/**
 * Get the downline base on the agentCodes provided, included provided agentCodes in result
 * @param {*} compCode
 * @param {*} agentCodes
 * @param {*} searchingKeys
 * @param {*} callback
 */
const recursiveSearchDownline = (compCode, agentCodes, searchingKeys, callback) => {
  searchingKeys = _.clone(searchingKeys);
  searchDownline(compCode, searchingKeys).then(donwlineAgentArray => {
      // Filter out any non searched agent code
      searchingKeys = _.filter(donwlineAgentArray, agentCode => agentCodes.indexOf(agentCode) === -1);
      agentCodes = _.union(agentCodes, donwlineAgentArray);
      if (searchingKeys.length) {
        recursiveSearchDownline(compCode, agentCodes, searchingKeys, callback);
      } else {
          callback(null, agentCodes);
      }
  }).catch(error => {
    logger.error(`ERROR in recursiveSearchDownline: ${error}`);
    callback(error);
  });
};
module.exports.recursiveSearchDownline = recursiveSearchDownline;

const searchFAFirmDownline = function(compCode, agentCodeArray, callback) {
  let keys = _.map(agentCodeArray, agentCode => '["' + compCode + '","fafirmCode","' + agentCode + '"]');
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then(result => {
    let resultArrary = [];
    if (result && result.rows && result.rows.length > 0) {
      _.each(result.rows, (row) => {
        let agent = row.value || {};
        if (agent.agentCode) {
          resultArrary.push(agent.agentCode);
        }
      });
    }
    return callback(null, resultArrary);
  }).catch((error)=>{
      logger.error(`Error in searchFAFirmDownline: ${error}`);
      callback(error);
  });
};
module.exports.searchFAFirmDownline = searchFAFirmDownline;

const searchDownline = function(compCode, agentCodeArray) {
  let keys = _.map(agentCodeArray, agentCode => '["' + compCode + '","' + agentCode + '"]');
  return dao.getViewByKeys('main', 'managerDownline', keys, null).then(result => {
    let resultArrary = [];
    if (result && result.rows && result.rows.length > 0) {
      _.each(result.rows, (row) => {
        let agent = row.value || {};
        if (agent.agentCode) {
          resultArrary.push(agent.agentCode);
        }
      });
    }
    return resultArrary;
  }).catch((error)=>{
      logger.error(`Error in searchDownline->getViewByKeys: ${error}`);
      return [];
  });
};
module.exports.searchDownline = searchDownline;

const searchDirectorDownline = function(compCode, agentCodeArray) {
  let keys = _.map(agentCodeArray, agentCode => '["' + compCode + '","' + agentCode + '"]');
  return dao.getViewByKeys('main', 'directorDownline', keys, null).then(result => {
    let resultArrary = [];
    if (result && result.rows && result.rows.length > 0) {
      _.each(result.rows, (row) => {
        let agent = row.value || {};
        if (agent.agentCode) {
          resultArrary.push(agent.agentCode);
        }
      });
    }
    return resultArrary;
  }).catch((error)=>{
      logger.error(`Error in searchDirectorDownline->getViewByKeys: ${error}`);
      return [];
  });
};
module.exports.searchDirectorDownline = searchDirectorDownline;

const searchUpline = function(compCode, agentCodes) {
  let keys = _.map(agentCodes, agentCode => '["' + compCode + '","agentCode","' + agentCode + '"]');
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then((result) => {
    let managerCodes = [];
    if (result) {
      _.each(result.rows, (row) => {
        let agent = row.value || {};
        managerCodes = _.union(managerCodes, [agent.managerCode]);

        // Push Director Code to result array
        if (agent && agent.rawData && agent.rawData.upline2Code) {
          managerCodes = _.union(managerCodes, [agent.rawData.upline2Code]);
        }
      });
    }
    return managerCodes;
  }).catch((error)=>{
    logger.error("Error in searchUpline->getViewByKeys: ", error);
    return [];
  });
};
module.exports.searchUpline = searchUpline;

module.exports.getNotifications = (agent) => {
  return new Promise(resolve => {
    const { profileId } = agent;
    dao.getDoc(getAgentSuppDocId(profileId), suppDoc => {
      if (suppDoc && !suppDoc.error) {
        const notifications = _.filter(_.get(suppDoc, 'notifications', []), notice => !notice.read);
        resolve(notifications);
      } else {
        resolve([]);
      }
    });
  });
};

module.exports.readAllNotifications = (notifications,messageGroup, agent) => {
  return new Promise(resolve => {
    const { profileId } = agent;
    const suppDocId = getAgentSuppDocId(profileId);
    dao.getDoc(suppDocId, suppDoc => {
      if (suppDoc && !suppDoc.error) {

        // old function before doing cr115-one-time-message
        // _.forEach(suppDoc.notifications, notice => {
        //   if(notice.messageGroup == messageGroup) {
        //     if (!notice.read) {
        //       notice.readTime = new Date();
        //     }
        //     notice.read = true;
        //   }
        // });

        for(let i in notifications) {
          if(notifications[i].messageGroup == messageGroup) {
            if (!notifications[i].read) {
              notifications[i].readTime = new Date();
            }
            notifications[i].read = true;
          }
        }
        // if (!suppDoc.notifications) {
          suppDoc.notifications = notifications;
        // } else if (_.isArray(suppDoc.notifications)) {
        //   suppDoc.notifications = _.concat(suppDoc.notifications, notifications);
        // }
        dao.updDoc(suppDocId, suppDoc, () => {
          resolve(suppDoc);
        });
      } else {
        resolve(false);
      }
    });
  });
};
