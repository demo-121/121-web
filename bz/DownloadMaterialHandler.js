const dao = require('./cbDaoFactory').create();
const dmDao = require("./cbDao/downloadMaterial");
var logger = global.logger || console;

module.exports.getDownloadMaterial = function(data, session, cb){
  logger.log("start get contact list");
  dmDao.getDownloadMaterial("08", session.agent.agentCode, function(pList){
    cb({
      success: true,
      materialList: pList
    })
  });
}

