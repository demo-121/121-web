var cDao = require("./cbDao/approval");
var agentDao = require("./cbDao/agent");
var bDao = require("./cbDao/bundle");
var applicationDao = require('./cbDao/application');
const dao = require('./cbDaoFactory').create();
const _ = require('lodash');
var forEach  = require('lodash/forEach');
var cloneDeep = require('lodash/cloneDeep');
var indexOf = require('lodash/indexOf');
var filter = require('lodash/filter');
var trim = require('lodash/trim');
var split = require('lodash/split');
var toUpper = require('lodash/toUpper');
var get = require('lodash/get');
var concat = require('lodash/concat');
const {formatDate, parseDatetime, dayDiff, truncDate} = require('../common/DateUtils');
var PDFHandler = require('./PDFHandler');
var clone = require('lodash/clone');
var cloneDeep = require('lodash/cloneDeep');
var logger = global.logger || console;
var moment = require('moment');
var _isArray = require('lodash/fp/isArray');
var _concat = require('lodash/fp/concat');

var async = require('async');
var _get = require('lodash/fp/get');
var _getOr = require('lodash/fp/getOr');
var _isEmpty = require('lodash/fp/isEmpty');
var _join = require('lodash/fp/join');

const approvalStatusModel = require('./model/approvalStatus');
const aNotifyHandler = require('./ApprovalNotificationHandler');

const FILTER_DATE_FORMAT = 'YYYY-MM-DD';

const ConfigConstant = require('../app/constants/ConfigConstants');

const {createPdfToken, setPdfTokensToRedis} = require('./utils/TokenUtils');
const appHandler = require('./handler/ApplicationHandler');
const fileHandler = require('./FileHandler');

const {getAttachmentList, transformInvalidFileName} = require('./EmailHandler');
var productUtils = require('../common/ProductUtils');
var { callApi } = require('./utils/RemoteUtils.js');

module.exports.searchApprovalCaseById = function(data, session, cb) {
    cDao.searchApprovalCaseById(data.id, function(c){
        cb(c);
    });
};

module.exports.searchApprovalCaseByIdAndLockCase = function(data, session, cb) {
    cDao.searchApprovalCaseByIdAndLockCase(data.id, session, function(c){
        cb(c);
    });
};

module.exports.unlockApprovalCaseById = function(data, session, cb) {
    cDao.unlockApprovalCaseById(data.id, session, function(c){
        cb(c);
    });
};

module.exports.downloadZippedSupportDocuments = (data, session, callback) => {
  const {agentCode, applicationId, isShield, policyId, approvalCaseId, proposalNumber, proposerName} = data.inputData;
  let appId = applicationId;
  let approvalId = isShield ? approvalCaseId : policyId;
  var body = {};
  body.password = agentCode;
  if (proposalNumber && proposerName){
    body.outputZipFileName = proposalNumber + '_' + proposerName + '.zip';
  }
  else {
    body.outputZipFileName = 'supportDocuments.zip';
  }

  applicationDao.getApplication(appId, (application) => {
    const quoId = _get('quotation.id', application);
    const fnaId = _get('bundleId', application);
    getAttachmentList(appId,approvalId).then((suppDocs) => {
      let promises = [];
      _.each(suppDocs, (suppDoc, docKey) => {
        if (docKey.indexOf('appPdf') > -1 && isShield && suppDoc) {
          let attachmentDetail = {};
          const shieldAppPdfName = transformInvalidFileName(suppDoc.title);
          attachmentDetail.docId = appId;
          attachmentDetail.attName = docKey;
          attachmentDetail.fileName = `${shieldAppPdfName}.pdf`;
          promises.push(attachmentDetail);

        } else if (docKey === 'appPdf') {
          let attachmentDetail = {};
          attachmentDetail.docId = appId;
          attachmentDetail.attName = docKey;
          attachmentDetail.fileName = 'eapp_form.pdf';
          promises.push(attachmentDetail);

        } else if (docKey === 'fnaReport') {
          let attachmentDetail = {};
          attachmentDetail.docId = fnaId;
          attachmentDetail.attName = docKey;
          attachmentDetail.fileName = 'fna_report.pdf';
          promises.push(attachmentDetail);

        } else if (docKey === 'proposal'  && isShield) {
          let attachmentDetail = {};
          attachmentDetail.docId = quoId;
          attachmentDetail.attName = docKey;
          attachmentDetail.fileName = 'AXA Shield Product Summary.pdf';
          promises.push(attachmentDetail);

        } else if (docKey === 'proposal') {
          let attachmentDetail = {};
          let pdfName = _getOr('policy_illustration.pdf', 'fileName', productUtils.getPILabelByQuotType(_get('quotation.quotType', application)));
          attachmentDetail.docId = quoId;
          attachmentDetail.attName = docKey;
          attachmentDetail.fileName = pdfName;
          promises.push(attachmentDetail);

        } else if (docKey === 'eapproval_supervisor_pdf') {
          let attachmentDetail = {};
          attachmentDetail.docId = approvalId;
          attachmentDetail.attName = docKey;
          attachmentDetail.fileName = 'Supervisor-Approval-Validation.pdf';
          promises.push(attachmentDetail);

        } else if (docKey === 'faFirm_Comment') {
          let attachmentDetail = {};
          attachmentDetail.docId = approvalId;
          attachmentDetail.attName = docKey;
          attachmentDetail.fileName = 'Comments-by-FA-Firm-Admin.pdf';
          promises.push(attachmentDetail);
        } else if (docKey === 'eCpdPdf' && _.get(application, '_attachments.eCpdPdf.length')) {
            let attachmentDetail = {};
            attachmentDetail.docId = appId;
            attachmentDetail.attName = docKey;
            attachmentDetail.fileName = 'e-CPD.pdf';
            promises.push(attachmentDetail);
        } else {
          let filename = (suppDocs[docKey] && suppDocs[docKey].filename) || docKey;
          let imgItems = [];
          _.each(suppDoc.items, (item, index) => {
            if (item.fileSize && item.fileType) {
              if (item.fileType.indexOf('pdf') > -1) {
                let attachmentDetail = {};
                attachmentDetail.docId = appId;
                attachmentDetail.attName = item.id;
                attachmentDetail.fileName = filename + '_' + (index + 1) + '.pdf';
                promises.push(attachmentDetail);
              } else if (item.fileType.indexOf('image') > -1) {
                imgItems.push(item);
                let attachmentDetail = {};
                attachmentDetail.docId = appId;
                attachmentDetail.attName = item.id;
                attachmentDetail.fileName = item.title;
                promises.push(attachmentDetail);
              }
            }
          });
        }
      });
      return Promise.all(promises);
    }).then((attachmentDetails) => {
      body.attachmentDetails = attachmentDetails;
      return new Promise(resolve => {
        resolve(body);
      });
    })
    .then((bodyPara) => {
      callApi('/approval/downloadZippedSupportDocuments',
        bodyPara,
        (res)=>{
          callback(res);
        });
    }).catch((error)=>{
      logger.error('Error in downloadZippedSupportDocuments->getAttachmentList: ', error);
    });
  });
};

module.exports.searchApprovalCaseByAppId = (data, session, cb) => {
    // const appId = _get('appId', data);
    const appId = data.appId;
    async.waterfall([
        (callback) => {
            applicationDao.getApplication(appId, (app) => {
                callback(null, _get('id', app));
            });
        },
        (policyNumber, callback) => {
            cDao.searchApprovalCaseById(policyNumber, (searchResult) => {
                callback(null, _get('foundCase', searchResult));
            });
        }
    ], (err, approvalCase) => {
        if (err) {
          logger.error("Error in searchApprovalCaseByAppId: ", err);
          cb({ success: false });
        } else {
          cb({
              success: true,
              approvalCase
          });
        }
    });
};

module.exports.geteApprovalByAppid = function(data, session, cb) {
    cDao.geteApprovalByAppid(data.id, function(c){
        cb(c);
    });
};

var populateToChildrenJson = function(subApprovalList, approvalCase) {
    logger.log('ApprovalHandler populateToChildrenJson starts');
    return new Promise((resolve) => {
        let promises = [];
        let success = true;
        let copyMasterCase = _.cloneDeep(approvalCase);
        const fieldsToOmit = ['_attachments', '_id', '_rev', 'applicationId', 'approvalCaseId', 'policiesMapping', 'subApprovalList', 'type'];

        copyMasterCase = _.omit(copyMasterCase, fieldsToOmit);

        _.forEach(subApprovalList, (subApprovalId) => {
            promises.push(new Promise((resolve2) => {
                dao.getDoc(subApprovalId, function(subApproval){
                    cDao.updateApprovalCaseById(subApprovalId, _.assign(subApproval, copyMasterCase), function(c){
                        if (!c.success) {
                            success = false;
                        }
                        resolve2();
                    });
                });
            }));
        });
        Promise.all(promises).then(() => {
            logger.log('ApprovalHandler populateToChildrenJson end with success -- ' + success);
            resolve();
        }).catch((error) => {
            logger.error('Error in ApprovalHandler populateToChildrenJson: ', error);
        });
    });
};

const _updateApprovalCaseById = function(data, session, cb) {
    let curTime  = new Date();
    data.approvalCase.lastEditedDate = curTime.toISOString();
    data.approvalCase.lastEditedBy = session.agent.agentCode;

    dao.getDoc(data.id, function(approvalDoc){
        if (_.get(approvalDoc, 'type') === 'masterApproval') {
            populateToChildrenJson(approvalDoc.subApprovalList, data.approvalCase).then(() => {
                cDao.updateApprovalCaseById(data.id, data.approvalCase, function(c){
                    cb(c);
                });
            });
        } else {
            cDao.updateApprovalCaseById(data.id, data.approvalCase, function(c){
                cb(c);
            });
        }
    });
};
module.exports.updateApprovalCaseById = _updateApprovalCaseById;

let _uploadAttachments = function(data, session, cb){
    let uploadArr = data.attachments;
    uploadAtt(0, uploadArr, data, data.rev,'eapproval_', cb);
};

module.exports.uploadAttachments = _uploadAttachments;

let _uploadCoeAttachments = function(data, session, cb){
  let uploadArr = data.attachments;
  uploadCoeAtt(0, uploadArr, data, data.rev,'eapproval_coe_', cb);
};

let uploadCoeAtt = function(index, uploadArr, data, rev, attachmentPrefix, cb){
    if (!attachmentPrefix){
      attachmentPrefix = 'eapproval_';
    }
    if (uploadArr.length > index){
        logger.log(`APPROVAL:: Upload Coe Attachment To Application:: ${data.applicationId}, Approval:: ${data.id}`);
        new Promise((resolve, reject)=>{
            if (rev === undefined) {
                applicationDao.getApplication(data.applicationId, function(app){
                  resolve(app._rev);
                });
            } else {
                resolve(rev);
            }
        }).then(searchRev => {
            let base64Str = uploadArr[index].imageUrl.split(',')[1];
            cDao.uploadAttachment(data.applicationId, attachmentPrefix + index, searchRev, base64Str, data.attachmentsType[index], (rev)=>{
              uploadCoeAtt(index + 1, uploadArr, data, rev, attachmentPrefix, cb);
            });
        }).catch((error)=>{
            logger.error('Error in uploadCoeAtt->new Promise: ', error);
        });
    } else {
        cDao.searchApprovalCaseById(data.id, function(c){
            cb(c);
        });
    }
};

let uploadAtt = function(index, uploadArr, data, rev, attachmentPrefix, cb){
    if (!attachmentPrefix){
      attachmentPrefix = 'eapproval_';
    }
    if (uploadArr.length > index){
        logger.log('APPROVAL:: Upload Approval Case Attachment');
        new Promise((resolve, reject)=>{
            if (rev === undefined) {
                cDao.searchApprovalCaseById(data.id, function(c){
                    resolve(c.foundCase._rev);
                });
            }else {
                resolve(rev);
            }
        }).then(searchRev => {
            let base64Str = uploadArr[index].imageUrl.split(',')[1];
            cDao.uploadAttachment(data.id, attachmentPrefix + index, searchRev, base64Str, data.attachmentsType[index], (rev)=>{
                uploadAtt(index + 1, uploadArr, data, rev, attachmentPrefix, cb);
            });
        }).catch((error)=>{
            logger.error("Error in uploadAtt->new Promise: ", error);
        });
    } else {
        cDao.searchApprovalCaseById(data.id, function(c){
            cb(c);
        });
    }
};

const _getAllPolicyIdsFromMasterApproval = (iCidMapping) => {
  let subApprovalList = [];
  _.each(iCidMapping, cidArray => {
    _.each(cidArray, policyObj => {
      if (policyObj && policyObj.policyNumber) {
        subApprovalList.push(policyObj.policyNumber);
      }
    });
  });
  return subApprovalList;
};

const _getMasterApprovalIdFromMasterApplicationId = (appplicationId) => {
  if (appplicationId) {
    return `SP${appplicationId.substring(2, 14)}`;
  } else {
    return '';
  }
}

module.exports.getMasterApprovalIdFromMasterApplicationId = _getMasterApprovalIdFromMasterApplicationId;

const _uploadAttachment = function(data, session, cb){
    new Promise((resolve)=>{
        cDao.searchApprovalCaseById(data.id, function(c){
            resolve(c.foundCase._rev);
        });
    }).then(searchRev => {
        dao.uploadAttachmentByBase64(data.id, data.fileId, searchRev, data.base64Str, 'application/pdf', function(res) {
            cb(res);
        });
    }).catch((error)=>{
        logger.error('Error in _uploadAttachment->new Promise: ', error);
    });
};
module.exports.uploadAttachment = _uploadAttachment;

module.exports.createApprovalCase = function(data, session, cb){
    /**
     *
     *  ids: [application.id],
     *  newApprovalCase
     */
    let newApprovalCase = {};
    let isShieldMasterApproval = _getOr(false, 'isShieldMaster', data);
    let isShield = _getOr(false, 'isShield', data);
    let currentTime = _get('currentTime', data);
    return new Promise((resolve, reject) =>{
        cDao.getAllDoc(data.ids, (results)=> {
            resolve([results.result[0], newApprovalCase]);
        });
    }).then(result =>{
        return agentDao.getManagerDirectorProfileByAgentCode(session.agent.compCode, session.agent.agentCode).then(profiles => {
            if (!_.isEmpty(profiles)) {
                let agent = _.get(profiles, 'agentProfile') || {};
                let manager = _.get(profiles, 'managerProfile') || {};
                let director = _.get(profiles, 'directorProfile') || {};
                result.push(
                    {
                        agent,
                        manager,
                        director
                    }
                );

                return result;
            } else {
                throw Error('CANNOT get agnet / manager / director in createApprovalCase');
            }
        });
    }).then(result=>{
        let today;
        today = currentTime || moment().toISOString();
        let markInfoStatus = ['A', 'PFAFA'];
        let application = result[0];
        let newApprovalCase = result[1];
        let agentProfiles = result[2];
        let aStatus = handleDirectorSubmitCase(session.agent, get(agentProfiles, 'agent.agentCode'), get(agentProfiles, 'director.agentCode'));

        newApprovalCase.agentId = get(agentProfiles, 'agent.agentCode');
        newApprovalCase.agentCode = get(session, 'agent.agentCode');
        newApprovalCase.compCode = get(session, 'agent.compCode');
        newApprovalCase.dealerGroup = get(session, 'agent.channel.code');
        newApprovalCase.agentProfileId = get(agentProfiles, 'agent.profileId');
        newApprovalCase.applicationId = application.id;

        if (isShieldMasterApproval) {
          newApprovalCase.approvalCaseId = _getMasterApprovalIdFromMasterApplicationId(application.id);
          newApprovalCase.subApprovalList = _getAllPolicyIdsFromMasterApproval(_.get(application, 'iCidMapping'));
          newApprovalCase.proposalNumber = newApprovalCase.subApprovalList.join(', ');
          newApprovalCase.isShield = true;
          newApprovalCase.type = 'masterApproval';
          newApprovalCase.policiesMapping = _.get(application, 'payment.premiumDetails');
          let pickedFields = ['covName', 'policyNumber', 'laName'];
          newApprovalCase.policiesMapping = _.map(newApprovalCase.policiesMapping, policyMappingObj => {
            return _.pick(policyMappingObj, pickedFields);
          });
        } else {
          newApprovalCase.approvalCaseId = application.policyNumber;
          newApprovalCase.proposalNumber = application.policyNumber;
          newApprovalCase.policyId = application.policyNumber;
          newApprovalCase.isShield = isShield;
          let parentId = _getOr(undefined, 'parentId', application);
          newApprovalCase.masterApprovalId = (parentId) ? _getMasterApprovalIdFromMasterApplicationId(parentId) : parentId;
          newApprovalCase.type = 'approval';
        }

        newApprovalCase.approveRejectDate = '';
        newApprovalCase.comment = [];
        newApprovalCase.compCode = get(agentProfiles, 'agent.compCode'); //TODO add to view
        newApprovalCase.onHoldReason = '';

        newApprovalCase.orginalManagerId = get(agentProfiles, 'agent.managerCode');

        newApprovalCase.customerId = get(application, 'applicationForm.values.proposer.personalInfo.cid');
        newApprovalCase.customerName = get(application, 'applicationForm.values.proposer.personalInfo.fullName');
        newApprovalCase.customerICNo = get(application, 'applicationForm.values.proposer.personalInfo.idCardNo');
        newApprovalCase.customerICType = get(application, 'applicationForm.values.proposer.personalInfo.idDocType');
        newApprovalCase.approvalStatus = aStatus;

        newApprovalCase.agentId = get(agentProfiles, 'agent.agentCode');
        newApprovalCase.agentName = get(agentProfiles, 'agent.name');
        newApprovalCase.agentEmail =  get(agentProfiles, 'agent.email');
        newApprovalCase.agentMobile = get(agentProfiles, 'agent.mobile');

        newApprovalCase.directorId = get(agentProfiles, 'director.agentCode');
        newApprovalCase.directorName = get(agentProfiles, 'director.name');
        newApprovalCase.directorEmail = get(agentProfiles, 'director.email');
        newApprovalCase.directorMobile = get(agentProfiles, 'director.mobile');

        newApprovalCase.managerId = get(agentProfiles, 'manager.agentCode');
        newApprovalCase.managerName = get(agentProfiles, 'manager.name');
        newApprovalCase.managerEmail = get(agentProfiles, 'manager.email');
        newApprovalCase.managerMobile = get(agentProfiles, 'manager.mobile');
        if (markInfoStatus.indexOf(aStatus) > -1) {
            newApprovalCase.approveRejectDate = today;
            newApprovalCase.approveRejectManagerId = session.agent.agentCode;
            newApprovalCase.approveRejectManagerName = session.agent.name;
            newApprovalCase.approveRejectManagerEmail = session.agent.email;
            newApprovalCase.approveRejectManagerMobile = session.agent.mobile;
        }

        if (session.agent.channel.type === 'FA'){
            newApprovalCase.faFirmName = session.agent.company;
            newApprovalCase.isFACase = true;
        } else {
          newApprovalCase.organizationName = get(agentProfiles, 'agent.company'); //TODO add to view
          newApprovalCase.isFACase = false;
        }
        newApprovalCase.lastEditedDate = today;
        newApprovalCase.lastEditedBy = get(agentProfiles, 'agent.id');
        newApprovalCase.lifeAssuredName = get(application, 'quotation.iFullName');
        if (_.get(application, 'quotation.quotType') === 'SHIELD') {
            let shieldProductName;
            shieldProductName = get(application, 'quotation.plans[0].covName.en') || get(application, 'quotation.baseProductName.en');
            newApprovalCase.productName  = shieldProductName + ' Plan';
            let multipleLangProductName = _.cloneDeep(get(application, 'quotation.plans[0].covName') || get(application, 'quotation.baseProductName'));
            multipleLangProductName.en = multipleLangProductName.en + ' Plan';
            newApprovalCase.multiLangProductName = multipleLangProductName;
        } else {
            newApprovalCase.productName = get(application, 'quotation.plans[0].covName.en');
            newApprovalCase.multiLangProductName = get(application, 'quotation.plans[0].covName');
        }
        newApprovalCase.proposerName = get(application, 'applicationForm.values.proposer.personalInfo.fullName');
        newApprovalCase.quotationId = get(application, 'quotation.id');
        newApprovalCase.submittedDate = today;
        return newApprovalCase;
    }).then(result => {
        cDao.createApprovalCase(result, (resp)=>{
            if (resp)
                cb(resp);
        });
    }).catch((error)=>{
        logger.error('Error in createApprovalCase->new Promise: ', error);
        cb({success: false});
    });
};

let handleDirectorSubmitCase = function(agentProfile, agentId, directorId) {
    let role = _identifyApproveLevel(agentProfile);
    if (agentId === directorId && role === approvalStatusModel.ONELEVEL_SUBMISSION) {
        return 'A';
    } else if(agentId === directorId && role === approvalStatusModel.FAAGENT) {
        return 'PFAFA';
    }else {
        return 'SUBMITTED';
    }
};

let _identifyApproveLevel = (agentProfile) =>{
  let channelType = _getOr('', 'channel.type', agentProfile );
  //let role = _getOr('', 'rawData.faAdvisorRole', agentProfile );
  const faAdminRole = _getOr('', 'rawData.userRole', agentProfile );
  if (channelType === 'FA' && faAdminRole === 'MM1') {
    return approvalStatusModel.FAADMIN;
  } else if (channelType === 'FA') {
    return approvalStatusModel.FAAGENT;
  } else {
    return approvalStatusModel.ONELEVEL_SUBMISSION;
  }
}
module.exports.identifyApproveLevel = _identifyApproveLevel;

let _getApproveStatusByRole = (role) =>{
  if (role === approvalStatusModel.FAADMIN) {
    return approvalStatusModel.APPRVOAL_STATUS_APPROVED;
  } else if (role === approvalStatusModel.FAAGENT) {
    return approvalStatusModel.APPRVOAL_STATUS_PFAFA;
  } else {
    return approvalStatusModel.APPRVOAL_STATUS_APPROVED;
  }
}
module.exports.getApproveStatusByRole = _getApproveStatusByRole;

const _genSupervisorPdf = function(data, session, cb){
    let promises =[];
    promises.push(
        new Promise(resolve =>{
            cDao.searchApprovalCaseById(data.id, function(c){
                resolve(c.foundCase);
            });
        })
    );
    promises.push(
        new Promise(resolve =>{
            resolve(cDao.getSupervisorTemplate(data.approverLevel));
        })
    );
    return Promise.all(promises).then(datas =>{
        PDFHandler.getSupervisorTemplatePdf(datas[0], datas[1], data.lang, function (pdf){
            cb({success: true, pdf});
        });
    }).catch((error)=>{
        logger.error("Error in _genSupervisorPdf->Promise.all: ", error);
    });
};

module.exports.genSupervisorPdf = _genSupervisorPdf

let handleSearchCasesStatusList = (agentProfile, workbenchStatus) => {
    const channelType = _getOr('', 'channel.type', agentProfile );
    const faAdminRole = _getOr('', 'rawData.userRole', agentProfile );
    if (workbenchStatus === true) {
        return ['SUBMITTED', 'PDoc', 'PDis', 'PFAFA', 'PDocFAF', 'PDisFAF', 'PCdaA', 'PDocCda', 'PDisCda', 'A', 'R', 'E'];
    } else if (channelType === 'FA' && faAdminRole === 'MM1') {
        return ['SUBMITTED', 'PDoc', 'PDis', 'PFAFA', 'PDocFAF', 'PDisFAF'];
    } else if (channelType === 'FA') {
        return ['SUBMITTED', 'PDoc', 'PDis', 'PDocFAF', 'PDisFAF'];
    } else {
        return ['SUBMITTED', 'PDoc', 'PDis'];
    }
}

module.exports.getPendingForApprovalCaseListLength = (data, session, cb) => {

    let statusList = handleSearchCasesStatusList(session.agent, false);
    async.waterfall([
        (callback) => {
            getApprovalCases(session.agent, statusList).then((approvals) => {
                callback(null, approvals);
            });
        },
        (approvals, callback) => {
            cDao.getApprovalPageTemplate(data, session, function(resp){
                callback(null, {
                    success: resp.success,
                    searchedApprovalCases: approvals,
                    reviewPageTemplate: resp.result,
                    length: approvals ? approvals.length : 0
                });
            });
        }
    ], (err, result) => {
        if (err) {
            logger.error('Failed to get no. of pending for approval cases', err);
            cb({ success: false });
        } else {
            cb(result);
        }
    });
};

module.exports.getDoc = (data, session, cb) =>{
    dao.getDoc(data.id, function(doc){
        cb({success: true, foundDoc: doc});
    });
};

module.exports.getAgentProfileId = (data, session, cb) => {
    let result = [];
    return new Promise((resolve, reject) => {
        dao.getViewRange(
            'main',
            'agents',
            '["'+ data.compCode + '","' + data.id + '"'+']',
            '["'+ data.compCode + '","' + data.id + '"'+']',
            null,
            function(pList){
            if(pList && pList.rows){
                for(var i=0; i<pList.rows.length; i++){
                    let valueObj = pList.rows[i].value;
                    valueObj.id = pList.rows[i].id;
                    result.push(valueObj);
                }
            }
            resolve(cb({success: true, result: result[0]}));
            }
        );

    }).catch((e) => {
        logger.error('ERROR:: getAgentProfileId', e);
        cb({success: false});
    });
};

module.exports.searchApprovalCases = function (data, session, cb) {
  let statusList = handleSearchCasesStatusList(session.agent, false);
  getApprovalCases(session.agent, statusList).then((approvals) => {
    let cases = _.filter(approvals, (approval) => {
      let selected = true;
      _.each(data.filter, (value, key) => {
        let searchTextValue = filter(split(toUpper(value), " "), text=>!_isEmpty(_.trim(text)));
        value = trim(value);
        if (value === '') {
          return;
        } else if (key !== 'approvalStatus') {
          _.forEach(searchTextValue, v=>{
            if(_.toUpper(approval[key]).indexOf(v) === -1 ){
              selected = false;
            }
          })
        } else if (key === 'approvalStatus') {
          let filterValues = split(value, ',');
          if (indexOf(filterValues, approval[key]) === -1 && indexOf(filterValues, 'all') === -1) {
            selected = false;
          }
        }
      });
      return selected;
    });
    cb({
      success: true,
      result: cases
    });
  }).catch((e) => {
    logger.error('Failed to get approval cases', e);
    cb({ success: false });
  });
};

let handleStartRange = (date) =>{
    if(_isEmpty(date)){
        date = new Date('1900-01-01');
    }
    date = handlePaddingZero(date);
    return Date.parse(date + 'T00:00:00.000Z');
}

let handleEndRange = (date) => {
    if (_isEmpty(date)) {
        date = moment().utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(FILTER_DATE_FORMAT);
    }
    date = handlePaddingZero(date);
    return Date.parse(date + 'T23:59:59.999Z');
}

let handlePaddingZero = (date) => {
  let dateArr = _.split(date, '-');
  if (dateArr && dateArr.length === 3) {
    forEach(dateArr, (value, index) => {
      if(value.length < 2) {
        dateArr[index] = '0' + value;
      }
    });
    return _join('-', dateArr);
  } else {
    return "";
  }
}

module.exports.searchWorkbenchCases = function (data, session, cb) {
  let statusList = handleSearchCasesStatusList(session.agent, true);
  const dateFilterMapping = {
      lastEdit: 'lastEditedDate',
      dateSubmitted: 'submittedDate',
      dateAppRej: 'approveRejectDate'
  }
  getInprogressBIandApplications(session.agent, (inProgressCases) => {
    getWorkbenchCases(inProgressCases, session.agent, statusList).then(approvals => {
        let cases = _.filter(approvals, (approval) => {
          let selected = true;
          let prefix;
          _.each(data.filter, (value, key) => {
            let fitleredKey =[];
            value = trim(value);
            let searchTextValue = filter(split(toUpper(value), " "), text=>!_isEmpty(_.trim(text)));
            if (value === '') {
                return;
            } else if (key.toUpperCase().indexOf('_START_DATE') > -1 || key.toUpperCase().indexOf('_END_DATE') > -1){
                let trimIndicator
                (key.toUpperCase().indexOf('_START_DATE') > -1 ) ? trimIndicator = '_START_DATE' : trimIndicator = '_END_DATE';
                prefix =  key.substr(0, key.indexOf(trimIndicator));

                if (fitleredKey.indexOf(prefix) === -1) {
                    let endDate = prefix + '_END_DATE';
                    let startDate = prefix + '_START_DATE';
                    let mappedField = dateFilterMapping[prefix];
                    let filterEndKey = handleEndRange(data.filter[endDate]);
                    let filterStartKey = handleStartRange(data.filter[startDate]);
                    let searchValue = undefined;
                    if (approval[mappedField]) {
                      searchValue = Date.parse(moment(approval[mappedField], moment.ISO_8601).utcOffset(ConfigConstant.MOMENT_TIME_ZONE));
                    }
                    if (_.isNaN(searchValue) || searchValue === undefined|| filterStartKey >  searchValue || searchValue > filterEndKey) {
                        selected = false;
                    }
                    fitleredKey.push(prefix);
                } else {
                    return;
                }
            } else if (key !== 'approvalStatus' && key.toUpperCase().indexOf('_DATE') === -1) {
              _.forEach(searchTextValue, v=>{
                if(_.toUpper(approval[key]).indexOf(v) === -1 ){
                  selected = false;
                }
              })
            } else if (key === 'approvalStatus') {
              let filterValues = split(value, ',');
              if (indexOf(filterValues, approval[key]) === -1 && indexOf(filterValues, 'all') === -1) {
                selected = false;
              }
            }
          });
          return selected;
        });
        cb({
          success: true,
          result: cases
        });
      }).catch((e) => {
        logger.error('Failed to get approval cases', e);
        cb({ success: false });
      });
  });
};


/**
 * Found out the downline / proxy agents
 * When user is FA Firm search all submitted cases in the same hierarchy
 * @param {*} compCode
 * @param {*} agentCode
 */
const getRelatedAgentsByAgentCode = (compCode, agentCode, isFAAdmin) => {
    if (!isFAAdmin) {
        return getNonFAFirmRelatedAgents(compCode, agentCode);
    } else {
        return getFAAdminRelatedAgents(compCode, agentCode);
    }
};

const getFAAdminRelatedAgents = (compCode, agentCode) => {
    return new Promise((resolve,reject) => {
        async.waterfall([
            (callback) => {
                let relatedAgentCode = [agentCode];
                agentDao.searchFAFirmDownline(compCode, relatedAgentCode, callback);
            }, (relatedAgentCodes, callback) => {
                // Get Agent Map in hierarchy
                agentDao.getAgentsByAgentCode(compCode, relatedAgentCodes).then((agentMap) => {
                    callback(null, agentMap);
                }).catch((error)=>{
                    logger.error(`Error in getRelatedAgentsByAgentCode->FAAdminRelatedAgents--> get agentMap in hierarchy: ${error}`);
                    callback(error);
                });
            }, (agentMap, callback) => {
                // Get related Proxy agents
                let profileIds = [];
                _.each(agentMap, agentProfile => {
                    if (agentProfile.isProxying && agentProfile.rawData) {
                        profileIds.push(agentProfile.rawData.proxy1UserId);
                        profileIds.push(agentProfile.rawData.proxy2UserId);
                    }
                });
                agentDao.getAgentsByUserId(compCode, profileIds).then((proxyMap) => {
                    callback(null, agentMap);
                }).catch((error)=>{
                    logger.error(`Error in getRelatedAgentsByAgentCode->FAAdminRelatedAgents--> get proxy agents: ${error}`);
                    callback(error);
                });
            }
        ], (err, agentMap) => {
            if (err) {
                logger.error('Error in getRelatedAgentsByAgentCode-->FAAdminRelatedAgents--> : ', err);
                reject({success: false});
            } else {
                resolve({success: true, agentMap});
            }
        });
    }).catch(error => {
        logger.error(`ERROR:: getRelatedAgentsByAgentCode-->FAAdminRelatedAgents--> : ${error}`);
    });
};

const getNonFAFirmRelatedAgents = (compCode, agentCode) => {
    return new Promise((resolve,reject) => {
        async.waterfall([
            (callback) => {
                let relatedAgentCode = [agentCode];
                agentDao.searchDirectorDownline(compCode, relatedAgentCode).then(directorDownlineAgentCodes => {
                    relatedAgentCode = _.union(relatedAgentCode, directorDownlineAgentCodes);
                    agentDao.recursiveSearchDownline(compCode, relatedAgentCode, relatedAgentCode, callback);
                });
            }, (relatedAgentCodes, callback) => {
                // Union Agent Code and manager Code
                agentDao.recursiveSearchUpline(compCode, relatedAgentCodes, relatedAgentCodes, callback);
            }, (relatedAgentCodes, callback) => {
                // Get Agent Map in hierarchy
                agentDao.getAgentsByAgentCode(compCode, relatedAgentCodes).then((agentMap) => {
                    callback(null, agentMap);
                });
            }, (agentMap, callback) => {
                agentDao.getProxyDownline(compCode, _.map(agentMap, agent => agent.profileId)).then(proxyDownlineAgentCodes => {
                    // let relatedAgents = _.union(_.map(agentMap, agent => agent.agentCode), proxyDownlineAgentCodes) || [];
                    proxyDownlineAgentCodes = _.uniq(proxyDownlineAgentCodes);
                    callback(null, {agentMap, proxyDownlineAgentCodes});
                });
            }, (result, callback) => {
                let {proxyDownlineAgentCodes, agentMap} = result;
                // Filter out the difference and do the downline search

                /** Handle Bug that the proxy has the same manager with the submitted agents */
                // let proxyRelatedAgentCodes = _.difference(proxyDownlineAgentCodes, _.map(agentMap, agent => agent.agentCode));
                let proxyRelatedAgentCodes = proxyDownlineAgentCodes;
                /** Handle Bug that the proxy has the same manager with the submitted agents */

                if (proxyRelatedAgentCodes.length) {
                    agentDao.recursiveSearchDownline(compCode, proxyRelatedAgentCodes, proxyRelatedAgentCodes, (error, proxyAgentCodes) => {
                        proxyRelatedAgentCodes = _.union(proxyRelatedAgentCodes, proxyAgentCodes);
                        (error) ? callback(error) : callback(null, {agentMap, proxyRelatedAgentCodes});
                    });
                } else {
                    callback(null, {agentMap, proxyRelatedAgentCodes});
                }
            }, (result, callback) => {
                let {proxyRelatedAgentCodes, agentMap} = result;
                // Dp upline search when there is proxy agents that not search before
                if (proxyRelatedAgentCodes.length) {
                    agentDao.recursiveSearchUpline(compCode, proxyRelatedAgentCodes, proxyRelatedAgentCodes, (error, proxyAgentCodes) => {
                        proxyRelatedAgentCodes = _.union(proxyRelatedAgentCodes, proxyAgentCodes);
                        (error) ? callback(error) : callback(null, {agentMap, proxyRelatedAgentCodes});
                    });
                } else {
                    callback(null, {agentMap, proxyRelatedAgentCodes});
                }
            },(result, callback) => {
                let {agentMap, proxyRelatedAgentCodes} = result;
                let relatedAgentCodes = _.union(_.map(agentMap, agent => agent.agentCode), proxyRelatedAgentCodes);
                agentDao.getAgentsByAgentCode(compCode, relatedAgentCodes).then((finalAgentMap) => {
                    callback(null, finalAgentMap);
                });
            }
            , (agentMap, callback) => {
                // Get related Proxy agents
                let profileIds = [];
                _.each(agentMap, agentProfile => {
                    if (agentProfile.isProxying && agentProfile.rawData) {
                        profileIds.push(agentProfile.rawData.proxy1UserId);
                        profileIds.push(agentProfile.rawData.proxy2UserId);
                    }
                });
                agentDao.getAgentsByUserId(compCode, profileIds).then((proxyMap) => {
                    callback(null, Object.assign(agentMap, proxyMap));
                }).catch((error)=>{
                    logger.error(`Error in getRelatedAgentsByAgentCode->getNonFAFirmRelatedAgents--> get proxy agents: ${error}`);
                    callback(error);
                });
            }
        ], (err, agentMap) => {
            if (err) {
                logger.error('Error in getRelatedAgentsByAgentCodegetNonFAFirmRelatedAgents--> : ', err);
                reject({success: false});
            } else {
                resolve({success: true, agentMap});
            }
        });
    }).catch(error => {
        logger.error(`ERROR:: getRelatedAgentsByAgentCodegetNonFAFirmRelatedAgents--> : ${error}`);
    });
};

const getRelatedAgents = (compCode, approvals) => {
  return new Promise((resolve, reject) => {
    let agentCodes = {};
    _.each(approvals, (approval) => {
      agentCodes[approval.agentId] = true;
    });
    agentDao.getAgents(compCode, _.map(agentCodes, (val, key) => key)).then((agentMap) => {
      let managerCodes = _.map(agentMap, agent => agent.managerCode);
      agentDao.getAgents(compCode, managerCodes).then((managerMap) => {
        _.each(managerMap, (manager) => {
          agentMap[manager.agentCode] = manager;
        });
        let directorCodes = _.map(managerMap, manager => manager.managerCode);
        agentDao.getAgents(compCode, directorCodes).then((directorMap) => {
          _.each(directorMap, (director) => {
            agentMap[director.agentCode] = director;
          });
          let proxyUserIds = [];
          _.each(managerMap, (manager) => {
            if (manager.isProxying && manager.rawData) {
              proxyUserIds.push(manager.rawData.proxy1UserId);
              proxyUserIds.push(manager.rawData.proxy2UserId);
            }
          });
          agentDao.getAgentsByUserId(compCode, proxyUserIds).then((proxyMap) => {
            _.each(proxyMap, (proxy) => {
              agentMap[proxy.rawData.userId] = proxy; // use userId as key for proxies
            });
            resolve(agentMap);
          }).catch((error)=>{
            logger.error("Error in getRelatedAgents->agentDao.getAgentsByUserId: ", error);
          });
        }).catch((error)=>{
            logger.error("Error in getRelatedAgents->agentDao.getAgents[3]: ", error);
        });
      }).catch((error)=>{
        logger.error("Error in getRelatedAgents->agentDao.getAgents[2]: ", error);
      });
    }).catch((error)=>{
        logger.error("Error in getRelatedAgents->agentDao.getAgents[1]: ", error);
    });
  });
};

const getAssignedManager = (approval, agent, manager, proxy1, proxy2) => {
  let assignedManager = null;
  if (manager && (!manager.isProxying || ((!proxy1 || proxy1.isProxying) && (!proxy2 || proxy2.isProxying)))) {
    assignedManager = manager;
  } else if (
    proxy1 && agent.agentCode !== proxy1.agentCode && !proxy1.isProxying && (
      (approval.approvalStatus === 'SUBMITTED' && (
        dayDiff(truncDate(parseDatetime(approval.submittedDate)), new Date()) < global.config.SECONDARY_PROXY_ASSIGNMENT_DAY ||
        !proxy2 || proxy2.isProxying
      )) ||
      (['PDoc', 'PDis'].indexOf(approval.approvalStatus) > -1 && approval.caseLockedManagerCodebyStatus === proxy1.agentCode) ||
      (approval.approvalStatus === 'PFAFA' && approval.approveRejectManagerId === proxy1.agentCode)
    )
  ) {
    approval.proxyManagerId = proxy1.agentCode;
    approval.proxyManagerName = proxy1.name;
    approval.proxyManagerEmail = proxy1.email;
    approval.proxyManagerMobile = proxy1.mobile;
    assignedManager = proxy1;
  } else if (
    proxy2 && agent.agentCode !== proxy2.agentCode && !proxy2.isProxying && (
      (approval.approvalStatus === 'SUBMITTED' && dayDiff(truncDate(parseDatetime(approval.submittedDate)), new Date()) >= global.config.SECONDARY_PROXY_ASSIGNMENT_DAY) ||
      (['PDoc', 'PDis'].indexOf(approval.approvalStatus) > -1 && approval.caseLockedManagerCodebyStatus === proxy2.agentCode) ||
      (proxy1.isProxying || agent.agentCode === proxy1.agentCode) ||
      (approval.approvalStatus === 'PFAFA' && approval.approveRejectManagerId === proxy2.agentCode)
    )
  ) {
    approval.proxyManagerId = proxy2.agentCode;
    approval.proxyManagerName = proxy2.name;
    approval.proxyManagerEmail = proxy2.email;
    approval.proxyManagerMobile = proxy2.mobile;
    assignedManager = proxy2;
  } else {
    // Default manager when no conditions fulfilled
    assignedManager = manager;
  }
  return assignedManager;
};

const gethierarchyAgentCode = (hierarchyOutput, searchingAgentCode, compCode, cb) => {
  return agentDao.searchDownline(compCode, searchingAgentCode).then(result => {
    if (_isArray(result) && result.length > 0) {

      let excludedSearchedAgents = _.filter(result, agentCode => {
        return hierarchyOutput.indexOf(agentCode) === -1;
      })
      hierarchyOutput = _concat(hierarchyOutput, result);
      gethierarchyAgentCode(hierarchyOutput, excludedSearchedAgents, compCode, cb);
    } else {
      let outpout = [];
      _.each(hierarchyOutput, value => {
        if (outpout.indexOf(value) === -1) {
          outpout.push(value);
        }
      })
      cb(outpout);
    }
  }).catch((error)=>{
    logger.error("Error in gethierarchyAgentCode->searchDownline: ", error);
  });
}

const returnViewKeysArray = (result, compCode) => {
  let viewKeysArray = [];
  _.each(result, agentCode => {
    viewKeysArray.push('["' + compCode + '","' + agentCode + '"]')
  });
  return viewKeysArray;
}

const getInprogressBIandApplications = (currentAgent, cb) => {
  let promises = [];
  let searchingAgentCode = [currentAgent.agentCode];
  let hierarchyOutput = [currentAgent.agentCode];
 return gethierarchyAgentCode(hierarchyOutput, searchingAgentCode, currentAgent.compCode, (result) => {
    let filterKeys = returnViewKeysArray(result, currentAgent.compCode);
    promises.push(cDao.getquotationByAgent(currentAgent.compCode, filterKeys));
    promises.push(cDao.getapplicationByAgent(currentAgent.compCode, filterKeys));
    promises.push(cDao.getValidBundleCaseByAgent(currentAgent.compCode, filterKeys));
    promises.push(cDao.getAgentsProfile(currentAgent.compCode, filterKeys));

    Promise.all(promises).then(datas =>{
      let quotationArr = datas[0];
      let applicationArr = datas[1];
      let validCaseNo = datas[2];
      let relatedAgentProfiles = datas[3];
      let result = [];
      let agentProfile;
      let managerProfile;
      _.each(quotationArr, (value, key) => {
        if (validCaseNo.indexOf(value.caseNo) > -1){
          value.inProgressBI = true;
          value.approvalStatus = 'inProgressBI';
          agentProfile = _getOr({}, value.agentId, relatedAgentProfiles );
          value.agentProfileId = agentProfile.profileId;
          value.customerName = value.proposerName;
          value.product = _getOr('', 'product.en', value);
          result.push(value);
        }
      });

      _.each(applicationArr, (value, key) => {
        if (validCaseNo.indexOf(value.applicationId) > -1){
          value.inProgressApp = true;
          value.approvalStatus = 'inProgressApp';

          value.customerName = value.proposerName;

          if(_.isEqual(_.get(value, 'agentId'), currentAgent.agentCode)) {
            value.canSubmit = true;
          } else {
            value.canSubmit = false;
          }

          value.product = _getOr('', 'product.en', value);
          value.productName = value.product;
          agentProfile = _getOr({}, value.agentId, relatedAgentProfiles );
          value.agentProfileId = agentProfile.profileId;
          value.organizationName = _getOr('', 'company', agentProfile );

          managerProfile = _getOr({}, agentProfile.managerCode, relatedAgentProfiles );

          value.managerName = managerProfile.agentName;

          result.push(value);
        }
      });

      cb(result);
  }).catch((error)=>{
    logger.error("Error in getInprogressBIandApplications->Promise.all: ", error);
  });
 })
}

const _commonChangesInSearchingCases = (approval, agent, manager, director, proxy1, proxy2, assignedManager, approvedRejectedManagerId, isFAAdmin, agentMap, currentAgent) => {
    const {name, company, channel = {}} = currentAgent;
    const showStampedInformationStatus = ['A', 'R', 'E', 'PFAFA', 'PDocFAF', 'PDisFAF'];
    const showProxywhenApprovedRejected = ['A', 'R'];
    //Search the stamped director if have
    if (isFAAdmin) {
        approval.searchFilter_directorName = name;
        approval.searchFilter_supervisorName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
        approval.managerName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
    } else if (showStampedInformationStatus.indexOf(get(approval, 'approvalStatus')) > -1 && approval.directorId) {
        approval.searchFilter_directorName = agentMap[approval.directorId] && agentMap[approval.directorId].name;
        approval.searchFilter_supervisorName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
        approval.managerName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
    } else if (showStampedInformationStatus.indexOf(get(approval, 'approvalStatus')) > -1) {
        approval.searchFilter_directorName = director && director.name;
        approval.searchFilter_supervisorName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
        approval.managerName = agentMap[approval.managerId] && agentMap[approval.managerId].name;
    } else {
        approval.searchFilter_directorName = director && director.name;
        approval.searchFilter_supervisorName = _.get(assignedManager, 'name');
        approval.managerName = _.get(manager, 'name');
        approval.managerId = _.get(manager, 'agentCode');
    }

    // Override the director name to fa firm name if it is fa channel
    if (channel.type === 'FA') {
        approval.searchFilter_directorName = company;
    }

    if (showProxywhenApprovedRejected.indexOf(get(approval, 'approvalStatus')) > -1 &&
        _getOr(approvedRejectedManagerId, 'managerId', approval) !== approvedRejectedManagerId &&
        _getOr(approvedRejectedManagerId, 'directorId', approval) !== approvedRejectedManagerId) {
        approval.proxyManagerId = approval.approveRejectManagerId;
        approval.proxyManagerName = approval.approveRejectManagerName;
    }
}

const getApprovalCases = (currentAgent, statusList) => {
  const {compCode, name} = currentAgent;
  const FAAdminPApprovalStatus = ['PFAFA', 'PDocFAF','PDisFAF'];
  const isAgencyDirector = currentAgent.channel.type === 'AGENCY' && currentAgent.role === 'MM1';
  const faAdminRole = _.get(currentAgent, 'rawData.userRole');
  const isFAAdmin = currentAgent.channel.type === 'FA' && faAdminRole === 'MM1';
  return getRelatedAgentsByAgentCode(compCode, currentAgent.agentCode, isFAAdmin).then(result => {
    if (result.success && result.agentMap) {
        let agentMap = result.agentMap;

        return cDao.getApprovalCasesByAgentCode(compCode, _.uniq(_.map(agentMap, obj => obj.agentCode)), statusList).then(approvals => {
            return _.filter(approvals, (approval) => {
                let agent = agentMap[approval.agentId];
                let manager = agent && agentMap[agent.managerCode];
                let director = (agent && agent.rawData && agent.rawData.upline2Code && agentMap[agent.rawData.upline2Code] || manager && agentMap[manager.managerCode]);
                let proxy1 = manager && manager.rawData && agentMap[manager.rawData.proxy1UserId];
                let proxy2 = manager && manager.rawData && agentMap[manager.rawData.proxy2UserId];
                let assignedManager = getAssignedManager(approval, agent, manager, proxy1, proxy2);
                let approvedRejectedManagerId = _getOr('', 'approveRejectManagerId', approval);
                _commonChangesInSearchingCases(approval, agent, manager, director, proxy1, proxy2, assignedManager, approvedRejectedManagerId, isFAAdmin, agentMap, currentAgent);
                return (assignedManager && assignedManager.agentCode === currentAgent.agentCode) ||
                    (isAgencyDirector && (agent && agent.rawData && agent.rawData.upline2Code || manager && manager.managerCode) === currentAgent.agentCode) ||
                    (isFAAdmin && _.get(agent, 'rawData.upline2Code') === currentAgent.agentCode && FAAdminPApprovalStatus.indexOf(approval.approvalStatus) > -1);
                });
            });
    } else {
        logger.info(`INFO:: agentMap count: ${result.agentMap && result.agentMap.length}`);
        return [];
    }
  });
//   return cDao.getApprovalCases(compCode, statusList).then((approvals) => {

//     return getRelatedAgents(compCode, approvals).then((agentMap) => {
//       return _.filter(approvals, (approval) => {
//         let agent = agentMap[approval.agentId];
//         let manager = agent && agentMap[agent.managerCode];
//         let director = manager && agentMap[manager.managerCode];
//         let proxy1 = manager && manager.rawData && agentMap[manager.rawData.proxy1UserId];
//         let proxy2 = manager && manager.rawData && agentMap[manager.rawData.proxy2UserId];
//         let assignedManager = getAssignedManager(approval, agent, manager, proxy1, proxy2);
//         let approvedRejectedManagerId = _getOr('', 'approveRejectManagerId', approval);

//         _commonChangesInSearchingCases(approval, agent, manager, director, proxy1, proxy2, assignedManager, approvedRejectedManagerId, isFAAdmin, agentMap, currentAgent);

//         return (assignedManager && assignedManager.agentCode === currentAgent.agentCode) ||
//           (isAgencyDirector && manager && manager.managerCode === currentAgent.agentCode) ||
//           (isFAAdmin && _.get(agent, 'rawData.upline2Code') === currentAgent.agentCode && FAAdminPApprovalStatus.indexOf(approval.approvalStatus) > -1);
//       });
//     }).catch((error)=>{
//         logger.error("Error in getApprovalCases->getRelatedAgents: ", error);
//     });
//   }).catch((error)=>{
//     logger.error("Error in getApprovalCases->cDao.getApprovalCases: ", error);
//   });
};

const getWorkbenchCases = (inProgressCases, currentAgent, statusList) => {
  const {compCode, name} = currentAgent;
  const FAAdminWBStatus = ['PFAFA', 'PDocFAF','PDisFAF'];
  const showStampedInformationStatus = ['A', 'R', 'E', 'PFAFA'];
  const showProxywhenApprovedRejected = ['A', 'R'];
  const isAgencyDirector = currentAgent.channel.type === 'AGENCY' && currentAgent.role === 'MM1';
  const faAdminRole = _.get(currentAgent, 'rawData.userRole');
  const isFAAdmin = currentAgent.channel.type === 'FA' && faAdminRole === 'MM1';

  return getRelatedAgentsByAgentCode(compCode, currentAgent.agentCode, isFAAdmin).then(result => {
    if (result.success && result.agentMap) {
        let agentMap = result.agentMap;

        return cDao.getApprovalCasesByAgentCode(compCode, _.uniq(_.map(agentMap, obj => obj.agentCode)), statusList).then(approvals => {
            approvals = _.concat(approvals, inProgressCases);
            return _.filter(approvals, (approval) => {
                let agent = agentMap[approval.agentId];
                let manager = agent && agentMap[agent.managerCode];
                let director = (agent && agent.rawData && agent.rawData.upline2Code && agentMap[agent.rawData.upline2Code] || manager && agentMap[manager.managerCode]);
                let proxy1 = manager && manager.rawData && agentMap[manager.rawData.proxy1UserId];
                let proxy2 = manager && manager.rawData && agentMap[manager.rawData.proxy2UserId];
                let assignedManager = getAssignedManager(approval, agent, manager, proxy1, proxy2);
                let approvedRejectedManagerId = _getOr('', 'approveRejectManagerId', approval);

                _commonChangesInSearchingCases(approval, agent, manager, director, proxy1, proxy2, assignedManager, approvedRejectedManagerId, isFAAdmin, agentMap, currentAgent);

                //Add the approver to getWorkbench cases result
                if (_get('channel.type', currentAgent) === 'FA' && FAAdminWBStatus.indexOf(approval.approvalStatus) > -1) {
                  approval.approver = [_.get(assignedManager, 'agentCode'), _.get(agent, 'rawData.upline2Code')]
                } else if(_get('channel.type', currentAgent) === 'FA') {
                  //Assigned Manager and FA Firm are the approver
                  approval.approver = [_.get(assignedManager, 'agentCode')]
                }  else {
                  //Assigned Manager and Director are the approver
                  approval.approver = [_.get(assignedManager, 'agentCode'), _.get(director, 'agentCode')]
                }

                return (assignedManager && assignedManager.agentCode === currentAgent.agentCode) ||
                    (isAgencyDirector && (agent && agent.rawData && agent.rawData.upline2Code || manager && manager.managerCode) === currentAgent.agentCode) ||
                    (isFAAdmin && _.get(agent, 'rawData.upline2Code') === currentAgent.agentCode) ||
                    (approval.agentId === currentAgent.agentCode) ||
                    (manager && manager.agentCode === currentAgent.agentCode);
            });
        });
    } else {
        logger.info(`INFO:: agentMap count: ${result.agentMap && result.agentMap.length}`);
        return [];
    }
  });
};

/**
 * Checks if the current agent can approve the given approval case.
 *
 * @param {*} currentAgent the agent from session
 * @param {*} approval the approval case
 */
const canApproveCase = (currentAgent, approval) => {
  approval = Object.assign({}, approval);
  return getRelatedAgents(currentAgent.compCode, [approval]).then((agentMap) => {
    let agent = agentMap[approval.agentId];
    let manager = agent && agentMap[agent.managerCode];
    let proxy1 = manager && manager.rawData && agentMap[manager.rawData.proxy1UserId];
    let proxy2 = manager && manager.rawData && agentMap[manager.rawData.proxy2UserId];
    let assignedManager = getAssignedManager(approval, agent, manager, proxy1, proxy2);
    if (['SUBMITTED', 'PDoc', 'PDis'].indexOf(approval.approvalStatus) > -1) {
      let isAgencyDirector = currentAgent.channel.type === 'AGENCY' && currentAgent.role === 'MM1';
      return (
        (assignedManager && (currentAgent.agentCode === assignedManager.agentCode)) ||
        (isAgencyDirector && manager && manager.managerCode === currentAgent.agentCode)
      );
    } else if (['PFAFA', 'PDocFAF', 'PDisFAF'].indexOf(approval.approvalStatus) > -1) {
      let isFAAdmin = currentAgent.channel.type === 'FA' && currentAgent.rawData.userRole === 'MM1';
      return isFAAdmin && currentAgent.agentCode === agent.rawData.upline2Code;
    }
    return false;
  }).catch((error)=>{
    logger.error("Error in canApproveCase->getRelatedAgents: ", error);
  });
};
module.exports.canApproveCase = canApproveCase;

module.exports.approveCase = (data, session, cb) => {
  logger.log('APPROVAL:: Approve Case Start');
    let approverLevel = _identifyApproveLevel(session.agent);
    let tempAppCase = data.approvalCase;
    let attachments = cloneDeep(data.approveChangedValues.jfwFile);
    let coeAttachments = [];
    if (data.approveChangedValues && data.approveChangedValues.coeFile && data.approveChangedValues.coeFile.length > 0){
        coeAttachments = cloneDeep(data.approveChangedValues.coeFile);
    }
    let jfwFileName = [];
    let coeFileName = [];
    let tempObj = {};
    let attachmentsType = [];
    let coeAttachmentsType = [];

    forEach(data.jfwFileProperties, (obj, index) =>{
        tempObj.fileName = obj.name;
        tempObj.type = obj.type;
        tempObj.attId = 'eapproval_' + index;
        jfwFileName.push(cloneDeep(tempObj));
        attachmentsType.push(obj.type);
    });
    if (data.coeFileProperties) {
      let uploadDate = moment().toISOString();
      forEach(data.coeFileProperties, (obj, index) => {
        tempObj = obj;
        tempObj.id = 'eapproval_coe_' + index;
        tempObj.uploadDate = uploadDate;
        coeFileName.push(cloneDeep(tempObj));
        coeAttachmentsType.push(obj.fileType);
      });
    }

    return agentDao.getManagerDirectorProfileByAgentCode(tempAppCase.compCode, tempAppCase.agentId).then(profiles => {
      logger.log('APPROVAL:: SUCCESSFULLY GET Director Profile');
      return new Promise(resolve => {
        let tempAppCase = data.approvalCase;
        tempAppCase.approvalStatus = _getApproveStatusByRole(approverLevel);
        tempAppCase.onHoldReason = '';
        let curTime = new Date();
        let enablePOSEmail = approverLevel === approvalStatusModel.FAADMIN || approverLevel === approvalStatusModel.ONELEVEL_SUBMISSION;
        if (approverLevel !== approvalStatusModel.FAADMIN) {
            tempAppCase.directorId = _getOr('', 'directorProfile.agentCode', profiles);
            tempAppCase.directorName = _getOr('', 'directorProfile.name', profiles);
            tempAppCase.directorEmail = _getOr('', 'directorProfile.email', profiles);
            tempAppCase.directorMobile = _getOr('', 'directorProfile.mobile', profiles);

            tempAppCase.managerId = _getOr('', 'managerProfile.agentCode', profiles);
            tempAppCase.managerName = _getOr('', 'managerProfile.name', profiles);
            tempAppCase.managerEmail = _getOr('', 'managerProfile.email', profiles);
            tempAppCase.managerMobile = _getOr('', 'managerProfile.mobile', profiles);
            tempAppCase.isProxyApproved = _getOr(session.agent.agentCode, 'managerProfile.agentCode', profiles) !== session.agent.agentCode &&
            _getOr(session.agent.agentCode, 'directorProfile.agentCode', profiles) !== session.agent.agentCode;
        }
        //Handle FA Admin Approval
        if (approverLevel === approvalStatusModel.FAADMIN) {
          logger.log('APPROVAL:: FA Admin Approve Case');
            //tempAppCase.supervisorApproveRejectDate = clone(tempAppCase.approveRejectDate);
            tempAppCase.approveRejectDate = curTime.toISOString();
            tempAppCase.approver_FAAdminCode = session.agent.agentCode;
            tempAppCase.accept_FAAdmin = data.approveChangedValues;
        } else if (approverLevel === approvalStatusModel.FAAGENT) {
          logger.log('APPROVAL:: FA Channel Agent Approve Case');
            tempAppCase.supervisorApproveRejectDate = curTime.toISOString();
            tempAppCase.approver_FAAdminCode = "";
            tempAppCase.accept = data.approveChangedValues;
            tempAppCase.approveRejectManagerId = session.agent.agentCode;
            tempAppCase.approveRejectManagerName = session.agent.name;
            tempAppCase.approveRejectManagerEmail = session.agent.email;
            tempAppCase.approveRejectManagerMobile = session.agent.mobile;
        }else {
          logger.log('APPROVAL:: Agency Channel Approve Case');
          tempAppCase.approveRejectDate = curTime.toISOString();
          tempAppCase.accept = data.approveChangedValues;
          tempAppCase.approveRejectManagerId = session.agent.agentCode;
          tempAppCase.approveRejectManagerName = session.agent.name;
          tempAppCase.approveRejectManagerEmail = session.agent.email;
          tempAppCase.approveRejectManagerMobile = session.agent.mobile;
          if(tempAppCase.accept){
            tempAppCase.accept.jfwFile = undefined;
            tempAppCase.accept.coeFile = undefined;
          }
          tempAppCase.jfwFileName = jfwFileName;
          if (tempAppCase.accept && tempAppCase.accept.callDate !== undefined) {
            let isoStringCallDate = new Date(tempAppCase.accept.callDate);
            tempAppCase.accept.callDate = moment(isoStringCallDate.toISOString()).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
          }
        }

        tempAppCase.approvalCaseHasIndividual = data.approvalCaseHasIndividual;

        _updateApprovalCaseById({id: tempAppCase.approvalCaseId, approvalCase: tempAppCase}, session, (resp) => {
            resolve(resp);
        });
      })
    }).then(resp => {
      logger.log('APPROVAL:: Generate Approve supervisor validation PDF');
        let caseId = resp.updatedCase.approvalCaseId;
        return new Promise(resolve => {
            _genSupervisorPdf({
                id: caseId,
                lang: 'en',
                approverLevel
            },
            session, (resp) =>{
                if (resp.success && resp.pdf) {
                    logger.log('APPROVAL:: Start to upload Approve supervisor validation PDF');
                    let pdfName;
                    (approverLevel === approvalStatusModel.FAADMIN) ? pdfName = 'faFirm_Comment' : pdfName = 'eapproval_supervisor_pdf';
                    _uploadAttachment({
                        id: caseId,
                        base64Str: resp.pdf,
                        fileId: pdfName
                    }, session, () => {
                        resolve();
                    });
                } else {
                    logger.error('APPROVAL:: Fail to generate APPROVAL supervisor validation pdf (no success response / no base64 string');
                }
            })
        })
    }).then(() => {
      logger.log('APPROVAL:: Upload Approve Process Attachments');
        if (attachments && attachments.length > 0) {
            return new Promise(resolve => {
                _uploadAttachments({
                    id: tempAppCase.approvalCaseId,
                    attachments,
                    attachmentsType
                }, session, () => resolve())
            })
        }else {
            return;
        }
    }).then(() => {
        logger.log('APPROVAL:: Upload Approve Process Coe Attachments');
        if (coeAttachments && coeAttachments.length > 0) {
            return new Promise(resolve => {
              _uploadCoeAttachments({
                    id: tempAppCase.approvalCaseId,
                    applicationId: tempAppCase.applicationId,
                    attachments: coeAttachments,
                    attachmentsType: coeAttachmentsType
                }, session, () => resolve());
            });
        } else {
            return;
        }
    }).then(() => {
        if (coeAttachments && coeAttachments.length > 0) {
            return new Promise(resolve => {
              let submitSuppDocsData = {
                pCertOfEndorsement: coeFileName,
                appId:tempAppCase.applicationId,
                isSupervisorChannel: false,
                isDocsFromApproval:true,
                uploadAtt:'coe'
              };
              appHandler.submitSuppDocsFiles(submitSuppDocsData, session, (res)=>{
                resolve();
              });
            });
        } else {
            return;
        }
    }).then(() => {
        return new Promise(resolve =>{
            dao.getDoc(tempAppCase.approvalCaseId, function(doc){
                resolve(doc);
            });
        })
    }).then((doc) => {
      logger.log('APPROVAL:: Send Approve Notification');
        aNotifyHandler.ApproveNotification({approverLevel, id: data.approvalCase.approvalCaseId}, session);
        cb({success: true, approvedCaase: doc});
    }).catch( e => {
        logger.error('Approve Process Error: ' + e);
        cb({success: false});
    })
};

module.exports.rejectCase = (data, session, cb) => {
  logger.log('APPROVAL:: reject case start');
    let agentProfile = session.agent;
    let approverLevel = _identifyApproveLevel(agentProfile);
    let tempAppCase = data.approvalCase;
    return agentDao.getManagerDirectorProfileByAgentCode(tempAppCase.compCode, tempAppCase.agentId).then(profiles => {
      logger.log('APPROVAL:: SUCCESSFULLY GET Director Profile');
      return new Promise(resolve => {
        tempAppCase.approvalStatus = approvalStatusModel.APPRVOAL_STATUS_REJECTED;
        tempAppCase.onHoldReason = '';
        let curTime = new Date();
        if (approverLevel !== approvalStatusModel.FAADMIN) {
            tempAppCase.directorId = _getOr('', 'directorProfile.agentCode', profiles);
            tempAppCase.directorName = _getOr('', 'directorProfile.name', profiles);
            tempAppCase.directorEmail = _getOr('', 'directorProfile.email', profiles);
            tempAppCase.directorMobile = _getOr('', 'directorProfile.mobile', profiles);

            tempAppCase.managerId = _getOr('', 'managerProfile.agentCode', profiles);
            tempAppCase.managerName = _getOr('', 'managerProfile.name', profiles);
            tempAppCase.managerEmail = _getOr('', 'managerProfile.email', profiles);
            tempAppCase.managerMobile = _getOr('', 'managerProfile.mobile', profiles);
            tempAppCase.isProxyApproved = _getOr(agentProfile.agentCode, 'managerProfile.agentCode', profiles) !== agentProfile.agentCode &&
            _getOr(agentProfile.agentCode, 'directorProfile.agentCode', profiles) !== agentProfile.agentCode;
        }
        //Handle FA Admin Rejection
        if (approverLevel === approvalStatusModel.FAADMIN) {
          logger.log('APPROVAL:: FA Admin Reject Case');
            //tempAppCase.supervisorApproveRejectDate = clone(tempAppCase.approveRejectDate);;
            tempAppCase.approveRejectDate = curTime.toISOString();
            tempAppCase.approver_FAAdminCode = agentProfile.agentCode;
            tempAppCase.reject_FAAdmin = data.rejectChangedValues
        } else if(approverLevel === approvalStatusModel.FAAGENT) {
          logger.log('APPROVAL:: FA Channel Agent reject case');
            tempAppCase.supervisorApproveRejectDate = curTime.toISOString();
            tempAppCase.approveRejectDate = curTime.toISOString();
            tempAppCase.approver_FAAdminCode = "";
            tempAppCase.reject = data.rejectChangedValues;
            tempAppCase.approveRejectManagerId = agentProfile.agentCode;
            tempAppCase.approveRejectManagerName = agentProfile.name;
            tempAppCase.approveRejectManagerEmail = agentProfile.email;
            tempAppCase.approveRejectManagerMobile = agentProfile.mobile;
        }else {
          logger.log('APPROVAL:: Agency Channel Reject Case');
            tempAppCase.approveRejectDate = curTime.toISOString();
            tempAppCase.reject = data.rejectChangedValues;
            tempAppCase.approveRejectManagerId = agentProfile.agentCode;
            tempAppCase.approveRejectManagerName = agentProfile.name;
            tempAppCase.approveRejectManagerEmail = agentProfile.email;
            tempAppCase.approveRejectManagerMobile = agentProfile.mobile;
        }
        tempAppCase.approvalCaseHasIndividual = data.approvalCaseHasIndividual;
        _updateApprovalCaseById({id: tempAppCase.approvalCaseId, approvalCase: tempAppCase}, session, (resp) => {
            resolve(resp);
        });
      })
    }).then(resp => {
      logger.log('APPROVAL:: Generate Reject supervisor validation pdf');
        let caseId = resp.updatedCase.approvalCaseId;
        return new Promise(resolve => {
            _genSupervisorPdf({
                id: caseId,
                lang: 'en',
                approverLevel
            },
            session, (resp) =>{
                if (resp.success && resp.pdf) {
                    logger.log('APPROVAL:: Start to upload reject supervisor validation PDF');
                    let pdfName;
                    (approverLevel === approvalStatusModel.FAADMIN) ? pdfName = 'faFirm_Comment' : pdfName = 'eapproval_supervisor_pdf';
                    _uploadAttachment({
                        id: caseId,
                        base64Str: resp.pdf,
                        fileId: pdfName
                    }, session, () => {
                        resolve();
                    });
                } else {
                    logger.error('APPROVAL:: Fail to generate REJECT supervisor validation pdf (no success response / no base64 string');
                }
            })
        })
    }).then(() => {
        return new Promise(resolve =>{
            dao.getDoc(tempAppCase.approvalCaseId, function(doc){
                resolve(doc);
            });
        })
    }).then((doc) => {
      logger.log('APPROVAL:: Start to send reject Notification');
        aNotifyHandler.RejectNotification({approverLevel, id: data.approvalCase.approvalCaseId}, session);
        cb({success: true, rejectCase: doc});
    }).catch(e => {
        logger.error('Reject Case: ' + e);
        cb({success: false});
    })
}

module.exports.saveComment = function(data, session, cb){
  logger.log('APPROVAL:: save comment start');
    new Promise(resolve => {
        dao.getDoc(data.id, function(doc){
           resolve(doc);
        });
    }).then((doc) =>{
        return new Promise(resolve => {
            let tempAppCase = doc;
            let agentProfile = session.agent;
            let curTime = new Date();
            let currentTime = curTime.toISOString();
            let tempComment = get(tempAppCase, 'comment') || [];
            let commentAdded = [{
                'content': data.comment,
                'author': agentProfile.name,
                'authorId': agentProfile.agentCode,
                'authorPid': agentProfile.profileId,
                'createTime': currentTime,
                'editedTime': currentTime,
            }];
            tempAppCase.onHoldReason = data.comment;
            tempAppCase.comment = concat(commentAdded,tempComment);
            _updateApprovalCaseById({id: tempAppCase.approvalCaseId, approvalCase: tempAppCase}, session, (resp) => {
                resolve(cb({success: true, updatedCase: resp.updatedCase}));
            });
        })
    }).catch(e =>{
        logger.error('Save Comment Error: ' + e);
        cb({success: false})
    })
}

module.exports.editComment = function(data, session, cb){
  logger.log('APPROVAL:: edit comment start');
    new Promise(resolve => {
        dao.getDoc(data.id, function(doc){
           resolve(doc);
        });
    }).then((doc) =>{
        let tempAppCase = doc;
        return new Promise(resolve => {
            let tempComment = get(tempAppCase, 'comment');
            if (data.index === 0) {
                tempAppCase.onHoldReason = data.comment;
            }
            tempComment[data.index].content = data.comment;
            let curTime = new Date();
            tempComment[data.index].editedTime = curTime.toISOString();

            _updateApprovalCaseById({id: tempAppCase.approvalCaseId, approvalCase: tempAppCase}, session, (resp) => {
                resolve(cb({success: true, updatedCase: resp.updatedCase}));
            });
        })
    }).catch(e =>{
        logger.error('Save Comment Error: ' + e);
        cb({success: false})
    })
}

module.exports.deleteComment = function(data, session, cb){
  logger.log('APPROVAL:: delete comment start');
    new Promise(resolve => {
        dao.getDoc(data.id, function(doc){
           resolve(doc);
        });
    }).then((doc) =>{
        let tempAppCase = doc;
        return new Promise(resolve => {
            let tempComment = get(tempAppCase, 'comment');

            if (data.index === 0) {
                tempAppCase.onHoldReason = _.get(tempComment, '[1].content', '');
            }

            tempAppCase.comment = filter(tempComment, (obj, index) => {return index !== data.index});

            _updateApprovalCaseById({id: tempAppCase.approvalCaseId, approvalCase: tempAppCase}, session, (resp) => {
                resolve(cb({success: true, updatedCase: resp.updatedCase}));
            });
        })
    }).catch(e =>{
        logger.error('Save Comment Error: ' + e);
        cb({success: false})
    })
}

let _getRelatedAgentsWithRoleAssigned = (compCode, agentCode, approval) =>{
    let rolesObj = {};
    return new Promise(resolve =>{
        agentDao.geAllAgents(compCode).then((agentMap) => {
            rolesObj.agent = agentMap[agentCode];
            rolesObj.manager = rolesObj.agent && agentMap[rolesObj.agent.managerCode];
            rolesObj.director = (rolesObj.agent && rolesObj.agent.rawData && rolesObj.agent.rawData.upline2Code && agentMap[rolesObj.agent.rawData.upline2Code] || rolesObj.manager && agentMap[rolesObj.manager.managerCode]);
            rolesObj.proxy1 = rolesObj.manager && rolesObj.manager.rawData && agentMap[rolesObj.manager.rawData.proxy1UserId];
            rolesObj.proxy2 = rolesObj.manager && rolesObj.manager.rawData && agentMap[rolesObj.manager.rawData.proxy2UserId];
            if (approval.approveRejectManagerId) {
              rolesObj.assignedManager = agentMap[approval.approveRejectManagerId];
            } else {
              rolesObj.assignedManager = getAssignedManager(approval, rolesObj.agent, rolesObj.manager, rolesObj.proxy1, rolesObj.proxy2);
            }
            resolve(rolesObj);
        });
    }).catch(e => {
        logger.error('Get Role Assigned: ' + e);
    });
};
module.exports.getRelatedAgentsWithRoleAssigned = _getRelatedAgentsWithRoleAssigned;

module.exports.viewJFWFiles = function(data, session, cb){
    let now = Date.now();
    let signDocConfig =  {
      // pdfUrl: '',
      attUrl: global.config.signdoc.getPdf,
      postUrl: global.config.signdoc.postUrl,
      resultUrl: global.config.signdoc.resultUrl,
      dmsId: global.config.signdoc.dmsid,
      docid: data.attId,
      auth: appHandler.generateSignDocAuth(data.attId, now),
      docts: now
    };
    fileHandler.getAttachment(data, session, (resp={}) => {
        let tokens = [createPdfToken(data.docId, data.attId, now, session.loginToken)];
        setPdfTokensToRedis(tokens, () => {
          logger.log('INFO: View JFW Files', data.docId);
          cb({
            success: true,
            token: tokens[0].token,
            signDocConfig: signDocConfig,
            data: resp.data
          });
        });
    })
}
