
const _ = require('lodash');
const _getOr = require('lodash/fp/getOr');
const moment = require('moment');
const logger = global.logger || console;

var async = require('async');

const commonApp = require('../common');
const dao = require('../../../cbDaoFactory').create();
const _application = require('./application');
const appDao = require('../../../cbDao/application');

const {
  callApiComplete
} = require('../../../utils/RemoteUtils');

const _removeInitPayMethodOptions = (templateItems, removeOptions) => {
  _.each(templateItems, (templateObj) => {
    if (templateObj.items) {
      _removeInitPayMethodOptions(templateObj.items, removeOptions);
    } else if (templateObj.options){
      templateObj.options = _.filter(templateObj.options, (option) => {
        return removeOptions.indexOf(option.value) < 0;
      });
    }
  });
}

var _getPaymentTemplate = function(frozen, initPayment, isFaChannel) {
  let result = {
    success: false,
    template: {}
  };

  return new Promise((resolve, reject) => {
    dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.PAYMENT_SHIELD, (tmpl) => {
      if (tmpl && !tmpl.error) {
        let removeOptions = [];
        if (initPayment >= 5000) {
          removeOptions.push('cash');
        }
        if (!isFaChannel) {
          removeOptions.push('payLater');
        }
        if (tmpl.items && removeOptions.length > 0) {
          _removeInitPayMethodOptions(tmpl.items, removeOptions);
        }
        result.success = true;
        if (frozen) {
          commonApp.frozenTemplate(tmpl);
        }
        result.template = tmpl;
        resolve(result);
      } else {
        reject(new Error('Fail to get payment template ' + _.get(tmpl, 'error')));
      }
    });
  }).catch(err => {
    logger.error(`ERROR in getPaymentTemplate ${err}`);
  });
};

//=ApplicationHandler.getPaymentTemplateValues
var _getPaymentTemplateValues = function() {
  return new Promise((resolve, reject) => {
    resolve();
  }).catch(error => {
    logger.error(`ERROR in _getPaymentTemplateValues ${error}`);
  });
};

var _savePaymentMethods =  function(data) {
  const { appId, changedValues } = data;

  return _application.getApplication(appId).then((app) => {
    return new Promise((resolve, reject) => {
      async.waterfall([
        (callback) => {
          //Update Application
          app.payment = _.assignIn(app.payment, changedValues);
          _application.updateApplicationCompletedStep(app);
          commonApp.updateAndReturnApp(app, callback);
        }, (parentApplication, callback) => {
          _saveParentPaymenttoChild(parentApplication, callback);
        }], (err, application) => {
          if (err) {
            logger.error(`ERROR: _savePaymentMethods ${appId}, ${err}`);
            resolve({success: false});
          } else {
            resolve(application);
          }
      });
    }).catch(error => {
      logger.error(`ERROR in _savePaymentMethods ${error}`);
    });
  });
};

module.exports.savePaymentMethods = function(data, session, callback) {
  const { appId } = data;

  return _savePaymentMethods(data).then((app) => {
    logger.log('INFO: savePaymentMethods - end [RETURN=100]', appId);
    if (_.isFunction(callback)) {
      callback({success: true, application: app});
    } else {
      return {success: true};
    }
  }).catch((error) => {
    logger.error('ERROR: savePaymentMethods - end [RETURN=-101]', appId, error);
    if (_.isFunction(callback)) {
      callback({success: false});
    } else {
      return {success: false};
    }
  });

};

const _saveParentPaymenttoChild = function(parentApplication, callback) {
  let childIds = _.get(parentApplication, 'childIds');
  let payment = _.get(parentApplication, 'payment');
  _application.getChildApplications(childIds).then(childApplications => {
      let updAppPromises = childApplications.map((app) => {
        return new Promise((resolve, reject) => {
          app.payment = _transformParentPaymenttoChildPayment(payment, app.id);
          app.payment.trxAmount = _.get(app, 'payment.cashPortion');
          logger.log(`Payment Data Checking :: ${app.id}, --shield/payment --_saveParentPaymenttoChild -- Previous Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Next Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Current Policy Number :: ${_.get(app, 'policyNumber')}`);
          appDao.updApplication(app.id, app, (res) => {
            if (res && !res.error) {
              resolve(res);
            } else {
              reject(new Error('Fail to update application ' + app.id + ' ' + _.get(res, 'error')));
            }
          });
        });
      });
      
      return Promise.all(updAppPromises).then(results =>{
        callback(null, parentApplication);
      }, rejectReason => {
        callback(`ERROR in _saveParentPaymenttoChild with Reject Reason,  ${rejectReason}`)
      }).catch((error)=>{
        logger.error(`ERROR in _saveParentPaymenttoChild ${error}`);
        callback(`ERROR in _saveParentPaymenttoChild, ${error}`);
      });
    }
  ).catch(error => {
    logger.error(`ERROR in _saveParentPaymenttoChild: ${error}`);
  });
}

const _transformParentPaymenttoChildPayment = (payment, childAppId) => {
  let parentPayment = _.cloneDeep(payment);
  let childPayment = parentPayment;
  let cpfAccNo = _.get(payment, 'cpfMedisaveAccDetails[0].cpfAccNo');
  let paymentChildObj = {};
  let cpfisoaBlock = {
    'idCardNo': cpfAccNo
  };
  let cpfissaBlock = {
    'idCardNo': cpfAccNo
  };
  let srsBlock = {
    'idCardNo': cpfAccNo
  };
 
  _.each(childPayment.premiumDetails, premiumDetail => {
    if (premiumDetail.applicationId === childAppId){
      paymentChildObj = premiumDetail;
    }
  });
  childPayment.cpfisoaBlock = cpfisoaBlock;
  childPayment.cpfissaBlock = cpfissaBlock;
  childPayment.srsBlock = srsBlock;
  
  childPayment = _.assignIn(childPayment, paymentChildObj);
  return _.omit(childPayment, ['cpfMedisaveAccDetails', 'premiumDetails', 'applicationId']);
}

module.exports.preparePayment =  function(data) {
  const { appId } = data;

  return _application.getApplication(appId).then((app) => {
    return new Promise((resolve, reject) => {
      async.waterfall([
        (callback) => {
          if (_.isEmpty(app.payment)) {
            // Initial Payment Store for first Time
            _initPaymentStore(data, app, callback);
          } else {
            callback(null, app);
          }
        }], (err, callbackApplication) => {
          if (err) {
            logger.error(`ERROR prepare Payemnt: ${appId}, ${err}`);
            resolve({success: false});
          } else {
            resolve(callbackApplication);
          }
      });
    }).catch(error => {
      logger.error(`ERROR in preparePayment ${error}`);
    });
  });
};

const _initPaymentStore =  function(data, app, cb) {
  const { appId } = data;
  const { pCid } = app;
  async.waterfall([
    (callback) => {
      // Init Premium Details
      _application.getApplication(appId).then((app) => {
          let payment = {
            premiumDetails: [],
            cpfMedisaveAccDetails: [],
            totCPFPortion: 0,
            totCashPortion: 0,
            totMedisave: 0
          };
      
          let cidsMapping = _.get(app, 'iCidMapping');
          let iCids = _.keys(cidsMapping);
          let laPremiumDetails = [];
          let proposerPremiumDetails = [];
          let mergeProposerPremiumDetails = false;
          _.each(iCids, (cid, key) => {
            let iCidChildren = _.get(cidsMapping, cid);
            _.each(iCidChildren, mapping => {
              let nonAXASheild = (_.get(mapping, 'covCode') === 'ASIM') ? false : true;
              let premiumDetails = [];
              if (cid === pCid) {
                premiumDetails.push(_createPremiumDetails(mapping, app, cid, true, nonAXASheild));
                premiumDetails.postfix = '(proposer)';
                proposerPremiumDetails = [...proposerPremiumDetails, ...premiumDetails];
                mergeProposerPremiumDetails = true;
              } else {
                premiumDetails.push(_createPremiumDetails(mapping, app, cid, false, nonAXASheild));
                laPremiumDetails = [...laPremiumDetails, ...premiumDetails];
              }
              
            });
          });
      
          if (mergeProposerPremiumDetails) {
            payment.premiumDetails = [...proposerPremiumDetails, ...laPremiumDetails];
          } else {
            payment.premiumDetails = [...laPremiumDetails];
          }
      
          payment.premiumDetails = _.filter(payment.premiumDetails, obj => {return !_.isEmpty(obj);});

          // Cal tot premium
          _.each(payment.premiumDetails, premiumObj => {
            payment.totMedisave += premiumObj.medisave;
            payment.totCPFPortion += premiumObj.cpfPortion;
            payment.totCashPortion += premiumObj.cashPortion;
          });
          callback(null, payment);
        });
    }, (payment, callback) => {
      // Init cpfMedisaveAccDetails
      dao.getDocFromCacheFirst(pCid, (clientProfile) => {
        if (clientProfile && !clientProfile.error) {
          let proposerAccDetails = {
            cpfAccHolderName: _.get(clientProfile, 'fullName'),
            cpfAccNo: _.get(clientProfile, 'idCardNo')
          }
          payment.cpfMedisaveAccDetails.push(proposerAccDetails);
          callback(null, payment);
        } else {
          callback('Cannot find client profile');
        }
      });
    }, (payment, callback) => {
      //Update Application
      app.isInitialPaymentCompleted = false;
      app.payment = payment;
      if (app.appStep < commonApp.EAPP_STEP.PAYMENT) {
        app.appStep = commonApp.EAPP_STEP.PAYMENT;
      }
      logger.log(`Payment Data Checking :: ${app.id}, --shield/payment --_initPaymentStore -- Previous Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Next Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Current Policy Number :: ${_.get(app, 'policyNumber')}`);
      appDao.updApplication(appId, app, () => {
        _saveParentPaymenttoChild(app, callback);
      });
    }
  ], (err, finalApplication) => {
      if (err) {
        logger.error(`ERROR in initial payment store: ${err}`);
        cb(err, {success: false});
      } else {
        cb(null, finalApplication);
      }
    });
};

const _createPremiumDetails = (mapping, application, cid, isPhSameAsLa, nonAXASheild) => {
  let pathToGroupedPlan;
  let personInfoObj = _.get(application, `quotation.insureds.[${cid}]`);
  if (isPhSameAsLa) {
    pathToGroupedPlan = 'applicationForm.values.proposer.groupedPlanDetails';
  } else {
    let insuredApplicationFormValues = _getOr({}, 'applicationForm.values.insured', application);
    let currentInsuredIndex = -1;
    insuredApplicationFormValues.forEach((obj, index) => {
      if (_.get(obj, 'personalInfo.cid') === cid) {
        currentInsuredIndex = index;
      }
    });

    pathToGroupedPlan = (currentInsuredIndex !== -1) ?  `applicationForm.values.insured[${currentInsuredIndex}].groupedPlanDetails` : undefined;
  }
  let groupedPlanDetails = _.get(application, pathToGroupedPlan);

  let planList = (nonAXASheild) ? _.get(groupedPlanDetails, 'riderPlanList') : _.get(groupedPlanDetails, 'basicPlanList');

  let basicPlanObj = (nonAXASheild) ?  _.get(_.filter(planList, obj => {return obj.covCode === 'ASCP';}), [0]) : _.get(_.filter(planList, obj => {return obj.covCode === 'ASIM';}), [0]);

  // Rider only has cash 
  let cashPort = (nonAXASheild) ? _getOr(0, 'totalRiderPremium', groupedPlanDetails) : _getOr(0, 'cashPortion', basicPlanObj);
  let payFrequency = _.get(basicPlanObj, 'payFreq');
  let details = {
    cashPortion: (payFrequency === 'M') ? cashPort * 2 : cashPort,
    covName: _.get(basicPlanObj, 'covName'),
    cpfPortion: (nonAXASheild) ? 0 : _getOr(0, 'cpfPortion', basicPlanObj),
    medisave: (nonAXASheild) ? 0 : _getOr(0, 'medisave', basicPlanObj),
    laName: _.get(personInfoObj, 'iFullName'),
    policyNumber: _.get(mapping, 'policyNumber'),
    applicationId: _.get(mapping, 'applicationId'),
    subseqPayMethod: (cashPort !== 0 && payFrequency === 'M') ? 'Y' : 'N',
    cid,
    payFrequency
  };

  return details;

}

module.exports.confirmAppPaid = function(data, session, cb) {
  let masterApplicationId = _.get(data, 'appId');
  if (masterApplicationId) {
    appDao.getApplication(data.appId, function(masterApplication) {
      if (masterApplication && !masterApplication.error) {
        const isSubmitted = _.get(masterApplication, 'isSubmittedStatus');
        async.waterfall([
          (callback) => {
            masterApplication.isInitialPaymentCompleted = true;
            masterApplication.lastUpdateDate = (new Date()).getTime();
            if (!masterApplication.isSubmittedStatus) {
              masterApplication.isSubmittedStatus = true;
              masterApplication.applicationSubmittedDate = (new Date()).getTime();
            }
            commonApp.updateAndReturnApp(masterApplication, callback);
          }, (mApplication, callback) => {
            let updateObj = {};
            if (!isSubmitted) {
              updateObj = {
                isInitialPaymentCompleted: mApplication.isInitialPaymentCompleted,
                lastUpdateDate: mApplication.lastUpdateDate,
                isSubmittedStatus: mApplication.isSubmittedStatus,
                applicationSubmittedDate: mApplication.applicationSubmittedDate
              };
            } else {
              updateObj = {
                isInitialPaymentCompleted: mApplication.isInitialPaymentCompleted,
                lastUpdateDate: mApplication.lastUpdateDate
              };
            }

            commonApp.saveParentValuestoChild(masterApplication, updateObj, callback);
          }, (mApplication, callback) => {
            if (!isSubmitted) {
              let promises = [];
              let childIds = _.get(mApplication, 'childIds');
              _.each(childIds, id => {
                promises.push(
                  new Promise((resolve, reject) => {
                    callApiComplete('/submitInvalidApp/'+ id, 'GET', {}, null, true, (resp) => {
                      logger.log('INFO: Submit invalidated paid application (paid after invalidated):', id, resp);
                      if (resp && resp.success) {
                        resolve({success: true});
                      } else {
                        reject(`Call submitInvalidApp failure: ${id}`);
                      }
                    });
                  })
                );
              });
              
              Promise.all(promises).then(results => {
                callback(null, mApplication);
              }, rejectReason => {
                callback(`Rejected in calling all promises confirmAppPaid:,  ${rejectReason}`);
              }).catch(error => {
                callback(`ERROR in calling all promises confirmAppPaid: ${error}`);
              });
            } else {
              callback(null, mApplication);
            }
          }
        ], (error, app) => {
          if (error) {
            cb({success: false, error: 'Update Application failure:' + app.id});
          } else {
            dao.updateViewIndex('main', 'summaryApps');
            cb({success: true});
          }
        });
      } else {
        cb({success: false, error: 'Cannot get Application:' + data.appId})
      }
    })
  } else {
    cb({success: false, error: 'Invalid Master Application'});
  }
}
/** Copy from old non-shield payment */
const _checkPaymentStatus = (data, session, cb) => {
  let trxNo = data.trxNo;
  let appId = data.appId;

  logger.log('INFO: checkShieldPaymentStatus - start', appId);
  _application.getApplication(appId).then((app) => {
    if (app && !app.error && app.payment) {
      let status = app.payment.trxStatus;
      let trxStatusRemark = '';
      if (status === 'I') {
        trxStatusRemark = 'O';
      } else {
        trxStatusRemark = status;
      }
      if (trxStatusRemark !== app.payment.trxStatusRemark) {
        app.payment.trxStatusRemark = trxStatusRemark;
        logger.log(`Payment Data Checking :: ${app.id}, --shield/payment --_checkPaymentStatus -- Previous Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Next Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Current Policy Number :: ${_.get(app, 'policyNumber')}`);
        appDao.updApplication(appId, app, (upsertResult) => {
          if (upsertResult && !upsertResult.error) {
            logger.log('INFO: checkPaymentStatus - end [RETURN=1]', appId);
            async.waterfall([
              (callback) => {
                _saveParentPaymenttoChild(app, callback);
              }
            ], (error, callback) => {
              if (error) {
                logger.error(`ERROR: Update child application in wildcard payment process ${error}`);
                cb({
                  success: false,
                  application: app
                });
              } else {
                cb({
                  success: true,
                  application: app
                });
              }
            });
          } else {
            logger.error('ERROR: checkPaymentStatus - end [RETURN=-2]', appId, _.get(upsertResult, 'error'));
            cb({
              success: false,
              application: app
            });
          }
        })
      } else {
        logger.log('INFO: checkPaymentStatus - end [RETURN=2]', appId);
        cb({
          success: true,
          application: app
        })
      }
    } else {
      logger.error('ERROR: checkPaymentStatus - end [RETURN=-1]', appId, _.get(app, 'error'));
      cb({
        success: false,
        application: app
      })
    }
  }).catch(error => {
    logger.error(`ERROR: in check shield payment status ${error}`);
    cb({
      success: false,
      application: {}
    })
  })
}

module.exports.checkPaymentStatus = _checkPaymentStatus;
module.exports.getPaymentTemplate = _getPaymentTemplate;
module.exports.getPaymentTemplateValues = _getPaymentTemplateValues;
module.exports.saveParentPaymenttoChild = _saveParentPaymenttoChild;
