
//=ApplicationHandler.getSupportDocuments
const logger = global.logger || console;
const _ = require('lodash');
const dao = require('../../../cbDaoFactory').create();
const applicationDao = require('../../../cbDao/application');
const bundleDao = require('../../../cbDao/bundle');
const ApprovalHandler = require('../../../ApprovalHandler');
const commonAppHandler = require('../common');
const clientDao = require('../../../cbDao/client');
const SuppDocsUtils = require('../../../utils/SuppDocsUtils');
const {createPdfToken, setPdfTokensToRedis} = require('../../../utils/TokenUtils');
const DateUtils = require('../../../../common/DateUtils');
const moment = require('moment');

var _genSupportDocumentsValues = function(app){
  let result = {};

  let genSuppDocsPolicyFormValues = function() {
    let values = {
      'sysDocs':{
        'fnaReport':{
          'id':'fnaReport',
          'title': 'e-FNA',
          'fileType':'application/pdf',
          'fileSize': ''
        },
        'proposal':{
          'id':'proposal',
          'title': 'Product Summary',
          'fileType':'application/pdf',
          'fileSize': ''
        },
        'appPdf':{
          'id':'appPdf',
          'title': 'e-App (' + _.get(app, 'quotation.pFullName') + ')',
          'fileType':'application/pdf',
          'fileSize': ''
        },
        // "eCpdPdf":{
        //   "id": 'eCpdPdf',
        //   "title": "e-CPD",
        //   "fileType":"application/pdf",
        //   "fileSize": ""
        // },
        'eapproval_supervisor_pdf':{
          'id':'eapproval_supervisor_pdf',
          'title': 'Supervisor Validation',
          'fileType':'application/pdf',
          'fileSize': ''
        },
        'faFirm_Comment':{
          'id':'faFirm_Comment',
          'title': 'Comments by FA Firm Admin',
          'fileType':'application/pdf',
          'fileSize': ''
        }
      },
      'mandDocs':{
        'cAck':[],
        'pNric':[],
        'pPass':[],
        'pPassport':[],
        'pPassportWStamp':[],
        'pAddrProof':[],
        'thirdPartyID':[],
        'thirdPartyAddrProof':[],
        'axasam':[],
        'chequeCashierOrder':[],
        'teleTransfer':[],
        'cash':[],
        'healthDeclaration':[],
        'pChinaVisit':[],
        'pBoardingPass':[]
      },
      'optDoc':{
        'axasam':[],
        'chequeCashierOrder':[],
        'teleTransfer':[],
        'cash':[]
      }
    };

    _.forEach(_.get(app, 'iCids', []), (iCid) => {
      let iKey = 'appPdf' + iCid;
      let iFullName = _.get(app, 'quotation.insureds.' + iCid + '.iFullName');
      let docObject = {
        'id' : iKey,
        'title': 'e-App (' + iFullName + ')',
        'fileType' : 'application/pdf',
        'fileSize': ''
      };
      _.set(values, 'sysDocs.' + iKey, docObject);
    });

    return values;
  };

  let genSuppDocsInsuredValues = function(){
    let values = {
      'mandDocs':{
        'iNric':[],
        'iReentryPermit':[]
      },
      'optDoc':{
        'iJuvenileQuestions':[],
        'iMedicalReport':[],
        'iDischargeSummary':[],
        'iOtherIllnessQues':[]
      }
    };
    return values;
  };

  let genSuppDocsProposerValues = function(){
    let values = {
      'mandDocs':{

      },
      'optDoc':{
        'pMedicalReport':[],
        'pDischargeSummary':[],
        'pOtherIllnessQues':[]
      }
    };
    return values;
  };

  result.policyForm = genSuppDocsPolicyFormValues();
  result.proposer = genSuppDocsProposerValues();

  _.forEach(_.get(app, 'iCids'), (iCid) => {
    result[iCid] = genSuppDocsInsuredValues();
  });

  // For Not influence batch submitToRLSWFI function
  result[app.pCid] = genSuppDocsInsuredValues();

  return result;
};

var _getSupportDocumentsTemplate = function(app, appStatus, agentData, eApprovalCase){
  logger.log('INFO: _getSupportDocumentsTemplate');
  let promises = [];
  let template = {};

  let _checkAppFormInsQuesAnswer = function(appFormInsObject) {
    const medicalReportQuesNumList = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '20', '21', '22', '23', '24'];
    /**
    * Ref: "AXASG-EASE-FS-Release 2-Shield V1.4", then "Shield-medical Qn"
    * T10 - "Have you ever had been treated for or been told to get treatment for: "
    * contains: "HEALTH_SHIELD_" + [01-17, 18, 20-23].
    * T13 - "Have you ever been informed by Ministry of Health that an extra premium of 30% has been imposed on your Medishield life insurance?"
    * contains: "INS05"
    * T14 - "Have you undergone any sex reassignment surgery?"
    * contains: "HEALTH_SHIELD_19"
    */
    let isAnyT10QuesCYes = false;
    let isAnyT10QuesCNo = false;
    let isAnyT13QuesDYes = false;
    let isAnyT13QuesDNo = false;
    let isAnyT14QuesCYes = false;
    _.forEach(medicalReportQuesNumList, (quesNumber) => {
      _.forEach(_.get(appFormInsObject, 'HEALTH_SHIELD_' + quesNumber + '_DATA', []), (t10RowData) => {
        if (_.get(t10RowData, 'HEALTH_SHIELD_' + quesNumber + 'c') === 'Y') {
          isAnyT10QuesCYes = true;
        } else if (_.get(t10RowData, 'HEALTH_SHIELD_' + quesNumber + 'c') === 'N') {
          isAnyT10QuesCNo = true;
        }
      });
    });
    // Health13b question: "Have you been diagnosed with any pregnancy complications or pregnancy-related conditions?"
    // It is one of the displaying 'Medical Report' condition in Support Document
    _.forEach(_.get(appFormInsObject, 'HEALTH13b_DATA', []), (t13bRowData) => {
      if (_.get(t13bRowData, 'HEALTH13b3') === 'Y') {
        isAnyT10QuesCYes = true;
      } else if (_.get(t13bRowData, 'HEALTH13b3') === 'N') {
        isAnyT10QuesCNo = true;
      }
    });

    _.forEach(_.get(appFormInsObject, 'INS05_DATA', []), (ins05RowData) => {
      if (_.get(ins05RowData, 'INS05d') === 'Y') {
        isAnyT13QuesDYes = true;
      } else if (_.get(ins05RowData, 'INS05d') === 'N') {
        isAnyT13QuesDNo = true;
      }
    });
    _.forEach(_.get(appFormInsObject, 'HEALTH_SHIELD_19_DATA', []), (t19RowData) => {
      if (_.get(t19RowData, 'HEALTH_SHIELD_19c') === 'Y') {
        isAnyT14QuesCYes = true;
      }
    });

    return {
      isAnyT10QuesCYes,
      isAnyT10QuesCNo,
      isAnyT13QuesDYes,
      isAnyT13QuesDNo,
      isAnyT14QuesCYes
    };
  };

  /**
   * agentData = session.agent
   * agentChannel = session.agent.channel.type
   * agentCode = session.agent.agentCode
   * managerCode = session.agent.managerCode
   */
  let getSuppDocsPolicyFormTemplate = function(){
    logger.log('INFO: getSuppDocsPolicyFormTemplate');
    // const eCpdPdfName = 'eCpdPdf';
    const pCid = _.get(app, 'quotation.pCid');
    let isFAChannel = _.get(agentData, 'channel.type') === 'FA';
    let sysDocsDocuments = [];
    let mandDocsDocuments = [];
    let optDocsDocuments = [];
    let {isThirdPartyPayer} = commonAppHandler.getPayerInfo(app);
    return new Promise((resolve) => {

      clientDao.getProfileById(pCid, (pProfile) => {
        if (_.get(app, 'applicationForm.values.proposer.extra.isPdfGenerated')) {
          sysDocsDocuments.push({
            type: 'file',
            id: 'appPdf'
          });
        }
        _.forEach(_.get(app, 'iCids', []), (iCid) => {
          let foundInsuredObj = _.find(_.get(app, 'applicationForm.values.insured'), (insured) => {
            return insured.personalInfo.cid === iCid;
          });
          if (_.get(foundInsuredObj, 'extra.isPdfGenerated')) {
            sysDocsDocuments.push({
              type: 'file',
              id: 'appPdf' + iCid
            });
          }
        });

        if (!isFAChannel) {
          sysDocsDocuments.push({
            type: 'file',
            id: 'fnaReport'
          });
        }

        sysDocsDocuments.push({
          type :'file',
          id: 'proposal'
        });

        /*
        * Shield contains NO eCpd as in FS
        if (['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(_.get(app, 'payment.initPayMethod')) > -1 && app.isSubmittedStatus){
          sysDocsDocuments.push({
            type: 'file',
            id: eCpdPdfName
          });
        }
        */
        let showSupervisorStatus = ['A', 'R', 'PFAFA'];
        if ((showSupervisorStatus.indexOf(eApprovalCase.approvalStatus) > -1) && eApprovalCase.isFACase === true && _.get(eApprovalCase, '_attachments.eapproval_supervisor_pdf') !== undefined) {
          //Show when the approver is not the director
          if (eApprovalCase.agentId !== eApprovalCase.approveRejectManagerId) {
            sysDocsDocuments.push({
              type:'file',
              id: 'eapproval_supervisor_pdf'
            });
          }

          //Show when it is approved by FA Admin
          if (eApprovalCase.approver_FAAdminCode !== '') {
            sysDocsDocuments.push({
              'type':'file',
              'id':'faFirm_Comment'
            });
          }

        } else if ((eApprovalCase.approvalStatus === 'A' || eApprovalCase.approvalStatus === 'R') && _.get(eApprovalCase, '_attachments.eapproval_supervisor_pdf') !== undefined){
        // } else if (eApprovalCase.approvalStatus === 'A' || eApprovalCase.approvalStatus === 'R'){
          sysDocsDocuments.push({
            'type':'file',
            'id':'eapproval_supervisor_pdf'
          });
        }

        // mandDocsDocuments
        // Application is in FA channel
        if (isFAChannel){
          mandDocsDocuments.push({
            'type':'subSection',
            'id':'cAck',
            'title':'Client acknowledgement/ FNA'
          });
        }
        // proposer is singaporean of singapore pr, N1 stands for singapore
        if (pProfile.nationality === 'N1' || pProfile.prStatus === 'Y') {
          mandDocsDocuments.push({
            'type':'subSection',
            'id':'pNric',
            'title':'Proposer\'s Singapore NRIC '
          });
        } else {
          if (pProfile.pass == 'ep' || pProfile.pass == 'wp' || pProfile.pass == 'dp' || pProfile.pass == 's' || pProfile.pass == 'lp' || pProfile.pass === 'sp') {
            mandDocsDocuments.push({
              'type':'subSection',
              'id':'pPass',
              'title':'Proposer\'s Copy of valid pass in Singapore ',
              'toolTips': 'Copy of valid Employment Pass / S Pass / Work Permit / Dependant / Student / Long Term Visit Pass. (both sides)'
            });
          } else {
            mandDocsDocuments.push({
              'type':'subSection',
              'id':'pPassport',
              'title':'Proposer\'s Copy of valid passport (first page) ',
              'toolTips': 'Passport cover page which contain all personal details '
            });
            mandDocsDocuments.push({
              'type':'subSection',
              'id':'pPassportWStamp',
              'title':'Proposer\'s Copy of valid passport pages with entry stamps issued by Singapore immigration ',
              'toolTips': 'Passport entry stamp is required as proof of entry into Singapore. '
            });
          }
        }

        mandDocsDocuments.push({
          'type':'subSection',
          'id':'pAddrProof',
          'title':'Proposer\'s Proof of Residential Address '
        });
        // If payer is third party payer
        if (isThirdPartyPayer) {
          mandDocsDocuments.push({
            'type':'subSection',
            'id':'thirdPartyID',
            'title':'Third Party Payor\'s ID copy ',
            'toolTips': 'Example of ID copy for third party payor: NRIC / Valid Pass in Singapore / Passport '
          });
          mandDocsDocuments.push({
            'type':'subSection',
            'id':'thirdPartyAddrProof',
            'title':'Third Party Payor\'s Proof of Residential Address '
          });
        }

        let pMailingAddrCountry = _.get(app,'applicationForm.values.proposer.personalInfo.mAddrCountry','');
        // proposer is China nationality and type of pass is others
        if (pProfile.nationality !== 'N1' && pProfile.prStatus === 'N' &&  (pMailingAddrCountry === 'R51' || pMailingAddrCountry === 'R52' || pProfile.residenceCountry === 'R51' || pProfile.residenceCountry === 'R52') && (pProfile.pass === 'o' || pProfile.pass === 'svp' ||  pProfile.pass === 'lp') ){
          mandDocsDocuments.push({
            'type':'subSection',
            'id':'pChinaVisit',
            'title':'Proposer\'s Mainland China Visitor Declaration Form ',
            'toolTips': 'Mainland China Visitor Declaration Form is compulsory for applicants who are China residents. '
          });
          mandDocsDocuments.push({
            'type':'subSection',
            'id':'pBoardingPass',
            'title':'Proposer\'s Copy of Boarding Pass ',
            'toolTips': 'Copy of Boarding Pass is compulsory for applicants who are China residents. '
          });
        }

        // optDocsDocuments
        if (appStatus === 'SUBMITTED' || _.get(agentData, 'agentCode') === _.get(agentData, 'managerCode')) {
          // Check initial payment method: AXA/SAM or Cheque/Cashier Order or Telegraphic Transfer or Cash
          if (app.payment && app.payment.initPayMethod) {
            if (app.payment.initPayMethod == 'axasam') {
              mandDocsDocuments.push({
                'type':'subSection',
                'id':'axasam',
                'title':'AXS/SAM Receipt '
              });
            } else if (app.payment.initPayMethod == 'chequeCashierOrder') {
              mandDocsDocuments.push({
                'type':'subSection',
                'id':'chequeCashierOrder',
                'title':'Cheque / Cashier Order Copy '
              });
            } else if (app.payment.initPayMethod == 'teleTransfter') {
              mandDocsDocuments.push({
                'type':'subSection',
                'id':'teleTransfer',
                'title':'Telegraphic Transfer Receipt '
              });
            } else if (app.payment.initPayMethod === 'cash') {
              mandDocsDocuments.push({
                'type':'subSection',
                'id':'cash',
                'title':'Conditional Receipt of Cash '
              });
            }
          }
        } else {
          // Check initial payment method: AXA/SAM or Cheque/Cashier Order or Telegraphic Transfer or Cash
          if (app.payment && app.payment.initPayMethod) {
            if (app.payment.initPayMethod == 'axasam') {
              optDocsDocuments.push({
                'type':'subSection',
                'id':'axasam',
                'title':'AXS/SAM Receipt '
              });
            } else if (app.payment.initPayMethod == 'chequeCashierOrder') {
              optDocsDocuments.push({
                'type':'subSection',
                'id':'chequeCashierOrder',
                'title':'Cheque / Cashier Order Copy '
              });
            } else if (app.payment.initPayMethod == 'teleTransfter') {
              optDocsDocuments.push({
                'type':'subSection',
                'id':'teleTransfer',
                'title':'Telegraphic Transfer Receipt '
              });
            } else if (app.payment.initPayMethod == 'cash') {
              optDocsDocuments.push({
                'type':'subSection',
                'id':'cash',
                'title':'Conditional Receipt of Cash '
              });
            }
          }
        }

        // Health Declaration
        if (commonAppHandler.isShowHealthDeclaration(app)) {
          if (eApprovalCase.approvalStatus && !_.includes(['A', 'R', 'E'], eApprovalCase.approvalStatus)) {
            mandDocsDocuments.push({
              'type':'subSection',
              'id':'healthDeclaration',
              'title':'Health Declaration'
            });
          }
        }

        // let subTemplate = [];
        let subTemplate = {};

        subTemplate.sysDocs = {
          'id':'sysDocs',
          'title': 'System Document',
          'hasSubLevel': 'false',
          'disabled': 'true',
          'type':'section',
          'items':sysDocsDocuments
        };

        subTemplate.mandDocs = {
          'id':'mandDocs',
          'hasSubLevel': 'false',
          'disabled': 'false',
          'title': 'Mandatory Document',
          'type':'section',
          'items':mandDocsDocuments
        };

        if (optDocsDocuments.length > 0) {
          subTemplate.optDoc = {
            'id':'optDoc',
            'hasSubLevel': 'false',
            'disabled': 'false',
            'title': 'Optional Document',
            'type':'section',
            'items':optDocsDocuments
          };
        }

        subTemplate.otherDoc = {
          'id':'otherDoc',
          'hasSubLevel': 'true',
          'disabled': 'false',
          'title': 'Other Document',
          'type':'section',
          'items':[]
        };

        template.policyForm = {
          tabName: 'Policy Form',
          tabContent: subTemplate,
          tabSequence: 0
        };
        resolve();
      });

    });
  };

  let getSuppDocsProposerTemplate = function(app){
    logger.log('INFO: getSuppDocsProposerTemplate');
    let optDocsDocuments = [];

    return new Promise((resolve) => {
      const pAppFormInsurablity = _.get(app, 'applicationForm.values.proposer.insurability', {});
      const {isAnyT10QuesCYes, isAnyT10QuesCNo, isAnyT13QuesDYes, isAnyT13QuesDNo, isAnyT14QuesCYes} = _checkAppFormInsQuesAnswer(pAppFormInsurablity);

      // add condition as 'If at least 1 of T10 column "Medical Test Report" selected as Yes, or at least 1 of T13 column "Any Existing Medical reports?" selected as Yes'
      if (isAnyT10QuesCYes || isAnyT13QuesDYes) {
        optDocsDocuments.push({
          type: 'subSection',
          id: 'pMedicalReport',
          title: 'Medical Report'
        });
      }

      // add condition as 'If T14 column "Discharge summary  Report" selected as Yes'
      if (isAnyT14QuesCYes) {
        optDocsDocuments.push({
          type: 'subSection',
          id: 'pDischargeSummary',
          title: 'Discharge Summary Report'
        });
      }

      // add condition as 'If at least 1 of T10 column "Medical Test Report" selected as no and at least 1 of T13 column "Any Existing Medical reports?" selected as No'
      if (isAnyT10QuesCNo || isAnyT13QuesDNo) {
        optDocsDocuments.push({
          type: 'subSection',
          id: 'pOtherIllnessQues',
          title: 'Other Illness Questionnaire'
        });
      }

      let subTemplate = {};

      subTemplate.optDoc = {
        'id':'optDoc',
        'hasSubLevel': 'false',
        'disabled': 'false',
        'title': 'Optional Document',
        'type':'section',
        'items':optDocsDocuments
      };

      subTemplate.otherDoc = {
        'id':'otherDoc',
        'hasSubLevel': 'true',
        'disabled': 'false',
        'title': 'Other Document',
        'type':'section',
        'items':[]
      };

      template.proposer = {
        tabName: _.get(app, 'quotation.pFullName'),
        tabContent: subTemplate,
        tabSequence: 1
      };
      resolve();
    });
  };

  let getSuppDocsInsuredListTemplate = function(app) {
    logger.log('INFO: getSuppDocsInsuredListTemplate');

    return new Promise((resolve) => {
      let promises = [];
      _.forEach(_.get(app, 'iCids'), (iCid, iIndex) => {
        promises.push(new Promise((resolve2) => {
          clientDao.getProfileById(iCid, (iProfile) => {
            const iAppFormInsurablity = _.get(app, 'applicationForm.values.insured[' + iIndex + '].insurability', {});
            const {isAnyT10QuesCYes, isAnyT10QuesCNo, isAnyT13QuesDYes, isAnyT13QuesDNo, isAnyT14QuesCYes} = _checkAppFormInsQuesAnswer(iAppFormInsurablity);
            let objInsured = {};

            objInsured.tabName = _.get(app, 'quotation.insureds.' + iCid + '.iFullName');
            objInsured.tabSequence = iIndex + 2;

            let mandDocsDocuments = [];
            let optDocsDocuments = [];

            if (_.get(iProfile, 'nationality') === 'N1' || _.get(iProfile, 'prStatus') === 'Y') {
              mandDocsDocuments.push({
                type: 'subSection',
                id: 'iNric',
                title: 'Singapore NRIC /Birth Cert'
              });
            }

            if (_.get(iProfile, 'prStatus') === 'Y' && _.get(iProfile, 'age') <= 15) {
              mandDocsDocuments.push({
                type: 'subSection',
                id: 'iReentryPermit',
                title: 'Re-entry Permit Form 7'
              });
            }

            // 'Below are the conditions for requesting Clients to upload Child Health Booklet:
            // a) Any of the Juvenile questions is answered as YES; OR
            // question a) HEALTH15, b) HEALTH14, c) HEALTH16, d) HEALTH_SHIELD_24
            // b) Life assured =<6 months'
            // tooltip is 'Child Health Booklet is required due to declaration in Juvenile Question in Medical and Health Information.
            // 1.   If only condition (a) is met
            // -   “Child Health Booklet is required due to declaration in Juvenile Question of Medical and Health Information”
            // 2.   If only condition (b) is met
            // -   “Child Health Booklet is required for Life Assured aged 6 months or less”
            // 3.   If both conditions (a) & (b) are met
            // -   “Child Health Booklet is required for Life Assured aged 6 months or less”'
            let isAnyJuvenileYes = _.find(_.at(iAppFormInsurablity, ['HEALTH15', 'HEALTH14', 'HEALTH16', 'HEALTH_SHIELD_24']), (value) => {return value === 'Y';}) ? true : false;
            let isLAUnder6Months = DateUtils.getAttainedAge(new Date(), new Date(_.get(app, 'quotation.insureds.' + iCid + '.iDob'))).month < 6;
            // let isLAUnder6Months = _.get(iProfile, 'nearAge') == 0 ? true : false;
            if (isAnyJuvenileYes || isLAUnder6Months) {
              let tooltip;
              if (isLAUnder6Months) {
                tooltip = 'Child Health Booklet is required for Life Assured aged 6 months or less';
              } else {
                tooltip = 'Child Health Booklet is required due to declaration in Juvenile Question of Medical and Health Information';
              }
              optDocsDocuments.push({
                type: 'subSection',
                id: 'iJuvenileQuestions',
                title: 'Copy of latest Child Health Booklet including all assessments done to date',
                toolTips: tooltip
              });
            }

            // add condition as 'If at least 1 of T10 column "Medical Test Report" selected as Yes, or at least 1 of T13 column "Any Existing Medical reports?" selected as Yes'
            if (isAnyT10QuesCYes || isAnyT13QuesDYes) {
              optDocsDocuments.push({
                type: 'subSection',
                id: 'iMedicalReport',
                title: 'Medical Report'
              });
            }

            // add condition as 'If T14 column "Discharge summary  Report" selected as Yes'
            if (isAnyT14QuesCYes) {
              optDocsDocuments.push({
                type: 'subSection',
                id: 'iDischargeSummary',
                title: 'Discharge Summary Report'
              });
            }

            // add condition as 'If at least 1 of T10 column "Medical Test Report" selected as no and at least 1 of T13 column "Any Existing Medical reports?" selected as No'
            if (isAnyT10QuesCNo || isAnyT13QuesDNo) {
              optDocsDocuments.push({
                type: 'subSection',
                id: 'iOtherIllnessQues',
                title: 'Other Illness Questionnaire'
              });
            }

            let subTemplate = {};
            subTemplate.mandDocs = {
              'id':'mandDocs',
              'hasSubLevel': 'false',
              'disabled': 'false',
              'title': 'Mandatory Document',
              'type':'section',
              'items': mandDocsDocuments
            };

            if (optDocsDocuments.length > 0) {
              subTemplate.optDoc = {
                'id':'optDoc',
                'hasSubLevel': 'false',
                'disabled': 'false',
                'title': 'Optional Document',
                'type':'section',
                'items':optDocsDocuments
              };
            }

            subTemplate.otherDoc = {
              'id':'otherDoc',
              'hasSubLevel': 'true',
              'disabled': 'false',
              'title': 'Other Document',
              'type':'section',
              'items':[]
            };

            objInsured.tabContent = subTemplate;
            template[iCid] = objInsured;
          });
        }));
      });
      resolve();
    });
  };

  return new Promise((resolve) => {
    promises.push(
      getSuppDocsPolicyFormTemplate()
    );

    // Only display if P is LA
    if (_.includes(_.keys(_.get(app, 'quotation.insureds', {})), app.pCid)) {
      promises.push(
        getSuppDocsProposerTemplate(app)
      );
    }

    promises.push(
      getSuppDocsInsuredListTemplate(app)
    );

    Promise.all(promises).then(() => {
      resolve(template);
    }).catch((error) => {
      logger.error('ERROR: _getSupportDocumentsTemplate ' + app.id);
    });
  });
};
module.exports.getSupportDocumentsTemplate = _getSupportDocumentsTemplate;

var _checkMandDocsStatus = function(template, values) {
  logger.log('INFO: Shield _checkMandDocsStatus');

  _.forEach(template, (tabTemplate, tabId)=>{
    let mandDocsTemplate = _.get(
      _.find(tabTemplate, (section)=>{
        return section.id === 'mandDocs';
      }),
      'items');
    let mandDocsValues = _.get(values[tabId], 'mandDocs', {});

    _.forEach(mandDocsTemplate, (fileTemplate)=>{
      if (_.isEmpty(_.get(mandDocsValues, _.get(fileTemplate, 'id')))) {
        return false;
      }
    });
  });

  return true;
};

//MOVE to ./common.js
/*
var _deletePendingSubmitList = function(app) {
  logger.log('INFO: closeSuppDocs - _deletePendingSubmitList');

  return new Promise((resolve) => {
    if (_.get(app, 'supportDocuments.pendingSubmitList')) {
      // remove the attachments
      applicationDao.deleteAppAttachments(app.id, _.get(app, 'supportDocuments.pendingSubmitList'), (resp) => {
        if (resp.success) {
          let changedApplication = resp.application;
          // remove pendingSubmitList
          delete changedApplication.supportDocuments['pendingSubmitList'];
          resolve({
            success: true,
            application: changedApplication
          });
        } else {
          resolve({success: false});
        }
      });
    } else {
      resolve({
        success: true,
        application: app
      });
    }
  });
};
*/

var showSupportDocments = function(data, session, cb){
  logger.log('INFO: showSupportDocments - start', data.applicationId);
  // manager role include 'manager' and 'director' here
  let agentCode = session.agentCode;
  let defaultDocNameListId = 'suppDocsDefaultDocNames';

  let initViewedList = function(app, template){
  // let diffWithSysDocs = function(app, template){
    let idList = [];

    if (agentCode === app.agentCode) {
      idList = ['appPdf', 'proposal'];
      if (session.agent.channel.type !== 'FA') {
        idList.push('fnaReport');
      }
      _.each(app.iCids, (iCid) => {
        idList.push('appPdf' + iCid);
      });
    } else {
      idList = _.keys(_.get(app, 'supportDocuments.viewedList.' + app.agentCode, {}));

      let shieldSysDocsIds = ['appPdf', 'proposal'];
      if (session.agent.channel.type !== 'FA') {
        shieldSysDocsIds.push('fnaReport');
      }
      _.each(app.iCids, (iCid) => {
        shieldSysDocsIds.push('appPdf' + iCid);
      });

      idList = _.union(idList, shieldSysDocsIds);
    }

    if (session.agent.channel.type === 'FA') {
      // if in FA Channel, need to add cAck's files into ViewedList, as the documents of FNA in FA Channel
      _.forEach(
        _.get(app, 'supportDocuments.values.policyForm.mandDocs.cAck', []),
        (cAckFile) => {
          idList.push(cAckFile.id);
        }
      );
    }
    _.forEach(idList, (id)=>{
      if (!_.includes(
            _.keys(_.get(app, 'supportDocuments.viewedList.' + agentCode))
            , id)) {
        _.set(app, 'supportDocuments.viewedList.' + agentCode + '.' + id, false);
      }
    });
  };

  let updateViewedList = function(app, template, eApprovalCase){
    if (!app.supportDocuments.viewedList[agentCode]) {
      app.supportDocuments.viewedList[agentCode] = {};
      initViewedList(app, template);
      // diffWithSysDocs(app, template);
    } else {
      if (agentCode !== app.agentCode) {
        if (_.get(eApprovalCase, 'approvalStatus') && !_.includes(['A', 'R', 'E'], _.get(eApprovalCase, 'approvalStatus'))) {
          let loginAgentViewedObj = app.supportDocuments.viewedList[agentCode];
          if (_.isEmpty(loginAgentViewedObj)) {
            logger.log('INFO: Support Document - Recover supervisor viewedList - Shield - empty object :', app.id);

            let idList = ['appPdf', 'proposal'];
            _.each(app.iCids, (iCid) => {
              idList.push('appPdf' + iCid);
            });
            if (session.agent.channel.type !== 'FA') {
              idList.push('fnaReport');
            }
            _.each(idList, (id) => {
              loginAgentViewedObj[id] = false;
            });

          } else if (!loginAgentViewedObj.hasOwnProperty('appPdf')) {
            logger.log('INFO: Support Document - Recover supervisor viewedList - Shield - missed eApp : ', app.id);

            let idList = ['appPdf'];
            _.each(app.iCids, (iCid) => {
              idList.push('appPdf' + iCid);
            });
            _.each(idList, (id) => {
              if (!loginAgentViewedObj.hasOwnProperty(id)) {
                loginAgentViewedObj[id] = false;
              }
            });
          }
        }
      }
    }
  };

  let getDefaultDocNameList = function() {
    return new Promise((resolve)=>{
      dao.getDoc(defaultDocNameListId, function(list) {
        resolve(list);
      });
    });
  };

  let getOtherDocNameList = function(docNamesList,iIsJapaneseAndNotShield,pIsJapaneseAndNotShield) {
    const otherDocNameListId = 'otherDocNames';
    let ikey = iIsJapaneseAndNotShield ? 'otherDocNames_JapaneseAndNotShield' : 'otherDocNames';
    let pkey = pIsJapaneseAndNotShield ? 'otherDocNames_JapaneseAndNotShield' : 'otherDocNames';
    return new Promise((resolve)=>{
      dao.getDoc(otherDocNameListId, function(obj) {
        let otherDocNames = obj ? {
          'la': obj[ikey].la,
          'ph': obj[pkey].ph
        } : {'la':[],'ph':[]};
        docNamesList.otherDocNames = otherDocNames;
        resolve(docNamesList);
      });
    });
  };

  applicationDao.getApplication(data.applicationId, (appDoc) => {
    if (!appDoc.error){
      commonAppHandler.deletePendingSubmitList(appDoc).then((resp) => {
        let app = resp.application || appDoc;
        let pCid = app.quotation.pCid;
        let iCid = app.quotation.iCid;
        let template, values;
        let tokensMap = {};
        const approvalDocId = ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(app.id);



        dao.getDocFromCacheFirst(approvalDocId, (approvalDoc) => {
          commonAppHandler.isSuppDocsReadOnly(app, session.agent, session.agentCode).then((isReadOnly) => {
            bundleDao.getApplicationByBundleId(_.get(app, 'bundleId'), app.id).then((bApp)=>{
              let tokens = [];
              let suppDocsAppView = {
                id: app.id,
                iCid: app.iCid,
                pCid: app.pCid,
                iFullName: app.quotation.iFullName,
                pFullName: app.quotation.pFullName
              };
              logger.log('isReadOnly :' + isReadOnly);
              _getSupportDocumentsTemplate(app, bApp.appStatus, session.agent, approvalDoc).then((template) => {
                if (!app.supportDocuments) {
                  app.supportDocuments = {};
                  app.supportDocuments.values = _genSupportDocumentsValues(app);
                  app.supportDocuments.viewedList = {};
                  app.supportDocuments.isAllViewed = false;
                } else {
                  if (!app.supportDocuments.viewedList) {
                    app.supportDocuments.viewedList = {};
                  }
                  if (!app.supportDocuments.values) {
                    app.supportDocuments.values = _genSupportDocumentsValues(app);
                  }

                  //generate token for image in mandDocs & optDoc
                  let now = new Date().getTime();
                  _.forEach(SuppDocsUtils.getNonSysDocs(app, ['application/pdf']), (doc) => {
                    let token = createPdfToken(app._id, doc.id, now, session.loginToken);
                    tokens.push(token);
                    tokensMap[doc.id] = token.token;
                  });
                }
                updateViewedList(app, template, approvalDoc);
                applicationDao.upsertApplication(app._id, app, function(resp){
                  if (resp) {
                    // Default DOC NAME LIST for checking duplication of document name
                    getDefaultDocNameList()
                    .then((docNamesList) => {
                      //get other doc selction list for Shield,update docNamesList
                      return getOtherDocNameList(docNamesList,false,false);
                    })
                    .then((docNamesList) => {
                      let supportDocDetails = {
                        otherDocNameList: docNamesList.otherDocNames
                      };
                      setPdfTokensToRedis(tokens, () => {
                        logger.log('INFO: getSupportDocuments - end [RETURN=2]', data.applicationId);
                        cb({
                          success: true,
                          template: template,
                          values: app.supportDocuments.values,
                          suppDocsAppView: suppDocsAppView,
                          viewedList: app.supportDocuments.viewedList[agentCode],
                          isReadOnly: isReadOnly,
                          defaultDocNameList: docNamesList.defaultDocumentNames,
                          supportDocDetails: supportDocDetails,
                          tokensMap: tokensMap
                        });
                      });
                    });
                  } else {
                    logger.error('ERROR: getSupportDocuments - end [RETURN=-3]', data.applicationId);
                    cb({success:false});
                  }
                });
              });
            });
          });
        });
      });
    } else {
      logger.error('ERROR: getSupportDocuments - end [RETURN=-1]', data.applicationId);
      cb({success: false});
    }
  });
};

var closeSuppDocs = function(data, session, cb){
  logger.log('INFO: Shield Handler closeSuppDocs - start', data.appId);
  let app = data.appDoc;

  let prepareSubmissionTemplateValues = function(currApp) {
    getSubmissionTemplate(currApp, function(tmpl) {
      getSubmissionValues(currApp, session, '', '', function (resValues) {
        logger.log('INFO: closeSuppDocs - end [RETURN=1]', data.appId);
        cb({
          success: true,
          template: tmpl,
          values: resValues.values,
          isMandDocsAllUploaded: mandDocsAllUploaded,
          isSubmitted: false
        });
      });
    });
  };

  let mandDocsAllUploaded = _checkMandDocsStatus(data.template, _.get(app, 'supportDocuments.values', {}));

  commonAppHandler.deletePendingSubmitList(app).then((resp) => {
    if (resp.success) {
      let application = resp.application;
      application.isMandDocsAllUploaded = mandDocsAllUploaded;
      applicationDao.upsertApplication(application.id, application, function(resp){
        if (resp) {
          prepareSubmissionTemplateValues(application);
        } else {
          logger.error('ERROR: closeSuppDocs - end [RETURN=-3]', data.appId);
          cb({success: false});
        }
      });
    } else {
      logger.error('ERROR: closeSuppDocs - end [RETURN=-2]', data.appId);
      cb({success: false});
    }
  }).catch((error) => {
    logger.error('ERROR: closeSuppDocs - _deletePendingSubmitList [RETURN=-1]', data.appId, error);
  });
};

module.exports.showSupportDocments = showSupportDocments;
module.exports.closeSuppDocs = closeSuppDocs;
