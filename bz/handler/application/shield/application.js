const _ = require('lodash');
const moment = require('moment');
const logger = global.logger || console;

const utils = require('../../../Quote/utils');
const CommonUtils = require('../../../utils/CommonUtils');
// const DateUtils = require('../../../../common/DateUtils');
const commonApp = require('../common');
const _signature = require('./signature');
const _payment = require('./payment');
const _submission = require('./submission');
const dao = require('../../../cbDaoFactory').create();
const bDao = require('../../../cbDao/bundle');
const pDao = require('../../../cbDao/product');
const cDao = require('../../../cbDao/client');
const nDao = require('../../../cbDao/needs');
const appDao = require('../../../cbDao/application');
const PDFHandler = require('../../../PDFHandler');
const needsHandler = require('../../../NeedsHandler');
const agentDao        = require('../../../cbDao/agent');

const _getNameOfOrganization = function(upline2Code, callback) {
  logger.log('INFO: _getNameOfOrganization shield->searchAgents upline2Code: ', upline2Code);
  return new Promise((resolve, reject) => {
    agentDao.searchAgents('01',upline2Code,upline2Code,(res)=>{
      if (res.success){
        if (res.result.length > 0){
          resolve(res.result[0].agentName);
        }
        else {
          logger.error('Error in _getNameOfOrganization shield->searchAgents upline2Code: ',upline2Code);
          resolve(null);
        }
      }
      else {
        logger.error('Error in _getNameOfOrganization shield->searchAgents upline2Code: ',upline2Code);
        resolve(null);
      }
    });
  });
};

var _getAppFormTemplate = function(quotation) {
  return new Promise((resolve, reject) => {
    logger.log('INFO: _getAppFormTemplate - start', quotation.id);
    let isPhSameAsLa = false;
    let plans = [];
    let pCid = _.get(quotation, 'pCid');
    let insureds = _.keys(_.get(quotation, 'insureds'));
    let iCids = [];
    _.forEach(insureds, (iCid, i) => {
      if (pCid === iCid) {
        isPhSameAsLa = true;
      } else {
        iCids.push(iCid);
      }
      plans = [...plans, ..._.get(quotation, 'insureds.' + iCid + '.plans', [])];
    });
    plans = _.uniqBy(plans, 'covCode');

    let _getInsForms = function(insForms, forms, index, cb) {
      logger.log('INFO: getAppFormTemplate - getInsForms', index, forms.length, isPhSameAsLa);
      if (forms.length > index) {
        let form = forms[index];

        dao.getDocFromCacheFirst(form.form, function(tpl) {
          if (tpl && !tpl.error) {
            let laItem =  _.cloneDeep(tpl.item);
            if (isPhSameAsLa && form.id === 'ph') {
              laItem.subType = 'proposer';
            } else if (form.id === 'la'){
              laItem.subType = 'insured';
            }

            let template =  _.cloneDeep(tpl);
            template.items = _.cloneDeep(insForms.items) || [];
            template.items.push(laItem);
            if (form.id === 'la') {
              //copy insured tab if more then 1 la
              for (let i = 1; i < iCids.length; i ++) {
                let insuredTab = _.cloneDeep(laItem);
                template.items.push(insuredTab);
              }
            }

            insForms.formId = template.formId;
            insForms.items = template.items;
          }
          _getInsForms(insForms, forms, index + 1, cb);
        });
      } else {
        if (insForms && insForms.items) {
          // Sort Proposer to the first item
          insForms.items = insForms.items.sort((a,b) => {if (a.subType > b.subType) {return -1;} else {return 1;}});
        }
        cb();
      }
    };

    let _getTemplate = function(forms, index, callback) {
      if (forms.length > index) {
        let form = forms[index];

        if (typeof form.form === 'string') {
          dao.getDocFromCacheFirst(form.form, function(tpl) {
            if (tpl && !tpl.error) {
              let template = _.cloneDeep(tpl);
              delete form.form;
              form.formId = template.formId;
              form.skipCheck = template.skipCheck;

              //copy insured tab if more then 1 la
              let insuredTabs = _.filter(template.items, (tab) => { return tab.subType === 'insured'; } );
              for (let i = 1; i < iCids.length && insuredTabs.length > 0; i ++) {
                let insuredTab = _.cloneDeep(insuredTabs[0]);
                template.items.push(insuredTab);
              }

              //remove insured tab if ph is la and no other insured
              if (isPhSameAsLa && iCids.length === 0) {
                for (let i = template.items.length; i > 1; i --) {
                  template.items.splice(1, 1);
                }
              }

              //remove proposer tab in ROP if ph is not la
              if (!isPhSameAsLa && (template.formId === 'menu_residency' || template.formId === 'menu_plan')) {
                template.items.splice(0, 1);
              }

              form.items = template.items;
            }
            logger.log('INFO: _getAppFormTemplate - getTemplate', index, '(got forms) [id=', _.get(form, 'id'), '; formId=', _.get(form, 'formId'), '; title=', _.get(form, 'title'), ']');
            _getTemplate(forms, index + 1, callback);
          });
        } else {
          let insForms = [];
          if (iCids.length > 0 && form.form.la) {
            for (let i in plans) {
              let plan = plans[i];
              if (form.form.la[plan.covCode]) {
                let tplCode = form.form.la[plan.covCode];
                if (_.findIndex(insForms, (it)=>{return it.form === tplCode;}) === -1) {
                  insForms.push({
                    id: 'la',
                    form: tplCode
                  });
                }
              }
            }
          }

          if (isPhSameAsLa && form.form.ph) {
            for (let i in plans) {
              let plan = plans[i];
              let tplCode = form.form.ph[plan.covCode];

              if (tplCode) {
                if (_.findIndex(insForms, (it)=>{return it.form === tplCode;}) === -1) {
                  insForms.push({
                    id: 'ph',
                    form: tplCode
                  });
                }
              }
            }
          }
          if (insForms.length > 0) {
            delete form.form;
            form.items = [];
            _getInsForms(form, insForms, 0, function() {
              logger.log('INFO: getAppFormTemplate - getTemplate', quotation.id, index, '(got Ins forms) [id=', _.get(form, 'id'), '; formId=', _.get(form, 'formId'), '; title=', _.get(form, 'title'), ']');
              _getTemplate(forms, index + 1, callback);
            });
          } else {
            _getTemplate(forms, index + 1, callback);
          }
        }
      } else {
        callback();
      }
    };

    let _getSectionItems = function(sections, index, callback) {
      logger.log('INFO: _getAppFormTemplate - getSectionItems', quotation.id, '[index=', index, '; numOfSection=', sections.length, ']');
      if (sections.length > index) {
        _getTemplate(sections[index].items, 0, () => {
          _getSectionItems(sections, index + 1, callback);
        });
      } else {
        callback(sections);
      }
    };

    dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, (mapping) => {
      if (mapping && mapping[quotation.baseProductCode]) {
        let sections = _.cloneDeep(mapping[quotation.baseProductCode].eFormSections);
        if (sections && sections.length) {
          _getSectionItems(sections, 0, resolve);
        } else {
          resolve(sections);
        }
      } else {
        logger.error('ERROR: _getAppFormTemplate - end [RETURN=-100]', quotation.id);
        reject();
      }
    });
  }).then((sections) => {
    logger.log('INFO: _getAppFormTemplate - allDone', quotation.id);

    let _genMenuForm = function (forms, title, detailSeq) {
      logger.log('INFO: getAppFormTemplate - genMenuForm', title);
      let menuForm = {
        'title': title,
        'type': 'menuItem',
        'skipCheck': forms.skipCheck,
        'key': forms.formId || forms.id || forms._id,
        'detailSeq': detailSeq,
        'items': [],
        'shieldLayout': true
      };

      // if (forms.id && (forms.id === 'menu_plan' || forms.id === 'plan')) {
      //   menuForm.items = forms.items;
      // } else {
        menuForm.items.push({
          type: 'tabs',
          subType: 'servergenerated',
          items: forms.items
        });
      // }
      return menuForm;
    };

    let eFormMenus = [];
    if (sections && sections.length) {
      for (let s in sections) {
        let sectTmp = {
          type: 'menuSection',
          detailSeq: parseInt(s),
          items: []
        };
        let forms = sections[s];
        for (let f in forms.items) {
          let form = forms.items[f];
          if (form.items && form.items.length) {
            sectTmp.items.push(_genMenuForm(form, form.title, parseInt(f)));
          }
        }
        eFormMenus.push(sectTmp);
      }
    }

    logger.log('INFO: _getAppFormTemplate - end', quotation.id);
    return {
      template: {
        'type': 'BLOCK',
        'items':
        [
          {
            'type': 'MENU',
            'items': eFormMenus
          }
        ]
      },
      sections
    };
  });
};

var _forzenAppFormTemplateByTabIndex = function(template, formTabMap, tabIndex) {
  let type = _.toUpper(_.get(template, 'type', ''));

  if (['BLOCK', 'MENU', 'MENUSECTION'].indexOf(type) >= 0 && _.isArray(template.items)) {
    _.forEach(template.items, (item) => {
      _forzenAppFormTemplateByTabIndex(item, formTabMap);
    });
  } else if (type === 'MENUITEM') {
    let key = _.get(template, 'key', '');
    let forzenTabIndex = formTabMap[key];
    _.forEach(template.items, (item) => {
      _forzenAppFormTemplateByTabIndex(item, formTabMap, forzenTabIndex);
    });
  } else if (type === 'TABS' && _.isNumber(tabIndex)) {
    if (tabIndex >= 0 && tabIndex < _.get(template, 'items.length', 0)) {
      logger.debug('DEBUG: _forzenAppFormTemplateByTabIndex - perpare to forzen', type, 'with index', tabIndex);
      commonApp.frozenTemplate(template.items[tabIndex]);
    } else {
      logger.debug('DEBUG: _forzenAppFormTemplateByTabIndex - tab index not found', tabIndex);
    }
  }
};

var _forzenAppFormTemplateByMenuItemKey = function(template, key) {
  let type = _.toUpper(_.get(template, 'type', ''));
  if (['BLOCK', 'MENU'].indexOf(type) >= 0 && _.isArray(template.items)) {
    _.forEach(template.items, (item) => {
      _forzenAppFormTemplateByMenuItemKey(item, key);
    });
  } else if (type === 'MENUSECTION' && _.isArray(template.items)) {
    let forzenItemIndex = _.findIndex(template.items, (item) => { return _.get(item, 'key', '') === key; });
    if (forzenItemIndex >= 0 && forzenItemIndex < _.get(template, 'items.length', 0)) {
      logger.debug('DEBUG: _forzenAppFormTemplateByMenuItemKey - menuItem key', key, 'found in type=', type, ',prepare to frozen');
      commonApp.frozenTemplate(template.items[forzenItemIndex]);
    } else {
      logger.debug('DEBUG: _forzenAppFormTemplateByMenuItemKey - menuItem key', key, 'not found in type=', type);
    }
  }
};

var _forzenAppFormTemplateByCid = function(application, template, cid) {
  let formTabMap = {
    menu_person: -1,
    menu_residency: -1,
    menu_insure: -1,
    menu_declaration: -1
  };
  let isPhSameAsLa = _.get(application, 'applicationForm.values.proposer.extra.isPhSameAsLa') === 'Y';

  if (application.pCid === cid) {
    formTabMap.menu_person = 0;
    formTabMap.menu_declaration = 0;

    if (isPhSameAsLa) {
      formTabMap.menu_residency = 0;
      formTabMap.menu_insure = 0;
    }
    _forzenAppFormTemplateByTabIndex(template, formTabMap);

  } else {
    let iCidIndex = _.get(application, 'iCids', []).indexOf(cid);

    if (iCidIndex < 0) {
      logger.error('ERROR: _forzenAppFormTemplateByCid - iCid not found', cid, application._id);
    } else {
      formTabMap.menu_person = iCidIndex + 1;
      formTabMap.menu_residency = isPhSameAsLa ? iCidIndex + 1 : iCidIndex;
      formTabMap.menu_insure = isPhSameAsLa ? iCidIndex + 1 : iCidIndex;
      formTabMap.menu_declaration = iCidIndex + 1;

      _forzenAppFormTemplateByTabIndex(template, formTabMap);
    }
  }
};

var _getAppFormTemplateWithOptionList = function(application) {
  return _getAppFormTemplate(application.quotation).then((templateRes) => {
    return new Promise((resolve) => {
      commonApp.getOptionsList(application, templateRes.template, () => {
        if (application.isStartSignature) {
          logger.log('INFO: _forzenAppFormTemplateByMenuItemKey - menu_person', application._id);
          _forzenAppFormTemplateByMenuItemKey(templateRes.template, 'menu_person');
          logger.log('INFO: _forzenAppFormTemplateByCid', application._id);
          _forzenAppFormTemplateByCid(application, templateRes.template, application.pCid);

          _.forEach(_.get(application, 'isAppFormInsuredSigned', []), (isLaSigned, index) => {
            if (isLaSigned) {
              let iCid = _.get(application, `iCids[${index}]`, '');
              if (iCid) {
                logger.log('INFO: _forzenAppFormTemplateByCid', application._id, iCid);
                _forzenAppFormTemplateByCid(application, templateRes.template, iCid);
              } else {
                logger.error('ERROR: _getAppFormTemplateWithOptionList - Fail to find iCids with index', index, application._id);
              }
            }
          });
        }

        resolve(templateRes);
      });
    });
  });
};

var _getApplication = function(appId) {
  return new Promise((resolve, reject) => {
    appDao.getApplication(appId, (app) => {
      if (app && !app.error) {
        resolve(app);
      } else {
        reject(new Error('_getApplication - Fail to get Application ' + appId + _.get(app, 'error')));
      }
    });
  });
};

var _getChildApplications = function(childIds) {
  let getAppPromises = childIds.map((childId, i) => {
    return _getApplication(childId);
  });
  return Promise.all(getAppPromises);
};

var _saveParentApplicationValuesToChild = function(parentApplication, childApplications) {
  logger.log('INFO: _saveParentApplicationValuesToChild', parentApplication._id);
  let pCid = _.get(parentApplication, 'pCid');
  let paFormValues = parentApplication.applicationForm.values;

  let proposerValues = _.cloneDeep(paFormValues.proposer);
  if (_.get(proposerValues, 'groupedPlanDetails')) {
    delete proposerValues.groupedPlanDetails;
  }

  _.forEach(childApplications, (childApp) => {
    let iCid = _.get(childApp, 'iCid');

    if (pCid === iCid) {
      childApp.applicationForm.values.proposer = proposerValues;
    } else {
      let insuredValues = _.cloneDeep(_.filter(paFormValues.insured, (la) => {
        return _.get(la, 'personalInfo.cid') === iCid;
      }));

      _.forEach(insuredValues, (iValues) => {
        if (_.get(iValues, 'groupedPlanDetails')) {
          delete iValues.groupedPlanDetails;
        }
      });

      childApp.applicationForm.values.proposer = proposerValues;
      childApp.applicationForm.values.insured = insuredValues;
    }
  });
};

//=ApplicationHandler.checkCrossAge
var _checkCrossAge = function(app) {
  // *** Shield don't have isBackDate, always N in quotation
  logger.log('INFO: _checkCrossAge - start', app._id);

  return new Promise((resolve, reject) => {
    pDao.getProduct(app.quotation.baseProductId, (product) => {
      if (product && !product.error) {
        resolve(product);
      } else {
        reject(new Error('Fail to get Product ' + app.quotation.baseProductId + ' for ' + app._id));
      }
    });
  }).then((product) => {
    let checkPromise = [];
    let insureds = _.get(app, 'quotation.insureds', {});

    _.forEach(insureds, (insured, i) => {
      checkPromise.push(new Promise((resolve, reject) => {
        let {currentAge, futureAge, ageCrossLine} = commonApp.getAgeForCheckingCrossAge(commonApp.CROSSAGE_MODE.NEXT_BIRTHDAY, insured.iAge, insured.iDob);

        let result = {
          success: true,
          crossedAge: false,
          status: commonApp.CROSSAGE_STATUS.NO_CROSSAGE,
          allowBackdate: product.allowBackdate === 'Y',
          iCid: insured.iCid
        };

        if (currentAge - ageCrossLine >= 0) {
          // crossed age
          result.crossedAge = true;

          // if (!app.isStartSignature && app.quotation.isBackDate === 'N' && !app.isBackDate) {
          if (!app.isStartSignature) {
            result.status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED;
            resolve(result);
          // appStatus need to change to get from bundle
          // } else if (app.isStartSignature && app.quotation.isBackDate === 'N' && !app.isBackDate) {
          } else if (app.isStartSignature) {
            bDao.getApplicationByBundleId(_.get(app, 'bundleId'), app.id).then((bApp)=>{

              if (_.get(bApp, 'appStatus') === 'APPLYING') {
                result.status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED;
              } else {
                result.status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_NO_ACTION;
              }
              resolve(result);
            }).catch((err)=>{
              logger.error('ERROR: _checkCrossAge - getApplicationByBundleId', app._id, err);
              reject(err);
            });
          } else {
            //No this case for shield
            result.status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_NO_ACTION;
            resolve(result);
          }
        } else if (currentAge - ageCrossLine === -1) {
          // will cross age soon
          let willCrossAgeInDays = ageCrossLine === futureAge;
          // if (willCrossAgeInDays && app.quotation.isBackDate === 'N' && !app.isBackDate && !app.isStartSignature) {
          if (willCrossAgeInDays && !app.isStartSignature) {
            result.status = commonApp.CROSSAGE_STATUS.WILL_CROSSAGE;
          } else {
            result.status = commonApp.CROSSAGE_STATUS.WILL_CROSSAGE_NO_ACTION;
          }
          resolve(result);
        } else {
          // will not cross age
          resolve(result);
        }
      }));
    });

    return Promise.all(checkPromise).then((results) => {
      let isCrossedAge = _.findIndex(results, (res) => { return res.crossedAge; }) >= 0;

      let crossedAgeCid = [];
      _.forEach(results, (res) => {
        if (res.crossedAge) {
          crossedAgeCid.push(res.iCid);
        }
      });

      let showWillCrossAge = _.findIndex(results, (res) => { return res.status === commonApp.CROSSAGE_STATUS.WILL_CROSSAGE; }) >= 0;
      let showCrossedAgeUnsignedMsg = _.findIndex(results, (res) => { return res.status === commonApp.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED; }) >= 0;
      let showCrossedAgeSignedMsg = _.findIndex(results, (res) => { return res.status === commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED; }) >= 0;

      let status = 0;
      if (showCrossedAgeSignedMsg) {
        status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED;
      } else if (showCrossedAgeUnsignedMsg) {
        status = commonApp.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED;
      } else if (showWillCrossAge) {
        status = commonApp.CROSSAGE_STATUS.WILL_CROSSAGE;
      }

      logger.log('INFO: _checkCrossAge - end', app._id, 'status=', status);
      return {
        success: true,
        crossedAge: isCrossedAge,
        crossedAgeCid: crossedAgeCid,
        status: status
      };
    });
  });
};

var _getPolicyNumbers = function(application) {
  let result = {
    success: false,
    parentApp: _.cloneDeep(application),
    childApps: []
  };
  let childIds = _.get(application, 'childIds', []);

  return new Promise((resolve) => {
    let numOfPolicyNumReq = childIds.length;
    commonApp.getPolicyNumberFromApi(numOfPolicyNumReq, true, (policyNumbers) => {
      if (policyNumbers) {
        if (policyNumbers.length === numOfPolicyNumReq) {
          resolve(policyNumbers);
        } else {
          logger.error('ERROR: _getPolicyNumbers - Exception occurs on getting policy number, count=' + numOfPolicyNumReq, application._id);
          resolve([]);
        }
      } else {
        logger.error('ERROR: _getPolicyNumbers - Fail to get policy number, count=' + numOfPolicyNumReq, application._id);
        resolve([]);
      }
    });
  }).then((policyNumbers) => {

    return _getChildApplications(childIds).then((cApplications) => {
      if (policyNumbers.length > 0) {
        _.forEach(cApplications, (childApp, i) => {
          let policyNumber = policyNumbers[i];
          childApp.policyNumber = policyNumber;

          let mappingList = result.parentApp.iCidMapping[childApp.iCid];
          _.forEach(mappingList, (mapping) => {
            if (mapping.applicationId === childApp.id) {
              mapping.policyNumber = policyNumber;
            }
          });
        });

        result.parentApp.isPolicyNumberGenerated = true;
        result.success = true;
      }
      result.childApps = [...cApplications];
      return result;
    });
  });
};

var _saveAppForm = function(data, session) {
  const { appId, changedValues, getPolicyNumber } = data;
  let cache = {
    application: {},
    bundle: {}
  };
  let result = {
    success: false,
    application: {},
    getPolicyNumberResult: false,
    updIds: []
  };
  let isFaChannel = session.agent.channel.type === 'FA';

  logger.log(`INFO: ShieldApplicationUpdate changedValues - [appid:${appId}; cid:${_.get(changedValues, 'proposer.personalInfo.cid')}; isSameAddr:${_.get(changedValues, 'proposer.personalInfo.isSameAddr')}; fn:_saveAppForm]`);
  return _getApplication(appId).then((app) => {
    return bDao.getCurrentBundle(app.pCid).then((bundle) => {
      if (bundle && !bundle.error) {
        cache.application = app;
        cache.bundle = bundle;
        return;
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then(() => {
    var orgFormValues = cache.application.applicationForm.values;

    logger.log(`INFO: ShieldApplicationUpdate orgFormValues - [appid:${appId}; cid:${_.get(orgFormValues, 'proposer.personalInfo.cid')}; isSameAddr:${_.get(orgFormValues, 'proposer.personalInfo.isSameAddr')}; fn:_saveAppForm]`);
    logger.log(`INFO: ShieldApplicationUpdate changedValues - [appid:${appId}; cid:${_.get(changedValues, 'proposer.personalInfo.cid')}; isSameAddr:${_.get(changedValues, 'proposer.personalInfo.isSameAddr')}; fn:_saveAppForm]`);
    let diffs = CommonUtils.difference(orgFormValues, changedValues);

    if (Object.keys(diffs).length > 0) {

      logger.log(`INFO: ShieldApplicationUpdate diffs - [appid:${appId}; cid:${_.get(changedValues, 'proposer.personalInfo.cid')}; org_isSameAddr:${_.get(orgFormValues, 'proposer.personalInfo.isSameAddr')}; new_isSameAddr:${_.get(changedValues, 'proposer.personalInfo.isSameAddr')}; fn:_saveAppForm]`);
      logger.log('INFO: _saveAppForm - update diffs to bundle', _.get(cache.bundle, 'id'), 'for', appId);
      let proposer = changedValues.proposer;
      let proposers = [];
      if (proposer instanceof Object) {
        proposers.push(proposer);
      } else if (proposer instanceof Array) {
        proposers = proposer;
      }

      let cids = [];
      for (let j = 0; j < proposers.length; j++) {
        cids.push(proposers[j].personalInfo.cid);
        commonApp.updateClientFormValues(cache.bundle, proposers[j]);
      }

      let insured = changedValues.insured;
      for (let j = 0; j < insured.length; j++){
        let _insured = insured[j];
        if (cids.length === 0 || cids.indexOf(_insured.personalInfo.cid) === -1) {
          commonApp.updateClientFormValues(cache.bundle, _insured);
        }
      }

      return true;
    }

    return false;
  }).then((isUpdateBundle) => {
    return new Promise((resolve, reject) => {
      if (isUpdateBundle) {
        logger.log(`INFO: BundleUpdate - [cid:${_.get(cache.application, 'pCid')}; bundleId:${_.get(cache.bundle, 'id')}; fn:_saveAppForm]`);
        bDao.updateBundle(cache.bundle, function(upRes) {
          if (upRes && !upRes.error) {
            resolve();
          } else {
            reject(new Error('Fail to update bundle' + _.get(upRes, 'error')));
          }
        });
      } else {
        resolve();
      }
    });
  }).then(() => {
    //populateBundle
    //update latest personalInfo data to client profile
    let _populatePersonalInfoToProfile = function (personalInfo, profile, cb) {
      logger.log('INFO: saveForm - _populatePersonalInfoToProfile', appId);

      let toUpdate = false;
      for (let i = 0; i < commonApp.CLIENT_FIELDS_TO_UPDATE.length; i++) {
        let field = commonApp.CLIENT_FIELDS_TO_UPDATE[i];
        if (personalInfo[field] !== profile[field]) {
          toUpdate = true;
          profile[field] = personalInfo[field];
        }
      }

      if (toUpdate) {
        result.updIds.push(profile._id);
        dao.updDoc(profile._id, profile, cb);
      } else {
        cb(true);
      }
    };

    let updProfilePromises = [];
    updProfilePromises.push(
      new Promise((resolve, reject) => {
        let pPersonalInfo = _.get(changedValues, 'proposer.personalInfo');
        let cid = _.get(pPersonalInfo, 'cid');
        cDao.getProfile(cid, false).then((profile) => {
          if (profile && !profile.error) {
            _populatePersonalInfoToProfile(pPersonalInfo, profile, (upRes) => {
              if (upRes && !upRes.error) {
                resolve();
              } else {
                reject(new Error('Fail to update Profile' + _.get(profile, 'id') + ',' + _.get(profile, 'error')));
              }
            });
          } else {
            reject(new Error('Fail to get Profile' + _.get(profile, 'id') + ',' + _.get(profile, 'error')));
          }
        });
      })
    );

    let insured = _.get(changedValues, 'insured');
    if (insured && insured.length > 0) {
      _.forEach(insured, (la) => {
        updProfilePromises.push(
          new Promise((resolve, reject) => {
            let iPersonalInfo = _.get(la, 'personalInfo');
            let cid = _.get(la, 'personalInfo.cid');
            cDao.getProfile(cid, false).then((profile) => {
              if (profile && !profile.error) {
                _populatePersonalInfoToProfile(iPersonalInfo, profile, (upRes) => {
                  if (upRes && !upRes.error) {
                    resolve();
                  } else {
                    reject(new Error('Fail to update Profile' + _.get(profile, 'id') + ',' + _.get(profile, 'error')));
                  }
                });
              } else {
                reject(new Error('Fail to get Profile' + _.get(profile, 'id') + ',' + _.get(profile, 'error')));
              }
            });
          })
        );
      });
    }

    return Promise.all(updProfilePromises);
  }).then(() => {
    result.application = _.cloneDeep(cache.application);
    result.application.applicationForm.values = changedValues;
    //reset appStep if proposer is not completed but it's in Signature page now
    if (!_.get(changedValues, 'proposer.extra.isCompleted', false) && result.application.appStep === commonApp.EAPP_STEP.SIGNATURE) {
      result.application.appStep = commonApp.EAPP_STEP.APPLICATION;
    }

    let childIds = _.get(result.application, 'childIds', []);
    let isGeneratePolicyNumber = getPolicyNumber && !result.application.isPolicyNumberGenerated;

    logger.log('INFO: _saveForm - getPolicyNumber isGen=', isGeneratePolicyNumber, 'for', appId);
    if (!isGeneratePolicyNumber) {
      result.getPolicyNumberResult = getPolicyNumber;
      return _getChildApplications(childIds).then((childApps) => {
        return {
          parentApp: result.application,
          childApps
        };
      });
    } else {
      return _getPolicyNumbers(result.application).then((res) => {
        result.getPolicyNumberResult = res.success;
        result.application = res.parentApp;
        return {
          parentApp: res.parentApp,
          childApps: res.childApps
        };
      });
    }
  }).then((apps) => {
    if (!isFaChannel) {
      let { parentApp } = apps;
      return bDao.updateApplicationFnaAnsToApplications(parentApp).then((results) => {
        return apps;
      });
    } else {
      return apps;
    }
  }).then((apps) => {
    _updateApplicationCompletedStep(apps.parentApp);
    _saveParentApplicationValuesToChild(apps.parentApp, apps.childApps);
    return [apps.parentApp, ...apps.childApps];
  }).then((applications) => {
    let updAppPromises = applications.map((app) => {
      return new Promise((resolve, reject) => {
        app.lastUpdateDate = moment().toISOString();
        logger.log(`Payment Data Checking :: ${app.id}, --shield/application --_saveAppForm -- Previous Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Next Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Current Policy Number :: ${_.get(app, 'policyNumber')}`);
        logger.log(`INFO: ShieldApplicationUpdate new - [appid:${_.get(app, 'id')}; pCid:${_.get(app, 'pCid')}; bundleId:${_.get(app, 'bundleId')}; isSameAddr:${_.get(app, 'applicationForm.values.proposer.personalInfo.isSameAddr')}; fn:_saveAppForm]`);
        appDao.updApplication(app.id, app, (res) => {
          if (res && !res.error) {
            resolve(res);
          } else {
            reject(new Error('Fail to update application ' + app.id + ' ' + _.get(res, 'error')));
          }
        });
      });
    });
    return Promise.all(updAppPromises);
  }).then((upResults) => {
    result.success = true;
    return result;
  });
};

var _genFnaReportPdf = function(session, application, bundle) {
  logger.log('INFO: _genFnaReportPdf', _.get(application, 'id'));
  let pCid = _.get(bundle, 'pCid');

  return new Promise((resolve, reject) => {
    needsHandler.generateFNAReport({cid: pCid}, session, function(pdfResult) {
      logger.log(`FNA Signature Checking Bundle ID :: ${_.get(bundle, 'id')}, -- shield/application _genFnaReportPdf :: FNA Status :: ${_.get(bundle, 'status')}`);
      logger.log(`FNA Signature Checking Bundle ID :: ${_.get(bundle, 'id')}, -- shield/application _genFnaReportPdf :: FNA Signed :: ${_.get(bundle, 'isFnaReportSigned')}`);
      logger.log(`FNA Signature Checking Bundle ID :: ${_.get(bundle, 'id')}, -- shield/application _genFnaReportPdf :: Applications Stored ::`, _.get(bundle, 'applications'));
      if (!pdfResult.fnaReport){
        reject(new Error('Fail to generate FNA report'));
      } else {
        dao.uploadAttachmentByBase64(bundle.id, commonApp.PDF_NAME.FNA, bundle._rev, pdfResult.fnaReport, 'application/pdf', function(res) {
          if (res && !res.error) {
            bundle._rev = res.rev;
            resolve(bundle);
          } else {
            reject(new Error('Fail to upload FNA report ' + _.get(res, 'id') + ' ' + _.get(res, 'error')));
          }
        });
      }
    });
  }).then(() => {
    return bDao.updateStatus(pCid, bDao.BUNDLE_STATUS.START_GEN_PDF);
  });
};

var _getAppFormPdfTemplates = function(application, isLa) {
  let appFormTemplate = null;

  return new Promise((resolve, reject) => {
    dao.getDoc(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, (mapping)=> {
      if (mapping && !mapping.error && mapping[application.quotation.baseProductCode]) {
        appFormTemplate = mapping[application.quotation.baseProductCode].appFormTemplate;
        resolve();
      } else {
        reject(new Error('Fail to get ' + commonApp.TEMPLATE_NAME.APP_FORM_MAPPING));
      }
    });
  }).then(() => {
    return new Promise((resolve, reject) => {
      dao.getDocFromCacheFirst(appFormTemplate.main, function(main) {
        if (main && !main.error) {
          var tplFiles = [];
          commonApp.getReportTemplates(tplFiles, appFormTemplate.template, 0, function() {
            if (tplFiles.length) {
              resolve([main, tplFiles]);
            } else {
              reject(new Error('Fail to get app form template template'));
            }
          });
        } else {
          reject(new Error('Fail to get app form template main' + _.get(main, 'error')));
        }
      });
    });
  });
};

var _formatAppFormPdfData = function(appFormValues, ccy) {
  let __formatPlanList = function(planList) {
    _.forEach(planList, (plan, i) => {
      for (let key in plan) {
        if (key === 'halfYearPrem' || key === 'monthPrem' || key === 'premium' || key === 'quarterPrem' || key === 'yearPrem') {
          plan[key] = utils.getCurrency(plan[key], utils.getCurrencySign(ccy), 2);
        }
      }
    });
  };

  //format currency in planList
  // for proposer is la
  if (_.get(appFormValues, 'proposer.extra.isPhSameAsLa') === 'Y') {
    let basicPlanList = _.get(appFormValues, 'proposer.groupedPlanDetails.basicPlanList', []);
    if (basicPlanList.length > 0) {
      __formatPlanList(basicPlanList);
    }

    let riderPlanList = _.get(appFormValues, 'proposer.groupedPlanDetails.riderPlanList', []);
    if (riderPlanList.length > 0) {
      __formatPlanList(riderPlanList);
    }
  }

  let insured = _.get(appFormValues, 'insured', []);
  _.forEach(insured, (la, i) => {
    let basicPlanList = _.get(appFormValues, `insured[${i}].groupedPlanDetails.basicPlanList`, []);
    if (basicPlanList.length > 0) {
      __formatPlanList(basicPlanList);
    }

    let riderPlanList = _.get(appFormValues, `insured[${i}].groupedPlanDetails.riderPlanList`, []);
    if (riderPlanList.length > 0) {
      __formatPlanList(riderPlanList);
    }
  });

  //Uppercase personalInfo
  for (let key in appFormValues.proposer.personalInfo) {
    let info = appFormValues.proposer.personalInfo[key];
    if (typeof info === 'string' && key !== 'email' && key !== 'isSameAddr') {
      appFormValues.proposer.personalInfo[key] = info.toUpperCase();
    }
  }
  for (let i = 0; i < appFormValues.insured.length; i ++) {
    for (let key in appFormValues.insured[i].personalInfo) {
      let info = appFormValues.insured[i].personalInfo[key];
      if (typeof info === 'string' && key !== 'email' && key !== 'isSameAddr') {
        appFormValues.insured[i].personalInfo[key] = info.toUpperCase();
      }
    }
  }
  //Uppercase payor & trusted individual information
  for (let key in appFormValues.proposer.declaration) {
    let info = appFormValues.proposer.declaration[key];
    if (typeof info === 'string' && commonApp.CAPITAL_LETTER_IDS.indexOf(key) >= 0) {
      appFormValues.proposer.declaration[key] = info.toUpperCase();
    } else if (key === 'trustedIndividuals') {
      appFormValues.proposer.declaration.trustedIndividuals.fullName = _.toUpper(appFormValues.proposer.declaration.trustedIndividuals.fullName);
    }
  }
};

var _genAppFormPdfSectionVisible = function(values, isProposer, callback) {
  let showQuestions = {
    residency: {
      question: []
    },
    foreigner: {
      question: []
    },
    policies: {
      question: []
    },
    insurability: {
      question: []
    },
    declaration: {
      question: []
    }
  };

  for (let menuItemKey in showQuestions) {
    if (isProposer) {
      commonApp.checkAppFormByPerson(values.proposer, menuItemKey, showQuestions);
    } else {
      values.insured.forEach(function(la, index) {
        commonApp.checkAppFormByPerson(la, menuItemKey, showQuestions);
      });
    }
  }

  callback(showQuestions);
};

var _genAppFormPdfData = function(application, bundle, template, iCid) {
  let lang = 'en';
  let trigger = {};
  let appTemplate = template.items[0]; //contain items of menuSection
  let appOrgValues = _.cloneDeep(application.applicationForm.values);
  let appNewValues = _.cloneDeep(application.applicationForm.values);
  let isProposer = !_.isString(iCid);
  let ccy = _.get(appOrgValues, 'proposer.extra.ccy');


  logger.log('INFO: _genAppFormPdfData - replaceAppFormValuesByTemplate - start', application._id, iCid);
  commonApp.replaceAppFormValuesByTemplate(appTemplate, appOrgValues, appNewValues, trigger, [], lang, ccy);
  logger.log('INFO: _genAppFormPdfData - replaceAppFormValuesByTemplate - end', application._id, iCid);

  if (!isProposer) {
    //insured
    appOrgValues.insured = _.filter(appOrgValues.insured, (la) => { return _.get(la, 'personalInfo.cid') === iCid; });
    appNewValues.insured = _.filter(appNewValues.insured, (la) => { return _.get(la, 'personalInfo.cid') === iCid; });
  }
  appNewValues.lang = lang;
  appNewValues.baseProductCode = application.quotation.baseProductCode;
  appNewValues.iCidMapping = application.iCidMapping;
  appNewValues.showLaSignature = commonApp.checkLaIsAdult(application);
  appNewValues.agent = application.quotation.agent;

  if (application.agentCodeDisp) {
    appNewValues.agent.agentCodeDisp = application.agentCodeDisp;
  }
  if (application.nameOfOrganization){
    appNewValues.agent.company = application.nameOfOrganization;
  } else {
    appNewValues.agent.company = '';
  }
  appNewValues.payment = application.payment;
  appNewValues.isProposer = isProposer;
  if (application.quotation.fund) {
    appNewValues.fund = application.quotation.fund;
  }

  _formatAppFormPdfData(appNewValues, ccy);

  for (let key in appOrgValues) {
    if (key === 'proposer') {
      logger.log('INFO: _genAppFormPdfData - removeAppFormValuesByTrigger: handle', key, application._id);
      commonApp.removeAppFormValuesByTrigger(key, appOrgValues[key], appNewValues[key], appOrgValues[key], trigger[key], true,appOrgValues);
    } else if (key === 'insured') {
      for (let i = 0; i < appOrgValues[key].length; i ++) {
        let iOrgValues = appOrgValues[key][i];
        let iNewValues = appNewValues[key][i];
        let iTrigger = trigger[key][i];
        logger.log('INFO: _genAppFormPdfData - removeAppFormValuesByTrigger: handle', key, i, application._id);
        commonApp.removeAppFormValuesByTrigger(key, iOrgValues, iNewValues, iOrgValues, iTrigger, true,appOrgValues);
      }
    }
  }

  //add age to insured in for shield case, which is equal to quotation age
  let quotationProposerAge = _.get(application, 'quotation.pAge', 0);
  appNewValues.proposer.personalInfo.quotationAge = quotationProposerAge;

  let quotationInsured = _.get(application, 'quotation.insureds', []);
  for (let i = 0; i < appNewValues.insured.length; i++) {
    let personalInfo = appNewValues.insured[i].personalInfo;
    if (personalInfo) {
      let cid = appNewValues.insured[i].personalInfo.cid;
      if (quotationInsured[cid] && quotationInsured[cid].iAge) {
        let quotationAge = quotationInsured[cid].iAge;
        appNewValues.insured[i].personalInfo.quotationAge = quotationAge;
      }
    }
  }

  //Grouping HEALTH data into one array
  let groupingHealthTableData = function(person, groupHealthKey, groupedKey) {
    let groupHealthData = [];

    for (let i = 0; i < groupHealthKey.length; i ++) {
      let groupHealthDataKey = groupHealthKey[i] + '_DATA';

      let healthDataArray = _.get(person, 'insurability.' + groupHealthDataKey);
      if (healthDataArray) {
        for (let j = 0; j < healthDataArray.length; j ++) {
          let healthDataRow = healthDataArray[j];
          let groupedRow = {};
          for (let key in healthDataRow) {
            let postfix = key.replace(groupHealthKey[i], '');

            groupedRow['HEALTH_GROUP' + postfix] = healthDataRow[key];
            groupedRow.DATATABLE = groupHealthDataKey;
            groupedRow.SHOW_QN = j === 0 ? 'Y' : 'N';
            groupedRow.ROW_COUNT = healthDataArray.length;
          }
          groupHealthData.push(groupedRow);
        }
      }
    }

    if (groupHealthData.length > 0) {
      logger.log('INFO: genAppFormPdfData - groupingHealthTableData: length', groupHealthData.length, application._id, iCid);
      person.insurability[groupedKey] = groupHealthData;
    }
  };

  return new Promise((resolve) => {

    if (isProposer) {
      groupingHealthTableData(appNewValues.proposer, commonApp.APP_FORM_SHIELD_GROUP_HEALTH_QN, 'HEALTH_GROUP_DATA');
      groupingHealthTableData(appNewValues.proposer, commonApp.APP_FORM_SHIELD_GROUP_HEALTH_F_QN, 'HEALTH_GROUP_DATA2');
    } else {
      let { insured } = appNewValues;
      if (insured) {
        for (let i = 0; i < insured.length; i ++) {
          groupingHealthTableData(appNewValues.insured[i], commonApp.APP_FORM_SHIELD_GROUP_HEALTH_QN, 'HEALTH_GROUP_DATA');
          groupingHealthTableData(appNewValues.insured[i], commonApp.APP_FORM_SHIELD_GROUP_HEALTH_F_QN, 'HEALTH_GROUP_DATA2');
        }
      }
    }

    _genAppFormPdfSectionVisible(appNewValues, isProposer, (showQuestions) => {
      appNewValues.showQuestions = showQuestions;
      resolve();
    });
  }).then(() => {
    return new Promise((resolve, reject) => {
      let reportData = {
        root: {}
      };

      dao.getDoc(bundle.fna[nDao.ITEM_ID.PDA], (pda)=>{
        if (pda && !pda.error) {
          logger.log('INFO: _genAppFormPdfData - populate PDA data', application._id, iCid);
          //For not FA channel
          if (pda.ownerConsentMethod) {
            let ownerConsentMethodList = pda.ownerConsentMethod.split(',');

            for (let i = 0; i < ownerConsentMethodList.length; i ++) {
              let method = ownerConsentMethodList[i];
              if (method === 'phone') {
                appNewValues.proposer.declaration.PDPA01a = 'Yes';
              } else if (method === 'text') {
                appNewValues.proposer.declaration.PDPA01b = 'Yes';
              } else if (method === 'fax') {
                appNewValues.proposer.declaration.PDPA01c = 'Yes';
              }
            }
          }
        }

        reportData.root.reportData = appNewValues;
        reportData.root.originalData = appOrgValues;

        resolve(reportData);
      });
    });
  });
};

var _genAppFormProposerPdf = function(lang, application, bundle, template) {
  return new Promise((resolve) => {
    resolve(!_.get(application, 'isAppFormProposerSigned'));
  }).then((isGenPdf) => {
    logger.log('INFO: _genAppFormProposerPdf', _.get(application, 'id'), 'isGenPdf=', isGenPdf);
    if (!isGenPdf) {
      return isGenPdf;
    }

    return _getAppFormPdfTemplates(application, false).then((appFormTemplates) => {
      // handle mutiple template
      let reportTemplates = [];
      for (let i = 0; i < appFormTemplates.length; i ++) {
        if (appFormTemplates[i] instanceof Array) {
          reportTemplates.push.apply(reportTemplates, appFormTemplates[i]);
        } else {
          reportTemplates.push(appFormTemplates[i]);
        }
      }

      return _genAppFormPdfData(application, bundle, template).then((reportData) => {
        return new Promise((resolve, reject) => {
          PDFHandler.getAppFormPdf(reportData, reportTemplates, lang, function(pdfStr) {
            dao.uploadAttachmentByBase64(application._id, commonApp.PDF_NAME.eAPP, application._rev, pdfStr, 'application/pdf', (res) => {
              if (res && !res.error) {
                application._rev = res.rev;
                resolve(isGenPdf);
              } else {
                reject(new Error('Fail to update App Form PDF ' + application._id + ' ' + _.get(res, 'error')));
              }
            });
          });
        });
      });
    });
  });
};

var _genAppFormInsuredPdf = function(lang, application, bundle, template) {
  let genPdfPromise = application.iCids.map((iCid, index) => {
    return new Promise((resolve) => {
      let isCompletedInsured = _.get(application, `applicationForm.values.insured[${index}].extra.isCompleted`, false);
      resolve(!_.get(application, `isAppFormInsuredSigned[${index}]`) && isCompletedInsured && application.iCids.length > 0);
    }).then((isGenPdf) => {
      logger.log('INFO: _genAppFormInsuredPdf', _.get(application, 'id'), iCid, 'isGenPdf=', isGenPdf);
      if (!isGenPdf) {
        return {isGenPdf, iCid};
      }

      return _getAppFormPdfTemplates(application, true).then((appFormTemplates) => {
        // handle mutiple template
        let reportTemplates = [];
        for (let i = 0; i < appFormTemplates.length; i ++) {
          if (appFormTemplates[i] instanceof Array) {
            reportTemplates.push.apply(reportTemplates, appFormTemplates[i]);
          } else {
            reportTemplates.push(appFormTemplates[i]);
          }
        }

        return _genAppFormPdfData(application, bundle, template, iCid).then((reportData) => {
          return new Promise((resolve, reject) => {
            PDFHandler.getAppFormPdf(reportData, reportTemplates, lang, (pdfStr) => {
              resolve({isGenPdf, iCid, pdfStr});
            });
          });
        });
      });
    });
  });

  return Promise.all(genPdfPromise).then((results) => {
    let _uploadAttachment = function(index, callback) {
      if (index >= results.length) {
        logger.log('INFO: _genAppFormInsuredPdf', _.get(application, 'id'), '- _uploadAttachment complete');
        callback();
      } else {
        logger.log('INFO: _genAppFormInsuredPdf', _.get(application, 'id'), '- _uploadAttachment', index + 1, 'of', results.length);
        if (_.get(results, `[${index}].isGenPdf`)) {
          let {iCid, pdfStr} = results[index];
          dao.uploadAttachmentByBase64(application._id, _signature.getAppFormAttachmentName(iCid), application._rev, pdfStr, 'application/pdf', (res) => {
            if (res && !res.error) {
              application._rev = res.rev;
              results[index].success = true;
              _uploadAttachment(index + 1, callback);
            } else {
              results[index].success = false;
              _uploadAttachment(results.length, callback);
            }
          });
        } else {
          logger.log('INFO: _genAppFormInsuredPdf', _.get(application, 'id'), '- empty result[' + index + ']');
          results[index].success = true;
          _uploadAttachment(index + 1, callback);
        }
      }
    };

    return new Promise((resolve, reject) => {
      _uploadAttachment(0, () => {
        let filterUpResults = _.filter(results, (res) => { return !res.success; });
        if (filterUpResults.length > 0) {
          reject(new Error('Fail to upload attachment to application ' + application._id));
        } else {
          resolve(results);
        }
      });
    });
  });
};

var _generatePdf = function(session, data) {
  let { appId } = data;
  let isFaChannel = session.agent.channel.type === 'FA';
  let lang = 'en';
  let agentCodeDisp = session.agent.agentCodeDisp;
  let upline2Code = session.agent.rawData.upline2Code;

  return _getApplication(appId).then((app) => {
    return bDao.getCurrentBundle(app.pCid).then((bundle) => {
      if (bundle && !bundle.error) {
        return {
          application: app,
          bundle: bundle
        };
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then((cache) => {
    let {application, bundle} = cache;
    if (agentCodeDisp) {
      application.agentCodeDisp = agentCodeDisp;
    }
    return _getNameOfOrganization(upline2Code).then((nameOfOrganization) => {
      if (nameOfOrganization) {
        application.nameOfOrganization = nameOfOrganization;
      }
      return _getAppFormTemplate(application.quotation).then((templateRes) => {
        return _genAppFormProposerPdf(lang, application, bundle, templateRes.template).then((isPhGenPdf) => {
          return _genAppFormInsuredPdf(lang, application, bundle, templateRes.template).then((isLaGenPdfs) => {
            return {
              proposer: isPhGenPdf,
              insured: isLaGenPdfs
            };
          });
        });
      });
    }).then((isGenPdfResult) => {
      return _getApplication(appId).then((app) => {
        if (app.appStep < commonApp.EAPP_STEP.SIGNATURE) {
          app.appStep = commonApp.EAPP_STEP.SIGNATURE;
        }

        if (!_.get(app, 'applicationForm.values.proposer.extra.isPdfGenerated') && isGenPdfResult.proposer) {
          _.set(app, 'applicationForm.values.proposer.extra.isPdfGenerated', true);
        }

        _.forEach(_.get(app, 'applicationForm.values.insured', []), (insured, index) => {
          let isLaGenPdfResult = _.filter(isGenPdfResult.insured, (la) => { return la.iCid === _.get(insured, 'personalInfo.cid'); });

          if (!_.get(insured, 'extra.isPdfGenerated') && isLaGenPdfResult.length > 0 && isLaGenPdfResult[0].isGenPdf) {
            _.set(insured, 'extra.isPdfGenerated', true);
          }
        });

        _updateApplicationCompletedStep(app);

        app.lastUpdateDate = moment().toISOString();
        return new Promise((resolve, reject) => {
          logger.log(`Payment Data Checking :: ${app.id}, --shield/application --_generatePdf -- Previous Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Next Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Current Policy Number :: ${_.get(app, 'policyNumber')}`);
          appDao.updApplication(app.id, app, (res) => {
            if (res && !res.error) {
              app._rev = res.rev;
              resolve(app);
            } else {
              reject(new Error('Fail to update application ' + app.id + ' ' + _.get(res, 'error')));
            }
          });
        });
      });
    }).then((newApp) => {
      if (isFaChannel) {
        return bDao.updateStatus(newApp.pCid, bDao.BUNDLE_STATUS.START_GEN_PDF).then((newBundle) => {
          return newApp;
        });
      } else if (!bundle.isFnaReportSigned) {
        return _genFnaReportPdf(session, application, bundle).then((newBundle) => {
          return newApp;
        });
      } else {
        return newApp;
      }
    }).then((newApp) => {
      return {success: true, application: newApp};
    }).catch((error) => {
      logger.error('ERROR: _generatePdf', appId, error);
      throw error;
    });
  });
};

const _updateApplicationCompletedStep = function(app) {
  let appCompletedStep = -1;

  //Check step application
  let phIsCompleted = _.get(app, 'applicationForm.values.proposer.extra.isCompleted', false);
  let laIsComplated = true;
  let phIsPdfGenerated = _.get(app, 'applicationForm.values.proposer.extra.isPdfGenerated', false);
  let laIsPdfGenerated = true;

  let insured = _.get(app, 'applicationForm.values.insured', []);
  _.forEach(insured, (la) => {
    laIsComplated = laIsComplated && _.get(la, 'extra.isCompleted', false);
    laIsPdfGenerated = laIsPdfGenerated && _.get(la, 'extra.isPdfGenerated', false);
  });

  if (phIsCompleted && laIsComplated && phIsPdfGenerated && laIsPdfGenerated) {
    appCompletedStep++;
  }

  if (appCompletedStep === commonApp.EAPP_STEP.APPLICATION) {
    //Check step signature
    if (app.isAppFormProposerSigned && (app.isAppFormInsuredSigned.length === 0 || app.isAppFormInsuredSigned.indexOf(false) < 0)) {
      appCompletedStep++;
    }
  }

  if (appCompletedStep === commonApp.EAPP_STEP.SIGNATURE) {
    //Check step payment
    if (_.get(app, 'payment.isCompleted', false)) {
      appCompletedStep++;
    }
  }

  if (appCompletedStep === commonApp.EAPP_STEP.PAYMENT) {
    if (_.get(app, 'isSubmittedStatus', false)) {
      appCompletedStep++;
    }
  }

  app.appCompletedStep = appCompletedStep;
};

//=ApplicationHandler.saveForm
module.exports.saveAppForm = function(data, session, callback) {
  let { appId } = data;
  logger.log('INFO: saveAppForm - start', appId);

  _saveAppForm(data, session).then((result) => {
    dao.updateViewIndex('main', 'summaryApps');
    logger.log('INFO: saveAppForm - end [RETURN=100]', appId);
    callback(result);
  }).catch((error) => {
    logger.error('ERROR: saveAppForm - end [RETURN=-101]', appId, error);
    // callback({success: false});
  });
};

module.exports.goNextStep = function(data, session, callback) {
  let { appId, currIndex, nextIndex } = data;
  let cache = {
    application: {},
    saveResult: {},
    beforeNextResult: {}
  };
  let isFaChannel = session.agent.channel.type === 'FA';

  let saveProcess = [];
  if (currIndex === commonApp.EAPP_STEP.APPLICATION) {
    saveProcess.push(_saveAppForm(data, session));
  } else if (currIndex === commonApp.EAPP_STEP.PAYMENT) {
    saveProcess.push(_payment.savePaymentMethods(data));
  } else if (currIndex === commonApp.EAPP_STEP.SUBMISSION) {
    saveProcess.push(_submission.saveSubmissionValuesPromise(data, session));
  }

  Promise.all(saveProcess).then((saveResult) => {
    logger.log('INFO: goNextStep - saveProcess completed', appId);
    if (saveResult.length > 0 && !saveResult[0]) {
      throw new Error('Fail to complete save process for step ' + currIndex + '->' + nextIndex);
    }
    cache.saveResult = saveResult[0];

    let beforeNextProcess = [];
    if (nextIndex === commonApp.EAPP_STEP.APPLICATION) {
      beforeNextProcess.push(_getApplication(appId));
    } else if (nextIndex === commonApp.EAPP_STEP.SIGNATURE) {
      beforeNextProcess.push(_generatePdf(session, data));
    } else if (nextIndex === commonApp.EAPP_STEP.PAYMENT) {
      beforeNextProcess.push(_payment.preparePayment(data));
    } else if (nextIndex === commonApp.EAPP_STEP.SUBMISSION) {
      beforeNextProcess.push(_submission.prepareSubmission(session, data));
    }

    return Promise.all(beforeNextProcess);
  }).then((beforeNextResult) => {
    logger.log('INFO: goNextStep - beforeNextProcess completed', appId);
    if (beforeNextResult.length > 0 && !beforeNextResult[0]) {
      throw new Error('Fail to complete before Next process for step ' + currIndex + '->' + nextIndex);
    }
    cache.beforeNextResult = beforeNextResult[0];

    let nextProcess = [];
    if (nextIndex === commonApp.EAPP_STEP.APPLICATION) {
      cache.application = _.get(beforeNextResult, '[0]');
      nextProcess.push(
        _getAppFormTemplateWithOptionList(cache.application).then((templateRes) => {
          return Object.assign(templateRes, {success:true});
        })
      );
    } else if (nextIndex === commonApp.EAPP_STEP.SIGNATURE) {
      cache.application = _.get(beforeNextResult, '[0].application');
      nextProcess.push(_signature.getSignatureInitUrl(session, appId));
    } else if (nextIndex === commonApp.EAPP_STEP.PAYMENT) {
      cache.application = _.get(beforeNextResult, '[0]');
      nextProcess.push(_payment.getPaymentTemplate(cache.application.isSubmittedStatus, _.get(cache.application, 'payment.totCashPortion', 0), isFaChannel));
    } else if (nextIndex === commonApp.EAPP_STEP.SUBMISSION) {
      cache.application = _.get(beforeNextResult, '[0]');
      nextProcess.push(_submission.getSubmissionTemplate(cache.application.isSubmittedStatus));
    }

    return Promise.all(nextProcess);
  }).then((results) => {
    logger.log('INFO: goNextStep - nextProcess completed', appId);
    if (results.length > 0 && !results[0].success) {
      throw new Error('Fail to complete Next process for step ' + currIndex + '->' + nextIndex);
    }
    let result = results[0];

    logger.log('INFO: goNextStep - end [RETURN=100]', appId);
    if (nextIndex === commonApp.EAPP_STEP.APPLICATION) {
      callback({success: true, application: cache.application, template: result.template});
    } else if (nextIndex === commonApp.EAPP_STEP.SIGNATURE) {
      callback({success: true, application: cache.application, signature: result.signature, updIds: cache.saveResult.updIds, warningMsg: result.warningMsg});
    } else if (nextIndex === commonApp.EAPP_STEP.PAYMENT) {
      callback({success: true, application: cache.application, template: result.template});
    } else if (nextIndex === commonApp.EAPP_STEP.SUBMISSION) {
      callback({success: true, application: cache.application, template: result.template});
    }
  }).catch((error) => {
    logger.error('ERROR: goNextStep - end [RETURN=-101]', appId, error);
    // callback({success: false});
  });
};

module.exports.invalidateApplication = function(data, session, callback) {
  let {appId} = data;
  logger.log('INFO: invalidateApplication - start', appId);
  _getApplication(appId).then((app) => {
    return bDao.onInvalidateApplicationById(app.quotation.pCid, appId);
  }).then((resp)=>{
    logger.log('INFO: invalidateApplication - end [RETURN=100]', appId);
    callback({success: true});
  }).catch((error) => {
    logger.error('ERROR: invalidateApplication - end [RETURN=-100]', appId, error);
    // callback({success: false});
  });
};

module.exports.setSignExpiryShown = function(data, session, callback) {
  let {appId} = data;
  logger.log('INFO: setSignExpiryShown - start', appId);

  _getApplication(appId).then((app) => {
    app.signExpiryShown = true;
    app.lastUpdateDate = moment().toISOString();

    return new Promise((resolve, reject) => {
      logger.log(`Payment Data Checking :: ${app.id}, --shield/application --setSignExpiryShown -- Previous Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Next Payment Policy Number :: ${_.get(app, 'payment.proposalNo')}, Current Policy Number :: ${_.get(app, 'policyNumber')}`);
      appDao.updApplication(app._id, app, (res) => {
        if (res && !res.error) {
          resolve();
        } else {
          reject(new Error('Fail to update application ' + app._id + ' ' + _.get(res, 'error')));
        }
      });
    });
  }).then(() => {
    logger.log('INFO: setSignExpiryShown - end [RETURN=100]', appId);
    callback({success: true});
  }).catch((error) => {
    logger.error('ERROR: setSignExpiryShown - end [RETURN=-100]', appId, error);
  });
};

module.exports.getApplication = _getApplication;
module.exports.getChildApplications = _getChildApplications;
module.exports.getAppFormTemplate = _getAppFormTemplate;
module.exports.getAppFormTemplateWithOptionList = _getAppFormTemplateWithOptionList;
module.exports.saveParentApplicationValuesToChild = _saveParentApplicationValuesToChild;
module.exports.updateApplicationCompletedStep = _updateApplicationCompletedStep;
module.exports.getPolicyNumbers = _getPolicyNumbers;
module.exports.checkCrossAge = _checkCrossAge;
