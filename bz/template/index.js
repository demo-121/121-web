const SUBMISSION_NOTIFICATION = require('./submissionNotification.json');
const ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION = require('./additionalDocumentUploadedNotification.json');
const ADDITIONAL_DOCUMENT_UPLOADED_FA_NOTIFICATION = require('./fa_additionalDocumentUploadedNotification.json');
const DIRECTOR_NOTIFICATION = require('./directorNotification.json');
const CASE_APPROVAL_REMINDER = require('./caseApprovalReminder.json');
const APPROVAL_EXPIRY_NOTIFICATION = require('./approvalExpiryNotification.json');
const APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT = require('./approvedNotificationForNotSelectedAgent.json');
const APPROVED_NOTIFICATION_FOR_SELECTED_AGENT = require('./approvedNotificationForSelectedAgent.json');
const REJECTED_NOTIFICATION = require('./rejectedNotification.json');
const SUBMISSION_NOTIFICATION_FOR_DIRECTOR = require('./submissionNotificationForDirector.json');
const SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION = require('./secondaryProxyAssignmentNotification.json');
const PENDING_FOR_DOCUMENT_NOTIFICATION = require('./pendingForDocumentNotification.json');
const SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS = require('./supervisorApprovalEmailAllDocuments.json');
const APPROVED_NOTIFICATION_FOR_FAFIRM = require('./approvedNotificationForFAFirm.json');
const APPROVED_NOTIFICATION_FOR_FASUPERVISOR = require('./approvedNotificationForFASupervisor.json');
const APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM = require('./approvedSupervisorNotificationForFAAdmin.json');
const REJECTED_NOTIFICATION_SUPERVISOR_TO_FAFIRM = require('./rejectedNotification_Supervisor_To_FAAdmin.json');
const REJECTED_NOTIFICATION_FAFIRM = require('./rejectedNotification_FAAdmin.json');
const ADDITIONALDOC_HEALTHDECLARATION = require('./additionalDocHealthDeclarationNotification.json');
const ABNORMAL_APPROVAL_CASE_NOTIFICATION = require('./abnormalApprovalCase.json');
const ABNORMAL_SUPP_DOC_CASE_NOTIFICATION = require('./abnormalSuppDocCase.json');
const AXA_LOGO = require('./axa_logo.json');

module.exports = {
  SUBMISSION_NOTIFICATION,
  ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION,
  DIRECTOR_NOTIFICATION,
  CASE_APPROVAL_REMINDER,
  APPROVAL_EXPIRY_NOTIFICATION,
  APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT,
  APPROVED_NOTIFICATION_FOR_SELECTED_AGENT,
  REJECTED_NOTIFICATION,
  SUBMISSION_NOTIFICATION_FOR_DIRECTOR,
  SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION,
  PENDING_FOR_DOCUMENT_NOTIFICATION,
  SUPERVISOR_APPROVAL_EMAIL_ALL_DOCUMENTS,
  APPROVED_NOTIFICATION_FOR_FAFIRM,
  APPROVED_NOTIFICATION_FOR_FASUPERVISOR,
  APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM,
  REJECTED_NOTIFICATION_SUPERVISOR_TO_FAFIRM,
  REJECTED_NOTIFICATION_FAFIRM,
  AXA_LOGO,
  ADDITIONAL_DOCUMENT_UPLOADED_FA_NOTIFICATION,
  ADDITIONALDOC_HEALTHDECLARATION,
  ABNORMAL_APPROVAL_CASE_NOTIFICATION,
  ABNORMAL_SUPP_DOC_CASE_NOTIFICATION
};
