const _ = require('lodash');
// const config = require('../config');
var CryptoJS = require("crypto-js");
var request = require('request');
var logger = global.logger || console;

var requestWithInternetProxy = function(uri, method, headers, data, callback, skipProxy) {
  try {
    request({
        uri: uri,
        method: method || 'POST',
        headers: headers,
        proxy: null,
        timeout: 60000,
        rejectUnauthorized: false,
        followRedirect: true,
        maxRedirects: 10,
        body: data?data:null
    }, function(error, response, body) {
      if (!error && body) {
        if (callback && typeof callback == 'function') callback(body);
      } else if (error) {
        logger.error("requestWithInternetProxy ERROR:", error);
        if (callback && typeof callback == 'function') callback({
                                                        success: false,
                                                        error: error
                                                      });
      } else {
        if (callback && typeof callback == 'function') callback({
          success: true
        });
      }
    });
  } catch(e) {
    logger.error("requestWithInternetProxy Exception:", e);
    if (callback && typeof callback == 'function') {
      callback({
        success: false,
        error: e
      });
    }
  }
}

let _callApiComplete = function(path, method, header, data, skipProxy, callback) {
  var url = global.config.host.api.replace(/\/$/, '') + '/' + path.replace(/^\//, '');
  let dataStr = ""
  if (data) {
    dataStr = JSON.stringify(data);
  }
  logger.log('call api:', url, dataStr.length);
  requestWithInternetProxy(url, method,
    header,
    dataStr,
    function(body) {
      if (body && !body.error) {
        if (typeof body == 'string') {
          try {
            logger.log('call Api return:', !!body);
            var data = JSON.parse(body);
            if (callback && typeof callback == 'function') callback(data);
          } catch (e) {
            if (callback && typeof callback == 'function') callback(body);
          }
        } else {
          if (callback && typeof callback == 'function') callback(body);
        }
      } else if (body.error) {
        logger.log("ERROR:", body && body.error);
        if (callback && typeof callback == 'function') callback({error: body.error});
      } else {
        if (callback && typeof callback == 'function') callback({});
      }
    }, skipProxy);
}

let _callSpeComplete = function(path, method, header, data, skipProxy, callback) {
  var url = global.config.speAgent.api.replace(/\/$/, '') + '/' + path.replace(/^\//, '');
  let dataStr = ""
  if (data) {
    dataStr = JSON.stringify(data);
  }
  logger.log('call SPE:', url, dataStr.length);
  requestWithInternetProxy(url, method,
    header,
    dataStr,
    function(body) {
      if (body && !body.error) {
        if (typeof body == 'string') {
          try {
            logger.log('call SPE return:', !!body);
            var data = JSON.parse(body);
            if (callback && typeof callback == 'function') callback(data);
          } catch (e) {
            if (callback && typeof callback == 'function') callback(body);
          }
        } else {
          if (callback && typeof callback == 'function') callback(body);
        }
      } else if (body.error) {
        logger.log("ERROR:", body && body.error);
        if (callback && typeof callback == 'function') callback({error: body.error});
      } else {
        if (callback && typeof callback == 'function') callback({});
      }
    }, skipProxy);
}

module.exports.callApi = function(path, data, callback) {
  _callApiComplete(path, 'POST', {"Content-Type": "application/json"}, data, true, callback);
};

module.exports.callApiByGet = function(path, callback) {
  _callApiComplete(path, 'GET', {}, {}, true, callback);
};

module.exports.callApiComplete = _callApiComplete;

module.exports.callSpe = function(path, data, callback) {
  _callSpeComplete(path, 'POST', {"Content-Type": "application/json"}, data, true, callback);
};

module.exports.simplify = function(obj, keys) {
  var newObj = {};

  for (var k in keys) {
    var paths = keys[k].split('/');
    var tnObj = newObj;
    var tObj = obj;
    for (var p = 0; p < paths.length; p++) {
      var path = paths[p];
      var tvalue = tObj[path]
      if (!isEmpty(tvalue)) {
        if (p + 1 == paths.length) {
          tnObj[path] = _.cloneDeep(tvalue);
        } else {
          if (isEmpty(tnObj[path])) {
            tnObj[path] = {}
          }
          tnObj = tnObj[path];
          tObj = tvalue;
        }
      } else {
        break;
      }
    }
  }
  return newObj;
}

module.exports.simplifyBlack = function(obj, keys) {

  var simp = function(cur, paths, p) {
    var path = paths[p];
    var tvalue = cur[path];
    if (!isEmpty(tvalue)) {
      if (p + 1 == paths.length) {
        cur[path] = undefined
      } else {
        if (tvalue instanceof Array) {
          for (var a = 0; a < tvalue.length; a++) {
            if (!isEmpty(tvalue[a])) {
              simp(tvalue[a], paths, p+1);
            }
          }
        } else {
          simp(tvalue, paths, p+1);
        }
      }
    }
  }

  var newObj = _.cloneDeep(obj);

  for (var k in keys) {
    var paths = keys[k].split('/');
    simp(newObj, paths, 0);
  }
  return newObj;
}

var _mask = function(type, val) {
  // mask email
  var tvalue = "";
  var count = val.length
  if (type == 'email') {
    for (var i = 0; i<count; i++) {
      var c = val.charAt(i)
      if (i == 0 || c == '@' || c == '.' || c == count - 1) {
        tvalue += val.charAt(i);
      } else {
        tvalue += "*";
      }
    }
  } else { // or else
    var leng = Math.ceil(count / 2);
    if (leng > 4) leng = 4;
    if (leng < 1) leng = 1;
    for (var i = 0; i<count; i++) {
      if (i < count - leng) {
        tvalue += "*";
      } else {
        tvalue += val.charAt(i);
      }
    }
  }
  return tvalue
}

var mask = function(item, values) {
  var type = item.input || item.type;
  if (item.id == 'iidNo') {
    logger.log('debug: find iidNo', item, values[item.id]);
  }
  if (item.id && item.masked && type
    && (type.toLowerCase() == 'txt' || type.toLowerCase() == 'text')
    && values[item.id]) {
    values[item.id] = _mask(item.subType, values[item.id]);
  }
  var items = item.fields || item.sections || item.questions || item.items || item.subQtn;
  for (var i in items) {
    var subItem = items[i];
    mask(subItem, values);
  }
}
module.exports.mask = mask

var unmask = function(item, newValues, values) {
  var type = item.input || item.type;
  if (item.id && item.masked && type
    && (type.toLowerCase() == 'txt' || type.toLowerCase() == 'text')
    && values[item.id] && newValues[item.id]) {

    var val = values[item.id];
    var maskedValue = _mask(item.subType, values[item.id]);
    // if new value is masked
    if (maskedValue == newValues[item.id]) {
      newValues[item.id] = val; // reset it to existing value
    }
  }
  var items = item.fields || item.sections || item.questions || item.items || item.subQtn;
  for (var i in items) {
    var subItem = items[i];
    unmask(subItem, newValues, values);
  }
}
module.exports.unmask = unmask;

module.exports.encrypt = function(obj, fields) {
  var encryKey = global.config.couchbase_encrytion_key;

  if (encryKey) {
    var encodedObj = _.cloneDeep(obj);

    for (var k in fields) {
      var paths = fields[k].split('/');
      var tObj = encodedObj;
      for (var p = 0; p < paths.length; p++) {
        var path = paths[p];
        var tvalue = tObj[path]
        if (!isEmpty(tvalue)) {
          if (p + 1 == paths.length) {
            if (typeof tvalue != 'string') {
              tvalue = JSON.stringify(tvalue);
            }
            tObj[path] = CryptoJS.AES.encrypt(tvalue, encryKey).toString();
          } else {
            tObj = tvalue;
          }
        } else {
          break;
        }
      }
    }
  }
  return encodedObj;
}

const isEmpty = function(value){
  return (value instanceof Date) || (_.isNumber(value) && value != 0) || _.isEmpty(value);
}

module.exports.isEmpty = isEmpty;

module.exports.decrypt = function(obj, fields) {
  var encryKey = global.config.couchbase_encrytion_key;

  if (encryKey) {
    var decryptObj = _.cloneDeep(obj);
    // logger.log('decrypt:', fields);
    for (var k in fields) {
      var paths = fields[k].split('/');
      var tObj = decryptObj;
      for (var p = 0; p < paths.length; p++) {
        var path = paths[p];
        var tvalue = tObj[path]
        try {
          if (!isEmpty(tvalue)) {
            if (p + 1 == paths.length) {
              var deValue = CryptoJS.AES.decrypt(tvalue, encryKey).toString(CryptoJS.enc.Utf8);
              tObj[path] = deValue.indexOf('{') > -1 ? JSON.parse(deValue): deValue;
            } else {
              tObj = tvalue;
            }
          } else {
            break;
          }
        } catch(e) {
          logger.error('decryption error: target obj:', path);
        }
      }
    }
  }
  return decryptObj;
}

//====================== RSA (Start) ======================
module.exports.SecureRandom = function(size) {
  let uuidv4 = require('uuid/v4');
  let min = 1;
  let max = 33;
  let random = '';
  let output = '';

  while (output.length < size){
    random = uuidv4();
    for (var i=0; i<5; i++) {
        var pos = Math.random() * (max - min) + min;
        var tmpChar = random.substr(pos, 1);
        tmpChar = tmpChar.toUpperCase();
        random = random.substr(0, pos-1) + tmpChar + random.substr(pos+1);
    }
    output += random.replace(/[^a-zA-Z0-9]+/g, '');
  }
  return output.substr(0, size);
};

module.exports.aesGen256Key = function() {
  return this.SecureRandom(32);     // 1 char = 8 bits
}

module.exports.aesEncrypt = function(data, key) {
  let ciphertext = null;

  try {
    if (data != null && key != null) {
      ciphertext = CryptoJS.AES.encrypt(data, key);
    } else {
      throw Error('empty data or key');
    }
  } catch (ex) {
    throw ex;
  }
  return ciphertext.toString();
}

module.exports.aesDecrypt = function(ciphertext, key) {
  let decrypted = null;

  try {
    decrypted = CryptoJS.AES.decrypt(ciphertext, key);
  } catch (ex) {
    throw ex;
  }
  return decrypted.toString(CryptoJS.enc.Utf8);
}
//====================== RSA (End) ======================
