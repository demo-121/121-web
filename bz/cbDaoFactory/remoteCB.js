// var cb = require('couchbase');
var http = require('http');
// var dbConfig = require('../dbconfig');
var fs = require('fs');
const nPath = require('path');
var request = require('request');
var fileHandler = require('../FileHandler');
var _ = require('lodash');
const commonDao = require('../cbDao/common');

const fnaPdfName = "fnaReport";
const proposalPdfName = "proposal";
const appPdfName = "appPdf";
var logger = global.logger || console;
var RemoteUtils = require('../utils/RemoteUtils');

// var ViewQuery = cb.ViewQuery;
// var cluster = null;
// var bucket = null;
//testing tag
module.exports.init = function(callback) {
  // if (!bucket) {
  //   cluster = new cb.Cluster('couchbase://'+dbConfig.cbUrl);
  //   bucket = cluster.openBucket(dbConfig.cbName, dbConfig.cbPw);
  // }
  getDoc('sysParameter', function(data) {
    if (data && !data.error) {
      let rev = data._rev;
      data._id = "";
      data._rev = "";
      global.config = _.merge(global.config, data, {fileKey: rev});
      callback({success: true});
    } else {
      callback({success: false, error: data.error});
    }
  })
}

module.exports.createView = function(ddname, view, callback) {
  // logger.log('create view:');
  updToSG('PUT', '_design/'+encodeURIComponent(ddname), 'application/json', view, function(res) {
    // logger.log('create view result:', res);
    callback(res);
  })
}

const getViewRangeNHAF = function(ddname, vname, start, end, params, callback) {
  // var query = ViewQuery.range(start, end, true);
  // bucket.query(query, function(err, result) {
  //   if (err) {
  //     callback(false)
  //   } else {
  //     callback(result);
  //   }
  // });
  var query = {
    startkey: start?start:null,
    endkey: end?end:null
  }

  if (params) {
    for(var p in params) {
      query[p] = params[p];
    }
  }

  // get session
  try {
    var updateIndex = true  //ddname == 'quotation' || ddname == 'application';
    queryFromSG('GET', '_design/'+ddname+'/_view/'+vname, query, updateIndex, function(res) {
      if (typeof callback == 'function') {
        callback(res);
      }
    })
  } catch (ex) {
    callback(false);
  }
};
module.exports.getViewRangeNHAF = getViewRangeNHAF;

const getViewRange = function(ddname, vname, start, end, params, callback) {
  // var query = ViewQuery.range(start, end, true);
  // bucket.query(query, function(err, result) {
  //   if (err) {
  //     callback(false)
  //   } else {
  //     callback(result);
  //   }
  // });
  var query = {
    startkey: start?start:null,
    endkey: end?end:null
  }

  if (params) {
    for(var p in params) {
      query[p] = params[p];
    }
  }

  // get session
  try {
    var updateIndex = false  //ddname == 'quotation' || ddname == 'application';
    queryFromSG('GET', '_design/'+ddname+'/_view/'+vname, query, updateIndex, function(res) {
      if (typeof callback == 'function') {
        callback(res);
      }
    })
  } catch (ex) {
    callback(false);
  }
};
module.exports.getViewRange = getViewRange;

module.exports.getViewByKeys = (ddname, vname, keys, params, batchSize = 40) => {
  let rows = [];
  let totalCnt = 0;
  let promise = Promise.resolve();
  for (let i = 0; i < keys.length; i += batchSize) {
    let batchKeys = _.slice(keys, i, i + batchSize);
    let batchParams = Object.assign({}, params, {
      keys: '[' + _.join(batchKeys, ',') + ']'
    });
    promise = promise.then(() => {
      return new Promise((resolve) => {
        getViewRange(ddname, vname, null, null, batchParams, (result) => {
          if (result && !result.error) {
            totalCnt += result.total_rows;
            rows = rows.concat(result.rows);
          }
          resolve();
        });
      });
    });
  }
  return promise.then(() => {
    return {
      total_rows: totalCnt,
      rows: rows
    };
  });
};

module.exports.updateViewIndex = function(ddname, vname, callback) {  // get session
  try {
    var updateIndex = true
    // change to be background process
    var backUpdateView = function() {
      var startTime = new Date();
      queryFromSG('GET', '_design/'+ddname+'/_view/'+vname, {key: '[null]'}, updateIndex, function(res) {
        var elapsedTimeMs = new Date() - startTime;
        logger.log("View update completed: " + vname + ", Elapsed " + elapsedTimeMs + "ms");
        if (typeof callback == 'function') {
          callback(res);
        }
      });
    };
    setTimeout(backUpdateView, 5);
  } catch (ex) {
    if (typeof callback == 'function') {
      callback(false);
    }
    logger.error("ERROR:::::: updateViewIndex::::::::::", ex);
  }
}

var getDoc = function(docId, callback) {
  // bucket.get(docId, function(err, result) {
  //   if (err) {
  //     callback(false)
  //   } else {
  //     callback(result);
  //   }
  // });
  // for dev only
  if (global.NODE_ENV == 'development') {
    let path;
    try {
      if (global && global.config && global.config.localDevelopment_FileMapping && global.config.localDevelopment_FileMapping[docId]) {
        docId = global.config.localDevelopment_FileMapping[docId];
        logger.log('INFO Local Development: get file from local path mapping:', docId);
      }
      path = global.rootPath + '/CB_resources/files/' + docId + '.json';
      var doc = JSON.parse(fs.readFileSync(path, 'utf8'));
      logger.log('INFO: get file from local path:', path);
      callback(doc);
      return;
    } catch (e) {
      // logger.log('EXCEPTION: fail to get file from local path:', path, e.stack || e);
      logger.error('load doc e:', __dirname, path);
    }
  }
  // --- end for dev only
  getFromSG('GET', docId, callback);
}
module.exports.getDoc = getDoc;

module.exports.getDocFromCacheFirst = (id, callback)=>{
  if (!global.config.useDocCache || !global.documentCache[id] || (global.documentCache[id].ts + (global.config.docCacheTimeout||60000)) < (new Date()).getTime()) {
    getDoc(id, function(result) {
      if (result && (result._id || !result.error)) {
        global.documentCache[id] = {
          doc: result,
          ts: (new Date()).getTime()
        }
        callback(result);
      } else {
        callback(false);
      }
    })
  } else {
    callback(global.documentCache[id].doc)
  }
}

module.exports.updDoc = function(docId, data, callback) {
  updToSG('PUT', encodeURIComponent(docId), 'application/json', data, callback);
  // bucket.replace(docId, data, callback);
}

module.exports.delDoc = function(docId, callback) {
  getFromSG('DELETE', docId, callback);
  // bucket.remove(docId, callback);
}

module.exports.delDocWithRev = function(docId, rev, session, callback) {
  commonDao.addIdsInAuditLog(docId, session.agent, function(updateAuditResp) {
    deleteFromSG('DELETE', docId, rev, true, callback);
  });
};

module.exports.getAttachment = function(docId, attName, callback) {
  getAttachmentByBase64(docId + "/" + attName, callback);
};

module.exports.getBinaryAttachment = function(docId, attName, callback) {
  getAttachmentByBinary(docId + "/" + attName, callback);
}

module.exports.getBinaryDocument = function(name, callback) {
  getAttachmentByBinary(name, callback);
}

module.exports.setAttachment = function(docId, attName, mime, data, callback) {
  updToSG('PUT', encodeURIComponent(docId) + "/" + encodeURIComponent(attName), mime, data, callback);
}

module.exports.delAttachment = function(docId, attName, callback) {
  getFromSG('DELETE', docId + "/" + attName, callback);
}

// by base64
const getAttachmentByBase64 = function(name, cb) {
  var options = getOptions('GET', name);
  var uid = RemoteUtils.SecureRandom(8);
  logger.log('INFO: getAttachmentByBase64: ' + uid + ': Options: ',options.path);

  var startTime = new Date();
  var req = http.request(options, (res) => {
    var data = '';
    res.setEncoding('binary');
    res.on('data', (chunk) => {
      data += chunk;
    });
    res.on('end', () => {
      var elapsedTimeMs = new Date() - startTime;
      var dataLength = 0;
      try {
        dataLength = Math.round((data.length / 1048576) * Math.pow(10, 4)) / Math.pow(10, 4);
      } catch (ex) {
        dataLength = 'error';
      }
      logger.log('INFO: getAttachmentByBase64 ' + uid + ': End: ' + dataLength + 'MB: Elapsed ' + elapsedTimeMs + 'ms');
      if (typeof cb == 'function') {
        if (data) {
          try {
            // error
            data = JSON.parse(data)
            cb(data);
          } catch (e) {
            data = new Buffer(data, 'binary').toString('base64');
            cb({
              success: true,
              data: data
            });
          }
        } else {
          cb(false);
        }
      }
    })
  }).on('error', (e)=> {
    logger.error('SG Error: ', e);
    cb({error: e})
  })
  req.end();
}

// by binary
const getAttachmentByBinary = function(name, cb) {
  var options = getOptions('GET', name);
  var uid = RemoteUtils.SecureRandom(8);
  logger.log('INFO: getAttachmentByBinary: ' + uid + ': Options: ',options.path);
  var startTime = new Date();
  var req = http.request(options, (res) => {
    var data = '';
    res.on('data', (chunk) => {
      cb(chunk);
    });
    res.on('end', () => {
      var elapsedTimeMs = new Date() - startTime;
      logger.log('INFO: getAttachmentByBinary: ' + uid + ': End: Elapsed ' + elapsedTimeMs + 'ms');
      cb(false);
    })
  }).on('error', (e)=> {
    logger.error('SG Error: ', e);
    cb({error: e})
  })
  req.end();
}

const getFromSG = function(method, name, cb) {
  if (name && typeof name != 'string') {
    logger.log("ERROR: invalid doc name:", name);
    cb(false);
    return;
  }
  var options = getOptions(method, name);
  var uid = RemoteUtils.SecureRandom(8);
  logger.log('INFO: getFromSG ' + uid + ': Options: ' + options.path);
  var startTime = new Date();
  var req = http.request(options, (res) => {
    var resp = '';
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      resp += chunk;
    });
    res.on('end', () => {
      var elapsedTimeMs = new Date() - startTime;
      var dataLength = 0;
      try {
        dataLength = Math.round((resp.length / 1048576) * Math.pow(10, 4)) / Math.pow(10, 4);
      } catch (ex) {
        dataLength = 'error';
      }
      logger.log('INFO: getFromSG End: ' + uid + ': ' + dataLength + 'MB: Elapsed ' + elapsedTimeMs + 'ms');
      try {
        if (typeof cb == 'function') {
          if (resp) {
            resp = JSON.parse(resp)
            cb(resp);
          } else {
            // logger.log('Error: getFromSG non-string resp?', resp);
            cb(false);
          }
        }
      } catch(e) {
        logger.error("get doc failure:", resp, options, e)
        cb(false);
      }
    })
  }).on('error', (e)=> {
    logger.log('SG Error: ', e);
    cb(false);
  })
  req.end();
}

const deleteFromSG = function(method, name, rev, updateIndex, cb) {
  if (name && typeof name != 'string') {
    logger.log("ERROR: invalid doc name:", name);
    cb(false);
    return;
  }
  var options = getOptions(method, encodeURIComponent(name) +'?rev='+rev +'&stale='+(updateIndex?'false':'ok'));
  var uid = RemoteUtils.SecureRandom(8);
  logger.log('INFO: deleteFromSG ' + uid + ': Options: ',options.path);
  var startTime = new Date();
  var req = http.request(options, (res) => {
    var resp = '';
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      resp += chunk;
    });
    res.on('end', () => {
      var elapsedTimeMs = new Date() - startTime;
      var dataLength = 0;
      try {
        dataLength = Math.round((resp.length / 1048576) * Math.pow(10, 4)) / Math.pow(10, 4);
      } catch (ex) {
        dataLength = 'error';
      }
      logger.log('INFO: deleteFromSG ' + uid + ': End: , Elapsed ' + elapsedTimeMs + 'ms: ' + dataLength + 'MB');
      if (typeof cb == 'function') {
        if (resp) {
          resp = JSON.parse(resp)
          cb(resp);
        } else {
          // logger.log('Error: deleteFromSG non-string resp?', resp);
          cb(false);
        }
      }
    })
  }).on('error', (e)=> {
    logger.log('SG Error: ', e);
  })
  req.end();
}

module.exports.updFileToSG = function(docId, attchId, rev, filename, mime, cb) {
  var options = getOptions("PUT", encodeURIComponent(docId) + '/' +encodeURIComponent(attchId) + "?rev=" + rev, {
    'Content-Type': mime,
    "Content-Transfer-Encoding": 'binary',
  });
  var uid = RemoteUtils.SecureRandom(8);
  logger.log('INFO: updFileToSG: ' + uid + ': Options: ',options.path);

  var root = global.root || __dirname;
  var filePath = nPath.join(global.rootPath, global.config.tempFolder, filename)
  try {
    fileHandler.decryptFile(filePath, function(){
      var startTime = new Date();
      var req = http.request(options, (res) => {
      var resp = '';
      res.on('data', (chunk) => {
        resp += chunk;
      });
      res.on('end', () => {
        var elapsedTimeMs = new Date() - startTime;
        logger.log('INFO: updFileToSG ' + uid + ': End: Elapsed ' + elapsedTimeMs + 'ms');
        if (typeof cb == 'function') {
          if (resp) {
            resp = JSON.parse(resp)
            fileHandler.removeFiles(filePath);
            cb(resp);
          } else {
            logger.log('ERROR: updFileToSG:', resp);
            cb(false);
          }
        }
      })
      }).on('error', (e)=> {
        logger.log('SG Error: ', e);
        fileHandler.removeOriginalFile(filePath);
      })

      fs.createReadStream(filePath).pipe(req);
    })
  } catch (e) {
    logger.log('ERROR: decrypt exception:', e, filePath);
  }
}

module.exports.uploadAttachmentByBase64 = function(docId, attchId, rev, data, mime, cb) {
  var options = getOptions("PUT", encodeURIComponent(docId) + '/' +encodeURIComponent(attchId) + "?rev=" + rev, {
    'Content-Type': mime,
    "Content-Transfer-Encoding": 'binary',
  });
  logger.log('INFO: uploadAttachmentByBase64: Options: ',options.path);
  global.uploadBase64(options, data, cb);
}

const updToSG = function(method, name, mime, data, cb) {

  var options = getOptions(method, name, {'Content-type': mime});
  logger.log('INFO: updToSG : Options: ' + options.path);
  var req = http.request(options, (res) => {
    var resp = '';
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      resp += chunk;
    });
    res.on('end', () => {
      if (cb && typeof cb == 'function') {
        if (resp) {
          try {
            resp = JSON.parse(resp)
            // logger.log('updata to SG result:', resp);
            if (resp.ok && !resp.error) {
              cb(resp);
            } else {
              logger.error('update CB error:', resp.error, options);
              cb(false);
            }
          } catch (e) {
            // calling error
            logger.error('update CB error:', e, resp, options);
            cb(false);
          }
        } else {
          cb(false);
        }
      }
    })
  }).on('error', (e)=> {
    logger.log('ERROR:: SG Error: ', e);
  })
  if (data) {
    if (typeof data == 'object') {
      data.lstChgDate = (new Date()).getTime();
      data = JSON.stringify(data);
    }
    req.write(data);
  }
  req.end();
}

const queryFromSG = function(method, name, params, updateIndex, cb) {
  var paramStr = ''
  if (params) {
    for(var p in params) {
      if (params[p]) {
        paramStr += (paramStr?"&":"?") + p +'='+ params[p]
      }
    }
  }
  //set stale to false, The index is updated before the query is executed
  paramStr += (paramStr?"&":"?") + (updateIndex?"stale=false":"stale=ok")

  var options = getOptions(method, name + (paramStr?paramStr:''), {'Content-type': 'application/json'});
  var uid = RemoteUtils.SecureRandom(8);
  logger.log('INFO: queryFromSG: ' + uid + ': Options: ',options.path);

  var startTime = new Date();
  var req = http.request(options, (res) => {
    var resp = '';
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      resp += chunk;
    });
    res.on('end', () => {
      var elapsedTimeMs = new Date() - startTime;
      var dataLength = 0;
      try {
        dataLength = Math.round((resp.length / 1048576) * Math.pow(10, 4)) / Math.pow(10, 4);
      } catch (ex) {
        dataLength = 'error';
      }
      logger.log('INFO: queryFromSG End: ' + uid + ': ' + dataLength + 'MB: Elapsed ' + elapsedTimeMs + 'ms');
      if (cb && typeof cb == 'function') {
        if (resp) {
          try {
            resp = JSON.parse(resp)
          } catch(ex) {
            logger.log("ERROR: queryFromSG", ex);
            resp = false;
          }
          cb(resp);
        } else {
          logger.log('ERROR: queryFromSG:', resp);
          cb(false);
        }
      }
    })
  }).on('error', (e)=> {
    logger.log('SG Error: ', e);
  })
  req.end();
}

var getOptions = function(method, path, headers) {
  var options = {
    hostname: global.config.sg.path,
    path: '/' + global.config.sg.name + '/'+ path,
    method: method
  };
  if (global.config.sg.port) {
    options.port = global.config.sg.port
  }
  if (global.config.sg.user && global.config.sg.pw) {
    options.auth =  global.config.sg.user+":"+global.config.sg.pw
  }
  if (headers) {
    options.headers = headers
  }
  return options;
}
