var aDao          = require("./cbDao/agent.js");
var companyDao    = require('./cbDao/company');
var constantDao   = require('./cbDao/cbConstant');
var clientHandler = require('./ClientHandler.js');
var needHandler   = require('./NeedsHandler.js');
var approvalHandler = require('./ApprovalHandler');
var approvalStatusModel = require('./model/approvalStatus');
var audHdr        = require("./AuditHandler");
var logger        = global.logger || console;
var uuid          = require('uuid');

var baseDao       = require('./cbDaoFactory').create();
const dao = require('./cbDaoFactory').create();
var {
  setToRedis,
  getFromRedis
} = require('./utils/CommonUtils');
const _ = require('lodash');

module.exports.initial = function(data, session, callback) {
  var localize = require("./localize");
  var languages = localize.init(data);
  session.skey = data.skey;
  session.clientRSA = data.yekasrb; //clientRSA; // rsa public key of client

  callback({
    success: true,
    langMap: languages.langMap,
    langs: languages.langs
  });
};


module.exports.refresh = function(data, session, callback) {
  callback({success: true, date: new Date()});
};

// var encryptPassword = function(email, pass, callback){
//   //use hmac-sha3 encryption
//   encryptionHandler.sha3Encryption(email, pass, function(encryptedPass){
//     callback(encryptedPass)
//   })
// }

// var createClient = function(data, session, callback) {
//   var clientId = "C-" + (new Buffer(data.email).toString('base64'));

//   cDao.getClientById(clientId, function(clientData){
//     if(clientData.error){
//       if(clientData.error == 'not_found'){
//         session.loginTime = new Date().getTime();
//         var password = data.password;
//         encryptPassword(data.email, password, function(encryptedPassword){
//           data.password = encryptedPassword;
//           var now = new Date();
//           var client = {
//             clientId: clientId,
//             type: "client",
//             email: data.email,
//             password: data.password,
//             compCode: "01",
//             agentCode: "SYS",
//             createDate: now.toString(),
//             lastUpDate: now.toString(),
//             profileId: "CP-" + (new Date()).getTime()
//           }

//           cDao.updateClientById(clientId, client, function(result) {
//             if(result.id){

//               client.clientRev = result.rev;
//               client.profileId = client.profileId || client.profile;

//               delete client.password;

//               //clientHandler.saveClientToSession(session, client);

//               delete client.clientId;
//               delete client.profileId;

//               var res = {
//                 success: true,
//                 loginSuccess: 'Y',
//                 client:client
//               };
//               getOptionsMap(res, function(thisRes){
//                 callback(thisRes);
//               })
//             } else {
//               //error
//               callback({success: false, errorMsg: 'signup.fail'});
//             }
//           })
//         })
//       }else{
//         callback({success: false, errorMsg: 'signup.fail'});
//       }
//     }else{
//       callback({success: false, errorMsg: 'signup.exist'});
//     }
//   })
// }

// module.exports.createClient = createClient;


module.exports.changeLang = function(data, session, callback) {
  var localize = require("./localize");
  var initData = localize.init(data);
  var resp = {
    success: true,
    langMap: initData.langMap,
  };
  callback(resp);
};


module.exports.extendSession = function(data, session, callback){
  callback({success: true});
};


let allowToApproveCase = (agentProfile) =>{
  const userRoletoApprove = ['MM1', 'MM2', 'MA'];
  const faRoleToApprove = ['DS', 'DM', 'MM1'];

  if (agentProfile.channel.type === 'FA' && agentProfile.rawData &&
    (faRoleToApprove.indexOf(agentProfile.rawData.faAdvisorRole) > -1 ||
    faRoleToApprove.indexOf(agentProfile.rawData.userRole)  > -1)){
    return true;
  } else if (userRoletoApprove.indexOf(agentProfile.role) > -1) {
    return true;
  } else {
    return false;
  }
};


module.exports.rejectTerm = function(data, session, callback) {
  if (session.agent) {
    let agentId = session.agent.profileId;
    aDao.getAgentById(agentId, function (agent) {
      if (agent && !agent.error) {
        delete agent.acceptDate;
        agent.rejectDate = new Date().toISOString();
        delete agent.acceptTermVersion;
        aDao.updateAgentProfile(agent._id, agent, function() {
          callback({
            success: true
          });
        });
      } else {
        callback({success: false});
      }
    });
  } else {
    callback({
      success: false
    });
  }
};


module.exports.acceptTerm = function(data, session, callback) {
  if (session.agent) {
    let agentId = session.agent.profileId;
    aDao.getAgentById(agentId, function (agent) {
      if (agent && !agent.error) {
        companyDao.getTNCTemplate((tncTemp)=>{
          agent.acceptDate = new Date().toISOString();
          delete agent.rejectDate;
          agent.acceptTermVersion = tncTemp.version;
          aDao.updateAgentProfile(agent._id, agent, function() {
            prepareLanding(data, session, session.agent, callback);
          });
        });
      } else {
        callback({success: false});
      }
    });
  } else {
    callback({success: false});
  }
};

var clearRedisLoginToken = function(session, data){
  var agentId = '';
  var loginToken = '';
  let sameMachineToken = data.easeTimeInitialized;
  if (session.agent && session.agent.rawData && session.agent.rawData.userId) {
    agentId = session.agent.rawData.userId;
  } else if (session.samlUser) {
    agentId = session.samlUser.id;
  }
  if (session.loginToken){
    loginToken = session.loginToken;
  }
  if (agentId && loginToken){
    getFromRedis('userPool', (data)=> {
      if (data && data[agentId]){
        _.remove(data[agentId], function (item) {
          return item.token === loginToken || item.exceedMaxConcurrentLogin || item.easeTimeInitialized === sameMachineToken;
        });
        if (data[agentId].length === 0) {
          delete data[agentId];
        }
        setToRedis('userPool', data);
      }
    });
  }
};

var isFaFirm = function(agent,channel){
  if (channel && channel.type && agent && agent.rawData && agent.rawData.userRole){
    return channel.type  === 'FA' && agent.rawData.userRole === 'MM1';
  }
  return false;
};

module.exports.login = function(data, session, callback) {

  var userName = "", password = "", agentId = "";
  if (data.userName) {
    userName = data.userName;
    password = data.password;
    agentId = userName;
  } else if (session.samlUser) {
    agentId = session.samlUser.id;
    userName = session.samlUser.name;
  } else {
    callback({success: false, errorMsg: "error.invalidID"});
    return;
  }

  session.loginTime = new Date().getTime();
  session.lang = 'en';

  //query from cb
  aDao.getLoginAgentById(agentId, function (agent) {
    // Remove SamlUser ID after login 
    session.samlUser = undefined;
    // if (session && session.passport && session.passport.user) {
    //   session.passport.user = undefined;
    // }

    if (!agent || agent.error) {
      if (!agent) {
        callback({success: false, errorMsg: "error.unknown"});
      } if (agent.error == 'not_found') {
        callback({success: false, errorMsg: "error.invalidID"});
      } else {
        callback({success: false, errorMsg: "error.unknown"});
      }
    } else {
      session.loginToken = uuid();
      // reset user pool for new logintoken
      dao.getDoc('channels', function(channels){
        var channel = channels.channels[agent.channel];
        getFromRedis("userPool", (redisData)=> {
          if (!redisData) {
            redisData = {};
          }
          if (!redisData[agentId]){
            redisData[agentId] = [];
          }
          let numOfConcurrentLogin = 0;
          _.each(redisData[agentId], function (item) {
            if (!item.exceedMaxConcurrentLogin) {
              numOfConcurrentLogin += 1;
            }
          });
          let allowLogin = true;
          if (isFaFirm(agent,channel) && numOfConcurrentLogin >= global.config.maxConcurrentUserFaFirm && !allowLogin) {
            return callback({
              success: false,
              errorMsg: 'error.exceedMaxConcurrentUserFaFirm'
            });
          } else {

            let exceedMaxConcurrentLogin = false;
            let agentSession = {
              token: session.loginToken,
              time: new Date().getTime(),
              easeTimeInitialized: data.easeTimeInitialized
            };
            if (isFaFirm(agent,channel) && numOfConcurrentLogin >= global.config.maxConcurrentUserFaFirm) {
              exceedMaxConcurrentLogin = true;
              agentSession.exceedMaxConcurrentLogin = true;
              redisData[agentId].push(agentSession);
            } else if (isFaFirm(agent,channel)) {
              redisData[agentId].push(agentSession);
            } else if (!isFaFirm(agent,channel)) {
              redisData[agentId] = [agentSession];
            }

            session.exceedMaxConcurrentLogin = exceedMaxConcurrentLogin;

            // clean the pool
            let validTime = new Date().getTime() - global.config.defaultRedisValidTime;
            for (var id in redisData) {
              _.remove(redisData[id], function (item) {
                let bExceedMaxLogin = item.exceedMaxConcurrentLogin && (agentSession.token !== item.token);
                return item.time < validTime || bExceedMaxLogin;
              });
              if (redisData[id].length === 0) {
                delete redisData[id];
              }
            }

            if (agent.status == null || !!agent.status) {
              agent.compCode = '01';
              agent.profileId = agent.profileId || agent.profile;
              session.agent = agent;
              session.agentCode = agent.agentCode;

              companyDao.getTNCTemplate((tncTemp)=>{

                if (tncTemp && !tncTemp.error) {
                  // check if need accept term
                  let needAcceptTerm = !agent.acceptDate ||
                                      agent.acceptTermVersion != tncTemp.version;

                  if (!needAcceptTerm && agent.acceptDate) {
                    let termAcceptDate = new Date(agent.acceptDate);
                    needAcceptTerm = session.loginTime - termAcceptDate.getTime() > 365 * 24 * 3600000;
                  }

                  if (needAcceptTerm) {
                    callback({
                      loginSuccess: 'Y',
                      showTerms: true,
                      tncContent: tncTemp.content[session.lang]
                    });
                  } else {
                    // add login audit
                    prepareLanding(data, session, agent, callback);
                  }
                } else {
                  prepareLanding(data, session, agent, callback);
                }
              });
            } else {
              callback({success:false, errorMsg: "error.not_active"});
            }
            setToRedis("userPool", redisData);
          }


        });
      });
    }

    audHdr.updateLoginAud({
      error: !agent || agent.error,
      profileId: agentId,
      compCode: agent && agent.compCode,
    }, data, session);

  });
};


var prepareLanding = function(data, session, agent, callback) {

  aDao.getChannels(function(channels){
    if (channels) {
      var channel = agent.channel;

      if (channels.channels[channel]) {
        agent.channel = channels.channels[channel];
        agent.channel.code = channel;
        agent.features = channels.features[agent.channel.type];

        agent.allowToEnterApproval = allowToApproveCase(agent);

        var response = {
          success: true,
          loginSuccess: 'Y',
          agent: agent,
          optionsMap: global.optionsMap,
          isFAFirm: approvalHandler.identifyApproveLevel(agent) === approvalStatusModel.FAADMIN
        };

        if (session.exceedMaxConcurrentLogin !== undefined || session.exceedMaxConcurrentLogin !== null) {
          response.exceedMaxConcurrentLogin = session.exceedMaxConcurrentLogin;
        }

        if (agent.allowToEnterApproval && session.goToApproval) {
          response.openApprovalPage = true;
        } else {
          response.openApprovalPage = false;
        }
        clientHandler.getProfileLayout(data, session, function(thirdRes){
          response.profileTemplate = {
            editDialog: thirdRes.template
          };
          clientHandler.getProfileDisplayLayout(data, session, function(forthRes){
            response.profileTemplate.profileDisplayLayout = forthRes.template;
            clientHandler.getTrustIndividualLayout(data, session, function(sixthRes){
              response.profileTemplate.tiTemplate = sixthRes.template;
              needHandler.getNeedForm(data, session, function(seventhRes){
                response.needTemplate = seventhRes;
                companyDao.getCompanyInfo((companyInfo) => {
                  response.companyInfo = companyInfo;
                  constantDao.getsysParameter((sysParameterConstant) => {
                    response.sysParameterConstant = sysParameterConstant;
                    callback(response);
                  });
                });
              });
            });
          });
        });
      } else {
        callback({
          success: false, errorMsg: "error.system_file_missing"
        });
      }
    } else {
      callback({success:false, errorMsg: "error.system_file_missing"});
    }
  });
};

// var updateFCount = function(client, reset){
//   //var newCnt = 0;
//   if(!isNaN(client.failCount)){
//     client.failCount = reset ? 0 : 1 + client.failCount;
//     // if(client.failCount > 5)
//     //   client.status = false;
//     cDao.updateClientById(client._id, client, function(result){
//       logger.log("INFO: updateFCount, reseted")
//     })
//   }else{
//     client.failCount = reset ? 0 : 1;
//     cDao.updateClientById(client._id, client, function(result){
//       logger.log("INFO: updateFCount, reset")
//     })
//   }
// }

module.exports.logout = function(data, session, callback) {
  audHdr.updateLogoutAud(data, session, callback);
  clearRedisLoginToken(session, data);
};

