import * as _ from 'lodash';
import ConfigConstants from '../constants/ConfigConstants.js'

import {checkIsWithinSixMonthOld} from '../../common/DateUtils';

import {identifyApproveLevel} from '../../app/actions/approval';

//once view hidden, remove all value inside it
window.resetHiddenFieldValues = function(template, values){
  if(values){
    if (!template.items) {
      if(template.hiddenField) {
        switch (template.hiddenField.type) {
          case 'notEqualto':
            if (_.indexOf(template.hiddenField.value, values[template.hiddenField.id]) ===  -1  && template.id && values[template.id])
            delete values[template.id];
            break;
          case 'skip':
            break;
        }
      }
      else if(template.id && values[template.id]) {
        delete values[template.id];
      }
    } else {
      if (template.type.toUpperCase() == 'TABLE' && template.allowEdit && template.trigger && (values[template.trigger.id] == 'N' || template.secondLevel)) {
        delete values[template.id];
      } else {
        for(let i = 0; i < template.items.length; i++){
          let item = template.items[i];
          if(item.items){
            resetHiddenFieldValues(item, values);
          } else if(item.id && item.type && item.type.toUpperCase() != 'READONLY' && !item.notHidden){
            delete values[item.id];
          }
        }
      }
    }
  }
}


window.getValueFromRef = function(id, changedValues, validRefValues, fValue){
  var value = changedValues[id];
  let thisVle = null;

  if(id.indexOf("@") !== -1){
    thisVle = validRefValues;
  }else{
    thisVle = changedValues;
  }

  let _id = id.replace("@", "");
  if(_id.indexOf("/") !== -1){
    value = getValueFmRootByRef(thisVle, _id, fValue);
  }

  return value;

}

window.setValueTrigger = function(iTemplate, changedValues, rootValues){
  if(iTemplate.valueTrigger){
    var trigger = iTemplate.valueTrigger;
    var tIds = [];
    if (trigger.id.split(',').length > 1) {
      tIds = trigger.id.split(',');
    } else {
      tIds = [trigger.id]
    }

    let checking = function(ind) {
      let tId = tIds[ind];
      let _value = changedValues[tId];
      if (_value == undefined) {
        _value = getValueFmRootByRef(rootValues, tId, null);
      }
      if(trigger.type == "setNA"){
        var tValue = trigger.value;
        if(!tValue.includes(_value)){
            iTemplate.value = "N/A";
        }
      } else if (trigger.type = "resetTargetValue") {
        // val[0] is condition value for checking the target field using val[1]
        // val[1] is reset value for target field
        var val = trigger.value.split(',');
        if (changedValues[iTemplate.id] == val[0]) {
          changedValues[tId] = val[1];
        }
      }
      else{
        iTemplate.title = _value;
      }
    }

    for (let i = 0; i < tIds.length; i++) {
      checking(i);
    }
  }
}



window.setMandatory = function(iTemplate, changedValues){
  if(iTemplate.mandatoryTrigger){
    var trigger = iTemplate.mandatoryTrigger;
    var tId = trigger.id;
    var tValue = trigger.value;
    var type = trigger.type.toUpperCase();
    var value = changedValues[tId];

    iTemplate.mandatory = false;
    if(type == "trueIfContains".toUpperCase()){
      if(tValue.includes(value)){
          iTemplate.mandatory = true;
      }
    }
    else if(type == "trueIfNotContains".toUpperCase()){
      if(!tValue.includes(value)){
          iTemplate.mandatory = true;
      }
    }else if(type == "trueIfEmpty".toUpperCase()){
      if(!value || value === ''){
        iTemplate.mandatory = true;
      }else{
        delete iTemplate.mandatory;
      }
    }else if (type == 'falseIfContains'.toUpperCase()){
      if(tValue.includes(value)){
        iTemplate.mandatory = false;
      }else {
        iTemplate.mandatory = true;
      }
    }

  }
}

let generateCountryWithCityList = function(optionsMap, option) {
  let countryWithCityList = [];
  let optionList = _.get(optionsMap, option).options;
  optionList.map((option) => {
    if (option.condition && countryWithCityList.indexOf(option.condition) < 0) {
      countryWithCityList.push(option.condition);
    }
  });
  return countryWithCityList;
}

window.setDisable = function(iTemplate, changedValues, validRefValues, optionsMap){
  let valid = false;
  if(iTemplate.disableTrigger){
    var trigger = iTemplate.disableTrigger;
    var tId = trigger.id;
    var tValue = trigger.value;
    var type = trigger.type.toUpperCase();

    if (type == "AnyFieldMatch".toUpperCase()) {
      let ids = tId.split(',');

      for (let i in ids) {
        let id = ids[i];
        let val = getValueFromRef(id, changedValues, validRefValues, iTemplate.value);
        if (val == tValue) {
          iTemplate.disabled = true;
          return;
        }
      }
      iTemplate.disabled = false;
    } else {
      var value = getValueFromRef(tId, changedValues, validRefValues, iTemplate.value);

      if(type == "trueIfEqual".toUpperCase()){
        if(tValue == value){
          // if (iTemplate.type.toUpperCase() == 'QRADIOGROUP') {
          iTemplate.disabled = true;
          // } else {
          //   iTemplate.type = "READONLY";
          //   valid = true;
          // }
        } else {
          iTemplate.disabled = false;
        }
      }
      else
      if(type == "trueIfContains".toUpperCase()){
        if (_.isString(tValue)) {
          if (tValue.toLowerCase() === 'city') {
            iTemplate.disableTrigger.value = tValue = generateCountryWithCityList(optionsMap, tValue.toLowerCase());
          }
        }

        if(tValue.indexOf(value) > -1){
          // if (iTemplate.type.toUpperCase() == 'QRADIOGROUP') {
            iTemplate.disabled = true;
          // } else {
          //   iTemplate.type = "READONLY";
          //   valid = true;
          // }
        } else {
          iTemplate.disabled = false;
        }
      }
      else
      if(type == "trueIfNotContains".toUpperCase()){
        if (_.isString(tValue)) {
          if (tValue.toLowerCase() === 'city') {
            iTemplate.disableTrigger.value = tValue = generateCountryWithCityList(optionsMap, tValue.toLowerCase());
          }
        }

        if(tValue.indexOf(value) == -1){
          // if (iTemplate.type.toUpperCase() == 'QRADIOGROUP') {
            iTemplate.disabled = true;
          // } else {
          //   iTemplate.type = "READONLY";
          //   valid = true;
          // }
        } else {
          iTemplate.disabled = false;
        }
      }
      else if(type == "trueIfNotEqual".toUpperCase()){
        if(tValue !=value){
          // if (iTemplate.type.toUpperCase() == 'QRADIOGROUP') {
            iTemplate.disabled = true;
          // } else {
          //   iTemplate.type = "READONLY";
          //   valid = true;
          // }
        } else {
          iTemplate.disabled = false;
        }
      }
    }

  }
  return valid;
}

window.setPicker = function(iTemplate, changedValues, optionsMap){
  if (iTemplate.disablePicker && iTemplate.type === 'picker') {
    let { id, value, type } = iTemplate.disablePicker;
    let cValue = changedValues[id];

    type = type.toUpperCase();

    if (_.isString(value)) {
      iTemplate.disablePicker.value = value = generateCountryWithCityList(optionsMap, value);
    }

    if (type === 'trueIfNotContains'.toUpperCase()){
      if (!value.includes(cValue) && iTemplate.options){
        iTemplate.type = 'text';
        delete iTemplate.options;
      }
    } else if (type === 'trueIfContains'.toUpperCase()){
      if (value.includes(cValue) && iTemplate.options){
        iTemplate.type = 'text';
        delete iTemplate.options;
      }
    }
  }
}

window.performValueUpdateTrigger = function (field, orgValue, values, params) {
  var value = orgValue;
  if (!field.trigger) {
    // skip check if trigger is empty
  } else
  if (field.trigger.type == 'valueOverride') {
    var cond = field.trigger.id.split('=');
    if (values[cond[0]] || values[cond[0]] === 0) {
      if (cond.length == 1) {
        cond.push(value)
      }
      if (cond.length == 2) {
        if (values[cond[0]] == cond[1]) {
          if (field.trigger.value || field.trigger.value === 0) {
            value = field.trigger.value;
          } else {
            value = values[cond[0]];
          }
        }
      }
    }
    cond = field.trigger.id.split('<');
    if (values[cond[0]] || values[cond[0]] === 0) {
      if (cond.length == 1) {
        cond.push(value)
      }
      if (cond.length == 2) {
        if (values[cond[0]] < cond[1]) {
          if (field.trigger.value || field.trigger.value === 0) {
            value = field.trigger.value;
          } else {
            value = values[cond[0]];
          }
        }
      }
    }
    cond = field.trigger.id.split('>');
    if (values[cond[0]] || values[cond[0]] === 0) {
      if (cond.length == 1) {
        cond.push(value)
      }
      if (cond.length == 2) {
        if (values[cond[0]] > cond[1]) {
          if (field.trigger.value || field.trigger.value === 0) {
            value = field.trigger.value;
          } else {
            value = values[cond[0]];
          }
        }
      }
    }
  } else
  if (field.trigger.type == 'defaultValueOverride') {
    if (!(value || value === 0)) { // only apply if value is not be set
      var cond = field.trigger.id.split('=');
      if (values[cond[0]] || values[cond[0]] === 0) {
        if (cond.length == 1) {
          cond.push(value)
        }
        if (cond.length == 2) {
          if (values[cond[0]] == cond[1]) {
            if (field.trigger.value || field.trigger.value === 0) {
              value = field.trigger.value;
            } else {
              value = values[cond[0]];
            }
          }
        }
      }
      cond = field.trigger.id.split('<');
      if (values[cond[0]] || values[cond[0]] === 0) {
        if (cond.length == 1) {
          cond.push(value)
        }
        if (cond.length == 2) {
          if (values[cond[0]] < cond[1]) {
            if (field.trigger.value || field.trigger.value === 0) {
              value = field.trigger.value;
            } else {
              value = values[cond[0]];
            }
          }
        }
      }
      cond = field.trigger.id.split('>');
      if (values[cond[0]] || values[cond[0]] === 0) {
        if (cond.length == 1) {
          cond.push(value)
        }
        if (cond.length == 2) {
          if (values[cond[0]] > cond[1]) {
            if (field.trigger.value || field.trigger.value === 0) {
              value = field.trigger.value;
            } else {
              value = values[cond[0]];
            }
          }
        }
      }
    }
  } else
  if (field.trigger.type == 'stringReplace' && _.isString(orgValue)) {
    var keys = field.trigger.id.split(',');
    var types = field.trigger.id.split(',');
    var iValues = [];
    for (let i in keys) {
      if (types[i]) {
        if (types[i] == 'date') {
          var d = new Date(values[keys[i]]);
          iValues.push(d.format(params.dateFormat?params.dateFormat:ConfigConstants.DateFormat));
        } else
        if (types[i] == 'time') {
          var d = new Date(values[keys[i]]);
          iValues.push(d.format(params.timeFormat?params.timeFormat:ConfigConstants.TimeFormat));
        } else
        if (types[i] == 'dateTime') {
          var d = new Date(values[keys[i]]);
          var df = params.dateFormat?params.dateFormat:"";
          df += params.timeFormat?(" " +params.timeFormat):"";
          iValues.push(d.format(df?df:ConfigConstants.DateTimeFormat));
        } else {
          iValues.push(values[keys[i]]);
        }
      } else {
        iValues.push(values[keys[i]]);
      }
    }

    for (let i = 1; i <= keys.length; i++) {
      value = value.replace("{"+i+"}", iValues[i-1]);
    }
  }
  return value;
}


let getValueFmRootByRef = function(values, referField, defValue) {
  if (!referField) return values;

  var ids = referField.split('/');
  var fvalue = values;
  for (var ii = 0; ii < ids.length; ii++) {

    var idd = ids[ii];
    var idds = idd.split('@');
    // get distinct value
    if (idds.length > 1) {
      var iddd = idds[0];
      if (fvalue[iddd]) {
        var distValue = []
        var tArr = fvalue[iddd];
        if (tArr instanceof Array) {
          for (var iii in tArr) {
            var iValue = tArr[iii]
            var tValue = iValue[idds[1]];
            if (tValue && tValue != "") {
              var tgts = tValue.split(',');
              for (var i in tgts) {
                if (tgts[i] && distValue.indexOf(tgts[i]) < 0) {
                  distValue.push(tgts[i]);
                }
              }
            }
          }
        }
        fvalue = distValue;
        break;
      } else {
        fvalue = [];
      }
    } else if (fvalue) {
      if (!fvalue[idd]) {
        fvalue = defValue;
        break;
      }
      fvalue = fvalue[idd];
    }
  }
  return fvalue;
}

let getTriggerValue = function (_value, values, validRefValues, lookUpId, field) {
  if(!lookUpId)
    return null;
  if(lookUpId.indexOf("@") !== -1){
    lookUpId = lookUpId.replace("@", "");
    if(validRefValues[lookUpId]){
      _value = validRefValues[lookUpId]
    }
  }
  if(!_value){
    if(lookUpId && lookUpId.indexOf("/") !== -1){
      _value = getValueFmRootByRef(values, lookUpId, field.id ? field.value || field.defaultValue : null);
      if(!_value)
      _value = getValueFmRootByRef(validRefValues, lookUpId, field.id ? field.value || field.defaultValue : null);

    }
  }

  return _value;
}

let checkTriggerCondition = function (validRefValues, field, trigger, orgValue, values, rootValues, showCondItems, context){

  let {id: lookUpId, value: lookUpValue, type, min, max, optionList} = trigger;
  var _value = values[lookUpId]

  // this type will provide a dynamic condition for mutiple components
  if (type === 'approvalRoleChecking') {
    let {store} = context;
    let {app: {agentProfile}} = store.getState();
    return lookUpValue === identifyApproveLevel(agentProfile);
  } else if(type === 'dynamicCondition') {
    // need to provide mutiple component ids and separated by comma
    // eg. 'id1,id2,id3'
    let ids = lookUpId.split(',')
    // store the corresponding values into array
    if (context && context.store){
      let {store} = context;
      let {app: {agentProfile}} = store.getState();
      if (agentProfile && agentProfile.authorised) {
        let {
          authorised
        } = agentProfile;
        let hasUT = (authorised.indexOf('M8') > -1 && authorised.indexOf('M8A') > -1 && authorised.indexOf('IFAST') > -1) ? 'Y' : 'N';
        values.hasUT = hasUT;
      }
    }

    _value = ids.map(id => getValueFromId(id, values, rootValues, validRefValues,context))
  } else if (type === 'showIfExistinArray') {
    let ids = lookUpId.split('/');
    // [0] should be array [1] is the key
    if (ids.length === 2) {
      _value = [];
      let array = _.get(values, ids[0]);
      let tempValue;
      _.each(array, obj => {
        tempValue = _.get(obj, ids[1]);
        if (tempValue) {
          _value.push(tempValue);
        }
      })
    } else {
      _value = getValueFromId(lookUpId, values, rootValues, validRefValues,context)
    }
  } else {
    _value = getValueFromId(lookUpId, values, rootValues, validRefValues,context)
  }

  if(!_value){
    _value = getTriggerValue(_value, values, validRefValues, lookUpId, field);
  }


  if (showCondItems) {
    lookUpId.split(',').map((__id)=>{
      if (!(showCondItems[__id] && showCondItems[__id] instanceof Array)){
        showCondItems[__id] = [];
      }
      showCondItems[__id].push(field.id);
    })

  }

  if (type === "showIfContains"){
    let tiggle = false;
    if (_value && _.isString(_value)){
      _value = _value.split(',');
      for (let i in _value){
        if (_.toUpper(lookUpValue) === _.toUpper(_.trim(_value[i]))){
          tiggle = true;
        }
      }
    } else if (values[lookUpValue] && _.isString(values[lookUpValue])){
      _value = values[lookUpValue].split(',');
      _value.forEach(function(element){
        if (_.toUpper(trigger.lookUpId) === _.toUpper(_.trim(element))){
          tiggle = true;
        }
      });
    }
    return tiggle;
  }
  else if(type === "showIfExist"){
    let _lookUpValue = lookUpValue.split(",");
     for (let i in _lookUpValue){
        if(_.toUpper(_lookUpValue[i]) == _.toUpper(_value)){
          return true;
        }
      }
  }
  else if (type === 'showIfExistinArray') {
    if (_value.indexOf(lookUpValue) > -1) {
      return true;
    }
  }
  else if(type === "showIfNotExist"){
    return _.toUpper(lookUpValue).indexOf(_.toUpper(_value)) < 0;
  }
  else if(type === 'dynamicCondition') {
    // this will repalce the condition with the corresponding element of array
    // eg. '#1 == false' will be replace as '_value[0] == false'
    if(_value && _value.constructor === Array){
      _value.forEach((value, index) => {
        let reg = new RegExp(`#${index +1}` + "\\b",'g');
        lookUpValue = lookUpValue.replace(reg, `_value[${index}]`);
      })
      return eval(lookUpValue);
    }
    return false;
  }
  else if (type === 'optionCondition'){
    if(trigger.showIfNoValue){
      return true;
    }
    let {options} = field;
    if(_.isString(options)){
      options = _.at(context, `optionsMap.${options}.options`)[0];
    }
    if(options && _.filter(options, opt=>opt.condition===_value).length){
      return true;
    }
    return false;
  }
  else if(type === 'showIfNotEqual') {
    return _value != undefined && (lookUpValue.toUpperCase() !== _value.toString().toUpperCase())
  }
  else if(type === 'showIfEqual'){
    return _value != undefined && lookUpValue.toUpperCase() == _value.toString().toUpperCase()
  }
  else if (type === 'showIfEqualBoolean') {
    return _value != undefined && lookUpValue === _value
  }
  else if (type === 'showIfUndefined') {
    return _value === undefined || lookUpValue === _value
  }
  else if (type === 'showIfNoFile') {
    return  _value === undefined || _value && _.isArray(_value) && _value.length === 0;
  }
  else if(type === "showIfBetween"){
    _value = parseInt(_value) || 0;
    min = parseInt(min) || 0;
    max = parseInt(max) || 0;
    return _value >= min && _value <= max
  }
  else if(type === "showIfLess"){
    if(lookUpValue == "@monthlyIncome"){
      let {profile} = validRefValues;
      lookUpValue = checkExist(profile, 'allowance')?profile.allowance: 0
    }else{
        lookUpValue = parseInt(lookUpValue) || 0;
    }
    _value = parseInt(_value) || 0;
    return _value < lookUpValue;
  }
  else if(type === "showIfMore") {
    lookUpValue = parseInt(lookUpValue) || 0;
    _value = parseInt(_value) || 0;
    return _value > lookUpValue;
  }
  else if(type === "showIfLessFloat") {
    lookUpValue = parseFloat(lookUpValue) || 0;
    _value = parseFloat(_value) || 0;
    return _value < lookUpValue;
  }
  else if(type === "showIfMoreFloat") {
    lookUpValue = parseFloat(lookUpValue) || 0;
    _value = parseFloat(_value) || 0;
    return _value > lookUpValue;
  }
  else if(type == "empty"){
    return !_value;
  }
  else if(type == "showIfSmaller"){
    if(_value<lookUpValue)
      return true;
  }
  else if(type == "showIfGreater"){
    if(_value>lookUpValue)
      return true;
  }
  else if(type == "notEmpty"){
    if(!!_value && _value != ""){
      return true;
    }else{
      return false;
    }
  }
  else if(type == "comparePremium"){
    let ccys = trigger.ccys;
    let ccy = values.ccy || (validRefValues.extra ? validRefValues.extra.ccy : null) || "SGD";
    let cpResult = false;
    for(let ii = 0; ii< ccys.length; ii++){
      if(ccy == ccys[ii].ccy){
        if(_value >= ccys[ii].amount)
          cpResult = true;
          break;
      }
    }
    return cpResult;
  }
  else if (type == "NotinOption") {
    var result;
    result = _.find(_.at(context, 'optionsMap.'+[optionList]+'.options')[0], (o) => {
      return o.value == _value;
    })
    return (_.isUndefined(result)) ? true : false;
  }
  else if (type == "inOption") {
    var result;
    result = _.find(_.at(context, 'optionsMap.'+[optionList]+'.options')[0], (o) => {
      return o.value == _value;
    })
    return (_.isUndefined(result)) ? false : true;
  } else if (type === 'showIfWithinSixMonthOld') {
    return checkIsWithinSixMonthOld(new Date(), new Date(_value));
  }
  else{
    return true;
  }
}

window.showCondition = function(validRefValues, field, orgValue, values, rootValues, showCondItems, context) {

  if (!field.trigger) {
    return true;
  } else {
    if(field.trigger instanceof Array){
      for(let i in field.trigger){
        let triggerResult = checkTriggerCondition(validRefValues, field, field.trigger[i], orgValue, values, rootValues, showCondItems, context);
        if(field.triggerType && field.triggerType == "and"){
          if(!triggerResult)
           return false;
          else if(i == field.trigger.length - 1)
            return true
        }
        else{
          if(triggerResult)
            return true;
          else{
            if(field.trigger[i].stop){
              return false;
            }
          }
        }
      }
    }else if(checkTriggerCondition(validRefValues, field, field.trigger, orgValue, values, rootValues, showCondItems, context)){
        return true;
    }
  }
  return false;
}

let checkOptionCondition = function (field, trigger, option, values, resetValueItems){
  let {type, id, value} = trigger;
  if (type === 'optionCondition') {
    if (option.condition) {
      if (resetValueItems) {
        if (resetValueItems[id] && resetValueItems[id] instanceof Array) {
        } else {
          resetValueItems[id] = [];
        }
        resetValueItems[id].push(field.id);
      }
      var conds = option.condition.split(',');
      if (conds.length == 1) {
        if (value && option.condition != value) {
          return true;
        }
        if(!values || !values[id]){ //no value to check condition
          return false;
        }
        if (id && values && option.condition != values[id]) {
          return true;
        }
      } else {
        if (value && conds.indexOf(value) >= 0) {
          return true;
        }
        if(!values || !values[id]){ //no value to check condition
          return false;
        }
        if (id && values && conds.indexOf(values[id]) >= 0) {
          return true;
        }
      }
    }
  } else if (type == 'notShowNAoption') {
    if (option.value == 'na') {
      return true;
    }
  } else if (type == 'valueOverride') { // skip the value override option for modification
    if (option.value == value) {
      return true;
    }
  }else if(type == 'excludeSG'){
    if(typeof value =="string"){
      let values = value.split(",");
      if(values.indexOf(option.value)>-1){
          return true;
      }
    }
  }

  return false;
}

window.optionSkipTrigger = function(field, option, values, resetValueItems) {
  if (!field.trigger) {
  } else {
    if(_.isPlainObject(field.trigger)){
      return checkOptionCondition(field, field.trigger, option, values, resetValueItems);
    }
    else if(_.isArray(field.trigger)){
      for(var i in field.trigger){
        if(checkOptionCondition(field, field.trigger[i], option, values, resetValueItems)){
          return true;
        }
      }
    }
  }
  return false;
}

window.disableTrigger = function(field, template, values, changedValues, rootValues, validRefValues) {
  if (!field.trigger) {
  } else
  if (field.trigger.type == 'disableIfFailValidate') {
    return isEqual(values, changedValues) || !!validate(template, changedValues, null, null,  values, rootValues, validRefValues);
  } else
  if (field.trigger.type == 'disableIfNotEqual') {
    if (field.trigger.id && field.trigger.value) {
      return changedValues[field.trigger.id] != field.trigger.value;
    }
  } else if (field.trigger.type == 'disableIfEqual') {
      if (field.trigger.id && field.trigger.value) {
        return changedValues[field.trigger.id] == field.trigger.value;
      }
  } else if(type === "disableIfExist"){
    let _lookUpValue = lookUpValue.split(",");
     for (let i in _lookUpValue){
        if(_.toUpper(_lookUpValue[i]) == _.toUpper(_value)){
          return true;
        }
      }
  }
  return false;
}
