
import {getProductId} from '../../common/ProductUtils';

module.exports.getProductThumbnailUrl = (appStore, product) => {
    return window.genFilePath(appStore, getProductId(product), 'thumbnail3');
};

