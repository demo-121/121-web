import * as _ from 'lodash';

export function getObjectiveTitle(context, id){
  let {lang, store} = context;
  let {template} = store.getState().needs;
  let objectives = _.at(template.cnaForm, 'needsLandingContent[2].template.items[0].items[0].options')[0];
  let objective = _.find(objectives, o=>o.id===id);
  return getLocalText(lang, objective && objective.title);
}

