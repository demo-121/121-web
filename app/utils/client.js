import * as _ from 'lodash';
import Constants from '../constants/SystemConstants';

export function genProfileMandatory(moduleId, hasFNA, isProposer, isSpouse){
  switch(moduleId){
    case Constants.FEATURES.FNA:
      return _genFnaMandatory(isSpouse);
    case Constants.FEATURES.FQ:
    case Constants.FEATURES.QQ:
      return _genProductMandatory(moduleId, hasFNA, isProposer, isSpouse);
    case Constants.FEATURES.EAPP:
      return _genApplicationMandatory(moduleId, hasFNA, isProposer);
  }
}

const _genFnaMandatory = function(isSpouse){
  let mandatory = ['idDocType', 'idCardNo', 'title', 'dob', 'marital', 'mobileNo', 'mobileNoSection', 'language', 'education', 'employStatus', 'allowance'];
  if(isSpouse){
    mandatory.push('relationship');
  }
  return mandatory;
}

const _genProductMandatory = function(moduleId, hasFNA, isProposer, isSpouse){
  let mandatory = ['nationality', 'prStatus','dob', 'gender', 'isSmoker', 'industry', 'occupation', 'addrCountry', 'residenceCountry'];

  // if(isProposer){
  //   if(hasFNA && moduleId === Constants.FEATURES.FQ){
  //     mandatory = _.concat(mandatory, ['dob', 'marital', 'employStatus'])
  //   }
  // }

  // if(hasFNA){
  //   _.forEach(['idCardNo', 'idDocType', 'title', 'marital', 'mobileNo', 'mobileNoSection', 'language', 'education', 'employStatus', 'allowance'], (_id)=>{
  //     mandatory.push(_id);
  //   })
  // }
  return mandatory;
}

const _genApplicationMandatory = function(moduleId, hasFNA, isProposer){
  let mandatory = [
      'nameOrder', 'dob', 'nationality', 'residenceCountry', 'idDocType', 'idCardNo',
      'marital', 'isSmoker', 'industry', 'occupation', 'addrCountry', 'allowance', 'pass'
  ];
  if(isProposer){
    mandatory = _.concat(mandatory, ['mobileNo', 'mobileNoSection', 'email']);
    //only for agent proposer
    if(hasFNA){
      mandatory = _.concat(mandatory, ['language', 'education']);
    }
  }
  if(hasFNA){
    mandatory = _.concat(mandatory, ['title']);
  }

  return mandatory;
}

//p=profile, dp=dependantProfiles
export function getClientName(p, dp, cid){
  let _profile = p.cid===cid?p: dp[cid];
  return _profile && _profile.fullName;
}

export function getSpouseId(context){
  let {store} = context;
  let {dependants} = store.getState().client.profile;
  let spouse = _.find(dependants, d=>d.relationship==='SPO');
  return spouse && spouse.cid;
}

export function getSpouseProfile(context={}, profile, dependantProfiles){
  if(!context.store){
    let spouse = _.find(_.get(profile, "dependants"), d=>d.relationship==='SPO');
    return _.get(dependantProfiles, _.get(spouse, "cid"));
  }
  else {
    let {store} = context;
    dependantProfiles = store.getState().client.dependantProfiles;
    return dependantProfiles && dependantProfiles[this.getSpouseId(context)];
  }
}

export function getPdaMember(context, owner, spouse, dependants){
  let {client, needs} = context.store.getState();

  let {profile, dependantProfiles} = client;
  let {pda} = needs;

  let result = [];
  if(owner){
    result.push({
      cid: profile.cid,
      fullName: profile.fullName,
      relationship: "OWNER"
    })
  }

  let _deps = pda.dependants && pda.dependants.split(",");

  if(pda.applicant==="joint" && spouse){
    profile.dependants.filter((dependant)=>{
      if(dependant.relationship == "SPO"){
        let _obj = _.cloneDeep(dependant);
        _obj.relationship = "SPOUSE";
        result.push(_obj);
      }
    })
  }



  if(_deps && dependants){
    profile.dependants && profile.dependants.filter((dependant)=>{
      if(_deps.indexOf(dependant.cid)>-1){
        let _obj = _.cloneDeep(dependant);
        _obj.filter = _obj.relationship;
        _obj.relationship = "DEPENDANTS";
        _obj.fullName = _.at(dependantProfiles, `${_obj.cid}.fullName`)[0];
        result.push(_obj);
      }
    })
  }
  return result;
}

export function getRelationTitle(profile, cid, optionsMap, lang){
  let {dependants: ds} = profile;
  let dependant = _.find(ds, d=>d.cid===cid);
  if(!dependant){
    return null;
  }

  let {relationship: r} = dependant;
  let option = _.find(_.at(optionsMap, 'relationship.options')[0], o=>o.value===r);
  if(!option){
    return null;
  }

  if(option.isOther){
    return dependant.relationshipOther;
  }
  else {
    return window.getLocalText(lang, option.title);
  }
}

export function getIdDocTypeTitle(template, values, lang){
    let _id='idDocType', v = values[_id];
    const recursive = function(template){
        let {options, id, items} = template;
        if(id===_id){
            let result = _.find(options, o=>o.value===v);
            if(result && result.isOther){
                return values.idDocTypeOther;
            }
            else{
                return result && window.getLocalText(lang, result.title);
            }
        }
        else if(items){
            for(let i in items){
                let result = recursive(items[i], values);
                if(result){
                    return result;
                }
            }
        }
        return null;
    }

    return recursive(template, values);
}

export function getAvatarInitial(profile){
  let {lastName, firstName} = profile;
    if (lastName && firstName) {
      return lastName.charAt(0).toUpperCase() + firstName.charAt(0).toUpperCase();
    } else if (lastName) {
      return lastName.charAt(0).toUpperCase();
    } else if (firstName) {
      return firstName.charAt(0).toUpperCase();
    }
    return '';
}

export function onOccupationChange (field, value, changedValues, rootValues, validRefValues) {
  const { age } = changedValues;
  const occupation = ['O674','O675','O1450','O1132','O1321','O1322'];
  const emplyStatus = ['hh','hw','rt','ue'];;
  const occupationOpt = _.find(_.get(validRefValues, 'optionsMap.occupation.options'), opt => opt.value === value);

  // auto update industry
  if (_.get(occupationOpt, 'condition')) {
    changedValues.industry = occupationOpt.condition;
  }

  // auto select employ status
  if (value === 'O674') {
    changedValues.employStatus = 'hh';
  } else if (value === 'O675') {
    changedValues.employStatus = 'hw';
  } else if (value === 'O1450') {
    changedValues.employStatus = 'ue';
  } else if (value === 'O1132') {
    changedValues.employStatus='rt';
  } else if (['O1321','O1322'].indexOf(value)>-1) {
    changedValues.employStatus = 'sd';
  } else if (occupation.indexOf(changedValues.occupation) > -1 && occupation.indexOf(value) <= -1) {
    // initial value when user change occupation from specfic option
    changedValues.employStatus = '';
    changedValues.organization = '';
    changedValues.organizationCountry = '';
  }

  if (['O674','O675','O1450','O1132'].indexOf(value) > -1) {
    changedValues.organization = 'N/A';
    changedValues.organizationCountry = 'na';
  }
}

export function validateOccupation (field, changedValues, rootValues, validRefValues) {
  const { age, occupation } = changedValues;

  // when age < 18, student above 18 cannot be selected
  if (occupation === 'O1321' && age && age < 18) {
    return 713;
  }
  // when age >= 18, Student (Below age 18) cannot be selected
  // when age < 6, should be select Child / Juvenile / Infant, not student (Below age 18)
  else if (occupation === 'O1322' && age && (age < 6 || age >= 18)) {
    return 713;
  }
}

export function onIndustryChange (field, value, changedValues, rootValues, validRefValues) {
  // clear occupation when industry change
  changedValues.occupation = '';
}

export function validateIndustry (field, changedValues, rootValues, validRefValues) {
  if (validRefValues.isApp) {
    field.mandatory = changedValues.employStatus === 'sd' ? false : true;
  }
}

export function onEmployStatusChange (field, value, changedValues, rootValues, validRefValues) {
  const { age } = changedValues;
  const employStatus = ['hw','hh','rt','ue'];

  // init field values when user change specific employee status
  if (employStatus.indexOf(changedValues.employStatus)>-1 && employStatus.indexOf(value) === -1 ) {
    changedValues.organization = '';
    changedValues.organizationCountry = null;
    changedValues.occupation = '';
    changedValues.industry = '';
  }

  // init occupation when user select student in employee status
  if (value === 'sd') {
    changedValues.occupation = '';
  }

  // auto fill in when user select specific value in employee status
  if (employStatus.indexOf(value) > -1) {
    changedValues.organization = 'N/A';
    changedValues.industry = 'I12';
    changedValues.organizationCountry = 'na';
  }

  // auto select occupation when user select specific value
  if (value === 'hh') {
    changedValues.occupation = 'O674';
  } else if (value === 'hw') {
    changedValues.occupation = 'O675';
  } else if (value === 'ue') {
    changedValues.occupation = 'O1450';
  } else if(value === 'rt') {
    changedValues.occupation = 'O1132';
  } else if (age && age <6 && (value === 'ot' || value === 'sd')) {
    changedValues.occupation = 'O246';
    changedValues.industry = 'I12';
    changedValues.organization = 'N/A';
    changedValues.organizationCountry = 'na';
  } else if (age && age >= 6 && age < 18 && value === 'sd') {
    changedValues.occupation = 'O1322';
    changedValues.industry = 'I12';
  } else if (age && age >=18 && value === 'sd') {
    changedValues.occupation = 'O1321';
    changedValues.industry = 'I12';
  }
}

export function validateEmployStatus (field, changedValues, rootValues, validRefValues) {
  const { employStatus, gender } = changedValues;

  // Househusband cannot when gender female
  if (employStatus === 'hh' && gender === 'F') {
    return 605;
  }
  // Housewife cannot when gender male
  else if (employStatus === 'hw' && gender === 'M') {
    return 606;
  }
}

export function onDobChange (field, value, changedValues, rootValues, validRefValues) {
  const { employStatus } = changedValues;
  const age = window.calcAge(value);
  changedValues.age = age;

  if (age <= 5 && (['ot', 'sd'].indexOf(employStatus) > -1)) {
    changedValues.occupation = 'O246';
    changedValues.industry = 'I12';
    changedValues.organization = 'N/A';
    changedValues.organizationCountry = 'na';
  }

  if (value) {
    if (age >= 18 && employStatus === 'sd') {
      changedValues.occupation = 'O1321';
      changedValues.industry = 'I12';
    } else if (age < 18) {
      changedValues.employStatus = 'sd';
      changedValues.industry = 'I12';
      if (age <= 5) {
        changedValues.occupation = 'O246';
        changedValues.organization = 'N/A';
        changedValues.organizationCountry = 'na';
      } else {
        changedValues.occupation = 'O1322';
      }
    }
  }
}

export function onNameChange (field, value = '', values, rootValues, validRefValues, params) {
  const nameOrder = (field.id === 'nameOrder' ? value : values.nameOrder) || 'F';
  let firstNameFirst = nameOrder !== 'L';
  const firstName = field.id === 'firstName' ? value : (values.firstName || '');
  const lastName = field.id === 'lastName' ? value : (values.lastName || '');
  let fullName =
    (firstNameFirst ? firstName : lastName) +
    (firstName && lastName ? ' ' : '') +
    (firstNameFirst ? lastName : firstName);

  if (fullName.length > 30) {
    fullName = fullName.substring(0, 30);
  }
  values.fullName = fullName;
}

export function validateFullName (field, values, rootValues, validRefValues) {
  if (!values.firstName) {
    return 603;
  }
}

export function validateTitle (field, values, rootValues, validRefValues) {
  const { gender = '', title } = values;
  if (
    (gender === 'M' && (['Mrs', 'Ms', 'Mdm'].indexOf(title) > -1)) ||
    (gender === 'F' && title === 'Mr')
  ) {
    return 601;
  }
}

export function validateGender (field, values, rootValues, validRefValues) {
  if (values.relationship && !values[field.id]) {
    if (['GFA','SON','FAT','BRO','GSO'].indexOf(values.relationship) > -1) {
      values[field.id] = 'M';
    } else if (['GMO','MOT','DAU','GDA','SIS'].indexOf(values.relationship) > -1) {
      values[field.id] = 'F';
    } else if (values.relationship === 'SPO' ) {
      if(validRefValues.profile.gender === 'M') {
        values[field.id] = 'F';
      } else {
        values[field.id] = 'M';
      }
    }
  }
}

export function validateNationality (field, values, rootValues, validRefValues) {
  if (!values[field.id]) {
    values.prStatus = null;
  }

  // N1 : Singaporean
  // N2 : seems removed
  if (values[field.id] === 'N1' || values[field.id] === 'N2') {
    values.prStatus = 'Y';
  }
}

export function onResidenceCountryChange (field, value = '', values, rootValues, validRefValues, params) {
  if (value || value === '') {
    values.addrCountry = value;
  }

  // R51  : China - Major City
  // R121 : Nauru
  // R148 : South George & S.Sandwich
  if (value !== 'R51' && value !== 'R121' && value !== 'R148') {
    values.addrCity = '';
    values.residenceCity = '';
  }
}

export function onResidenceCityChange (field, value = '', values, rootValues, validRefValues, params) {
  values.addrCity = value;
}

export function validationIdDocType (field, values, rootValues, validRefValues) {
  if (values.nationality === 'N1' || values.nationality === 'N2' || values.prStatus === 'Y') {
    field.disabled = true;
    values[field.id] = 'nric';
  } else {
    field.disabled = false;
  }
}

export function validateIdCardNo (field, values, rootValues, validRefValues) {
  values[field.id] = values[field.id] ? values[field.id].toUpperCase() : '';
  if (values.idDocType === 'nric' && values[field.id]) {
    var a = values[field.id];
    if (a.length !== 9) {
      return 604;
    }
    var k, icArray = [];
    for (k = 0; k < 9; k++) {
      icArray[k] = a.charAt(k);
    }
    icArray[1] = parseInt(icArray[1], 10) * 2;
    icArray[2] = parseInt(icArray[2], 10) * 7;
    icArray[3] = parseInt(icArray[3], 10) * 6;
    icArray[4] = parseInt(icArray[4], 10) * 5;
    icArray[5] = parseInt(icArray[5], 10) * 4;
    icArray[6] = parseInt(icArray[6], 10) * 3;
    icArray[7] = parseInt(icArray[7], 10) * 2;

    var weight = 0;
    for (k = 1; k < 8; k++) {
      weight += icArray[k];
    }
    var offset = (icArray[0] == 'T' || icArray[0] == 'G') ? 4 : 0;
    var temp = (offset + weight) % 11;
    var st = ['J', 'Z', 'I', 'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A'];
    var fg = ['X', 'W', 'U', 'T', 'R', 'Q', 'P', 'N', 'M', 'L', 'K'];
    var theAlpha;
    if (icArray[0] == 'S' || icArray[0] == 'T') {
      theAlpha = st[temp];
    }
    if (icArray[8] !== theAlpha) {
      return 604;
    }
  } else if (values.idDocType === 'fin' && values[field.id]) {
    var a = values[field.id];
    if (a.length != 9) {
      return 607;
    }

    var k, icArray = [];
    for (k = 0; k < 9; k++) {
      icArray[k] = a.charAt(k);
    }

    icArray[1] = parseInt(icArray[1], 10) * 2;
    icArray[2] = parseInt(icArray[2], 10) * 7;
    icArray[3] = parseInt(icArray[3], 10) * 6;
    icArray[4] = parseInt(icArray[4], 10) * 5;
    icArray[5] = parseInt(icArray[5], 10) * 4;
    icArray[6] = parseInt(icArray[6], 10) * 3;
    icArray[7] = parseInt(icArray[7], 10) * 2;

    var weight = 0;
    for (k = 1; k < 8; k++) {
      weight += icArray[k];
    }

    var offset = (icArray[0] === 'T' || icArray[0] === 'G') ? 4 : 0;
    var temp = (offset + weight) % 11;
    var st = ['J', 'Z', 'I', 'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A'];
    var fg = ['X', 'W', 'U', 'T', 'R', 'Q', 'P', 'N', 'M', 'L', 'K'];

    var theAlpha;
    if (icArray[0] === 'F' || icArray[0] === 'G') {
      theAlpha = fg[temp];
    }

    if (icArray[8] !== theAlpha) {
      return 607;
    }
  }
}

export function validateMarital (field, values, rootValues, validRefValues) {
  if (values.relationship === 'SPO') {
    values[field.id] = 'M';
    field.disabled = true;
  } else {
    field.disabled = false;
  }
}

export function validateMobileNo (field, values, rootValues, validRefValues) {
  var mobileCountryCode = values.mobileCountryCode || '';
  field.maxLength = 15 - mobileCountryCode.length;

  if (values[field.id] && values[field.id].length > field.maxLength) {
    values[field.id] = values[field.id].substring(0, field.maxLength);
  }
}

export function validateOtherNo (field, values, rootValues, validRefValues) {
  var otherMobileCountryCode = values.otherMobileCountryCode || '';
  field.maxLength = 15 - otherMobileCountryCode.length;

  if (values[field.id] && values[field.id].length > field.maxLength) {
    values[field.id] = values[field.id].substring(0, field.maxLength);
  }
}

export function validateLanguageOther (field, values, rootValues, validRefValues) {
  if (!values[field.id]) {
    return 709;
  }
}

export function validateOrganization (field, values, rootValues, validRefValues) {
  var employStatus = ['hh', 'hw', 'rt', 'ue'];
  if (employStatus.indexOf(values.employStatus) > -1) {
    field.disabled = true;
  } else {
    field.disabled = false;
  }
}

export function validateOrganizationCountry (field, values, rootValues, validRefValues) {
  var employStatus = ['hh', 'hw', 'rt', 'ue'];
  if (employStatus.indexOf(values.employStatus) > -1) {
    values.organization = 'N/A';
    field.disabled = true;
  } else {
    field.disabled = false;
  }

  if (values.dob && field.disabled) {
    var today = new Date();
    var birthDate = new Date(values.dob);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }

    if (age >= 0 && age < 6 && (values.organization == '' || values.organization == null)) {
      values.organization = 'N/A';
    }
  }
}

export function validateRelationship (field, values, rootValues, validRefValues) {
  const { relationship, gender, cid } = values;
  const { gender: pGender, dependants, marital: pMarital } = validRefValues.profile;

  if (
    (['GFA','SON','FAT','BRO','GSO'].indexOf(relationship) >- 1 && gender === 'F') ||
    (['GMO','MOT','DAU','GDA','SIS'].indexOf(relationship) >- 1 && gender === 'M') ||
    (relationship === 'SPO' && gender === pGender)
  ) {
    return 601;
  }

  if (_.find(dependants, dependant => dependant.relationship === 'SPO' && cid !== dependant.cid)) {
    return 602;
  }

  if (relationship === 'SPO' && pMarital !== 'M') {
    return 608;
  }
}
