import React from 'react';
import fileType from 'file-type';
import * as _ from 'lodash';
import * as _c from './client';

var FormulaParser = require('hot-formula-parser').Parser;
var parser = new FormulaParser();

window.MASK_FIELD_NOT = 0
window.MASK_FIELD_IF_EXIST_AND_DISABLE = 1
window.MASK_FIELD = 2

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

window.getValueFmRootByRef = function(values, referField, defValue) {
  if (!referField) return values;

  var ids = referField.split('/');
  var fvalue = values;
  for (var ii = 0; ii < ids.length; ii++) {

    var idd = ids[ii];
    var idds = idd.split('@');
    // get distinct value
    if (idds.length > 1) {
      var iddd = idds[0];
      if (fvalue[iddd]) {
        var distValue = []
        var tArr = fvalue[iddd];
        if (tArr instanceof Array) {
          for (var iii in tArr) {
            var iValue = tArr[iii]
            var tValue = iValue[idds[1]];
            if (tValue && tValue != "") {
              var tgts = tValue.split(',');
              for (var i in tgts) {
                if (tgts[i] && distValue.indexOf(tgts[i]) < 0) {
                  distValue.push(tgts[i]);
                }
              }
            }
          }
        }
        fvalue = distValue;
        break;
      } else {
        fvalue = [];
      }
    } else if (fvalue) {
      if (!fvalue[idd]) {
        fvalue = defValue;
        break;
      }
      fvalue = fvalue[idd];
    }
  }
  return fvalue;
}

window.getFieldFmTemplate = function(field, path, level, tgtType) {
  // let current = path[level];
  // let items = field.fields || field.items;
  // let item = items.filter(function(obj){return obj.id==current})
  // if(!isEmpty(item)){
  //   if(level != path.length - 1){
  //     return getFieldFmTemplate(item[0], path, level+1, tgtType);
  //   }else{
  //     return item[0][tgtType];
  //   }
  // }else{
  //   return ;
  // }
  let items = field.fields || field.items;
  if (level === 0) {
    for(let i in items) {
      let item = items[i];
      if (item.type.toLowerCase() == 'section' && item.id == path[level]) {
        return getFieldFmTemplate(item, path, 1, tgtType);
      } else if (item.type.toLowerCase() == 'sectiongroup') {
        let found = getFieldFmTemplate(item, path, 0, tgtType);
        if (found) {
          return found;
        }
      } if (item && item.items && item.items.length) {
        let found = getFieldFmTemplate(item, path, 0, tgtType);
        if (found) {
          return found;
        }
      } else {
        let current = path[level];
        let item = items.filter(function(obj){return obj.id==current})
        if(!window.isEmpty(item)){
          if(level != path.length - 1){
            return getFieldFmTemplate(item[0], path, level+1, tgtType);
          }else if (item[0] && tgtType) {
            return item[0][tgtType];
          } else if (tgtType) {
            return item[tgtType];
          } else {
            return item;
          }
        }else{
          return ;
        }
      }
    }
  } else {
    // last
    let subPaths = null;
    for (let j in items) {
      let item = items[j];
      if (item.id == path[level]) {
        if (path.length - 1 === level && (!tgtType || item[tgtType])) {
          return item;
        } else {
          return getFieldFmTemplate(item, path, level + 1, tgtType);
        }
      }
      let found = getFieldFmTemplate(item, path, level, tgtType);
      if (found) {
        return found;
      }
    }
  }
  return false;
}

window.runExcelFunc = function(...args) {
  try{
    var funcStr = '';
    _.forEach(args, (arg, index) => {
      if (index === 0) {
        funcStr = arg + '(';
      } else {
        if (!arg) {
          funcStr += (index > 1 ? ';' : '');
        } else {
          let varId = `VAR_${index}`;
          parser.setVariable(varId, arg);
          funcStr += (index > 1 ? ';' : '') + varId;
        }
      }
    });
    funcStr += ')';

    var resultObj = parser.parse(funcStr);
    return resultObj.result;
  } catch (e) {
    return null;
  }

}


// return error message if it is not valid
window.validate = function(field, values, langMap, optionsMap, orgValues, rootValues, validRefValues) {
  // check mandatory
  let {id, type, title, subType, items, min, max, interval, validation, masked, mandatory, trigger, options, defaultValue} = field;
  field.error = null;

  var value = undefined, oValue;
  if(!values){
    return null;
  }
  if (id) {
    value = values[id];
    oValue = orgValues?orgValues[id]:value;
  }
  if(defaultValue && !value){
    value = defaultValue;
  }

  type = _.toUpper(type);
  subType = _.toUpper(subType);

  //skip if trigger hide the item
  if(trigger && !showCondition(validRefValues, field, orgValues, values, rootValues)){
    return false;
  }

  //remove extra tab value
  if(type == 'TAB' && subType){
    if(validRefValues.pda && subType === 'SPOUSE'){
      if(validRefValues.pda.applicant === 'single'){
        values.spouse = null;
      }
    }
    else if(validRefValues.pda && validRefValues.pda.dependants && subType === ('DEPENDANTS')){
      let sDeps = validRefValues.pda.dependants.split(",");
      let vDeps = values.dependants || [];
      if(sDeps.length && vDeps.length){
        for(let i = vDeps.length -1; i>=0; i--){
          if(sDeps.indexOf(vDeps[i].cid)==-1){
            values.dependants.splice(i, 1);
          }
        }
      }
    }
  }

  //has validation function
  if (validation) {
    if (validation == "HKID") {
      if (!checkHKID(value)) {
        field.error = {
          code: 801,
          msg: window.getLocalizedText(langMap, "VALIDATE_MSG.INVALID_HKID", "Invalid HKID.")
        }
        return field.error;
      }
    }
    else {
      let errorMsg = "";
      if(type == 'TAB' && subType){
        let _values = values[_.toLower(subType)] || values;
          if(_values instanceof Array){
            let dependants = _.at(validRefValues,'profile.dependants')[0];
            for(var i in _values){
              if(!field.relationship || (field.relationship && _.findIndex(dependants, d=>{return d.cid === _values[i].cid && field.relationship.indexOf(d.relationship)>-1})!=-1)){
                eval(`var execFunc = ${validation}; errorMsg = execFunc(field, _values[i], rootValues, validRefValues)`);
                if (errorMsg){
                  let err = {code: 901,msg: errorMsg};
                  field.error=Object.assign({invalid:[]}, field.error, err);
                  field.error.invalid[i] = err;
                }
              }
            }
          }
          else{
            eval(`var execFunc = ${validation}; errorMsg = execFunc(field, _values, rootValues, validRefValues)`);
          }
      }else{
        eval(`var execFunc = ${validation}; errorMsg = execFunc(field, values, rootValues, validRefValues)`);
      }

      if(errorMsg && !field.error){
        field.error = {
          code: 901,
          msg: errorMsg
        }
        return field.error;
      }
    }
  }

  let numType = ['NUMBER', 'INTEGER', 'INTEGERCURRENCY', 'UNIT_NO', 'CURRENCY', 'FLOAT'];
  let relationshipType = ['OWNER', 'SPOUSE', 'DEPENDANTS'];

  if(type === 'TAB'&& subType){
    if(_.toLower(subType) === 'spouse' && (_.at(validRefValues,'pda.applicant')[0]!='joint' || _.at(validRefValues,'profile.marital')[0]!='M')){
      return false;

      let _values = values[_.toLower(subType)];
      if(_values instanceof Array){
        let dependants = _.at(validRefValues,'profile.dependants')[0];
        for(var i in _values){
          if(!field.relationship || (field.relationship && _.findIndex(dependants, d=>{return d.cid === _values[i].cid && field.relationship.indexOf(d.relationship)>-1})!=-1)){
            if(_values[i].init || _values[i].init == null){
              let err = {code: 902, msg: 'Please read once first.'};
              field.error=Object.assign({invalid:[]}, field.error, err);
              field.error.invalid[i] = err;
            }
          }
        }
      }
      else{
        if(_values && (_values.init || _values.init == null)){
          field.error = {code: 902, msg: 'Please read once first.'};
          return field.error;
        }
      }
    }
  }
  else if(type === 'ASSETS'){
    let sn = rootValues.aspects && rootValues.aspects.split(",");//selected needs
    let {cid} = values;
    //find total amount of specific asset of specific person
    //find the index first
    let {pda, profile} = validRefValues;
    if(window.isEmpty(pda)){
      field.error = {code: 701, msg: 'Please fill in personal data acknowledgement first.'};
      return field.error;
    }

    let rType;
    if(pda.applicant==='joint' && _.findIndex(profile.dependants, d=>{return d.cid===cid &&d.relationship==='SPO'})){
      rType='spouse';
    }
    else {
      rType = 'owner';
    }

    let assetValue = {'savAcc':0, 'fixDeposit':0, 'invest':0, 'cpfOa':0, 'cpfSa':0, 'srs':0};
    const addValue = function(p, key, _v){
      if(p && p.isActive){
        let _a = _.find(p.assets, as=>as.key===key);
        if(_a){
          _v[key] += Number(_a.usedAsset) || 0;
        }
      }
    }
    _.forEach(sn, n=>{
      let np = rootValues[n];
      if(!window.isEmpty(np)){
        _.forEach(assetValue, (a,v)=>{
          if(rType === 'spouse'){
            addValue(np[rType], v, assetValue);
          }
          else {
            addValue(np[rType], v, assetValue);
            _.forEach(np['dependants'], i=>{
              addValue(i, v, assetValue);
            })
          }
        })
      }

    })
    let fep = validRefValues.fe[rType==='spouse'?rType:'owner'];
    _.forEach(assetValue, (a,v)=>{
      let fepv = (fep && fep[v]) || 0;
      if(a>fepv){
        field.error={
          code: 704,
          msg: 'Amount is greater than the income of financial evaluation, please check.'
        }
        return field.error;
      }
    })

  }

  else if(type != 'HBOX'){
    if (!(value || value === 0) || value == "" || (value instanceof Array && value.length == 0)) {
      if (mandatory) {
        if (type == 'CHECKBOXGROUP') {
          field.error = {
            code: 301,
            msg: window.getLocalizedText(langMap, "VALIDATE_MSG.MANDATORY_CBGROUP", "Please check at least one box.").replace('{1}', title)
          }
          return field.error;
        }
        field.error = {
          code: 302,
          msg: window.getLocalizedText(langMap, "VALIDATE_MSG.MANDATORY", "This field is required.").replace('{1}', title)
        }
        return field.error;
      }
    }
    else if (!masked || value != oValue) {
      if(type === 'PICKER'){
        if(value && subType=='LIMIT'){
          if(_.isString(options)){
            options=optionsMap[options].options;
          }
          if(_.findIndex(options,option=>option.value==value)==-1){
            field.error = {
              code: 501,
              msg: window.getLocalizedText(langMap, "VALIDATE_MSG.NOT_MATCH", "Not Macth.")
            }
            return field.error;
          }
        }
      }
      else if (type === 'TEXT' || type==='TEXTAREA') {
        if (value && subType == 'EMAIL') {
          if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(value)) {
          } else {
            field.error = {
              code: 401,
              msg: window.getLocalizedText(langMap, "VALIDATE_MSG.INVALID_EMAIL", "Invalid email address.")
            }
            return field.error;
          }
        }
        else if (subType == 'PHONE') {
          if (/^\d+$/.test(value) && value.length<=15) {
          } else {
            field.error = {
              code: 402,
              msg: window.getLocalizedText(langMap, "VALIDATE_MSG.INVALID_TEL", "Invalid telephone number.")
            }
            return field.error;
          }
        }
        else if (value && numType.indexOf(subType)>-1) {
          if (_.isNumber(value)) {
            if (value == "." && (['CURRENCY', 'NUMBER', 'FLOAT', 'UNIT_NO'].indexOf(subType)>-1)) {
              field.error = {
                code: 403,
                msg: window.getLocalizedText(langMap, "VALIDATE_MSG.INVALID_NUMBER", "Invalid number.").replace("{1}", field.min)
              }
              return field.error;
            }
          }
          var floatValue = parseFloat(value);
          if (_.isNumber(min) && floatValue < parseFloat(min)) {
            field.error = {
              code: 404,
              msg: window.getLocalizedText(langMap, "VALIDATE_MSG.INVALID_NUMBER_MAX", "Amount should be larger than or equal to {1}.").replace("{1}", min)
            }
            return field.error;
          }
          if (_.isNumber(max) && floatValue > parseFloat(max)) {
            field.error = {
              code: 405,
              msg: window.getLocalizedText(langMap, "VALIDATE_MSG.INVALID_NUMBER_MIN", "Amount should be less than or equal to {1}.").replace("{1}", max)
            }
            return field.error;
          }
          if (_.isNumber(interval)) {
            interval = parseFloat(interval);
            if (interval > 0 && floatValue % interval) {
              field.error = {
                code: 406,
                msg: window.getLocalizedText(langMap, "VALIDATE_MSG.INVALID_INTERVAL", "Amount should be divisible with {1}.").replace("{1}", interval)
              }
              return field.error;
            }
          }
        }
        else {
          if (_.isNumber(min)) {
            if (_.isString(value)) {
                if (value.length < parseInt(min)) {
                field.error = {
                  code: 407,
                  msg: window.getLocalizedText(langMap, "VALIDATE_MSG.INVALID_LENGTH_MIN", "Length should be longer than or equal to {1}.").replace("{1}", min)
                }
                return field.error;
              }
            }
          }
          if (_.isNumber(max)) {
            if (_.isString(value)) {
              if (value.length > parseInt(max)) {
                field.error = {
                  code: 408,
                  msg: window.getLocalizedText(langMap, "VALIDATE_MSG.INVALID_LENGTH_MAX", "Length should be shorter than or equal to {1}.").replace("{1}", field.min)
                }
                return field.error;
              }
            }
          }
        }
      }
    }
    else {
    }
  }

  if (items && (!type || !(type == 'YESNO' || type == 'CHECKBOX')|| values[id] == 'Y' || values[id] === true)) {

    for(let i in items){
      let sItem = items[i];
      if(type==='12BOX' && mandatory){
        sItem = _.cloneDeep(sItem);
        sItem.mandatory = true;
      }
      if(sItem.id =="branch"){
        var ueueu = 1;
      }
      if (!sItem.condition || sItem.condition == 'existing' || sItem.condition == values[id]) {
          var error;
          if(type == 'MENUITEM'){
            var thisValue = id ? values[id] : values;
            error = validate(sItem, thisValue, langMap, optionsMap, orgValues || orgValues, rootValues, validRefValues);
            if(values[id]){
              values[id].isValid = error?false: true;
            }
          }
          else if(type === 'DUPLICATEPANEL'){
            let {id: tid} = field.trigger;
            for(let i=0; i<Number(values[tid]); i++){
              let errorObj = validate(sItem, values[id][i] ||{}, langMap, optionsMap, _.at(orgValues, `{$id}[{$i}]`)[0] || orgValues, rootValues, validRefValues)
              if(errorObj){
                field.error = errorObj;
              }
            }
          }
          else if(type == 'TABLEVIEW'){
            error = validate(sItem, values[id] || values, langMap, optionsMap, orgValues, rootValues, validRefValues);
          }
          else if(type == 'TAB'){
            if(subType){
              if(_.toLower(subType) === 'proposer' || _.toLower(subType) === 'insured'){
                let _values = (!_.isUndefined(values[_.toLower(subType)])) ? values[_.toLower(subType)] : values;
                let _orgValues = (!_.isUndefined(orgValues)) ? orgValues[_.toLower(subType)] : {};
                if(_values instanceof Object){
                  error = validate(sItem, _values, langMap, optionsMap, orgValues || {}, rootValues, validRefValues);
                  field.error = error;
                }
              }else{
                let _values = (!_.isUndefined(values[_.toLower(subType)])) ? values[_.toLower(subType)] : values;
                let _orgValues = (!_.isUndefined(orgValues)) ? orgValues[_.toLower(subType)] : {};
                let dependants = _.at(validRefValues,'profile.dependants')[0];
                if(_values instanceof Array){
                  for(var i in _values){
                    if (_.isUndefined(orgValues)) orgValues = {};
                    if(!field.relationship || (field.relationship && _.findIndex(dependants, d=>{return d.cid === _values[i].cid && field.relationship.indexOf(d.relationship)>-1})!=-1)){
                      error = validate(sItem, _values[i], langMap, optionsMap, orgValues[i] || orgValues, rootValues, validRefValues )
                      if (error){
                        field.error=Object.assign({invalid:[]}, field.error, error);
                        field.error.invalid[i] = error;
                      }
                    }

                  }
                }
                else{
                  error = validate(sItem, _values, langMap, optionsMap, orgValues || {}, rootValues, validRefValues);
                }
              }
            }else{
              error = validate(sItem, values, langMap, optionsMap, orgValues, rootValues, validRefValues);
            }
          }
          else if(type == 'HBOX'){
            let _error = validate(sItem, values[id] || values, langMap, optionsMap, orgValues, rootValues, validRefValues);
            sItem.error = _error;
          }
          else if(type == 'BLOCK'){
            let _blkValues = values[id]|| values;
            error = validate(sItem, sItem.id? _blkValues[sItem.id] : _blkValues, langMap, optionsMap, orgValues, rootValues, validRefValues)
          }
          else if(type == "MENU" || type == "STEPPER" || type == "SECTION"){
            error = validate(sItem, values, langMap, optionsMap, orgValues, rootValues, validRefValues);
          }else{
            error = validate(sItem, !!id ? values[id] : values, langMap, optionsMap, orgValues, rootValues, validRefValues);
          }
          if (error && !field.error){
            field.error=error;
          }
      }

      if (subType == 'checkbox' && sItem.input == 'checkbox' && sItem.subQtn) {
        if (value.indexOf(sItem.id) >= 0) {
          for (var j in sItem.subQtn) {
            var error = validate(sItem.subQtn[j], values, langMap, optionsMap, orgValues, rootValues, validRefValues)
            if (error) {
              field.error=error;
            }
          }
        }
      }

    }
  }

  return field.error;
};


window.getLocalizedText = function(langMap, langKey, defaultText) {
  return (langMap && langMap[langKey])?langMap[langKey]:(defaultText?defaultText:langKey)
};

window.getLocalText = function(lang, text) {
  return text?(typeof text == 'object'?text[lang]:text):"";
};

window.getLocalTextFromTextArray = function(lang, array, delimiter) {
  let output = "";
  array.forEach((text, i) => {
    output += window.getLocalText(lang, text) + (i < array.length - 1? delimiter: "");
  })
  return output;
}

window.getMsg = function (lang, langMap, key, params) {
  let msg = window.getLocalizedText(langMap, key);
  _.each(params, (param, index) => {
    msg = msg.replace('%' + (index + 1) + '$@', window.getLocalText(lang, param));
  });
  return msg;
};

window.calcAge = function(birthday) {
  let today = new Date();
  let birthDate = new Date(birthday);
  let age = today.getFullYear() - birthDate.getFullYear();
  let m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
  }
  return age;
}

window.getCurrencyByCcy = function(value, ccy, decimals) {
  var sign = '$';
  if (ccy == 'SGD') {
    sign = 'S$ ';
  } else if (ccy === 'USD') {
    sign = 'US$ ';
  } else if (ccy === 'GBP') {
    sign = '£ ';
  } else if (ccy === 'EUR') {
    sign = '€ ';
  } else if (ccy === 'AUD') {
    sign = 'A$ ';
  }
  return window.getCurrency(value, sign, decimals);
}

window.getCurrency = function(value, sign, decimals) {
    if (!_.isNumber(value)) {
        return '-';
    }
    if (!_.isNumber(decimals)) {
        decimals = 2;
    }
    var text = parseFloat(value).toFixed(decimals);
    var parts = text.split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    parts[0] = (sign && sign.trim().length > 0 ? sign : '') + parts[0];
    return parts.join(".");
};

window.getCurrencySign = function (compCode, ccy, optionsMap) {
  const currency = _.find(optionsMap.ccy.currencies, c => c.compCode === compCode);
  if (currency) {
    const option = _.find(currency.options, opt => opt.value === ccy);
    if (option) {
      return option.symbol;
    }
  }
  return 'S$';
};

window.getDate = function(dateStr) {
  var thisDate = new Date(dateStr);
  var month = thisDate.getUTCMonth() + 1;
  if(month < 10)
    month = "0" + month;
  var year = thisDate.getUTCFullYear();
  var day = thisDate.getUTCDay();
  if(day < 10)
    day = "0" + day;

  return year + "-" + month + "-" + day;
}

window.displayDateTime = function(locale, dateString){

  var currentDate = new Date(dateString);

  //get date
  var day = currentDate.getDate();
  var month = currentDate.getMonth();
  var year = currentDate.getFullYear();
      //get time
    var hours = currentDate.getHours();
    var minutes = currentDate.getMinutes();
    if (minutes < 10)
    minutes = "0" + minutes

  if(locale == 'en'){
    var suffix = "AM";
    if (hours >= 12) {
    suffix = "PM";
    hours = hours - 12;
    }
    if (hours == 0) {
    hours = 12;
    }
  var date = day + " " + window.getMonth(month) + " " + year;
  var time = hours + ":" + minutes + " " + suffix;

  return date + " " + time;

  }else{
    return "0000-00-00";
  }
}
window.getMonth = function(index){
  var month = new Array();
  month[0] = "Jan";
  month[1] = "Feb";
  month[2] = "Mar";
  month[3] = "Apr";
  month[4] = "May";
  month[5] = "Jun";
  month[6] = "Jul";
  month[7] = "Aug";
  month[8] = "Sep";
  month[9] = "Oct";
  month[10] = "Nov";
  month[11] = "Dec";
  return month[index];
}

window.multilineText = function(txt){
  var out=[];
  if(txt!=null){
    var txtArray=txt.split("\\n");
    for(var i =0; i<txtArray.length;i++){
      out.push(<div>{txtArray[i]}<br/></div>);
    }
  }else {
    out.push(<div> </div>);
  }
  return out;
};

window.checkExist = function(obj, key){
  if(window.isEmpty(key)) return false;
  let keys = key.split(".");
  if(keys.length==1){
    return (obj && obj[key]);
  }else{
    return key.split(".").every(function(x) {
        if(typeof obj != "object" || obj === null || ! x in obj)
            return false;
        obj = obj[x];
        if(!obj)
          return false;
        return true;
    });
  }
};

window.base64ToArrayBuffer = function(base64) {
  if (typeof base64 == 'string') {
    var raw = window.atob(window.trimBase64(base64));
    var len = raw.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
      bytes[i] = raw.charCodeAt(i);
    }
    return bytes;
  } else {
    return base64;
  }
}
var BASE64_MARKER = ';base64,';

window.trimBase64 = function(base64) {
  var rawStr = base64,
      base64Index = rawStr.indexOf(BASE64_MARKER) ;
  if (base64Index > 0) {
    rawStr = rawStr.substring(base64Index + BASE64_MARKER.length);
  }
  return rawStr;
}

window.addBase64Prefix = function(base64, mime) {
  if (base64) {
    if (base64.indexOf(BASE64_MARKER) < 0) {
      if (!mime) {
        var ftype = fileType(window.base64ToArrayBuffer(base64.substring(0,350)))
        mime = (ftype && ftype.mime) || 'image/png';
      }
      return "data:"+mime+";base64,"+base64
    }
  }
  return base64

}

window.isEmpty = function(value){
  //if type == date, return false
  if(_.isDate(value)){
    return false;
  }
  else if (_.isBoolean(value)) {
    return false;
  }
  //if type == number and != 0, return false
  else if(_.isNumber(value) && value !== 0){
    return false;
  }
  else {
    if(_.isPlainObject(value) || _.isArray(value)){
      let empty = true;
      _.forEach(value, v=>{
        if(!window.isEmpty(v)){
          empty = false;
        }
      })
      return empty;
    }
    else {
      return _.isEmpty(value)
    }
  }
}

window.getValueFromId = function(id, values, rootValues, validRefValues, context){
  id = _.trim(id);
  if(id.indexOf("@")===0 && id.indexOf("@spouse") === -1){
    return _.at(validRefValues, id.substring(1, id.length))[0];
  }
  else if (id.indexOf("@spouse")===0) {
    let spouseP = _c.getSpouseProfile(context, validRefValues.profile, validRefValues.dependantProfiles);
    if (spouseP) {
      return _.get(spouseP, id.substring(8, id.length))
    }else return '';
  }
  else if(id.indexOf("#")===0){
    return _.at(rootValues, id.substring(1, id.length))[0];
  }
  else if(id.indexOf("$") ===0) {
    return _.get(context, `optionsMap.${id.substring(1, id.length)}.options`)
  }
  else {
    return _.get(values, id);
  }
}

window.getOptionsTitle = function(item, value, optionsMap, lang){
  let options = null;
  if(typeof item.options === 'string'){
    options = optionsMap[item.options] && optionsMap[item.options]['options'];
  }else if(item.options instanceof Array){
    options = item.options;
  }

  if(!options || typeof options === 'string'){
    return options;
  }

  let title = value;
  if(options instanceof Array){
    for(let i = 0; i < options.length; i++){
      if(options[i].value == value){
        if(options[i].title instanceof Object){
          title = options[i].title[lang || 'en'];
        }else{
          title = options[i].title;
        }
      }
    }
  }

  return title;
}

window.getOptionTitle = function (value, optionsMap, lang) {
  let options = optionsMap.options;
  let option;
  if (options instanceof Array) {
    option = _.find(options, o => o.value === value);
  } else {
    option = _.find(options, (o, key) => key === value);
  }
  return option && window.getLocalText(lang, option.title);
};

// window.isEqualArray = function(array1, array2) {
//   if (!(array1 instanceof Array && array2 instanceof Array)) {
//     return false;
//   }

//   if (array1.length != array2.length) {
//     return false;
//   }

//   for (var i = 0; i < array1.length; i ++) {
//     if (array1[i] instanceof Array && array2[i] instanceof Array) {
//       if (!isEqualArray(array1[i], array2[i])) {
//         return false;
//       }
//     } else if (!isEqual(array1[i], array2[i])) {
//       return false;
//     }
//   }
//   return true;
// }

window.isEqual = function(item1, item2, level){
  // recursive for object and array
  if (!level) {
    level = 1;
  } else if (level > 10) {
    return  true;
  } else {
    level ++;
  }

  if((_.isPlainObject(item1) && _.isPlainObject(item2)) || (_.isArray(item1) && _.isArray(item2))){
    let itemKeys = _.keys(item1), item2Keys = _.keys(item2);
    if (itemKeys.length == item2Keys.length) {
      _.forEach(item2Keys, key=>{
        if(_.findIndex(itemKeys, value=>value===key)==-1){
          itemKeys.push(key);
        }
      })
      for(let key in itemKeys){
        if(!window.isEqual(item1[itemKeys[key]], item2[itemKeys[key]], level)){
          return false;
        }
      }
    } else {
      return false;
    }
    return true;
  }
  else {
    if(window.isEmpty(item1) && window.isEmpty(item2)){
      //0 is not equal to undefine
      if((item1 === 0 || item2 === 0) && item1 !== item2){
        return false;
      }
      return true;
    }
    else {
      if(!_.isEqual(item1, item2)){
      }
      return _.isEqual(item1, item2);
    }
  }
}

window.imageResize = function(imageUrl, MAX_WIDTH, MAX_HEIGHT,fileType, cb) {
  // MAX_WIDTH should be larger than MAX_HEIGHT

  let getRadio = function(longSide, shortSide){
    let longRatio = longSide/MAX_WIDTH;
    let shortRatio = shortSide/MAX_HEIGHT;
    return longRatio>shortRatio?longRatio:shortRatio;
  }

  try {
    var canvas = document.createElement("canvas");
    var img = new Image();
    img.onload = function(e) {
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);

      let sidesLength = {
        width: img.width,
        height: img.height
      }

      var width = img.width;
      var height = img.height;
      let radio = 1;

      if (width > height) {
        // if (width / height > 2) {
        //   cb({
        //     success: false,
        //     error: "Invalid ratio"
        //   });
        //   return;
        // }
        // if (height > MAX_HEIGHT) {
        //   width *= MAX_HEIGHT / height;
        //   height = MAX_HEIGHT;
        // }
        if (width > MAX_WIDTH || height > MAX_HEIGHT) {
          radio = getRadio(width, height);
          width /= radio;
          height /= radio;
        }
      } else {
        // if (height / width > 2) {
        //   cb({
        //     success: false,
        //     error: "Invalid ratio"
        //   });
        //   return;
        // }
        // if (width > MAX_WIDTH) {
        //   height *= MAX_WIDTH / width;
        //   width = MAX_WIDTH;
        // }
        if (height > MAX_WIDTH || width > MAX_HEIGHT) {
          radio = getRadio(height, width);
          width /= radio;
          height /= radio;
        }
      }
      canvas.width = width;
      canvas.height = height;
      ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0, width, height);

      if(!fileType){
        fileType = "image/png";
      }
      cb({
        success: true,
        imageUrl: canvas.toDataURL(fileType)
      });
    }
    img.src = imageUrl;
  } catch(e) {
    cb({
      success: false,
      error: "Invalid format"
    });
  }
}

window.longestStringinArray = function(Arr = []) {
  var longest = _.reduce(Arr, function (a, b) { return a.length > b.length ? a : b; }); longest;
  return longest || '';
}

window.isIPad = () => {
  return navigator.userAgent.indexOf('iPad') > -1;
};

window.getTabWidthByTabsTitleArray = function(titleArray) {
  let longestStrLegnth = window.longestStringinArray(titleArray);
  let calcTabsPixel = longestStrLegnth.length * 10;
  if (calcTabsPixel < 300)
    calcTabsPixel = 300;
  return calcTabsPixel;
}

window.getTabsMinHeightByWidth = function(TabsWidth, isApp) {
  let minHeight;
  if (window.matchMedia('(min-width: ' + TabsWidth + 'px)').matches) {
    minHeight = { minHeight: isApp ? '53px' : '48px' };
  } else {
    minHeight = { minHeight: '58px' };
  }
}

window.validateEmailAddress = function(emailAddr) {
  return (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]{2,4}$/.test(emailAddr));
}

window.logout = function () {
  let timeInitialized = 0;
  try {
    timeInitialized = localStorage.getItem('timeInitialized');
    localStorage.removeItem('timeInitialized');
  } catch (error) {
    // Do Nothing when using disable localStorage
  }
  var xhttp = new XMLHttpRequest();
  xhttp.open('GET', `./logout?timeInitialized=${timeInitialized}`, false);
  xhttp.send(null);
  window.loggedIn = false;
}
