let React = require('react');


import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import Dialog from 'material-ui/Dialog';
import {getIcon} from '../Icons/index';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import {formatDateForApproval} from '../../../common/DateUtils';

import {deleteComment, editComment} from '../../actions/approval';

import * as _v from '../../utils/Validation';

class CommentItem extends EABInputComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      openEditComment: false,
      commentIndex: props.commentIndex,
      content: props.values.content || '',
      textFieldContent: props.values.content || '',
      authorPid: props.authorPid,
      photoValue: '',
      initProfilePic: false,
      errorMsg: ''
    });
  }

  componentWillReceiveProps(nextProps) {
      let newState = {};
      let updateFlag = false;

      if(!window.isEqual(this.state.content, nextProps.values.content)){
          newState.content = nextProps.values.content;
          newState.textFieldContent = nextProps.values.content;
          newState.initProfilePic = false;
          updateFlag = true;
      }

      if(!window.isEqual(this.state.authorPid, nextProps.authorPid)){
          newState.authorPid = nextProps.authorPid;
          newState.initProfilePic = false;
          updateFlag = true;
      }

      if (updateFlag) {
          this.setState(newState);
      }
  }

  openDialog= () => {
    this.setState({
      openEditComment: true
    });
  }

  closeDialog= () => {
    this.setState({
      openEditComment: false,
      errorMsg: ''
    });
  }

  submit= () =>{
    let {textFieldContent, commentIndex} = this.state;
    let {approval={}} = this.context.store.getState();

    editComment(this.context, approval.approvalCase.approvalCaseId, textFieldContent, commentIndex, () => {
      this.closeDialog();
    });
  }

  deleteComment(){
    let {commentIndex} = this.state;
    let {approval={}} = this.context.store.getState();

    deleteComment(this.context, approval.approvalCase.approvalCaseId, commentIndex, () => {
      this.closeDialog();
    });
  }

  getProfilePic = (docId) =>{
    let {store} = this.context;
    let {app} = store.getState();
    let {initProfilePic} = this.state;
    if (docId) {
      let url = genFilePath(app, 'U_' + docId, 'agentProfilePic');
      let testingImg = new Image;
      testingImg.src = url;

      testingImg.onerror = () => {
        if (initProfilePic === false) {
          this.setState({
            photoValue: '',
            initProfilePic: true
          });
        }
      }

      testingImg.onload = () => {
          if (initProfilePic === false) {
            this.setState({
              photoValue: url,
              initProfilePic: true
            });
          }
      }
    } else {
      if (initProfilePic === false) {
        this.setState({photoValue: '', initProfilePic: true});
      }
    }
  }

  commentValidate = (value) =>{
    let {langMap, optionsMap, lang} = this.context;
    let {max} = this.props;
    let error = {};
    let changedValues = {}
    changedValues.commentEdit = value;
    let template = {id: 'commentEdit', type: 'textarea', max};
    _v.exec(template, optionsMap, langMap, changedValues, changedValues, changedValues, error);

    let errorMsg = _v.getErrorMsg(template, error[template.id].code, lang);

    value = value.substr(0, max);
    this.setState({
      errorMsg,
      textFieldContent: value
    });
  }

  render() {
    let {
      values = {},
      enableBtn,
      disabled
    } = this.props;

    let {commentIndex, content, textFieldContent, authorPid, photoValue, errorMsg} = this.state;
    let {langMap} = this.context;
    if (!window.isMobile.apple.phone) {
      this.getProfilePic(authorPid);
    }

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.closeDialog}
      />,
      <FlatButton
        label="OK"
        primary={true}
        onClick={this.submit}
      />,
    ];

    const padding = (window.isMobile.apple.phone) ? {padding: '12px'} : {padding: '24px'};
    const contentMobileStyle = (window.isMobile.apple.phone) ? {padding: '12px', wordBreak: 'break-all'} : {padding: '24px'};

    const createTimeDiv = <div className={styles.StandardLineHeight + ' ' + styles.CommentDateLabel}>Created: {formatDateForApproval(new Date(values.createTime))}</div>;
    const editedTimeDiv = <div className={styles.StandardLineHeight + ' ' + styles.CommentDateLabel}>Edited: {formatDateForApproval(new Date(values.editedTime))}</div>;

    return (
        <div key={'comment' + commentIndex} style={(!window.isMobile.apple.phone) ? {display: 'flex', minHeight: '0px'} : undefined}>
          <div style={{flex: '4', ...padding}}>
            {(!window.isMobile.apple.phone) ?
              <Avatar
                key={'comment' + commentIndex}
                src={photoValue}
                icon={getIcon('person', '#FFFFFF')}
              /> : undefined
            }
            <div className={styles.StandardLineHeight} style={{marginTop: '5px', fontWeight: (window.isMobile.apple.phone) ? 600 : undefined }}>{values.author}</div>
            {(!window.isMobile.apple.phone) ? createTimeDiv : undefined}
            {(!window.isMobile.apple.phone) ? editedTimeDiv : undefined}
          </div>
          {
            <div style={{flex: '9', wordWrap: 'break-word', maxWidth: '600px', ...contentMobileStyle}}>{content}</div>
          }
         { (enableBtn) ?
            <div  style={(window.isMobile.apple.phone) ? {display: 'flex', minHeight: '0px', minWidth: '100px', ...padding} : {flex: '1', minWidth: '100px', ...padding}}>
              {
                <IconButton onClick={() => {this.openDialog();}} disabled={disabled}>
                  {getIcon('edit', 'black')}
                </IconButton>
              }
              {
                <IconButton
                  onClick={() => {this.deleteComment();}}
                  disabled={disabled}
                >
                  {getIcon('delete', 'black')}
                </IconButton>
              }
              {
                (window.isMobile.apple.phone) ? 
                <div style={{paddingLeft: '24px'}}>
                  {createTimeDiv}
                  {editedTimeDiv}
                </div>
                :
                undefined
              }
            </div> :
            <div style={{flex: '1', ...padding}}>
              {(window.isMobile.apple.phone) ? createTimeDiv : undefined}
              {(window.isMobile.apple.phone) ? editedTimeDiv : undefined}
            </div>
          }
          <Dialog
            title={window.getLocalizedText(langMap, 'menu.eapproval.commentItemTitle', 'Comment Box')}
            actions={actions}
            modal={true}
            open={this.state.openEditComment}
          >
            <TextField
                id={'comment-box-edit-' + commentIndex}
                style={{width: '100%', height: '100%', marginBottom: '0px'}}
                multiLine
                rows={3}
                rowsMax={3}
                errorText={errorMsg}
                errorStyle={{lineHeight:'16px', bottom: '-25px'}}
                underlineStyle={{bottom: '-10px'}}
                value={textFieldContent}
                onChange={(e, newValue)=> {this.commentValidate(newValue);}}
              />
        </Dialog>
        </div>
    );
  }
}

export default CommentItem;
