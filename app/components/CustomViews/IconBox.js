import React from 'react';
import PropTypes from 'prop-types';
import {IconButton, Paper} from 'material-ui';

import ImageView from './ImageView';
import EABComponent from '../Component';

import styles from '../Common.scss';

class IconBox extends EABComponent {
  constructor(props) {
      super(props);
      this.state = Object.assign( {}, this.state, {
        image: props.image,
        size: props.size,
        title: props.title,
        selected: props.selected,
        onCheck: props.onCheck,
        type: props.type,
        docId: props.docId,
        txtAlign: props.txtAlign,
        disabled: props.disabled
    });
  };

  componentWillReceiveProps(nextProps){
    this.state = {
      image: nextProps.image,
      size: nextProps.size,
      title: nextProps.title,
      selected: nextProps.selected,
      onCheck: nextProps.onCheck,
      docId: nextProps.docId,
      type: nextProps.type,
      txtAlign: nextProps.txtAlign,
      disabled: nextProps.disabled
    }
  }

  getStyle=()=>{
    return {
      'buttonBgColor':'#FAFAFA',
      'buttonGreen':'#2DB154',
      'buttonRed':'#F1453D',
    };
  }

  render() {
    let {muiTheme, lang} = this.context;
    let {title, size, type, image, selected, onCheck, docId, txtAlign, disabled} = this.state;

    let descHeight, imgHeight, lineheight, boxHeight, width;

    if (size===4) {
      descHeight = 88;
      //note: lucia imgHeight = 100;
      //note: lucia width = 100;
      //note: lucia lineheight= descHeight;
      imgHeight = 120;
      width = 120;
      lineheight= descHeight *1.3;
    } else if (size === 3){
      descHeight = 88;
      imgHeight = 130;
      width = 225;
      lineheight= descHeight /2;
    } else {
      descHeight = 38;
      imgHeight = 180;
      width = 225;
      lineheight= descHeight /2;
    }


    boxHeight = imgHeight + descHeight;

    descHeight = descHeight + 'px';
    imgHeight = imgHeight + 'px';
    lineheight = lineheight + 'px';
    boxHeight = boxHeight + 'px';
    width = width + 'px';

    let imgObj = (
      <ImageView docId={docId} attId={image} styleClass={styles.PanelBackgroundColor} imageStyle={{paddingTop: (size === 4) ? '20px' : undefined}} style={{ color: 'rgba(0,0,0,0.87)', width: width, height: imgHeight, margin: '0 auto'}}/>
    );
    let styleName
    (selected? styleName = styles.IconBoxSelected: styleName =  styles.IconBoxNotSelected)

    return (
      <div style={{cursor:'pointer', flexBasis: size, textAlign: 'center', marginRight:'16px', marginBottom:'24px', verticalAlign:'top', height: boxHeight, minWidth: '231px', maxWidth: '231px', flex: '1 0 auto'}}>
        <div className={styleName + ' ' + styles.PanelBackgroundColor}>
        <Paper onClick={(disabled) ? undefined : onCheck} style={{backgroundColor: '#FAFAFA'}}>
          {imgObj}
          <div id={'Title'} style={{height: descHeight, lineHeight: lineheight, textAlign: (txtAlign) ? txtAlign : 'center'}}>{getLocalText(lang,title)}</div>
        </Paper>
        </div>
      </div>
    );
  }

}

IconBox.propTypes = Object.assign({}, IconBox.propTypes, {
  image: PropTypes.string,
  size: PropTypes.number,
  title: PropTypes.string,
  selected: PropTypes.bool,
  onCheck: PropTypes.func,
  docId: PropTypes.string,
  type: PropTypes.string
})

export default IconBox;
