import React, { Component, PropTypes} from 'react';
import mui, { CircularProgress } from 'material-ui';
const OverLay = require('material-ui/internal/Overlay').default;
const appTheme = require('../../theme/appBaseTheme.js');

import Events from 'material-ui/utils/events.js'

class Loading extends Component {
  constructor(props) {
      super(props);
      this.state = Object.assign( {}, this.state, {
        muiTheme: appTheme.getTheme(),
        open: false
      });
  };

  static childContextTypes = {
    muiTheme: PropTypes.object
  }

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme
    }
  }

  static contextTypes = {
   store: PropTypes.object
  }

  componentDidMount() {
    this._handleResize();
    this.state.mounted = true;
    Events.on(window, 'resize', this._handleResize.bind(this));
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
  }

  componentWillUnmount() {
    this.state.mounted = false;
    Events.off(window, 'resize', this._handleResize.bind(this));

    if (typeof this.unsubscribe == 'function') {
      this.unsubscribe();
    }

  }

  storeListener = () =>{
    var state = this.context.store.getState()
    if (this.state.open != state.app.loading) {
      this.setState({
        open: state.app.loading
      })
    }
  }


  _handleResize(){
    if (this.state.mounted) {
      this.setState({
        muiTheme: appTheme.getTheme(true),
      });
    }
  }


  getStyles() {
    return {
      contentStyle: {
        zIndex: 5000,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex', minHeight: '0px',
        alignItems: 'center',
        justifyContent: 'center'
      }
    }
  }

  render() {
    var {
      open
    } = this.state;

    var {
      contentStyle,
      overlayStyle
    } = this.getStyles()

    if(this.state.muiTheme.isMobile){
      contentStyle.width = "100%";
    }

    if (open) {
      return (
        <div key="LoadingBlock" id="loading" style={ Object.assign(contentStyle, {display: 'flex', minHeight: '0px'})}>
          <CircularProgress key="loader" ref="loader"/>
        </div>
      );
    } else {
      return <div key="LoadingBlock"></div>
    }
  }
}

export default Loading;
