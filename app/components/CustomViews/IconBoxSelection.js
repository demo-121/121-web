import React from 'react';
import PropTypes from 'prop-types'

import EABTextField from './TextField.js';
import IconBox from './IconBox';
import EABInputComponent from './EABInputComponent';

import HeaderTitle from './HeaderTitle';
import ErrorMessage from './ErrorMessage';

import styles from '../Common.scss';
import * as _ from 'lodash';

class IconBoxSelection extends EABInputComponent{
  constructor(props){
    super(props);
    this.state = Object.assign( {}, this.state, {
        docId: props.docId || {}
    });
  }

  componentWillReceiveProps(nextProps){
    this.setState({
      docId: nextProps.docId,
      changedValues: nextProps.changedValues
    });
  }

  getCheckBoxValues = (cbValues) =>{
    return cbValues.join(",");
  }

  render() {
    let {lang, validRefValues} = this.context;
    let {changedValues} = this.state;
    let {template, rootValues, values, handleChange, disabled, docId, resetValueItems} = this.props;
    let {id, options, title, hints, mandatory, subType, boxRegularWidth, isSingleSelect, placeholder="No Option", optionTitleAlgin, BoxinOneRow} = template;

    let cValue = this.getValue("");
    let valueArr = [];
    let cbGroup = [];

    if (cValue && typeof cValue == 'string') {
      valueArr = cValue.split(",");
    }

    _.forEach(options, (option, index)=>{
      if (optionSkipTrigger(template, option, changedValues, resetValueItems)) {
        return;
      }

      let onSelected = ()=>{
        if(isSingleSelect){
          valueArr = [option.value];
          this.requestChange(this.getCheckBoxValues(valueArr));
        }else{
          let index = valueArr.indexOf(option.value);
          if(index > -1){
            valueArr.splice(index, 1);
          } else {
            valueArr.push(option.value);
          }
          this.requestChange(this.getCheckBoxValues(valueArr));
        }
      }

      cbGroup.push(
        <IconBox
          key={`ic-${id}-${option.value}`}
          ref={option.id}
          docId={docId}
          image={option.image}
          title={getLocalText(lang, option.title)}
          size={boxRegularWidth}
          txtAlign={optionTitleAlgin}
          selected={valueArr.indexOf(option.value) > -1}
          onCheck={onSelected}
          disabled={option.disabled}
        />
      )
    })

    if (window.isEmpty(cbGroup)) {
      cbGroup.push(<label key={"placeholder"} className="CheckBox">{placeholder}</label>)
    }

    let errorMsg = this.getErrMsg();

    return (
      <div className={styles.FlexColumnContainer}>
        <HeaderTitle title={title} mandatory={mandatory} hints={hints}/>
        <div className={styles.ErrorMsgContainer}>{_.isString(errorMsg) ? <ErrorMessage template={template} message={errorMsg}/> : null}</div>
        <div className={styles.FlexContainer} style={{maxWidth: BoxinOneRow ? (250 * BoxinOneRow) + 'px' : '1400px'}}>
        {cbGroup}
        </div>
      </div>
    );
  }
}

IconBoxSelection.propTypes = Object.assign({}, IconBoxSelection, {
  docId: PropTypes.string
})

export default IconBoxSelection;
