import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from '../Common.scss';

import styled, {css} from 'styled-components';
import * as _ from 'lodash';

import moment from 'moment';

import { MomentDateTimeFormatForApproval, MOMENT_TIME_ZONE, DateTimeFormat3} from '../../constants/ConfigConstants';

import {formatDateWithTime, formatDateTimeWithPresent} from '../../../common/DateUtils';

import CaseStatus from './CaseStatus';

const Container = styled.div`
    line-height: 1.5em;
    display: flex;
min-height: 0px;

    ${
        props => props.iphone && css`
            width: calc(100vw - 12px);
        `
    }

    ${
        props => props.rightAlign && css`
            justify-content: space-between;
        `
    }
`;

const TitleBlock = styled.div`
    display: inline-block;
    font-weight: 600;
    min-width: 240px;
    width: 240px;
    vertical-align: top;

    ${
        props => props.iphone && css`
            min-width: 120px;
            width: 120px;
            padding-right: 6px;
        `
    }
`;

const ContentBlock = styled.div`
    display: flex;
min-height: 0px;
    align-items: flex-end;
`;

class CardItem extends PureComponent{
    convertDisplayValue = (iTemplate, cItemValue, changedValues, agentProfile) =>{

        if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'include' && changedValues[_.get(iTemplate, 'Mapping.id')]){
            cItemValue = cItemValue + ' (' + changedValues[_.get(iTemplate, 'Mapping.id')] + ')';
        } else if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'isEmpty' && _.isEmpty(changedValues[_.get(iTemplate, 'Mapping.value')])){
            cItemValue = _.get(iTemplate, 'Mapping.display');
        } else if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'isEqual' && changedValues[_.get(iTemplate, 'Mapping.valuePair[0]')] === changedValues[_.get(iTemplate, 'Mapping.valuePair[1]')]){
            cItemValue = _.get(iTemplate, 'Mapping.display');
        } else if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'notEqual' && changedValues[_.get(iTemplate, 'Mapping.valuePair[0]')] !== changedValues[_.get(iTemplate, 'Mapping.valuePair[1]')] && _.get(iTemplate, 'Mapping.display')) {
            cItemValue = _.get(iTemplate, 'Mapping.display');
        } else if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'notEqual' && changedValues[_.get(iTemplate, 'Mapping.valuePair[0]')] === changedValues[_.get(iTemplate, 'Mapping.valuePair[1]')] && _.get(iTemplate, 'Mapping.display')) {
            cItemValue = changedValues[iTemplate.id];
        } else if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'checkManagerisMyself' && agentProfile.agentCode === changedValues['managerId']) {
            cItemValue = _.get(iTemplate, 'Mapping.display');
        }

        if (iTemplate.subType && iTemplate.subType.toUpperCase() === 'APPROVAL_DATE' && cItemValue && cItemValue !== '-' ) {
            cItemValue = moment(cItemValue).utcOffset(MOMENT_TIME_ZONE).format(MomentDateTimeFormatForApproval);
        }

        if (iTemplate.subType === 'APPROVALSTATUS') {
            // Return Object
            cItemValue = <CaseStatus template={iTemplate} changedValues={changedValues}/>;
        }

        if (iTemplate.subType && iTemplate.subType.toUpperCase() === 'DATEWITHTIME' && changedValues[iTemplate.id]) {
            let displayTime = new Date(changedValues[iTemplate.id]);
            if (iTemplate.presentation) {
                cItemValue = _.clone(formatDateTimeWithPresent(displayTime, iTemplate.presentation));
            } else {
                cItemValue = _.clone(formatDateWithTime(displayTime));
            }
          }

        if (iTemplate.subType && iTemplate.subType.toUpperCase() === 'EXPIRYDATE' && changedValues[iTemplate.stampedValue]) {
            cItemValue = moment(changedValues[iTemplate.stampedValue]).utcOffset(MOMENT_TIME_ZONE).format(MomentDateTimeFormatForApproval);
        } else if (iTemplate.subType && iTemplate.subType.toUpperCase() === 'EXPIRYDATE' ){
            cItemValue = moment(cItemValue).add(_.get(this.props.app, 'sysParameterConstant.EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION', 14), 'd').utcOffset(MOMENT_TIME_ZONE).format(DateTimeFormat3);
        }

        if (changedValues && iTemplate.mapping && changedValues[iTemplate.mapping.mappingKey] && iTemplate.mapping.mappingValue && iTemplate.mapping.mappingValue.indexOf(changedValues[iTemplate.mapping.mappingKey]) > -1){
            cItemValue = iTemplate.mapping.displayValue;
        }

        if(!cItemValue && iTemplate.emptyValue){
            cItemValue = iTemplate.emptyValue;
        }

        return cItemValue;
    }

    render(){
        let { iTemplate, changedValues, cItemValue, agentProfile} = this.props;
        cItemValue = this.convertDisplayValue(iTemplate, cItemValue, changedValues, agentProfile);
        return (
            <Container key={`${iTemplate.id}-Container-key`} rightAlign={iTemplate.styleClass === 'rightAlign'}>
                <TitleBlock key={`${iTemplate.id}-TitleBlock-key`} iphone={window.isMobile.apple.phone}>
                    {(window.isMobile.apple.phone && iTemplate.mobileName) ? iTemplate.mobileName : iTemplate.name}:
                </TitleBlock>
                <ContentBlock key={`${iTemplate.id}-ContentBlock-key`}>
                    {cItemValue}
                </ContentBlock>
            </Container>
        );
    }
}

CardItem.propTypes = {
    iTemplate: PropTypes.object,
    changedValues: PropTypes.object,
    cItemValue: PropTypes.string,
    agentProfile: PropTypes.object,
    app: PropTypes.object
};

export default CardItem;
