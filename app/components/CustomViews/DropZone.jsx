import React from 'react';
import PropTypes from 'prop-types';
import EABComponent from '../Component'
import * as _ from 'lodash'
import styles from '../Common.scss'

class Dropzone extends EABComponent{
  constructor(props){
    super(props);
    this.state = Object.assign({}, this.state, {
      isDragActive: false
    })
  }
  
  onDragLeave = (e) => {
    this.setState({
      isDragActive: false
    });
  }

  onDragOver = (e) => {
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy';

    this.setState({
      isDragActive: true
    });
  }

  onDrop =(e) =>{
    e.preventDefault();
    this.setState({
      isDragActive: false
    });

    var files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }

    if (_.size(files) > 1  && this.props.forbidMultiFiles) {
      this._returnMultiFiles();
      return;
    }

    _.each(files, this._createPreview);
  }

  onClick = () => {
    this.fileInput.click();
  }

  _returnMultiFiles = () => {
    if (_.isFunction(this.props.onDrop)){
      this.props.onDrop({
        isMultiFiles: true
      });
    }
  }

  _createPreview = (file) =>{
    let reader = new FileReader();
    reader.onloadend = (e)=>{
      if(_.isFunction(this.props.onDrop)){
        this.props.onDrop({
          file: file,
          imageUrl: e.target.result,
        })
      }
    };
    reader.readAsDataURL(file);
  }

  render() {
    let {isDragActive} = this.state;
    let {style, accept, children} = this.props;


    return (
      <div
        className={isDragActive?styles.DropzoneActive:styles.Dropzone}
        onClick={this.onClick}
        onDragLeave={this.onDragLeave}
        onDragOver={this.onDragOver}
        style={style}
        onDrop={this.onDrop}>
        <input
          style={{display: 'none'}}
          type='file'
          ref={(ref)=>{this.fileInput = ref}}
          onChange={this.onDrop}
          value=""
          accept={accept} />
          {isDragActive?"Drop Here":children}
      </div>
    );
  }
}

Dropzone.propTypes = {
  onDrop: PropTypes.func.isRequired,
  accept: PropTypes.string,
  require: PropTypes.object // {width, height}
}

export default Dropzone;