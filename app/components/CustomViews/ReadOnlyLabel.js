/** In this file, we create a React component which incorporates components provided by material-ui */
import React from 'react'
import PropTypes from 'prop-types';
import {ToolTip} from 'material-ui';

import EABComponent from '../Component';
import styles from '../Common.scss';

class ReadOnlyLabel extends EABComponent{
  constructor(props){
    super(props)
    this.state = Object.assign({}, this.state, {
      showTooltips: false
    })
  }

  render() {
    let {muiTheme} = this.context;
    let {
      id, className, style, labelStyle, underline, underlineStyle, disabled, floatingLabelText,
      floatingLabelStyle, defaultValue, fullWidth, tooltip, icon, subType, mandatory, onClick, iTemplateStyle,
      showInOneRow, cardReadOnly
    } = this.props;    
     
    // if (!defaultValue) defaultValue = "-";
    /**if (!isEmpty(defaultValue)) {
      localStyles.floatingLabel.transform = 'scale(1) translate3d(0, 0, 0)';
      localStyles.floatingLabel.color = muiTheme.textField.hintColor;
    }*/

    if (onClick) {
      style.cursor = disabled?'not-allowed':'pointer'
    } else {
      style.cursor = 'text'
    }

    // var star = mandatory?<span style={ {color:'red'} }>&nbsp;*</span>:null;
    let fLabel = null;
    if(floatingLabelText instanceof Object){
      var {
          lang
        } = this.context
      fLabel = getLocalText(lang, floatingLabelText); 
    }else{
      fLabel = floatingLabelText;
    }

   

    let labelId, valueId;
    if (defaultValue && fLabel) {
      labelId = 'LinkedLabel';
      valueId = 'LinkedValue';
    } else {
      labelId = 'SingleLabel';
      valueId = 'SingleValue';
    }


    let labelTextElement = fLabel && showInOneRow ? (
      <label key="label" className={cardReadOnly? styles.ReadOnlyLabelInCard: styles.ReadOnlyLabel}
        style={ Object.assign({}, floatingLabelStyle) } >
        {fLabel}:
      </label>
    ) : fLabel ? (
      <label key="label" className={cardReadOnly? styles.ReadOnlyLabelInCard: styles.ReadOnlyLabel + ' ' + styles.ShowSubContentColor}
        style={ Object.assign({}, floatingLabelStyle) } >
        <span id={labelId} className={styles.ReadOnlyFieldTitleStyle} style={ Object.assign({}, (defaultValue) ? {} : {}) }>{fLabel}</span>
      </label>
    ) : null;

    if(subType && subType == 'password'){
      defaultValue = '********';
    }


    let valueTextElement = defaultValue && showInOneRow ?  (
      <label id={valueId} key="value" className={cardReadOnly? styles.ReadOnlyLabelInCard: styles.ReadOnlyLabel + ' ' + styles.ShowPrimaryBlueColor + ' ' + styles.bold }
        style={ Object.assign({}, labelStyle) } >{defaultValue}</label>
    )
    : defaultValue ? (
      icon ? (
      <label key="value" className={cardReadOnly? styles.ReadOnlyLabelInCard: styles.ReadOnlyLabel}
        style={ Object.assign({}, { display:'inline-flex',
          alignItems:'center'}, labelStyle) } >
        <span id={valueId} key="inner" className={cardReadOnly? styles.ReadOnlyLabelInCard: styles.ReadOnlyLabel} 
          style={ Object.assign({}, {top: null}, labelStyle) }>{defaultValue}</span>
        { icon }
      </label>
      ):(
      <label id={valueId} key="value" className={cardReadOnly? styles.ReadOnlyLabelInCard: styles.ReadOnlyLabel}
        style={ Object.assign({}, labelStyle) } >{defaultValue}</label>)
    ) : null;

    let underlineElement = underline?<hr key="hr" className={styles.ReadOnlyUnderline} style={Object.assign({}, underlineStyle)}/>:null;

    var handleTap = function() {
      if (!disabled && onClick && typeof onClick == 'function') {
        onClick();
      }
    }

    var tooltipBlock = null;
    if (tooltip) {
      tooltipBlock = <ToolTip
        key = {"tt_"+id}
        label = {tooltip}
        show = {this.state.showTooltips}
        />
    }

    let _handleHoverExit = function (e) {
      this.parent.setState({showTooltips: false});
    }

    var _handleHover = function (e) {
      this.parent.setState({showTooltips: true});
    }

    let fieldItems;

    if (showInOneRow) {
      fieldItems = <div style={{display: 'flex', minHeight: '0px', flexWrap: 'no-wrap'}}>
                        {tooltipBlock}
                        {labelTextElement}
                        {valueTextElement}
                        {underlineElement}
                      </div>
    }else {
      fieldItems = [];
      fieldItems.push(tooltipBlock);
      fieldItems.push(labelTextElement);
      fieldItems.push(valueTextElement);
      fieldItems.push(underlineElement);
    }
    if(subType && subType.toUpperCase() == "QRADIOGROUP"){
      iTemplateStyle = {"width":"auto"};
    }
    return (
      <div key="item"
        id="ReadOnlyLabel" 
        className={showInOneRow ? className + ' ' + styles.ReadOnlyFieldRootinOneRow : className + ' ' + styles.ReadOnlyFieldRoot}
        style={Object.assign({}, style, iTemplateStyle)}
        onClick={handleTap}
        onMouseLeave = {tooltip?_handleHoverExit.bind({parent: self}):null}
        onMouseEnter = {tooltip?_handleHover.bind({parent: self}):null}
      >
        {fieldItems}
      </div>
    );
  }
}

ReadOnlyLabel.propTypes = Object.assign({}, ReadOnlyLabel.propTypes, {
  floatingLabelText: PropTypes.string,
  floatingLabelStyle: PropTypes.object,
  underline: PropTypes.bool,
  underlineStyle: PropTypes.object,
  labelStyle: PropTypes.object,
  id: PropTypes.string,
  autoWidth: PropTypes.bool,
  tooltip: PropTypes.array,
  target: PropTypes.object,
  onClick: PropTypes.func,
  defaultValue: PropTypes.any,
  icon: PropTypes.object,
  subType: PropTypes.string,
  mandatory: PropTypes.bool,
})

ReadOnlyLabel.getDefaultProps = Object.assign({}, ReadOnlyLabel.getDefaultProps, {
  fullWidth: false,
  target: null,
  onClick: null,
  defaultValue: '-',
  underline: false
})

export default ReadOnlyLabel;