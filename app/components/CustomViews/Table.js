let React = require('react');
let mui, {RadioButton, TextField, FontIcon, IconButton, RaisedButton, Checkbox} = require('material-ui');
import EABButton from './Button'
import InputDialog from './InputDialog'
import appTheme from '../../theme/appBaseTheme.js';
import EABComponent from './EABInputComponent.js';
import styleClss from '../Common.scss';
import * as _ from 'lodash';
import ErrorMessage from './ErrorMessage';
import ConfigConstants from '../../constants/ConfigConstants';

import DynColumn from '../DynViews/DynColumn';

class EABTable extends EABComponent {
  constructor(props) {
      super(props);
      this.state = Object.assign( {}, this.state, {
        template: props.template,
        changedValues: this.props.changedValues,
        dialogValues: {},
        showDialog:false
      });
  }

  getStyles = () => {
    let {muiTheme} = this.context
    var table = {
      'border': '1px solid ' + muiTheme.palette.borderColor
    }
    var tr = {
      'border': '1px solid ' + muiTheme.palette.borderColor
    }
    var td = {
      'border': '1px solid ' + muiTheme.palette.borderColor,
      'position': 'relative',
      'verticalAlign':'top'
    }

    var midTh = {
      textAlign: 'center',
      fontSize: '12px',
      fontWeight: 'normal',
      color: '#7f7f7f'
    }

    var leftTh = {
      textAlign: 'left',
      fontSize: '12px',
      fontWeight: 'normal',
      color: '#7f7f7f'
    }

    var rightTh = {
      textAlign: 'right',
      fontSize: '12px',
      fontWeight: 'normal',
      color: '#7f7f7f'
    }

    var leftTd = {
      textAlign: 'left',
      color: muiTheme.palette.textColor,
      fontSize: '16px',
      fontWeight: 400
    }

    var leftTdMod = {
      textAlign: 'left',
      fontSize: '16px'
    }

    var rightTd = {
      textAlign: 'right',
      color: muiTheme.palette.textColor,
      fontSize: '16px',
      fontWeight: 400
    }

    var addBorder = {
      'border': '1px solid ' + muiTheme.palette.borderColor
    }


    var tableNoBorder = {
      'border': 'none'
    }
    var trNoBorder = {
      'border': 'none'
    }
    var tdNoBorder = {
      'border': 'none',
      'position': 'relative',
      'verticalAlign':'middle'
    }

    var thBold = {
      fontWeight: 'bold'
    }

    var styles = { table, tr, td, leftTh, rightTh, midTh, tableNoBorder, trNoBorder, tdNoBorder, leftTd, rightTd, addBorder, thBold, leftTdMod};

    return styles;
  }

  add = () => {
    let {changedValues , template } = this.state;
    let id = template.id;
    let index = this.state.index;
    if((!index && index != 0) || index == changedValues[id].length)
      changedValues[id].push(this.state.dialogValues);
    else
      changedValues[id][index] = this.state.dialogValues;

    this.props.validate();
    this.setState(
      {
        changedValues: changedValues,
        showDialog: false,
        dialogValues:{}
      }
    )

  }

  onEditClick = (index) => {
    let {changedValues, template } = this.state;
    if(template.allowEdit){
      let dialogValues = _.cloneDeep(changedValues[template.id][index]);
      this.setState({showDialog: true, dialogValues: dialogValues, index: index});
    }
  }

  onCancelClick = () => {
    this.setState({showDialog: false, dialogValues: {}});
  }

  addOnlick = () => {
    var changedValues = this.state.changedValues;
    var id = this.state.template.id;
    if(!changedValues[id]){
      changedValues[id] = [];
    }
    this.setState({showDialog: true, dialogValues:{}, index: changedValues[id].length || 0, changedValues: changedValues});
  }

  getDisplayText = (item, value, rowValue) => {
    let result;
    let {ccy} = this.props;

    if(!item)
      return null;
    if(!item.type)
      return value;

    let show = true;
    if (item.trigger && !showCondition(rowValue, item, rowValue, rowValue, rowValue, null, this.context)) {
      show = false;
    }
    if(show){
      var viewType = item.type.toUpperCase();
      if( viewType== "PICKER" || item.type && item.type.toUpperCase() == 'QRADIOGROUP'){
        if(item.valueId){
          return rowValue[item.valueId];
        }
        else if(rowValue[item.id] == "other" && rowValue['resultOther']){
          return rowValue['resultOther'];
        }
        else{
          return getOptionsTitle(item, value, this.context.optionsMap);
        }
      }
      else if(viewType == 'IMAGE'){
        return value ? <img src={value} width="40" height="50"/> : null;
      }else if(viewType == 'CHECKBOX'){
        if(item.valueId && value=="Y"){
          return rowValue[item.valueId];
        }else if(item.id == "other" && value == 'Y' && rowValue && rowValue['resultOther']){
          return rowValue['resultOther'];
        }else{
          return (value && value == 'Y') ? item.title : "";
        }
      }else if(viewType == 'DATE' || viewType == 'DATEPICKER'){
        if(value){
          let dateValue = new Date(value);
          value = dateValue.format(item.dateFormat || ConfigConstants.DateFormat);
        }
        return value;
      }
      else{
        if(value instanceof Object){
          var {
            lang
          } = this.context
          return window.getLocalText(lang, value);
        }else{
          if (item.subType === 'currency'){
            let dec = 2;
            if (item.decimal || item.decimal === 0) {
              dec = item.decimal;
            }
            value = item.showHyphenZeroValue && !value ? '-' : window.getCurrencyByCcy(value, ccy, dec);
          }else if(item.subType == "year"){
            value = value + " Years";
          }else if (item.subType == 'percent') {
            value += "%"
          }

          if (item.postfix) {
            value = `${value} ${item.postfix}`;
          }

          return value;
        }

      }
    }
    else{
      (_.get(item, 'trigger.replacement')) ? result = _.get(item, 'trigger.replacement') : result = null;
      return result;
    }
  }

  getPresentation = (item, rowValue) => {
    var presentation = item.presentation;
    var showKeys = []
    // '@' -> horizontal
    // '$' -> vertical
    if(presentation.indexOf('/')!== -1){
      showKeys = presentation.split('/');
    }else if(presentation.indexOf('@')!== -1){
      showKeys = presentation.split('@');
    }else if(presentation.indexOf('$')!== -1){
      showKeys = presentation.split('$');
    }else{
      showKeys = presentation.split('&');
    }

    var itemValue = presentation;
    if(itemValue.indexOf('@')!== -1){
      var resultItems = []
      for(var k = 0; k < showKeys.length; k++){
        var keyView = this.getViewById(item, showKeys[k]);
        var keyValue = this.getDisplayText(keyView, rowValue[showKeys[k]], rowValue) || '';

        if (keyValue && keyValue.length > 0) {
          resultItems.push(<div key={'presentation-' + k + '-' + itemValue} className={(item.disableTdStyle) ? undefined : styleClss.tableViewItem}>{keyView.type.toUpperCase() == 'CHECKBOX'? this.genCheckBoxItem(keyValue): keyValue}</div>)
        }
      }
      return <div className={(item.disableTdStyle) ? undefined : styleClss.tableView}>{resultItems}</div>
    }else if(itemValue.indexOf('$') !== -1){
      var resultItems = []
      for (var k = 0; k < showKeys.length; k++) {
        var keyView = this.getViewById(item, showKeys[k]);
        var keyValue = this.getDisplayText(keyView, rowValue[showKeys[k]], rowValue) || '';
        if (keyValue && keyValue.length > 0) {
          resultItems.push(<div key={'presentation-' + k + '-' + itemValue} className={styleClss.verticalView}>{keyView.type.toUpperCase() == 'CHECKBOX'? this.genCheckBoxItem(keyValue): keyValue}</div>)
        }
      }
      return <div className={styleClss.verticalViewItem}>{resultItems}</div>
    } else if (itemValue.indexOf('(') > -1 && itemValue.indexOf(')') > -1) {
      let mainValueId = itemValue.substring(0, itemValue.indexOf('(')).trim();
      let mainView = this.getViewById(item, mainValueId);
      let mainText = this.getDisplayText(mainView, rowValue[mainValueId], rowValue) || '';

      let postfixId = itemValue.substring(itemValue.indexOf('(') + 1, itemValue.indexOf(')')).trim();
      let postfixView = this.getViewById(item, postfixId);
      let postfixText = this.getDisplayText(postfixView, rowValue[postfixId], rowValue) || '';

      return <div key={'presentation-' + k + '-' + itemValue} className={styleClss.verticalViewItem}>{mainText + (postfixText.length > 0 ? ' (' + postfixText + ')' : '')}</div>
    }else{
      for(var k = 0; k < showKeys.length; k++){
        var keyView = this.getViewById(item, showKeys[k]);
        var keyValue = this.getDisplayText(keyView, rowValue[showKeys[k]], rowValue) || '';
        if (keyValue && keyValue.length > 0) {
          itemValue = itemValue.replace(showKeys[k], keyValue);
        }
      }
      return itemValue;
    }
  }

  getViewById = (parenTitem, id) =>{
    if(parenTitem.id && parenTitem.id == id){
      return parenTitem;
    }else{
      var items = parenTitem.items;
      for(var i = 0; i < items.length; i++){
        if(items[i].id == id)
          return items[i]
      }
    }
    return null;
  }

  genCheckBoxItem = (title) => {
    return <Checkbox
      className={styleClss.CheckBox}
      label={title}
      inputStyle={{ cursor: 'default' }}
      labelStyle={{ color: '#000' }}
      checked={true}
      disabled={true} />
  }

  deleteClick = (index) => {
    let self = this;
    var changedValues = this.state.changedValues;
    var id = this.state.template.id;
    var tableValues = changedValues[id];
    tableValues.splice(index,1);
    changedValues[id] = tableValues;

    this.setState(
      {
        changedValues: changedValues,
        dialogValues:{},
        showDialog: false
      }
    )
    self.props.validate();

  }

  _getLastItemIndexWithId = (items) => {
    let result;
    if (items) {
      items.forEach((obj, index) => {
        if (obj.id) {
          result = index;
        }
      });
    }
    return result;
  }

  _getLastItemIndexExceptParagraph = (items) => {
    if (items) {
      for (let index = items.length - 1; index >= 0; index--) {
        if (items[index].type && items[index].type.toUpperCase() !== 'PARAGRAPH') {
          return index;
        }
      }
    }
    return items.length - 1;
  }

  genItems = (rowValues, items, allowEdit) => {
    let self = this;
    var {hasBorder, allowEdit, no1stItemLeftPadding, totalColSpan, equalWidthCol, setWidth, zebraRow, generateTotalRow, id, totalRowAlignLeft, disabled, totalIndex, blueHighlighter} = this.state.template;
    var styles = this.getStyles();
    let tdStyle = hasBorder ? styles.td : styles.tdNoBorder;
    var rows = [];
    if(!rowValues)
      return rows;

    for(let j = 0; j < rowValues.length; j++){
      var rowValue = rowValues[j];
      var row = [];
      let onTdClick = function(){
          self.onEditClick(j);
      }
      for(var i = 0; i < items.length; i++){
        var item = items[i];
        if (_.get(item, 'id') === undefined && _.get(item, 'key') === undefined && _.get(item, 'items') === undefined) {
          continue;
        }
        var iconDel = null;
        let styleObj = (blueHighlighter) ? styles.leftTdMod : styles.leftTd;
        let tdClassName = '';
        styleObj = Object.assign(styleObj, tdStyle);

        if (blueHighlighter) {
          tdClassName = styleClss.bold + ' ' + styleClss.ShowPrimaryBlueColor;
        }

        if(no1stItemLeftPadding){
          styleObj = Object.assign({}, styleObj, tdStyle, {paddingLeft:0});
        }

        if (item.center) {
          styleObj = Object.assign({}, styleObj, {textAlign:'center'});
        }

        if(item.right){
          styleObj = Object.assign({}, styleObj, {textAlign:'right'});
        }

        if (totalColSpan) {
          styleObj = Object.assign({}, styleObj, { width: (100 * (item.colSpan || 1) / totalColSpan) + '%'});
        }

        if(equalWidthCol) {
          styleObj = Object.assign({}, styleObj, { width: (100 / items.length) + '%' });
        }

        if(setWidth) {
          styleObj = Object.assign({}, styleObj, { width: item.inputWidth });
        }

        if (item.presnetationStyle) {
          styleObj = Object.assign({}, styleObj, item.presnetationStyle);
        }

        if((i == items.length - 1 || this._getLastItemIndexExceptParagraph(items) === i) && allowEdit && rowValues.length != 1)
          iconDel =  <FontIcon onClick={()=>{this.deleteClick(j)}} className={"material-icons" + " " + styleClss.deleteIcon}>delete</FontIcon>
        if (item.presentation){
          var itemValue = this.getPresentation(item, rowValue);
          row.push(<td key={"td-"+j+"-"+i} style={styleObj} className={styleClss.dynTd + ' ' + tdClassName}><div><div onClick={onTdClick}>{itemValue || ""}</div>{iconDel}</div></td>);
        } else if (item.editable) {
          let displayComp = this._genRowEditableItem(item, j, rowValue[item.id], rowValue, disabled);

          row.push(<td key={`td-${j}-${i}`} style={styleObj} className={styleClss.dynTd + ' ' + tdClassName}><div><div>{displayComp} </div>{iconDel}</div></td>);
        }  else {
          let displayText = (item.notShowInTable) ? '' : this.getDisplayText(item, rowValue[item.id], rowValue) || '';
          let displayComp = item.type.toUpperCase() == 'CHECKBOX'? this.genCheckBoxItem(displayText): displayText;

          if  (item.highlighter && item.highlighter.content && item.highlighter.style && displayComp && typeof displayComp === 'string') {
            let foundIndex = _.indexOf(displayComp, item.highlighter.content);
            if (foundIndex !== 0) {
              row.push(<td key={"td-"+j+"-"+i} style={styleObj} className={styleClss.dynTd + ' ' + tdClassName}>
                          <div>
                            <div onClick={onTdClick}>
                              {displayComp.substr(0, foundIndex)} <span style={item.highlighter.style}>{item.highlighter.content}</span> {displayComp.substr(item.highlighter.content.length, displayComp.length)}
                              {(item.subPostfix) ?
                                <div className={styleClss.dynTh_subTitle}>{item.subPostfix}</div> :
                                undefined
                              }
                            </div>
                          {iconDel}
                          </div>
                        </td>);
            } else {
              row.push(<td key={"td-"+j+"-"+i} style={styleObj} className={styleClss.dynTd + ' ' + tdClassName}>
                        <div>
                          <div onClick={onTdClick}>
                          <span style={item.highlighter.style}>{item.highlighter.content}</span> {displayComp.substr(item.highlighter.content.length, displayComp.length)}
                            {(item.subPostfix) ?
                              <div className={styleClss.dynTh_subTitle}>{item.subPostfix}</div> :
                              undefined
                            }
                          </div>
                        {iconDel}
                        </div>
                      </td>);
            }
          } else {
            row.push(<td key={"td-"+j+"-"+i} style={styleObj} className={styleClss.dynTd + ' ' + tdClassName}><div><div onClick={onTdClick}>{displayComp} {(item.subPostfix) ? <div className={styleClss.dynTh_subTitle}>{item.subPostfix}</div> : undefined} </div>{iconDel}</div></td>);
          }
        }
      }

      if (zebraRow && j % 2 === 0) {
        rows.push(<tr key={"tr"+j} className={styleClss.ZebraRow}>{row}</tr>);
      } else {
        rows.push(<tr key={"tr"+j}>{row}</tr>);
      }
    }

    if (generateTotalRow) {
      rows = this._genLastTotalRow(rows, items, id, totalRowAlignLeft, totalIndex);
    }

    return rows;
  }

  tableCellOnChange = (id, value) => {
    let {changedValues} = this.state;
    let { template: items } = this.props;

    let templateId = _.get(_.split(id, '-'), '[0]');
    let index = _.get(_.split(id, '-'), '[1]');
    if (changedValues && changedValues[items.id] && changedValues[items.id][index] && templateId){
      changedValues[items.id][index][templateId] = value;

      if (_.isFunction(this.props.completeChangedValues)) {
        this.props.completeChangedValues();
      }
    }
  }

  _genRowEditableItem = (item, index, value, rowValue, disableFrTable) => {
    let template = {
      'type': 'layout',
      items: []
    };
    let trigger = true;
    if (item.tableTrigger) {
      trigger = this._checkTriggerCondition(item, rowValue);
    }

    if (trigger) {
      let templateItem = _.cloneDeep(item);
      let disabled;
      if (item.disableRowTrigger) {
        disabled = this._checkDisableCondition(rowValue, item);
      }
      templateItem.title = templateItem.tableCellTitle;
      templateItem.id = `${item.id}-${index}`;
      templateItem.disabled = disableFrTable || disabled;
      template.items.push(templateItem);
      return (<DynColumn
        template={template}
        values={{[`${item.id}-${index}`]: value}}
        changedValues={{[`${item.id}-${index}`]: value}}
        handleOnChange={this.tableCellOnChange}
      />);
    } else {
      return _.get(item, 'tableTrigger.replacement');
    }
  }

  _checkDisableCondition = (rowValue, templateObj) => {
    let lookupValue = _.get(rowValue, `${_.get(templateObj, 'disableRowTrigger.id')}`);
    let determinedValue = _.get(templateObj, 'disableRowTrigger.value');
    if (lookupValue === determinedValue) {
      return true;
    } else {
      return false;
    }
  }

  _checkTriggerCondition = (item, rowValue) => {
    let checkId = _.get(item, 'tableTrigger.id');
    let checkValue = _.get(rowValue, checkId);
    let result;
    switch (_.get(item, 'tableTrigger.type')) {
      case 'showIfNotEqual':
        (checkValue == _.get(item, 'tableTrigger.value')) ? result = false : result = true;
        break;
    }
    return result;
  }

  _getTotal = (tableValues, id) => {
    let total = 0;
    _.each(tableValues, (rowValues, key) => {
      total += _.get(rowValues, id);
    });

    return total;
  }

  _genLastTotalRow = (rows, items, id, totalRowAlignLeft, totalIndex = 0) => {
    let row = [];
    let { changedValues } = this.state;
    let {ccy} = this.props;
    let tableValues = changedValues[id];
    for(let i = 0; i < items.length; i++){
      let item = _.cloneDeep(items[i]);
      if (_.get(item, 'id') === undefined && _.get(item, 'key') === undefined && _.get(item, 'items') === undefined) {
        continue;
      }
      let displayComp;
      if (i === totalIndex) {
        displayComp = 'Total: ';
      } else if (item.calTotal) {
        displayComp = this._getTotal(tableValues, item.id);
      }

      if (item.subType === 'currency'){
        let dec = 2;
        if (item.decimal || item.decimal == 0) {
          dec = item.decimal
        }

        displayComp = window.getCurrencyByCcy(displayComp, ccy, dec);
      }

      row.push(<td key={`td-${i}-${items.length + 1}`} className={styleClss.padding12}><div>{displayComp} </div></td>);
    }

    let className;
    if (totalRowAlignLeft) {
      className = styleClss.dynTotalLeftTr;
    } else {
      className = styleClss.dynTotalTr;
    }

    rows.push(<tr key={`tr-${items.length + 1}`} className={className}>{row}</tr>);
    return rows;
  }

  render() {
    var self = this;
    let {validRefValues, muiTheme} = this.context;
    let errorMsg = this.getErrMsg();
    var {
      template,
      values,
      width,
      handleChange,
      rootValues,
      deleteAble,
      allowEdit,
      ...others
    } = this.props;
    var {changedValues, dialogValues} = this.state;
    var { id, allowEdit, hasBorder } = template;
    var styles = this.getStyles();

    var headers = [];
    var {
      changedValues
    } = this.state;
   if(!this.props.changedValues[template.id] || this.props.changedValues[template.id] == null){
      if(template.static){
        changedValues[template.id] = template.values;
      }
    }

    var {
      id,
      type,
      title,
      placeholder,
      disabled,
      value,
      min,
      max,
      multiLine,
      interval,
      subType,
      hasBorder,
      fullWidth,
      style,
      no1stItemLeftPadding,
      planDetailLayout,
      equalWidthCol,
      setWidth,
      thBold
    } = template;

    var cValue = self.getValue([])
    changedValues[id] = cValue;

    placeholder = placeholder || "No records found."

    var rows = [];

    var items = template.items;
    for(var i in items){
      var item = items[i];

      if (_.get(item, 'id') === undefined && _.get(item, 'key') === undefined && _.get(item, 'items') === undefined) {
        continue;
      }

      let _sty = hasBorder ? Object.assign({}, styles.addBorder, styles.midTh) : {};
      let _className = template.noBottomBorder ? styleClss.dynTh_noBottomBorder : styleClss.dynTh;

      if (item.width){
        _sty.width = item.width;
      }

      if (thBold) {
        _sty = Object.assign({}, _sty, styles.thBold);
      }

      if (planDetailLayout) {
        _className = styleClss.AppPlanDetailLayout + ' ' + _className ;
      } else if (no1stItemLeftPadding) {
        _sty = Object.assign({}, _sty, { paddingLeft: 0 });
      } else {
        _sty = Object.assign({}, _sty, styles.thBold, {
          background: muiTheme.palette.primary2Color,
          color: muiTheme.palette.primary4Color
        });
      }

      if (item.center){
        _sty = Object.assign({}, _sty, {textAlign:'center'});
      }

      if (item.right){
        _sty = Object.assign({}, _sty, {textAlign:'right'});
      }

      if (item.left){
        _sty = Object.assign({}, _sty, {textAlign:'left'});
      }

      if (equalWidthCol) {
        _sty = Object.assign({}, _sty, { width: (100 / items.length) + '%' });
      }

      if(setWidth) {
        _sty = Object.assign({}, _sty, { width: item.inputWidth });
      }

      if (item.presnetationStyle) {
        _sty = Object.assign({}, _sty, item.presnetationStyle);
      }

      headers.push(<th key={"th-"+i} style={_sty} className={_className}>{item.title}{(item.subTitle) ? <div className={styleClss.dynTh_subTitle}>{item.subTitle}</div> : undefined}</th>);
    }

    var rows = this.genItems(changedValues[id], items, allowEdit);

    max = max || 8;
    let showAllowButton = allowEdit && (max ? (changedValues[id].length< max) :true );

    var title = getLocalText("en", title);
    var btTemplate = {
      "title" : "Add"
    }

    let error = (errorMsg && errorMsg != "") ? <div className={styles.ErrorMsgContainer}>{_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg} style={{top: '0px', left: '0px'}}/>: null}</div>:null;

    return <div
      key={"tblDiv_"+id}
      className={"DetailsItem"}>
      { title ? <span style={{
        fontSize: '20px',
        lineHeight: '40px',
        fontWeight: 300
      }}>{title}</span>:null }
      {error}
      <table key={"tbl" + id} className={styleClss.DetailsItemTable} style={hasBorder ? Object.assign(styles.table, { width: (fullWidth) ? '100%' : 'calc(100% - 48px)' }, (style ? style : {})) : Object.assign(styles.tableNoBorder, { width: (fullWidth) ? '100%' : 'calc(100% - 48px)' }, (style ? style : {}))}>
        <thead>
          <tr>
            {headers}
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
      {
        showAllowButton?
       <EABButton
          handleChange = {this.addOnlick}
          template = {btTemplate}
          style={{padding: '12px', maxHeight: '30px', paddingLeft: '0px'}}
        />:null
      }
      {this.state.showDialog?
      <InputDialog
        open={this.state.showDialog}
        genItems = {this.props.renderItems}
        template = {template}
        index = {1}
        changedValues = {dialogValues}
        onSaveClick = {this.add}
        onCancelClick = {this.onCancelClick}
      />:null}

    </div>
  }
}

export default EABTable;
