import React, {Component} from 'react';
import PropTypes from 'prop-types';
import EABComponent from '../Component';
import ConfigConstants from '../../constants/ConfigConstants.js';
import EABToolTips from './ToolTips';
import numeral from 'numeral';
import * as _ from 'lodash';
import * as _v from '../../utils/Validation';
import * as _n from '../../utils/needs';
import * as _c from '../../utils/client';
import styles from '../Common.scss';

class EABInputComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            options: props.template?this._getOptionsByReference(props.template):null,
            changedValues: props.changedValues,
            valid: false
        }
    }

    componentWillReceiveProps(newProps, newState) {
        this.setState({
            changedValues: newProps.changedValues
        });
    }

    _getOptionsByReference = (field) =>{
      let {rootValues} = this.props;
      let {reference, options} = field;

      if(!this.context)
        return options;

      if(reference && isEmpty(options)){


        let template = this.context.rootTemplate;
        let ref = reference.replaceAll("@", "/");
        let path = ref.split("/");
        let refField = getFieldFmTemplate(template, path, 0, "options");

        if(!isEmpty(refField && refField.options)){
          return refField.options
        }
        else if(!isEmpty(rootValues)){
          let refValues = getValueFmRootByRef(rootValues, reference, []);
          if(!isEmpty(refValues)){
            options = [];
            _.forEach(refValues, (refValue, index)=>{
              options.push({
                value: refValues[index],
                title: refValues[index]
              })
            })
            return options;
          }
        }
      }
      return options;
    }

    getStyles=()=>{
      let {muiTheme: theme} = this.context;

      return {
        floatingTitleStyle : {
          lineHeight: '22px',
          top: '24px',
          bottom: 'none',
          fontSize: '16px',
          opacity: 1,
          color: theme.textField.fixedLabelColor,
          transform: 'perspective(1px) scale(0.875) translate3d(0px, -28px, 0)',
          transformOrigin: 'left top',
          position: 'absolute',
          width: '115%',
          zIndex: 100
        },
        titleStyle: {
          overflow: 'hidden',
          textOverflow: 'ellipsis'
        },
        headerTitleStyle : {
          display: 'block'
        },
        disabledUnderline: {
          borderColor: "RGB(202,196,196)"
        },
        floatingShrinkStyle: {
          color: theme.palette.primary2Color, fontSize: '18px', top: '28px', whiteSpace: 'nowrap'
        },
        // AutoSuggestfloatingShrinkStyle: {
        //   color: theme.palette.primary2Color, fontSize: '18px', top: '31px', whiteSpace: 'nowrap'
        // },
        floatingShadowLabelStyle: {
           color: theme.palette.text1Color, top: '28px', lineHeight: '24px', whiteSpace: 'nowrap'
        },
        floatingisCustomLabelStyle:{
          top:'28px', lineHeight:'24px', whiteSpace: 'nowrap'
        },
        floatingLabelStyle: {
          color: theme.palette.primary2Color, top:'28px', lineHeight:'24px', whiteSpace: 'nowrap'
        },
        // PickerfloatingLabelStyle: {
        //   top:'28px', lineHeight:'24px', whiteSpace: 'nowrap'
        // },
        hintStyle: {
          bottom: '32px', fontSize:'14px', lineHeight:'16px'
        },
        isCustomColumnField: {
          height: '32px'
        },
        columnField: {
          width:'100%', minWidth:'240px'
        },
        errorStyle: {
          lineHeight:'16px', bottom: '16px'
        },
        // datePickerErrorStyle: {
        //   lineHeight:'16px', bottom: '14px'
        // },
        // pickerErrorStyle: {
        //   lineHeight: '16px', bottom: '0px'
        // },
        // NOTE: lucia dropDownMenu top 20 to -40
        pickerLabel: {
          lineHeight: '40px', height:40, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace:'nowrap', top: '20px'
        },
        textFieldLabel: {
          width:"100%", height: '80px', display:'block', top: '0px'
        },
        LabelinButton: { fontSize: '16px', color: theme.palette.primary1Color, fontWeight: 500
        },
        LabelinButtonIncon: { fontSize: '16px', color: "#FFFFFF", fontWeight: 500
        },
        LabelinDisabledButton: { fontSize: '16px', color: theme.palette.accent3Color, fontWeight: 500
        },
        LabelinDisabledButtonIncon: { fontSize: '16px', color: "#FFFFFF", fontWeight: 500
      } ,
        WhiteBtnLabel: {
          color: theme.palette.secondary1Color, fontWeight: 500, fontSize: '14px'
        },
        selectedBtn: { width: '240px', border: `solid 2px ${theme.palette.selectColor}`, margin: '3px 12px', minHeight: '42px', height: '100%'
        },
        filledBtn: { minWidth: '240px', border: `solid 2px ${theme.palette.primary2Color}`, margin: '3px 12px', minHeight: '42px', height: '100%'
        },
        filledBtnLabel: { color: theme.palette.secondary1Color, fontWeight: 'bold'
        },
        normalfilledBtnLabel: { color: theme.palette.secondary1Color
        },
        notSelectedBtn: { minWidth: '240px', border: `solid 2px ${theme.palette.primary2Color}`, margin: '3px 12px', minHeight: '42px', height: '100%'
        },
        notSelectedLabel: { color: theme.palette.primary1Color, fontWeight: 'bold'
        },
        normalnotSelectedLabel: { color: theme.palette.primary1Color
        },
        largeTextSelection: {
          margin: '0px 24px 24px 24px', padding: '12px', display: 'flex', minHeight: '0px', width: '320px', alignItems: 'center', cursor: 'pointer'
        },
        rightTextField: {
          textAlign: 'right', marginTop: '0px !important', color: theme.palette.primary2Color
        },
        leftTextField: {
          textAlign: 'left', marginTop: '0px !important', color: theme.palette.primary2Color
        },
        showGreenColor: {
          color: theme.palette.primary5Color
        },
        showRedColor: {
          color: theme.palette.alertColor
        },
        tableHeaderStyle: {
          backgroundColor: theme.palette.panelBackgroundColor, color: theme.palette.textColor, fontWeight: 500, fontSize: '12px', whiteSpace: 'initial'
        },
        tableSubHeaderStyle: {
          backgroundColor: theme.palette.borderColor, fontWeight: 500, fontSize: '12px', height: '24px'
        },
        paperStyle: {
          display: 'flex', minHeight: '0px', width: 'calc(100% - 48px)', alignItems: 'center', height: '100%', marginBottom: '16px'
        },
        toolBarTitle: {
          color: theme.palette.primary1Color, paddingLeft: '16px', fontWeight: 600, fontSize: '20px'
        },
        toolBarActionFlatBtn: {
          color: theme.palette.primary1Color, fontWeight: 600, fontSize: '20px'
        },
        toolBarActionDisabledFlatBtn: {
          color: theme.palette.accent3Color, fontWeight: 600, fontSize: '20px'
        },
        toolBar: {
          borderBottom: '1px solid ' + theme.palette.borderColor, boxShadow: '0px 2px 4px ' + theme.palette.borderColor, backgroundColor: theme.palette.panelBackgroundColor
        },
        // NOTE: lucia dropDownMenu
        selectedMenuItemStyle: {
          color: theme.palette.primary1Color, /*fontWeight: 500,*/
        }
      }
    }

    getFieldTitle = (mandatory, title, tips) =>{
      let {langMap, lang, muiTheme} = this.context;
      let titleText = getLocalizedText(langMap, getLocalText(lang, title));

      // if) {
        return (<div key={titleText}>{titleText} {(mandatory && titleText && titleText != "")?<span className={styles.mandatoryStar}>&#42;</span>:""}
          {tips?<EABToolTips hints={tips.msg} iconType={tips.icon} Iconstyle={{padding: 0, height: '24px'}}/>:""}
        </div>);
      // }else {
      //   return (<div key={titleText}>{titleText}</div>);
      // }
    }

    getFieldTitleNoStar = (mandatory, title) =>{
      let {langMap, lang, muiTheme} = this.context;
      let titleText = getLocalizedText(langMap, getLocalText(lang, title));
        return (<div key={titleText}>{titleText} {(mandatory && titleText && titleText != "")?<span className={styles.mandatoryStar}></span>:""}</div>);
    }

    checkDiff = () => {
      let {changedValues} = this.state;
      let {requireShowDiff, template, values} = this.props;
      let {id, type} = template;
      let value = values[id];
      let cValue = changedValues[id];

      if(!requireShowDiff)
        return "";

      type = _.toUpper(type);
      if(type === 'CHECKBOX'){
        value = (_.toUpper(value) === 'Y');
        cValue = (_.toUpper(cValue) === 'Y');
      }

      return !isEqual(value, cValue) ? " diff": "";
    }

    getErrMsg = () =>{
      let {lang='en'} = this.context;
      let {error, template} = this.props;
      let {errorCode} = this.state;
      let code = _.at(error, `${template.id}.code`)[0];
      return _v.getErrorMsg(template, errorCode || code, lang);
    }

    getValue = (dValue) => {
        var {changedValues} = this.state;
        let {template} = this.props;

        if (template) {
            let {id, value, allowZero, allowNegative} = template;
            var cValue = id && changedValues && changedValues[id];
            if(allowZero || allowNegative){
              if(!_.isNumber(cValue) && !allowNegative)
                cValue = changedValues[id] = !isEmpty(value)?value:(dValue != undefined?dValue:null);
            }
            else if (isEmpty(cValue)) {
                cValue = changedValues[id] = !isEmpty(value)?value:(dValue != undefined?dValue:null);
            }
        } else {
            cValue = dValue;
        }
        return cValue;
    }

    _getDisplayValue = (item, value, values) => {
        if (item.options && value) {

            if (value == '*') {
                return getLocalizedText('OPTION.ALL');
            }

            for (let idx in item.options) {
                var op = item.options[idx];

                if (optionSkipTrigger(item, op, values)) {
                continue;
                }
                if (op.value.toUpperCase() == value.toUpperCase()) {
                return op.title;
                }
            }
        }
        else if (item.type == 'datepicker' && value instanceof Date) {
            return value.format(ConfigConstants.DateFormat);
        }
        else if (item.type.toUpperCase() == 'CHECKBOX') {
            if (item.id == "default") {
                if (value == "Y") {
                    return " (Default)";
                } else {
                    return "";
                }
            }
        }
        return value;
  }

  requestChange = (value) =>{
    var item = this.props.template;
    let {type, isKey, subType, onChange, max, min, maxLength, warning, id} = item;
    let {changedValues} = this.state;
    this.state.errorCode = null;
    type = _.toUpper(type);
    subType = _.toLower(subType);
 /**   if(value === _.get(changedValues, item.id)){
      return;
    }*/
    if(warning){
      eval(`var execFunc = ${warning}; this.state.errorCode = execFunc(item, {${id}: value})`);
    }
    if (type === 'TEXT') {
      if (value && isKey) {
        value = _.toUpper(value);
      }

      // validation
      var error = null;
      if (subType === 'number' || subType === 'currency') {
        // if (isNaN(value) && !(value == "." || value == "-")) {
        //   return
        // }
      }
      else {
        if (max && value.length > max) {
          this.setState({errorCode: 406});
          return
        }
      }
    } else if (type === 'TEXTAREA') {
      //reset to default value if template is set
      if (maxLength && value.length > maxLength) {
        this.setState({errorCode: 406});
        value = value.substring(0, maxLength);
      }
    } else
    if (type === 'DATEPICKER') {
      //value = value.getTime();
      // var d = new Date(value);
      // value = d.getTime();

    } else
    if (type === 'CHECKBOX' || type === 'CHECKBOXPOPUP') {
      value = value?"Y":"N"
    }

    if(onChange){
      let newValue, validRefValues = this.context.validRefValues || {};
      let {profile} = this.context.store.getState().client;
      validRefValues.optionsMap = this.context.optionsMap;
      validRefValues.profile = profile;
      if (_.isString(onChange)) {
        eval(`var execFunc = ${onChange};newValue = execFunc(item, value, this.state.changedValues, this.props.rootValues, validRefValues)`);
      } else if (_.isPlainObject(onChange)) {
        if (onChange.module) {
          let funcModule = 
            onChange.module === 'needs' ? _n :
            onChange.module === 'client' ? _c :
            null;

          let onChangeFunc = _.get(funcModule, onChange.type);
          if (_.isFunction(onChangeFunc)) {
            newValue = onChangeFunc(item, value, this.state.changedValues, this.props.rootValues, validRefValues, onChange.values);
          }
        }
      }

      if (newValue) {
        value = newValue;
      }
    }

    // bubble up the changeds

    if (_.isFunction(this.props.requestChange)) {
      this.props.requestChange(value, item.id);
    } else {
      this.handleChange(value);
    }
  }

  // only for text field / text area
  handleBlur = (value) => {
    var item = this.props.template;
    let {type, isKey, subType, onBlur, max, min, maxLength, warning, id} = item;
    let { changedValues } = this.state;

    if (item.type.toUpperCase() == 'TEXT' || item.type.toUpperCase() == 'TEXTFIELD') {
      if (item.subType && item.subType == 'number') {
        if (value != undefined && !isNaN(parseFloat(value)) && item.presentation) {
          value = numeral(value).format(item.presentation);
        } else {
          value = !isNaN(parseFloat(value))?parseFloat(value):0;
        }

        if (item.min != undefined && value < item.min) {
          // error = langMap["INPUT_ERROR.AMOUNT_LESS_THAN"]?"Amount less than {0}":null
          value = item.min;
        }
        if (item.max != undefined && value > item.max) {
          // error = langMap["INPUT_ERROR.AMOUNT_GREATER_THAN"]?"Amount greater than {0}":null
          value = item.max;
        }
      }

      if(onBlur){
        let newValue, validRefValues = this.context.validRefValues || {};
        let {profile} = this.context.store.getState().client;
        validRefValues.optionsMap = this.context.optionsMap;
        validRefValues.profile = profile;
        eval(`var execFunc = ${onBlur};newValue = execFunc(item, value, this.state.changedValues, this.props.rootValues, validRefValues)`);
        if(newValue){
          value = newValue;
        }
      }

      if (_.isFunction(this.props.handleBlur)) {
        let _valid = this.props.handleBlur(value);
        if(_valid != this.state.valid){
          this.setState({valid: _valid});
        }
      } 
      /* this part is hidden [#34827: unable to hide keyboard when user enter only one digit] 
      else {
        this.handleChange(value)
      }*/
    } else if (item.type.toUpperCase() === 'TEXTAREA') {
      let defaultValueId = item.id + 'Default';
      if (item.resetDefaultValue) {
        if (value.trim().length === 0 && changedValues.hasOwnProperty(defaultValueId)) {
          value = changedValues[defaultValueId];
        }
      }

      if (_.isFunction(this.props.handleBlur)) {
        let _valid = this.props.handleBlur(item.id, value);
        if (_valid != this.state.valid){
          this.setState({valid: _valid});
        }
      } else {
        this.handleChange(value);
      }
    }
  }

  handleChange = (newValue, actionIndex, changedFieldId) => {
    var item = this.props.template;
    let {
      changedValues,
      rootValues
    } = this.props;

    if (_.isFunction(this.props.handleChange)) {
      this.props.handleChange(item.id, newValue);
    }
    else{
        if (this.state.changedValues) {
          this.state.changedValues[item.id] = newValue;
        } else {
          this.state.value = newValue;
        }
        setValueTrigger(item, changedValues, rootValues);                
    }
    this.forceUpdate();    
  }
}

EABInputComponent.propTypes = {
    template: PropTypes.object.isRequired,
    values: PropTypes.object,
    changedValues: PropTypes.object.isRequired,
    changeActionIndex: PropTypes.func,
    floatingTitleStyle: PropTypes.object,
    titleStyle: PropTypes.object,
    resetValueItems: PropTypes.object,
    style: PropTypes.object,
    id: PropTypes.string,
    value: PropTypes.any,
    disabled: PropTypes.bool,
    handleChange: PropTypes.func,
    handleBlur: PropTypes.func,
    requestChange: PropTypes.func,
    requireShowDiff: PropTypes.bool
}

EABInputComponent.contextTypes = {
  store: PropTypes.object,
  muiTheme: PropTypes.object,
  langMap: PropTypes.object,
  lang: PropTypes.string,
  optionsMap: PropTypes.object,
  rootTemplate: PropTypes.object,
  validRefValues: PropTypes.object
}

export default EABInputComponent;
