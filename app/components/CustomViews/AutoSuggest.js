
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import { MenuItem } from 'material-ui/Menu';
import { Divider } from 'material-ui';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import { withStyles, createStyleSheet } from 'material-ui/styles';
import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';
import * as _ from 'lodash';

function renderInput(inputProps) {
  // classes`, `template`, `floatingLabelStyle`, `floatingLabelShrinkStyle`, `errorText`, `getStyles`, `getFieldTitle`
  const { classes, autoFocus, template, lang, value, ref, onChange, onBlur, getStyles, getFieldTitle,floatingLabelStyle, floatingLabelShrinkStyle, errorText, ...other } = inputProps;
  const {placeholder, title, mandatory, small, disabled} = template;

  return (
    <div {...other}>
      <TextField
        key = {'autosuggest-textfield-key-' + template.id}
        id={'autosuggest-textfield-id-' + template.id}
        className = {styles.ColumnField}
        style={small ? undefined : getStyles().columnField }
        hintText= { placeholder?getLocalText(lang, placeholder): undefined }
        hintStyle= { getStyles().hintStyle }
        floatingLabelText = {title?getFieldTitle(mandatory, title):null}
        errorStyle = {getStyles().errorStyle}
        errorText = { errorText }
        inputStyle = {{marginTop: '4px !important'}}
        floatingLabelStyle = { getStyles().floatingLabelStyle }
        floatingLabelShrinkStyle = {getStyles().floatingShrinkStyle}
        underlineStyle = {{bottom:'22px'}}
        underlineDisabledStyle = { getStyles().disabledUnderline }
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        disabled={disabled}
      />
    </div>
  );
}

class AutoSuggest extends EABInputComponent {

  constructor(props){
    super(props);
    this.state = Object.assign( {}, this.state, {
      value: props.cValue || '',
      dataSource: props.dataSource,
      maxSearchResults: props.maxSearchResults,
      suggestions: this.getSuggestions(props.cValue, props.dataSource, props.maxSearchResults),
      suggestionLength: this.getFullSuggestionLength(props.cValue, props.dataSource),
      dataSourceCaseInsensitive: _.map(props.dataSource, obj =>{ return _.toLower(obj.title); }),
      dataSourceCaseID: _.map(props.dataSource, obj =>{ return _.toLower(obj.value); })
    });
  }

  componentWillReceiveProps(nextProps) {
    let newState = {};
    let nextSuggestion = [];

    if(!isEqual(this.state.value, nextProps.cValue)){
      newState.value = nextProps.cValue;
    }

    if(!isEqual(this.state.dataSource, nextProps.dataSource)){
      newState.dataSource = nextProps.dataSource;
      newState.dataSourceCaseInsensitive = _.map(nextProps.dataSource, obj =>{ return _.toLower(obj.title); });
      newState.dataSourceCaseID = _.map(nextProps.dataSource, obj =>{ return _.toLower(obj.value); });
    }

    if (!isEmpty(newState) || newState.value === '') {
      this.setState(newState);
    }
  }

  handleSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value, this.state.dataSource, this.state.maxSearchResults),
      suggestionLength: this.getFullSuggestionLength(value, this.state.dataSource)
    });
  };

  handleSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  handleBlur = (event) =>{
    // let {value, suggestIndex} = this.state;
    // let optionsCI = this.state.dataSourceCaseInsensitive;
    // let optionsID = this.state.dataSourceCaseID;
    // let optionIndex = (suggestIndex) ? _.findIndex(optionsID, function(id) { return id === suggestIndex; }) : _.indexOf(optionsCI, _.toLower(_.trim(value)));
    // console.log('suggestIndex: ', suggestIndex);
    // console.log('handleBlur: ', value);
    // if ( optionIndex > -1) {
    //   value = this.props.dataSource[optionIndex];
    //   this.handleChange(event, {newValue: value});
    // }
  }

  handleChange = (event, { newValue }) => {
    let template = this.props.template;
    let optionsCI = this.state.dataSourceCaseInsensitive;
    let optionsID = this.state.dataSourceCaseID;
    let suggestIndex = _.get(newValue, 'value');
    if (_.isObject(newValue)) {
      newValue = _.get(newValue, 'title');
    }
    let optionIndex;
    if  (suggestIndex) {
      optionIndex =_.findIndex(optionsID, function(id) { return id === suggestIndex; });
    } else if (_.filter(optionsCI, (title) => {return title === newValue;}).length === 1){
      optionIndex = _.findIndex(optionsCI, function(title) { return title === newValue; });
    }

    let suggestedValue = this.props.dataSource[optionIndex];

    if (suggestedValue && suggestedValue.title) {
      newValue = suggestedValue.title;
    } else if (suggestedValue){
      newValue = suggestedValue;
    }

    if (!template.max || template.max >= newValue.length){
      this.props.ComponenthandleChange(newValue);
      let dS = this.state.dataSource;
      let maxSearch = this.state.maxSearchResults;
      this.setState({
        suggestions: this.getSuggestions(newValue, dS, maxSearch),
        suggestionLength: this.getFullSuggestionLength(newValue, dS)
      });
    }
  };

  characterToPixel (Arr){
    let pixel = 8;
    _.forEach(Arr, obj => {
      if (obj.length * 8 > pixel) {
        pixel = obj.length * 8;
      }
    })

    return pixel;
  }

  renderSuggestionsContainer = (options)=> {
    const { containerProps, children } = options;
    let hasFooter = false;
    let defaultContainerWidth = 210;
    let containerWidth;
    let notFitInputFieldWIdth;
    if (children && children.props && children.props.items && _.isNumber(this.state.suggestionLength)
        && children.props.items.length > 0
    ) {
      containerWidth = this.characterToPixel(children.props.items);
    }

    if (_.get(children, 'props.items.length') < this.state.suggestionLength) {
        hasFooter = true;
    }

    if (containerWidth > defaultContainerWidth) {
        notFitInputFieldWIdth = true;
    }
    if (containerWidth < defaultContainerWidth) {
        notFitInputFieldWIdth = true;
        containerWidth = defaultContainerWidth;
    }
    containerWidth = containerWidth + 'px';
    return (
      <Paper {...containerProps} style={{margin: '-10px 0px', minWidth: (notFitInputFieldWIdth) ? containerWidth : '100%'}}>
        { (hasFooter) ?
          <div style={{maxHeight: '455px', overflowY: 'auto', overflowX: 'auto', marginTop: '-10px'}}>
            {children}
            <Divider />
            <div className={styles.AutoSuggestionFooter} style={{minWidth: (notFitInputFieldWIdth) ? containerWidth : '100%'}}>{`${children.props.items.length} out of ${this.state.suggestionLength} Records`}</div>
          </div>
        :
        <div style={{maxHeight: '405px', overflowY: 'auto', overflowX: 'hidden', marginTop: '-10px'}}>
            {children}
        </div>
        }
      </Paper>
    );
  }

  renderSuggestion = (suggestion, { query, isHighlighted }) =>{
    let suggestionTitle = _.get(suggestion, 'title');
    const matches = match(suggestionTitle, query);
    const parts = parse(suggestionTitle, matches);

    // CR63 FSD part3.1.1
    let countryName = _.get(suggestion, "countryName");
    let tab = 0;
    //

    return (
      <MenuItem selected={isHighlighted}>
        <div className={styles.suggestWrap}>
          {parts.map((part, index) => {
            tab += part.text.replace("-", "").length;
            return part.highlight
              ? <span key={index} className={styles.HighlightedParts}>
                  {part.text}
                </span>
              : <strong key={index} className={styles.NotHighlightedParts}>
                  {part.text}
                </strong>;
          })}
          {
            // CR63 FSD part3.1.1
            ( countryName ) ? <span>{this.getspace(tab) + countryName} </span> : ""
          }
        </div>
      </MenuItem>
    );
  }

  // CR63 FSD part3.1.1
  getspace = (tab) => {
    let space = ["\t","\t","\t","\t","\t"];
    if(tab == 5) {
      space = ["\t","\t"];
    } else {
      while(tab) {
        space.pop();
        tab--;
      }
    }
    return space.join("");
  }

  shouldRenderSuggestions = () => {
    // input is focused consult this function to render suggestion or not
    return true;
  }

  getSuggestionValue = (suggestion)=> {
    return _.get(suggestion, 'value', '');
  }

  getFullSuggestionLength = (value, suggestions) => {
    if (_.isUndefined(value) && _.get(value, 'length') === 0 || isEmpty(value)){
      return suggestions.length;
    } else {
      return suggestions.filter(suggestion => { return _.get(suggestion, 'title', '').toLowerCase().indexOf(value) > -1}).length;
    }
  }

  getSuggestions = (value, suggestions, maxSearchResults) => {
    const inputValue = _.trim(value).toLowerCase();
    const inputLength = inputValue.length;
    let count = 0;

    let suggestionResult =[];

     if (inputLength === 0 || isEmpty(inputValue)){
      suggestionResult = suggestions.filter(suggestion => {
        const keep =
          count < maxSearchResults;

        if (keep) {
          count += 1;
        }

        return keep;
      })
     }else {
      suggestionResult = suggestions.filter(suggestion => {
          const keep =
            count < maxSearchResults && _.get(suggestion, 'title', '').toLowerCase().indexOf(inputValue) > -1;

          if (keep) {
            count += 1;
          }

          return keep;
        });
     }

    return suggestionResult;
  }

  render() {
    const { classes, template, floatingLabelStyle, floatingLabelShrinkStyle,errorText } = this.props;
    const { lang } = this.context;
    let { value } = this.state;

    return (
      <Autosuggest
        key={'autosuggest-key-' + template.id}
        id={'autosuggest-id-' + template.id}
        theme={styles}
        renderInputComponent={renderInput}
        suggestions={this.state.suggestions}
        onSuggestionsFetchRequested={this.handleSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
        renderSuggestionsContainer={this.renderSuggestionsContainer}
        getSuggestionValue={this.getSuggestionValue}
        renderSuggestion={this.renderSuggestion}
        shouldRenderSuggestions={this.shouldRenderSuggestions}
        focusInputOnSuggestionClick={false}
        inputProps={{
          classes,
          template,
          lang,
          floatingLabelStyle,
          floatingLabelShrinkStyle,
          errorText,
          value: value,
          onChange: this.handleChange,
          onBlur: this.handleBlur,
          getStyles: this.getStyles,
          getFieldTitle: this.getFieldTitle
        }}
      />
    );
  }
}

AutoSuggest.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default AutoSuggest;
