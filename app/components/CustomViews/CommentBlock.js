import  React from 'react';

import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';

import CommentItem from './CommentItem';
import forEach from 'lodash/forEach';

class CommentBlock extends EABInputComponent {
  constructor(props) {
    super(props);
  }

  genCommentItems(value, disabled, max){
    let items = [];
    let {app} = this.context.store.getState();
    let enableBtn = true;
    const tableStyle = (window.isMobile.apple.phone) ? styles.MobileCommentBoxBorder : styles.CommentBoxBorder;
    forEach(value, (obj, index) => {
      (app.agentProfile.agentCode === obj.authorId) ? enableBtn = true : enableBtn = false;
      items.push(
        <tr className={tableStyle} key={'key-comment-block-' + index}>
          <td>
            <CommentItem
              values={obj}
              commentIndex={index}
              enableBtn={enableBtn}
              disabled={disabled}
              authorPid={obj.authorPid}
              max={max}
            />
          </td>
        </tr>
      );
    });

    return (
      <table className={tableStyle  + ' ' + styles.width100}>
        {items}
      </table>
    );
  }

  render() {
    let {
      template
    } = this.props;


    let {
      id,
      disabled,
      max
    } = template;

    let cValue = this.getValue('');
    return (
      <div style={(window.isMobile.apple.phone) ? {marginBottom: '24px'} : {margin: '24px 0px'}}>
        {this.genCommentItems(cValue, disabled, max)}
      </div>
    );
  }
}

export default CommentBlock;
