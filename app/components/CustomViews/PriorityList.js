import React from 'react';
import {Paper} from 'material-ui';

import EABTextField from './TextField.js';
import ImageView from './ImageView';
import SortableList from './SortableList.js';
import EABInputComponent from './EABInputComponent';
import HeaderTitle from './HeaderTitle';
import ToolTips from './ToolTips';
import {getIcon} from '../Icons/index';

import styles from '../Common.scss';

import * as _ from 'lodash';
import * as _c from '../../utils/client'
import * as _n from '../../utils/needs';

class PriorityList extends EABInputComponent{
  constructor(props, context){
    super(props);
    let {template,rootValues} = props;
    let {needs, client} = context.store.getState();
    let {pda, fna, template: nTemplate, fe} = needs;
    let {fnaForm} = nTemplate;
    let {profile, dependantProfiles} = client;
    let optionObj = _n.getPriorityOptions(fnaForm, template, profile, dependantProfiles, pda, rootValues) || [];
    let options = optionObj[0] || [];
    let sfTemplate = _.cloneDeep(template);
    if (sfTemplate){
      sfTemplate.id = 'sfAspects';
      sfTemplate.subType = 'shortfall';
    }
      
    let sfOptions = _n.getPriorityOptions(fnaForm, sfTemplate, profile, dependantProfiles, pda, fna)[0];
    let listNoforSurplus = (sfOptions) ? sfOptions.length : 0;
    //Validate the Priority List
    this.props.validate();
    this.state=Object.assign({}, this.state, {
      docId: props.docId || {},
      options,
      lengthOfAllPriorities: optionObj[1] || 0,
      listNoforSurplus
    })
  }


  componentWillReceiveProps(nextProps){
    this.setState({
      docId: nextProps.docId
    });
  }

  render() {
    let {lang, muiTheme, needs} = this.context;
    let {docId, options, changedValues} = this.state;
    let {template} = this.props;
    let {id, hints, title, desc, mandatory, subType, changeStyleLvl, type} = template;

    let alertColor, posColor;
    alertColor = muiTheme.palette.errorColor;
    posColor = muiTheme.palette.primary5Color;

    let cValue = this.getValue([]);
    //options = this._getOptionsByReference(template);
    let listNumberColor = muiTheme.palette.accent3Color;
    let textColor = muiTheme.palette.textColor;
    let isLarge;
   //let isLarge = (changedValues['sfAspects']? changedValues['sfAspects'].length: 0) + (changedValues['spAspects']? changedValues['spAspects'].length:0) > 10?false: true;
    if (options)
      isLarge = this.state.lengthOfAllPriorities < changeStyleLvl;

    let items = [];
    let laDivStyle;
    let cropLength;
    if ( window.matchMedia('(min-width: 1700px)').matches){
      cropLength = 22;
    } else if ( window.matchMedia('(min-width: 1500px)').matches){
      cropLength = 19;
    } else if ( window.matchMedia('(min-width: 1300px)').matches) {
      cropLength = 16;
    } else if ( window.matchMedia('(min-width: 1000px)').matches) {
      cropLength = 13;
    } else {
      cropLength = 8;
    }
  
    _.forEach(options, (option, i, options)=>{
      
      let {id: oid, timeHorizon, totShortfall, image, name, aTitle, cid, aid, nameArray, annRetireIncome} = option;
      let isHcProtection = aid === "hcProtection";
      let isCrop = name && name.length > cropLength && !isLarge;
      let hospitalisationToolTipHints = '';
      laDivStyle = styles.TextTitle + ' ' + styles.alignLeft + ' ' + styles.bold;
      if (isCrop && isHcProtection) {
        _.forEach(nameArray, (optName, index) => {
          hospitalisationToolTipHints += `${index + 1}. ${optName}<br/>`;
        });
      }
      items.push(
          <div key={`prioity-list-${id}-${i}`} className={"drag-area"}  style={{display: 'flex', minHeight: '0px', alignItems: 'center', minWidth: (isLarge) ? '720px' :'300px'}}>
          <div style={{width: '15px', padding:'15px', color: listNumberColor, fontSize: '24px', fontWeight: '600', marginBottom: '16px'}}>
             {(_.toLower(subType)=='shortfall') ? i + 1 : this.state.listNoforSurplus + 1 + i}
          </div>
          
          <Paper style={Object.assign({minHeight: (isLarge) ? '100px' : '130px', backgroundColor: '#f7f7f7'}, this.getStyles().paperStyle)}>
            {isLarge ? <ImageView key={`prioity-image-${oid}`} style={{width: '120px', height: '80px'}} docId={docId} attId={image}/>: undefined}
            <div style={{display: 'flex', minHeight: '0px', flexDirection: (isLarge) ? 'row' : 'column', alignItems: (isLarge) ? 'center' : '', justifyContent: 'space-between', width: 'calc(100% - 96px)'}}>
              <div>
                <div style={{display: 'flex', minHeight: '0px', alignItems: 'center'}}>
                  <div className={laDivStyle} style={{flex: 1}}>
                    {isCrop && isHcProtection ? name.substring(0, cropLength - 3) + '...' : name}
                  </div>
                  {
                    isCrop && isHcProtection ? (
                      <div style={{flexBasis: 109}}>
                        <ToolTips hints={hospitalisationToolTipHints} iconType={'viewApplicant'} containerStyle={{alignSelf: 'center', display:'inline-block'}}/>
                      </div>
                    ) : null
                  }
                </div>
                <div className={styles.TextTitle + ' ' + styles.alignLeft + ' ' + styles.bold}>
                  {getLocalText(lang, aTitle)}
                </div>
              </div>
              <div style={{minWidth: (_.isNumber(timeHorizon) || _.isNumber(totShortfall) || _.isNumber(annRetireIncome) && isLarge) ? '300px' : undefined, textAlign: isLarge ? 'right' : 'left'}}>
                {_.isNumber(timeHorizon)?
                  <div className={styles.TextTitle + ' ' +  styles.bold}>Time Horizon: {timeHorizon} years</div>: null
                }
                {_.isNumber(totShortfall)?
                  <div className={styles.TextTitle + ' ' +  styles.bold}>{_.toLower(subType)=='shortfall'?'Shortfall: ': 'Surplus: '}<span style={{color: _.toLower(subType)=='shortfall'? alertColor: posColor}}>{getCurrency(Math.abs(totShortfall), '$', 2)}</span></div>: null
                }
                {_.isNumber(annRetireIncome) && _.toLower(subType)=='shortfall' ?
                  <div className={styles.TextTitle + ' ' +  styles.bold}>{'Annual Retirement Income: '}<span style={{color: alertColor}}>{getCurrency(Math.abs(annRetireIncome), '$', 2)}</span></div>: null
                }
              </div>
            </div>
            <div className={"drag-element"} style={{padding: '0px 24px'}}>{getIcon('order', muiTheme.palette.primary1Color)}</div>
          </Paper>
        </div>
      );
    })

    let handleSort = (e) =>{
      e.preventDefault();
      this.requestChange(_n.getPriorityValues(this.state.options));
    }

    return (
      <div>
        <div style={{width: 'calc(100% - 12px)'}}>
          <div className={styles.TitleContentContainer}>
            {(title) ? <HeaderTitle title={title} mandatory={mandatory} hints={hints}/> : undefined}
            {(desc && options && options.length > 0) ? <div className={styles.PriorityTextTitle + ' ' + styles.bold}>{getLocalText(lang, desc)}</div> : undefined}
          </div>
          <SortableList
            items={items}
            changedValues={options}
            handleSort={handleSort}
            isLargeStyle={isLarge}
          />
        </div>
      </div>
    );
  }

}

export default PriorityList;