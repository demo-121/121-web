import React from 'react';
import {Divider} from 'material-ui';
import * as _ from 'lodash';
import EABComponent from '../Component';
import ConfigConstants from '../../constants/ConfigConstants';

import {getIcon} from '../Icons/index';
import styles from '../Common.scss';

class DisplayCardView extends EABComponent {

  getValue=(id, value, type, format, items, options, condition)=>{
    let {optionsMap, langMap, lang} = this.context;

    type = type.toUpperCase();
    if(type==="CONCATENATE"){
      if(!items)
        return "-";

      let result = "";
      let hasEmpty = false;
      _.forEach(items, item=>{
        let {id: _id, type: _type, format: _format, items: _items, options: _options, trigger: _trigger, conditon: _condition} = item;
        let itemValue = value[_id]?this.getValue(_id, value, _type, _format, _items, _options, _condition) : "";
        if(condition === 'all' && !itemValue){
          hasEmpty = true;
          return;
        }
        else if(condition === 'skip' && !itemValue){
          return ;
        }
        if(_trigger){
          if(_trigger.type === 'replace'){
            result.replace(
              items.find(triggerItem=>triggerItem.id === _trigger.id).value,
              itemValue
            );
          }
        }else{
          result += itemValue
        }
      })
      return result && !hasEmpty? result : condition==='skip'?null: "-";
    }else if(type == "MULTILINE"){
      if(!items){
        return "-";
      }
      
      let result = [];
      items.forEach((item, i)=>{
        let {id: _id, type: _type, format: _format, items: _items, options: _options, condition: _condition} = item;
        let _value = this.getValue(_id, value, _type, _format, _items, _options, condition);
        if (!(condition==='skip' && !_value)){
          result.push(
            <div key={`display-value-${id}-${i}`}>{_value}</div>
          )
        }
      })
      return result.length?result: '-';
    }


    let resultStr = "";
    if(isEmpty(value[id])){
      if(condition==='skip'){
        return null;
      }
      resultStr = "-";
    }
    else if(type === 'CURRENCY'){
      resultStr = getCurrency(value[id], '$', 0);
    }
    else if(type === "TEXT"){
      resultStr = value[id];
    }
    else if(type === "PICKER"){
      if(_.isString(options)){
        options = _.at(optionsMap, `${options}.options`)[0] || [];
      }
      resultStr = _.cloneDeep(value[id]).split(",");
      _.forEach(options, opt=>{
        let i = resultStr.indexOf(opt.value);
        if(i>-1){
          if(opt.isOther){
            resultStr[i] = value[id+'Other'];
          }
          else{
            resultStr[i] = getLocalText(lang, opt.title);
          }
        }
      })
      resultStr = resultStr.join(", ");
    }
    else if(type === "DATE"){
      let dateValue = new Date(value[id]);
      resultStr = dateValue.format(ConfigConstants.DateFormat);
    }
    

    if(id === 'allowance' && value[id] === 0) {
      resultStr = '0';
    }
    
    return format?format.replace("%s", resultStr): resultStr;
  }

  renderItems = () =>{
    let {muiTheme, langMap,  lang, validRefValues} = this.context;
    let {template={}, value} = this.props;
    let {fields=[]} = template;
    
    let items = [];
    let iconColor = muiTheme.palette.primary1Color;

    fields.forEach((field, index)=>{
      let {title, items: fItems=[]} = field;
      if(_.isString(title)){
        title = getLocalizedText(langMap, title);
      }
      else if(!isEmpty(title)){
        title = getLocalText(lang, title);
      }

      let header = title? <div className={styles.title} >{title}</div>: null;
      let subItems = [];
      fItems.forEach((subItem, sIndex)=>{
        if (!showCondition(validRefValues, subItem, value, value, value, [])) {
          return;
        }

        let {id: sId, type: sType='', title: sTitle, items: sItems, icon: sIcon, options: sOptions, fullWidth: sFullWidth, format: sFormat, condition:sCondition} = subItem;
        sType=_.toUpper(sType);
        let cValue = this.getValue(sId, value, sType, sFormat, sItems, sOptions, sCondition);
        if(_.isString(sTitle)){
          sTitle = getLocalizedText(langMap, sTitle);
        }
        else if(!isEmpty(title)){
          sTitle = getLocalText(lang, sTitle);
        }

        let _sIcon = sIcon?<div className={styles.SmallIcon}>{getIcon(sIcon, iconColor)}</div>:null;
        subItems.push(
          <div key={`display-subitems-${index}-${sIndex}`} className={(sFullWidth) ? styles.fullWidth + ' ' + styles.items : styles.halfWidth + ' ' + styles.items}>
            {_sIcon}
            <div>
              <div className={styles.subTitle}>{sTitle}</div>
              <div className={styles.subTitleValue}>{cValue}</div>
            </div>
          </div>
        )
      })

      if(!isEmpty(subItems)){

        //add divider
        if(!isEmpty(items)){
          items.push(
            <div key={`display-divider-${index}`} className={styles.Divider}/>
          )
        }

        items.push(
          <div key={`diplay-section-${index}`} className={styles.CardSection}>
            {header}
            {subItems}
          </div>
        )
      }
    })
    return items;
  }

  render(){
	  let {muiTheme, langMap, lang} = this.context;
    let {style, title} = this.props;

    let items = this.renderItems();
    
    if(_.isString(title)){
      title = <div>{title}</div>
    }

    let header = title?(
      <div key="display-card-header" className={styles.TitleContainer}>
        {title}
      </div>
    ):null;

    return (
      <div className={styles.Card} style={style}>
        <div className={styles.CardPadding}>
          {header}
          {items}
        </div>
      </div>
	  )
  }
}
export default DisplayCardView;
