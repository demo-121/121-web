import React from 'react';
import EABComponent from '../Component';
import styles from '../Common.scss';

class Title extends EABComponent {

    handleTitle = (title, template={}) =>{
        if(title.indexOf("<") > -1 && title.indexOf(">") > -1){
            let _arryToReplce = title.split(/[<>]/);
            let strToDisplay = [];
            let subIndex = 1;
            let hover = template.hover;
            let hoverIndex = 0;
            let spanStyle={};
            let lastSpanStyle = {};
            if (template.multiLineTitle){
                spanStyle = {display: 'block', lineHeight: '1.5em'};
                lastSpanStyle = {display: 'inline-block', lineHeight: '1.5em'};
            }
            for(let sub = 0; sub < _arryToReplce.length; sub ++){
                let str = _arryToReplce[sub];
                if(sub == subIndex){
                    subIndex = subIndex + 2;
                    let _hover = null;
                    if(hover && hover[hoverIndex]){
                        let _sty = {
                            position:'absolute',
                            top:'20px',
                            left:'-150px',
                            fontSize: '12px',
                            padding: '5px',
                            fontWeight: 200,
                            backgroundColor: 'rgba(0,0,0,0.7)',
                            lineHeight: '15px',
                            color: 'white',
                            borderRadius: '5px'
                        }
                        if(hover[hoverIndex]['style']){
                            _sty = Object.assign({}, hover[hoverIndex]['style'], _sty);
                        }
                        _hover = <div className={styles.tHover} style={_sty}>{hover[hoverIndex]['msg']}</div>
                    }
                    str = <u className={styles.tUnderLine}>{_hover}{str}</u>
                } else if (sub + 1 === _arryToReplce.length) {
                    str = <span style={lastSpanStyle}>{str}</span>
                } else {
                    str = <span style={spanStyle}>{str}</span>
                }
                strToDisplay.push(str);
            }
            return strToDisplay;
        } else {
            return title;
        }
    }
    render(){
        let { lang, langMap, muiTheme } = this.context;
        let { mandatory, title="", style, customHeaderStyle, defaultStyle, template, isCustom , labelStyle} = this.props;

        let titleStyle = defaultStyle ? {} : (customHeaderStyle ? customHeaderStyle : styles.headerTitleStyle);
        
        title = window.getLocalizedText(langMap, window.getLocalText(lang, title));

        if (isCustom) {
            return (
                <span style={{display: 'flex', minHeight: '0px', alignItems: 'center'}}>
                    <span key="t" className={titleStyle} style={labelStyle} dangerouslySetInnerHTML={{__html:title}}/>{mandatory && title ? <span key="s" className={styles.HeadermandatoryStar}>&nbsp;*</span>: null}
                </span>            
            )
        }

        title = this.handleTitle(title, template);

        if(_.get(template, 'hashAfterMandatory')){
        return (
                <span style={{display: 'flex', minHeight: '0px', alignItems: 'center'}}>
                  <span key="t" className={titleStyle} >{title}{mandatory && title ? <span key="s" className={styles.HeadermandatoryStar}>&nbsp;*<span style={{color: '#00008f'}}>#</span></span>: null}</span>
               </span>            
                )
        }else{
            return (
             <span style={{display: 'flex', minHeight: '0px', alignItems: 'center'}}>
               <span key="t" className={titleStyle} >{title}{mandatory && title ? <span key="s" className={styles.HeadermandatoryStar}>&nbsp;*</span>: null}</span>
            </span>            
             )
         }
    }
}

export default Title;