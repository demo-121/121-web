import React, { Component, PropTypes}  from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import {getIcon} from '../Icons/index';
import _getOr from 'lodash/fp/getOr';

import EABInputComponent from './EABInputComponent';
import styles from '../Common.scss';
import forEach from 'lodash/forEach';
import {viewJFWFiles} from '../../actions/approval';

import SignDocViewer from './SignDocViewer';
class DynColFilePreview extends EABInputComponent{

    constructor(props){
        super(props);
        this.state = Object.assign({},this.state,{
            openPreviewDialog: false,
            previewObj: ''
        });
    }

    genPreviewFile = () =>{
        let {changedValues, template} = this.props;
        let items =[];
        let fileName;
        let filesArr = changedValues[template.id];
        forEach(filesArr, (obj, index) =>{
            fileName = obj.fileName;
            items.push(
                <div>
                    {this.gentPreviewIcon(obj, fileName, index)}
                    <div style={{paddingLeft: '12px', display: 'inline'}}>{fileName}</div>
                </div>
            )
        });
        return items;
    }

    previewAttachment = (obj) => {

        viewJFWFiles(this.context, this.state.changedValues.docId, obj.attId, (resp) => {
            if (resp.data || resp.token) {
                let {signDocConfig, token} = resp;
                let trimPrefix = 'data'+obj.type+'base64';
                let objPrefix = 'data:' + obj.type + ';base64,';
                let data;

                if (resp.data && resp.data.indexOf(trimPrefix) > -1 ) {
                    data = resp.data.substr(trimPrefix.length, resp.data.length);
                } else if (resp.data) {
                    data = resp.data;
                }

                let embedObj;
                if (obj.type.indexOf('image') > -1) {
                  embedObj = <img src={objPrefix + data} style={{maxWidth: '100%', height: 'auto'}}/>
                } else {
                  embedObj = <embed src={window.genAttPath(token)} type={obj.type} style={{width: '700px', height: '500px'}}/>;
                }

                let iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
                let android = /android/i.test(navigator.userAgent) && !window.MSStream;
                let previewObj;
                if ((iOS || android) && !window.isEmpty(signDocConfig) && obj.type.indexOf('pdf') > -1) {
                    previewObj = (<div style={{ display: 'flex', minHeight: '0px', flexDirection: 'column', flexGrow: 1, width: '700px', height: '500px' }} >
                    <SignDocViewer
                        id={signDocConfig.docid}
                        visible={true}
                        signed
                        eSignBorder={false}
                        postUrl={signDocConfig.postUrl}
                        pdfUrl={signDocConfig.attUrl + '/' + token}
                        auth={signDocConfig.auth}
                        docts={signDocConfig.docts}
                        dmsId={signDocConfig.dmsId}
                        signFieldList={[]}
                        resultUrl={signDocConfig.resultUrl}
                        onHandleSign={(resultJSON) => {
                            // this.handleSign(resultJSON, i);
                        }}
                    />
                    </div>)
                } else {
                    previewObj = <object style={{width: '700px', height: '500px'}}>
                                {embedObj}
                            </object>
                }
                
                this.setState({
                    openPreviewDialog: true,
                    previewObj: previewObj
                })
            }
        })
    }

    gentPreviewIcon = (obj, fileName, index) => {
        let {muiTheme} = this.context;
        let objType = _getOr('', 'type', obj);
        if (objType.indexOf('image') > -1) {
            let imgIcon = getIcon('image', muiTheme.palette.primary2Color);
            return (<IconButton
                        key={`pdf-${fileName[0]}-${index}`}
                        id={`pdf-${fileName[0]}-${index}`}
                        onTouchTap={() => {this.previewAttachment(obj)}}
                    >
                        {imgIcon}
                    </IconButton>);
        } else if (objType.indexOf('pdf') > -1) {
            let pdfIcon = getIcon('pdf', muiTheme.palette.primary2Color);
            return (<IconButton
                        key={`pdf-${fileName[0]}-${index}`}
                        id={`pdf-${fileName[0]}-${index}`}
                        onTouchTap={() => {this.previewAttachment(obj)}}
                    >
                        {pdfIcon}
                    </IconButton>);
        } else {
            return;
        }
    }

    render(){
        var {
            template,
            changedValues,
            values,
            allFilesValues,
            ...others
        } = this.props;
        var {
            muiTheme
        } = this.context;

        let { openPreviewDialog, previewObj } = this.state;

        let applicationId = values.id;
        let title, subTitle;
        if (template && template.title){
            title = template.title;
        }

        if (template && template.subTitle){
            subTitle = template.subTitle;
        }

        const actions = [
            <FlatButton
                label="Close"
                primary={true}
                onClick={()=>{
                    this.setState({
                        openPreviewDialog: false
                    });
                }}
            />,
        ];

        return (
            <div style={{paddingTop: '12px'}}>
                <div>{title}{template.mandatory && title? <span key="s" className={styles.HeadermandatoryStar}>&nbsp;*</span>: null}</div>
                {this.genPreviewFile()}
                <Dialog
                  className={styles.FixWrongDialogPadding}
                    actions={actions}
                    modal={true}
                    open={openPreviewDialog}
                >
                    {previewObj}
                </Dialog>
            </div>
        )
    }
}

export default DynColFilePreview;

