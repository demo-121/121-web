import React from 'react'
import {Checkbox} from 'material-ui';
import * as _ from 'lodash';
import EABInputComponent from './EABInputComponent';

import HeaderTitle from './HeaderTitle';
import ErrorMessage from './ErrorMessage';
import styles from '../Common.scss';


const checkBoxLabelStyle = {
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap',
  width: 'calc(100% - 24px)',
  fontSize: '16px'
}

class CheckboxGroup extends EABInputComponent{
  getCheckBoxValues = (cbValues) =>{
    return cbValues.filter(cbValue=>cbValue!='').join(',');
  }

  render() {
    let {lang, validRefValues} = this.context;
    let {changedValues} = this.state;
    let {template, rootValues, values, handleChange, disabled, mode, resetValueItems} = this.props;
    let {id, options, title, mandatory, value, subType, placeholder, presentation, hints} = template;

    let valueArr = [];
    let cbGroup = [];

    let cValue = this.getValue("");
    if (_.isString(cValue)) {
      valueArr = cValue.split(",");
    }
    if(valueArr.length){
      for(let i=valueArr.length; i>=0; i--){
        let v = valueArr[i];
        if(v!='*' && _.findIndex(options, opt=>opt.value==v)==-1){
          valueArr.splice(i, 1);
        }
      }
    }


    if (subType && 'WITH_ALL' == subType.toUpperCase()) {
      cbGroup.push(
        <Checkbox
          key={'ALL'}
          className={"CheckBox"}
          style={{width: null, display:'inline-block'}}
          label={getLocalizedText("OPTION.ALL")}
          labelStyle={checkBoxLabelStyle}
          checked={valueArr.indexOf('*') > -1}
          onCheck={
            ()=>{
              // var cbValues = cValue.split(",");
              valueArr = valueArr.indexOf('*')>-1?[]:['*']
              this.requestChange(this.getCheckBoxValues(valueArr));
            }
          }
        />
      );
    }

    options.forEach((option, index)=>{
      if (optionSkipTrigger(template, option, changedValues, resetValueItems))
        return;

      let {value: optValue, title: optTitle} = option;
      cbGroup.push(
        <Checkbox
          key={optValue}
          className={"CheckBox"}
          style={{width: null, display:'inline-block', paddingRight: '24px'}}
          label={getLocalText(lang, optTitle)}
          disabled={disabled || template.disabled}
          labelStyle={checkBoxLabelStyle}
          checked={valueArr.indexOf(optValue) > -1}
          onCheck={
            ()=>{
              let valueIndex = valueArr.indexOf(optValue);
              if (valueIndex > -1) {
                valueArr.splice(valueIndex, 1);
              } else {
                valueArr.push(optValue);
              }

              let all = valueArr.indexOf('*')
              if (all > -1) {
                valueArr.splice(all, 1);
              }
              this.requestChange(this.getCheckBoxValues(valueArr));
            }
          }
        />
      );
    })

    if (isEmpty(cbGroup)) {
      cbGroup.push(<label key={"placeholder"} className="CheckBox">{placeholder || "No Option"}</label>)
    }
    let errorMsg = this.getErrMsg();
    let customClass;
    if (template.align)
      customClass = styles.RbheaderTitleAlignLeft;

    if (template.mode)
      mode = template.mode;
    
    return (
      <div className={"CheckboxGroup"+ (presentation?" "+presentation:"") + this.checkDiff()}>
        {(title) ? <HeaderTitle title={title} mandatory={mandatory} hints={hints} customStyleClass={customClass}/> : undefined}
        <div className={styles.ErrorMsgContainer}>{
          _.isString(errorMsg) ?
            <ErrorMessage template={template} message={errorMsg} style={{textAlign: template.align || 'center'}}/>: null
        }</div>
        <div style={{display: 'flex', minHeight: '0px', flexDirection: mode=='P'?'column':'row'}}>
        {cbGroup}
        </div>
      </div>
    );
  }
}

export default CheckboxGroup;
