import React from 'react';

import EABInputComponent from './EABInputComponent';
import {getIcon} from '../Icons/index';

import HeaderTitle from './HeaderTitle';
import ErrorMessage from './ErrorMessage';
import ValueDialog from './ValueDialog';
import styles from '../Common.scss';

class EABStepper extends EABInputComponent{
  _handleChange = (type) =>{
    let {interval=1, min=0, max=100, defaultValue=0, decimalNeeded=0} = this.props.template;

    let cValue = this.getValue((defaultValue));
    cValue = Number(cValue || 0);
    interval = Number(interval);
    if(type==='decrease'){
      if(cValue-Number(interval)<min){
        return;
      }else{
        this.requestChange((Number(cValue - interval)).toFixed(decimalNeeded))
      }
    }else{
      if(cValue+interval>max){
        return;
      }else{
        this.requestChange((Number(cValue + interval)).toFixed(decimalNeeded))
      }
    }
  }

  componentDidMount() {
    var interval;
    var self = this;
    var time = 500;
    var count = 0;
    var resetInterval = function(e){
      clearInterval(interval);
      time = 500;
      count = 0;
    }

    let incrementInterval = function(_time, type){
      return setInterval(function() {
        self._handleChange(type);
        count++;
        if(count%5==0){
          if(_time-100>0){
            _time=_time-100;
            clearInterval(interval);
            interval = incrementInterval(_time, type);
          }
        }
      },_time);
    }
    document.getElementById('addBtn').addEventListener('mousedown', function(e){
      interval = incrementInterval(time, 'add');
    });
    document.getElementById('addBtn').addEventListener('mouseup',resetInterval);
    document.getElementById('addBtn').addEventListener('mouseout',resetInterval);

    document.getElementById('decreaseBtn').addEventListener('mousedown', function(e){
      interval = incrementInterval(time, 'decrease');
    });
    document.getElementById('decreaseBtn').addEventListener('mouseup',resetInterval);
    document.getElementById('decreaseBtn').addEventListener('mouseout',resetInterval);
  }

  render() {
    let {lang, validRefValues, muiTheme } = this.context;
    let {changedValues} = this.state;
    let {template, rootValues, values} = this.props;
    let {id, title, hints, mandatory, defaultValue="", min=0, disabled} = template;

    let cValue = disabled && !changedValues[id]? "": this.getValue(defaultValue);
    let errorMsg = this.getErrMsg();
    let iconColor = muiTheme.palette.secondary1Color;

    return (
      <div className={styles.FlexColumnNoWrapContainer}>
        <HeaderTitle title={title} mandatory={mandatory} hints={hints} customStyleClass={this.props.headerContainerStyle} customHeaderStyle={this.props.headerStyle}/>
        <div className={styles.ErrorMsgContainer}>{_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg}/>: null}</div>
        <div className={styles.StepperContainer}>
          <div id="decreaseBtn" onTouchTap={()=>{this._handleChange('decrease')}}>{getIcon('remove', '#FFFFFF')}</div>
          <div id="numberBtn" onClick={disabled ? null : ()=>{this.dialog.open()}}>{cValue && !isNaN(Number(cValue))?cValue: "-"}</div>
          <div id="addBtn" onTouchTap={()=>{this._handleChange('add')}}>{getIcon('add', '#FFFFFF')}</div>
        </div>
        <ValueDialog ref={(ref)=>{this.dialog=ref;}} template={template} value={cValue} handleChange={(value)=>{this.requestChange(value);}}/>
      </div>
    )
  }
}

export default EABStepper;