import  React from 'react';

import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';
import EABPickerField from './PickerField';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
// import {formatDateWithTime} from '../../../common/DateUtils';
import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';
import {updateApprovalCaseById, searchApprovalCases, searchWorkbenchCases} from '../../actions/approval';
import {USING_WORKBENCH_TEMPLATE_USED, USING_PENDING_APPROVAL_TEMPLATE} from '../Pages/Pos/Approval/main';

import { pendingDocNotification, saveComment, FAADMIN, identifyApproveLevel } from '../../actions/approval';
import _isEmpty from 'lodash/fp/isEmpty';

import * as _v from '../../utils/Validation';

class CommentReason extends EABInputComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
        comment: '',
        errorMsg: '',
        id: ''
    });
  }

  componentWillReceiveProps(nextProps) {
      let newState = {};
      let updateFlag = false;

      if (!window.isEqual(this.state.comment, nextProps.comment) && !window.isEqual(this.state.id, nextProps.changedValues.approvalCaseId)){
          newState.comment = nextProps.comment;
          if (_isEmpty(nextProps.comment)) {
            newState.errorMsg = '';
          }
          updateFlag = true;
      }

      if (!window.isEqual(this.state.id, nextProps.changedValues.approvalCaseId)){
          newState.id = nextProps.changedValues.approvalCaseId;
          updateFlag = true;
      }

      if (updateFlag) {
          this.setState(newState);
      }
  }

  commentValidate = (value) =>{
    let {langMap, optionsMap, lang} = this.context;
    let {template} = this.props;
    let {comment} = this.state;
    let error = {};
    let changedValues = {}
    changedValues.comment = value;
    let max = get(template, 'items[1].max');
    let temptemplate = {id: 'comment', type: 'textarea', max};
    _v.exec(temptemplate, optionsMap, langMap, changedValues, changedValues, changedValues, error);

    let errorMsg = _v.getErrorMsg(temptemplate, error[temptemplate.id].code, lang);
    value = value.substr(0, max);
    this.setState({
      errorMsg,
      comment: value
    });
  }

  saveComment = () => {
    let {approval, app} = this.context.store.getState();
    let tempAppCase = cloneDeep(approval.approvalCase);
    if (_isEmpty(this.state.comment)) {
        return;
    }
    saveComment(this.context, tempAppCase.approvalCaseId, this.state.comment, () => {
        this.setState({comment: '', errorMsg: ''});
        if (approval.currentTemplate === USING_WORKBENCH_TEMPLATE_USED) {
          searchWorkbenchCases(this.context, approval.workbenchFilter, () => {});
        } else {
          searchApprovalCases(this.context, approval.approvalFilter, ()=>{});
        }
    });
  }

  handlePickerChange =(id, value) => {
    let {approval, app} = this.context.store.getState();
    let {agentProfile} = app;
    let tempAppCase = cloneDeep(approval.approvalCase);
    tempAppCase.approvalStatus = value;
    tempAppCase.caseLockedManagerCodebyStatus = agentProfile.agentCode;
    
    new Promise(resolve => {
        updateApprovalCaseById(this.context, tempAppCase.approvalCaseId, tempAppCase, () => {
            resolve();
        });
    }).then(()=>{
      if (approval.currentTemplate === USING_WORKBENCH_TEMPLATE_USED) {
        searchWorkbenchCases(this.context, approval.workbenchFilter, () => {
          return;
        });
      } else {
        searchApprovalCases(this.context, approval.approvalFilter, ()=>{
            return;
        });
      }

    }).then(()=>{
        pendingDocNotification(this.context, tempAppCase.approvalCaseId);
    }).catch((e) =>{
        // logger.error(e);
    });
  }

  genContent(template, changedValues) {
    let items = [];
    let {errorMsg} = this.state;
    let {store} = this.context;
    let {app: {agentProfile}} = store.getState();
    template.items.forEach((obj) =>{
        if (obj.type.toUpperCase() === 'PICKER') {
          if (identifyApproveLevel(agentProfile) === FAADMIN) {
            obj.options = [
                {
                    'value': 'PDocFAF',
                    'title': {
                        'en': 'Pending document'
                    }
                },
                {
                    'value': 'PDisFAF',
                    'title': {
                        'en': 'Pending discussion'
                    }
                }
            ];
          }
          items.push(
              <EABPickerField
                  id = {obj.id}
                  itemId={'reason-section-' + obj.id}
                  key = {'key-reason-section-' + obj.id}
                  template = {obj}
                  values = {changedValues}
                  changedValues = {changedValues}
                  handleChange = {this.handlePickerChange}
                  disabled={obj.disabled}
              />
          );
        } else if (obj.type.toUpperCase() === 'BUTTON') {
            items.push(
              <div style={{paddingTop: '20px'}}>
                <FlatButton
                    key = {'key-reason-section-' + obj.id}
                    label={obj.label}
                    onClick={this.saveComment}
                    disabled={obj.disabled}
                    backgroundColor={window.isMobile.apple.phone ? '#103184' : undefined}
                    labelStyle={window.isMobile.apple.phone ? {color: 'white'} : undefined}
                    style={window.isMobile.apple.phone ? {width: '100%'} : undefined}
                />
              </div>
               );
        } else if (obj.type.toUpperCase() === 'TEXTAREA') {
            items.push(
                 <div className={styles.BorderAllAround} style={{height: '100px'}}>
                    <TextField
                        id={'reason-section-' + obj.id}
                        key = {'key-reason-section-' + obj.id}
                        style={{padding: '0px 12px', width: 'calc(100% - 24px)'}}
                        multiLine={true}
                        rows={3}
                        rowsMax={3}
                        errorText= { errorMsg }
                        errorStyle={{lineHeight:'16px', bottom: '-10px'}}
                        fullWidth={true}
                        value={this.state.comment}
                        hintText='Please provide your reason'
                        hintStyle={{top: '0px'}}
                        onChange={(e, newValue)=> {this.commentValidate(newValue);}}
                        disabled={obj.disabled}
                        underlineShow={false}
                    />
                </div>
            );
        }
    });
    return items;
  }

  render() {
    let {
      template,
      changedValues
    } = this.props;


    let {
      id
    } = template;

    return (
      <div key='commentOnHoldReason'>
        {   this.genContent(template, changedValues) }
      </div>
    );
  }
}

export default CommentReason;
