import  React from 'react';

import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';
import IconButton from 'material-ui/IconButton';
import {goSupportDocuments, downloadZippedSupportDocuments} from '../../actions/supportDocuments';
import {goShieldSupportDocuments} from '../../actions/shieldSupportDocuments';
import { getIcon } from '../Icons/index';
import isFunction from 'lodash/isFunction';
import * as _ from 'lodash';
import b64toBlob from 'b64-to-blob';
import FileSaver from 'file-saver';

class DocAction extends EABInputComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
        approvalAccess: '',
        supCompleteFlag: false,
        approvalCase: {}
    })
  }

  componentDidMount(){
        this.unsubscribe = this.context.store.subscribe(this.storeListener);
        this.storeListener();
    }

    storeListener= ()=>{
        if(this.unsubscribe){
            let {store} = this.context;
            let {approval} = store.getState();
            let newState = {};

            if(!_.isEqual(this.state.approvalAccess, approval.currentTemplate)) {
                newState.approvalAccess = approval.currentTemplate;
            }

            if(!_.isEqual(this.state.supCompleteFlag, approval.supCompleteFlag)) {
                newState.supCompleteFlag = approval.supCompleteFlag;
            }

            if(!_.isEqual(this.state.approvalCase, approval.approvalCase)) {
                newState.approvalCase = approval.approvalCase;
            }

            if(!window.isEmpty(newState)){
                this.setState(newState);
            }
        }
    }

    componentWillUnmount(){
        if(isFunction(this.unsubscribe)){
            this.unsubscribe();
            this.unsubscribe = undefined;
        }
    }

  genContent() {
    let {langMap, store} = this.context;
    let {approval, app:{agentProfile}} = store.getState();
    let {agentCode} = agentProfile;
    let {
      template,
      changedValues
    } = this.props;

    let {supCompleteFlag} = this.state;

    let {approvalAccess} = this.state;
    let role;
    if (approvalAccess === 'A' || approval.canApprove) {
        role = 'supervisor';
    } else {
        role = 'agent';
    }

    let {
      id,
      title,
      iconName,
      subType
    } = template;
    let onclickFunc;
    if (subType === 'supportingDoc') {
        if (_.get(this.state.approvalCase, 'isShield')) {
            onclickFunc = goShieldSupportDocuments;
        } else {
            onclickFunc = goSupportDocuments;
        }
    }
    else if (subType === 'downloadSupportingDocs') {
      onclickFunc = downloadZippedSupportDocuments;
    }

    const titleClass = (window.isMobile.apple.phone) ? styles.DynColLabel : styles.subTextTitle + ' ' + styles.DynColLabel;

    return <div key={id} id={id} className={styles.DetailsItem + ' ' + styles.h12box} style={(window.isMobile.apple.phone) ? {display: 'flex', minHeight: '0px'} : undefined}>
        <div className={titleClass} style={(window.isMobile.apple.phone) ? {fontWeight: '600', fontSize: '16px', minWidth: '110px'} : undefined}>
          {window.getLocalizedText(langMap, title)}
        </div>
        <div style={{opacity: (supCompleteFlag) ? undefined : 0.3}}>
            <IconButton onClick={()=>{
              if (subType === 'supportingDoc') {
                onclickFunc(this.context, changedValues.applicationId, 'SUBMITTED', role);
              }
              else if (subType === 'downloadSupportingDocs') {
                let {applicationId, isShield, policyId, approvalCaseId, policiesMapping, proposerName} = changedValues;
                let proposalNumber = policyId || 'supportDocuments';
                if (isShield && policiesMapping && policiesMapping[0]) {
                  if (policiesMapping[0].policyNumber) {
                    proposalNumber = policiesMapping[0].policyNumber;
                  }
                }
                let  data = {agentCode, applicationId, isShield, policyId, approvalCaseId, proposalNumber, proposerName};
                onclickFunc(this.context, data, (res) => {
                  if (res && res.success) {
                    let contentType = 'application/zip';
                    let b64Data = res.data;
                    let fileName = res.fileName || 'supportDocuments.zip';
                    let blob = b64toBlob(b64Data, contentType);
                    FileSaver.saveAs(blob, fileName);
                  }
                });
              }
            }}>
            {getIcon(iconName, 'black')}
            </IconButton>
        </div>
    </div>;
  }


  render() {
    return (
      this.genContent()
    );
  }
}

export default DocAction;
