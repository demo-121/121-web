import React, {PropTypes} from 'react';
import {CircularProgress} from 'material-ui';
import * as _ from 'lodash';
import PDFJS from 'pdfjs-dist/build/pdf';

import EABComponent from '../Component';

export default class ImagesViewer extends EABComponent {

  static propTypes = {
    url: PropTypes.string,
    pdf: PropTypes.string,
    scale: PropTypes.number
  };

  static defaultProps = {
    scale: 1.5
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      pages: 0,
      rendering: true
    });
  }

  componentDidMount() {
    this.onresize = () => {
      clearTimeout(this.resizing);
      this.resizing = setTimeout(() => {
        this.setState({
          rendering: true
        }, () => {
          this.renderPages();
        });
      }, 500);
    };
    window.addEventListener('resize', this.onresize);

    this.loadPdf(this.props.url, this.props.pdf);
  }

  componentWillUnmount() {
    if (this.onresize) {
      window.removeEventListener('resize', this.onresize);
    }
    this.unmounted = true;
  }

  componentWillReceiveProps(newProps) {
    if (this.props.url !== newProps.url || this.props.pdf !== newProps.pdf) {
      this.loadPdf(newProps.url, newProps.pdf);
    }
  }

  loadPdf(url, data) {
    if (!url && !data) {
      return;
    }
    //scroll to top and clear view in second time
    if (this.container) {
      this.container.scrollTop = 0;
    }
    PDFJS.getDocument(url || window.base64ToArrayBuffer(data).buffer).then((pdf) => {
      let promises = [];
      for (let i = 1; i <= pdf.numPages; i++) {
        promises.push(pdf.getPage(i));
      }
      Promise.all(promises).then((pages) => {
        this.setState({
          pages: pages,
          rendering: true
        }, () => {
          this.renderPages();
        });
      });
    });
  }

  renderPages() {
    const {scale} = this.props;
    const {pages} = this.state;
    let minScale = scale;
    let windowWidth = window.innerWidth * 0.9;
    _.each(pages, (page) => {
      let viewport = page.getViewport(scale);
      if (viewport.width > windowWidth) {
        minScale = Math.min(minScale, scale * windowWidth / viewport.width);
      }
    });
    let p = Promise.resolve();
    _.each(pages, (page, index) => {
      p = p.then(() => {
        if (this.unmounted) {
          return;
        }
        return this.renderPage(index, minScale);
      });
      if (index >= pages.length - 1) {
        p.then(() => {
          this.setState({ rendering: false });
        });
      }
    });
  }

  renderPage(pageIndex, scale) {
    const {pages} = this.state;
    let page = pages[pageIndex];
    if (!page) {
      return;
    }
    let viewport = page.getViewport(scale);
    let canvas = this.refs['canvas-' + pageIndex];
    canvas.width = viewport.width;
    canvas.height = viewport.height;
    return page.render({
      canvasContext: canvas.getContext('2d'),
      viewport
    }).promise;
  }

  render() {
    const {muiTheme} = this.context;
    const {pages, rendering} = this.state;
    let pageDivs = _.map(pages, (page, index) => (
      <div
        key={'canvas-container-' + index}
        style={{
          margin: muiTheme.baseTheme.spacing.desktopGutterMini,
          border: '1px solid #979797',
          boxShadow: 'rgba(0, 0, 0, 0.5) 0px 2px 4px'
        }}
      >
        <canvas ref={'canvas-' + index} />
      </div>
    ));
    return (
      <div
        ref={(ref)=>{this.container = ref;}}
        style={{
          overflow: 'auto',
          display: 'flex', minHeight: '0px',
          flexDirection: 'column',
          alignItems: 'center',
          width: '100%',
          height: '100%'
        }}
      >
        {pageDivs}
        <CircularProgress
          style={{
            position: 'fixed',
            left: '50%',
            top: '50%',
            margin: '-20px 0 0 -20px',
            display: !rendering ? 'none' : null
          }}
        />
      </div>
    );
  }

}
