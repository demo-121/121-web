import {PropTypes} from 'react';

import EmbedPDFViewer from './EmbedPDFViewer';

export default class EmbedPDFAttachmentViewer extends EmbedPDFViewer {

  static propTypes = {
    docId: PropTypes.string,
    attchmentId: PropTypes.string,
    getTokenFunc: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      token: null
    };
  }

  componentDidMount() {
    this.getToken(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.docId !== nextProps.docId || this.props.attachmentId !== nextProps.attachmentId) {
      this.getToken(nextProps);
    }
  }

  getToken(props) {
    const {docId, attachmentId, getTokenFunc} = props;
    if (docId && attachmentId) {
      getTokenFunc && getTokenFunc(docId, attachmentId, (token) => {
        this.setState({
          token: token || ''
        });
      });
    }
  }

  onOpenPdf(dataUrl) {
    // refresh 1-time token after opening the pdf
    this.getToken(this.props);
    super.onOpenPdf(dataUrl);
  }

  getUrl(props) {
    const {token} = this.state;
    return token && window.genAttPath(token);
  }

}
