import React, { Component, PropTypes} from 'react';
import mui, {TextField} from 'material-ui';
import EABInputComponent from './EABInputComponent';

import HeaderTitle from './HeaderTitle';
import ErrorMessage from './ErrorMessage';
import styles from '../Common.scss';

let _mount = false;

class TextArea extends EABInputComponent{
  componentWillUnmount(){
    _mount=false;
  }

  componentDidMount(){
    _mount=true;
  }


  render() {
    let {lang, validRefValues} = this.context;
    let {changedValues} = this.state;
    let {
      template,
      rootValues,
      values,
      handleChange,
      disabled,
      docId
    } = this.props;

    let {id, title, hints, mandatory, desc, placeholder, masked, align, rows, rowsMax, maxLength, wordLimit, whiteBackground, multiLine=true, disableErrorContainer} = template;

    let cValue = this.getValue("");
    let errorMsg = this.getErrMsg();
    let customClass;
    if (align)
      customClass = styles.headerTitleAlignLeftContainer;

    return (
      <div className={(align) ? '' : styles.TextAreaContainer + ' ' + styles.PageMaxWidth}>
        {(title) ? <HeaderTitle title={title} mandatory={mandatory} hints={hints} customStyleClass={customClass}/> : undefined}
        <div className={(align) ? '' : styles.TextArea}>
          {(desc) ? <div className={styles.Description}>{getLocalText(lang, desc)}</div> : undefined}
          <div className={(whiteBackground) ? styles.WhiteBorderAllAround : styles.BorderAllAround}>
            <TextField
              id={id}
              style={{padding: '0px 12px', width: 'calc(100% - 12px)'}}
              hintText={getLocalText(lang, placeholder)}
              hintStyle={{top: '12px'}}
              underlineShow={false}
              rows={rows || 5}
              rowsMax={rowsMax || 10}
              multiLine={multiLine}
              fullWidth={true}
              disabled={disabled}
              maxLength={wordLimit}
              onFocus = {(e)=>{
                if (masked == MASK_FIELD && changedValues[id] == values[id]) {
                  changedValues[id] = "";
                  this.forceUpdate();
                }
              }}
              value = {cValue}
              onChange = {
                (e)=> {
                  this.requestChange(e.target.value);
                }
              }
              onBlur = {
                (e)=>{
                  if (masked == MASK_FIELD && values && changedValues[id] == "" && values[id]) {
                    changedValues[id] = values[id];
                    this.forceUpdate();
                  } else {
                    this.handleBlur(e.target.value);
                  }
                }
              }
            />
          </div>
          {(disableErrorContainer) ? <div style={{paddingBottom: '24px'}} /> : <div className={styles.ErrorMsgContainer}>{_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg} style={{textAlign: align || 'center'}}/>: null}</div>}
        </div>
      </div>
    );
  }
}

export default TextArea;
