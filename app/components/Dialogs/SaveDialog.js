import React, { PropTypes } from 'react';
import { Dialog, Toolbar, ToolbarGroup, IconButton, FlatButton, ToolbarTitle } from 'material-ui';

import styles from '../Common.scss';
import {getIcon} from '../Icons/index';
import EABInputComponent from '../CustomViews/EABInputComponent';

export default class SaveDialog extends EABInputComponent {

  static propTypes = {
    open: PropTypes.bool,
    title: PropTypes.string,
    children: PropTypes.any,
    bodyClassName: PropTypes.object,
    style: PropTypes.func,
    className: PropTypes.string,
    disableSave: PropTypes.bool,
    onSave: PropTypes.func,
    onClose: PropTypes.func
  };

  onSave() {
    const {onSave} = this.props;
    onSave && onSave();
  }

  onClose() {
    const {onClose} = this.props;
    onClose && onClose();
  }

  render() {
    const {open, title, disableSave, children, style, bodyClassName, className} = this.props;
    const {muiTheme, langMap} = this.context;
    let fontColor = muiTheme.palette.primary1Color;
    let titleBar = (
      <div style={{ padding: 0, margin: 0, background: '#FAFAFA' }}>
        <Toolbar className={styles.Toolbar} style={this.getStyles().toolBar}>
          <ToolbarGroup style={{ paddingLeft: '16px' }}>
            <IconButton onTouchTap={() => this.onClose()}>
              {getIcon('close', fontColor)}
            </IconButton>
            <ToolbarTitle text={title} style={this.getStyles().toolBarTitle} />
          </ToolbarGroup>
          <ToolbarGroup style={{ display:'flex', paddingRight: 16, paddingTop: 2 }}>
            <FlatButton
              label={window.getLocalizedText(langMap, 'quotation.riders.save')}
              labelStyle={disableSave ? this.getStyles().toolBarActionDisabledFlatBtn : this.getStyles().toolBarActionFlatBtn}
              disabled={disableSave}
              onTouchTap={() => this.onSave()}
            />
          </ToolbarGroup>
        </Toolbar>
        <div className={styles.Divider} />
      </div>
    );
    return (
      <Dialog
        open={open}
        autoScrollBodyContent
        title={titleBar}
        contentStyle={{ ...style }}
        contentClassName={className}
        bodyClassName={bodyClassName}
        bodyStyle={{ overflow: 'auto', padding: 0 }}
      >
        {children}
      </Dialog>
    );
  }

}
