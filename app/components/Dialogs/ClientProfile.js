import React from 'react';
import {TextField, Toolbar, ToolbarTitle, ToolbarGroup,  Dialog, Divider, IconButton, FlatButton} from 'material-ui';

import * as _ from 'lodash';
import * as _v from '../../utils/Validation';

import LinkClient from './LinkClient';
import DynColumn from '../DynViews/DynColumn';
import EABComponent from '../Component';
import Appbar from '../CustomViews/AppBar';

import {getIcon} from '../Icons/index';

import {
  saveProfile,
  deleteProfile,
  saveFamilyMember,
  unlinkRelationship,
  updateContactList
} from '../../actions/client';

import styles from '../Common.scss';

class Main extends EABComponent {

  constructor(props, context) {
      super(props);
      let {values={}, template} = props;

      this.state = Object.assign({}, this.state, {
        values: _.cloneDeep(values),
        changedValues: _.cloneDeep(values),
        error: {},
        template: this.handleTemplate(props, context),
        showUnlinkDialog: false,
        showUpdFMDialog: false,
        showDelProfileDialog: false,
        pageIndex: 0,
        searchValue: '',
        viewAll: false,
        appbar: {
          menu: {
            icon: 'close',
            action: ()=>{
              this.closeDialog();
            }
          },
          title: 'Client Profile',
          itemsIndex: (props.isFamily && !values.cid)?2:0,
          items:[
            [{
              type: 'flatButton',
              title: {
                "en": "SAVE"
              },
              disabled: true
            }],
            [{
              type: 'flatButton',
              title: {
                "en": "SAVE"
              },
              action: ()=>{
                this.submit();
              }
            }],
            [{
              type: 'flatButton',
              title: {
                "en": "LINK"
              },
              action: ()=>{
                this.setState({pageIndex:1})
              }
            },{
              type: 'flatButton',
              title: {
                "en": "SAVE"
              },
              disabled: true
            }],
            [{
              type: 'flatButton',
              title: {
                "en": "LINK"
              },
              action: ()=>{
                this.setState({pageIndex:1})
              }
            },{
              type: 'flatButton',
              title: {
                "en": "SAVE"
              },
              action: ()=>{
                this.submit();
              }
            }]
          ]
        }
      })
  };

  openDialog=(value, isNew)=>{
    let newState = {
      open:true,
      showUnlinkDialog: false,
      showUpdFMDialog: false,
      isNew,
      pageIndex: 0
    };

    if(value){
      newState.values = _.cloneDeep(value);
      newState.changedValues = _.cloneDeep(value)
      if(this.props.isSpouse){
        newState.changedValues.relationship = 'SPO';
        newState.changedValues.marital = 'M';
      }
    }else{
      newState.changedValues = _.cloneDeep(this.state.values);
    }

    newState.template = this.handleTemplate(this.props, this.context);

    this.validate(newState.changedValues, newState);
  }

  closeDialog=()=>{
    this.setState({open: false, showUnlinkDialog: false, showUpdFMDialog: false});
  }

  addMandatory=(template, mandatoryId)=>{
    if(template.id && mandatoryId.indexOf(template.id)>-1){
      template.mandatory = true
    }
    else if(template.key && mandatoryId.indexOf(template.key)>-1){
      template.mandatory = true
    }
    if(template.items){
      _.forEach(template.items, item=>this.addMandatory(item, mandatoryId));
    }
  }

  handleTemplate=(props, context)=>{
    let {store} = context;
    let {isFamily, isInsured, afterFNA, mandatoryId, isSpouse} = props;
    let {editDialog} = store.getState().client.template;

    let template = _.cloneDeep(editDialog);
    //add relationship into section for family member
    if(isFamily && !isInsured){
      template.items[0].items[0].items.push({
        "type":"12BOX",
        "items":[{
          "id": "relationship",
          "title": {
            "en": "Relationship"
          },
          "type": "PICKER",
          "options": "relationship",
          "mandatory": true,
          "disabled": isSpouse || afterFNA,
          "validation": "function(i,v,vr,vrv){var a=v[i.id], g=v.gender;if((['GFA','SON','FAT','BRO','GSO'].indexOf(a)>-1 && g=='F') || (['GMO','MOT','DAU','GDA','SIS'].indexOf(a)>-1 && g=='M') || (a==='SPO' && g===vrv.profile.gender)){return 601;} var d=vrv.profile.dependants; for(var i in d){if(d[i].relationship === 'SPO' && a==='SPO' && v.cid != d[i].cid){return 602;}} if(v.relationship=='SPO' && vrv.profile.marital != 'M') {return 608;} }"
        },{
          "id": "relationshipOther",
          "title": {
            "en": "Please Specify"
          },
          "type": "text",
          "mandatory": true,
          "disabled": afterFNA,
          "trigger":{
            "id": "relationship",
            "type": "showIfContains",
            "value": "OTH"
          }
        }]
      });
    }

    if(!isEmpty(mandatoryId)){
      this.addMandatory(template, mandatoryId);
    }
    return template;
  }


  validateValues=(profile)=>{
    let {langMap, optionsMap, store} = this.context;
    let {template} = this.state;
    let {profile: cProfile} = store.getState().client;
    let validRefValues = {isApp: this.props.isApp, optionsMap};
    if(this.props.isFamily){
      validRefValues.profile = cProfile;
    }
    return _v.exec(template, optionsMap, langMap, profile, null, validRefValues);
  }

  getAppbarItemsIndex=(valid, isNew)=>{
    return this.props.isFamily && isNew?valid?3:2:valid?1:0;
  }

  validate=(changedValues, newState={})=>{
    let {langMap, optionsMap, store} = this.context;
    let {profile: cProfile} = store.getState().client;
    if(isEmpty(newState)){
      newState.template = this.state.template;
      newState.values = _.cloneDeep(this.state.values);
    }
    let {template, values} = newState;

    let error = newState.error = {};
    let validRefValues = {isApp: this.props.isApp};
    if(this.props.isFamily){
      validRefValues.profile = cProfile;
    }
    _v.exec(template, optionsMap, langMap, changedValues, null, validRefValues, error)
    let valid = !error.code;
    let {appbar} = this.state;
    appbar.itemsIndex = this.getAppbarItemsIndex(valid, !changedValues.cid);
    newState.appbar = appbar;
    this.setState(newState);
    return valid;
  }

  submit=(confirm)=>{
    let {changedValues, isNew} = this.state;
    let {onSubmit, isFamily} = this.props;

    let callback = ()=>{
      this.closeDialog();
      if(_.isFunction(onSubmit)){
        onSubmit();
      }
    }

    if(isFamily){
      saveFamilyMember(this.context, changedValues.cid, changedValues, confirm, (code)=>{
        if(code){
          this.setState({
            errorCode: code,
            showUpdFMDialog: true
          })
        }
        else {
          updateContactList(this.context, false, callback);
        }
      });
    }else{
      saveProfile(this.context, changedValues, isNew, confirm, (code)=>{
        if(code){
          this.setState({
            errorCode: code,
            showUpdFMDialog: true
          })
        }
        else {
          updateContactList(this.context, false, callback);
        }
      });
    }
  }

  generatePage1 = (sV, viewAll) => {
    return <LinkClient
      key="dialog-link-client"
      filterDependants={true}
      ref={ref=>{this.LinkClient=ref}}
      searchValue={sV}
      viewAll={viewAll}
      onItemClick={(profile)=>{
        if(this.props.isFamily && this.props.isSpouse){
          profile.relationship = 'SPO';
          profile.marital = 'M';
        }
        this.openDialog(profile, true);
      }}/>
  }

  generateLinkClientAppbar = () => {
    let {
      muiTheme,
      router,
      langMap
    } = this.context;

    let _style = {
      paddingLeft: '16px',
      flex: 1
    }
    let iconColor = muiTheme.palette.alternateTextColor;

    let searchBar = (
      <TextField
        id="search"
        key="searchBar"
        ref="searchBar"
        hintText={getLocalizedText(langMap, "search.contact.hintText", "Please Enter number")}
        fullWidth={true}
        value={this.state.searchValue}
        onChange={(e)=>{
            this.setState({searchValue: e.target.value, viewAll: false});
        }}
      />
    );

    return <Toolbar className= {styles.Toolbar} style={{background: '#FAFAFA', boxShadow: 'none'}}>
      <ToolbarGroup style={_style}>
        <IconButton onTouchTap={()=>{
          this.setState({pageIndex: 0})
        }}>
          {getIcon("back", iconColor)}
        </IconButton>
        <ToolbarTitle text={searchBar}style={{paddingLeft: '16px', flex: 1}}/>
      </ToolbarGroup>
      <ToolbarGroup style={{display:'flex', marginRight:20, paddingTop: 2}}>
        {!isEmpty(this.state.searchValue)?
          <FlatButton
            label={"CLEAR"}
            style={{margin: 0}}
            labelStyle={{ fontWeight: 'bold', color: iconColor }}
            onTouchTap={()=>{this.setState({searchValue:""})}}
          />:
          <FlatButton
            label={"VIEW ALL"}
            style={{margin: 0}}
            labelStyle={{ fontWeight: 'bold', color: iconColor }}
            onTouchTap={()=>{this.setState({viewAll: true})}}
          />
        }
      </ToolbarGroup>
    </Toolbar>
  }


  render() {
    let {muiTheme, langMap, lang} = this.context;
    let {open, values, changedValues, error, template, appbar, errorCode, showUnlinkDialog, showUpdFMDialog, showDelProfileDialog, isNew} = this.state;
    let {onDelete} = this.props;

    let iconColor = muiTheme.palette.alternateTextColor;
    let _style = {paddingLeft: '16px'}

    let deleteButton=(
      <div className={styles.dialogDelBtn}>
        <FlatButton
          label={"Delete Customer"}
          onTouchTap={()=>{
            this.setState({showDelProfileDialog: true});
          }}/>
      </div>
    )

    const updFMActions = [
      (
        <FlatButton
          key="profileDialog-updFM-cancel-btn"
          label={"No"}
          onTouchTap={()=>{
            this.setState({
              showUpdFMDialog: false,
              changedValues: _.cloneDeep(this.state.values),
            })
            }}
        />
      ),
      (
        <FlatButton
          key="profileDialog-updFM-confirm-btn"
          label={"Yes"}
          onTouchTap={()=>{
            this.submit(true);
          }}
        />
      )
    ]

    const unlinkActions = [
      (
        <FlatButton
          key="profileDialog-unlink-cancel-btn"
          label={"No"}
          onTouchTap={()=>{this.setState({showUnlinkDialog: false})}}
        />
      ),
      (
        <FlatButton
          key="profileDialog-unlink-confirm-btn"
          label={"Yes"}
          onTouchTap={()=>{
            unlinkRelationship(this.context, values.cid, ()=>{
              this.closeDialog();
            });
          }}
        />
      )
    ]

    const delProfileActions = [
      (
        <FlatButton
          key="profileDialog-delete-profile-cancel-btn"
          label={"No"}
          onTouchTap={()=>{this.setState({showDelProfileDialog: false})}}
        />
      ),
      (
        <FlatButton
          key="profileDialog-delete-profile-confirm-btn"
          label={"Yes"}
          onTouchTap={()=>{
            deleteProfile(this.context, changedValues, ()=>{
              this.closeDialog();
              if(_.isFunction(onDelete)){
                onDelete();
              }
            });
          }}
        />
      )
    ]

    let unlinkButton=(
      <div style={{width: '100%', textAlign: 'right', paddingRight: '24px'}}>
        <FlatButton
          label={"Delete"}
          labelStyle={{ fontWeight: 'bold', color: "#FF0000" }}
          onTouchTap={()=>{
            this.setState({showUnlinkDialog: true});
          }}/>
      </div>
    )

    var page = null;

    if (this.state.pageIndex===0) {
      page = (
        <div key="page0" >
              <Dialog open={showUnlinkDialog} actions={unlinkActions}>
                {getLocalizedText(langMap, "FMDIALOG._UNLINK", "Do you wish to exclude the life assured?")}
              </Dialog>
              <Dialog open={showUpdFMDialog} actions={updFMActions}>
                {_v.getErrorMsg(null, errorCode, lang)}
              </Dialog>
              <Dialog open={showDelProfileDialog} actions={delProfileActions}>
                {getLocalizedText(langMap, "PROFILE_DIALOG.DELETE_MSG", "All the client information will be lost if you delete. Do you confirm?")}
              </Dialog>
              <DynColumn
                ref={ref=>{this.dynColumn=ref}}
                template={template}
                values={values}
                changedValues={changedValues}
                error={error}
                validate={this.validate}
                docId={changedValues.cid}
              />
              {!isNew?(
                this.props.isFamily?
                  (!this.props.afterFNA?unlinkButton:null):
                  !changedValues.haveSignDoc?deleteButton:null
                ):null
              }
        </div>
      )
    } else {
      page = this.generatePage1(this.state.searchValue, this.state.viewAll)
    }

    return (
      <Dialog open={!!open}
        className={(this.state.pageIndex===0) ? styles.FixWrongDialogPadding : ''}
        bodyClassName={(this.state.pageIndex===0) ? styles.ClientProfileDialog : styles.LinkProfileDialog}
        contentStyle={{width: '100%', maxWidth: '1024px'}}
        title={(this.state.pageIndex===0) ?
         <div style={{padding: '0px'}}>
         <Appbar ref={ref=>{this.appbar=ref}} showShadow={false} template={appbar} style={{background: '#FAFAFA'}}/>
           <div className={styles.Divider}/>
          </div>
          :
          this.generateLinkClientAppbar()
        }
        autoScrollBodyContent={true}
        autoDetectWindowHeight={true}
        onDrop={(e)=>{e.preventDefault()}}
        onDragOver={(e)=>{e.preventDefault()}}>
        {page}
      </Dialog>
    );
  }
}

export default Main;
