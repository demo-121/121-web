import React from 'react';
import * as _ from 'lodash';
import{TextField, Toolbar, ToolbarTitle, ToolbarGroup,  Dialog, Divider, IconButton, FlatButton} from 'material-ui';

import LinkClient from './LinkClient';
import DynColumn from '../DynViews/DynColumn';
import Appbar from '../CustomViews/AppBar';

import {getIcon} from '../Icons/index';
import EABComponent from '../Component';

import styles from '../Common.scss';

class Main extends EABComponent {

  constructor(props) {
      super(props);
      this.state = Object.assign({}, this.state, {
        open: false,
        searchValue: '',
        viewAll: false
      })
  };

  openDialog=()=>{
    this.setState({open: true});
  }

  closeDialog=()=>{
    this.setState({open: false});
  }

  generateLinkClientAppbar = () => {
    let {
      muiTheme,
      router,
      langMap
    } = this.context;

    let _style = {
      paddingLeft: '16px',
      flex: 1
    }
    let iconColor = muiTheme.palette.alternateTextColor;

    let searchBar = (
      <TextField
        id="search"
        key="searchBar"
        ref="searchBar"
        hintText={getLocalizedText(langMap, "search.contact.hintText", "Please Enter number")}
        fullWidth={true}
        value={this.state.searchValue}
        onChange={(e)=>{
            this.setState({searchValue: e.target.value, viewAll: false});
        }}
      />
    );

    return <Toolbar className= {styles.Toolbar} style={{background: '#FAFAFA', boxShadow: 'none'}}>
      <ToolbarGroup style={_style}>
        <IconButton onTouchTap={()=>{
          this.closeDialog();
        }}>
          {getIcon("back", iconColor)}
        </IconButton>
        <ToolbarTitle text={searchBar}style={{paddingLeft: '16px', flex: 1}}/>
      </ToolbarGroup>
      <ToolbarGroup style={{display:'flex', marginRight:20, paddingTop: 2}}>
        {!isEmpty(this.state.searchValue)?
          <FlatButton
            label={"CLEAR"}
            style={{margin: 0}}
            labelStyle={{ fontWeight: 'bold', color: iconColor }}
            onTouchTap={()=>{this.setState({searchValue:""})}}
          />:
          <FlatButton
            label={"VIEW ALL"}
            style={{margin: 0}}
            labelStyle={{ fontWeight: 'bold', color: iconColor }}
            onTouchTap={()=>{this.setState({viewAll: true})}}
          />
        }
      </ToolbarGroup>
    </Toolbar>
  }


  render() {
    let {muiTheme, langMap} = this.context;

    let iconColor = muiTheme.palette.alternateTextColor;
    let _style = {paddingLeft: '16px'}
    let {open} = this.state;


    return (
      <Dialog open={open} bodyStyle={{padding: '0px', margin: '0px'}}
        title={this.generateLinkClientAppbar()}
         autoScrollBodyContent={true}  autoDetectWindowHeight={true}
      >
        <LinkClient
          key="dialog-link-client"
          filterDependants={true}
          searchValue={this.state.searchValue}
          viewAll={this.state.viewAll}
          onItemClick={(profile)=>{
            this.closeDialog();
            if(_.isFunction(this.props.onSubmit)){
              this.props.onSubmit(profile)
            }
          }}/>
      </Dialog>
    );
  }
}

export default Main;
