import React, { Component, PropTypes } from 'react';
import mui, {Paper, TextField, RaisedButton, FontIcon,FlatButton} from 'material-ui';
import { Link } from 'react-router';
import styles from '../../Home.css';
import * as actions from '../../../actions/home.js';

import EABComponent from '../../Component';

let _mount = false;

class Login extends EABComponent {
  constructor(props) {
      super(props);
      this.state = Object.assign({}, this.state, {data001:'', data002:'', hasError:false, errorMsg:'', logging:false, showLogin:true});
  };

  componentDidMount() {
    _mount = true;
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    let {home} = this.context.store.getState();
    this.setState({
      hasError: home.hasError,
      errorMsg: home.errorMsg
    })
  };

  componentWillUnmount(){
    _mount = false;
    this.unsubscribe();
  };

  storeListener=()=>{
    if(_mount){
      var {home} = this.context.store.getState();
      this.setState({
          logging: false,
          hasError: home.hasError,
          data002 : home.hasError ? "" : this.state.data002,
          errorMsg: home.errorMsg
      });
    }
  };


  login=()=>{

    var d1 = this.state.data001;
    var d2 = this.state.data002;
    var data = {
      action: 'login',
      userName: d1,
      password: d2,
    }
    if(d1 != '' && d2 != '' && !this.state.logging){
      this.setState({logging:true, data001:'', data002:''});
      this.refs.data001.focus();
      actions.login(this.context, data);
    }
    //
    // this.context.router.replace("land");
  };

  handleD1Change=(e)=>{
    this.setState({data001: e.target.value});
  };

  handleD2Change=(e)=>{
    this.setState({data002: e.target.value});
  };

  keyPress=(e)=>{
    if(13 == e.keyCode){
      var d1 = this.state.data001;
      var d2 = this.state.data002;
      var data = {
        action: 'login',
        userName: d1,
        password: d2,
      }
      if(d1 != '' && d2 != '' && !this.state.logging){
        this.setState({logging:true});
        actions.login(this.context, data);
      }
    }

  };


  handleChange=()=>{
    var home = this.context.store.getState().home;
    this.setState({
        logging: false,
        hasError: home.hasError,
        data002 : home.hasError ? "" : this.state.data002,
        errorMsg: home.errorMsg
    });

  };

  changePage=()=>{
    var state = !this.state.showLogin;
    this.setState({showLogin:state})
  }

  render() {


    var self = this;
    var langMap = this.context.langMap;

    var labelStyle = {
      color:'#2696CC',
      fontSize: '13px',
      marginTop:'20px',
      display: 'block',
      cursor: 'pointer',
          fontWeight: 'bold',
    }

    var show = {
      'display' : 'inline-block',
      verticalAlign: 'top',
      padding:'0px 30px',
      width:'400px',
    };


    var btStyle={
    }

    var viewTitleStyle = {
      fontSize:'18px',
      fontWeight: 300,
      margin: 0,
    }

    var btlabelStyle = {
      color:'#2696CC',
      fontSize:'16px',
      fontWeight: 300,
    }
    return (

      <div key="viewLogin" style={show}>
        <p key="vlTitle" style={viewTitleStyle}>{getLocalizedText(langMap, 'login', "Log in")}</p>
        <div className={styles.item} id="tfs" key="tfs">
          <TextField
          id="username"
          className={styles.item}
          key="username"
          ref="data001"
          hintStyle={{ bottom:'18px'}}
          floatingLabelText={getLocalizedText(langMap, 'email', 'Email')}
          errorStyle = {{lineHeight:'16px'}}
          floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
          onKeyDown = { self.keyPress.bind(this.login)}
          value={this.state.data001}
          onChange={self.handleD1Change}
          />
        </div>

        <div className={styles.item}>
          <TextField
          id="userpass"
          className={styles.item}
          key="userpass"
          ref="data002"
          type={this.state.data002 ? "password" : "text"}
          label={getLocalizedText(langMap, 'login.password')}
          floatingLabelText={getLocalizedText(langMap, 'login.password')}
          hintStyle={{ bottom:'18px'}}
          errorStyle = {{lineHeight:'16px'}}
          floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
          onKeyDown = { self.keyPress.bind(this.login)}
          value={this.state.data002}
          onChange={self.handleD2Change}
          />
        </div>

        <div className={styles.item} id="errMsg" key="errMsg">
          <p className={styles.error} id="errorMsg" key="errorMsg">{ getLocalizedText(langMap, this.state.errorMsg, "") }</p>
        </div>

        <div className={styles.item} id="loginBtC" key="loginBtC">

          <RaisedButton
            key="loginButton"
            secondary={true}
            label={ getLocalizedText(langMap, "login", "Log in")}
            onClick = { self.login}
            />


          <label className="forgetpass_" key="forpass" style={labelStyle} onClick={self.changePage}>{getLocalizedText(langMap, 'login.forgot_password')}</label>
        </div>
      </div>

    );
  }
}

export default Login;
