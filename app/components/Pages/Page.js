import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import Events from 'material-ui/utils/events.js'
import styles from '../Common.scss';
import appTheme from '../../theme/appBaseTheme';

import * as _ from 'lodash';

class Page extends Component {
  constructor(props) {
      super(props);
      this.state = {
        langMap: {},
        lang: 'en',
        muiTheme: appTheme.getTheme()
      }
  };

  getChildContext() {
    return {
      optionsMap: this.state.optionsMap,
      langMap: this.state.langMap,
      lang: this.state.lang,
      muiTheme: this.state.muiTheme
    }
  }

  componentDidMount() {
    this._handleResize();
    Events.on(window, 'resize', this._handleResize.bind(this));
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  }

  componentWillUnmount() {
    Events.off(window, 'resize', this._handleResize.bind(this));

    if (typeof this.unsubscribe == 'function') {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener = () =>{
    if(this.unsubscribe){
      let {optionsMap, langMap, lang} = this.context.store.getState().app;
      let newState = {};

      if(!isEqual(this.state.optionsMap, optionsMap) && !isEmpty(optionsMap)){
        newState.optionsMap = optionsMap;
      }

      if(!isEqual(this.state.langMap, langMap) && !isEmpty(langMap)){
        newState.langMap = langMap;
      }

      if(!isEqual(this.state.lang, lang) && !isEmpty(lang)){
        newState.lang = lang;
      }

      if(!isEmpty(newState))
        this.setState(newState);
    }
  }

  _handleResize=()=>{
    if (this.unsubscribe) {
      this.setState({
        muiTheme: appTheme.getTheme(true),
      });
    }
  }

  render() {
    return (
      <div className={styles.Page} 
        onDrop={(e)=>{e.preventDefault()}}
        onDragOver={(e)=>{e.preventDefault()}}>
        {this.props.children}
      </div>

    );
  }
}

Page.contextTypes={
    router: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired,
    children: PropTypes.array
}

Page.childContextTypes = {
  optionsMap: PropTypes.object,
  langMap: PropTypes.object,
  lang: PropTypes.string,
  muiTheme: PropTypes.object
}

export default Page;
