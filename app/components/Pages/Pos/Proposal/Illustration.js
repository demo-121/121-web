import React, {PropTypes} from 'react';
import {SelectField, MenuItem} from 'material-ui';
import * as _ from 'lodash';

import styles from '../../../Common.scss';
import EABComponent from '../../../Component';
import IllustrationChart from './Illustration/IllustrationChart';
import SelectableTable from './Illustration/SelectableTable';

import {backToProposal} from '../../../../actions/proposal';

export default class Illustration extends EABComponent {

  static propTypes = {
    quotation: PropTypes.object,
    planDetails: PropTypes.object,
    illustrateData: PropTypes.object
  };

  constructor(props) {
    super(props);
    const {quotation, planDetails} = this.props;
    const baseProductCode = quotation.baseProductCode;
    const projectionRates = planDetails[baseProductCode].projection;
    this.state = {
      selectedRow: 0,
      projectionRate: projectionRates && projectionRates.length ? projectionRates[0].title.en : null
    };
  }

  getAppbarData = () => {
    const {langMap} = this.context;
    return {
      menu: {
        icon: 'back',
        action: () => {
          backToProposal(this.context);
        }
      },
      title: window.getLocalizedText(langMap, 'proposal.illustration.title'),
      itemsIndex: 0,
      items: []
    };
  };

  _getProjectionRatesOptions = () => {
    const {quotation, planDetails} = this.props;
    const baseProductCode = quotation.baseProductCode;
    const projectionRates = planDetails[baseProductCode].projection;
    const options = _.map(projectionRates, (projectionRate) => {
      const rate = projectionRate.title.en;
      const rateTitle = isNaN(Number(rate)) ? (Number(Object.keys(projectionRate.rates)[0]) ? Number(Object.keys(projectionRate.rates)[0]).toFixed(2) : rate) : Number(rate).toFixed(2);
      return <MenuItem key={rate} value={rate} primaryText={rateTitle} />;
    });
    return options;
  };

  _getHeader() {
    const {quotation, planDetails} = this.props;
    const {projectionRate} = this.state;
    const projectionRates = planDetails[quotation.baseProductCode].projection;
    if (_.isEmpty(projectionRates)) {
      return null;
    }
    return (
      <div className={styles.IllHeader}>
        <div style ={{ display: 'inline-block'}}>
          <div className={styles.IllHeaderItem}>
            <span>Illustrated at</span>
            <SelectField
              id={'projectionRate'}
              value={projectionRate}
              onChange={(e, index, value) => {
                this.setState({ projectionRate: value });
              }}
              autoWidth
              style={{ flex: 1, margin: '0 24px', width: '160px' }}
            >
              {this._getProjectionRatesOptions()}
            </SelectField>
            <span> % Investment Return</span>
          </div>
        </div>
      </div>
    );
  }

  getColNames() {
    const {lang, langMap} = this.context;
    const {quotation, planDetails} = this.props;
    const baseProductCode = quotation.baseProductCode;
    const illCols = planDetails[baseProductCode].illustrationColumns;
    const headers = [];
    headers.push(window.getLocalizedText(langMap, 'benefitIllustration.table.age'));
    _.each(illCols, (illCol) => {
      headers.push(window.getLocalText(lang, illCol.title));
    });
    return headers;
  }

  getRowNames() {
    const {quotation, illustrateData} = this.props;
    const illustrations = illustrateData[quotation.baseProductCode];
    const labels = [];
    if (illustrations) {
      if (illustrations.illustration && Array.isArray(illustrations.illustration)){
        _.each(illustrations.illustration, (illData, index) => {
          labels.push(illData.age);
        });
      } else {
        _.each(illustrations, (illData, index) => {
          labels.push(quotation.iAge + index + 1);
        });
      }
      
    }
    return labels;
  }

  getData() {
    const {quotation, planDetails, illustrateData} = this.props;
    const {projectionRate} = this.state;
    const illustrations = illustrateData[quotation.baseProductCode];
    const baseProductCode = quotation.baseProductCode;
    const illCols = planDetails[baseProductCode].illustrationColumns;
    const data = [];
    if (illustrations) {
      if (illustrations.illustration && Array.isArray(illustrations.illustration)){
        _.each(illustrations.illustration, (illData, index) => {
          const cols = [];
          _.each(illCols, (illCol, i) => {
            const {id, isRelatedToProjectionRate} = illCol;
            if (illData[id]){
              let colVal = isRelatedToProjectionRate === 'Y' ? (projectionRate === '4' ? illData[id][0] : illData[id][1]) : illData[id];
              cols.push(Math.floor(colVal));
            }
          });
          data.push(cols);
        });

      } else {
        _.each(illustrations, (illData, index) => {
          const cols = [];
          _.each(illCols, (illCol, i) => {
            const {id, isRelatedToProjectionRate} = illCol;
            let colVal = isRelatedToProjectionRate === 'Y' ? illData[id][projectionRate] : illData[id];
            cols.push(Math.floor(colVal));
          });
          data.push(cols);
        });
      }
      
    }
    return data;
  }

  _hasIllustrateData() {
    const {quotation, planDetails} = this.props;
    const baseProductCode = quotation.baseProductCode;
    return planDetails[baseProductCode].illustrationColumns.length > 0;
  }

  render() {
    if (!this._hasIllustrateData()) {
      return (
        <div style={{height: '100%', width: '100%', display: 'inline-block', textAlign: 'center'}}>
          <div>Illustration data is not set up.</div>
        </div>
      );
    }
    const {optionsMap} = this.context;
    const {quotation} = this.props;
    const {selectedRow} = this.state;
    const sign = window.getCurrencySign(quotation.compCode, quotation.ccy, optionsMap);
    const colNames = this.getColNames();
    const rowNames = this.getRowNames();
    const data = this.getData();
    return (
     <div className={styles.IllContainer}>
        <div className={styles.IllContent}>
          {this._getHeader()}
          <IllustrationChart
            colNames={colNames}
            rowNames={rowNames}
            data={data}
            currencySign={sign}
            selectedRow={selectedRow}
            onSelectRow={(rowIndex) => this.setState({ selectedRow: rowIndex })}
          />
          <SelectableTable
            colNames={colNames}
            rowNames={rowNames}
            data={data}
            currencySign={sign}
            selectedRow={selectedRow}
            onSelectRow={(rowIndex) => this.setState({ selectedRow: rowIndex })}
          />
        </div>
      </div>
    );
  }

}
