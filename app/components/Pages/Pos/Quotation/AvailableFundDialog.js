import React, { PropTypes } from 'react';
import { SelectField, MenuItem, TextField, Checkbox } from 'material-ui';
import * as _ from 'lodash';

import EABComponent from '../../../Component';
import NumericField from '../../../CustomViews/NumericField';
import WarningDialog from '../../../Dialogs/WarningDialog';
import SaveDialog from '../../../Dialogs/SaveDialog';

import styles from '../../../Common.scss';

export default class AvailableFundDialog extends EABComponent {

  static propTypes = {
    open:           PropTypes.bool,
    init:           PropTypes.bool,
    onClose:        PropTypes.func,
    onSave:         PropTypes.func,
    riskProfile:    PropTypes.number,
    invOpt:         PropTypes.string,
    model:          PropTypes.object,
    riskDesc:       PropTypes.string,
    availableFunds: PropTypes.array,
    filterRiskRatings: PropTypes.array,
    fundAllocs:     PropTypes.object,
    hasTopUpAlloc:  PropTypes.bool,
    showTopUpAlloc: PropTypes.bool,
    topUpAllocs:    PropTypes.object,
    maxSelection:   PropTypes.number
  };

  static defaultProps = {
    maxSelection: 10
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      searchRisk: null,
      searchText: '',
      selectedFunds: {},
      fundAllocs: {},
      topUpAllocs: {},
      warningMsg: null
    });
  }

  componentWillReceiveProps(newProps) {
    let newState = {};
    if (!_.isEqual(newProps.availableFunds, this.state.availableFunds)) {
      newState.availableFunds = newProps.availableFunds;
    }
    if (!_.isEqual(newProps.fundAllocs, this.state.fundAllocs) || !_.isEqual(newProps.topUpAllocs, this.state.topUpAllocs)) {
      newState.fundAllocs = {};
      newState.selectedFunds = {};
      newState.topUpAllocs = {};
      _.each(newProps.fundAllocs, (alloc, fundCode) => {
        newState.fundAllocs[fundCode] = alloc;
        newState.selectedFunds[fundCode] = true;
      });
      _.each(newProps.topUpAllocs, (alloc, fundCode)=>{
        newState.topUpAllocs[fundCode] = alloc;
        newState.selectedFunds[fundCode] = true;
      });
    }
    if (!_.isEmpty(newState)) {
      this.setState(newState);
    }
    if (newProps.init){
      this.setState({ searchRisk: ''});
    }
  }

  _onSave() {
    const {onSave, showTopUpAlloc} = this.props;
    const {selectedFunds, fundAllocs, topUpAllocs} = this.state;
    if (onSave) {
      let saveResult = {
        fundAllocs: {},
        topUpAllocs: {}
      };
      _.each(selectedFunds, (selected, fundCode) => {
        if (selected) {
          saveResult.fundAllocs[fundCode] = fundAllocs[fundCode] || 0;
          saveResult.topUpAllocs[fundCode] = showTopUpAlloc ? (topUpAllocs[fundCode] || 0) : null;
        }
      });
      onSave(saveResult);
    }
  }

  _getFundList() {
    const {lang, langMap, optionsMap} = this.context;
    const { riskProfile,
            model,
            availableFunds,
            maxSelection,
            filterRiskRatings,
            hasTopUpAlloc} = this.props;

    let architasFunds = optionsMap.portfolioModels.architasFunds;

    const {selectedFunds, fundAllocs, topUpAllocs, searchRisk, searchText} = this.state;
    let searchValues = (searchText || '').split(' ').map(v => v.toLowerCase());
    let isDisplayNoAvailableFund = true;
    let fundList = _.map(availableFunds, (fund) => {
      let fundName = window.getLocalText(lang, fund.fundName);
      let assetClassDesc = window.getOptionTitle(fund.assetClass, optionsMap.assetClass, lang);
      let riskRatingDesc = window.getOptionTitle(fund.riskRating, optionsMap.riskRating, lang);
      if (filterRiskRatings) { // use explicit risk ratings first if exists, for model portfolio
        if (filterRiskRatings.indexOf(fund.riskRating) === -1) {
          return null;
        }
      } else if (riskProfile && fund.riskRating > riskProfile) {
        return null;
      }

      if (searchRisk && searchRisk !== '*') {
        if (fund.riskRating !== searchRisk) {
          return null;
        }
      }

      if (searchText) {
        let matchedValues = _.filter(searchValues, (v) => {
          return fundName.toLowerCase().indexOf(v) > -1 || fund.ccy.toLowerCase().indexOf(v) > -1 || fund.assetClass.toLowerCase().indexOf(v) > -1;
        });
        if (matchedValues.length !== searchValues.length) {
          return null;
        }
      }

      if (model && model.title === "Architas"){
        if (architasFunds.includes(fund.fundCode) == false){return null;}
      }

      isDisplayNoAvailableFund = false;

      return (
        <tr key={fund.fundCode}>
        <td>{window.getLocalText(lang, fund.fundName)}</td>
        <td>{fund.ccy}</td>
        <td>{assetClassDesc}</td>
        <td>{riskRatingDesc}</td>
        <td>
          <NumericField
            id="fundAlloc"
            fullWidth
            maxIntDigit={3}
            maxValue={100}
            minValue={0}
            value={fundAllocs[fund.fundCode]}
            onChange={(e, value) => {
              if (selectedFunds[fund.fundCode] || _.filter(selectedFunds, f => f).length < maxSelection) {
                fundAllocs[fund.fundCode] = value;
                selectedFunds[fund.fundCode] = true;
                this.setState({ fundAllocs, selectedFunds });
              } else {
                this.setState({
                  warningMsg: window.getLocalizedText(langMap, 'quotation.fund.warning.maxSelected')
                });
              }
            }}
          />
        </td>
        {
          hasTopUpAlloc ? (
            <NumericField
              id="topUpAlloc"
              fullWidth
              maxIntDigit={3}
              maxValue={100}
              minValue={0}
              value={topUpAllocs[fund.fundCode]}
              onChange={(e, value) => {
                if (selectedFunds[fund.fundCode] || _.filter(selectedFunds, f => f).length < maxSelection) {
                  topUpAllocs[fund.fundCode] = value;
                  selectedFunds[fund.fundCode] = true;
                  this.setState({ topUpAllocs, selectedFunds });
                } else {
                  this.setState({
                    warningMsg: window.getLocalizedText(langMap, 'quotation.fund.warning.maxSelected')
                  });
                }
              }}
            />
          ) : null
        }
        <td>
          <Checkbox
            checked={selectedFunds[fund.fundCode]}
            onCheck={(e, value) => {
              if (!value || _.filter(selectedFunds, f => f).length < maxSelection) {
                selectedFunds[fund.fundCode] = value;
                this.setState({ selectedFunds });
              } else {
                this.setState({
                  warningMsg: window.getLocalizedText(langMap, 'quotation.fund.warning.maxSelected')
                });
              }
            }}
          />
        </td>
      </tr>
      );
    });
    return (
      <div>
      <table className={styles.AvailableFundList + ' ' + styles.QuotTable}>
        <colgroup>
          <col className={styles.FundNameCol} />
          <col className={styles.ColGroupA} />
          <col className={styles.ColGroupB} />
          <col className={styles.ColGroupB} />
          <col className={styles.ColGroupB} />
          {hasTopUpAlloc ? <col className={styles.ColGroupB} /> : null}
          <col className={styles.ColGroupC} />
        </colgroup>
        <thead>
          <tr>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.fundName')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.ccy')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.assetClass')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.riskRating')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.alloc')}</th>
            {hasTopUpAlloc ? <th>{window.getLocalizedText(langMap, 'quotation.fund.list.topUpAlloc')}</th> : null}
            <th />
          </tr>
        </thead>
        <tbody>
          {fundList}
        </tbody>
      </table>
      {isDisplayNoAvailableFund ? <p style= {{textAlign:'center', color:'grey'}}>{window.getLocalizedText(langMap, 'quotation.fund.message.nofundavailabletitle')}</p> : null}
      </div>
    );
  }

  _getSearchRiskOptions() {
    const {optionsMap, langMap, lang} = this.context;
    const {filterRiskRatings, riskProfile} = this.props;
    let options = [];
    options.push(<MenuItem key="*" value={'*'} primaryText={window.getLocalizedText(langMap, 'quotation.fund.availableList.allFunds')} />);
    let riskRatingOptions = _.filter(optionsMap.riskRating.options, (option) => {
      if (filterRiskRatings) {
        return filterRiskRatings.indexOf(option.value) > -1;
      } else if (riskProfile) {
        return option.value <= riskProfile;
      }
      return true;
    });
    _.each(riskRatingOptions, (option) => {
      options.push(
        <MenuItem
          key={'riskRating-' + option.value}
          value={option.value}
          primaryText={window.getLocalText(lang, option.title)}
        />
      );
    });
    return options;
  }

  _hasModelAdjusted(){
    const {model} = this.props;
    if (model == null){return false;}

    let adjustedCount = 0;

    _.each(model.groups, (group) => {
      if (group.percentageNew){
        if (group.percentage !== group.percentageNew){adjustedCount++;}
      }
    });

    if (adjustedCount > 0){return true;}
    return false;
  }

  _getInfoContainer() {
    const {langMap} = this.context;
    const {riskProfile, invOpt, riskDesc} = this.props;
    const {searchRisk, searchText} = this.state;   
    
    let displayPortfolio    = invOpt === 'buildPortfolio'  ? true : false;
    let portfolioText  =  window.getLocalizedText(langMap, 'quotation.fund.info.portfolio');
    if (riskDesc === ''){
      portfolioText += ' ' + window.getLocalizedText(langMap, 'quotation.fund.riskProfile.noRestrict');
    }else if (riskDesc){
        portfolioText += ' ' + riskDesc;
    }

    return (
      <div className={styles.InfoContainer}>
        <div className={styles.InfoRow}>
          {riskProfile ? (
            <div className={styles.InfoItem}>
              <div className={styles.InfoTitle}>{riskProfile ? window.getLocalizedText(langMap, 'quotation.fund.info.assessedRiskProfile') : null}</div>
              <div className={styles.InfoValue}>{riskProfile ? window.getLocalizedText(langMap, 'quotation.fund.riskProfile.' + riskProfile) : null}</div>
            </div>
          ) : null}
          <div className={styles.InfoItem}>
            <div className={styles.InfoTitle}>{window.getLocalizedText(langMap, 'quotation.fund.info.chooseInvOpt')}</div>
            <div className={styles.InfoValue}>{window.getLocalizedText(langMap, 'quotation.fund.invOpt.' + invOpt)}</div>
          </div>
        </div>

        <div className={styles.InfoItem}>
          <div className={styles.InfoTitle}>{window.getLocalizedText(langMap, 'quotation.fund.info.riskRating')}</div>
          <div className={styles.InfoValue}>
            <SelectField
              value={searchRisk || '*'}
              onChange={(e, index, value) => {
                console.log('value searchRisk' + value);
                this.setState({ searchRisk: value });
              }}
            >
              {this._getSearchRiskOptions()}
            </SelectField>
          </div>
        </div>

        <div className={styles.InfoItem}>
          <div className={styles.InfoTitle}>{window.getLocalizedText(langMap, 'quotation.fund.info.search')}</div>
          <div className={styles.InfoValue}>
            <TextField
              id="fundSearchText"
              hintText={window.getLocalizedText(langMap, 'quotation.fund.availableList.searchHint')}
              fullWidth
              value={searchText}
              onChange={(e, value) => {
                this.setState({ searchText: value });
              }}
            />
          </div>
        </div>

        {displayPortfolio ? (
          <div className={styles.InfoItem}>
            {portfolioText}
          </div>
        ) : null}
      </div>
    );
  }

  render() {
    const {langMap} = this.context;
    const {onClose, riskProfile} = this.props;
    const {warningMsg} = this.state;
    return (
      <SaveDialog
        open={this.props.open}
        title={window.getLocalizedText(langMap, 'quotation.fund.availableList.title')}
        className={styles.FundDialog}
        bodyClassName={styles.FundDialogSize}
        bodyStyle={{height: '100% !important', minHeight:'1200px !important' }}
        onSave={() => this._onSave()}
        onClose={onClose}
      >
        <div style={{ minWidth: 880, padding: 24 }}>
          {this._getInfoContainer()}
          {this._getFundList()}

          {riskProfile ? (
            <div className={styles.RiskNote}>{window.getLocalizedText(langMap, 'quotation.fund.list.riskNote')}</div>
          ) : null}
          <WarningDialog
            open={!!warningMsg}
            msg={warningMsg}
            onClose={() => this.setState({ warningMsg: null })}
          />
        </div>
      </SaveDialog>
    );
  }

}
