import React, { PropTypes} from 'react';
import { IconButton, RaisedButton, SelectField, MenuItem } from 'material-ui';
import * as _ from 'lodash';

import EABComponent from '../../../Component';
import NumericField from '../../../CustomViews/NumericField';
import {getIcon} from '../../../Icons/index';
import AvailableFundDialog from './AvailableFundDialog';
import AdjustFundDialog from './AdjustFundDialog';
import ConfirmDialog from '../../../Dialogs/ConfirmDialog';
import TableDialog from '../../../Dialogs/TableDialog';
import SaveDialog from '../../../Dialogs/SaveDialog';

import styles from '../../../Common.scss';

import { queryFunds, allocFunds } from '../../../../actions/quotation';

export default class FundSelectionDialog extends EABComponent {

  static propTypes = {
    quotation: PropTypes.object,
    availableFunds: PropTypes.array,
    hasTopUpAlloc: PropTypes.bool, //has user select top-up option
    showTopUpAlloc: PropTypes.bool //is product has top up option or not
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    handleConfigChange: PropTypes.func
  });

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      open: false,
      openAvailableList: false,
      confirmMsg: null,
      onConfirm: null,
      openDisclaimer: null,
      onCloseDisclaimer: null,
      riskProfile: null,
      invOpt: null,
      portfolio: null,
      initAvailable: true,
      model: null,
      fundAllocs: {},
      topUpAllocs: {}
    });
  }

  openDialog = () => {
    const {quotation, hasTopUpAlloc} = this.props;
    let {invOpt = 'mixedAssets', portfolio, funds} = quotation.fund || {};
    let fundAllocs = {};
    let topUpAllocs = {};
    let model = null;
    let displayAdjust = false;
    let displayReset = false;

    let getAdjustedModel = false;

    if (quotation.fund && quotation.fund.adjustedModel){
      if (invOpt === 'buildPortfolio'){
        model = _.cloneDeep(quotation.fund.adjustedModel);
        displayAdjust = this._shouldDisplayAdjustButton(quotation.fund.adjustedModel)
        displayReset = true;
        getAdjustedModel = true;
      }
    }
    _.each(funds, (fund) => {
      fundAllocs[fund.fundCode] = fund.alloc;
      if (hasTopUpAlloc){
        topUpAllocs[fund.fundCode] = fund.topUpAlloc;
      }
    });
    queryFunds(this.context, invOpt, () => {
      if (getAdjustedModel){
        this.setState({
          open: true,
          riskProfile: quotation.extraFlags.fna && quotation.extraFlags.fna.riskProfile,
          invOpt: invOpt,
          portfolio: portfolio,
          fundAllocs: fundAllocs,
          topUpAllocs,
          model: model,
          displayAdjust,
          displayReset
        });
      }
      else {
        let newState = {
          open: true,
          riskProfile: quotation.extraFlags.fna && quotation.extraFlags.fna.riskProfile,
          invOpt: invOpt,
          portfolio: portfolio,
          fundAllocs: fundAllocs,
          topUpAllocs
        };
        if (quotation.fund === null) {
          newState = Object.assign(newState, {
            displayAdjust: false,
            displayReset: false,
            model: null
          });
        }
        this.setState(newState);
      }
    });
  };

  closeDialog = () => {
    this.setState({
      open: false,
      invOpt: '',
      fundAllocs: {},
      topUpAllocs: {},
      model: null,
      displayAdjust: false,
      displayReset: false
    });
  };

  saveFunds() {
    const {quotation, hasTopUpAlloc} = this.props;
    const {invOpt, portfolio, fundAllocs, topUpAllocs,model} = this.state;
    allocFunds(this.context, quotation, invOpt, portfolio, fundAllocs, hasTopUpAlloc, topUpAllocs, model, () => {
      this.setState({ open: false });
    });
  }

  _getTotalFundAlloc() {
    return _.reduce(this.state.fundAllocs, (sum, alloc) => sum + (alloc || 0), 0);
  }

  _getTotalTopUpAlloc() {
    return _.reduce(this.state.topUpAllocs, (sum, alloc) => sum + (alloc || 0), 0);
  }

  _hasValidPortfolio() {
    const {hasTopUpAlloc} = this.props;
    const {invOpt, fundAllocs, topUpAllocs} = this.state;
    if (invOpt === 'buildPortfolio') {
      let model = this._getCurrentPortfolioModel(true);
      if (model) {
        let selectedFunds = this._getSelectedFunds();
        return !_.find(model.groups, (group) => {
          let funds = _.filter(selectedFunds, f => group.riskRatings.indexOf(f.riskRating) > -1);
          let groupTotal = _.reduce(funds, (sum, fund) => sum + (fundAllocs[fund.fundCode] || 0), 0);
          let groupTopUpTotal = _.reduce(funds, (sum, fund) => sum + (topUpAllocs[fund.fundCode] || 0), 0);
          let percentage  = _.isNumber(group.percentageNew) ? group.percentageNew : group.percentage;
          return groupTotal !== percentage || (hasTopUpAlloc && groupTopUpTotal !== percentage);
        });
      }
      return false;
    }
    return true;
  }

  _changeInvOpt(invOpt) {
    const {model, displayAdjust} = this.state;
    if (invOpt === 'buildPortfolio') {
      if (model != null){
        this.setState({
          displayAdjust: this._shouldDisplayAdjustButton(model) ,
          openDisclaimer: true,
          onCloseDisclaimer: () => this._confirmChangeInvOpt(invOpt)
        });
      }
      else {
        this.setState({
          openDisclaimer: true,
          onCloseDisclaimer: () => this._confirmChangeInvOpt(invOpt)
        });
      }
    } else {
      if (displayAdjust === true) {
        this.setState({
          displayAdjust: false
        });
      }
      this._confirmChangeInvOpt(invOpt);
    }
  }

  _confirmChangeInvOpt(invOpt) {
    const {langMap} = this.context;
    const {hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    if (!_.isEmpty(fundAllocs) || (hasTopUpAlloc && !_.isEmpty(topUpAllocs))) {
      this.setState({
        confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmCleanList'),
        onConfirm: () => {
          this.setState({
            confirmMsg: null,
            fundAllocs: {},
            topUpAllocs: {},
            invOpt: invOpt,
            model: null,
            portfolio: null,
            initAvailable: true
          }, () => {
            queryFunds(this.context, invOpt);
          });
        }
      });
    } else {
      this.setState({ 
        invOpt,
        model: null,
        portfolio: null 
      }, () => queryFunds(this.context, invOpt));
    }
  }

  _confirmResetModelAdjusted() {
    const {langMap, optionsMap} = this.context;
    const {model, portfolio} = this.state;

    this.setState({
      confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmCleanList'),
      onConfirm: () => {
        let totalGroups = model.groups.length;
        let index = 0;
        let curMax = 0;
        let curModel;
        _.each(optionsMap.portfolioModels.portfolios, p => {
          _.each(p.models, model => {
            if (model.id === portfolio) {
              curModel = model;
            }
          });
        });
        _.each(model.groups, (group) => {
          let min = 0;
          let max = 0;

          // Copy preset precentage
          group.percentageNew = group.percentage;
          curMax += group.percentage;

          if (index == 0){max = group.percentage;}

          max = curMax;

          if (index == (totalGroups - 1)){
            min = group.percentage;
            max = 100;
          }

          group.min = min;
          group.max = max;

          index++;
        });

        this.setState({
          confirmMsg:     null,
          fundAllocs:     {},
          topUpAllocs:    {},
          portfolio:      portfolio,
          initAvailable: true,
          model: curModel
        });
      }
    });
  }

  _closeAdjustFundDialog(){
    this.setState({ openAdjustDialog: false });
  }

  _confirmAdjustFundAllocation() {
    const {langMap} = this.context;
    const {hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    if (!_.isEmpty(fundAllocs) || (hasTopUpAlloc && !_.isEmpty(topUpAllocs))) {
      this.setState({
        confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmCleanList'),
        onConfirm: () => {
          this.setState({
            confirmMsg: null,
            fundAllocs: {},
            topUpAllocs: {},
            initAvailable: true
          });
        }
      });
    } else {
      this.setState({ openAdjustDialog: true });
    }
  }

  _changePortfolio(portfolio) {
    const {langMap, optionsMap} = this.context;
    const {hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs, riskProfile} = this.state;

    let portfolioOptions = _.cloneDeep(_.filter(optionsMap.portfolioModels.portfolios, p => p.riskProfile <= riskProfile));
    let foundModel  = null;

    _.each(portfolioOptions, (portfolioOption) => {
      _.each(portfolioOption.models, (model) => {
        if (model.id === portfolio){foundModel = model; return false;}
      });
      if (foundModel != null){return false;}
    });

    let displayAdjust = this._shouldDisplayAdjustButton(foundModel);

    if (!_.isEmpty(fundAllocs) || (hasTopUpAlloc && !_.isEmpty(topUpAllocs))) {
      this.setState({
        confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmCleanList'),
        onConfirm: () => {
          this.setState({
            fundAllocs: {},
            topUpAllocs: {},
            portfolio: portfolio,
            displayAdjust: displayAdjust ,
            initAvailable: true,
            model: foundModel
          });
        }
      });
    } else {
      this.setState({
        fundAllocs: {},
        topUpAllocs: {},
        portfolio: portfolio,
        displayAdjust: displayAdjust ,
        model: foundModel
      });
    }
  }

  _confirmDeleteAllTopUp(){
    const {langMap} = this.context;
    const {topUpAllocs, fundAllocs} = this.state;
    if (!_.isEmpty(topUpAllocs)) {
      let newTopUpAlloc = {};
      for (var fundCode in fundAllocs){
        newTopUpAlloc[fundCode] = 0;
      }
      this.setState({
        confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmDeleteAllTopUp'),
        onConfirm: () => {
          this.setState({
            confirmMsg: null,
            topUpAllocs: newTopUpAlloc
          });
        }
      });
    }
  }

  _confirmDeleteAll() {
    const {langMap} = this.context;
    const {hasTopUpAlloc, showTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    if (!_.isEmpty(fundAllocs)) {
      let newFundAllocs = {};
      if (showTopUpAlloc && hasTopUpAlloc) {
        for (var fundCode in topUpAllocs) {
          newFundAllocs[fundCode] = 0;
        }
      }
      this.setState({
        confirmMsg: window.getLocalizedText(langMap, showTopUpAlloc && hasTopUpAlloc? 'quotation.fund.list.confirmDeleteAllRp': 'quotation.fund.list.confirmDeleteAll'),
        onConfirm: () => {
          this.setState({
            confirmMsg: null,
            fundAllocs: newFundAllocs
          });
        }
      });
    }
  }

  _confirmDeleteGroup(funds) {
    const {langMap} = this.context;
    const {hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    if (!_.isEmpty(funds) || (hasTopUpAlloc && !_.isEmpty(topUpAllocs))) {
      this.setState({
        confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmDeleteGroup'),
        onConfirm: () => {
          let updatedFunds = {}, updatedTopUps = {};
          _.each(fundAllocs, (alloc, fundCode) => {
            if (!_.find(funds, f => f.fundCode === fundCode)) {
              updatedFunds[fundCode] = alloc;
            }
          });
          _.each(topUpAllocs, (alloc, fundCode) => {
            if (!_.find(funds, f => f.fundCode === fundCode)) {
              updatedTopUps[fundCode] = alloc;
            }
          });
          this.setState({
            confirmMsg: null,
            fundAllocs: updatedFunds,
            topUpAllocs: updatedTopUps
          });
        }
      });
    }
  }

  _confirmDelete(fund) {
    const {langMap} = this.context;
    const {fundAllocs, topUpAllocs} = this.state;
    this.setState({
      confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmDelete'),
      onConfirm: () => {
        let updatedFunds = {};
        let updatedTopUps = {};
        _.each(fundAllocs, (alloc, fundCode) => {
          if (fundCode !== fund.fundCode) {
            updatedFunds[fundCode] = alloc;
          }
        });
        _.each(topUpAllocs, (alloc, fundCode) => {
          if (fundCode !== fund.fundCode) {
            updatedTopUps[fundCode] = alloc;
          }
        });
        this.setState({
          confirmMsg: null,
          fundAllocs: updatedFunds,
          topUpAllocs: updatedTopUps
        });
      }
    });
  }

  _updateSelectedFunds(selectedFunds) {
    this.setState({
      fundAllocs: selectedFunds.fundAllocs,
      topUpAllocs: selectedFunds.topUpAllocs,
      openAvailableList: false
    });
  }

  _updateSelectedModel(selectedModel){
    this.setState({
      model: selectedModel,
      openAdjustDialog: false
    });
  }

  _getFundGroupDesc(group, useAdjustedPercentage) {
    const {optionsMap, lang} = this.context;
    let riskRatingsDesc = _.join(_.map(group.riskRatings, rr => window.getOptionTitle(rr, optionsMap.riskRating, lang)), ' / ');
    let percentage = group.percentage;
    if (useAdjustedPercentage){
      percentage = (group.percentageNew != null ? group.percentageNew : group.percentage);
    }
    return percentage + '% ' + riskRatingsDesc;
  }

  _getPortfolioModelDesc(riskProfile, modelTitle, groups) {
    const {lang, langMap} = this.context;
    let riskProfileDesc = window.getLocalizedText(langMap, 'quotation.fund.riskProfile.' + riskProfile);
    let modelDesc = window.getLocalText(lang, modelTitle);
    let riskDesc = _.join(_.map(groups, (group) => this._getFundGroupDesc(group)), ', ');
    return riskProfileDesc + ' | ' + modelDesc + ' (' + riskDesc + ')';
  }

  _getPortfolioModelDescByModelId(){
    const {lang, langMap, optionsMap} = this.context;
    const {riskProfile, portfolio} = this.state;
    let portfolioOptions = _.filter(optionsMap.portfolioModels.portfolios, p => p.riskProfile <= riskProfile);

    let modelDesc = null;

    _.each(portfolioOptions, (portfolioOption) => {
      _.each(portfolioOption.models, (model) => {
        if (model.id == portfolio){
          modelDesc = this._getPortfolioModelDesc(portfolioOption.riskProfile, model.title, model.groups);
          return false;
        }
      });
      if (modelDesc != null){return false;}
    });

    if (modelDesc != null){return modelDesc;}
    return '';
  }

_getPortfolioList() {
    const {lang, langMap, optionsMap} = this.context;
    const {quotation} = this.props;

    const {riskProfile, portfolio} = this.state;
    let portfolioOptions = _.cloneDeep(_.filter(optionsMap.portfolioModels.portfolios, p => p.riskProfile <= riskProfile));

    let menuItems = [];

    if (portfolioOptions) {
      _.each(portfolioOptions, (portfolioOption) => {
        _.each(portfolioOption.models, (model) => {
          let includeModel = false;
          if (model.products !== undefined){// check for product -specific models
            let curProduct;
            let index;
            for (index = 0; index < model.products.length; index++){
              curProduct = model.products[index];
              if (curProduct === quotation.baseProductCode){
                includeModel = true; break;
              }
            }
          }
          else{
            includeModel = true;
          }

          if (includeModel == true){
            menuItems.push(
              <MenuItem
                key={model.id}
                value={model.id}
                primaryText={model.any ? window.getLocalText(lang, model.title) : this._getPortfolioModelDesc(portfolioOption.riskProfile, model.title, model.groups)}
              />
            );
          }
        });
      });
    }
    return (
      <SelectField
        autoWidth
        value={portfolio}
        errorText={!portfolio && window.getLocalizedText(langMap, 'form.err.required')}
        onChange={(e, index, value) => {
          this._changePortfolio(value);
        }}
      >
        {menuItems}
      </SelectField>
    );
  }

  _getFundListItem(fund) {
    const {lang, optionsMap} = this.context;
    const {hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    let fundName = window.getLocalText(lang, fund.fundName);
    let assetClassDesc = window.getOptionTitle(fund.assetClass, optionsMap.assetClass, lang);
    let riskRatingDesc = window.getOptionTitle(fund.riskRating, optionsMap.riskRating, lang);
    return (
      <tr key={fund.fundCode}>
        <td>{fundName}</td>
        <td>{fund.ccy}</td>
        <td>{assetClassDesc}</td>
        <td>{riskRatingDesc}</td>
        <td>
          <NumericField
            id="fundAlloc"
            fullWidth
            maxIntDigit={3}
            maxValue={100}
            minValue={0}
            value={fundAllocs[fund.fundCode]}
            onChange={(e, value) => {
              fundAllocs[fund.fundCode] = value;
            }}
            onBlur={() => {
              this.setState({});
            }}
          />
        </td>
        {hasTopUpAlloc ? (
          <td>
            <NumericField
              id="topUpAlloc"
              fullWidth
              maxIntDigit={3}
              maxValue={100}
              minValue={0}
              value={topUpAllocs[fund.fundCode]}
              onChange={(e, value) => {
                topUpAllocs[fund.fundCode] = value;
              }}
              onBlur={() => {
                this.setState({});
              }}
            />
          </td>
         ) : null
        }
        <td>
          <IconButton onTouchTap={() => this._confirmDelete(fund)}>
            {getIcon('removeCircle', 'red')}
          </IconButton>
        </td>
      </tr>
    );
  }

  _getSelectedFunds() {
    const {availableFunds, hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    return _.filter(availableFunds, f => fundAllocs[f.fundCode] !== undefined || (hasTopUpAlloc && topUpAllocs[f.fundCode] !== undefined));
  }

  _getCurrentPortfolioModel(useModelAdjusted) {
    const {optionsMap} = this.context;
    let {portfolio, model} = this.state;
    let curModel;

    if (useModelAdjusted && useModelAdjusted === true){
      if (model !== null){
        return _.cloneDeep(model);;
      }
    }
    _.each(optionsMap.portfolioModels.portfolios, p => {
      _.each(p.models, m => {
        if (m.id === portfolio) {
          curModel = m;
        }
      });
    });
    return _.cloneDeep(curModel);
  }

  _getFundListItemsWithGrouping() {
    const {langMap} = this.context;
    const {hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    let model = this._getCurrentPortfolioModel(true);

    if (model) {
      if (model.any) {
        return this._getFundListItems();
      }
      let items = [];
      let selectedFunds = this._getSelectedFunds();
      _.each(model.groups, (group, index) => {
        let funds           = _.filter(selectedFunds, f => group.riskRatings.indexOf(f.riskRating) > -1);
        let groupTotal      = _.reduce(funds, (sum, fund) => sum + (fundAllocs[fund.fundCode] || 0), 0);
        let groupTopUpTotal = _.reduce(funds, (sum, fund) => sum + (topUpAllocs[fund.fundCode] || 0), 0);

        let percentage      = _.isNumber(group.percentageNew) ? group.percentageNew : group.percentage;

        if (percentage > 0) {
          items.push(
            <tr key={'group-' + index} className={styles.GroupHeader}>
              <td colSpan="3">{this._getFundGroupDesc(group, true)}</td>

              <td colSpan="2">
                <div style={{ display: 'flex', minHeight: '0px'}}>
                  <div style={{textAlign: 'right', width: '40%'}}>{window.getLocalizedText(langMap, 'quotation.fund.list.percentage')}</div>
                  {groupTotal != percentage ?<div style={{textAlign: 'right', color:'red', width: '40%'}}>{groupTotal}</div>:
                <div style={{textAlign: 'right', color:'black', width: '40%'}}>{groupTotal}</div>}
                </div>
                {groupTotal != percentage ?
                <div style={{textAlign: 'right', width: '80%'}}>
                   <label style={{ flex: 1, textAlign: 'right', color: 'red' }}>
                   {window.getLocalizedText(langMap, 'quotation.fund.info.totalAllocNeeds') + percentage + '%'}
                    </label>
                </div>: null}
              </td>
              {hasTopUpAlloc ? <td/ > : null}

              <td>
                <IconButton onTouchTap={() => this._confirmDeleteGroup(funds)}>
                  {getIcon('removeCircle', 'red')}
                </IconButton>
              </td>
            </tr>
          );
        }
        _.each(funds, (fund) => {
          items.push(this._getFundListItem(fund));
        });
      });
      return items;
    }
  }

  _getFundListItems() {
    return _.map(this._getSelectedFunds(), fund => this._getFundListItem(fund));
  }

  _getFundList() {
    const {langMap} = this.context;
    const {hasTopUpAlloc, showTopUpAlloc} = this.props;
    return (
      <table className={styles.SelectedFundList + ' ' + styles.QuotTable}>
        <colgroup>
          <col className={styles.FundNameCol} />
          <col className={styles.ColGroupA} />
          <col className={styles.ColGroupB} />
          <col className={styles.ColGroupB} />
          <col className={styles.ColGroupB} />
          {hasTopUpAlloc? <col className={styles.ColGroupB} />: null}
          <col className={styles.ColGroupC} />
        </colgroup>
        <thead>
          <tr>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.fundName')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.ccy')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.assetClass')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.riskRating')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.alloc')}</th>
            {hasTopUpAlloc? <th>{window.getLocalizedText(langMap, 'quotation.fund.list.topUpAlloc')}</th>: null}
            <th />
          </tr>
        </thead>
        <tbody>
          {this.state.invOpt === 'buildPortfolio' ? this._getFundListItemsWithGrouping() : this._getFundListItems()}
        </tbody>
      </table>
    );
  }

  _hasModelAdjusted(){
    const {model} = this.state;
    if (model == null){return false;}

    let adjustedCount = 0;

    _.each(model.groups, (group) => {
      if (group.percentageNew){
        if (group.percentage !== group.percentageNew){adjustedCount++;}
      }
    });

    if (adjustedCount > 0){return true;}
    return false;
  }

  _shouldDisplayAdjustButton(model) {
    if (_.has(model, 'notDisplayAdjustButton')) {
      if (model.notDisplayAdjustButton) {
        return false;
      }
    }
    return true;
  }

  _getInfoContainer() {
    const {langMap} = this.context;
    const {hasTopUpAlloc, showTopUpAlloc} = this.props;
    const {riskProfile, invOpt} = this.state;
    let totalAlloc      = this._getTotalFundAlloc();

    let totalTopUpAlloc = this._getTotalTopUpAlloc();
    let disableSave     = totalAlloc !== 100 || (hasTopUpAlloc && totalTopUpAlloc !== 100) || !this._hasValidPortfolio();
    let {displayAdjust} = this.state;
    let displayReset    = this._hasModelAdjusted();

    return (
      <div className={styles.InfoContainer}>
        {riskProfile ? (
          <div className={styles.InfoItem}>
            <div className={styles.InfoTitle}>{window.getLocalizedText(langMap, 'quotation.fund.info.assessedRiskProfile')}</div>
            <div className={styles.InfoValue}>{window.getLocalizedText(langMap, 'quotation.fund.riskProfile.' + riskProfile)}</div>
          </div>
        ) : null}

        <div className={styles.InfoItem}>
          <div className={styles.InfoTitle}>{window.getLocalizedText(langMap, 'quotation.fund.info.chooseInvOpt')}</div>
          <div className={styles.InfoValue}>
            <SelectField
              value={invOpt}
              onChange={(e, index, value) => {
                this._changeInvOpt(value);
              }}
            >
              <MenuItem value={'mixedAssets'} primaryText={window.getLocalizedText(langMap, 'quotation.fund.invOpt.mixedAssets')} />
              {riskProfile ? <MenuItem value={'buildPortfolio'} primaryText={window.getLocalizedText(langMap, 'quotation.fund.invOpt.buildPortfolio')} /> : null}
              <MenuItem value={'optimizeToProfile'} primaryText={window.getLocalizedText(langMap, 'quotation.fund.invOpt.optimizeToProfile')} />
            </SelectField>
          </div>
        </div>

        {riskProfile ? (
          <div className={styles.InfoItem}>
            <div className={styles.InfoTitle}>{invOpt === 'buildPortfolio' ? window.getLocalizedText(langMap, 'quotation.fund.info.portfolio') : ''}</div>
            <div className={styles.InfoValue}>{invOpt === 'buildPortfolio' ? this._getPortfolioList() : null}</div>
          </div>
        ) : null}

        <div className={styles.InfoItem}>
          <div className={styles.InfoWithMessage}>
            <div className={styles.InfoRow}>
              <div className={styles.InfoTitle}>{window.getLocalizedText(langMap, showTopUpAlloc? 'quotation.fund.info.totalPremiumAlloc': 'quotation.fund.info.totalAlloc')}</div>
              <div className={styles.InfoValue} style={{ display: 'flex', minHeight: '0px', alignItems: 'center' }}>
                <label style={{ flex: 1, textAlign: 'center', color: totalAlloc !== 100 ? 'red' : null }}>{totalAlloc + '%'}</label>
                <IconButton onTouchTap={() => this._confirmDeleteAll()}>
                  {getIcon('removeCircle', 'red')}
                </IconButton>
              </div>
            </div>

            <div className={styles.InfoRow}>
              <div className={styles.InfoTitle}></div>
              <div className={styles.InfoValue}>
                {disableSave ? (
                  <label style={{ flex: 1, textAlign: 'left', color: 'red' }}>
                    Total fund allocation is not equal to 100%
                  </label>
                ) : null}
              </div>
            </div>
          </div>
        </div>

        {hasTopUpAlloc ? (
          <div className={styles.InfoItem}/>
        ): null}

        {hasTopUpAlloc ? (
          <div className={styles.InfoItem}>
            <div className={styles.InfoTitle}>{window.getLocalizedText(langMap, 'quotation.fund.info.totalTopUpAlloc')}</div>
            <div className={styles.InfoValue} style={{ display: 'flex', minHeight: '0px', alignItems: 'center' }}>
              <label style={{ flex: 1, textAlign: 'center', color: totalAlloc !== 100 ? 'red' : null }}>{totalTopUpAlloc + '%'}</label>
              <IconButton onTouchTap={() => this._confirmDeleteAllTopUp()}>
                {getIcon('removeCircle', 'red')}
              </IconButton>
            </div>
          </div>
        ) : null}

        {displayAdjust ? (
          <div className={styles.InfoItem}>
            <div className={styles.BtnContainer}
                style={{ marginRight: 20}}>
              <RaisedButton
                primary
                label={window.getLocalizedText(langMap, 'quotation.fund.btn.adjustFund')}
                onTouchTap={() => this._confirmAdjustFundAllocation()}
              />
            </div>

            {displayReset ? (
              <div className={styles.BtnContainer}>
                <RaisedButton
                  primary
                  label={window.getLocalizedText(langMap, 'quotation.fund.btn.resetFund')}
                  onTouchTap={() => this._confirmResetModelAdjusted()}
                />
              </div>) : null}
          </div>


        ) : null}

        {displayAdjust ? (<div className={styles.InfoItem}/>) : null}

        {displayAdjust && displayReset ? (
          <div style={{width: '100%', paddingTop: 10, paddingBottom:10}}>
            <b>
              <label style={{ flex: 1, color: 'red' }}>
              {window.getLocalizedText(langMap, 'quotation.fund.info.adjusted')}<br/>
              {window.getLocalizedText(langMap, 'quotation.fund.info.beforeAdjusted')}
              {this._getPortfolioModelDescByModelId()}
              </label>
            </b>
          </div>
        ) : null}
      </div>
    );
  }

  render() {
    const {optionsMap,langMap} = this.context;
    const {availableFunds, hasTopUpAlloc, showTopUpAlloc} = this.props;
    const { openAvailableList,
            openAdjustDialog,
            model,
            confirmMsg,
            onConfirm,
            openDisclaimer,
            onCloseDisclaimer,
            riskProfile,
            invOpt,
            initAvailable,
            fundAllocs,
            topUpAllocs} = this.state;

    let portfolioModel = this._getCurrentPortfolioModel(true);
    let filterRiskRatings = [1, 2, 3, 4, 5];
    let riskDesc = '';
    if (model != null){
      let cloneGroups = _.cloneDeep(model.groups);
      let filterGroup = _.filter(cloneGroups, function(group) {
        if (_.isNumber(group.percentageNew)) {
            if (group.percentageNew !== 0) {
               return true;
            }
            return false;
        } else {
            if (group.percentage !== 0) {
              return true;
            }
            return false;
        }
      });
      riskDesc = _.join(_.map(filterGroup, (group) => this._getFundGroupDesc(group, true)), ', ');
    }

    if (invOpt === 'buildPortfolio' && portfolioModel) {
      if (portfolioModel.any) {
        filterRiskRatings = [1, 2, 3, 4, 5];
      } else {
        filterRiskRatings = [];
        _.each(portfolioModel.groups, (group) => {
          if (_.isNumber(group.percentageNew)) {
            if (group.percentageNew !== 0) {
              filterRiskRatings = filterRiskRatings.concat(group.riskRatings);
            }
          } else {
            if (group.percentage !== 0) {
              filterRiskRatings = filterRiskRatings.concat(group.riskRatings);
            }
          }
        });
      }
    } else if (invOpt !== 'buildPortfolio') {
      filterRiskRatings = [1, 2, 3, 4, 5].filter(riskRating => riskProfile >= riskRating);
    }

    let architasFunds = optionsMap.portfolioModels.architasFunds;
    let adjustFundRiskRatings = [];
    if (model && model.title === "Architas"){
      adjustFundRiskRatings = _.map(availableFunds, (fund) => {
        if (architasFunds.includes(fund.fundCode)){
          return fund.riskRating;
        }
      });
    } else {
      adjustFundRiskRatings = [1, 2, 3, 4, 5];
    }

    return (
      <SaveDialog
        open          ={this.state.open}
        title         ={window.getLocalizedText(langMap, 'quotation.fund.fundSelection.title')}
        disableSave   ={this._getTotalFundAlloc() !== 100 || (hasTopUpAlloc && this._getTotalTopUpAlloc() !== 100) || !this._hasValidPortfolio()}
        onSave        ={() => this.saveFunds()}
        onClose       ={() => this.closeDialog()}
        className     ={styles.FundDialog}
        bodyClassName ={styles.FundDialogSize}
        bodyStyle     ={{height: '100% !important', minHeight:'1200px !important' }}
      >
        <div style={{ minWidth: 880, padding: 24 }}>
          {this._getInfoContainer()}
          {this._getFundList()}
          {this._getSelectedFunds().length == 0 ? (<p style={{ textAlign: 'center', color:'grey' }}>{window.getLocalizedText(langMap, 'quotation.fund.message.nofundtitle')}</p>) : null}

          <div className={styles.BtnContainer}>
            <RaisedButton
              primary
              disabled={invOpt === 'buildPortfolio' && !portfolioModel}
              label={window.getLocalizedText(langMap, 'quotation.fund.btn.addFund')}
              onTouchTap={() => this.setState({ openAvailableList: true })}
            />
          </div>

          {riskProfile ? (
            <div className={styles.RiskNote}>{window.getLocalizedText(langMap, 'quotation.fund.list.riskNote')}</div>
          ) : null}

          <AvailableFundDialog
            open            ={openAvailableList}
            init            ={initAvailable}
            onClose         ={() => this.setState({ openAvailableList: false, initAvailable: false })}
            onSave          ={(selectedFunds) => this._updateSelectedFunds(selectedFunds)}
            riskProfile     ={riskProfile}
            invOpt          ={invOpt}
            model           ={model}
            riskDesc        ={riskDesc}
            availableFunds  ={availableFunds}
            filterRiskRatings={portfolioModel && filterRiskRatings}
            fundAllocs      ={fundAllocs}
            topUpAllocs     ={topUpAllocs}
            hasTopUpAlloc   ={hasTopUpAlloc}
            showTopUpAlloc  ={showTopUpAlloc}
          />

          <AdjustFundDialog
            open            ={openAdjustDialog}
            onClose         ={() => this._closeAdjustFundDialog()}
            onSave          ={(selectedModel) => this._updateSelectedModel(selectedModel)}
            model           ={_.cloneDeep(model)}
            adjustFundRiskRatings  ={adjustFundRiskRatings}
          />

          <ConfirmDialog
            open            ={!!confirmMsg}
            confirmMsg      ={confirmMsg}
            onClose         ={() => this.setState({ confirmMsg: null })}
            onConfirm       ={() => {
              onConfirm && onConfirm();
              this.setState({ confirmMsg: null });
            }}
          />

          <TableDialog
            open            ={!!openDisclaimer}
            className       ={styles.FundDialog}
            onClose         ={() => {
              this.setState({
                openDisclaimer: false,
                onCloseDisclaimer: null
              }, () => onCloseDisclaimer && onCloseDisclaimer());
            }}
          />
        </div>
      </SaveDialog>
    );
  }

}
