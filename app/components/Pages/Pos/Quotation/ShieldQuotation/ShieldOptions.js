import React, {PropTypes} from 'react';
import * as _ from 'lodash';
import {Paper, SelectField, MenuItem} from 'material-ui';

import EABComponent from '../../../../Component';
import NumericField from '../../../../CustomViews/NumericField';

export default class PlanConfig extends EABComponent {

  static propTypes = {
    style: PropTypes.object,
    quotation: PropTypes.object,
    planDetails: PropTypes.object
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    handleConfigChange: PropTypes.func
  });

  _getCcyOptions() {
    const {quotation, planDetails} = this.props;
    const {optionsMap, lang, store} = this.context;
    const agentProfile = store.getState().app.agentProfile;

    let basicPlan = planDetails[quotation.baseProductCode];
    let options = [];

    if (basicPlan && basicPlan.currencies) {
      let ccyOptions = null;
      _.each(optionsMap.ccy.currencies, (currency) => {
        if (currency.compCode === agentProfile.compCode) {
          ccyOptions = currency.options;
        }
      });
      _.each(basicPlan.currencies, (currency) => {
        let ccy = _.find(ccyOptions, (ccyOption) => ccyOption.value === currency.ccy[0]);
        if (ccy) {
          options.push(
            <MenuItem
              key={ccy.value}
              value={ccy.value}
              primaryText={window.getLocalText(lang, ccy.title)}
            />
          );
        }
      });
    }
    return options;
  }

  getConfigFields() {
    const {handleConfigChange, langMap, optionsMap} = this.context;
    const {quotation} = this.props;
    const sign = window.getCurrencySign(quotation.compCode, quotation.ccy, optionsMap);
    let configFields = [];
    configFields.push(
      <SelectField
        id={'ccy'}
        key={'ccy'}
        disabled
        fullWidth
        value={quotation.ccy}
        onChange={(e, index, value) => {
          quotation.ccy = value;
          handleConfigChange();
        }}
        floatingLabelText={window.getLocalizedText(langMap, 'quotation.currency')}
        underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
        style={{ verticalAlign: 'bottom' }}
        labelStyle={{ color: 'black' }}
      >
        {this._getCcyOptions()}
      </SelectField>
    );
    configFields.push(
      <NumericField
        id={'medishield'}
        key={'medishield'}
        disabled
        fullWidth
        sign={sign}
        decimalPlace={2}
        value={quotation.policyOptions.medishieldLife}
        floatingLabelText = {window.getLocalizedText(langMap, 'quotation.plans.mediShieldLife')}
      />
    );
    configFields.push(
      <NumericField
        id={'axaShield'}
        key={'axaShield'}
        disabled
        fullWidth
        sign={sign}
        decimalPlace={2}
        value={quotation.policyOptions.axaShield}
        floatingLabelText = {window.getLocalizedText(langMap, 'quotation.plans.axaShield')}
      />
    );
    configFields.push(
      <NumericField
        id={'awl'}
        key={'awl'}
        disabled
        fullWidth
        sign={sign}
        decimalPlace={2}
        value={quotation.policyOptions.awl}
        floatingLabelText={window.getLocalizedText(langMap, 'quotation.plans.awl')}
      />
    );
    configFields.push(
      <NumericField
        id={'cashPortion'}
        key={'cashPortion'}
        disabled
        fullWidth
        sign={sign}
        decimalPlace={2}
        value={quotation.policyOptions.cashPortion}
        floatingLabelText = {window.getLocalizedText(langMap, 'quotation.plans.cashPortion')}
      />
    );
    return _.map(configFields, (field, index) => (
      <div key={index} style={{ width: '20%' }}>
        <div style={{ margin: '0 24px' }}>
          {field}
        </div>
      </div>
    ));
  }

  render() {
    const {muiTheme} = this.context;
    return (
      <Paper
        style={{
          boxShadow: muiTheme.palette.shadowColor + ' 1px 1px 4px',
          padding: '12px 0',
          backgroundColor: '#FAFAFA',
          display: 'flex', minHeight: '0px',
          alignItems: 'center',
        }}
      >
        {this.getConfigFields()}
      </Paper>
    );
  }

}
