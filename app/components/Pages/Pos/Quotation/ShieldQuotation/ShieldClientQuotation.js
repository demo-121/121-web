import React, {PropTypes} from 'react';
import * as _ from 'lodash';
import {RaisedButton, SelectField, MenuItem, IconButton} from 'material-ui';

import EABComponent from '../../../../Component';
import {getIcon} from '../../../../Icons/index';
import SelectRiderDialog from '../SelectRiderDialog';
import ShieldInsuredInfo from './ShieldInsuredInfo';
import ShieldOptions from './ShieldOptions';
import ShieldPlanTable from './ShieldPlanTable';

import styles from '../../../../Common.scss';

export default class ClientQuotation extends EABComponent {

  static propTypes = {
    style: PropTypes.object,
    baseProductCode: PropTypes.string,
    profile: PropTypes.object,
    availableClasses: PropTypes.array,
    validProfile: PropTypes.bool,
    eligible: PropTypes.bool,
    quotation: PropTypes.object,
    planDetails: PropTypes.object,
    inputConfigs: PropTypes.object
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    onChangeInsuredBp: PropTypes.func,
    handleUpdatePlanList: PropTypes.func,
    showErrorMsg: PropTypes.func,
    openClientProfile: PropTypes.func
  });

  _getBasicPlanOptions() {
    const {lang} = this.context;
    const {availableClasses} = this.props;
    let options = [];
    options.push(<MenuItem key={'-'} value={null} primaryText={'-'} />);
    _.each(availableClasses, (availableClass) => {
      options.push(
        <MenuItem
          key={availableClass.covClass}
          value={availableClass.covClass}
          primaryText={window.getLocalText(lang, availableClass.className)}
        />
      );
    });
    return options;
  }

  _getQuotContent() {
    const {handleUpdatePlanList, showErrorMsg} = this.context;
    const {quotation, planDetails, inputConfigs} = this.props;
    return (
      <div style={{ marginTop: 20 }}>
        <ShieldOptions
          quotation={quotation}
          planDetails={planDetails}
        />
        <ShieldPlanTable
          style={{ marginTop: '20px' }}
          quotation={quotation}
          planDetails={planDetails}
          inputConfigs={inputConfigs}
        />
        {this._getRiderButton()}
        <SelectRiderDialog
          ref={ref=>{ this.selectRiderDialog = ref; }}
          quotation={quotation}
          planDetails={planDetails}
          riderList={inputConfigs[quotation.baseProductCode].riderList}
          onSave={(covCodes) => {
            handleUpdatePlanList(quotation.iCid, covCodes, () => {
              this.selectRiderDialog.closeDialog();
            }, (errorMsg) => {
              this.selectRiderDialog.closeDialog();
              showErrorMsg(errorMsg);
            });
          }}
        />
      </div>
    );
  }

  _getRiderButton() {
    const {langMap} = this.context;
    const {quotation, planDetails, inputConfigs} = this.props;
    const {riderList} = inputConfigs[quotation.baseProductCode];
    if (_.size(planDetails) > 1) {
      return (
        <div className={styles.RiderBtn}>
          <RaisedButton
            primary
            disabled={!riderList || !riderList.length}
            label={window.getLocalizedText(langMap, 'quotation.btn.selectRiders')}
            onTouchTap={() => {
              this.selectRiderDialog.openDialog();
            }}
          />
        </div>
      );
    }
    return null;
  }

  getInsuredBtn() {
    const {muiTheme, langMap, showErrorMsg, onChangeInsuredBp} = this.context;
    const {quotation, profile, validProfile} = this.props;
    if (quotation) {
      return (
        <IconButton onTouchTap={() => onChangeInsuredBp(profile.cid, null)}>
          {getIcon('removeCircle', 'red')}
        </IconButton>
      );
    } else if (!validProfile) {
      return (
        <IconButton onTouchTap={() => showErrorMsg(window.getLocalizedText(langMap, 'quotation.shield.incompleteProfile'))}>
          {getIcon('info', muiTheme.palette.primary2Color)}
        </IconButton>
      );
    }
  }

  getBasicPlanField() {
    const {langMap, onChangeInsuredBp, openClientProfile} = this.context;
    const {profile, validProfile, eligible, quotation} = this.props;
    const covClass = _.get(quotation, 'plans[0].covClass');
    // use overlay to cover select field as field must be disabled but not show stop sign
    return (
      <div style={{ position: 'relative' }}>
        <SelectField
          value={covClass}
          disabled={!validProfile || !eligible}
          floatingLabelText={window.getLocalizedText(langMap, 'quotation.basic_plan')}
          onChange={(e, index, value) => {
            onChangeInsuredBp(profile.cid, value);
          }}
          floatingLabelFixed
          underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
          labelStyle={{ color: 'black' }}
        >
          {this._getBasicPlanOptions()}
        </SelectField>
        {!validProfile ? (
          <div
            style={{
              position: 'absolute',
              top: 0,
              width: 256,
              height: 86,
              cursor: 'pointer',
              zIndex: 2
            }}
            onTouchTap={(e) => {
              openClientProfile();
            }}
          />
        ) : null}
      </div>
    );
  }

  render() {
    const {profile, quotation, planDetails, baseProductCode, style} = this.props;
    const bpDetail = planDetails && planDetails[baseProductCode];
    return (
      <div style={style}>
        <div style={{ display: 'flex', minHeight: '0px', marginTop: '8px' }}>
          <div>
            <div style={{ display: 'flex', minHeight: '0px', alignItems: 'center' }}>
              <ShieldInsuredInfo
                style={{ flexGrow: '1' }}
                profile={profile}
                bpDetail={bpDetail}
              />
              {this.getInsuredBtn()}
            </div>
            {this.getBasicPlanField()}
          </div>
        </div>
        {quotation ? this._getQuotContent() : null}
      </div>
    );
  }

}
