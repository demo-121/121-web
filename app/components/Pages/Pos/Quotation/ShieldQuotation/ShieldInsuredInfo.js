import React, { PropTypes} from 'react';
import * as _ from 'lodash';

import EABComponent from '../../../../Component';
import styles from '../../../../Common.scss';
import {getAgeByMethod} from '../../../../../../common/ProductUtils';
import {parseDate} from '../../../../../../common/DateUtils';

export default class ShieldInsuredInfo extends EABComponent {

  static propTypes = {
    bpDetail: PropTypes.object,
    profile: PropTypes.object
  };

  _getGender(gender) {
    return window.getLocalizedText(this.context.langMap, gender === 'M' ? 'profile.gender.male' : 'profile.gender.female');
  }

  _getAge(age) {
    return age + ' ' + window.getLocalizedText(this.context.langMap, 'profile.years_old');
  }

  _getSmoke(smoke) {
    if (smoke === 'Y') {
      return window.getLocalizedText(this.context.langMap, 'profile.smoking_habit.smoker');
    } else if (smoke === 'N') {
      return window.getLocalizedText(this.context.langMap, 'profile.smoking_habit.non_smoker');
    }
    return null;
  }

  _getOccupation(occupation) {
    let occ = _.find(this.context.optionsMap.occupation.options, (option) => option.value === occupation);
    return occ && window.getLocalText(this.context.lang, occ.title);
  }

  _getResidence(residence) {
    let cty = _.find(this.context.optionsMap.residency.options, (option) => option.value === residence);
    return cty && window.getLocalText(this.context.lang, cty.title);
  }

  render() {
    const {profile, bpDetail} = this.props;
    const {fullName, gender, dob, isSmoker, occupation, residenceCountry} = profile || {};

    let infoLine1 = [];
    infoLine1.push(this._getGender(gender));
    infoLine1.push(this._getAge(getAgeByMethod(bpDetail.calcAgeMethod, new Date(), parseDate(dob))));
    infoLine1.push(this._getSmoke(isSmoker));
    infoLine1.push(this._getOccupation(occupation));
    infoLine1.push(this._getResidence(residenceCountry));

    return (
      <div className={styles.ShieldInsuredInfo}>
        <span className={styles.FullName}>{fullName}</span>
        <span className={styles.Others}>
          {_.join(_.filter(infoLine1, info => !!info), ', ') }
        </span>
      </div>
    );
  }

}
