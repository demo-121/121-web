import React, {PropTypes} from 'react';
import {
  Paper,
  SelectField,
  TextField,
  MenuItem,
  IconButton
} from 'material-ui';
import * as _ from 'lodash';

import EABComponent from '../../../Component';
import NumericField from '../../../CustomViews/NumericField';
import {getIcon} from '../../../Icons/index';
import styles from '../../../Common.scss';

export default class PlanInfo extends EABComponent {

  static propTypes = {
    style: PropTypes.object,
    quotation: PropTypes.object,
    planDetails: PropTypes.object,
    inputConfigs: PropTypes.object,
    planErrors: PropTypes.object
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    handleConfigChange: PropTypes.func,
    handleRemovePlan: PropTypes.func
  });

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      changed: false,
      focusedField: null,
      focusedPlan: null
    });
  }

  _genOptions(list) {
    let options = [];
    var lang = this.context.lang;

    _.each(list || [], (item) => {
      options.push(
        <MenuItem
          key={'o_' + item.value}
          value={item.value}
          primaryText={window.getLocalText(lang, item.title)}
        />
      );
    });
    return options;
  }

  onChange() {
    if (this.state.changed) {
      this.setState({
        changed: false
      });
      this.context.handleConfigChange();
    }
  }

  _isCompulsory(covCode) {
    const {quotation, inputConfigs} = this.props;
    let isCompulsory;
    if (covCode === quotation.baseProductCode) {
      isCompulsory = true;
    } else {
      let basicPlan = inputConfigs[quotation.baseProductCode];
      let rider = _.find(basicPlan.riderList, r => r.covCode === covCode);
      if (rider) {
        return rider.compulsory === 'Y';
      }
      return false;
    }
    return isCompulsory;
  }

  _getPlanConfigComponent(plan, config) {
    if (!config) {
      return null;
    }
    const {lang, langMap, optionsMap, handleConfigChange, handleRemovePlan} = this.context;
    const {inputConfigs, quotation, planDetails} = this.props;
    const bpDetail = planDetails[quotation.baseProductCode];
    const inputConfig = inputConfigs[config.covCode];
    const {hasClass, hasPolTerm, hasPremTerm, hasSumAssured, hasOthSa} = this._getUiConfig();

    let skipRider = false;
    for (var p in quotation.plans) {
      if (quotation.plans[p].covCode === config.covCode) {
        skipRider = quotation.plans[p].hiddenRider === 'Y';
      }
    }
    if (skipRider){
      return null;
    }

    const sign          = window.getCurrencySign(quotation.compCode, quotation.ccy, optionsMap);
    let polTermOptions  = this._genOptions(inputConfig && inputConfig.policyTermList);
    let premTermOptions = this._genOptions(inputConfig && inputConfig.premTermList);
    let classOptions    = this._genOptions(inputConfig && inputConfig.classTypeList);
    let removeBtn = (
      <IconButton onTouchTap={() => handleRemovePlan(plan)}>
        {getIcon('removeCircle', 'red')}
      </IconButton>
    );
    return (
      <tr key={plan.covCode} className={styles.PlanInfoRow}>
        <td>{this._isCompulsory(plan.covCode) ? null : removeBtn}</td>
        <td className={styles.PlanName}>{window.getLocalText(lang, plan.covName)}</td>
        {hasPolTerm ? (
          <td>
            <SelectField
              id="policyTerm"
              fullWidth
              hintText={polTermOptions.length ? window.getLocalizedText(langMap, 'quotation.select') : '-'}
              disabled={!polTermOptions.length || !inputConfig || !inputConfig.canEditPolicyTerm}
              underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
              value={polTermOptions.length ? config.policyTerm : null}
              onChange={(e, index, value) => {
                config.policyTerm = value;
                handleConfigChange();
              }}
              style={{ verticalAlign: 'bottom' }}
              labelStyle={{ color: 'black' }}
            >
              {polTermOptions}
            </SelectField>
          </td>
        ) : null}

        {hasPremTerm ? (
          <td>
            <SelectField
              id="premTerm"
              fullWidth
              autoWidth
              hintText={premTermOptions.length ? window.getLocalizedText(langMap, 'quotation.select') : '-'}
              disabled={!inputConfig || !inputConfig.canEditPremTerm}
              underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
              value={config.premTerm}
              onChange={(e, index, value) => {
                config.premTerm = value;
                handleConfigChange();
              }}
              style={{ verticalAlign: 'bottom' }}
              labelStyle={{ color: 'black' }}
            >
              {premTermOptions}
            </SelectField>
          </td>
        ) : null}

        {hasClass ? (
          <td>
            {inputConfig.canEditClassType ? (
              <SelectField
                id="covClass"
                fullWidth
                hintText={classOptions.length ? window.getLocalizedText(langMap, 'quotation.select') : '-'}
                disabled={!inputConfig}
                underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
                value={config.covClass}
                onChange={(e, index, value) => {
                  config.covClass = value;
                  handleConfigChange();
                }}
                style={{ verticalAlign: 'bottom' }}
              >
                {classOptions}
              </SelectField>
            ) : (
              <TextField
                id="covClassText"
                disabled
                fullWidth
                underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
                inputStyle={{ textAlign: 'center' }}
                value={config.covClass || '-'}
              />
            )}
          </td>
        ) : null}

        {hasSumAssured ? (
          <td>
            <NumericField
              id="sumInsured"
              fullWidth
              disabled={!inputConfig || !inputConfig.canEditSumAssured}
              sign={sign}
              maxIntDigit={inputConfig && inputConfig.saInput && inputConfig.saInput.noOfDigit && inputConfig.saInput.noOfDigit > 0 ? inputConfig.saInput.noOfDigit : 10 }
              value={!inputConfig || !inputConfig.canViewSumAssured ? '-' : config.sumInsured}
              onChange={(e, value) => {
                config.sumInsured = value;
                config.calcBy = 'sumAssured';
                this.setState({
                  changed: true
                });
              }}
              onBlur={() => {
                this.setState({
                  focusedField: null,
                  focusedPlan: null
                });
                if (this.state.changed) {
                  this.onChange();
                }
              }}
              onFocus={() => {
                this.setState({
                  focusedField: 'sumInsured',
                  focusedPlan: plan.covCode
                });
              }}
            />
            {!_.isEmpty(plan.saPostfix) ? (
              <div style={{ textAlign: 'right', height: '20px'}}>
                {window.getLocalText(lang, plan.saPostfix)}
              </div>
            ) : null}
          </td>
        ) : null}

        {hasOthSa ? (
          <td>
            <NumericField
              id="othSa"
              fullWidth
              disabled={!inputConfig || !inputConfig.canEditOthSa}
              sign={sign}
              maxIntDigit={bpDetail.othSaLength}
              decimalPlace={bpDetail.othSaDecimal}
              value={!inputConfig || !inputConfig.canViewOthSa ? '-' : (_.isNumber(config[bpDetail.othSaKey]) ? config[bpDetail.othSaKey] : null)}
              onChange={(e, value) => {
                config.calcBy = 'othsa';
                config[bpDetail.othSaKey] = value;
                this.setState({ changed: true });
              }}
              onFocus={() => {
                this.setState({
                  focusedField: 'othSa',
                  focusedPlan: plan.covCode
                });
              }}
              onBlur={() => {
                this.setState({
                  focusedField: null,
                  focusedPlan: null
                });
                if (this.state.changed) {
                  this.onChange();
                }
              }}
            />
          </td>
        ) : null}

        <td>
          <NumericField
            id="premium"
            fullWidth
            disabled={!inputConfig || !inputConfig.canEditPremium}
            sign={sign}
            maxIntDigit={inputConfig && inputConfig.premInput && inputConfig.premInput.noOfDigit && inputConfig.premInput.noOfDigit > 0 ? inputConfig.premInput.noOfDigit : 7 }
            decimalPlace={inputConfig && inputConfig.premInput ? inputConfig.premInput.decimal : 2}
            value={inputConfig && inputConfig.canViewPremium === false ? '-' : config.premium}
            onChange={(e, value) => {
              config.premium = value;
              config.calcBy = 'premium';
              this.setState({
                changed: true
              });
            }}
            onBlur={() => {
              this.setState({
                focusedField: null,
                focusedPlan: null
              });
              if (this.state.changed) {
                this.onChange();
              }
            }}
            onFocus={() => {
              this.setState({
                focusedField: 'premium',
                focusedPlan: plan.covCode
              });
            }}
          />
          {!_.isEmpty(plan.premPostfix) ? (
              <div style={{ textAlign: 'right', height: '20px',fontSize: '10px'}}>
                {window.getLocalText(lang, plan.premPostfix)}
              </div>
            ) : null}
        </td>
      </tr>
    );
  }

  _getPlanErrorComponent(plan, error) {
    const {lang, langMap} = this.context;
    const {tableColCnt} = this._getUiConfig();
    return (
      <tr key={plan.covCode + '-' + error.code} className={styles.PlanInfoErrorContainer}>
        <td colSpan={tableColCnt} className={styles.PlanInfoErrorContent}>
          {error.msg || window.getMsg(lang, langMap, error.code, error.msgPara)}
        </td>
      </tr>
    );
  }

  _hasClass() {
    const {quotation, inputConfigs} = this.props;
    return inputConfigs[quotation.baseProductCode].hasClass;
  }

  _hasPolTerm(){
    const {quotation, inputConfigs} = this.props;
    return !inputConfigs[quotation.baseProductCode].isPolTermHide;
  }

  _hasPremTerm(){
    const {quotation, inputConfigs} = this.props;
    return !inputConfigs[quotation.baseProductCode].isPremTermHide;
  }

  _hasSumAssured(){
    const {quotation, inputConfigs} = this.props;
    return inputConfigs[quotation.baseProductCode].canViewSumAssured;
  }

  _hasOthSa() {
    const {quotation, planDetails} = this.props;
    return planDetails[quotation.baseProductCode].othSaInd === 'Y';
  }

  _getUiConfig(){
    const hasClass      = this._hasClass();
    const hasPolTerm    = this._hasPolTerm();
    const hasPremTerm   = this._hasPremTerm();
    const hasSumAssured = this._hasSumAssured();
    const hasOthSa      = this._hasOthSa();
    return {
      hasClass,
      hasPolTerm,
      hasPremTerm,
      hasSumAssured,
      hasOthSa,
      tableColCnt: 3 + [hasClass, hasPolTerm, hasPremTerm, hasSumAssured, hasOthSa].map(v => v ? 1 : 0)
    };
  }

  _getPlanInfoHint(plan) {
    const {lang, langMap, optionsMap} = this.context;
    const {inputConfigs, quotation} = this.props;
    const {focusedField, focusedPlan} = this.state;
    const inputConfig = inputConfigs[plan.covCode];
    const {tableColCnt} = this._getUiConfig();
    const sign = window.getCurrencySign(quotation.compCode, quotation.ccy, optionsMap);

    if (focusedPlan !== plan.covCode) {
      return null;
    }
    let limit = null;
    let multiple = null;
    if (focusedField === 'premium') {
      limit = inputConfig.premlim;
      if (inputConfig.premInput && inputConfig.premInput.factor && inputConfig.premInput.factor > 1){
        multiple = inputConfig.premInput.factor;
      }
    } else if (focusedField === 'sumInsured') {
      limit = inputConfig.benlim;
      if (inputConfig.saInput && inputConfig.saInput.factor && inputConfig.saInput.factor > 1){
        multiple = inputConfig.saInput.factor;
      }
    }
    if (Array.isArray(inputConfig.planInfoHintMsg)){
      if (focusedField === 'premium') {
        return (
          <tr key={plan.covCode + '-PlanInfohintMsg'} className={styles.PlanInfoErrorContainer}>
            <td colSpan={tableColCnt} className={styles.PlanInfoHintContent}>{inputConfig.planInfoHintMsg[0].premium}</td>
          </tr>
        );
      } else if (focusedField === 'sumInsured') {
        return (
          <tr key={plan.covCode + '-PlanInfohintMsg'} className={styles.PlanInfoErrorContainer}>
            <td colSpan={tableColCnt} className={styles.PlanInfoHintContent}>{inputConfig.planInfoHintMsg[0].sumInsured}</td>
          </tr>
        );
      }
    }
    if (limit) {
      let {min, max} = limit;
      if (min || max){
        let hintCode, params = null;
        if (multiple){
          if (min && max) {
            hintCode = 'quotation.plans.hint.range.' + focusedField + '.multiple';
            params = [ window.getCurrency(min, sign, 2), window.getCurrency(max, sign, 2), window.getCurrency(multiple, '', 0) ];
          } else if (max) {
            hintCode = 'quotation.plans.hint.max.' + focusedField + '.multiple';
            params = [ window.getCurrency(max, sign, 2), window.getCurrency(multiple, '', 0)];
          } else if (min) {
            hintCode = 'quotation.plans.hint.min.' + focusedField + '.multiple';
            params = [ window.getCurrency(min, sign, 2), window.getCurrency(multiple, '', 0)];
          }
        } else {
          if (min && max) {
            hintCode = 'quotation.plans.hint.range.' + focusedField;
            params = [ window.getCurrency(min, sign, 2), window.getCurrency(max, sign, 2) ];
          } else if (max) {
            hintCode = 'quotation.plans.hint.max.' + focusedField;
            params = [ window.getCurrency(max, sign, 2)];
          } else if (min) {
            hintCode = 'quotation.plans.hint.min.' + focusedField;
            params = [ window.getCurrency(min, sign, 2)];
          }
        }

        return (
          <tr key={plan.covCode + '-' + hintCode} className={styles.PlanInfoErrorContainer}>
            <td colSpan={tableColCnt} className={styles.PlanInfoHintContent}>{window.getMsg(lang, langMap, hintCode, params)}</td>
          </tr>
        );
      }
    }

    if (inputConfig.planInfoHintMsg_othSa) {
      if (focusedField === 'othSa') {
        return (
          <tr key={plan.covCode + '-PlanInfohintMsg'} className={styles.PlanInfoErrorContainer}>
            <td colSpan={tableColCnt} className={styles.PlanInfoHintContent}>{inputConfig.planInfoHintMsg_othSa}</td>
          </tr>
				);
      }
    } else if (inputConfig.planInfoHintMsg) {
      return (
          <tr key={plan.covCode + '-PlanInfohintMsg'} className={styles.PlanInfoErrorContainer}>
            <td colSpan={tableColCnt} className={styles.PlanInfoHintContent}>{inputConfig.planInfoHintMsg}</td>
          </tr>
        );
    }
    return null;
  }

  _getClassTitle() {
    const {langMap} = this.context;
    const {quotation, inputConfigs} = this.props;
    return _.get(inputConfigs, `${quotation.baseProductCode}.classTitle.en`) || window.getLocalizedText(langMap, 'quotation.plans.covClass');
  }

  render() {
    const {lang, langMap} = this.context;
    const {quotation, planDetails, planErrors} = this.props;
    var bpDetails = planDetails[quotation.baseProductCode];
    const {hasClass, hasPolTerm, hasPremTerm, hasSumAssured, hasOthSa, tableColCnt} = this._getUiConfig();
    let bpEls     = [];
    let riderEls  = [];

    _.each(quotation.plans, (plan) => {
      let els = plan.covCode === quotation.baseProductCode ? bpEls : riderEls;
      let el = this._getPlanConfigComponent(planDetails[plan.covCode], plan);

      //basic plan or rider not hidden
      if (plan.covCode === quotation.baseProductCode || plan.covCode !== quotation.baseProductCode && plan.hiddenRider !== 'Y') {
      els.push(el);
      var hint = this._getPlanInfoHint(plan);
      if (hint) {els.push(hint);}
      else {
        if (planErrors && planErrors[plan.covCode]) {
          _.each(planErrors[plan.covCode], (error) => {
            els.push(this._getPlanErrorComponent(plan, error));
          });
        }
      }
      }
    });

    let bpGroupHeader = (
      <tr className={styles.GroupHeader}>
        <td colSpan={tableColCnt}>{window.getLocalizedText(langMap, 'quotation.plans.basicPlan')}</td>
      </tr>
    );
    let riderGroupHeader = null;
    if (riderEls.length > 0) {
      riderGroupHeader = (
        <tr className={styles.GroupHeader}>
          <td colSpan={tableColCnt}>{window.getLocalizedText(langMap, 'quotation.plans.riders')}</td>
        </tr>
      );
    }

    return (
      <Paper style={{width: '100%'}}>
        <table className      = {styles.QuotTable + ' ' + styles.QuotPlanInfoTable}>
          <colgroup>
            <col className    = {styles.ColRowBtn} />
            <col className    = {styles.ColPlanName} />
            {hasPolTerm ?     <col className={styles.ColTermInput}/>  : null}
            {hasPremTerm ?    <col className={styles.ColTermInput}/>  : null}
            {hasClass ?       <col className={styles.ColCovClass}/>   : null}
            {hasSumAssured ?  <col className={styles.ColNumInput}/>   : null}
            {hasOthSa ?       <col className={styles.ColNumInput}/>   : null}
            <col className={styles.ColNumInput} />
          </colgroup>
          <thead>
            <tr>
              <th colSpan="2">{window.getLocalizedText(langMap, 'quotation.plans.covName')}</th>
              {hasPolTerm ?     <th>{window.getLocalizedText(langMap, 'quotation.plans.policyTerm')}{bpDetails.polTermRemarkSymbol ? <span className={styles.Superscript}>{bpDetails.polTermRemarkSymbol}</span> : null}</th> : null}
              {hasPremTerm ?    <th>{window.getLocalizedText(langMap, 'quotation.plans.premTerm')}{bpDetails.premTermRemarkSymbol ? <span className={styles.Superscript}>{bpDetails.premTermRemarkSymbol}</span> : null}</th> : null}
              {hasClass ?       <th>{this._getClassTitle()}</th> : null}
              {hasSumAssured ?  <th className={styles.ColRight}>{window.getLocalizedText(langMap, 'quotation.plans.sumInsured')}</th> : null}
              {hasOthSa ?       <th className={styles.ColRight}>{window.getLocalText(lang, bpDetails.othSaDesc)}</th> : null}
              <th className     = {styles.ColRight}>{window.getLocalizedText(langMap, 'quotation.plans.premium')}</th>
            </tr>
          </thead>
          <tbody>
            {bpGroupHeader}
            {bpEls}
            {riderGroupHeader}
            {riderEls}
          </tbody>
        </table>
      </Paper>
    );
  }
}
