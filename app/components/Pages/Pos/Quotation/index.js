import React from 'react';
import * as _ from 'lodash';

import { Popover, Paper, MenuItem } from 'material-ui';
import EABComponent from '../../../Component';
import WarningDialog from '../../../Dialogs/WarningDialog';
import AppbarPage from '../../AppbarPage';
import FloatingPage from '../../FloatingPage';
import Quotation from './Quotation';
import ShieldQuotation from './ShieldQuotation';

import {closeQuotation, genProposal, getPdfLanguage} from '../../../../actions/quotation';
const extConfig = require('../../../../../conf/extConfig.json');

export default class QuotationPage extends EABComponent {

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      prevQuotation: null,
      quotation: null,
      planDetails: null,
      inputConfigs: null,
      quotationErrors: null,
      quotWarnings: null,
      availableFunds: null,
      availableInsureds: null,
      errorMsg: null,
      popMsg: null,
      onCloseMsg: null,
      anchorEl: null,
      languageList: null
    });
  }

  componentDidMount() {
    const {store} = this.context;
    this.unsubscribe = store.subscribe(() => {
      const quotation = store.getState().quotation;
      let newState = {};
      if (!window.isEqual(this.state.prevQuotation, quotation.quotation)) {
        newState.prevQuotation = _.cloneDeep(quotation.quotation);
        newState.quotation = _.cloneDeep(quotation.quotation);
      }
      if (!window.isEqual(this.state.planDetails, quotation.planDetails)) {
        newState.planDetails = quotation.planDetails;
      }
      if (!window.isEqual(this.state.inputConfigs, quotation.inputConfigs)) {
        newState.inputConfigs = quotation.inputConfigs || {};
      }
      if (!window.isEqual(this.state.quotationErrors, quotation.quotationErrors)) {
        newState.quotationErrors = quotation.quotationErrors;
      }
      if (!window.isEqual(this.state.quotWarnings, quotation.quotWarnings)) {
        newState.quotWarnings = quotation.quotWarnings;
      }
      if (!window.isEqual(this.state.availableFunds, quotation.availableFunds)) {
        newState.availableFunds = quotation.availableFunds;
      }
      if (!window.isEqual(this.state.availableInsureds, quotation.availableInsureds)) {
        newState.availableInsureds = quotation.availableInsureds;
      }
      if (!window.isEmpty(newState)) {
        this.setState(newState, () => {
          this.onChangePage();
        });
      }
    });
    this.init();
  }

  componentWillUnmount() {
    this.unsubscribe && this.unsubscribe();
  }

  init() {
    const {store} = this.context;
    const quotation = store.getState().quotation;
    this.floatingPage.open();
    this.setState({
      prevQuotation: _.cloneDeep(quotation.quotation),
      quotation: _.cloneDeep(quotation.quotation),
      planDetails: quotation.planDetails,
      inputConfigs: quotation.inputConfigs,
      quotationErrors: quotation.quotationErrors,
      quotWarnings: quotation.quotWarnings
    }, () => {
      this.onChangePage();
    });
  }

  onChangePage() {
    const {langMap} = this.context;
    const {quotation, quotationErrors} = this.state;
    if (quotation) {
      let hasError = !_.every(quotationErrors, (errors) => _.isEmpty(errors));
      this.appbarPage.initAppbar({
        menu: {
          icon: 'close',
          action: () => {
            closeQuotation(this.context);
          }
        },
        title: window.getLocalizedText(langMap, 'quotation.title'),
        itemsIndex: 0,
        items: [
          [
            {
              type: 'flatButton',
              title: window.getLocalizedText(langMap, 'quotation.btn.genProposal'),
              disabled: hasError || !quotation.premium,
              action: event => {
                event.preventDefault();
                this.setState({
                  anchorEl: event.currentTarget
                });
              }
            }
          ]
        ]
      });
    }
  }

  _getContent() {
    const {quotation, planDetails, inputConfigs, quotationErrors, quotWarnings, availableFunds, availableInsureds} = this.state;
    if (quotation.quotType === 'SHIELD') {
      return (
        <ShieldQuotation
          quotation={quotation}
          planDetails={planDetails}
          inputConfigs={inputConfigs}
          quotationErrors={quotationErrors}
          quotWarnings={quotWarnings}
          availableInsureds={availableInsureds}
        />
      );
    } else {
      return (
        <Quotation
          quotation={quotation}
          planDetails={planDetails}
          inputConfigs={inputConfigs}
          quotationErrors={quotationErrors}
          quotWarnings={quotWarnings}
          availableFunds={availableFunds}
        />
      );
    }
  }

  getProposalLanguage(){
    const {langMap} = this.context;
    const {quotation, anchorEl} = this.state;
    let optionValues;
    if (this.state.languageList){
      optionValues = this.state.languageList;
    } else if (quotation) {
      getPdfLanguage(this.context, quotation, (error) => {
        this.setState({ errorMsg: error });
      }, (result) => {
        const languageList = [];
        for (var key in result) {
          languageList.push(key);
        }
        this.setState({ languageList });
        optionValues = languageList;
      });
    } else {
      optionValues = [];
    }
    const options = _.map(optionValues || [], (lang) => {
      return { value: lang, disabled: false };
    });
    _.map(extConfig.language || [], (lang) => {
      if (optionValues) {
        if (optionValues.indexOf(lang) === -1) {
          options.push({ value: lang, disabled: true });
        }
      } else {
        options.push({ value: lang, disabled: true });
      }
    });
    return (
      <Popover
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
        targetOrigin={{horizontal: 'left', vertical: 'top'}}
        onRequestClose={() => {
          this.setState({ anchorEl: null });
        }}
      >
        <Paper
          zDepth={2}
          style={{
            width: 175,
            paddingLeft: 12,
            paddingRight: 12,
            paddingTop: 3,
            textAlign: 'left',
            display: 'inline-block'
          }}
        >
          {_.map(options || [], (option, inx) =>
            <MenuItem
              key={`simple-menu-${option.value}${inx}`}
              value={option.value}
              disabled={option.disabled}
              onTouchTap={() => {
                this.setState({ anchorEl: null });
                genProposal(this.context, quotation, option.value, (error) => {
                  this.setState({
                    errorMsg: error
                  });
                }, (msg, callback) => {
                  this.setState({
                    popMsg: msg,
                    onCloseMsg: callback
                  });
                });
              }}
              primaryText={window.getLocalizedText(langMap, `quotation.btn.${option.value}`)}
            />
          )}
        </Paper>
      </Popover>
    );
  }

  render() {
    const {quotation, planDetails, errorMsg, popMsg, onCloseMsg} = this.state;
    return (
      <FloatingPage ref={ref=>{ this.floatingPage = ref; }}>
        <AppbarPage ref={ref=>{ this.appbarPage = ref; }}>
          {!window.isEmpty(quotation) && !window.isEmpty(planDetails) ? this._getContent() : null}
          <WarningDialog
            open={!!errorMsg}
            isError
            msg={errorMsg}
            onClose={() => this.setState({ errorMsg: null })}
          />
          <WarningDialog
            open={!!popMsg}
            hasTitle={false}
            msg={popMsg}
            onClose={() => {
              this.setState({
                popMsg: null
              }, () => {
                onCloseMsg && onCloseMsg();
              });
            }}
          />
          {this.getProposalLanguage()}
        </AppbarPage>
      </FloatingPage>
    );
  }
}
