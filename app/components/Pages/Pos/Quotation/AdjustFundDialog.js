import React, { PropTypes } from 'react';
import { SelectField, MenuItem, TextField, Checkbox } from 'material-ui';
import * as _ from 'lodash';

import EABComponent from '../../../Component';
import NumericField from '../../../CustomViews/NumericField';
import SaveDialog from '../../../Dialogs/SaveDialog';

import styles from '../../../Common.scss';

export default class AdjustFundDialog extends EABComponent {

  static propTypes = {
    open:     PropTypes.bool,
    onClose:  PropTypes.func,
    onSave:   PropTypes.func,
    model:    PropTypes.object,
    adjustFundRiskRatings:    PropTypes.array
  };


  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      model: null,
      adjustFundRiskRatings: []
    });
  }

  componentWillReceiveProps(newProps) {
    let newState = {};

    if (!_.isEqual(newProps.model, this.state.model) ) {
      let modelGroup = _.get(newProps, 'model.groups');
      if (modelGroup) {
        newProps.model.groups = this._checkMandatoryFundGroup(modelGroup,newProps.adjustFundRiskRatings);
      }

      newState.model = newProps.model;

      if (newState.model && newState.model.groups != null) {
        newState.model.groups = this.adjustModelPossibleRange(newState.model.groups);
      }
    }

    if (!_.isEqual(newProps.adjustFundRiskRatings, this.state.adjustFundRiskRatings) ) {
      newState.adjustFundRiskRatings = newProps.adjustFundRiskRatings;
    }

    if (!_.isEmpty(newState)) {
      this.setState(newState);
    }
  }

  adjustModelPossibleRange(groups) {
    let totalGroups = groups.length;
    let curMax = 0;
    let maximumGroupUpperLimit = 0;

    return _.each(groups, (group, index, originalGroups) => {
      let min = 0;
      let max = 0;
      let currentRiskRating = _.get(group, 'riskRatings[0]');
       // Maximum possible upper limit
       maximumGroupUpperLimit = _.get(_.reduce(originalGroups, function(groupA, groupB){
         let accumlatedPercentageByRiskRating = function(validGroup) {
          return (_.get(validGroup, 'riskRatings[0]') >= currentRiskRating || validGroup.calculatedByReduce) ? validGroup.percentage || 0 : 0;
         }
         let percentageA = accumlatedPercentageByRiskRating(groupA);
         let percentageB = accumlatedPercentageByRiskRating(groupB);
         return {calculatedByReduce: true, percentage: percentageA + percentageB};
       }), 'percentage');

      // Copy preset precentage
      if (!_.isNumber(group.percentageNew)) {
        group.percentageNew = group.percentage;
      }

      if (index === 0) {
        max = group.percentage;
      } else {
        max = maximumGroupUpperLimit - curMax;
      }

      if (index === (totalGroups - 1)) {
        min = group.percentage;
      }

      group.min = min;
      group.max = max;

      if (group.percentageNew > group.max){
        group.percentageNew = group.max;
      }

      // Maximum possible upper limit after User input
      curMax += group.percentageNew || 0;
    });
  }

  _checkMandatoryFundGroup(groups,adjustFundRiskRatings) {
    const maxRiskRating = _.get(groups, '[0].riskRatings[0]');
    const {optionsMap} = this.context;
    let optionsMapRiskRatings = [];
    _.each(_.get(optionsMap, 'riskRating.options'), riskRating => {
      optionsMapRiskRatings.push(riskRating.value);
    });
    let riskLevelIndex = _.findIndex(_.sortBy(optionsMapRiskRatings, riskRating => riskRating), value => value === maxRiskRating);
    let availableFundGroup = [];
    let matchingRiskRating;

    _.each(groups, group => {
      let groupRiskRating = _.get(group, 'riskRatings[0]');
      matchingRiskRating = optionsMapRiskRatings[riskLevelIndex];
      if (groupRiskRating !== matchingRiskRating) {
        let riskRatingDiff = matchingRiskRating - groupRiskRating;
        for (let i = riskRatingDiff; i > 0; i--) {
          let missedRiskRating = optionsMapRiskRatings[riskLevelIndex];
          if (adjustFundRiskRatings.indexOf(missedRiskRating) > -1) {
            availableFundGroup.push(this._addSkipeedFundGroup(missedRiskRating));
          }
          riskLevelIndex = riskLevelIndex - 1;
        }
      }
      availableFundGroup.push(group);
      riskLevelIndex = riskLevelIndex - 1;
    });
    for (let i = riskLevelIndex; i >= 0; i--) {
      matchingRiskRating = optionsMapRiskRatings[i];
      if (adjustFundRiskRatings.indexOf(matchingRiskRating) > -1) {
        availableFundGroup.push(this._addSkipeedFundGroup(matchingRiskRating));
      }
    }

    return availableFundGroup;
  }

  _addSkipeedFundGroup(riskRating) {
    return {
      min: 0,
      max: 0,
      percentage: 0,
      percentageNew: 0,
      riskRatings: [riskRating]
    };
  }

  _onSave() {
    const {onSave} = this.props;
    const {model} = this.state;
    let adjustedModel = _.cloneDeep(model);
    _.each(adjustedModel.groups, group => {
      if (group.percentageNew === null || group.percentageNew === undefined || group.percentageNew === '') {
        group.percentageNew = 0;
      }
    });
    if (onSave) {
      onSave(adjustedModel);
    }
  }

  _getTotalGroupAlloc(){
    const {model} = this.state;
    if (model === null){return 0;}

    let totalPercentage = 0;
    _.each(model.groups, (group) => {
      totalPercentage += group.percentageNew;
    });
    return totalPercentage;
  }

  _isValidRiskAlloc(groups){
    let isValid = true;
    let cumulativeMax = 0;
    let cumulativeSum = 0;
    _.each(groups, (group) => {
      cumulativeMax += group.percentage;
      if (_.isNumber(group.percentageNew)){
        cumulativeSum += group.percentageNew;
        if (cumulativeSum > cumulativeMax){
          isValid = false;
          return false;
        }
      }
    });
    return isValid;
  }

  _getValidationMsg(){
    const {langMap} = this.context;
    const {model} = this.state;
    if (!model){return null;}
    let groupsTotal = _.reduce(model.groups, (sum, group) => sum + (group.percentageNew || 0), 0);
    let msgItems = [];
    let isValid = true;
    if (groupsTotal !== 100){
      isValid = false;
      msgItems.push(
        <div style={{width: '80%', paddingTop: 10}}>
              <label style={{ flex: 1, color: 'red' }}>
              {window.getLocalizedText(langMap, 'quotation.fund.info.totalAllocNeeds') + '100%'}
              </label>
        </div>
      );

    }
    return !isValid ? msgItems : null;
  }

  _getAllocationList() {
    const {langMap} = this.context;
    const {model} = this.state;

    if (model === null || !model.groups){return null;}

    let groupsCount = (model.groups !== null ? model.groups.length : null);
    let groupsIndex = 0;

    let items = [];
    _.each(model.groups, (group, index, groups) => {
      let riskRating  = group.riskRatings[0];
      let riskName    = '';

      if (riskRating === 1){riskName = window.getLocalizedText(langMap, 'quotation.fund.riskProfile.1');}
      if (riskRating === 2){riskName = window.getLocalizedText(langMap, 'quotation.fund.riskProfile.2');}
      if (riskRating === 3){riskName = window.getLocalizedText(langMap, 'quotation.fund.riskProfile.3');}
      if (riskRating === 4){riskName = window.getLocalizedText(langMap, 'quotation.fund.riskProfile.4');}
      if (riskRating === 5){riskName = window.getLocalizedText(langMap, 'quotation.fund.riskProfile.5');}

      groupsIndex++;

      let minimum =  (groupsIndex != groupsCount ? group.min :  group.percentage);


      items.push(
        <tr key={model.id + riskRating}>
          <td>{riskName}</td>
          <td>{group.percentage + '%'}</td>
          <td>{minimum + '% to ' + group.max + '%'}</td>
          <td style={{paddingRight: 20}}>
            <NumericField
              id          ={model.id + riskRating + "AdjustAlloc"}
              style       ={{width: '80%'}}
              inputStyle  ={{textAlign: 'center'}}
              maxIntDigit ={3}
              value       ={group.percentageNew}
              onChange    ={(e, value) => {
                group.percentageNew = value;
                this.setState({ model});
              }}
              onBlur = {() => {
                let value = group.percentageNew;
                if (value < group.min || value === null || value === undefined) {
                  group.percentageNew = group.min;
                } else if (value > group.max) {
                  group.percentageNew = group.max;
                } else {
                  group.percentageNew = value;
                }
                model.groups = this.adjustModelPossibleRange(model.groups);
                this.setState({ model});
              }}
            /> %
          </td>
        </tr>
      );

    });

    return (
      <div>
      <table className={styles.AvailableFundList + ' ' + styles.QuotTable}>
        <colgroup>
          <col className={styles.ColGroupB} />
          <col className={styles.ColGroupB} />
          <col className={styles.ColGroupB} />
          <col className={styles.ColGroupB} />
        </colgroup>
        <thead>
          <tr>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.riskProfile.' + model.id.substring(3,4))}</th>
            <th>{model.title}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.alloc.col_3')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.alloc.col_4')}</th>
          </tr>
        </thead>
        <tbody>
          {items}
        </tbody>
      </table>

      {this._getValidationMsg()}
      </div>
    );
  }

  _getDisableSave(){
    const {model} = this.state;
    if (this._getTotalGroupAlloc() !== 100) {
      return true;
    }
    return false;
  }


  render() {
    const {langMap} = this.context;
    const {onClose} = this.props;

    return (
      <SaveDialog
        open          ={this.props.open}
        title         ={window.getLocalizedText(langMap, 'quotation.fund.alloc.title')}
        disableSave   ={this._getDisableSave()}
        className     ={styles.FundDialog}
        bodyStyle     ={{height: '100% !important', minHeight:'1200px !important' }}
        bodyClassName ={styles.AdjustFundDialogSize}
        onSave        ={()=> this._onSave()}
        onClose       ={onClose}
      >
        <div style={{ minWidth: 880, padding: 24}}>
          {this._getAllocationList()}
        </div>
      </SaveDialog>
    );
  }

}
