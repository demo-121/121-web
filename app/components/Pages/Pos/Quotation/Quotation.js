import React, {PropTypes} from 'react';
import {RaisedButton, FlatButton, Divider} from 'material-ui';
import * as _ from 'lodash';

import EABComponent from '../../../Component';
import ClientInfo from './ClientInfo';
import QuotationConfigs from './QuotationConfigs';
import PolicyOptions from './PolicyOptions';
import PlanIllustration from './PlanIllustration';
import PlanInfo from './PlanInfo';
import SelectRiderDialog from './SelectRiderDialog';
import FundSelectionDialog from './FundSelectionDialog';
import {getIcon} from '../../../Icons/index';
import ConfirmDialog from '../../../Dialogs/ConfirmDialog';
import styles from '../../../Common.scss';

import {quote, resetQuot, updateRiders} from '../../../../actions/quotation';
import {removeInclusiveRiders, removeDisabledRiders} from './utils';

export default class Quotation extends EABComponent {

  static propTypes = {
    quotation: PropTypes.object,
    planDetails: PropTypes.object,
    inputConfigs: PropTypes.object,
    quotationErrors: PropTypes.object,
    quotWarnings: PropTypes.array,
    availableFunds: PropTypes.array
  };

  static childContextTypes = {
    handleConfigChange: PropTypes.func,
    handleResetQuot: PropTypes.func,
    handleRemovePlan: PropTypes.func,
    confirmAction: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      showIllustration: false,
      confirmMsg: null,
      onConfirm: null,
      isJoint: false
    });
  }

  componentWillReceiveProps(nextProps) {
    const {quotation, planDetails} = nextProps;
    const bpDetail = planDetails[quotation.baseProductCode];
    if (!bpDetail.payModes || bpDetail.payModes.length <= 1 || quotation.paymentMode === 'L') {
      this.setState({
        showIllustration: false
      });
    }
  }

  getChildContext() {
    return {
      handleConfigChange: () => {
        this.setState({}); // force rerender
        quote(this.context, this.props.quotation);
      },
      handleResetQuot: (keepConfigs, keepPolicyOptions, keepPlans, keepFunds, callback) => {
        resetQuot(this.context, this.props.quotation, keepConfigs, keepPolicyOptions, keepPlans, keepFunds, callback);
      },
      handleRemovePlan: (plan) => {
        const {langMap} = this.context;
        const {quotation, planDetails, inputConfigs} = this.props;
        const bpDetail = planDetails[quotation.baseProductCode];
        const riderList = inputConfigs[quotation.baseProductCode].riderList;
        this.setState({
          confirmMsg: window.getLocalizedText(langMap, 'quotation.riders.confirmRemove'),
          onConfirm: () => {
            let selectedCovCodes = [];
            _.each(quotation.plans, (p) => {
              if (p.covCode !== plan.covCode) {
                selectedCovCodes.push(p.covCode);
              }
            });
            selectedCovCodes = removeInclusiveRiders(plan.covCode, selectedCovCodes, quotation, bpDetail, riderList);
            selectedCovCodes = removeDisabledRiders(selectedCovCodes, quotation, bpDetail, riderList);
            updateRiders(this.context, selectedCovCodes, () => {
              this.setState({
                confirmMsg: null,
                onConfirm: null
              });
            });
          }
        });
      },
      confirmAction: (confirmMsg, onConfirm) => {
        this.setState({
          confirmMsg: confirmMsg,
          onConfirm: () => {
            onConfirm && onConfirm();
            this.setState({
              confirmMsg: null,
              onConfirm: null
            });
          }
        });
      }
    };
  }

  refreshClientLabel() {
    const {quotation, planDetails} = this.props;
    let {isJoint} = this.state;
    var product = planDetails[quotation.baseProductCode];
    if (quotation.sameAs === 'Y'){
      return false;
    }

    let mustJoint = product.prodFeature && product.prodFeature.en && product.prodFeature.en.includes('joint');
    if (mustJoint && !isJoint){
      this.setState({
        isJoint: mustJoint
      });
      return mustJoint;
    }
    if (quotation.policyOptions.policytype){
      let showJoint = quotation.policyOptions.policytype === 'joint';
      if (isJoint !== showJoint){
        this.setState({
          isJoint: showJoint
        });
        return showJoint;
      }
    }
    return isJoint;
  }

  _getClientInfoSection() {
    const {quotation} = this.props;
    let insured = {
      cid: quotation.iCid,
      fullName: quotation.iFullName,
      firstName: quotation.iFirstName,
      lastName: quotation.iLastName,
      gender: quotation.iGender,
      dob: quotation.iDob,
      age: quotation.iAge,
      residence: quotation.iResidence,
      smoke: quotation.iSmoke,
      email: quotation.iEmail,
      occupation: quotation.iOccupation
    };
    let proposer = {
      cid: quotation.pCid,
      fullName: quotation.pFullName,
      firstName: quotation.pFirstName,
      lastName: quotation.pLastName,
      gender: quotation.pGender,
      dob: quotation.pDob,
      age: quotation.pAge,
      residence: quotation.pResidence,
      smoke: quotation.pSmoke,
      email: quotation.pEmail,
      occupation: quotation.pOccupation
    };
    let clientInfos = [];
    if (quotation.sameAs === 'N') {
      clientInfos.push(<ClientInfo key="P" rtype="P" role={this.refreshClientLabel()} profile={proposer} style={{ width: '50%' }} />);
    }
    clientInfos.push(<ClientInfo key="I" rtype="I" role={this.refreshClientLabel()} profile={insured} style={{ width: '50%' }} />);
    return <div>{clientInfos}</div>;
  }

  _getQuotRemark() {
    const {quotation, planDetails} = this.props;
    const {lang} = this.context;
    let basicPlan = planDetails[quotation.baseProductCode];
    if (basicPlan && basicPlan.remarks) {
      var remark = window.getLocalText(lang, basicPlan.remarks);
      if (remark && remark.length > 5) {
        if (remark.indexOf('%%%') === -1){
          return <div key={'quotRemark'} className={styles.QuotRemark}>{remark}</div>;
        } else {
          var index = remark.indexOf('%%%');
          var remarksarray1 = remark.substr(0, index);
          var remarksarray2 = remark.substr(index + 3, remark.length);
          return <div key={'quotRemark'} className={styles.QuotRemark}><div>{remarksarray1}</div><br/><div>{remarksarray2}</div></div>;
        }
      }
    }
    return null;
  }

  _getQuotWarnings() {
    const {quotWarnings} = this.props;
    let warningEls = _.map(quotWarnings, (warning, index) => {
      return <div key={'quotWarn-' + index} className={styles.QuotaionWarning}>{warning}</div>;
    });
    return warningEls;
  }

  _getQuotationBtns() {
    const {showIllustration} = this.state;
    const {planDetails, quotation, quotationErrors} = this.props;
    const {muiTheme, langMap} = this.context;
    let bpDetail = planDetails[quotation.baseProductCode];
    let fontColor = muiTheme.palette.primary2Color;
    let warningColor = muiTheme.palette.warningColor;
    let btns = [];
    if (bpDetail && bpDetail.payModes && bpDetail.payModes.length > 1 && quotation.paymentMode !== 'L') {
      btns.push(
        <FlatButton
          key="planBtn"
          label={showIllustration ? window.getLocalizedText(langMap, 'quotation.btn.planInfo') : window.getLocalizedText(langMap, 'quotation.btn.illustration')}
          labelStyle={{ color: fontColor }}
          onTouchTap={() => {
            this.setState({
              showIllustration: !showIllustration
            });
          }}
        />
      );
    }
    if (bpDetail && bpDetail.fundInd === 'Y') {
      const fundError = _.get(quotationErrors, 'mandatoryErrors.fund');
      btns.push(
        <FlatButton
          key="fundBtn"
          label={window.getLocalizedText(langMap, 'quotation.btn.fund')}
          labelStyle={{ color: fontColor }}
          icon={fundError ? getIcon('warning', warningColor) : null}
          onTouchTap={() => {
            this.fundSelectDialog.openDialog();
          }}
        />
      );
    }
    return (
      <div className={styles.QuotBtns}>
        {btns}
      </div>
    );
  }

  _getRiderButton() {
    const {langMap} = this.context;
    const {quotation, planDetails, inputConfigs} = this.props;
    const {riderList} = inputConfigs[quotation.baseProductCode];
    if (_.size(planDetails) > 1) {
      return (
        <div className={styles.RiderBtn}>
          <RaisedButton
            primary
            disabled={!riderList || !riderList.length}
            label={window.getLocalizedText(langMap, 'quotation.btn.selectRiders')}
            onTouchTap={() => {
              this.selectRiderDialog.openDialog();
            }}
          />
        </div>
      );
    }
    return null;
  }

  _getPlanTable() {
    const {quotation, planDetails, inputConfigs, quotationErrors} = this.props;
    const {showIllustration} = this.state;
    if (showIllustration) {
      return (
        <PlanIllustration
          quotation={quotation}
          planDetails={planDetails}
          inputConfigs={inputConfigs}
        />
      );
    } else {
      return (
        <PlanInfo
          quotation={quotation}
          planDetails={planDetails}
          inputConfigs={inputConfigs}
          planErrors={quotationErrors && quotationErrors.planErrors}
        />
      );
    }
  }

  _getFooter() {
    const {langMap, optionsMap} = this.context;
    const {quotation} = this.props;
    const {showIllustration} = this.state;
    const sign = window.getCurrencySign(quotation.compCode, quotation.ccy, optionsMap);
    let footerContent;
    if (showIllustration) {
      footerContent = (
        <div className={styles.QuotationFooter}>
          <span className={styles.QuotationFooterItems}>{window.getLocalizedText(langMap, 'quotation.summary.totMonthPrem')}: {window.getCurrency(quotation.totMonthPrem, sign, 2)}</span>
          <span className={styles.QuotationFooterItems}>{window.getLocalizedText(langMap, 'quotation.summary.totQuarterPrem')}: {window.getCurrency(quotation.totQuarterPrem, sign, 2)}</span>
          <span className={styles.QuotationFooterItems}>{window.getLocalizedText(langMap, 'quotation.summary.totHalfyearPrem')}: {window.getCurrency(quotation.totHalfyearPrem, sign, 2)}</span>
          <span className={styles.QuotationFooterItems}>{window.getLocalizedText(langMap, 'quotation.summary.totYearPrem')}: {window.getCurrency(quotation.totYearPrem, sign, 2)}</span>
        </div>
      );
    } else {
      var title = '';
      var key = '';
      var payMode = quotation.paymentMode;
      switch (payMode) {
        case 'S':
          title = window.getLocalizedText(langMap, 'quotation.summary.totPrem.halfyear');
          key = 'totHalfyearPrem';
          break;
        case 'Q':
          title = window.getLocalizedText(langMap, 'quotation.summary.totPrem.quarter');
          key = 'totQuarterPrem';
          break;
        case 'M':
          title = window.getLocalizedText(langMap, 'quotation.summary.totPrem.month');
          key = 'totMonthPrem';
          break;
        case 'L':
          title = window.getLocalizedText(langMap, 'quotation.summary.totPrem.single');
          key = 'totYearPrem';
          break;
        case 'A':
        default:
          title = window.getLocalizedText(langMap, 'quotation.summary.totPrem.year');
          key = 'totYearPrem';
      }
      footerContent = (
        <div className={styles.QuotationFooter}>
          <span className={styles.QuotationFooterItem}>{title}: {window.getCurrency(quotation[key], sign, 2)}</span>
        </div>
      );
    }
    return (
      <div className={styles.QuotationFooterContainer}>
        <Divider />
        {footerContent}
      </div>
    );
  }

  render() {
    const {quotation, planDetails, inputConfigs, availableFunds, quotationErrors} = this.props;
    const {confirmMsg, onConfirm, isJoint} = this.state;
    const hasTopUpAlloc = inputConfigs[quotation.baseProductCode].topUpSelect;
    const showTopUpAlloc = _.get(planDetails, `${quotation.baseProductCode}.topUpAllocInd`) === 'Y';
    
    return (
      <div className={styles.Quotation}>
        <div style={{ flex: 1, overflow: 'auto', padding: '0 24px' }}>
          <div className={styles.QuotationContent}>
            {this._getClientInfoSection()}
            <QuotationConfigs
              quotation={quotation}
              planDetails={planDetails}
              inputConfigs={inputConfigs}
              style={{ marginTop: '24px' }}
            />
            <PolicyOptions
              quotation={quotation}
              planDetails={planDetails}
              inputConfigs={inputConfigs}
              quotationErrors={quotationErrors}
              style={{ marginTop: '24px' }}
            />
            {this._getQuotWarnings()}
            {this._getQuotationBtns()}
            {this._getPlanTable()}
            {this._getQuotRemark()}
            {this._getRiderButton()}
          </div>
        </div>
        {this._getFooter()}
        <SelectRiderDialog
          ref={ref=>{ this.selectRiderDialog = ref; }}
          quotation={quotation}
          planDetails={planDetails}
          riderList={inputConfigs[quotation.baseProductCode].riderList}
          onSave={(covCodes) => {
            updateRiders(this.context, covCodes, () => {
              this.selectRiderDialog.closeDialog();
            });
          }}
        />
        <FundSelectionDialog
          ref={ref=>{ this.fundSelectDialog = ref; }}
          quotation={quotation}
          availableFunds={availableFunds}
          hasTopUpAlloc={hasTopUpAlloc}
          showTopUpAlloc={showTopUpAlloc}
        />
        <ConfirmDialog
          open={!!confirmMsg}
          confirmMsg={confirmMsg}
          onClose={() => this.setState({ confirmMsg: null, onConfirm: null })}
          onConfirm={() => onConfirm && onConfirm()}
        />
      </div>
    );
  }

}
