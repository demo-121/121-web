import React from 'react';
import {FlatButton} from 'material-ui';

import FloatingPage from '../../../FloatingPage';
import AppbarPage from '../../../AppbarPage';
import StepperPage from '../../../StepperPage';
import Main from './Main';
import EABComponent from '../../../../Component';

import styles from '../../../../Common.scss';

class NAMain extends EABComponent {
  open=(template)=>{
    this.main.init(template);
    this.floatingPage.open();
  }

  close=()=>{
    this.floatingPage.close();
  }

  validate=()=>{
    return this.main && this.main.validate();
  }
  
  render() {
    return (
      <FloatingPage ref={(ref)=>{this.floatingPage=ref;}}>
        <AppbarPage>
          <StepperPage>
            <Main ref={(ref)=>{this.main=ref;}}/>
          </StepperPage>
        </AppbarPage>
      </FloatingPage>
    );
  }
}

export default NAMain;