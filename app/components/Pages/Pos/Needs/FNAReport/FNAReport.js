import React from 'react';

import PropTypes from 'prop-types';

import EABComponent from '../../../../Component';
import NeedsSummaryList from '../../../../CustomViews/NeedsSummaryList';
import DynSizePDFViewer from '../../../../CustomViews/DynSizePDFViewer';
import {getFNAReport, openEmail, clearFNAReport} from '../../../../../actions/needs';

import * as _ from 'lodash';

class FNAReport extends EABComponent {
  constructor(props, context) {
    super(props);
    
    let {needs, client} = context.store.getState();    

    this.state = Object.assign({}, this.state, {
      values: undefined,
      profile: client.profile
    })

    
    
  };

  getChildContext() {
    return {
      rootTemplate: this.state.template
    }
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null
    }
  }

  storeListener=()=>{
    let {store} = this.context;
    let {needs, client} = store.getState();

    let newState = {};
    if(needs.fnaReport && !isEqual(this.state.values, needs.fnaReport.pdfstr)){
      newState.values = _.cloneDeep(needs.fnaReport.pdfstr);
    }

    if(!isEmpty(newState))
      this.setState(newState);
  }

  init=()=>{
    const {langMap} = this.context;
    this.context.initAppbar({
      menu: {
        icon: 'close',
        action: ()=>{
          this.context.onPageClose();
          this.context.closePage();
          clearFNAReport(this.context);
        }
      },
      title: {
        "en": "FNA Report"
      },
      itemsIndex: 0,
      items:[
        [
          {
            type: 'flatButton',
            title: getLocalizedText(langMap, 'Email'),
            action: () => {
              openEmail(this.context);
            }  
          }
        ]
      ]
    });
    this.setState({values: {}}, ()=>{getFNAReport(this.context);});
    
  }  

  render() {
    let {pageOpen} = this.context;
    let {values} = this.state;

    if(!pageOpen || isEmpty(values)){
      return <div/>
    }
    
    return (
      <div style={{overflowY: 'auto'}}>
         <DynSizePDFViewer pdf={values} scale={3}/>
      </div>
    );
  }

}

FNAReport.contextTypes = Object.assign(FNAReport.contextTypes, {
  onPageClose: PropTypes.func
})

FNAReport.childContextTypes = Object.assign({}, FNAReport.childContextTypes, {
  rootTemplate: PropTypes.object
});

export default FNAReport;
