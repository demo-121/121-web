import React from 'react';
import {FlatButton} from 'material-ui';

import FloatingPage from '../../../FloatingPage';
import AppbarPage from '../../../AppbarPage';
import Main from './Main';
import EABComponent from '../../../../Component';

import styles from '../../../../Common.scss';

class LearnMore extends EABComponent {
  open=()=>{
    this.floatingPage.open();
  }

  close=()=>{
    this.floatingPage.close();
  }
  render() {
    return (
      <FloatingPage ref={(ref)=>{this.floatingPage=ref;}}>
        <AppbarPage>
          <Main/>
        </AppbarPage>
      </FloatingPage>
    );
  }
}

export default LearnMore;