import React from 'react';
import PropTypes from 'prop-types';
import {FlatButton} from 'material-ui';

import FloatingPage from '../../FloatingPage';
import AppbarPage from '../../AppbarPage';
import StepperPage from '../../StepperPage';
import DynColumn from '../../../DynViews/DynColumn';

import EABComponent from '../../../Component';

import * as _ from 'lodash';

import styles  from '../../../Common.scss';

class ApplySection extends EABComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      template: props.template || {},
      values: _.cloneDeep(changedValues),
      changedValues: _.cloneDeep(changedValues),
      valid: false,
      index: props.index,
      lastIndex: props.lastIndex
    })
  };

  componentDidMount() {
    this.context.updateAppbar({
      itemsIndex: 0,
      items:[[{
        id: "next",
        type: "flatButton",
        title: {
          "en": "NEXT"
        },
        disabled: true
      }],[{
        id: "next",
        type: "flatButton",
        title: {
          "en": "NEXT"
        },
        action: ()=>{
          saveFNA(
            this.context,
            this.state.changedValues,
            ()=>{
              if(this.state.index == this.state.lastIndex - 1){
                this.context.closePage();
              }else{
                this.context.goStep(this.state.index+1, this.state.index);
              }
            }
          )
        }
      }]]
    })
  }

  componentWillReceiveProps(nextProps){
    let {
      template,
      index,
      lastIndex
    } = nextProps;

    this.setState({
      template: template,
      changedValues: _.cloneDeep(changedValues),
      index: index,
      lastIndex: lastIndex
    });
  }

  validate=()=>{
    let {
      template,
      changedValues,
      values,
      valid
    } = this.state;

    //let _valid = !isEqual(changedValues, values) && validateValues(template, changedValues, values);
    let _valid = true;
    if(_valid != valid){
      this.setState({valid: _valid});
      this.context.updateAppbar({itemsIndex: 1});
    }
    return _valid;
  }

  render() {
    let {template, values, changedValues, showCondItems} = this.state;

    return (
      <div style={{display: 'flex', minHeight: '0px', flexGrow: 1, flexDirection: 'column', overflowY: 'auto'}}>
        <DynColumn
          template={template}
          changedValues={changedValues}
          values={values}
          validate={this.validate}
          onSave={(callback)=>{
            //to do
          }}
        />
      </div>
    );
  }
}

class Main extends EABComponent {
  constructor(props) {
      super(props);
      this.state = Object.assign({}, this.state, {
        template: {},
        values: {},
        changedValues: {}
      })
  };

  getChildContext() {
    return {
      rootTemplate: this.state.template
    }
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();

    this.context.initAppbar({
      menu: {
        icon: 'close',
        action: ()=>{
          this.context.closePage();
        }
      },
      title: {
        "en": "Personal Data Acknowledgement"
      },
      items:[[{
        id: "next",
        type: "flatButton",
        title: {
          "en":"NEXT"
        },
        action: ()=>{
          this.setState({
            index: this.state.index+1
          })
        }
      }]]
    })
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
      let newState = {};
      let {store} = this.context;
      let {dyn} = store.getState();

      if(!isEqual(this.state.template, dyn.template)){
        newState.template = dyn.template;
      }

      if(!isEqual(this.state.values, dyn.values)){
        newState.values = dyn.values;
      }

      if(!isEqual(this.state.changedValues, dyn.changedValues)){
        newState.changedValues = dyn.changedValues;
      }

      if(!isEmpty(newState))
        this.setState(newState);
  }

  init=()=>{
    let { template } = this.state;

    let {
      values,
      changedValues
    } = this.context.store.getState().dyn;

    let headers = [];
    _.forEach(items, (item, i)=>{
      let {id, title, disabled} = item;
      headers.push({
        id: id,
        title: title,
        disabled: disabled,
        validate: ()=>{
          return this.section.validate();
        }
      });
    })
        
    this.context.updateStepper({
      index: 0,
      headers: headers,
      onSave: this.onSave.bind(this)
    })

    this.setState({
      template: template,
      values: values,
      changedValues: changedValues
    })
  }

  onSave=(callback)=>{
    callback();
  }


  getContent=()=>{
    let {stepperIndex} = this.context;
    let {template, values} = this.state;

    if(!checkExist(template, 'items'))
      return;

    let field=template.items[stepperIndex];

    return <ApplySection ref={ref=>{this.section=ref}} index={stepperIndex} lastIndex={template.items.length} template={field} values={values}/>
  }

  render() {
    let {pageOpen} = this.context;

    if(!pageOpen){
      return <div/>
    }

    let content = this.getContent();

    return (
      <div style={{display: 'flex', minHeight: '0px', flexGrow: 1, flexDirection: 'column'}}>
        {content}
      </div>
    );
  }
}

Main.childContextTypes = Object.assign({}, Main.childContextTypes, {
  rootTemplate: PropTypes.object
});


export default class Apply extends EABComponent {
  open=()=>{
    this.main.init();
    this.floatingPage.open();
  }

  close=()=>{
    this.floatingPage.close();
  }
  render() {
    return (
      <FloatingPage ref={ref=>{this.floatingPage=ref}}>
        <AppbarPage>
          <StepperPage>
            <Main ref={ref=>{this.main=ref}}/>
          </StepperPage>
        </AppbarPage>
      </FloatingPage>
    );
  }
}
