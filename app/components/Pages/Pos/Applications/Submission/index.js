import React, { Component, PropTypes } from 'react';
import SubmissionMain from './Main';
import EABComponent from '../../../../Component';

class SubmissionIndex extends EABComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <SubmissionMain ref='submitMain' updateAppBarTitle={this.props.updateAppBarTitle}/>
    );
  }
}

export default SubmissionIndex;
