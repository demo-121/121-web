import React from 'react';
import EABComponent from '../../../../Component';
import SystemConstants from '../../../../../constants/SystemConstants';
import DynColumn from '../../../../DynViews/DynColumn';
import * as actions from '../../../../../actions/shieldApplication';
import _getOr from 'lodash/fp/getOr';

import * as _ from 'lodash';
import * as _v from '../../../../../utils/Validation';

class ShieldSubmission extends EABComponent {
  constructor(props, context) {
    super(props);
    let { shieldApplication } = context.store.getState();
    let { application } = shieldApplication;
    let submissionValues = _.cloneDeep(application);
    this.state = {
      changedValues: submissionValues,
      values: submissionValues,
      error:{}
    };
  }

  storeListener=()=>{
    let { shieldApplication } = this.context.store.getState();
    let { changedValues } = this.state;
    let newState = {};

    if (!window.isEqual(changedValues, _.get(shieldApplication, 'application') )) {
      newState.changedValues = _.get(shieldApplication, 'application');
      this.setState(newState);
    }
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
    }
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.validate(this.state.changedValues);
  }

  componentDidUpdate(prevProps, prevState) {
    let {changedValues} = this.state;
    if (!window.isEqual(prevState.changedValues, changedValues)) {
      this.validate(changedValues);
    }
  }

  validate=(changedValues)=>{
    let { optionsMap, langMap, store } = this.context;
    let { shieldApplication } = store.getState();
    let { handleChangedValues } = this.props;
    let { template } = shieldApplication;
    handleChangedValues(changedValues);

    let error = {};
    _v.exec(template, optionsMap, langMap, changedValues, changedValues, changedValues, error);

    let enableNextStep = !_.isNumber(error.code);
    let isCompleted = changedValues.isSubmittedStatus;

    actions.validate(this.context, isCompleted, enableNextStep);
    this.setState({error});
  }

  render() {
    let { shieldApplication } = this.context.store.getState();
    let { template, application } = shieldApplication;
    let { handleChangedValues } = this.props;
    let { changedValues, error, values } = this.state;

    return (
      <DynColumn
        customStyle={{paddingTop: '24px'}}
        onMenuItemChange={this.onMenuItemChange}
        isApp
        template={template}
        changedValues={changedValues}
        error={error}
        values={values}
        validate={this.validate}
        completeChangedValues={() => {
          handleChangedValues(changedValues);
        }}
        // updateCompleteMenu={this.updateCompleteMenu}
      />
    );
  }
}

export default ShieldSubmission;
