import React, { Component } from 'react';
import DynColumn from '../../../../DynViews/DynColumn';

import EABComponent from '../../../../Component';
import SystemConstants from '../../../../../constants/SystemConstants';

import * as actions from '../../../../../actions/submission';
import * as appActions from '../../../../../actions/application.js';
import {goSupportDocuments} from '../../../../../actions/supportDocuments';

import * as _v from '../../../../../utils/Validation';

import styles from '../../../../Common.scss';
import {getIcon} from '../../../../Icons/index';

class SubmissionMain extends EABComponent {
  constructor(props) {
    super(props);

    this.state = Object.assign({}, this.state, {
      template: {},
      values: {}, 
      changedValues: {},
      error: {},
      isMandDocsAllUploaded: false,
      isSubmitted: false
    })
  }

  componentWillMount() {
    let {
      appForm
    } = this.context.store.getState();

    let docId = appForm.application.id;

    actions.initSubmitStore(this.context, docId);
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.init();
  };

  componentDidUpdate(prevProps, prevState) {
    if (isEmpty(prevState.template) && !isEmpty(this.state.template) || prevState.isMandDocsAllUploaded != this.state.isMandDocsAllUploaded || prevState.isSubmitted != this.state.isSubmitted) {
      this.validateAfterInitStore();
    }
  }

  componentWillUnmount(){
    this.unsubscribe();
    actions.closeSubmissionPage(this.context);
  };

  storeListener=()=>{
    let newState = {};
    let {
      store
    } = this.context;

    let {
      submission
    } = store.getState();

    let {
      template,
      values,
      isMandDocsAllUploaded,
      isSubmitted
    } = this.state;

    if(!isEqual(template, submission.template) && submission.values.proposerName !== ""){
      newState.template = submission.template;
    }

    if(!isEqual(values, submission.values) && submission.values.proposerName !== ""){
      newState.values = submission.values;
      newState.changedValues = submission.values;
    }

    if (isMandDocsAllUploaded != submission.isMandDocsAllUploaded) {
      newState.isMandDocsAllUploaded = submission.isMandDocsAllUploaded;
    }

    if (isSubmitted != submission.isSubmitted) {
      newState.isSubmitted = submission.isSubmitted;
    }

    if(!isEmpty(newState)) {
      this.setState(newState);
    }
  };

  init=()=>{
    let {muiTheme} = this.context;
    let {
      appForm, 
      pos
    } = this.context.store.getState();

    let {
      changedValues
    } = this.state;

    this.props.updateAppBarTitle(SystemConstants.EAPP.STEP.SUBMISSION);

    this.validate(changedValues, true);
  }

  validateAfterInitStore=()=>{
    let {
      changedValues
    } = this.state;

    this.validate(changedValues, false);
  }

  getAppBarItemsIndex = (valid) => {
    let { isMandDocsAllUploaded, isSubmitted } = this.state;
    let itemsIndex = -1;

    //See the representation of itemsIndex in ../../AppForm/Main getAppBarItems()
    if (isSubmitted) {
      return 11;
    }

    if (isMandDocsAllUploaded) {
      itemsIndex = valid ? 10 : 9;
    } else {
      itemsIndex = 8;
    }

    return itemsIndex;
  }

  //Called by parent
  submitOnClick=()=>{
    let {
      appForm
    } = this.context.store.getState();

    let {
      values
    } = this.state;

    actions.doAppFormSubmission(this.context, appForm.application.id, values);
  }

  doneOnClick=(appId)=>{
    appActions.backToPosMain(this.context);
  }

  getStepperPageIndex=(error, init)=>{
    let {
      appForm
    } = this.context.store.getState();

    let completed = appForm.application.appStep > this.context.stepperIndex ? appForm.application.appStep - 1: this.context.stepperIndex - 1;
    let active = appForm.application.appStep > this.context.stepperIndex ? appForm.application.appStep: this.context.stepperIndex;

    if (this.state.isSubmitted) {
      completed = SystemConstants.EAPP.STEP.SUBMISSION;
    }

    return {completed, active};
  }

  validate=(changedValues, init)=>{
    let {langMap, optionsMap} = this.context;
    let {template, values} = this.state;
    let {app} = this.context.store.getState();

    let error = {};
    _v.exec(template, optionsMap, langMap, changedValues, changedValues, changedValues, error);
    let {completed, active} = this.getStepperPageIndex(error, init);
    let valid = !error.code;

    this.context.updateAppbar(Object.assign({}, {itemsIndex:this.getAppBarItemsIndex(valid)}));
    this.context.updateStepper(Object.assign({}, {completed, active}));
    this.setState({changedValues, values, template, error});
  }

  render() {
    let {
      template,
      values, 
      changedValues,
      error
    } = this.state;

    return (
      <div className={styles.PaymentSubmitPage}>
        <DynColumn
          template={template}
          values={values}
          error={error}
          changedValues={changedValues}
          validate={this.validate}
        />
      </div>
    );
  }
}

export default SubmissionMain;