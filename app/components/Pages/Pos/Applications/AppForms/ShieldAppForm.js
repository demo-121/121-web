import React from 'react';
import EABComponent from '../../../../Component';
import DynColumn from '../../../../DynViews/DynColumn';
import * as actions from '../../../../../actions/shieldApplication';

import * as _ from 'lodash';
import * as _v from '../../../../../utils/Validation';
import SystemConstants from '../../../../../constants/SystemConstants';

class ShieldAppForm extends EABComponent {
  constructor(props, context) {
    super(props);
    let { shieldApplication } = context.store.getState();
    let { application } = shieldApplication;
    this.state = {
      changedValues: _.get(application, 'applicationForm.values') ? application.applicationForm.values : {},
      error:{}
    };
  }

  componentDidMount() {
    let { handleChangedValues } = this.props;
    let { changedValues } = this.state;
    this.unsubscribe = this.context.store.subscribe(this.storeListener);

    handleChangedValues(changedValues);
    this.validate(changedValues);
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
    }
  }

  storeListener=()=>{
    let { shieldApplication } = this.context.store.getState();
    let { changedValues } = this.state;
    let newState = {};

    if (!window.isEmpty(newState)) {
      this.setState(newState);
    }
  }

  onMenuItemChange = (callback, getPolicyNumber) => {
    let { changedValues } = this.state;
    let { shieldApplication } = this.context.store.getState();

    actions.saveAppForm(this.context, _.get(shieldApplication, 'application.id'), changedValues, getPolicyNumber, () => {
      this.validate(changedValues);
      callback();
    });
  }

  checkAddr = (tabValues) => {
    let { shieldApplication } = this.context.store.getState();
    let { changedValues } = this.state;
    if (shieldApplication.application.isStartSignature) {
      return false;
    }

    let ph = changedValues.proposer;
    let phPersonal = ph.personalInfo;
    let laPersonal = tabValues.personalInfo;
    if (phPersonal.addrCountry === laPersonal.addrCountry) {
      if (phPersonal.addrCountry === 'R51') {
        return phPersonal.addrCity === laPersonal.addrCity;
      } else {
        return true;
      }
    }
  }

  copyAddr = (tabValues) => {
    let { changedValues } = this.state;
    let ph = changedValues.proposer;
    let pPersonal = ph.personalInfo;
    let lPersonal = tabValues.personalInfo;

    if (!changedValues.insured) {
      changedValues.insured = [];
    }

    let update = false;
    for (let i = 0; i < changedValues.insured.length; i++) {
      if (lPersonal.cid === changedValues.insured[i].personalInfo.cid) {
        changedValues.insured[i].personalInfo.addrCity = pPersonal.addrCity;
        changedValues.insured[i].personalInfo.addrBlock = pPersonal.addrBlock;
        changedValues.insured[i].personalInfo.addrEstate = pPersonal.addrEstate;
        changedValues.insured[i].personalInfo.addrStreet = pPersonal.addrStreet;
        changedValues.insured[i].personalInfo.postalCode = pPersonal.postalCode;
        changedValues.insured[i].personalInfo.unitNum = pPersonal.unitNum;
        update = true;
        break;
      }
    }
    if (update) {
      this.setState({changedValues});
    }
  }

  validatePhCompletedValues = (error, pCid) => {
    let menuErrorCode;
    let isCompleted = true;
    _.each(error, (menuErrors, key) => {
      menuErrorCode = _.get(menuErrors, 'proposer.code') || _.get(menuErrors, 'insured.' + pCid + '.code');
      if (key !== 'menu_plan' && _.isNumber(menuErrorCode)) {
        isCompleted = false;
        return false;
      }
    });

    return isCompleted;
  }

  validateLACompletedValues = (error) => {
    let menuErrorCode;
    let isCompleted = true;
    let result = [];
    let laCids = _.keys(_.get(error, 'menu_person.insured'));
    _.each(laCids, (cid) => {
      isCompleted = true;
      _.each(error, (menuErrors, menuKey) => {
        menuErrorCode = _.get(menuErrors, 'insured.' + cid + '.code');
        if (menuKey !== 'menu_plan' && _.isNumber(menuErrorCode)) {
          isCompleted = false;
          return false;
        }
      });

      if (isCompleted === true) {
        result.push(cid);
      }
    });

    return result;
  }

  validateLACompletedPersonalInfoValues = (error) => {
    let isCompleted = true;
    let laCids = _.keys(_.get(error, 'menu_person.insured'));
    _.each(laCids, (cid) => {
      let laPersonInfoCompleted = !_.isNumber(_.get(error, 'menu_person.insured.' + cid + '.code'));
      isCompleted = isCompleted && laPersonInfoCompleted;
    });
    return isCompleted;
  }

  validate=(changedValues)=>{
    let { optionsMap, langMap, store } = this.context;
    let { shieldApplication } = store.getState();
    let { template } = shieldApplication;
    let { isPolicyNumberGenerated } = shieldApplication.application;
    let { handleChangedValues } = this.props;

    let error = {};
    _v.exec(template, optionsMap, langMap, changedValues, changedValues, changedValues, error);

    // Check Proposer tab
    let updateParentChangedValues = false;
    let proposerCid = _.get(changedValues, 'proposer.personalInfo.cid');
    let phCompleted = this.validatePhCompletedValues(error, proposerCid);
    let isCompletedBeforePh = _.get(changedValues, 'proposer.extra.isCompleted');
    updateParentChangedValues = phCompleted && !isCompletedBeforePh || !phCompleted && isCompletedBeforePh;
    changedValues.proposer.extra.isCompleted = phCompleted;
    if (!phCompleted) {
      changedValues.proposer.extra.isPdfGenerated = false;
    }

    // Check LA Tabs and update parent component
    let completedLAForms = this.validateLACompletedValues(error);
    let changedValuesInsuredArray = _.cloneDeep(_.get(changedValues, 'insured'));
    if (changedValuesInsuredArray) {
      changedValuesInsuredArray.forEach((value, index)  => {
        let cid = _.get(value, 'personalInfo.cid');
        let laCompleted = completedLAForms.indexOf(cid) > -1;
        let isCompletedBeforeLa = _.get(changedValues, `insured.[${index}].extra.isCompleted`);
        if (!updateParentChangedValues) {
          updateParentChangedValues = laCompleted && !isCompletedBeforeLa || !laCompleted && isCompletedBeforeLa;
        }
        changedValues.insured[index].extra.isCompleted = laCompleted;
        if (!laCompleted) {
          changedValues.insured[index].extra.isPdfGenerated = false;
        }
      });
    }

    let laAllCompleted = false;
    if (completedLAForms.length === _.get(changedValues, 'insured', []).length) {
      laAllCompleted = true;
    }

    let laPersonInfoCompleted = this.validateLACompletedPersonalInfoValues(error);

    if (updateParentChangedValues) {
      handleChangedValues(changedValues);
    }

    let enableNextStep = isPolicyNumberGenerated && phCompleted && laPersonInfoCompleted;
    let isCompleted = enableNextStep && laAllCompleted;

    actions.validate(this.context, isCompleted, enableNextStep);
    this.setState({error});
  }

  updateCompleteMenu = (error, values) =>{
    if (!values.completedMenus){
      values.completedMenus = [];
    }
    let completedMenus = values.completedMenus;
    let menus = values.menus || [];
    for (let mIndex = 0; mIndex < menus.length; mIndex++){
      let menu = menus[mIndex];
      if (error && error[menu] && error[menu].code){
        let cmIndex = completedMenus.indexOf(menu);
        if (cmIndex > -1){
          completedMenus.splice(cmIndex, 1);
        }
      } else {
        if (completedMenus.indexOf(menu) === -1){
          completedMenus.push(menu);
        }
      }
    }
  }

  render() {
    let { shieldApplication } = this.context.store.getState();
    let { template, application } = shieldApplication;
    let { handleChangedValues } = this.props;
    let { changedValues, error } = this.state;

    let values = _.get(application, 'applicationForm.values');

    return (
      <div style={{display: 'flex', minHeight: '0px', flexGrow: 1, flexDirection: 'column', height: '100%'}}>
        <div style={{display: 'flex', minHeight: '0px', flexGrow: 1, flexDirection: 'column', overflowY: 'auto'}}>
          <DynColumn
            customStyle={{flexGrow: 1}}
            onMenuItemChange={this.onMenuItemChange}
            isApp={true}
            isShield
            template={template}
            changedValues={changedValues}
            error={error}
            values={values}
            validate={this.validate}
            completeChangedValues={() => {
              handleChangedValues(changedValues);
            }}
            updateCompleteMenu={this.updateCompleteMenu}
            extra={{checkAddr: this.checkAddr, copyAddr: this.copyAddr}}
          />
        </div>
      </div>
    );
  }
}

export default ShieldAppForm;
