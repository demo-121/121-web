import React from 'react';
import EABComponent from '../../../../Component';
import {Dialog, TextField, FlatButton, MenuItem, SelectField} from 'material-ui';
import Appbar from '../../../../CustomViews/AppBar';
import styles from '../../../../Common.scss';
import {addOtherDocument} from '../../../../../actions/supportDocuments';
import * as _ from 'lodash';

class OtherDocDialog extends EABComponent {

    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.state, {
            open: false,
            appbar: {
                menu:{
                    type:'flatButton',
                    title:{
                        'en':'Cancel'
                    },
                    action: ()=>{
                        this.closeDialog();
                    }
                },
                title:'Add Other Document',
                items:[
                    [{
                        type:'flatButton',
                        title:{
                            'en': 'Done'
                        },
                        disabled: true,
                        action: ()=>{
                            this.submit();
                        }
                    }]
                ]
            },
            docNameOption:'',
            docName:'',
            containsRegExp: false,
            docNameDuplicated: false
        });
    }

    updateAppBar = (doneDisabled) => {
        this.setState({
            appbar: {
                menu:{
                    type:'flatButton',
                    title:{
                        'en':'Cancel'
                    },
                    action: ()=>{
                        this.closeDialog();
                    }
                },
                title:'Add Other Document',
                items:[
                    [{
                        type:'flatButton',
                        title:{
                            'en': 'Done'
                        },
                        disabled: doneDisabled,
                        action: ()=>{
                            this.submit();
                        }
                    }]
                ]
            }
        });
    }

    handleTextChange = (event)=>{
        if (event.target.value.length === 0) {
            this.updateAppBar(true);
            this.setState({
                containsRegExp:false,
                docName: event.target.value
            });
        } else {
            // Do alphanumberic checking
            let patt = /[^a-z0-9 ]/i;
            let containsRegExp = patt.test(event.target.value);
            if (containsRegExp) {
                this.updateAppBar(true);
                this.setState({
                    containsRegExp:true,
                    docName: event.target.value
                });
            } else {
                let value = event.target.value;
                if (value && typeof(value) === 'string' && value.trim() === '') {
                  this.updateAppBar(true);
                }
                else {
                  this.updateAppBar(false);
                }
                this.setState({
                    containsRegExp:false,
                    docName: event.target.value
                });
            }
        }
    }

    openDialog=(infoDic)=>{
        this.setState({
            open: true
        });
    }

    closeDialog=()=>{
        this.setState({
            open: false,
            docName:'',
            docNameOption:'',
            containsRegExp: false
        });
        this.updateAppBar(true);
    }

    isDuplicateDocName = (docName) => {
        let {defaultDocNameList} = this.props;
        return _.includes(defaultDocNameList, docName);
    }

    submit = () =>{
        let {values, rootValues, appId, tabId} = this.props;
        let {docName,docNameOption} = this.state;
        if (!this.isDuplicateDocName(docName)) {
            addOtherDocument(this.context, values, docName,docNameOption , appId, tabId, rootValues, (resp)=>{
                if (resp.duplicated) {
                    this.setState({docNameDuplicated: true});
                }
                this.closeDialog();
            });
        } else {
            this.closeDialog();
            this.setState({docNameDuplicated: true});
        }
    }

    getDocOptionsView(options){
      let optionsView = [];
      _.each(options, (option) => {
        let optionId = 'otherDocPicker-' + option.title;
        optionsView.push(
          <MenuItem
            id = {optionId}
            key={option.title}
            value={option.title}
            primaryText={option.title}
          />
        );
      });
      return optionsView;
    }
    getPickerField(docNameOption,options){
      return <SelectField
        id="otherDocPicker"
        key="otherDocPicker"
        autoWidth
        value={docNameOption}
        onChange={(e, index, value) => this.onSelectionChange(value)}
        hintText='Select'
      >
      {
        (window.isMobile.apple.phone) ?
        <div style={{width: '100vw', overflowX: 'auto'}}>
        {this.getDocOptionsView(options)}
        </div>
        :
        this.getDocOptionsView(options)
        }
      </SelectField>
    }
    onSelectionChange(value){
      let {docName, docNameOption} = this.state;
      if (value) {
        if (docNameOption !== value) {
          if (value === 'Other' && docName.trim() === '') {
            this.updateAppBar(true);
          } else {
            this.updateAppBar(false);
          }

          if (value !== 'Other') {
            this.setState({
              docNameOption: value,
              docName: value
            });
          } else {
            this.setState({
              docNameOption: value,
              docName: ''
            });
          }

        }
      }
    }

    checkIsMandatory(docNameOption, docName) {
      if (typeof(docNameOption) === 'string' && typeof(docName) === 'string'){
        if (docNameOption === 'Other' && docName.trim() === '') {
          return true;
        }
      }
      return false;
    }

    showDocNameField(docNameOption){
      if (typeof(docNameOption) === 'string' && docNameOption === 'Other'){
        return true;
      }
      return false;
    }

    render() {
        let {appbar ,open, containsRegExp} = this.state;

        let {supportDocDetails, tabId} = this.props;
        let {docName, docNameOption} = this.state;

        let isMandatory = this.checkIsMandatory(docNameOption, docName);
        let isDocNameFieldShow = this.showDocNameField(docNameOption);
        let iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

        let options = [];
        if (supportDocDetails && supportDocDetails.otherDocNameList){
          let otherDocNames = supportDocDetails.otherDocNameList;
          if (otherDocNames.ph && (tabId === 'policyForm' || tabId === 'proposer')) {
            options = otherDocNames.ph;
          } else if (otherDocNames.la && tabId === 'insured') {
            options = otherDocNames.la;
          } else {
            options = otherDocNames.la;
          }
        }


            if(iOS){
                return (
                <div>
                <Dialog
                    title="WARNING"
                    open={this.state.docNameDuplicated}
                    actions={[
                        <FlatButton
                            label='OK'
                            primary={true}
                            onTouchTap={()=>this.setState({docNameDuplicated: false})}
                        />
                    ]}>
                    There is already a document with the same name in this application. Please use another name.
                </Dialog>
                <Dialog titleClassName={styles.OtherDocDialogtitle} open={open} bodyStyle={{padding: '24px'}} contentStyle={{width: '100%'}}
                    title={<div style={{padding:'0px'}}><Appbar ref={ref=>{this.appbar=ref}} showShadow={false} template={appbar} style={{background: '#FAFAFA', paddingLeft: '0px !important'}}/><div className={styles.Divider}/></div>}
                >
                    <div style={{height:'18vh'}}>
                    <div style={{display:'block'}}>
                    {this.getPickerField(docNameOption,options)}</div>
                    {isDocNameFieldShow ?
                    <div style={{display:'block',position:'absolute'}}>
                    <TextField
                        id = 'otherDocNameText'
                        key = 'otherDocNameText'
                        value={docName}
                        onChange={this.handleTextChange}
                        errorText={isMandatory ? 'This field is required' : ''}
                        maxLength="20"/>
                    </div> : null}
                    <div>
                    {containsRegExp?<div style={{fontSize:'10px',color:'red'}}>Invalid letter</div>:null}


                    </div>
                    </div>
                </Dialog>
            </div>
            );
            }else{
                return (
                <div>
                <Dialog
                    title="WARNING"
                    open={this.state.docNameDuplicated}
                    actions={[
                        <FlatButton
                            label='OK'
                            primary={true}
                            onTouchTap={()=>this.setState({docNameDuplicated: false})}
                        />
                    ]}>
                    There is already a document with the same name in this application. Please use another name.
                </Dialog>
                <Dialog titleClassName={styles.OtherDocDialogtitle} open={open} bodyStyle={{padding: '24px'}}
                    title={<div style={{padding:'0px'}}><Appbar ref={ref=>{this.appbar=ref}} showShadow={false} template={appbar} style={{background: '#FAFAFA', paddingLeft: '0px !important'}}/><div className={styles.Divider}/></div>}
                >
                    <div>
                    <div style={{display:'inline-block'}}>
                    {this.getPickerField(docNameOption,options)}</div>
                    {isDocNameFieldShow ?
                    <div style={{display:'inline-block',position:'absolute',paddingLeft:'24px'}}>
                    <TextField
                        id = 'otherDocNameText'
                        key = 'otherDocNameText'
                        value={docName}
                        onChange={this.handleTextChange}
                        errorText={isMandatory ? 'This field is required' : ''}
                        maxLength="20"/>
                    </div> : null}
                    <div>
                    {containsRegExp?<div style={{fontSize:'10px',color:'red'}}>Invalid letter</div>:null}


                    </div>
                    </div>
                </Dialog>
            </div>
            );
            }
    }
}

export default OtherDocDialog;
