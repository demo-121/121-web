import React from "react";
import PropTypes from 'prop-types';
import {CircularProgress } from 'material-ui';
import EABComponent from '../../../../Component';
import {getImage} from '../../../../../actions/GlobalActions';
import {getIcon} from '../../../../Icons/index';
import styles from '../../../../Common.scss';

class SuppDocsImageView extends EABComponent {
  constructor(props) {
      super(props);
      this.state = Object.assign( {}, this.state, {
        image:  null,
        docId: props.docId,
        attId: props.attId,
        errMsg: null
      });
  };

  componentDidMount() {
    // let {docId, attId} = this.props;
    // if(!this.state.image)
      // this.loadImage(docId, attId);
    this.unmounted = false;
  }

  // componentWillReceiveProps(nextProps){
  //   let {docId, attId} = nextProps;
    // if(!isEqual(this.props.docId, docId) || !isEqual(this.props.attId, attId))
    //   this.loadImage(docId, attId);
  // }

  componentWillUnmount() {
    this.unmounted = true;
  }

  // loadImage=(docId, attId)=>{
  //   let {langMap} = this.context;
  //   let {fileType} = this.props;
  //   if (!docId || !attId)
  //     return;

  //   if (fileType === 'application/pdf')
  //     return;

  //   this.setState({errMsg: null, image: null});

  //   getImage(
  //     this.context,
  //     docId,
  //     attId,
  //     (image)=>{
  //       if (!this.unmounted) {
  //         if(image){
  //           this.setState({
  //             image: image
  //           })
  //         } else {
  //           this.setState({
  //             errMsg: getLocalizedText(langMap, 'Image cannot find')
  //           })
  //         }
  //       }
  //     }
  //   )
  // }

  render(){
    let {image, docId, attId, errMsg} = this.state;
    let {onItemClick, style, fileType, canScroll, token} = this.props;
    let {muiTheme, store} = this.context;
    let strScroll = canScroll ? 'scroll' : '';

    return (
      // <div className={styles.ImageView} style={{overflow: strScroll}} onTouchTap={typeof onItemClick == "function"?onItemClick:null}>
      <div style={Object.assign({overflow: strScroll}, canScroll ? {flexGrow: 1} : {})} onTouchTap={typeof onItemClick == "function"?onItemClick:null}>
        {
          fileType === 'application/pdf' ? <div>{getIcon('pdf', muiTheme.palette.primary2Color)}</div> :
            <img key={`img-${docId}-${attId}`} src={ window.genAttPath(token) } style={style}/>
        }
      </div>
    )
  }
}

SuppDocsImageView.propTypes = Object.assign({}, SuppDocsImageView.propTypes, {
  onItemClick: PropTypes.func, 
  style: PropTypes.object, 
  imageStyle: PropTypes.object
});

export default SuppDocsImageView;
