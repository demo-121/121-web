import React from 'react';
import DynColumn from '../../../../DynViews/DynColumn';
import EABComponent from '../../../../Component';
import SystemConstants from '../../../../../constants/SystemConstants';
import CircularProgress from 'material-ui/CircularProgress';
import {goSupportDocuments} from '../../../../../actions/supportDocuments.js';

import * as actions from '../../../../../actions/payment';

import * as _v from '../../../../../utils/Validation';
import * as _ from 'lodash';

import styles from '../../../../Common.scss';
import {getIcon} from '../../../../Icons/index';

class PaymentMain extends EABComponent {
  constructor(props) {
    super(props);
    this.showDialog = this.showDialog.bind(this);
    this.state = Object.assign({}, this.state, {
      template: {},
      values: {},
      changedValues: {},
      error: {},
      isMandDocsAllUploaded: false,
      isSubmitted: false,
      showDialog: false
    });
  }

  showDialog(status) {
    if(status != this.state.showDialog) {
      this.setState({
        showDialog: status
      })
    }
  }

  componentWillMount() {
    let {
      appForm
    } = this.context.store.getState();

    let docId = appForm.application.id;

    actions.initPayStore(this.context, docId);
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    let {muiTheme} = this.context;

    let {
      changedValues
    } = this.state;

    let {
      appForm
    } = this.context.store.getState();

    this.props.updateAppBarTitle(SystemConstants.EAPP.STEP.PAYMENT);
    this.validate(changedValues, true);
  };

  componentDidUpdate(prevProps, prevState) {
    if (window.isEmpty(prevState.template) && !window.isEmpty(this.state.template)
      || this.state.changedValues != prevState.changedValues ) {
      this.validateAfterInitStore();
    }
  }

  componentWillUnmount(){
    this.unsubscribe();
    actions.closePaymentPage(this.context);
  };

  storeListener=()=>{
    let newState = {};
    let {
      store, validRefValues, langMap, optionsMap
    } = this.context;

    let {
      payment
    } = store.getState();

    let {
      template,
      values,
      // changedValues,
      isMandDocsAllUploaded,
      isSubmitted
    } = this.state;

    if (!window.isEqual(values, payment.values) && payment.values.proposalNo !== ''){
      newState.values = payment.values;
      newState.changedValues = payment.values;
    }

    if (isMandDocsAllUploaded != payment.isMandDocsAllUploaded) {
      newState.isMandDocsAllUploaded = payment.isMandDocsAllUploaded;
    }

    if (isSubmitted != payment.isSubmitted) {
      newState.isSubmitted = payment.isSubmitted;
    }

    if (!window.isEqual(template, payment.template) && payment.values.proposalNo !== ''){
      newState.template = payment.template;
    }

    if (!window.isEmpty(newState)) {
      this.setState(newState, () => {
        // force validate the changes after state was changed
        this.validateAfterInitStore();
      });
    }
  };

  validateAfterInitStore=()=>{
    let {
      changedValues
    } = this.state;

    this.validate(changedValues, false);
  }

  getAppBarItemsIndex=(completed)=>{
    let itemsIndex = -1;
    let { isMandDocsAllUploaded, isSubmitted } = this.state;

    //See the representation of itemsIndex in ../../AppForm/Main getAppBarItems()
    if (completed || isSubmitted) {
      itemsIndex = isMandDocsAllUploaded ? 3 : 1;
    } else {
      itemsIndex = isMandDocsAllUploaded ? 2 : 0;
    }
    return itemsIndex;
  }

  //Called by parent
  goSuppDocs=(appId)=>{
    let { values } = this.state;
    let { appForm } = this.context.store.getState();

    let docId = appForm.application.id;

    actions.updatePaymentMethods(this.context, docId, values, this.context.stepperIndex, false, () => {
      goSupportDocuments(this.context, appId);
    });
  }

  //Called by parent
  saveBeforeChangeStepperIndex=(nextIndex, callback)=>{
    let {
      values
    } = this.state;
    let {
      store
    } = this.context;
    let {
      appForm,
      payment
    } = store.getState();

    let docId = appForm.application.id;

    actions.updatePaymentMethods(this.context, docId, values, this.context.stepperIndex, nextIndex == 3, () => {
      if (_.isFunction(callback)){
        callback();
      }
    });
  }

  nextOnClick=()=>{
    this.context.goStep(this.context.stepperIndex + 1, true);
  }

  getStepperPageIndex=(error, init)=>{
    let {
      appForm
    } = this.context.store.getState();
    let valid = !error.code && this._checkPaymentComplete();

    let completed = appForm.application.appStep > this.context.stepperIndex? appForm.application.appStep - 1: this.context.stepperIndex - 1;
    let active = appForm.application.appStep > this.context.stepperIndex? appForm.application.appStep: this.context.stepperIndex;

    if (!init) {
      completed = valid ? SystemConstants.EAPP.STEP.PAYMENT : SystemConstants.EAPP.STEP.SIGNATURE;
      active = valid ? SystemConstants.EAPP.STEP.SUBMISSION : SystemConstants.EAPP.STEP.PAYMENT;
    }

    if (this.state.isSubmitted) {
      completed = SystemConstants.EAPP.STEP.SUBMISSION;
    }

    return {completed, active};
  }

  validate=(changedValues, init)=>{
    let {langMap, optionsMap} = this.context;
    let {template, values} = this.state;

    let error = {};
    _v.exec(template, optionsMap, langMap, changedValues, changedValues, changedValues, error);
    let {completed, active} = this.getStepperPageIndex(error, init);
    let valid = !error.code && this._checkPaymentComplete();

    let itemsIndex = {itemsIndex:this.getAppBarItemsIndex(!init && valid)};
    this.context.updateAppbar(Object.assign({}, itemsIndex));
    this.context.updateStepper(Object.assign({}, {completed, active}));
    this.setState({changedValues, values, template, error});
  }

  _updateAppbar=(valid, index)=>{
    if(!index) index = this.state.index;
    let {lastIndex} = this.state;

    this.context.updateAppbar({
      itemsIndex: index==lastIndex? 2 : (valid? 1: 0)
    });
  }

  _checkPaymentComplete = () => {
    let {
      values,
      changedValues
    } = this.state;

    if (changedValues.initPayMethod) {
      var payMethod = changedValues.initPayMethod;
      var trxStatus = changedValues.trxStatus;
      var trxTimestamp = changedValues.trxTime;
      var trxNo = changedValues.trxNo;

      if (_.findIndex(['crCard', 'eNets', 'dbsCrCardIpp'], (ele) => {return ele == payMethod}) >= 0) {
        if (trxNo && trxStatus == 'Y' && trxTimestamp) {
          return true;
        }
      } else {
        return true;
      }
    }
    return false;
  }

  render() {
    let {
      template,
      values,
      changedValues,
      error
    } = this.state;

    return (
      <div className={styles.PaymentSubmitPage}>
        <DynColumn
          template={template}
          values={values}
          error={error}
          changedValues={changedValues}
          validate={this.validate}
          ccy={_.get(values, 'policyCcy', 'SGD')}
          showDialog ={this.showDialog}
        />
        {this.state.showDialog ? <div key="PaymentLoadingBlock" id="PaymentLoadingBlock" style={{
              zIndex: 5000,
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100vh',
              display: 'flex', minHeight: '0px',
              alignItems: 'center',
              justifyContent: 'center'
            }}>
          <CircularProgress key="PaymentLoading" ref="PaymentLoading" title={"Payment in Progress"}/>
        </div> : null}
      </div>
    );
  }
}

export default PaymentMain;
