import React from 'react';
import EABComponent from '../../../../Component';

import AppbarPage from '../../../AppbarPage';
import StepperPage from '../../../StepperPage';
import ClientChoiceMain from './Main';

class ClientChoice extends EABComponent {
  render() {
    return (
      <AppbarPage>
        <StepperPage>
          <ClientChoiceMain/>
        </StepperPage>
      </AppbarPage>
    )
  }
}

export default ClientChoice
