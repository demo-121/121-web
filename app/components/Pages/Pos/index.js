import React, { Component, PropTypes } from 'react';
import FloatingPage from '../FloatingPage';
import AppbarPage from '../AppbarPage';
import Main from './Main';

import EABComponent from '../../Component';

class Pos extends EABComponent {
  open=(page)=>{
    this.main.init(page);
    this.floatingPage.open();
  }

  close=()=>{
    this.floatingPage.close();
  }

  render() {
    return (
      <FloatingPage ref={ref=>{this.floatingPage=ref}}>
        <AppbarPage id="posAppbar" showShadow={false}>
          <Main ref={ref=>{this.main=ref}}/>
        </AppbarPage>
      </FloatingPage>
    );
  }
}


export default Pos;