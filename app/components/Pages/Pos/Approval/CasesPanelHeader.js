import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from '../../../Common.scss';

import styled, {css} from 'styled-components';

import EABPickerField from '../../../CustomViews/PickerField';

const PickerFieldWrapper = styled.div`
    position: relative;
    top: 10px;

    ${
        props => props.iphone && css `
            top: 5px;
        `
    }
`;

class CasesPanelHeader extends PureComponent{
    render(){
        let { headerTemplate, sortSearchCase, searchedCases, handleSortingChange} = this.props;
        return (
            <div className={styles.EApprovalHeader} key='key-approval-cases-header'>
                <div className={styles.EApprovalHeaderComp1} key={'key-approval-cases-header1'}>
                    <span className={styles.bold}>{searchedCases.length}</span> <span className={styles.subTextTitle}> records found</span>
                </div>
                <PickerFieldWrapper iphone={window.isMobile.apple.phone}>
                    <EABPickerField
                        key={'key-approval-cases-header2'}
                        id = {headerTemplate.id}
                        itemId={headerTemplate.id}
                        template = {headerTemplate}
                        values = {{sortSearchCase}}
                        changedValues = {{sortSearchCase}}
                        handleChange = {handleSortingChange}
                    />
                </PickerFieldWrapper>
            </div>
        );
    }
}

CasesPanelHeader.propTypes = {
    headerTemplate: PropTypes.object,
    items: PropTypes.array,
    searchedCases: PropTypes.array,
    sortSearchCase: PropTypes.object,
    handleSortingChange: PropTypes.func
};

export default CasesPanelHeader;
