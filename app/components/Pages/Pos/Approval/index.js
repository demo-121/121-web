import React, { PropTypes } from 'react';
import EABComponent from '../../../Component';

import FloatingPage from '../../FloatingPage';
import AppbarPage from '../../AppbarPage';

import DynColumn from '../../../DynViews/DynColumn';
import Main from './main';

import Travel from '../../../../img/Travel.svg';
import Car from '../../../../img/Car.png';
import Golfer from '../../../../img/Golfer.svg';
import DomesticHelper from '../../../../img/Domestic_helper.svg';

import styles from '../../../Common.scss';

import {getPendingForApprovalCaseListLength, resetFilter, openApprovalPage, resetFilterAndCloseApprovalPage} from '../../../../actions/approval';
import _isEqual from 'lodash/fp/isEqual';

import WarningDialog from '../../../Dialogs/WarningDialog';

import AppForm from '../Applications/AppForms';

import SupportDocPage from '../Applications/UploadDocument/index';

import MobileSearchMenu from './MobileSearchMenu';

import * as _ from 'lodash';

export default class approval extends EABComponent {
    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.state, {
          showPendingForApprovalDialog: false,
          waitingForPendingApprovalCount: 0,
          currentPage: '',
          openGi: false,
          openTi: false,
          openApplication: false,
          openSuppDocsPage: false,
          openFilterDialog: false
        });
    }

    componentDidMount(){
        this.unsubscribe = this.context.store.subscribe(this.storeListener);
        this.storeListener();
    }

    storeListener= ()=>{
        if(this.unsubscribe){
            let {store} = this.context;
            let { pos} = store.getState();
            let newState = {};

            if(pos.currentPage && this.state.currentPage !== pos.currentPage){
            newState.currentPage =  pos.currentPage;
            }

            if(!_.isEqual(this.state.openSuppDocsPage, pos.openSuppDocsPage)) {
                newState.openSuppDocsPage = pos.openSuppDocsPage;
            }

            if(!window.isEmpty(newState)){
            this.setState(newState);
            }
        }
    }

    componentWillUnmount(){
        if(isFunction(this.unsubscribe)){
            this.unsubscribe();
            this.unsubscribe = undefined;
        }
    }

    open(title, action) {
        this.updateAppbar(title);
        this.main.init(action);
        this.floatingPage.open();
        this.setState({
          openApplication: true
        })
    }

    openFilter() {
        this.setState({
            openFilterDialog: true
        });
    }

    closeFilter() {
        this.setState({
            openFilterDialog: false
        });
    }

    closeFilterDialog() {
        this.setate({
            openFilterDialog: false
        });
    }

    close() {

        if (this.state.openTi && this.state.openGi) {
            this.setCloseTi();
            return;
        } else if (this.state.openGi) {
            this.setCloseGi();
        }
        this.floatingPage.close();
        resetFilterAndCloseApprovalPage(this.context, false);
        this.setState({
          openApplication: false
        });
    }

    handleClose = () => {
        this.setState({showPendingForApprovalDialog: false});
    };

    onClickApproval = () => {
        openApprovalPage(this.context, true);
        this.setState({
            showPendingForApprovalDialog: false
        });
    };

    componentWillMount() {
        const {app} = this.context.store.getState();
        const {agentProfile} = app;
        getPendingForApprovalCaseListLength(this.context, (length) => {
            if (_isEqual(length, 0) || !agentProfile.allowToEnterApproval) {
                return;
            }
            this.setState({
                showPendingForApprovalDialog: true,
                waitingForPendingApprovalCount: length
            });
        });
    }

    updateAppbar(title) {
        const {langMap} = this.context;
        this.appbarPage.initAppbar({
        menu: {
            icon: 'back',
            action: () => {
            this.close();
            }
        },
        titleStyle: (window.isMobile.apple.phone) ? {width: '100%', fontWeight: 500, color: 'black'} : undefined,
        title: title,
        itemsIndex: 0,
        items: (window.isMobile.apple.phone) ?
        [
            [
                {
                    id: 'openMobileFilter',
                    type: 'flatButton',
                    title: window.getLocalizedText(langMap, 'menu.eapproval.openMobileFilter', 'Filter'),
                    action: (e)=>{
                        if (e && _.isFunction(e.preventDefault) && _.isFunction(e.stopPropagation)) {
                            e.preventDefault();
                            e.stopPropagation();
                        }
                         
                        this.openFilter();
                    }
                }
            ]
        ]
         :
        [
            [
                {
                    id: 'resetFilter',
                    type: 'flatButton',
                    title: window.getLocalizedText(langMap, 'menu.eapproval.resetfilter', 'Reset All Filter'),
                    action: ()=>{
                        resetFilter(this.context);
                    }
                }
            ]
        ]
        });
    }

    renderGIBox = (svgImg, title, regName = 'general') => {
        return (
            <div className={`${styles.GIBox} ${styles[regName]}`} id={`GIBox_${title}`} onClick={() => {
                if (regName === "Travel" && !this.state.openTi) {
                    this.setOpenTi();
                }
            }}>
                {/* <div style={{display: "block", width: "100%"}}> */}
                    <img className={styles.giImage} src={svgImg} />
                    <div id={`${title}_Title`}>{title}</div>
                {/* </div> */}
            </div>
        );
    }

    setOpenTi() {
        let title='Travel Insurance';
        this.floatingPage.open();
        this.setState({
            openTi: true,
            openApplication: true
        });
        this.appbarPage.initAppbar({
            menu: {
                icon: 'back',
                action: () => {
                this.close();
                }
            },
            titleStyle: (window.isMobile.apple.phone) ? {width: '100%', fontWeight: 500, color: 'black'} : undefined,
            title: title,
            itemsIndex: 0
        });
    }
    setOpenGi() {
        let title='General Insurance';
        this.floatingPage.open();
        this.setState({
            openGi: true,
            openApplication: true
        });
        this.appbarPage.initAppbar({
            menu: {
                icon: 'back',
                action: () => {
                this.close();
                }
            },
            titleStyle: (window.isMobile.apple.phone) ? {width: '100%', fontWeight: 500, color: 'black'} : undefined,
            title: title,
            itemsIndex: 0
        });
    }

    setCloseGi() {
        this.setState({
            openGi: false
        });
    }

    setCloseTi() {
        this.appbarPage.initAppbar({
            menu: {
                icon: 'back',
                action: () => {
                this.close();
                }
            },
            titleStyle: (window.isMobile.apple.phone) ? {width: '100%', fontWeight: 500, color: 'black'} : undefined,
            title: "General Insurance",
            itemsIndex: 0
        });
        this.setState({
            openTi: false
        });
    }

    renderGIPage() {
        return (
            <div id="GI-page">
                  <div className={styles.GIBoxContainer} id="GIBoxContainer">
                    {this.renderGIBox(Travel, "Travel Insurance", "Travel")}
                    {this.renderGIBox(Car, "Car Insurance")}
                    {this.renderGIBox(DomesticHelper, "Domestic Helper Insurance")}
                    {this.renderGIBox(Golfer, "Golfer's Insurance")}
                  </div>
              </div>
        )
    }

    getPage() {
        const { openTi, openGi, openApplication, openFilterDialog } = this.state;
        if (openTi) {
            return <iframe
            style={{ border: 0, width: "100%", height: "100%", position: "relative" }}
            src={"http://display-sit.eab-cafe.k8s-inno1/preview/5d9419447eb193001a720381"}
            title="cafe-display"
            id="cafe-display"
            // ref={(ref) => this.frameRef = ref}
            onLoad={() => {
              // this.frameRef.contentWindow.postMessage("Testing", "*")
            }}
          />
        }
        if (openGi) {
            return this.renderGIPage();
        }
        return <Main ref={ref => {this.main = ref;}} openApplication={openApplication} openFilterDialog={openFilterDialog} closFilterDialog={() => {this.closeFilter();}} resetFilter={() => {resetFilter(this.context);}}/>
    }
    render() {
      let {currentPage, openApplication, openSuppDocsPage, openFilterDialog, openGi} = this.state;
      let renderItem;
      let secItem, supportDoc;
      if(openSuppDocsPage && openApplication){
        supportDoc = <SupportDocPage isShield={this.context.store.getState().pos.isShield || false}/>;
      }
        renderItem = (<FloatingPage ref={ref => { this.floatingPage = ref; }}>
                    <AppbarPage ref={ref => { this.appbarPage = ref; }}>
                        {this.getPage()}
                        {/* <Main ref={ref => {this.main = ref;}} openApplication={openApplication} openFilterDialog={openFilterDialog} closFilterDialog={() => {this.closeFilter();}} resetFilter={() => {resetFilter(this.context);}}/> */}
                        {supportDoc}
                    </AppbarPage>
                    <WarningDialog
                        open={this.state.showPendingForApprovalDialog}
                        hasTitle={false}
                        msgElement={<PendingForApprovalCountMessage waitingForPendingApprovalCount={this.state.waitingForPendingApprovalCount} onClickApproval={this.onClickApproval} />}
                        onClose={this.handleClose}
                    />
                </FloatingPage>)
        return (
          <div>{renderItem}</div>
        );
    }
}

class PendingForApprovalCountMessage extends EABComponent {

    static propTypes = {
        waitingForPendingApprovalCount: PropTypes.number,
        onClickApproval: PropTypes.func
    };

    render() {
        const { waitingForPendingApprovalCount, onClickApproval } = this.props;
        return (
            <div>
                You have {waitingForPendingApprovalCount} case(s) pending for approval, please <a href="#" onClick={onClickApproval}>click here</a> to view.
            </div>
        );
    }
}
