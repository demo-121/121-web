export const APPRVOAL_STATUS_APPROVED = 'A';
export const APPRVOAL_STATUS_REJECTED = 'R';
export const APPRVOAL_STATUS_EXPIRED = 'E';
export const APPRVOAL_STATUS_SUBMITTED = 'SUBMITTED';
export const APPRVOAL_STATUS_PDOC = 'PDoc';
export const APPRVOAL_STATUS_PDIS= 'PDis';
export const APPRVOAL_STATUS_PFAFA = 'PFAFA';
export const APPRVOAL_STATUS_PDOCFAF = 'PDocFAF';
export const APPRVOAL_STATUS_PDISFAF = 'PDisFAF';
export const APPRVOAL_STATUS_PCDAA = 'PCpaA';
export const APPRVOAL_STATUS_PDOCCda = 'PCpaA';
export const APPRVOAL_STATUS_PDISCda = 'PCpaA';
export const INPROGRESS_APPLICATION = 'inProgressApp';
export const INPROGRESS_BI = 'inProgressBI';

export const STATUS = {
    A:          'Approved',
    R:          'Rejected',
    E:          'Expired',
    SUBMITTED:  'Pending Supervisor Approval',
    PDoc:       'Pending Document',
    PDis:       'Pending Discussion',
    PFAFA:      'Pending FA firm approval',
    PDocFAF:    'Pending Document (FA Firm)',
    PDisFAF:    'Pending Discussion (FA Firm)',
    PCdaA:      'Pending CDA approval',
    PDocCda:    'Pending Document (CDA)',
    PDisCda:    'Pending Discussion (CDA)',
    inProgressBI: 'In Progress PI',
    inProgressApp: 'In Progress Application'
};
