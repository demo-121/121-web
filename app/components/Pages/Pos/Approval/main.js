import React from 'react';
import EABComponent from '../../../Component';
import DynColumn from '../../../DynViews/DynColumn';

import isFunction from 'lodash/isFunction';
import get from 'lodash/get';
import * as _ from 'lodash';

import styles from '../../../Common.scss';
import EABPickerField from '../../../CustomViews/PickerField';

import { SET_APPROVAL_TEMPLATE, SET_WORKBENCH_TEMPLATE, searchApprovalCases, searchWorkbenchCases, genSupervisorPdf, getDocAction } from '../../../../actions/approval';

import { getProposalByQuotId } from '../../../../actions/quotation';

import { goApplication, POS_EAPP } from '../../../../actions/application';

import * as shieldActions from '../../../../actions/shieldApplication';

import Proposal from '../Proposal/index';

import  ReviewPage from './reviewPage';

import SearchMenu from './searchMenu';

import MobileSearchMenu from './MobileSearchMenu';

import CasesPanel from './CasesPanel';

import CasesPanelHeader from './CasesPanelHeader';

import AppForm from '../Applications/AppForms';

import ApprovalUtils from '../../../../../common/ApprovalUtils';

import {APPRVOAL_STATUS_APPROVED, APPRVOAL_STATUS_REJECTED} from '../../../../../bz/model/approvalStatus';

export const USING_PENDING_APPROVAL_TEMPLATE = 'A';
export const USING_WORKBENCH_TEMPLATE_USED = 'W';

export default class Main extends EABComponent{
    constructor(props, context){
        super(props);
        let {approval} = context.store.getState();
        this.state = Object.assign({}, this.state, {
                template: {},
                approvalFilter: approval.approvalFilter || {},
                workbenchFilter: approval.workbenchFilter || {},
                caseTemplate: approval.caseTemplate || {},
                searchedCases: [],
                searchedApprovalCases: approval.searchedApprovalCases || [],
                searchedWorkbenchCases: approval.searchedWorkbenchCases || [],
                reviewCases: {},
                openReviewPage: false,
                sortApprovalSearchCase: 'oldTonew',
                sortWorkbenchSearchCase: 'newToold',
                currentTemplate: undefined,
                currentFilter: [],
                currentPage: ''
            }
        );
    }

    shouldComponentUpdate(nextProps, nextState) {
        let {store} = this.context;
        let {approval} = store.getState();

        if (!window.isEqual(_.get(nextState, 'reviewCases.onHoldReason'), _.get(approval, 'approvalCase.onHoldReason'))
            || !window.isEqual(_.get(nextState, 'reviewCases.approvalStatus'), _.get(approval, 'approvalCase.approvalStatus'))
            || !window.isEqual(_.get(nextState, 'reviewCases.approvalCaseId'), _.get(approval, 'approvalCase.approvalCaseId'))
            || _.isEmpty(this.state.reviewCases)
            || _.isEmpty(approval)
            || this.ReviewPage.floatingPage.state.open === false)  {
            return true;
        } else {
            return false;
        }
    }

    componentDidMount(){
        this.unsubscribe = this.context.store.subscribe(this.storeListener);
        this.storeListener();
    }

    findCaseAction= () =>{
        let {approvalFilter, workbenchFilter} = this.state;
        let {approval} = this.context.store.getState();
        if (approval.currentTemplate === USING_PENDING_APPROVAL_TEMPLATE) {
            searchApprovalCases(this.context, approvalFilter, () => {} );
        } else if(approval.currentTemplate === USING_WORKBENCH_TEMPLATE_USED){
            searchWorkbenchCases(this.context, workbenchFilter, () => {} );
        }
        if (window.isMobile.apple.phone) {
            this.props.closFilterDialog();
        }
    }

    replaceTemplateAction = (templateItems, key, replace) => {
        _.forEach(templateItems, obj => {
            if (obj[key] === replace && replace === 'findCasesByFilter') {
              obj[key] = this.findCaseAction;
            } else if (obj[key] === replace && replace === 'openReviewPage') {
              obj[key] = this.handleClickEvent;
            } else if (!_.isEmpty(_.get(obj, 'items'))){
                obj.items = this.replaceTemplateAction(obj.items, key, replace);
            }
        });
        return templateItems;
    }

    storeListener= ()=>{
        if(this.unsubscribe){
            let {store} = this.context;
            let {approval, pos} = store.getState();
            let newState = {};

            let updateFlag = false;

            if(pos.currentPage && this.state.currentPage !== pos.currentPage){
              newState.currentPage =  pos.currentPage;
            }

            if(!window.isEqual(this.state.approvalFilter, approval.approvalFilter)){
                newState.approvalFilter = approval.approvalFilter;
                //Reset the Filter to Null
                if (window.isEmpty(approval.approvalFilter) && approval.approvalFilter !== undefined){
                     newState.sortApprovalSearchCase = 'oldTonew';
                }
                updateFlag = true;
            }

            if(!window.isEqual(this.state.workbenchFilter, approval.workbenchFilter)){
                newState.workbenchFilter = approval.workbenchFilter;
                //Reset the Filter to Null
                if (window.isEmpty(approval.workbenchFilter) && approval.workbenchFilter !== undefined){
                     newState.sortWorkbenchSearchCase = 'newToold';
                }
                updateFlag = true;
            }

            if(!window.isEqual(this.state.template, approval.approvalTemplate) && approval.currentTemplate === USING_PENDING_APPROVAL_TEMPLATE) {
                
                let searchingTemplate = approval.approvalTemplate;
                searchingTemplate.items = this.replaceTemplateAction(searchingTemplate.items, 'action', 'findCasesByFilter');

                newState.template = searchingTemplate;
            }

            if (!window.isEqual(this.state.currentTemplate, approval.currentTemplate)) {
                newState.currentTemplate = approval.currentTemplate;
            }

            if(!window.isEqual(this.state.template, approval.workbenchTemplate) && approval.currentTemplate === USING_WORKBENCH_TEMPLATE_USED) {

                let searchingTemplate = approval.workbenchTemplate;
                searchingTemplate.items = this.replaceTemplateAction(searchingTemplate.items, 'action', 'findCasesByFilter');

                newState.template = searchingTemplate;
            }
            
            if (!window.isEqual(this.state.searchedWorkbenchCases, approval.searchedWorkbenchCases)){
                newState.searchedWorkbenchCases = approval.searchedWorkbenchCases;
                updateFlag = true;
            }

            if (!window.isEqual(this.state.searchedApprovalCases, approval.searchedApprovalCases)){
                newState.searchedApprovalCases = approval.searchedApprovalCases;
                updateFlag = true;
            }

            if (!window.isEqual(this.state.caseTemplate, approval.caseTemplate)){
                newState.caseTemplate = Object.assign({}, approval.caseTemplate);
            }

            if (!window.isEqual(this.state.reviewCases, approval.approvalCase)){
                newState.reviewCases = Object.assign({}, approval.approvalCase);
            }

            if (!window.isEmpty(newState) || newState.approvalFilter || updateFlag){
                this.setState(newState);
            }
        }
    }

    componentWillUnmount(){
        if(isFunction(this.unsubscribe)){
            this.unsubscribe();
            this.unsubscribe = undefined;
        }
    }

    componentWillReceiveProps(nextProps){
        if (!_.isEqual(nextProps.openFilterDialog, this.state.openFilterDialog)){
            this.setState(Object.assign({}, this.state, nextProps));
        }
    }

    genPendApprovalTemplate = ()=> {
        let {store} = this.context;
        let {app: { agentProfile }, approval} = store.getState();
        store.dispatch({
            type: SET_APPROVAL_TEMPLATE,
            approvalTemplate: approval.approvalTemplate || {},
            currentTemplate: USING_PENDING_APPROVAL_TEMPLATE
        });
    }

    genWorkBenchTemplate(){
        let {store} = this.context;
        let {approval} = store.getState();
        
        store.dispatch({
            type: SET_WORKBENCH_TEMPLATE,
            workbenchTemplate: approval.workbenchTemplate || {},
            currentTemplate: USING_WORKBENCH_TEMPLATE_USED
        });
    }

    init(action){
        let {store} = this.context;
        let {approval} = store.getState();
        if (action === USING_PENDING_APPROVAL_TEMPLATE){
            searchApprovalCases(this.context, approval.approvalFilter, ()=>{});
        } else if (action === USING_WORKBENCH_TEMPLATE_USED){
            searchWorkbenchCases(this.context, approval.workbenchFilter, () => {})
        } 
        
    }

    handleClickEvent = (values) => {
      if (values && values.inProgressBI) {
        getProposalByQuotId(this.context, values.caseNo, true);
      } else if (values && values.inProgressApp && values.canSubmit && !values.isShield) {
        goApplication(this.context, values.applicationId);
      } else if (values && values.inProgressApp && values.canSubmit && values.isShield) {
        // Go Shield Applicaiton
        shieldActions.continueApplication(this.context, values.applicationId);
      } else if (values && values.inProgressApp && !values.canSubmit) {
        this.setState({
          openReviewPage: true,
          reviewCases: values
        });
      } else {
        getDocAction(this.context, values, (resp)=>{
            this.setState({
                openReviewPage: true,
                reviewCases: values
            });
        });
      }
    }

    closeReviewPage= (status) => {
        let {store} = this.context;
        let {approval} = store.getState();

        this.setState({
            openReviewPage: false,
            reviewCases: {}
        }, () => {
            if (this.state.currentTemplate === USING_PENDING_APPROVAL_TEMPLATE){
                searchApprovalCases(this.context, approval.approvalFilter, ()=>{});
            } else if (this.state.currentTemplate === USING_WORKBENCH_TEMPLATE_USED){
                searchWorkbenchCases(this.context, approval.workbenchFilter, () => {});
            }
        });
    }

    handleSortingChange=(id, value) =>{
        if (this.state.currentTemplate === USING_PENDING_APPROVAL_TEMPLATE) {
            this.setState({
                sortApprovalSearchCase: value
            });
        } else {
            this.setState({
                sortWorkbenchSearchCase: value
            });
        }

    }

    genCasesContent(searchedCases=[], template, filterTemplate){
        let items =[];
        let headerItems = [];
        let {sortApprovalSearchCase, sortWorkbenchSearchCase, currentTemplate} = this.state;
        let {store} = this.context;
        let {approval, app} = store.getState();
        let sortSearchCase;
        if (currentTemplate === USING_PENDING_APPROVAL_TEMPLATE) {
            sortSearchCase = sortApprovalSearchCase;
            searchedCases = approval.searchedApprovalCases;
        } else if (currentTemplate === USING_WORKBENCH_TEMPLATE_USED){
            sortSearchCase = sortWorkbenchSearchCase;
            searchedCases = approval.searchedWorkbenchCases || [];
        }
        let headerTemplate = ApprovalUtils.getHeaderTemplate(window.isMobile.apple.phone);
        if (!_.isEmpty(filterTemplate) && searchedCases) {
        headerItems.push(
            <CasesPanelHeader headerTemplate={headerTemplate} sortSearchCase={sortSearchCase} searchedCases={searchedCases} handleSortingChange={(id, value) => this.handleSortingChange(id, value)}/>
        );
        searchedCases = this.sortSearchCaseByDate(sortSearchCase, searchedCases)
        _.forEach(searchedCases, (Obj, index) =>{
            if (template.items) {
              template.items = this.replaceTemplateAction(template.items, 'onClick', 'openReviewPage');
            }

            items.push(
                <DynColumn
                    key={'key-approval-cases'+index}
                    template={template}
                    changedValues={Obj}
                    values={Obj}
                    overwideStyle
                    customStyleClass={(window.isMobile.apple.phone) ? 'MobilePendingApprovalShowCases' : 'PendingApprovalShowCases'}
                />
            )
        })
        return (
                <CasesPanel headerItems={headerItems} items={items} device={app.device}>
                    {items}
                </CasesPanel>
            );
        }
    }

    sortSearchCaseByDate = (sorting, searchCases) =>{
        let factorA, factorB;
        if (sorting === 'newToold'){
            factorA = -1
            factorB = 1
        }else {
            factorA = 1;
            factorB = -1
        }
        return searchCases.sort(function(a,b){return new Date(a.submittedDate) > new Date(b.submittedDate) ? factorA : factorB})
    }

    genSearchMenu (currentTemplate, template){
        let { app } = this.context.store.getState();
        let {approvalFilter = {}, workbenchFilter = {}} = this.state;
        let key, filter, customStyleClass;
        if (currentTemplate === USING_PENDING_APPROVAL_TEMPLATE){
            key = 'key-approval-search';
            filter = approvalFilter;
            customStyleClass = (window.isMobile.apple.phone) ? 'MobilePendingApprovalSearchMenu' : 'PendingAprrovalSearchMenu';
        } else if (currentTemplate === USING_WORKBENCH_TEMPLATE_USED) {
            key = 'key-workbench-search';
            filter = workbenchFilter;
            customStyleClass = (window.isMobile.apple.phone) ? 'MobilePendingWorkbenchSearchMenu' : 'PendingAprrovalSearchMenu';
        }

        return (<SearchMenu
            key={key}
            template={template}
            changedValues={filter}
            values={filter}
            customStyleClass={customStyleClass}
            isApproval
            searchMenu
        />)
    }

    render(){
      let {template,caseTemplate, searchedCases, openReviewPage, reviewCases, currentTemplate} = this.state;
      let secItem, proposalPage;
      if (this.state.currentPage === POS_EAPP && this.props.openApplication) {
        secItem = <div className={styles.secIndex} id="AppForm-Approval" ><AppForm /></div>;
      }

      if (this.state.currentPage === shieldActions.POS_SHIELD_EAPP && this.props.openApplication) {
        secItem = <div className={styles.secIndex} id="AppForm-Approval" ><AppForm isShield/></div>;
      }

      if (this.props.openApplication) {
        proposalPage = <Proposal key={'Approval-proposal'} />;
      }
      const searchMenuContent = this.genSearchMenu(currentTemplate, template);

      return ( 
        <div className={styles.ApprovalSearchContainer} key='key-approval-main' id='key-approval-main'>
            <div style={(window.isMobile.apple.phone) ? {display: 'none'} : {}}>
                {searchMenuContent}
            </div>
            <MobileSearchMenu
                open={this.props.openFilterDialog}
                searchMenuContent={searchMenuContent}
                closeMobileSearchMenu={() => {this.props.closFilterDialog();}}
                resetFilter={() => {this.props.resetFilter();}}
            />
            {this.genCasesContent(searchedCases, caseTemplate, template)}
            <ReviewPage key={'review-page'} zIndex={300} ref={ref => {this.ReviewPage = ref;}}
            changedValues={reviewCases} open={openReviewPage} closeReviewPage={this.closeReviewPage} currentTemplate={currentTemplate}
            openRejectPage={false} openApprovePage={false}
            />
            {proposalPage}
            {secItem}
        </div>
      );
    }
}
