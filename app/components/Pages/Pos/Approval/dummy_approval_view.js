export const CASES_TEMPLATE = {
    "totoalRows": 1,
    "rows": [
        {   
            "approvalCaseId": "dummyApprovalCase1",
            "applicationId": "NA001001-001002222",
            "policyId": "135564654",
            "approvalStatus": "A",
            "proxySetting": {

            },
            "orginalManagerId": "CP001001-001002",
            "agentId": "CP001001-001001",
            "customerId": "CP00120390123",
            "submitted": "2017-01-20"
        },
        {
            "approvalCaseId": "dummyApprovalCase2",
            "applicationId": "NA001001-001001",
            "policyId": "135sdfd564654",
            "approvalStatus": "PSA",
            "proxySetting": {

            },
            "orginalManagerId": "CP001001-001002",
            "agentId": "CP001001-001001",
            "customerId": "CP00120390123",
            "submitted": "2017-01-20"
        }
    ]
}   