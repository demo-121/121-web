import React from 'react';

import EABComponent from '../../../Component';
import FloatingPage from '../../FloatingPage';
import AppbarPage from '../../AppbarPage';

import styles from '../../../Common.scss';
import eab from "../../../../img/eab.png";

export default class AboutEase extends EABComponent {

  open() {
    this.updateAppbar();
    this.floatingPage.open();
  }

  close() {
    this.floatingPage.close();
  }

  updateAppbar() {
    const {langMap} = this.context;
    this.appbarPage.initAppbar({
      menu: {
        icon: 'back',
        action: () => {
          this.close();
        }
      },
      title: 'About EAB'
    });
  }

  render() {
    const {langMap} = this.context;
    return (
      <FloatingPage ref={ref => { this.floatingPage = ref; }}>
        <AppbarPage ref={ref => { this.appbarPage = ref; }}>           
          <div className={styles.easeContainer}>
              <div className={styles.logoContainer}>
                  <div><img src={eab} alt="eab" /></div>
              </div>
              <div>
                  <p>EAB Systems is a leading enterprise software company in Asia Pacific. We offer robust, flexible and scalable solutions to well-known insurance and wealth management providers, both regional and internationally. Leveraging our IT expertise and experience in the life insurance and wealth management industries, we help our clients accelerate the introduction of new products to the market, enhance their distribution capabilities and automate administration processes.</p>
              </div>
          </div>
        </AppbarPage>
      </FloatingPage>
    );
  }

}
