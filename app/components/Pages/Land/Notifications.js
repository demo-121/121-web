import React from 'react';
import EABComponent from '../../Component';
import * as _ from 'lodash';

export default class Notifications extends EABComponent {
  genIndividualNotification(notification) {
    let newLineMessages = [];
    newLineMessages.push(
      <div dangerouslySetInnerHTML={{__html:notification}} />
    );
    return newLineMessages;
  }

  genContent(notifications) {
    let content = [];
    _.map(notifications, notification => {
      content.push(
          <div>{this.genIndividualNotification(notification.message)}</div>
      );
    });
    return (
      <div>
          {
            _.get(notifications, '[0].groupTitle')
          }
          {
            content
          }
      </div>
    );
  }

  render() {
      const { notifications } = this.props;
    return (
      this.genContent(notifications)
    );
  }
}
