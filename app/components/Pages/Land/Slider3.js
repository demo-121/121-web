import React from 'react';
import * as _ from 'lodash';

import EABComponent from '../../Component';
import EABAvatar from '../../CustomViews/Avatar';
import {getIcon} from '../../Icons/index';
import sliderStyles from './Slider.css';

let l2 = require("../../../img/agent_background.jpg");

class Main extends EABComponent {

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      agentProfilePicRev:0
    });
  }

  componentDidMount(){
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  }

  storeListener= ()=>{
    if (this.unsubscribe){
      let {store} = this.context;
      let {attachments} = store.getState();
      let newState = {};
      if (attachments && attachments.agentProfilePicRev) {
        if (!_.isEqual(this.state.agentProfilePicRev, attachments.agentProfilePicRev)) {
          newState.agentProfilePicRev = attachments.agentProfilePicRev;
        }
      }
      if (!window.isEmpty(newState)){
          this.setState(newState);
      }
    }
  }

  componentWillUnmount(){
      if (window.isFunction(this.unsubscribe)){
          this.unsubscribe();
          this.unsubscribe = undefined;
      }
  }


  getProfileSectionWithSubHeader(title, values) {
    if (values && values.length) {
      let items = [];
      values.forEach((value, index) => {
        items.push(
          <li key={index} className={sliderStyles.profileListStyle}>
            <span className={sliderStyles.profileSubHeader}>{value.title}:</span>
            <span>{value.value}</span>
          </li>
        );
      });
      return (
				<ul className={sliderStyles.profileSection}>
          <li className={sliderStyles.profileListStyle + ' ' + sliderStyles.profileHeader}>{title}</li>
          {items}
				</ul>
      );
    }
  }

  getProfileSection(title, values) {
    if (values && values.length) {
      return (
				<ul className={sliderStyles.profileSection}>
					<li className={sliderStyles.profileListStyle + ' ' + sliderStyles.profileHeader}>{title}</li>
          <li className={sliderStyles.profileListStyle} style={{ whiteSpace: 'pre-wrap' }}>
            {_.isArray(values) ? _.join(values, ', ') : values}
          </li>
				</ul>
      );
    }
  }

  render() {
    const {langMap, store} = this.context;
    const {app, attachments} = store.getState();
    const {agentProfile,lastUpdateDate} = app;
    const {agentCodeDisp, name, position, manager, managerDisp, company, email, mobile, achievements, authorisedDes} = agentProfile;
    let profileValues = [
      { title: window.getLocalizedText(langMap, 'profile.agent.agentCode'), value: agentCodeDisp },
      { title: window.getLocalizedText(langMap, 'profile.agent.position'), value: position },
      { title: window.getLocalizedText(langMap, 'profile.agent.manager'), value: manager },
      { title: window.getLocalizedText(langMap, 'profile.agent.managerCode'), value: managerDisp },
      { title: window.getLocalizedText(langMap, 'profile.agent.organization'), value: company },
      { title: window.getLocalizedText(langMap, 'profile.agent.email'), value: email },
      { title: window.getLocalizedText(langMap, 'profile.agent.mobile'), value: mobile },
    ];
    let profilePic = { agentProfilePic: attachments.items.agentProfilePic || 1 };
    let {agentProfilePicRev} = this.state;

    return (
      <div
        id="landSlide3"
        style={{
						backgroundImage: 'url(' + l2 + ')',
						backgroundSize: 'cover',
						position: 'relative',
            overflow: 'auto'
					}}>
          <div
						id="landProfile"
						key="landProfile"
						style={{
							backgroundColor: 'rgba(255, 255, 255, 0.95)',
							padding: '26px',
							width: '400px',
							margin: '26px 40px',
							position: 'absolute',
							right: '5%',
							top: '20px'
						}}>
						<div id="profileHeader" style={{ display: 'flex', minHeight: '0px', alignItems: 'center' }}>
							<span id="landAgtName"
                key="landAgtName"
                style={{
                  textTransform: 'uppercase',
                  fontSize: '24px',
                  width: '280px',
                }}
              >
                {name}
              </span>
              <EABAvatar
                  disabled
                  docId={'UX_' + agentProfile.profileId}
                  template={{ id: 'agentProfilePic', type: 'photo' }}
                  origin={'Slider3'}
                  agentProfilePicRev={agentProfilePicRev}
                  width={100}
                  icon={getIcon('person', '#FFFFFF')}
                  values={profilePic}
                  lastUpdateDate={lastUpdateDate}
                  changedValues={profilePic}
              />
						</div>
						<div id="profileContent">
              {this.getProfileSectionWithSubHeader(window.getLocalizedText(langMap, 'profile.agent.profile'), profileValues)}
              {this.getProfileSection(window.getLocalizedText(langMap, 'profile.agent.achievements'), achievements)}
              {this.getProfileSection(window.getLocalizedText(langMap, 'profile.agent.authorised'), authorisedDes)}
						</div>
					</div>
      </div>
    );
  }
}

export default Main;
