import { INIT_SUPPORT_DOCUMENTS, SHOW_DRAG_DROP_ZONE, UPDATE_SUPPORT_DOCUMENTS, UPDATE_VIEWED_LIST, CLOSE_SUPPORT_DOCUMENTS, UPDATE_PENDING_SUBMIT_LIST} from '../actions/supportDocuments';
import {INIT_SUPPORT_DOCUMENTS_SHIELD} from '../actions/shieldSupportDocuments';
import { LOGOUT } from '../actions/GlobalActions';

const getInitState = () => {
  return {
      // supportDocumentsTemplate:{},
      template:{},
      values:{},
      appId:'',
      showFileSizeLargeDialog:false,
      dragDropZone:{
        isShow:false,
        valueLocation:{}
      },
      viewedList: {},
      tokensMap: {},
      pendingSubmitList: []
  }
}

export default function supportDocuments(state = getInitState(), action) {
  switch(action.type) {
    case INIT_SUPPORT_DOCUMENTS:
    case INIT_SUPPORT_DOCUMENTS_SHIELD:
      return Object.assign({}, state, {
        template:action.template,
        values:action.values,
        suppDocsAppView: action.suppDocsAppView,
        appStatus: action.appStatus,
        viewedList: action.viewedList,
        userChannel: action.userChannel,
        isReadOnly: action.isReadOnly,
        defaultDocNameList: action.defaultDocNameList,
        supportDocDetails: action.supportDocDetails,
        tokensMap: action.tokensMap,
        pendingSubmitList: []
      });
    case UPDATE_VIEWED_LIST:
      return Object.assign({}, state, {
        viewedList: action.viewedList
      });
    case UPDATE_PENDING_SUBMIT_LIST:
      return Object.assign({}, state, {
        pendingSubmitList: action.pendingSubmitList
      });
    case UPDATE_SUPPORT_DOCUMENTS:
      return Object.assign({}, state, {
        values:action.values,
        dragDropZone:{
          isShow:false,
          valueLocation:{}
        },
        viewedList: action.viewedList || state.viewedList,
        pendingSubmitList: action.pendingSubmitList || state.pendingSubmitList,
        tokensMap: Object.assign({}, action.tokensMap, state.tokensMap)
      });
    case SHOW_DRAG_DROP_ZONE:
      return Object.assign({}, state, {
        dragDropZone:{
          isShow:action.isShow,
          valueLocation:action.valueLocation
        }
      });
    case CLOSE_SUPPORT_DOCUMENTS:
      return Object.assign({}, state, {
        dragDropZone:{
          isShow:false,
          valueLocation:{},
        },
        pendingSubmitList: []
      });
    case LOGOUT:
      return getInitState();  // reset the state
    default:
      return state;
  }
}
