import { INIT_PAYMENT_PAGE, PAYMENT_SUCCESS, PAYMENT_DONE, DO_PAYMENT,RELOAD_PAYMENT, UPDATE_PAYMENT, SERVER_ERROR, CLEAR_ERROR, CLOSE_PAYMENT_PAGE, DO_SHIELD_PAYMENT, RELOAD_SHIELD_PAYMENT } from '../actions/payment';
import { VALIDATE_SHIELD_APPLICATION } from '../actions/shieldApplication';
import { CLOSE_SUPPORT_DOCUMENTS } from '../actions/supportDocuments';
import { LOGOUT } from '../actions/GlobalActions';

const getInitState = () => {
  return {
    template: {},
    values: {}, 
    paymentWindow: false,
    isInitPaymentCompleted: true,
    isMandDocsAllUploaded: false,
    isSubmitted: false
  }
}

//Function: upload Payment Status if success
const getPaymentSuccessTemplate = (state) => {
  let newTemplate = Object.assign({}, state.template);
  const payMethod = newTemplate.items[1];
  const initPayButton = payMethod.items[1].button;
  initPayButton["disabled"] = true;
  return newTemplate;
}

export default function payment(state = getInitState(), action) {
  switch(action.type) {
    case INIT_PAYMENT_PAGE:
      return Object.assign({}, state, {
        template: action.template,
        values: action.values, 
        isMandDocsAllUploaded: action.isMandDocsAllUploaded,
        isSubmitted: action.isSubmitted,
        updatePayment: action.updatePayment
      });
    case PAYMENT_SUCCESS: 
      return Object.assign({}, state, {
        template: getPaymentSuccessTemplate(state), 
        isInitPaymentCompleted: true,
        updatePayment: action.updatePayment
      });
    case UPDATE_PAYMENT:
      return Object.assign({}, state, {
        values: action.values,
        updatePayment: action.updatePayment
      });
    case DO_PAYMENT:
      return Object.assign({}, state, {
        paymentWindow: action.paymentWindow,
        values: action.values,
        updatePayment: action.updatePayment
      }) 
    case DO_SHIELD_PAYMENT:
      return Object.assign({}, state, {
        paymentWindow: action.paymentWindow,
        updatePayment: action.updatePayment
      }) 
    case RELOAD_PAYMENT:
      if (action.values) {
        return Object.assign({}, state, {
          values: action.values,
          updatePayment: action.updatePayment
        })
      } else {
        return state;
      }
    case PAYMENT_DONE: 
      return Object.assign({}, state, {
        paymentWindow: false,
        updatePayment: action.updatePayment
      })
    case CLOSE_SUPPORT_DOCUMENTS:
      return Object.assign({}, state, {
        isMandDocsAllUploaded: action.isMandDocsAllUploaded
      });    
    case SERVER_ERROR:
      return Object.assign({}, state, {
        errorMsg: action.errorMsg,
        updatePayment: action.updatePayment
      });
    case CLEAR_ERROR:
      return Object.assign({}, state, {
        errorMsg: "",
        updatePayment: action.updatePayment
      });
    case RELOAD_SHIELD_PAYMENT:
      return Object.assign({}, state, {
        updatePayment: true
      });
    case VALIDATE_SHIELD_APPLICATION:
      return Object.assign({}, state, {
        updatePayment: false
      });
    case CLOSE_PAYMENT_PAGE: 
      if (state.paymentWindow && !state.paymentWindow.closed) {
        if (_.isFunction(state.paymentWindow.close)) {
          state.paymentWindow.close();
        }
      }
    case LOGOUT:
      return getInitState();  // reset the state
    default:
      return state;
  }
}
