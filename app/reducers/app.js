import {INITIAL_APP, INITIAL_FAIL, LOGIN_SUCCESS, EXIST_USER_LOGIN, SHOW_TERMS} from '../actions/home';
import {SHOW_LOADING, HIDE_LOADING, CHANGE_LANG, SHOW_APP_ERR_MSG, HIDE_APP_ERR_MSG, LOGOUT} from '../actions/GlobalActions'
import {UPDATE_AGENT_INFO, READ_ALL_MESSAGE} from '../actions/agent';

import {OPEN_APPROVAL_PAGE} from '../actions/approval';

import * as _ from 'lodash';

var getInitState = function () {
  return {
    inited: false,
    loading: false,
    isLogin: false,
    loggedOut: false,
    showTerms: false,
    saml: "",
    agentProfile: null,
    lastUpdateDate: null, // for updating company/agent info
    showErrorMsg: false,
    errMsg: '',
    lang: 'en',
    langs: {},
    langMap: {},
    optionsMap: {},
    comanyInfo: null,
    notifications: [],
    showNotificationsDialog: false,
    exceedMaxConcurrentLogin: false
  }
}

export default function app(state = getInitState(), action) {
  switch (action.type) {
    case INITIAL_APP:
      window.loggedIn = false;
      return Object.assign({}, state, {
        inited: true,
        loggedOut: false,
        langMap: action.langMap,
        langs: action.langs,
        lang: action.lang || 'en'
      });
    case SHOW_TERMS:
      return Object.assign({}, state, {
        showTerms: true,
        tncContent: action.tncContent
      });
    case CHANGE_LANG:
      return Object.assign({}, state, {
        langMap: action.langMap,
        lang: action.lang || 'en'
      });
    case EXIST_USER_LOGIN:
      window.loggedIn = true;

      return Object.assign({}, state, {
        showTerms: false,
        tncContent: "",
        optionsMap: action.optionsMap,
        agentProfile: action.agent,
        isLogin: !!action.agent,
        loggedOut: false,
        companyInfo: action.companyInfo,
        sysParameterConstant: action.sysParameterConstant,
        initOpenApprovalPage: action.initOpenApprovalPage,
        isFAFirm: action.isFAFirm,
        notifications: action.notifications,
        showNotificationsDialog: _.filter(action.notifications, notice => !notice.read).length ? true : false,
        exceedMaxConcurrentLogin: action.exceedMaxConcurrentLogin
      });
    case OPEN_APPROVAL_PAGE:
      return Object.assign({}, state, {
        initOpenApprovalPage: action.openApproval
      })
    case SHOW_LOADING:
      if (!state.loading) {
        return Object.assign({}, state, {loading: true}); // reset the state
      }
      return state;
    case HIDE_LOADING:
      if (state.loading) {
        return Object.assign({}, state, {loading: false});
      }
      return state;
    case SHOW_APP_ERR_MSG:
      return Object.assign({}, state, {showErrorMsg: true, showTerms: false, errorMsg: action.errorMsg});
    case HIDE_APP_ERR_MSG:
      return Object.assign({}, state, {showErrorMsg: false, errorMsg: ""});
    case INITIAL_FAIL:
    case LOGOUT:
      window.loggedIn = false;
      return Object.assign(getInitState(), {
        loggedOut: true
      }); // reset the state
    //   return Object.assign({}, state, {
    //     loading: false,
    //     isLogin: false,
    //     agentProfile: null,
    //     lastUpdateDate: null, // for updating company/agent info
    //     showErrorMsg: false,
    //     errMsg: '',
    //     optionsMap: {},
    //     comanyInfo: null
    // });
    case UPDATE_AGENT_INFO:
      return Object.assign({}, state, {
        lastUpdateDate: action.lastUpdateDate
      });
    case READ_ALL_MESSAGE:
      let show = false;
      for(let i in action.notification) {
        if(!action.notification[i].read) {
          show = true;
          break;
        }
      }

      return Object.assign({}, state, {
        notifications: action.notification,
        showNotificationsDialog: show
      })
    default:
      return state;
  }
}
