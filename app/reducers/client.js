import {
  OPEN_PROFILE,
  SAVE_PROFILE,
  UPDATE_PROFILE,
  CLEAR_PROFILE,
  UPDATE_DEPENDANTS_PROFILE,
  UPDATE_CONTACT_LIST,
  UPDATE_ALL_PROFILES,
  UPDATE_ADDRESS_BY_PC
} from '../actions/client';

import {
  EXIST_USER_LOGIN
}  from '../actions/home';

import {
  INIT_NEEDS,
  UPDATE_PDA,
  UPDATE_FNA,
  UPDATE_FE
} from '../actions/needs';

import {
  NEW_QUOTATION,
  NEW_QUOTATION_ERROR
} from '../actions/quotation';

import {
  SIGN_PDF_BY_ATTACHMENT_ID
} from '../actions/signature';

import {
  LOGOUT
} from '../actions/GlobalActions';

var getInitState = function() {
  return {
    profile:{},
    dependantProfiles: {},
    contactList: [],
    template: {}
  }
  // completed: false
}

export default function client(state = getInitState(), action) {
  switch (action.type) {
    case SAVE_PROFILE:
    case UPDATE_PROFILE:
      return Object.assign(state, {
        profile: action.profile || state.profile,
        dependantProfiles: action.dependantProfiles || state.dependantProfiles
      });
    case UPDATE_ALL_PROFILES:
      return Object.assign(state, {
        profile: action.profile,
        dependantProfiles: action.dependantProfiles
      });
    case CLEAR_PROFILE:
      return Object.assign(state, {
        profile: {},
        dependantProfiles: []
      });
    case INIT_NEEDS:
      return Object.assign( state, {
        dependantProfiles: action.dependantProfiles
      })
    case UPDATE_DEPENDANTS_PROFILE:
      return Object.assign(state, {
        dependantProfiles: action.dependantProfiles
      });
    case UPDATE_CONTACT_LIST:
      return Object.assign(state, {
        contactList: action.contactList
      });
    case EXIST_USER_LOGIN:
      return Object.assign(getInitState(), {
        template: action.profileTemplate
      });
    case SIGN_PDF_BY_ATTACHMENT_ID:
    case UPDATE_PDA:
    case UPDATE_FE:
    case UPDATE_FNA:
    case NEW_QUOTATION:
    case NEW_QUOTATION_ERROR:
      if (!isEmpty(action.profile)) {
        return Object.assign(state, {
          profile: action.profile || state.profile
        });
      } else {
        return state;
      }
    case UPDATE_ADDRESS_BY_PC:
      return Object.assign(state, {
        postalBldgNo: action.postalBldgNo,
        postalBldgName: action.postalBldgName,
        postalStName: action.postalStName,
        postalCode: action.postalCode
      });
    case LOGOUT:
      return getInitState();
    default:
      return state;
  }
}
