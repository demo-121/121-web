import {
  LOGIN_FAIL,
} from '../actions/home';

import {
  SESSION_TIME_OUT
} from '../actions/sessions';

import { LOGOUT } from '../actions/GlobalActions';

var getInitState = function() {

  return {
    hasError: false,
    errorMsg: "",
    agent:{
      agentName: ""
    },
    welcomeMessage:""
  }
}

export default function home(state = getInitState(), action) {
  switch (action.type) {

    case SESSION_TIME_OUT:
    case LOGOUT:
      return getInitState();  // reset the state
    default:
      return state;
  }
}
