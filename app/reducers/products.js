import { LOGOUT } from '../actions/GlobalActions';
import { INITIAL_APP } from '../actions/home';
import { UPDATE_DEPENDANT_LIST, UPDATE_PRODUCTS, QUOT_CLOSE_CONFIRM, QUOT_CLOSE_ERROR, CLEAR_PRODUCTS } from '../actions/products';
import { NEW_QUOTATION, CONFIRM_CREATE_QUOTE, NEW_QUOTATION_ERROR } from '../actions/quotation';

const getInitState = function() {
  return {
    ccy: null,
    insuredCid: null,
    dependants: [],
    prodCategories: [],
    error: null,
    confirmMsg: null,
    errorMsg: null,
    selectedProductId: null
  };
};

export default function products(state = getInitState(), action) {
  switch (action.type) {
    case UPDATE_DEPENDANT_LIST:
      return Object.assign({}, state, {
        dependants: action.dependants
      });
    case UPDATE_PRODUCTS:
      return Object.assign({}, state, {
        prodCategories: action.prodCategories,
        error: action.error,
        ccy: action.ccy,
        insuredCid: action.insuredCid
      });
    case CONFIRM_CREATE_QUOTE:
      return Object.assign({}, state, {
        confirmMsg: action.confirmMsg,
        selectedProductId: action.productId,
        errorMsg: null
      });
    case NEW_QUOTATION:
    case QUOT_CLOSE_CONFIRM:
      return Object.assign({}, state, {
        confirmMsg: null,
        selectedProductId: null,
        errorMsg: null
      });
    case NEW_QUOTATION_ERROR:
      return Object.assign({}, state, {
        errorMsg: action.errorMsg,
        confirmMsg: null,
        selectedProductId: null
      });
    case QUOT_CLOSE_ERROR:
      return Object.assign({}, state, {
        errorMsg: null
      });
    case CLEAR_PRODUCTS:
    case INITIAL_APP:
    case LOGOUT:
      return getInitState();
    default:
      return state;
  }
}
