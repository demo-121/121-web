import { LOGOUT } from '../actions/GlobalActions';
import { OPEN_QUICK_QUOTES, LOAD_QUICK_QUOTES, CLOSE_QUICK_QUOTES } from '../actions/quickQuote';

const getInitState = () => {
  return {
    open: false,
    quickQuotes: null
  };
};

export default function quickQuote(state = getInitState(), action) {
  switch (action.type) {
    case OPEN_QUICK_QUOTES:
      return Object.assign({}, state, {
        open: true
      });
    case LOAD_QUICK_QUOTES:
      return Object.assign({}, state, {
        quickQuotes: action.quickQuotes
      });
    case CLOSE_QUICK_QUOTES:
    case LOGOUT:
      return getInitState();
    default:
      return state;
  }
}
