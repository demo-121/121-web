import {EXCEED_MAX_FILES_SIZE} from '../actions/supportDocuments';
import { LOGOUT } from '../actions/GlobalActions';

const getInitState = () => {
  return {
    showTotalFilesSizeLargeDialog: false
  }
}

export default function fileUpload(state = getInitState(), action) {
  switch (action.type) {
    case EXCEED_MAX_FILES_SIZE:
      return Object.assign({}, state, {
        showTotalFilesSizeLargeDialog: action.showTotalFilesSizeLargeDialog
      });
    case LOGOUT:
      return getInitState();  // reset the state
    default:
      return state;
  }
}