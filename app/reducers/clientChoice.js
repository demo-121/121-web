import { INIT_CLIENT_CHOICE, UPDATE_CLIENT_CHOICE, SERVER_ERROR } from '../actions/clientChoice'
import { LOGOUT } from '../actions/GlobalActions';


const getInitState = () => {
  return {
    template:{},
    values:{},
    isRecommendationCompleted:false, 
    isBudgetCompleted:false, 
    isAcceptanceCompleted: false,
    clientChoiceStep: 0,
    errorMsg:""
  }
}

export default function clientChoice(state = getInitState(), action) {
  switch(action.type) {
    case INIT_CLIENT_CHOICE:
      return Object.assign({}, state, {
        template: action.template, 
        values: action.values,
        isRecommendationCompleted: action.isRecommendationCompleted, 
        isBudgetCompleted: action.isBudgetCompleted,
        isAcceptanceCompleted: action.isAcceptanceCompleted, 
        clientChoiceStep: action.clientChoiceStep
      });
    case UPDATE_CLIENT_CHOICE:
      return Object.assign({}, state, {
        values: action.values, 
        isRecommendationCompleted: action.isRecommendationCompleted, 
        isBudgetCompleted: action.isBudgetCompleted,
        isAcceptanceCompleted: action.isAcceptanceCompleted,
        clientChoiceStep: action.clientChoiceStep
      });
    case SERVER_ERROR:
      return Object.assign({}, state, {
        errorMsg: action.errorMsg
      });
    case LOGOUT:
      return getInitState();  // reset the state
    default:
      return state;
  }
}