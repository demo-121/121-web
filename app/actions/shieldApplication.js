import * as _ from 'lodash';

import SystemConstants from '../constants/SystemConstants';
import { FNA_NOT_COMPLETED, POS_CHANGE_PAGE, POS_MAIN } from './application';
import { updateStoreProfile } from './client';

export const INIT_SHIELD_APPLICATION = 'INIT_SHIELD_APPLICATION';
export const SHOW_SHIELD_APPLICATION = 'SHOW_SHIELD_APPLICATION';
export const VALIDATE_SHIELD_APPLICATION = 'VALIDATE_SHIELD_APPLICATION';
export const SAVE_SHIELD_APPLICATION = 'SAVE_SHIELD_APPLICATION';
export const SHOW_SHIELD_SIGNATURE = 'SHOW_SHIELD_SIGNATURE';
export const UPDATE_SHIELD_SIGNATURE_URL = 'UPDATE_SHIELD_SIGNATURE_URL';
export const SHOW_SHIELD_PAYMENT = 'SHOW_SHIELD_PAYMENT';
export const SAVE_SHIELD_PAYMENT = 'SAVE_SHIELD_PAYMENT';
export const SAVE_SHIELD_SUBMISSION = 'SAVE_SHIELD_SUBMISSION';
export const SHOW_SHIELD_SUBMISSION = 'SHOW_SHIELD_SUBMISSION';
export const DO_SHIELD_SUBMISSION = 'DO_SHIELD_SUBMISSION';
export const CLOSE_SHIELD_APPLICATION = 'CLOSE_SHIELD_APPLICATION';
export const HIDE_SHIELD_SIGNATURE_EXPIRY_WARNING = 'HIDE_SHIELD_SIGNATURE_EXPIRY_WARNING';
export const HIDE_SHIELD_WARNING = 'HIDE_SHIELD_WARNING';
export const SHOW_SHIELD_ERROR = 'SHOW_SHIELD_ERROR';
export const HIDE_SHIELD_ERROR = 'HIDE_SHIELD_ERROR';

export const POS_SHIELD_EAPP = 'POS_SHIELD_EAPP';


//Application functions

export function applyAppForm(context, quotId) {
  let { store } = context;
  window.callServer(
    context,
    '/shieldApplication',
    {
      action: 'applyAppForm',
      quotId: quotId
    },
    (resp) => {
      if (resp.isInvalidated) {
        // store.dispatch({
        //   type: FNA_NOT_COMPLETED,
        //   isFNACompleted: false,
        //   appList: resp.appList
        // });
      } else {
        store.dispatch({
          type: INIT_SHIELD_APPLICATION,
          stepper: resp.stepper,
          template: resp.template,
          application: resp.application
        });
      }
    }
  );
}

export function continueApplication(context, appId) {
  let { store } = context;
  window.callServer(
    context,
    '/shieldApplication',
    {
      action: 'continueApplication',
      appId: appId
    },
    (resp) => {
      if (resp && resp.application) {
        switch (_.get(resp, 'stepper.index.current', 0)) {
          case SystemConstants.EAPP.STEP.APPLICATION:
            store.dispatch({
              type: SHOW_SHIELD_APPLICATION,
              template: resp.template,
              application: resp.application,
              stepper: resp.stepper,
              warningMsg: resp.warningMsg
            });
            break;
          case SystemConstants.EAPP.STEP.SIGNATURE:
            store.dispatch({
              type: SHOW_SHIELD_SIGNATURE,
              application: resp.application,
              signature: resp.signature,
              stepper: resp.stepper,
              warningMsg: resp.warningMsg
            });
            break;
          case SystemConstants.EAPP.STEP.PAYMENT:
            store.dispatch({
              type: SHOW_SHIELD_PAYMENT,
              template: resp.template,
              application: resp.application,
              stepper: resp.stepper,
              warningMsg: resp.warningMsg
            });
            break;
          case SystemConstants.EAPP.STEP.SUBMISSION:
            store.dispatch({
              type: SHOW_SHIELD_SUBMISSION,
              template: resp.template,
              application: resp.application,
              stepper: resp.stepper,
              warningMsg: resp.warningMsg
            });
            break;
          default:
            store.dispatch({
              type: SHOW_SHIELD_APPLICATION
            });
        }
      }
    }
  );
}

export function saveAppForm(context, appId, changedValues, getPolicyNumber, callback) {
  let { store } = context;
  window.callServer(
    context,
    '/shieldApplication',
    {
      action: 'saveAppForm',
      appId,
      changedValues,
      getPolicyNumber
    },
    (resp) => {
      let dispatchObj = {
        type: SAVE_SHIELD_APPLICATION,
        application: resp.application
      };
      if (getPolicyNumber && !resp.getPolicyNumberResult) {
        dispatchObj.errorMsg = {
          showDialog: true,
          msg: 'Policy number exhausted. Please contact IT support (REF code: 00002)'
        };
      }
      store.dispatch(dispatchObj);

      if (resp.updIds && resp.updIds.length > 0) {
        let _cValues = _.get(resp, 'application.applicationForm.values', changedValues);
        let upArray = [];
        if (resp.updIds.indexOf(_.get(_cValues, 'proposer.personalInfo.cid')) > -1) {
          upArray.push(_cValues.proposer.personalInfo);
        }
        if (_.get(_cValues, 'insured', []).length > 0) {
          _.forEach(_cValues.insured, (la) => {
            if (resp.updIds.indexOf(_.get(la, 'personalInfo.cid')) > -1) {
              upArray.push(la);
            }
          });
        }
        if (upArray.length > 0) {
          setTimeout(() => {
            updateStoreProfile(context, upArray);
          }, 1000);
        }
      }

      callback();
    }
  );
}

export function goNextStep(context, appId, currIndex, nextIndex, changedValues, callback) {
  let { store } = context;
  let { shieldApplication } = store.getState();
  window.callServer(
    context,
    '/shieldApplication',
    {
      action: 'goNextStep',
      appId,
      currIndex,
      nextIndex,
      changedValues
    },
    (resp) => {
      let newStepper = _.cloneDeep(shieldApplication.stepper);
      newStepper.index = {
        current: nextIndex,
        completed: _.get(resp, 'application.appCompletedStep', shieldApplication.stepper.index.completed),
        active: _.max([_.get(resp, 'application.appStep', shieldApplication.stepper.index.active), shieldApplication.stepper.index.active])
      };

      if (nextIndex === SystemConstants.EAPP.STEP.APPLICATION) {
        store.dispatch({
          type: SHOW_SHIELD_APPLICATION,
          template: resp.template,
          application: resp.application,
          stepper: newStepper
        });
      } else if (nextIndex === SystemConstants.EAPP.STEP.SIGNATURE) {
        store.dispatch({
          type: SHOW_SHIELD_SIGNATURE,
          application: resp.application,
          stepper: newStepper,
          signature: resp.signature,
          warningMsg: resp.warningMsg
        });
      } else if (nextIndex === SystemConstants.EAPP.STEP.PAYMENT) {
        store.dispatch({
          type: SHOW_SHIELD_PAYMENT,
          template: resp.template,
          application: resp.application,
          stepper: newStepper
        });
      } else {
        store.dispatch({
          type: SHOW_SHIELD_SUBMISSION,
          template: resp.template,
          application: resp.application,
          stepper: newStepper
        });
      }
      callback();
    }
  );
}

const _getUpdatedStepperIndexAfterValidate = function(currStepperIndex, completed, enableNextStep) {
  let newStepperIndex = _.cloneDeep(currStepperIndex);

  //update completed index
  if (completed && currStepperIndex.completed === currStepperIndex.current - 1) {
    newStepperIndex.completed = currStepperIndex.current;
  } else if (!completed && currStepperIndex.completed === currStepperIndex.current) {
    newStepperIndex.completed = currStepperIndex.current - 1;
  }

  //update active index
  if (enableNextStep && currStepperIndex.active < SystemConstants.EAPP.STEP.SUBMISSION && currStepperIndex.active === currStepperIndex.current) {
    newStepperIndex.active = currStepperIndex.active + 1;
  } else if (!enableNextStep && currStepperIndex.active > currStepperIndex.current) {
    newStepperIndex.active = currStepperIndex.current;
  }
  return newStepperIndex;
};

export function validate(context, completed, enableNextStep) {
  let { store } = context;
  let { shieldApplication } = store.getState();
  let newStepper = _.cloneDeep(shieldApplication.stepper);
  newStepper.index = _getUpdatedStepperIndexAfterValidate(shieldApplication.stepper.index, completed, enableNextStep);
  newStepper.enableNextStep = enableNextStep;

  store.dispatch({
    type: VALIDATE_SHIELD_APPLICATION,
    stepper: newStepper
  });
}

// Signature functions

export function getSignatureUpdatedUrl(context, signResult, tabIdx) {
  let { store } = context;
  if (signResult.sdweb_result === 'success') {
    window.callServer(
      context,
      '/shieldApplication',
      {
        action: 'getSignatureUpdatedUrl',
        sdwebDocid: signResult.sdweb_docid,
        tabIdx
      },
      (resp) => {
        store.dispatch({
          type: UPDATE_SHIELD_SIGNATURE_URL,
          newAttUrls: resp.newAttUrls,
          tabIdx: tabIdx,
          nextTabIdx: resp.nextTabIdx,
          isSigned: resp.isSigned,
          isSigningProcess: resp.isSigningProcess
        });
      }
    );
  } else {
    store.dispatch({
      type: SHOW_SHIELD_ERROR,
      msg: signResult.sdweb_message ? signResult.sdweb_message : (signResult.sdweb_errormessage ? signResult.sdweb_errormessage : 'Sign Error')
    });
  }
}

//Payment functions

export function savePaymentMethods(context, appId, changedValues, callback) {
  let { store } = context;

  window.callServer(
    context,
    '/shieldApplication',
    {
      action: 'savePaymentMethods',
      appId,
      changedValues
    },
    (resp) => {
      store.dispatch({
        type: SAVE_SHIELD_PAYMENT,
        application: resp.application
      });
      callback();
    }
  );
}

//Submission functions

export function saveSubmissionValues(context, appId, changedValues, callback) {
  let { store } = context;

  window.callServer(
    context,
    '/shieldApplication',
    {
      action: 'saveSubmissionValues',
      appId,
      changedValues
    },
    (resp) => {
      store.dispatch({
        type: SAVE_SHIELD_SUBMISSION,
        application: resp.application
      });
      callback();
    }
  );
}

export function performSubmission(context, appId, changedValues, submissionTemplate) {
  let { store } = context;
  let { app, client, approval, shieldApplication } = store.getState();

  window.callServer(
    context,
    '/shieldApplication',
    {
      action: 'submission',
      appId,
      submissionTemplate,
      agentProfile: app.agentProfile,
      changedValues: changedValues
    }, function (resp) {
      let newStepper = _.cloneDeep(shieldApplication.stepper);
      newStepper.index = _getUpdatedStepperIndexAfterValidate(shieldApplication.stepper.index, resp.application.isSubmittedStatus, shieldApplication.stepper.enableNextStep);

      store.dispatch({
        type: DO_SHIELD_SUBMISSION,
        template: resp.template,
        application: resp.application,
        stepper: newStepper
      });
    }
  );
}

//Other functions

export function invalidateApplication(context, appId) {
  window.callServer(
    context,
    '/shieldApplication',
    {
      action: 'invalidateApplication',
      appId
    },
    (resp) => {
      closePage(context);
    }
  );
}

export function setSignExpiryShown(context) {
  let { store } = context;
  let { shieldApplication } = store.getState();

  window.callServer(
    context,
    '/shieldApplication',
    {
      action: 'setSignExpiryShown',
      appId: shieldApplication.application.id
    },
    (resp) => {
      store.dispatch({
        type: HIDE_SHIELD_SIGNATURE_EXPIRY_WARNING
      });
    }
  );
}

export function hideWarningMessage(context) {
  let { store } = context;

  store.dispatch({
    type: HIDE_SHIELD_WARNING
  });
}

export function hideErrorMessage(context) {
  let { store } = context;

  store.dispatch({
    type: HIDE_SHIELD_ERROR
  });
}

export function closePage(context) {
  let { store } = context;

  store.dispatch({
    type: POS_CHANGE_PAGE,
    page: POS_MAIN
  });
}

export function confirmAppPaid(context, appId, cb) {
  window.callServer(
    context,
    '/shieldApplication',
    {
      action: 'confirmAppPaid',
      appId: appId
    },
    (resp) => {
      if (resp && resp.success) {
        cb(true);
      } else {
        cb(false);
      }
    }
  );

}