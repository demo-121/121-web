import * as _ from 'lodash';
import {
  resetSession
} from './sessions';

import { logout, showLoading, SHOW_APP_ERR_MSG } from './GlobalActions';
import config from '../config';
import uuid from 'uuid';
import rsa from 'node-rsa';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const INITIALING = 'INITIALING';
export const INITIAL_APP = 'INITIAL_APP';
export const INITIAL_FAIL = 'INITIAL_FAIL';
export const SHOW_TERMS = 'SHOW_TERMS';
export const CHANGE_LANG = 'CHANGE_LANG';
export const EXIST_USER_LOGIN = 'EXIST_USER_LOGIN';
export const UPDATE_ATTACHMENT_PROFILE_PIC = 'UPDATE_ATTACHMENT_PROFILE_PIC';
import platform from 'platform';

export function initApp(context) {

  showLoading(context);

  let {
    store
  } = context;

  let xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState === 4) { // done
      if (xhttp.status === 200 && xhttp.responseText) {
        let res = JSON.parse(xhttp.responseText);
        if (!res.user && res.saml) {
          window.location = './samlLogin';
        } else {
          let {
            app
          } = store.getState();

          let rsaKey = null;
          if (config.enableRSA) {
            rsaKey = new rsa({b: 512});
            app.yekasr = rsaKey;
          }
          // rsa public key of server
          app.lang = res.lang;
          app.csrfToken = res.csrfToken;
          app.sid = res.sid;
          app.requireSAML = !!res.saml;
          app.signOutSAML = res.samlSignOut;
          app.user = res.user;
          app.version = res.version;
          app.build = res.build;

          if (res.yekasrs) {
            let RSA = new rsa();
            RSA.importKey(res.yekasrs);
            app.yekasrs = RSA;
          }

          let skey = uuid();
          app.skey = skey;

          window.callServer(
            context,
            './auth',
            {
              action: 'initial',
              lang: app.lang,
              skey: skey,
              localTime: new Date().getTime(),
              yekasrb: rsaKey ? rsaKey.exportKey('public') : null
            }, function(resp) {
              if (resp.success) {
                store.dispatch({
                  type: INITIAL_APP,
                  success: true,
                  langMap: resp.langMap,
                  langs: resp.langs,
                  lang: app.lang
                })
              } else {
                store.dispatch({
                  type: SHOW_APP_ERR_MSG,
                  errorMsg: {
                    isError: true,
                    errorMsg: "System initialization failure. Please contact administrator.",
                  }
                });
              }
              if (app.user) {
                login(context);
              }
            }
          );
        }
      }
    }
  };

//  xhttp.open("GET", window.location.origin + './init', true);
  xhttp.open('GET', './init', true);
  xhttp.send();
}

//
export function changeLang(context, data) {

  window.callServer(context, '/auth', {
    context,
    action: 'changeLang',
    localTime: new Date().getTime(),
    lang: data.lang
  }, function(resp) {
    if (resp.success) {
//set lang to cookie
      window.setCookie('lang', data.lang, 30);

      context.store.dispatch({
        type: CHANGE_LANG,
        success: true,
        langMap: resp.langMap,
      })
    } else {
      context.store.dispatch({
        type: CHANGE_LANG,
        success: false,
        langMap: {}
      });
    }
  });
}

export let acceptTerm = function(context) {
  let {
    store
    // , router
  } = context;

  // let respAction = null;
  // let toDoAction = store.getState().app.toDoAction;
  setTimeout(function() {
    window.callServer(context, '/auth', {
      action: 'acceptTerm'
    }, function(resp) {
      if (resp && resp.success){
        store.dispatch({
          type: EXIST_USER_LOGIN,
          optionsMap: resp.optionsMap,
          agent: resp.agent,
          companyInfo: resp.companyInfo,
          profileTemplate: resp.profileTemplate,
          needTemplate: resp.needTemplate,
          initOpenApprovalPage: resp.openApprovalPage,
          isFAFirm: resp.isFAFirm
        });
      } else {
        store.dispatch({
          type: SHOW_APP_ERR_MSG,
          errorMsg: {
            isError: true,
            errorMsg: "Server Error, Login failure.",
            action: function() {
              logout(context);
            }
          }
        });
      }
    });
  }, 100);
}

export let rejectTerm = function(context) {
    let {
    store
    // , router
  } = context;

  // let respAction = null;
  // let toDoAction = store.getState().app.toDoAction;
  setTimeout(function() {
    window.callServer(context, '/auth', {
      action: 'rejectTerm'
    }, function(resp) {
      if (resp && resp.success){
        logout(context);
      } else {
        store.dispatch({
          type: SHOW_APP_ERR_MSG,
          errorMsg: {
            isError: true,
            errorMsg: "Server Error, Login failure.",
            action: function() {
              logout(context);
            }
          }
        });
      }
    });
  }, 100);
}

export let login = function(context, data) {
  // call server here to check to auth
  let {
    store
    // , router
  } = context;

  if (!data) {
    data = {}
  }
  data.action = 'login'

  Object.assign(data, {
    loginTime: new Date().getTime(),
		device: platform.os && platform.os.toString(),
		browser: platform.name +" "+ platform.version +" "+platform.layout
  })

  // let respAction = null;
  // let toDoAction = store.getState().app.toDoAction;
  setTimeout(function() {
    window.callServer(context, '/auth', data, function(resp) {
      //if (resp && resp.success){
      if (resp && resp.loginSuccess === 'Y'){
        resetSession(context, true);
        if (resp.showTerms) {
          store.dispatch({
            type: SHOW_TERMS,
            tncContent: resp.tncContent
          })
        } else {
          let agent = resp.agent;
          let {agentProfilePic} = agent;
          store.dispatch({
            type: EXIST_USER_LOGIN,
            optionsMap: resp.optionsMap,
            agent: agent,
            companyInfo: resp.companyInfo,
            profileTemplate: resp.profileTemplate,
            needTemplate: resp.needTemplate,
            initOpenApprovalPage: resp.openApprovalPage,
            sysParameterConstant: resp.sysParameterConstant,
            isFAFirm: resp.isFAFirm,
            notifications: _.get(resp, 'agent.notifications', []),
            exceedMaxConcurrentLogin: resp.exceedMaxConcurrentLogin
          });
          if (agentProfilePic && agentProfilePic.revpos){
            store.dispatch({
              type: UPDATE_ATTACHMENT_PROFILE_PIC,
              agentProfilePicRev: agentProfilePic.revpos
            });
          }
        }
      //  router.history.push('land');
      } else {
        store.dispatch({
          type: SHOW_APP_ERR_MSG,
          errorMsg: {
            isError: true,
            errorMsg: "Login Failure",
            action: function() {
              logout(context);
            }
          }
        });
      }
    });
  }, 100);
};

let checkDateBack = function(context, isTest) {
  let localTime = (new Date()).toString();
  if (isTest) {
    localTime = 'Tue May 31 2016 10:10:02 GMT+0800 (HKT)';
  }
  let data = {
    action: 'checkDateBack',
    localTime: localTime,
  };
  window.callServer(context, '/agent', data, function(resp) {
    if (resp.success){
      //check dateback every 60 secs
      setTimeout(function(){checkDateBack(context, false);}, 1000 * 60);
    } else {
      logout(context);
    }
  });
};

export function forgetPassword(router) {
  router.history.replace( 'forgetpass');
}

export function goHome(router){
  router.history.push('land');
}

