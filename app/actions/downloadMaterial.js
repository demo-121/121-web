export const GET_MATERIAL = 'GET_MATERIAL';

export function getDownloadMaterial(context, callback){
    let {
        store
        // , router
      } = context;

      window.callServer(
        context,
        '/downloadMaterial',
        {
          action: 'getDownloadMaterial'
        }, function(resp){
          if (resp && resp.success){
            store.dispatch({
              type: GET_MATERIAL,
              materialList: resp.materialList
            });
          }
        }
      );
}


