export const LOGOUT = 'LOGOUT';
export const SHOW_LOADING = 'SHOW_LOADING';
export const HIDE_LOADING = 'HIDE_LOADING';
export const SHOW_APP_ERR_MSG = 'SHOW_APP_ERR_MSG';
export const HIDE_APP_ERR_MSG = 'HIDE_APP_ERR_MSG';
export const CHANGE_LANG = 'CHANGE_LANG';
export const SESSION_TIME_OUT = 'SESSION_TIME_OUT';

export function showLoading(context) {
  context.store.dispatch({
    type: SHOW_LOADING
  });
}

export function hideLoading(context) {
  context.store.dispatch({
    type: HIDE_LOADING
  });
}

// errorContent: { hasTitle, isError, action, errorMsg }
export function showErrorMsg(context, errorContent) {
  context.store.dispatch({
    type: SHOW_APP_ERR_MSG,
    errorMsg: errorContent
  });
}

export function hideErrorMsg(context){
  context.store.dispatch({
    type: HIDE_APP_ERR_MSG
  });
}

export function logout(context, isSessionTimeout) {
  window.loggedIn = false;
  window.location = './logout';
}

export function logoutForMultipleConcurrentLogin(context, isSessionTimeout) {
  window.loggedIn = false;
  window.location = './';
}

export function getValueByView(context, view, startKey, endKey, cb) {
  window.callServer(
    context,
    '/view',
    {
      action: 'getViewnByParam',
      view,
      startKey,
      endKey
    },
    (resp) => {
      if (resp && resp.success) {
        cb(resp);
      }else {
        cb({success: false});
      }
    }
  );
}
