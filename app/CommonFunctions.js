import * as _ from 'lodash';
import SystemConstants from './constants/SystemConstants';
const SGTIMEZONE = 8;

let getSGTimeZone = function() {
    return SGTIMEZONE + (new Date()).getTimezoneOffset() / 60;
};

/**This function aligns with bz/CommonFunctions */
module.exports.getLocalTime = function(){
    return new Date((new Date()).getTime() + getSGTimeZone() * 3600 * 1000);
};


module.exports.getNameListStringForAllInsured = function(item){
    let nameLabel;

    // MultiInsured Case, e.g.: Shield
    if (_.get(item, 'quotType', '') === 'SHIELD' || _.get(item, 'type') === SystemConstants.DOCTYPE.MASTERAPP) {
        let insuredList;
        let nameList = [];
        if (_.get(item, 'type') === SystemConstants.DOCTYPE.MASTERAPP) {
            insuredList = _.get(item, 'quotation.insureds', []);
        } else {
            insuredList = _.get(item, 'insureds', []);
        }
        _.forEach(insuredList, (insured) =>{
            nameList.push(_.trim(_.get(insured, 'iFullName', '')));
        });
        nameLabel = nameList.join(', ');
    } else {
        // One Insured Case, e.g.: Savvy Saver
        const {iName, pName} = item;
        if (iName) {
            if (pName) {
                if (iName === pName){
                    nameLabel = iName;
                } else {
                    nameLabel = pName + ', ' + iName;
                }
            } else {
                nameLabel = iName;
            }
        }
    }

    return nameLabel;
};
