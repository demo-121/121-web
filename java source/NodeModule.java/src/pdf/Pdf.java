package pdf;

import java.io.*;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.json.JSONObject;


import com.realobjects.pdfreactor.PDFreactor;
import com.realobjects.pdfreactor.Configuration;
import com.realobjects.pdfreactor.Result;

public class Pdf {

	public static String setPassword(String base64Pdf, String password){
		byte[] encodedByte = Base64.decodeBase64(base64Pdf);
		PDDocument doc = null;
		try {
			doc = PDDocument.load(encodedByte);

			AccessPermission ap = new AccessPermission();
			ap.setCanAssembleDocument(false);
			ap.setCanExtractContent(false);
			ap.setCanExtractForAccessibility(false);
			ap.setCanFillInForm(false);
			ap.setCanModify(false);
			ap.setCanModifyAnnotations(false);
			ap.setCanPrint(true);
			ap.setCanPrintDegraded(false);
			ap.setReadOnly();
			StandardProtectionPolicy policy = new StandardProtectionPolicy(password, password, ap);
			doc.protect(policy);
			System.out.println(doc.isEncrypted() ? "isEncrypted" : "isNotEncrypted");
			return getBase64Pdf(doc);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (doc != null) {
					doc.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

  private static String[] getSplitedString(String names){
    String[] newline_names = new String[16];
    int maxLength = 42 - 1;
    int numberLine = 0;
    String senetence = "";
    int length = 0;

    for (String name: names.split(" "))
    {
      length = senetence.length();
      if(length + name.length() < maxLength){
        if(senetence.equals("")){
          senetence = name;
        }else{
          senetence += " " + name;
        }
      }else if(length + name.length() == maxLength){
        senetence += " " + name;
        newline_names[numberLine++] = senetence;
        senetence = "";
      }else if(name.length() < maxLength){
        newline_names[numberLine++] = senetence;
        senetence = name;
      }else{
        senetence += " ";
        char[] charArray = name.toCharArray();
        for(int m = 0; m < name.length(); m++){
          length = senetence.length();
          if(length + 5 >= maxLength && m == 0){
            newline_names[numberLine++] = senetence;
            senetence = "";
          }else if(length == maxLength){
            newline_names[numberLine++] = senetence;
            senetence = "";
          }else{
            senetence += charArray[m];
          }
        }

      }
    }
    newline_names[numberLine++] = senetence;
    while(numberLine < 2){
      newline_names[numberLine++] = " ";
    }
    return newline_names;
  }

	public static String merge(String[] base64Pdfs) {
		PDFMergerUtility merger = new PDFMergerUtility();
		ByteArrayOutputStream os = null;
		try {
			for (String base64Pdf : base64Pdfs) {
				if (base64Pdf != null && base64Pdf.length() > 0) {
					InputStream is = getInputStreamFromBase64Str(base64Pdf);
					merger.addSource(is);
				}
			}
			os = new ByteArrayOutputStream();
			merger.setDestinationStream(os);
			merger.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
			return getBase64Pdf(os);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (os != null) {
					os.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

  public static String addNames(String base64Pdf, String financialConsultantName, String proposerName, float x1, float y1, float x2, float y2) {
    byte[] encodedByte = Base64.decodeBase64(base64Pdf);
    PDDocument doc = null;
    float diff_x1 = 0;
    float diff_y1 = 10;
    float diff_x2 = 0;
    float diff_y2 = 10;
    String[] financialConsultantNames = getSplitedString(financialConsultantName);
    String[] proposerNames = getSplitedString(proposerName);

    try {
      doc = PDDocument.load(encodedByte);
      PDDocumentInformation docInfo = doc.getDocumentInformation();
      PDPage firstPage = doc.getPage(0);
      doc.setDocumentInformation(docInfo);
      try (PDPageContentStream contentStream = new PDPageContentStream(doc, firstPage, PDPageContentStream.AppendMode.APPEND,true,true)) {

        contentStream.setFont(PDType1Font.HELVETICA, 10);

        contentStream.beginText();
        contentStream.newLineAtOffset(x1, y1);
        contentStream.showText(financialConsultantNames[0]);
        contentStream.endText();
        // Loop to create 25 lines of text
        if(financialConsultantNames.length > 1){
          contentStream.beginText();
          contentStream.newLineAtOffset(x1 - diff_x1, y1 - diff_y1);
          contentStream.showText(financialConsultantNames[1]);
          contentStream.endText();
        }
        contentStream.beginText();
        contentStream.newLineAtOffset(x2, y2);
        contentStream.showText(proposerNames[0]);
        contentStream.endText();

        if(proposerNames.length > 1){
          contentStream.beginText();
          contentStream.newLineAtOffset(x2 + diff_x2, y2 - diff_y2);
          contentStream.showText(proposerNames[1]);
          contentStream.endText();
        }

        contentStream.close();
      } catch(Exception ex){
        System.out.println("Content added failed " + ex);
      }
      return getBase64Pdf(doc);

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (doc != null) {
          doc.close();
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  public static String addTitle(String base64Pdf, String title) {
		byte[] encodedByte = Base64.decodeBase64(base64Pdf);
		PDDocument doc = null;
		try {
			doc = PDDocument.load(encodedByte);
			PDDocumentInformation docInfo = doc.getDocumentInformation();
			docInfo.setTitle(title);
			doc.setDocumentInformation(docInfo);
			return getBase64Pdf(doc);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (doc != null) {
					doc.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static String convertBase64Images2Pdf(String[] base64Imgs) {
	 	if (base64Imgs == null) {
	 		return null;
		}
		PDDocument doc = new PDDocument();
		try {
			for (String base64Img : base64Imgs) {
				PDPage page = new PDPage();
				doc.addPage(page);

				InputStream is = getInputStreamFromBase64Str(base64Img);
				PDImageXObject imgObj = LosslessFactory.createFromImage(doc, ImageIO.read(is));
				PDPageContentStream pageStream = new PDPageContentStream(doc, page);
				float scale = getImgScale(page, imgObj, 40);
				pageStream.drawImage(imgObj, 40f, page.getMediaBox().getHeight() - imgObj.getHeight() * scale - 40, imgObj.getWidth() * scale, imgObj.getHeight() * scale);
				pageStream.close();
			}
			return getBase64Pdf(doc);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				doc.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static float getImgScale(PDPage page, PDImageXObject img, int margin) {
		float scale = 1;
		float availableWidth = page.getMediaBox().getWidth() - margin * 2;
		if (img.getWidth() > availableWidth) {
			scale = Math.min(scale, availableWidth / img.getWidth());
		}
		float availableHeight = page.getMediaBox().getHeight() - margin * 2;
		if (img.getHeight() > availableHeight) {
			scale = Math.min(scale, availableHeight / img.getHeight());
		}
		return scale;
	}

	private static InputStream getInputStreamFromBase64Str(String base64Str) {
		byte[] encodedByte = Base64.decodeBase64(base64Str);
		return new ByteArrayInputStream(encodedByte);
	}

	private static String getBase64Pdf(ByteArrayOutputStream os) {
		if (os == null) {
			return null;
		}
		return Base64.encodeBase64String(os.toByteArray());
	}

	private static String getBase64Pdf(PDDocument doc) throws IOException {
	 	if (doc == null) {
	 		return null;
		}
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			doc.save(os);
			return Base64.encodeBase64String(os.toByteArray());
		} finally {
			os.close();
		}
	}

	public static String convertHtml2Pdf(String html, String pdfOptionsStr){
		try{
			PDFreactor pdfReactor = new PDFreactor();
			Configuration config = new Configuration();
			String license = "<license><licenseserialno>4436</licenseserialno><licensee><name>AXA Singapore</name><address><street>8 Shenton Way, #24-01 AXA Tower</street><city>Singapore 068811</city><country>Singapore</country></address></licensee><product>PDFreactor</product><majorversion>9</majorversion><minorversion>0</minorversion><licensetype>CPU</licensetype><amount>8</amount><unit>CPU</unit><maintenanceexpirationdate>2019-01-26</maintenanceexpirationdate><expirationdate>2019-01-26</expirationdate><purchasedate>2018-01-26</purchasedate><options><option>pdf</option></options><advanced><conditions><condition>This license is for use on development systems only. It may not be used on staging or productive systems of any kind.</condition></conditions></advanced><signatureinformation><signdate>2018-01-26 11:18</signdate><signature>302d021473bc6def8c7e0b2631a03a6c194237303638a68302150081f7737eca6f2669e2441526f2bbba9d4912089b</signature><checksum>3334</checksum></signatureinformation></license>";
			config.setLicenseKey(license);
			JSONObject pdfOptions = null;
			if(pdfOptionsStr != null){
				pdfOptions = new JSONObject(pdfOptionsStr);
			}
			config.setDocument(html);
			Result result = pdfReactor.convert(config);
			byte[] pdf = result.getDocument();
			return Base64.encodeBase64String(pdf);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
