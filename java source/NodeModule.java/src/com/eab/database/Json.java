package com.eab.database;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Base64.Encoder;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Json {
	private static final boolean DEBUG = false;
	
	/**
	 * Convert ResetSet to be JsonArray
	 * @param rs - ResetSet
	 * @return JsonArray
	 */
	public static JsonArray ResultSetToJsonArray(ResultSet rs) {
		JsonObject element = null;
		JsonArray ja = new JsonArray();
		ResultSetMetaData rsmd = null;
		String columnName, columnValue, columnType = null;
		Encoder encoder = null;
		
		try {
			encoder = Base64.getEncoder();
			rsmd = rs.getMetaData();
			while (rs.next()) {
				element = new JsonObject();
				for (int i = 0; i < rsmd.getColumnCount(); i++) {
					columnName = rsmd.getColumnName(i + 1);
					columnType = rsmd.getColumnTypeName(i + 1);

					if (DEBUG) System.out.println("Column: " + columnName + ", Type: " + columnType);
					if ("BLOB".equalsIgnoreCase(columnType)) {
						columnValue = encoder.encodeToString(rs.getBlob(columnName).getBytes(1, (int)rs.getBlob(columnName).length()));
					} else {
						columnValue = rs.getString(columnName);
					}
					columnValue = rs.getString(columnName);
					element.addProperty(columnName, columnValue);
				}
				ja.add(element);
			}
		} catch (SQLException e) {
//			e.printStackTrace();
		} finally {
			encoder = null;
		}
		
		return ja;
	}
 
	/**
	 * Convert ResetSet to be JsonObject
	 * @param rs - ResetSet
	 * @return ResetSet
	 */
    public static JsonObject ResultSetToJsonObject(ResultSet rs) {
		JsonObject element = null;
		JsonArray ja = new JsonArray();
		JsonObject jo = new JsonObject();
		ResultSetMetaData rsmd = null;
		String columnName, columnValue, columnType = null;
		Encoder encoder = null;
		
		try {
			encoder = Base64.getEncoder();
			rsmd = rs.getMetaData();
			while (rs.next()) {
				element = new JsonObject();
				for (int i = 0; i < rsmd.getColumnCount(); i++) {
					columnName = rsmd.getColumnName(i + 1);
					columnType = rsmd.getColumnTypeName(i + 1);
					columnValue = null;

					if (DEBUG) System.out.println("Column: " + columnName + ", Type: " + columnType);
					if ("BLOB".equalsIgnoreCase(columnType)) {
						Blob objBlob = rs.getBlob(columnName);
						if (objBlob != null) {
							columnValue = encoder.encodeToString(objBlob.getBytes(1, (int)objBlob.length()));
						}
					} else {
						columnValue = rs.getString(columnName);
					}
					element.addProperty(columnName, columnValue);
					
				}
				ja.add(element);
			}
			jo.add("result", ja);
		} catch (SQLException e) {
//			e.printStackTrace();
		} finally {
			encoder = null;
		}
		return jo;
	}
 
    /**
     * Convert ResultSet to be Json String
     * @param rs - ResetSet
     * @return Json String
     */
    public static String ResultSetToJsonString(ResultSet rs) {
        return ResultSetToJsonObject(rs).toString();
    }
    
    /**
     * Generate Sample Json
     * @param isError - Is Error Output?
     * @param code - Code
     * @param message - Message
     * @return Json String
     */
    public static String GetOutputJsonString(boolean isError, String code, String message) {
    	JsonObject element = null;
    	String output = null;
    	
    	try {
    		element = new JsonObject();
    		element.addProperty("error", isError);
    		element.addProperty("code", code);
    		element.addProperty("message", message);
    		output = element.toString();
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	
    	return output;
    }
}
