package com.eab.database;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONObject;

import sun.misc.BASE64Decoder;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.DatabaseOptions;
import com.couchbase.lite.Document;
import com.couchbase.lite.JavaContext;
import com.couchbase.lite.Manager;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.UnsavedRevision;
import com.couchbase.lite.auth.Authenticator;
import com.couchbase.lite.auth.AuthenticatorFactory;
import com.couchbase.lite.replicator.Replication;
import com.couchbase.lite.util.Log;
import com.eab.Util;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CBLite {
	private final static boolean DEBUG = false;
	private final static String DB_NAME = "mycbl";	//database names cannot contain uppercase letters.
	
	public static JavaContext context;
	public static Manager manager;
	public static Database database;
	public static Replication pullRep;
	public static Replication pushRep;
	
	/*
	 * destructor
	 */
	protected void finalize() {
		Close();
	}
	
	public static boolean Connect(String dbName) {
		return Connect(dbName, null);
	}
	
	public static boolean firstCBConncet(String dbName, String encryptKey) {
		String keyString = encryptKey.substring(0, 11);
		return Connect(dbName, keyString);
	}
	
	
	public static boolean Connect(String dbName, String encryptKey) {
		boolean status = false;
		DatabaseOptions options = null;
		
		try {
			if (manager != null) {
				Close();
			}
			
			context = new JavaContext();
			
			// Create CBLite Manager.
			if (context != null) {
				if (DEBUG) System.out.println("context.getFileDir() = " + context.getFilesDir());
				manager = new Manager(context, Manager.DEFAULT_OPTIONS);
			}
			
			// Connect Database.
			if (manager != null) {
				if (encryptKey != null && encryptKey.length() > 0) {
					if (DEBUG) System.out.println("Create / Open database " + DB_NAME + " with Encryption Key [" + encryptKey + "].");
					if (DEBUG) System.out.println("...Manager.isValidDatabaseName(" + dbName + ") is " + Manager.isValidDatabaseName(dbName));
					
					options = new DatabaseOptions();
					//if (!Manager.isValidDatabaseName(dbName)) {		// Create if Database Name is not exist.
						options.setCreate(true);
						if (DEBUG) System.out.println("...Database " + DB_NAME + " is not exist.");
					//}
					options.setEncryptionKey(encryptKey);
					database = manager.openDatabase(dbName, options);
				} else {
					if (DEBUG) System.out.println("Create / Open database " + DB_NAME + ".");
					database = manager.getDatabase(dbName);
				}
			}
			
			if (database.isOpen()) {
				status = true;
			}
		} catch(CouchbaseLiteException e) {
			if (DEBUG) e.printStackTrace();
		} catch(IOException e) {
			if (DEBUG) e.printStackTrace();
		}
		
		return status;
	}
	
	public static void Close() {
		// Close Database.
		if (database != null && database.isOpen()) {
			database.close();
		}
		database = null;
		
		// Close Manager.
		if (manager != null) {
			manager.close();
		}
		manager = null;

		// initialize Context.
		context = null;
	}
	
	public static boolean Sync(String url, String user, String password) {
		boolean status = false;
		URL urlObj = null;
		Authenticator auth = null;
		
		try {
			urlObj = new URL(url);
			
			if (database != null && database.isOpen()) {
				pullRep = database.createPullReplication(urlObj);
				pullRep.setContinuous(true);
				
				pushRep = database.createPushReplication(urlObj);
				pushRep.setContinuous(true);
				
				if (user != null) {
					auth = AuthenticatorFactory.createBasicAuthenticator(user, password);
					pullRep.setAuthenticator(auth);
					pushRep.setAuthenticator(auth);
				}
				
				pullRep.start();
				pushRep.start();
				
				status = true;
			}
		} catch(MalformedURLException e) {
			if (DEBUG) e.printStackTrace();
		}
		
		return status;
	}
	
	public static void StopSync() {
		
		try {
			if (pullRep != null) {
				pullRep.stop();
			}
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		}
		pullRep = null;
		
		try {
			if (pushRep != null) {
				pushRep.stop();
			}
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		}
		pushRep = null;
		
	}
	
	public static String GetDocument(String docID) {
		String output = null;
		Document document = null;
		JSONObject jsonObj = null;
		Map<String, Object> json = null;
		
		// Check Database is ready.
		if (database != null && database.isOpen()) {
			
			// Get Properties by ID from database.
			document = database.getDocument(docID);
			if (document != null) {
				json = document.getProperties();
				jsonObj = new JSONObject(json);
			}
			
			// Convert Properties into JSON format.
			output = jsonObj.toString();
		}
		
		return output;
	}
	
	public static Map<String, Document> GetAllDocument() {
		Map<String, Document> output = null;
		int docCount = 0;
		QueryRow row = null;
		Query query = null;
		JSONObject jsonObj = null;
		Map<String, Object> json = null;
		
		try {
			output = new HashMap<String, Document>();
			
			// Check Database is ready.
			if (database != null && database.isOpen()) {
				query = database.createAllDocumentsQuery();
				query.setAllDocsMode(Query.AllDocsMode.ALL_DOCS);
				QueryEnumerator result = query.run();
				for (Iterator<QueryRow> it = result; it.hasNext(); ) {
				    row = it.next();
				    String docID = row.getDocumentId();
				    Document document = row.getDocument();
				    output.put(docID, document);
				    docCount++;
				    
				    if (DEBUG) {
				    	json = document.getProperties();
				    	jsonObj = new JSONObject(json);
				    	System.out.println("DocID=" + docID + ", Document=" + jsonObj.toString());
				    }
				}
			}
		} catch(CouchbaseLiteException e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			row = null;
			query = null;
			jsonObj = null;
			json = null;
		}
		
		return output;
	}
	
	public static String CreateDocument(String docID, String json) {
		String revision = null;
		ObjectMapper objMapper = null;
		Document document = null;
		UnsavedRevision newRevision = null;
		
		try {
			// Check Database is ready.
			if (database != null && database.isOpen()) {
				objMapper = new ObjectMapper();
				// Convert JSON to Properties (HashMap<String, Object>).
				Map<String,Object> docObj = objMapper.readValue(json, HashMap.class);
				
				if (DEBUG) {
					for(Entry<String, Object> entry : docObj.entrySet()) {
						System.out.println("[" + entry.getKey() + "]:[" + entry.getValue().toString() + "]");
					}
				}
				
				// Open document
				document = database.getDocument(docID);
				
				// Put Properties into database.
				document.putProperties(docObj);
				
				revision = document.getCurrentRevisionId();
			}
		} catch(JsonMappingException e) {
			if (DEBUG) e.printStackTrace();
		} catch(JsonParseException e) {
			if (DEBUG) e.printStackTrace();
		} catch(IOException e) {
			if (DEBUG) e.printStackTrace();
		} catch(CouchbaseLiteException e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			objMapper = null;
		}
		
		return revision;
	}
	
	
	public static String UpdateDocument(String docID, String json) {
		String revision = null;
		ObjectMapper objMapper = null;
		Document document = null;
		UnsavedRevision newRevision = null;
		
		try {
			// Check Database is ready.
			if (database != null && database.isOpen()) {
				objMapper = new ObjectMapper();
				// Convert JSON to Properties (HashMap<String, Object>).
				Map<String,Object> docObj = objMapper.readValue(json, HashMap.class);
				
				// Open document
				document = database.getDocument(docID);
				
				// Put Properties into database.
				newRevision = document.getCurrentRevision().createRevision();
				newRevision.setProperties(docObj);
				newRevision.save();		// Auto Commit document update.
				revision = document.getCurrentRevisionId();
			}
		} catch(JsonMappingException e) {
			if (DEBUG) e.printStackTrace();
		} catch(JsonParseException e) {
			if (DEBUG) e.printStackTrace();
		} catch(IOException e) {
			if (DEBUG) e.printStackTrace();
		} catch(CouchbaseLiteException e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			objMapper = null;
		}
		
		return revision;
	}
	
	public static boolean DeleteDocument(String docID) {
		boolean status = false;
		Document document = null;
		
		try {
			// Check Database is ready.
			if (database != null && database.isOpen()) {
				// Open document
				document = database.getDocument(docID);
				
				if (document != null && !document.isDeleted()) {
					// Delete by ID from database.
					status = document.delete();
				}
			}
		} catch(CouchbaseLiteException e) {
			if (DEBUG) e.printStackTrace();
		}
		
		return status;
	}
	
	
	public static String UpdateAttach(String docID, String name, String mime, String base64Str) {
		String revision = null;
		BASE64Decoder decoder = null;
		byte[] attachByte = null;
		Document document = null;
		UnsavedRevision newRevision = null;
		InputStream is = null;
		
		try {
			// Convert Base64 String to Byte Array
			if (docID != null && name != null && mime != null && base64Str != null && base64Str.length() > 0) {
				decoder = new BASE64Decoder();
				attachByte = decoder.decodeBuffer(base64Str);
				is = new ByteArrayInputStream(attachByte);
				
				if (database != null && database.isOpen()) {
					document = database.getDocument(docID);
					newRevision = document.getCurrentRevision().createRevision();
					newRevision.setAttachment(name, mime, is);
					newRevision.save();		// Auto Commit document update.
					revision = newRevision.getId();
				}
				is.close();
			}
		} catch(IOException e) {
			if (DEBUG) e.printStackTrace();
		} catch(CouchbaseLiteException e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			is = null;
			decoder = null;
		}
		
		return revision;
	}
	
	public static String GetAttach(String docID, String name) {
		///TODO
		
		return null;
	}
	
	public static boolean DeleteAttach(String docID, String name) {
		///TODO
		
		return false;
		
	}
}
