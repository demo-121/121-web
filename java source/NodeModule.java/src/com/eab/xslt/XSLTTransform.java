package com.eab.xslt;
import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.json.JSONArray;
import org.json.JSONObject;

public class XSLTTransform {
	
	public static void main(String[] args) throws Exception{
		// Unit Test
		String js = "{data:[{\"No\":\"1\",\"Name\":\"ABC\"},{\"No\":\"2\",\"Name\":\"PQR\"},{\"No\":\"3\",\"Name\":\"XYZ\"}]}";
    	
		String xml = jsonToXml(js);
		System.out.println("*** op OF xml ***" + xml);
		System.out.println("*** END OF PROCESS ***");
	}
	
    public static String transformXml(String xml, String xslt) {
        String retVal = "";
        try {
            StringReader reader = new StringReader(xml);
            StringWriter writer = new StringWriter();
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(new ByteArrayInputStream(xslt.getBytes("UTF-8"))));
            transformer.transform(new StreamSource(reader), new StreamResult(writer));
            return writer.toString();
        } catch (Exception ex) {
            System.out.println("Exception in transformXml:" + ex.getMessage());
        }
        return retVal;
    }
    
    
    public static String jsonToXml(String jsonStr){
    	
    	//String js = "{data:[{\"No\":\"1\",\"Name\":\"ABC\"},{\"No\":\"2\",\"Name\":\"PQR\"},{\"No\":\"3\",\"Name\":\"XYZ\"}]}";
    	JSONObject jsonObj = new JSONObject(jsonStr); 
    	String xml = org.json.XML.toString(jsonObj);
    	return xml;
    	
    }
    public static String test() {
        return "abc";
    }
}
